package com.bokdoc.pateintsbook.ui.notifications

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.repositories.notifications.NotificationsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.notifications.NotificationsWebservice
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.utils.Utils
import kotlinx.android.synthetic.main.activity_notifications.*

class NotificationsActivity : AppCompatActivity() {

    private lateinit var notificationsViewModel:NotificationsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        notificationsViewModel = ViewModelsCustomProvider(NotificationsViewModel(
                NotificationsRepository(UserSharedPreference(applicationContext),
                        NotificationsWebservice(WebserviceManagerImpl.instance())))).create(NotificationsViewModel::class.java)

        notificationsViewModel.notificationsLiveData.observe(this, Observer {
            if(it?.status == DataResource.SUCCESS) {
                it.data?.let {
                    if (it.isNotEmpty()) {
                        rv_notifications.adapter = NotificationsAdapter(it, this::onClickListener)
                    }
                }
            }else{
                it?.message?.let {
                    if(it.isNotEmpty()){
                        Utils.showLongToast(this,it)
                    }else{
                        Utils.showLongToast(this,getString(R.string.server_error))
                    }
                }
            }
        })



    }


    private fun onClickListener( notifications:INotifications,dynamicLink:DynamicLinks,position:Int){

    }
}
