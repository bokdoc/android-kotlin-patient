package com.bokdoc.pateintsbook.ui.patientReviews

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Review
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationAdapterCallback
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_reviews.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.ArrayList

class ReviewsActivity : ParentActivity(), PaginationAdapterCallback {


    lateinit var reviewsViewModel: ReviewsViewModel

    private var isLoading = false
    private var isLastPage = false

    private var reviewsAdapter: ReviewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)
        enableBackButton()
        title = getString(R.string.reviews)
        filterIcon.visibility = VISIBLE
        getViewModel()

        getDataFromIntent()
        filterIcon.setOnClickListener {
            goToFilterActivity()
        }

        rightTextView.visibility = View.GONE

        val layoutManager: LinearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_patient_reviews.layoutManager = layoutManager

        rv_patient_reviews.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                return reviewsViewModel.currentPage >= reviewsViewModel.totalPages
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                reviewsViewModel.currentPage += 1
                reviewsAdapter?.addLoadingFooter()
                reviewsViewModel.getReviews()
            }


        })

        when (hasInternetConnection()) {
            false -> showError(getString(R.string.no_connection), lay_container)
            true -> {
                reviewsViewModel.getReviews()
            }
        }

        setupViewModelObservers()

    }

    private fun getDataFromIntent() {
        if (intent.hasExtra(getString(R.string.idKey))) {
            reviewsViewModel.providerId = intent.getStringExtra(getString(R.string.idKey))
        } else {
            reviewsViewModel.providerId = reviewsViewModel.getUserId()
        }
    }

    private fun setupViewModelObservers() {

        reviewsViewModel.nextReviewsLiveData.observe(this, Observer {
            isLoading = false
            if (it != null && it.isNotEmpty()) {
                reviewsAdapter?.removeLoadingFooter()
                reviewsAdapter?.addAll(it as MutableList<Review>)
            }
        })

        reviewsViewModel.firstReviewsLiveData.observe(this, Observer {
            progress.visibility = GONE
            when (it!!.size) {
                0 -> {
                    showEmpty(getString(R.string.no_reviews_found), "", null)
                    filterIcon.visibility = View.GONE
                }
                else -> {
                    showMain(lay_container)
                    rightTextView.visibility = VISIBLE
                    reviewsAdapter = ReviewsAdapter(activity, (it as MutableList<Review>?)!!)
                    rv_patient_reviews.adapter = reviewsAdapter
                }
            }
        })

        reviewsViewModel.metaLiveData.observe(this, Observer {

            /*
            TOTAL_PAGES = it!!.pagination.totalPages!!

            if (currentPage <= TOTAL_PAGES) {
                reviewsAdapter?.addLoadingFooter()
            } else
                isLastPage = true
                */
        })



        reviewsViewModel.progressLiveData.observe(this, Observer {
            when (it) {
                false -> {
                    rv_patient_reviews.visibility = VISIBLE
                    progress.visibility = View.GONE
                }

                true -> {
                    rv_patient_reviews.visibility = GONE
                    progress.visibility = View.VISIBLE

                }
            }
        })

        reviewsViewModel.errorMessageLiveData.observe(this, Observer {
            showError(getString(R.string.some_thing_went_wrong), lay_container)
        })

    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            filterIcon.id -> {
                goToFilterActivity()
            }

        }
    }

    override fun retryPageLoad() {

    }

    private fun goToFilterActivity() {
        if (reviewsViewModel.metaLiveData.value?.filter != null && reviewsViewModel.metaLiveData.value?.filter!!.isNotEmpty()) {
            val intent = Intent(activity, FilterActivity::class.java).putParcelableArrayListExtra("list",
                    reviewsViewModel.metaLiveData.value?.filter?.get(0)?.data as ArrayList<out Parcelable>)
            activity.startActivityForResult(intent, FILTER_REQUEST)
        }
    }


    private fun onAdapterCLickListener(view: View, profile: Profile, selectedPosition: Int) {

    }

    private fun getViewModel() {
        reviewsViewModel = InjectionUtil.getReviewsViewModel(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILTER_REQUEST && resultCode == Activity.RESULT_OK) {
//            (rv_patient_reviews.adapter as ReviewsAdapter).clear()
            reviewsViewModel.serviceType = (data!!.getStringExtra("selectedParam"))
            reviewsViewModel.currentPage = 1
            reviewsViewModel.getReviews()

        }
    }


    companion object {
        private val PAGE_START = 1
        private val FILTER_REQUEST = 2
    }

}