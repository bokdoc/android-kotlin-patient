package com.bokdoc.pateintsbook.ui.medicalHistory.medications

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil

class MedicationsActivity : MedicalHistoryActivity() {

    override fun getViewModel(): MedicalHistoryViewModel {
        return InjectionUtil.getMedicationsViewModel(this)
    }

    override fun getLookupType(): String {
        return getString(R.string.lookupMedications)
    }

    override fun title(): String {
        return getString(R.string.myMedications)
    }

    override fun emptyDataMessage(): String {
        return getString(R.string.medications_profile_empty_text)
    }
}