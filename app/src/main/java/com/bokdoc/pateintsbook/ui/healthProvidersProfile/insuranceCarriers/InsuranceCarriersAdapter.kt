package com.bokdoc.pateintsbook.ui.healthProvidersProfile.insuranceCarriers

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.InsuranceCarriersRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.text_with_line_row.view.*

class InsuranceCarriersAdapter(private val insuranceCarriers: List<InsuranceCarriersRow>) : RecyclerView.Adapter<InsuranceCarriersAdapter.InsuranceCarriersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): InsuranceCarriersViewHolder {
        return InsuranceCarriersViewHolder(parent.inflateView(R.layout.text_with_line_row))
    }

    override fun getItemCount(): Int {
        return insuranceCarriers.size
    }

    override fun onBindViewHolder(holder: InsuranceCarriersViewHolder, position: Int) {
        insuranceCarriers[position].let {
            holder.itemView.tv_title.text = it.name
        }
    }


    inner class InsuranceCarriersViewHolder(view: View) : RecyclerView.ViewHolder(view)
}