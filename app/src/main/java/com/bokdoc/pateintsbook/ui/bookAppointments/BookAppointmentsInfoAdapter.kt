package com.bokdoc.pateintsbook.ui.bookAppointments

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_appointement_info_row.view.*

class BookAppointmentsInfoAdapter(private val titles:List<String>,private val values:List<String>):
        RecyclerView.Adapter<BookAppointmentsInfoAdapter.InfoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoViewHolder {
        return InfoViewHolder(parent.inflateView(R.layout.item_appointement_info_row))
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    override fun onBindViewHolder(holder: InfoViewHolder, position: Int) {
        holder.itemView?.title?.text = titles[position]
        holder.itemView?.value?.text = titles[position]
    }


    class InfoViewHolder(view:View):RecyclerView.ViewHolder(view)
}