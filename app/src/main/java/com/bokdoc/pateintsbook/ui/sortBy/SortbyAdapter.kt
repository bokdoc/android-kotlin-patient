package com.bokdoc.pateintsbook.ui.sortBy

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.TextWithParam
import kotlinx.android.synthetic.main.switch_option_row.view.*

open class SortbyAdapter(private val textWithParams: List<TextWithParam>,
                         private var lastSelectedPosition: Int = -1,
                         private var onSelectListener:(View)->Unit) : RecyclerView.Adapter<SortbyAdapter.SortViewHolder>() {

    private var onBind: Boolean = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.switch_option_row, parent, false)
        return SortViewHolder(view)
    }

    override fun getItemCount(): Int {
        return textWithParams.size
    }

    override fun onBindViewHolder(holder: SortViewHolder, position: Int) {
        holder.view.optionTitle.text = textWithParams[position].text
        onBind = true
        holder.view.switchButton.isChecked = lastSelectedPosition == position
        onBind = false
        /*
        holder.view.switchButton.setOnCheckedChangeListener { compoundButton, isChecked ->
            onChecked(isChecked,position)
            notifyDataSetChanged()
        }
        */
        /*
        holder.view.switchButton.setOnClickListener {
            onChecked(true,position)
        }
        */
    }

    private fun onChecked(isChecked: Boolean, position: Int) {
        if (isChecked) {
            lastSelectedPosition = position
        } else {
            lastSelectedPosition = -1
        }
    }

    fun getSelectedParam(): String {
        return when {
            lastSelectedPosition != -1 ->
                textWithParams[lastSelectedPosition].params
            else -> ""
        }

    }

    fun resetSelection() {
        if (lastSelectedPosition != -1) {
            val position = lastSelectedPosition
            lastSelectedPosition = -1
            notifyItemChanged(position)
        }
    }


     inner class SortViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.switchButton.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (!onBind) {
                    onChecked(isChecked, layoutPosition)
                    //notifyDataSetChanged()
                    onSelectListener.invoke(view)
                }
            }
        }
    }
}