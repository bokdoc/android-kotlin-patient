package com.bokdoc.pateintsbook.ui.twilioVideoConsulting

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.twillio.TwillioRepository
import com.bokdoc.pateintsbook.data.webservice.models.ConsultingStreamResource
import com.twilio.video.VideoTrack

class TwillioViewModel(val twillioRepository: TwillioRepository) : ViewModel() {

    var requestId: String = ""
    var token: String = ""
    var twillioToken = ""
    var duration = ""
    var chatUrl = ""
    var leaveUrl = ""
    var isVideoActive = true

    var twillioRoom = ""

    fun getTwilliotoken(): LiveData<DataResource<ConsultingStreamResource>> {
        return twillioRepository.getTwillioToken(requestId, token)
    }

    fun leaveTwillio(): LiveData<DataResource<Boolean>> {
        return twillioRepository.leaveTwillioToken(leaveUrl)
    }
}