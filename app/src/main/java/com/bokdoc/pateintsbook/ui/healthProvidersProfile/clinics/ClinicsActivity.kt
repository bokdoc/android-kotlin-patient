package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.DOCTOR
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.maps.MapShowActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.toolbar.*

class ClinicsActivity : ParentActivity() {

    private lateinit var clinicsViewModel: ClinicsViewModel
    private var isLoading: Boolean = false
    private var isLastPage = false
    var clinicsAdapter: ClinicsAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()
        getViewModel()
        getDataFromIntent()
        rv_list.layoutManager = LinearLayoutManager(this)
        getClinics()
    }


    private fun getClinics() {
        progress.visibility = View.VISIBLE
        clinicsViewModel.getClinics().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    when (it.data.isNullOrEmpty()) {
                        true -> {
                            showEmpty(getString(R.string.no_surgeries), "", null)
                            progress.visibility = View.GONE
                        }
                        false -> {
                            clinicsAdapter = ClinicsAdapter((it.data as ArrayList<ClinicRow>?)!!, this::itemCLickListener, this::onMapClickLictner, this::mediaCLickListener)
                            clinicsViewModel.meta.pagination = it.meta.pagination
                            rv_list.adapter = clinicsAdapter
                            progress.visibility = View.GONE
                        }
                    }
                }
                INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE
                }
            }
        })

        setUpPagination()

    }


    private fun setUpPagination() {
        rv_list.addOnScrollListener(object : PaginationScrollListener(rv_list.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                clinicsViewModel.currentPage = clinicsViewModel.currentPage.plus(1)
                if (clinicsViewModel.currentPage <= clinicsViewModel.meta.pagination.totalPages!!) {
                    isLoading = true
                    clinicsAdapter!!.addLoadingFooter()
                    clinicsViewModel.getClinics().observe(this@ClinicsActivity, Observer {
                        when (it!!.status) {
                            SUCCESS -> {
                                if (clinicsAdapter == null) {
                                    clinicsAdapter = ClinicsAdapter((it!!.data as ArrayList<ClinicRow>?)!!, this@ClinicsActivity::itemCLickListener, this@ClinicsActivity::onMapClickLictner
                                            , this@ClinicsActivity::mediaCLickListener)
                                    rv_list.adapter = clinicsAdapter
                                    clinicsViewModel.meta.pagination = it.meta.pagination
                                } else {
                                    if (isLoading) {
                                        clinicsAdapter!!.removeLoadingFooter()
                                        isLoading = false
                                        if (it.data != null)
                                            clinicsAdapter!!.addAll(it.data as ArrayList<ClinicRow>)
                                    } else {
                                        if (it.data != null)
                                            clinicsAdapter!!.updateList(it!!.data as ArrayList<ClinicRow>)
                                    }

                                }
                            }
                            INTERNAL_SERVER_ERROR -> {
                                Utils.showShortToast(this@ClinicsActivity, getString(R.string.some_thing_went_wrong))
                                progress.visibility = View.GONE
                            }
                        }

                    })
                } else
                    isLastPage = true
            }


        })

    }


    private fun getViewModel() {
        clinicsViewModel = InjectionUtil.getClinicsViewModel(this)
    }

    private fun getDataFromIntent() {
        val typeKey = intent?.getStringExtra(getString(R.string.typeKey))!!
        when (typeKey) {
            DOCTOR.toString() -> {
                title = getString(R.string.clinics_list_doc_place_holder,
                        Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
            }
        }

        try {
            if (Profile.isHospitalCategory(typeKey.toInt())) {
                title = getString(R.string.clinics_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
            }
        } catch (e: Exception) {

        }
        showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
        clinicsViewModel.id = intent?.getStringExtra(getString(R.string.idKey))!!
    }

    fun itemCLickListener(url: String, nv: NextAvailabilitiesParcelable, position: Int) {
        if (nv.book_button!!.endpointUrl.isEmpty()) {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key)), arrayOf(url), getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nv), BookAppointmentsActivity::class.java, BOOK_REQUEST)
        } else {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)), arrayOf(nv.book_button!!.endpointUrl, url),
                    getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nv), NextAvailabilityActivity::class.java, BOOK_REQUEST)
        }
    }

    fun onMapClickLictner(location: Location) {
        Navigator.navigate(this, getString(R.string.locatioKey), location, MapShowActivity::class.java)
    }

    fun mediaCLickListener(allMedia: List<IMediaParcelable>) {
        Navigator.navigate(activity!!, arrayOf(getString(R.string.is_show_only)),
                arrayOf("true"), getString(R.string.media_key),
                allMedia as ArrayList<out Parcelable>
                , AttachmentMediaActivity::class.java, -1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }

}

