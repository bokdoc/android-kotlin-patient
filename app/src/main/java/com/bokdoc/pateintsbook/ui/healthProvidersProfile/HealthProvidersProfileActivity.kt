package com.bokdoc.pateintsbook.ui.healthProvidersProfile

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.FAIL
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.ProvidersProfileRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView.OverViewWebService
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.OverviewRow
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.DOCTOR
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.ClinicsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting.ProfileConsultingActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments.DepartmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.insuranceCarriers.InsuranceCarriersActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewRepository
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback.FeedbackAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details.QualificationsDetailsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries.ProfileSurgeriesActivity
import com.bokdoc.pateintsbook.ui.maps.MapShowActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_health_providers_profile.*
import kotlinx.android.synthetic.main.include_accommodation_section.*
import kotlinx.android.synthetic.main.include_accreditation_section.*
import kotlinx.android.synthetic.main.include_certification.*
import kotlinx.android.synthetic.main.include_education.*
import kotlinx.android.synthetic.main.include_insurance_section.*
import kotlinx.android.synthetic.main.include_insurance_section.view.*
import kotlinx.android.synthetic.main.include_membership.*
import kotlinx.android.synthetic.main.include_overview_section.*
import kotlinx.android.synthetic.main.include_profile_top_data.*
import kotlinx.android.synthetic.main.include_provider_services.*
import kotlinx.android.synthetic.main.include_publication.*
import kotlinx.android.synthetic.main.include_reviews_section.*
import kotlinx.android.synthetic.main.include_reviews_section.view.*
import kotlinx.android.synthetic.main.sheet_profile_actions.view.*
import kotlinx.android.synthetic.main.toolbar.*
import android.support.annotation.NonNull
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.CENTER
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.HOSPITAL
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.POL_CLINIC
import com.bokdoc.pateintsbook.utils.FeesSpannable
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.dynamiclinks.PendingDynamicLinkData
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks


class HealthProvidersProfileActivity : ParentActivity() {

    private lateinit var providersViewModel: HealthProvidersViewModel
    private var mBottomSheetDialog: Dialog? = null
    var likesNumber: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_providers_profile)
        enableBackButton()
        showFilter()
        filterIcon.setImageResource(R.drawable.ic_dots)
        getViewModel()
        getDataFromIntentAndSetupList()
        getOverViewData()
        setListeners()
        setupProfileActionsObservers()
        appointments_text.text = getString(R.string.clinics_appointments)
    }

    private fun setListeners() {
        card_view_appointments.setOnClickListener(this)
        filterIcon.setOnClickListener(this)
        card_view_surgeries.setOnClickListener(this)
        card_view_online_consulting.setOnClickListener(this)
        tv_clinics_value.setOnClickListener(this)
        tv_departments_value.setOnClickListener(this)
        iv_profile_location.setOnClickListener(this)
        tv_certifications_viewAll.setOnClickListener(this)
        tv_education_viewAll.setOnClickListener(this)
        tv_membership_viewAll.setOnClickListener(this)
        tv_publication_viewAll.setOnClickListener(this)
        tv_accredation_viewall.setOnClickListener(this)
        tv_reviews_view_all.setOnClickListener(this)
        btn_consult_now.setOnClickListener(this)
        tv_insurance_viewAll.setOnClickListener(this)
        include_insurance_doctor.tv_insurance_viewAll.setOnClickListener(this)
    }

    private fun setupProfileActionsObservers() {
        providersViewModel.likedToggleLiveDate.observe(this, Observer {
            mBottomSheetDialog!!.dismiss()
            when (it) {
                R.string.successliked -> {
                    tv_likes_num.text = getString(R.string.likesPlaceHolder, (likesNumber.toInt() + 1).toString())
                    likesNumber = (likesNumber.toInt() + 1).toString()
                }
                R.string.successDisliked -> {
                    tv_likes_num.text = getString(R.string.likesPlaceHolder, (likesNumber.toInt() - 1).toString())
                    likesNumber = (likesNumber.toInt() - 1).toString()

                }
            }
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        providersViewModel.favouritesToggleLiveDate.observe(this, Observer {
            mBottomSheetDialog!!.dismiss()

            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        providersViewModel.comparedToggleLiveData.observe(this, Observer {
            mBottomSheetDialog!!.dismiss()

            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        providersViewModel.errorMessageLiveDate.observe(this, Observer {
            mBottomSheetDialog!!.dismiss()

            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

    }

    private fun getOverViewData() {
        lay_shimmer.startShimmerAnimation()
        providersViewModel.getProfileOverView(providersViewModel.profile.id, getString(R.string.profile_includes))
                .observe(this, Observer {
                    when (it!!.status) {
                        SUCCESS -> {
                            providersViewModel.overviewRow = it.data!![0]
                            when (providersViewModel.profile.type) {
                                DOCTOR -> {
                                    showDoctorSections()
                                    bindDoctorData(it.data!![0])
                                    if (!providersViewModel.isProfileLoaded) {
                                        bindProfile(it.data!![0], providersViewModel.profile.type)
                                    }
                                }
                            }
                            if (Profile.isHospitalCategory(providersViewModel.profile.type)) {
                                showHospitalSections()
                                bindHospitalData(it.data!![0])
                                if (!providersViewModel.isProfileLoaded) {
                                    bindProfile(it.data!![0], providersViewModel.profile.type)
                                }
                            }
                        }
                        FAIL -> {
                            showHospitalSections()
                            Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                        }
                        INTERNAL_SERVER_ERROR -> {
                            showHospitalSections()
                            Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                        }
                    }

                })

    }

    private fun bindDoctorData(overviewRow: OverviewRow) {
        when (overviewRow.consultingFees.amount.isNotEmpty() && overviewRow.consultingFees.amount!="0") {
            true -> {
                var oldAmount = ""
                if(overviewRow.consultingFees.oldAmount!=null && overviewRow.consultingFees.oldAmount!="0"){
                    oldAmount = overviewRow.consultingFees.oldAmount!! + " " +
                            overviewRow.consultingFees.currency_symbols
                }
                btn_consult_now.text = FeesSpannable.getSpannable(getString(R.string.three_place_holder,
                        getString(R.string.consult_now),overviewRow.consultingFees.amount
                        + " " + overviewRow.consultingFees.currency_symbols,oldAmount),
                        overviewRow.consultingFees.amount + " " +
                                overviewRow.consultingFees.currency_symbols,
                        ContextCompat.getColor(this, R.color.colorYellow),oldAmount)

                btn_consult_now.visibility = VISIBLE

                //getString(R.string.consult_now) + " / "+ overviewRow.consultingFees.amount + " "+ overviewRow.consultingFees.currency_symbols
            }
        }

        when (overviewRow.certification.isNotEmpty()) {
            true -> {
                tv_certificates_title_data.text = overviewRow.certification[0].title
                tv_certificates_year_data.text = overviewRow.certification[0].years
                tv_certificates_desc_data.text = overviewRow.certification[0].content
                tv_certificates_university.text = overviewRow.certification[0].university_id
                tv_certifications_viewAll.setOnClickListener(this)
            }
            false -> {
                include_certification.visibility = View.GONE
            }
        }
        if (overviewRow.certification.isNotEmpty() && overviewRow.certification[0].content.isEmpty()) {
            group_certification_desc.visibility = View.GONE

        }

        when (overviewRow.education.isNotEmpty()) {
            true -> {
                tv_education_university.text = overviewRow.education[0].university_id
                tv_education_year_data.text = overviewRow.education[0].years
                tv_education_description.text = overviewRow.education[0].content
                tv_education_viewAll.setOnClickListener(this)
            }
            false -> {
                include_education.visibility = View.GONE
            }
        }

        when (overviewRow.publication.isNotEmpty()) {
            true -> {
                tv_puplications_title.text = overviewRow.publication[0].title
                tv_publication_viewAll.setOnClickListener(this)
            }
            false -> {
                include_publication.visibility = View.GONE
            }
        }

        when (overviewRow.membership.isNotEmpty()) {
            true -> {
                tv_membership_title.text = overviewRow.membership[0].title
                tv_publication_viewAll.setOnClickListener(this)
            }
            false -> {
                include_membership.visibility = View.GONE
            }
        }


        when (overviewRow.insuranceCarriersRow.isNotEmpty()) {
            true -> {
                if (overviewRow.insuranceCarriersRow.size > 2) {
                    include_insurance_doctor.tv_first_insurance.text = overviewRow.insuranceCarriersRow[0].name
                    include_insurance_doctor.tv_second_insurance.text = overviewRow.insuranceCarriersRow[1].name
                } else {
                    include_insurance_doctor.tv_first_insurance.text = overviewRow.insuranceCarriersRow[0].name
                    include_insurance_doctor.tv_second_insurance.visibility = View.GONE

                }
            }
            false -> {
                include_insurance_doctor.visibility = View.GONE
            }
        }


        //reviews
        when (overviewRow.reviews.isNotEmpty()) {
            true -> {
                include_reviews_doctor.rv_reviews.adapter = FeedbackAdapter(overviewRow.reviews)
                include_reviews_doctor.tv_reviews_view_all.setOnClickListener(this)
                include_reviews_doctor.tv_reviews_count.text = getString(R.string.feedbackPlaceHolder, overviewRow.reviewsNumbers)
            }
            false -> {
                include_reviews_doctor.group_count.visibility = View.GONE
                include_reviews_doctor.rv_reviews.visibility = View.GONE
                include_reviews_doctor.tv_no_reviews.visibility = View.VISIBLE
            }
        }
    }

    private fun bindHospitalData(overviewRow: OverviewRow) {
        //overview
        if (providersViewModel.hasOverview(overviewRow)) {
            include_overview.visibility = View.VISIBLE
        }

        if (providersViewModel.getSpokenLanguage(overviewRow.spokenLanguage).isEmpty()) {
            group_lang.visibility = View.GONE
        } else {
            tv_spoken_lang.text = android.text.TextUtils.join(",", overviewRow.spokenLanguage)
        }

        if (providersViewModel.getPaymentMethods(overviewRow.paymentOptions).isEmpty()) {
            group_payment.visibility = View.GONE
        } else {
            tv_payment_methods.text = android.text.TextUtils.join(",", overviewRow.paymentOptions)
        }

        if (overviewRow.bedsNumber.isEmpty() || overviewRow.bedsNumber.equals("0")) {
            group_beds_num.visibility = View.GONE
        } else {
            tv_beds_numbers.text = overviewRow.bedsNumber
        }

        if (overviewRow.ambulanceCount.isEmpty() || overviewRow.ambulanceCount.equals("0")) {
            group_ambulance_count.visibility = View.GONE
        } else {
            tv_ambulance_count.text = overviewRow.ambulanceCount
        }


        //accommodations
        if (providersViewModel.hasAccomedations(overviewRow)) {
            include_accommodation.visibility = View.VISIBLE
            tv_patient_rooms.text = overviewRow.accommodations.patientRoom
            tv_hotel_title.text = overviewRow.accommodations.rooms
            tv_accompanying_person.text = overviewRow.accommodations.accompaniedPersons
            tv_meals_data.text = overviewRow.accommodations.meals
        } else {
            include_accommodation.visibility = View.GONE

        }


        //accreditation
        when (overviewRow.accreditation.isNotEmpty()) {
            true -> {
                Glide.with(this).load(overviewRow.accreditation[0].icon).into(iv_accreditation)
                tv_accredation_title.text = overviewRow.accreditation[0].title
                tv_accredation_description.text = overviewRow.accreditation[0].description
                tv_accredation_viewall.setOnClickListener(this)
            }
            false -> {
                include_accreditation.visibility = View.GONE
            }
        }

        when (overviewRow.insuranceCarriersRow.isNotEmpty()) {
            true -> {
                if (overviewRow.insuranceCarriersRow.size > 2) {

                    tv_first_insurance.text = overviewRow.insuranceCarriersRow[0].name
                    tv_second_insurance.text = overviewRow.insuranceCarriersRow[1].name
                } else {
                    tv_first_insurance.text = overviewRow.insuranceCarriersRow[0].name
                    tv_second_insurance.visibility = View.GONE
                }
            }
            false -> {
                include_insurance_hospital.visibility = View.GONE
            }
        }

        //reviews
        when (overviewRow.reviews.isNotEmpty()) {
            true -> {
                rv_reviews.adapter = FeedbackAdapter(overviewRow.reviews)
                tv_reviews_view_all.setOnClickListener(this)
                tv_reviews_count.text = getString(R.string.feedbackPlaceHolder, overviewRow.reviewsNumbers)
            }
            false -> {
                group_count.visibility = View.GONE
                rv_reviews.visibility = View.GONE
                tv_no_reviews.visibility = View.VISIBLE
            }
        }
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            card_view_appointments.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.typeKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, providersViewModel.profile.type.toString(), providersViewModel.profile.name, providersViewModel.profile.picture),
                        ClinicsActivity::class.java, BOOK_REQUEST)
            }
            card_view_surgeries.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.typeKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, providersViewModel.profile.type.toString(), providersViewModel.profile.name, providersViewModel.profile.picture),
                        ProfileSurgeriesActivity::class.java, BOOK_REQUEST)
            }
            card_view_online_consulting.id -> {
                when (providersViewModel.profile.type) {
                    DOCTOR -> {
                        if (providersViewModel.overviewRow.consultingBookButton != null) {
                            Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                                    arrayOf(providersViewModel.overviewRow.consultingBookButton!!.endpointUrl),
                                    BookAppointmentsActivity::class.java, BOOK_REQUEST)
                        }
                    }
                }

                if (Profile.isHospitalCategory(providersViewModel.profile.type)) {
                    Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.typeKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                            arrayOf(providersViewModel.profile.id, providersViewModel.profile.type.toString(),
                                    providersViewModel.profile.name, providersViewModel.profile.picture),
                            ProfileConsultingActivity::class.java, BOOK_REQUEST)
                }

//                Navigator.navigate(this, arrayOf(getString(R.string.idKey)),
//                        arrayOf(providersViewModel.profile.id), ProfileConsultingActivity::class.java)

            }
            tv_clinics_value.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, getString(R.string.doctors_include), providersViewModel.profile.name, providersViewModel.profile.picture), DoctorsActivity::class.java)

            }
            tv_departments_value.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, getString(R.string.doctors_include), providersViewModel.profile.name, providersViewModel.profile.picture), DepartmentsActivity::class.java)

            }
            iv_profile_location.id -> {
                when (providersViewModel.profile.type) {
                    DOCTOR -> {
                        Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(providersViewModel.profile.id), MapShowActivity::class.java)
                    }
                }

                if (Profile.isHospitalCategory(providersViewModel.profile.type)) {
                    Navigator.navigate(this, getString(R.string.locatioKey), providersViewModel.profile.locationData, MapShowActivity::class.java)
                }

            }
            tv_certifications_viewAll.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.typeKey), getString(R.string.idKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(getString(R.string.certificates), providersViewModel.profile.type.toString(), providersViewModel.profile.id, providersViewModel.profile.name, providersViewModel.profile.picture), QualificationsDetailsActivity::class.java)
            }
            tv_education_viewAll.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.typeKey), getString(R.string.idKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(getString(R.string.education), providersViewModel.profile.type.toString(), providersViewModel.profile.id, providersViewModel.profile.name, providersViewModel.profile.picture), QualificationsDetailsActivity::class.java)
            }
            tv_membership_viewAll.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.typeKey), getString(R.string.idKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(getString(R.string.memberShips), providersViewModel.profile.type.toString(), providersViewModel.profile.id, providersViewModel.profile.name, providersViewModel.profile.picture), QualificationsDetailsActivity::class.java)
            }
            tv_publication_viewAll.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.typeKey), getString(R.string.idKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(getString(R.string.publications), providersViewModel.profile.type.toString(), providersViewModel.profile.id, providersViewModel.profile.name, providersViewModel.profile.picture), QualificationsDetailsActivity::class.java)
            }
            tv_accredation_viewall.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.typeKey), getString(R.string.idKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(getString(R.string.accreditation), providersViewModel.profile.type.toString(),
                                providersViewModel.profile.id, providersViewModel.profile.name, providersViewModel.profile.picture), QualificationsDetailsActivity::class.java)
            }
            filterIcon.id -> {
                showSheet()
            }
            tv_reviews_view_all.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.typeKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, providersViewModel.profile.type.toString(), providersViewModel.profile.name, providersViewModel.profile.picture), AllProvidersReviewsActivity::class.java)

            }
            tv_insurance_viewAll.id -> {
                Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.typeKey), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                        arrayOf(providersViewModel.profile.id, providersViewModel.profile.type.toString(), providersViewModel.profile.name, providersViewModel.profile.picture), InsuranceCarriersActivity::class.java)

            }
            btn_consult_now.id -> {
                if (providersViewModel.overviewRow.consultingBookButton != null) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                            arrayOf(providersViewModel.overviewRow.consultingBookButton!!.endpointUrl),
                            BookAppointmentsActivity::class.java, BOOK_REQUEST)
                }
            }
        }
    }

    private fun getDataFromIntentAndSetupList() {
        if (intent?.hasExtra(getString(R.string.profileKey))!!) {
            intent?.getParcelableArrayListExtra<HealthProvidersProfile>(getString(R.string.profileKey)).let {
                providersViewModel.profile = it!![0]
                if (providersViewModel.profile.picture.isNotEmpty()) {
                    bindProfile(providersViewModel.profile)
                } else {
                    providersViewModel.isProfileLoaded = false
                }
            }
        } else {
            //bindProfile(providersViewModel.getProfileFromUri(intent?.data!!))
        }
    }

    private fun getViewModel() {
        providersViewModel = ViewModelProviders.of(this, HealthProvidersModelProvider(ProvidersProfileRepository(this.application,
                SharedPreferencesManagerImpl(this)),
                ProfileSocialActionsRepository(applicationContext, SharedPreferencesManagerImpl(this), CompareSharedPreferences(this),
                        ProfileSocailActionsWebservice(WebserviceManagerImpl.instance())),
                OverViewRepository(this, OverViewWebService(WebserviceManagerImpl.instance()), UserSharedPreference(this)))).get(HealthProvidersViewModel::class.java)
    }

    private fun bindProfile(profile: HealthProvidersProfile) {
        Glide.with(this).load(profile.picture).into(profileImage)
        profileName.text = profile.name
        if(profile.rateing!=0f) {
            rating.visibility = VISIBLE
            rating.rating = profile.rateing
        }
        if(profile.reviewsCount!="0") {
            reviewsNumbers.visibility = VISIBLE
            reviewsNumbers.text = getString(R.string.reviewsPlaceHolder, profile.reviewsCount)
        }

        when (profile.type) {
            DOCTOR -> {
                title = getString(R.string.doctor_profile_placeholder, profile.name)
                profileDesc.text = profile.speciality
                iv_establish_date.setImageResource(R.drawable.ic_birth_date)
                tv_likes_num.text = getString(R.string.likesPlaceHolder, profile.likesNum)
                likesNumber = profile.likesNum
            }
            HOSPITAL -> {
                title = getString(R.string.hospital_profile_placeholder, profile.name)
            }
            CENTER -> {
                title = getString(R.string.placeHolder, getString(R.string.center), profile.name)
            }
            POL_CLINIC -> {
                title = getString(R.string.placeHolder, getString(R.string.polyclinics), profile.name)
            }

        }

        if (Profile.isHospitalCategory(profile.type)) {
            group_doctor_data.visibility = View.GONE
            group_clinics.visibility = View.VISIBLE
            group_date_hospital.visibility = View.VISIBLE
            tv_likes_num_hos.text = getString(R.string.likesPlaceHolder, profile.likesNum)
            profileDesc.text = profile.location
            tv_clinics_value.text = ColoredTextSpannable.getSpannable(getString(R.string.two_texts_placeholder, getString(R.string.doctorsDots), profile.docsNum), profile.docsNum, ContextCompat.getColor(this, R.color.colorPrimary))
            tv_departments_value.text = ColoredTextSpannable.getSpannable(getString(R.string.two_texts_placeholder,
                    getString(R.string.departmentsDots), profile.departmentsNum), profile.departmentsNum, ContextCompat.getColor(this, R.color.colorPrimary))
            tv_establish_date.text = getString(R.string.establishedDatePlaceHolder, profile.date)
            likesNumber = profile.likesNum
        }


    }

    private fun bindProfile(profile: OverviewRow, profileType: Int) {
        title = profile.name
        Glide.with(this).load(profile.picture).into(profileImage)
        profileName.text = profile.name
        rating.rating = profile.rating
        reviewsNumbers.text = getString(R.string.reviewsPlaceHolder, profile.reviewsNumbers)
        likesNumber = profile.likesNumbers


        when (profileType) {
            DOCTOR -> {
                tv_likes_num.text = getString(R.string.likesPlaceHolder, profile.likesNumbers)
                profileDesc.text = profile.speciality
                iv_establish_date.setImageResource(R.drawable.ic_birth_date)
            }
        }

        if (Profile.isHospitalCategory(profileType)) {
            group_doctor_data.visibility = View.GONE
            group_clinics.visibility = View.VISIBLE
            group_date_hospital.visibility = View.VISIBLE
            profileDesc.text = profile.location?.address
            tv_likes_num_hos.text = getString(R.string.likesPlaceHolder, profile.likesNumbers)
            tv_clinics_value.text = ColoredTextSpannable.getSpannable(getString(R.string.two_texts_placeholder, getString(R.string.doctorsDots),
                    profile.doctorsNumbers), profile.doctorsNumbers, ContextCompat.getColor(this, R.color.colorPrimary))
            tv_departments_value.text = ColoredTextSpannable.getSpannable(getString(R.string.two_texts_placeholder,
                    getString(R.string.departmentsDots), profile.departmentsNumbers), profile.departmentsNumbers, ContextCompat.getColor(this, R.color.colorPrimary))
            tv_establish_date.text = getString(R.string.establishedDatePlaceHolder, profile.establishBirthDate)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        providersViewModel.saveSelectedCompare()
    }

    private fun showHospitalSections() {
        lay_shimmer.visibility = View.GONE
        include_reviews_hospital.visibility = View.VISIBLE
        include_accreditation.visibility = View.VISIBLE
        include_accommodation.visibility = View.VISIBLE
        include_insurance_hospital.visibility = View.VISIBLE
    }

    private fun showDoctorSections() {
        lay_shimmer.visibility = View.GONE
        include_certification.visibility = View.VISIBLE
        include_education.visibility = View.VISIBLE
        include_publication.visibility = View.VISIBLE
        include_membership.visibility = View.VISIBLE
        include_reviews_doctor.visibility = View.VISIBLE
        include_insurance_doctor.visibility = View.VISIBLE
    }

    fun showSheet() {
        val view = layoutInflater.inflate(R.layout.sheet_profile_actions, null)
        view.rv_list.layoutManager = LinearLayoutManager(this)
        view.rv_list.adapter = ProfileActionsSheetAdapter(providersViewModel.getProfileActionsMenu(this, providersViewModel.profile), this::onMenuItemSelected)

        mBottomSheetDialog = Dialog(this, R.style.MaterialDialogSheet)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()

    }

    private fun onMenuItemSelected(imageTextRow: ImageTextRow) {
        when (imageTextRow.text) {
            getString(R.string.share) -> Navigator.share(this, "https://bokdocpatient.page.link/?link=https%3A//bokdocpatient.page.link/share_profile%3Fscreen_type=share_profile%26profile_id=" + providersViewModel.profile.id + "%26profile_type=" + providersViewModel.profile.type)
            getString(R.string.like) -> providersViewModel.toggleLike()
            getString(R.string.favorite) -> providersViewModel.toggleFavorite()
            getString(R.string.compare) -> providersViewModel.toggleCompare()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }
}
