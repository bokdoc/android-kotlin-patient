package com.bokdoc.pateintsbook.ui.bookAppointmentsReport

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.PopupMenu
import android.view.MenuItem
import android.view.View
import android.webkit.PermissionRequest
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.R.id.book_appointments_report_content
import com.bokdoc.pateintsbook.navigators.ShareNavigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ui.PermissionsAlertDialog
import com.bokdoc.pateintsbook.utils.ui.ViewScreenShotHelper
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.wdullaer.materialdatetimepicker.Utils
import kotlinx.android.synthetic.main.activity_book_appointments_report.*
import java.lang.ref.WeakReference

class BookAppointmentsReportActivity : ParentActivity(), PopupMenu.OnMenuItemClickListener {

    private lateinit var reportViewModel: BookAppointmentsReportViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointments_report)
        enableBackButton()
        setTitle(R.string.appointmentsReport)
        setupMenu()
        reportViewModel = ViewModelProviders.of(this).get(BookAppointmentsReportViewModel::class.java)
        reportViewModel.bookInfo = intent?.getParcelableExtra(getString(R.string.bookInfoKey))!!
        bind()
    }

    private fun setupMenu() {
        moreMenuRes = R.menu.appointments_book_report_menu
        moreIconsClickListener = this
    }

    private fun bind() {
        Glide.with(this).load(reportViewModel.bookInfo.picture).into(profileImage)
        profileTypeText.text = reportViewModel.bookInfo.profileTypeName
        profileName.text = reportViewModel.bookInfo.name
        profileSpeciality.text = reportViewModel.bookInfo.specialty
        locationValue.text = reportViewModel.bookInfo.location?.address
        time.text = reportViewModel.bookInfo.reservationTime
        bindPatientInfo()
    }

    private fun bindPatientInfo() {
        if (reportViewModel.bookInfo.patientImage.isNotEmpty()) {
            Glide.with(this).load(reportViewModel.bookInfo.patientImage).into(patientImage)
        } else {
            Glide.with(this).load(R.drawable.ic_default_male).into(patientImage)
        }
        nameValue.text = reportViewModel.bookInfo.patientName
        emailValue.text = reportViewModel.bookInfo.patientEmail
        mobileValue.text = reportViewModel.bookInfo.patientMobile
        ageValue.text = reportViewModel.bookInfo.patientAge
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.print, R.id.share -> {
                val snackPermissionDenied = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(book_appointments_report_parent,
                        R.string.storage_permissions_denied_feedback)
                        .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                        .build()

                val permissionListeners = object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            ScreenShotAsyncTask() {
                                if (item.itemId == R.id.share) {
                                    ShareNavigator.shareImage(this@BookAppointmentsReportActivity,
                                            it)
                                } else {
                                    com.bokdoc.pateintsbook.utils.Utils.showLongToast(this@BookAppointmentsReportActivity,
                                            R.string.messageScreenShotSuccess)
                                }
                            }.execute(book_appointments_report_content)
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        PermissionsAlertDialog.show(this@BookAppointmentsReportActivity,
                                token!!, R.string.storage_permission_title, R.string.storage_rationale_message)
                    }
                }

                val compositeListener = CompositeMultiplePermissionsListener(permissionListeners, snackPermissionDenied)

                Dexter.withActivity(this)
                        .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(compositeListener).check()
            }
            R.id.edit -> {
            }
        }
        return true
    }

    class ScreenShotAsyncTask(private val onFinishedListener: (Uri) -> Unit)
        : AsyncTask<View, Void, Uri>() {
        override fun doInBackground(vararg view: View?): Uri {
            return ViewScreenShotHelper.takeScreenShot(view[0]!!)
        }

        override fun onPostExecute(result: Uri) {
            onFinishedListener.invoke(result)
        }

    }


}
