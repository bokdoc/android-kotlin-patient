package com.bokdoc.pateintsbook.ui.attachment

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.net.Uri
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.repositories.attachmentMedia.AttachmentsMediaRepository
import com.bokdoc.pateintsbook.utils.ImagesUtils
import com.bokdoc.pateintsbook.utils.background.AppExecutors
import com.bokdoc.pateintsbook.utils.files.FilesUtils
import com.esafirm.imagepicker.model.Image

class AttachmentMediaViewModel(private val attachmentsMediaRepository: AttachmentsMediaRepository,
                               private val application: Application,
                               private val appExecutors: AppExecutors) : ViewModel() {
    val errorMessageLiveData = MutableLiveData<Int>()
    val dataLiveData = MutableLiveData<List<IMediaParcelable>>()
    val uploadProgressLiveData = MutableLiveData<Double>()
    val uriImagesLiveData = MutableLiveData<ArrayList<Uri>>()
    val uriFilesLiveData = MutableLiveData<ArrayList<Uri>>()
    var mediaUrl: String = "l"
    var urisImages = ArrayList<Uri>()
    var urisFiles = ArrayList<Uri>()
    var messageLiveData = MutableLiveData<String>()
    //already uploaded
    var urisImagesUploaded = ArrayList<Uri>()
    var urisFilesUploaded = ArrayList<Uri>()
    var isShowOnly = true
    var images: List<Image> = ArrayList()
    var passedData: List<MediaParcelable> = ArrayList()

    //load and show only media
    fun showMedia(): LiveData<DataResource<IMediaParcelable>> {
        return attachmentsMediaRepository.showMedia(mediaUrl)
    }

    //load already uploaded
    fun loadFromMedia(media: List<MediaParcelable>) {
        passedData = media
        media.forEach {
            if (it.mediaType == IMedia.IMAGE_TYPE) {
                urisImagesUploaded.add(Uri.parse(it.url))
            } else {
                urisFilesUploaded.add(Uri.parse(it.url))
            }
        }
    }

    fun getMediaUploaded(): List<IMediaParcelable> {
        return passedData
    }

    fun getAllImagesFiles(): ArrayList<Uri> {
        val uris = ArrayList<Uri>()
        if (urisImages.isNotEmpty())
            uris.addAll(urisImages)

        if (urisImagesUploaded.isNotEmpty())
            uris.addAll(urisImagesUploaded)

        return uris
    }


    fun addImagesPaths(images: List<Image>, type: String) {
        this.images = images
        val paths = ArrayList<String>()
        images.forEach {
            paths.add(it.path)
        }
        addPaths(paths, type)
    }

    fun getAllDocFiles(): ArrayList<Uri> {
        val uris = ArrayList<Uri>()

        if (urisFiles.isNotEmpty())
            uris.addAll(urisFiles)

        if (urisFilesUploaded.isNotEmpty())
            uris.addAll(urisFilesUploaded)

        return uris
    }

    fun remove(uri: Uri, type: String) {
        if (type == IMAGE_URI) {
            if (urisImages.contains(uri)) {
                urisImages.remove(uri)
            } else {
                urisImagesUploaded.remove(uri)
            }
        } else {
            if (urisFiles.contains(uri)) {
                urisFiles.remove(uri)
            } else {
                urisFilesUploaded.remove(uri)
            }
        }

        val count = passedData.size
        (0 until count).forEach {
            if (it >= passedData.size)
                return@forEach
            if (passedData[it].url == uri.toString()) {
                (passedData as MutableList).removeAt(it)
                return@forEach
            }
        }
    }

    /*
    fun uploadFiles() {
        getNotUploadedFiles().let {
            if (it.isNotEmpty()) {
                attachmentsMediaRepository.uploadFiles(it
                        , this::onProgressUpload).observeForever {
                    if (it?.data != null) {
                        dataLiveData.value = (it.data as MutableList).apply {
                            addAll(getMediaUploaded())
                        }

                        if(it.status == DataResource.SUCCESS){
                                messageLiveData.value = it.message
                        }else{
                                errorMessageLiveData.value = R.string.errorSent
                         }
                        //errorMessageLiveData.value = R.string.errorSomeThingWrong
                    }
                }
            }
        }
    }
    */
    fun uploadFiles() {
        getNotUploadedFiles().let {
            if (it.isNotEmpty()) {
                appExecutors.diskIO().execute {
                    val paths = ImagesUtils.getCompressed(application, FilesUtils.getPaths(it))
                    attachmentsMediaRepository.uploadFiles(paths
                            , this::onProgressUpload).observeForever {
                        if (it?.data != null) {
                            dataLiveData.value = (it.data as MutableList).apply {
                                addAll(getMediaUploaded())
                            }
                        } else {
                            errorMessageLiveData.postValue(R.string.errorSomeThingWrong)
                        }
                    }
                }

            }
        }
    }

    /*
    * get not uploaded files
    * */
    private fun getNotUploadedFiles(): ArrayList<Uri> {
        val uris = ArrayList<Uri>()
        if (urisImages.isNotEmpty())
            uris.addAll(urisImages)

        if (urisFiles.isNotEmpty())
            uris.addAll(urisFiles)

        return uris
    }

    fun isUrisFound(): Boolean {
        return urisImages.isNotEmpty() || urisFiles.isNotEmpty()
    }


    fun addPaths(paths: ArrayList<String>, type: String) {
        if (paths.isEmpty()) {
            if (type == IMAGE_URI && urisImages.size == 1) {
                urisImages = ArrayList()
                uriImagesLiveData.value = getAllImagesFiles()
            } else
                if (type == FILES_URI && urisFiles.size == 1) {
                    urisFiles = ArrayList()
                    uriFilesLiveData.value = getAllDocFiles()
                }
            return
        }
        val uris = attachmentsMediaRepository.getUris(paths)
        type.let {
            if (it == IMAGE_URI) {
                if (attachmentsMediaRepository.isFilesSupport(paths, attachmentsMediaRepository.supportedMedia)) {
                    urisImages = uris.toCollection(ArrayList())
                    uriImagesLiveData.value = getAllImagesFiles()
                } else {
                    errorMessageLiveData.value = R.string.errorNoSupportMedia
                }
            } else {
                if (attachmentsMediaRepository.isFilesSupport(paths, attachmentsMediaRepository.supportedDocuments)) {
                    urisFiles = uris.toCollection(ArrayList())
                    uriFilesLiveData.value = getAllDocFiles()
                } else {
                    errorMessageLiveData.value = R.string.errorNoSupportFiles
                }
            }
        }
    }

    fun uploadFile(path: String) {
        attachmentsMediaRepository.uploadFiles(listOf(getUris(arrayListOf(path))[0])
                , this::onProgressUpload).observeForever {
            if (it?.data != null) {
                dataLiveData.value = ArrayList(it.data).apply {
                    addAll(getMediaUploaded())
                }
            } else {
                errorMessageLiveData.value = R.string.errorSomeThingWrong
            }
        }
    }


    fun getPaths(uris: Array<Uri>): ArrayList<String> {
        return attachmentsMediaRepository.getPaths(uris)
    }

    fun getUris(paths: ArrayList<String>): Array<Uri> {
        return attachmentsMediaRepository.getUris(paths)
    }

    fun getSupportedFilesTypes(): Array<String> {
        return attachmentsMediaRepository.supportedDocuments
    }

    private fun onProgressUpload(progress: Double) {
        uploadProgressLiveData.postValue(progress)
    }

    fun cancelUpload() {
        attachmentsMediaRepository.cancelUpload()
    }


    fun getUserMedia(): LiveData<DataResource<IMediaParcelable>> {
        return attachmentsMediaRepository.getUserMedia()
    }


    companion object {
        const val IMAGE_URI = "image"
        const val FILES_URI = "file"
    }
}