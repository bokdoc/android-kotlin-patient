package com.bokdoc.pateintsbook.ui.autoCompleteResults

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.AutoCompleteResource
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.ResourceWithNameAttr
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import kotlinx.android.synthetic.main.activity_auto_complete_results.*

class AutoCompleteResultsActivity : ParentActivity() {
    private lateinit var viewModel: AutoCompletesViewModel
    private var autoCompletesAdapter: AutoCompletesResultsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_complete_results)
        //returnArrow.visibility = VISIBLE
        iv_return.setOnClickListener {
            finish()
        }

        autoCompleteList.layoutManager = LinearLayoutManager(this)

        clearIcon.setOnClickListener {
            search.setText("")
        }

        if (intent?.hasExtra(getString(R.string.hint_key))!!) {
            search.hint = intent?.getStringExtra(getString(R.string.hint_key))
        }

        progress.indeterminateDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent),
                android.graphics.PorterDuff.Mode.MULTIPLY)

        viewModel = ViewModelProviders.of(this, CustomAutoCompletesModel(this)).get(AutoCompletesViewModel::class.java)
        viewModel.setScreenType(intent?.getStringExtra(getString(R.string.screenType))!!)
        viewModel.setCompleteType(intent?.getIntExtra(getString(R.string.completeType), -1)!!)


        getCompleteData()

        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {

                // ------------ Local filter ------------- //
                autoCompletesAdapter?.filter(text.toString())

                //----------- server filter will be active --------///

//                if (text!!.isNotEmpty()) {
//                    clearIcon.visibility = VISIBLE
//                    autoCompleteList.visibility = GONE
//                    if (text.length > 2) {
//                        progress.visibility = VISIBLE
//                        viewModel.complete(text.toString()).observe(this@AutoCompleteResultsActivity,
//                                Observer<List<AutoComplete>> { autoComplete ->
//                                    progress.visibility = GONE
//                                    autoCompleteList.visibility = VISIBLE
//                                    when (autoCompletesAdapter) {
//                                        null -> addAdapterToList(autoComplete!!)
//                                        else -> {
//                                            autoCompletesAdapter?.updateList(getSectionsHeaders(autoComplete!!))
//                                        }
//                                    }
//                                })
//                    }
//                } else {
//                    clearIcon.visibility = GONE
//                    autoCompletesAdapter?.updateList(arrayListOf())
//                }

            }

        })

        search.setOnEditorActionListener { _, actionId,_ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                Utils.hideKeyboard(this)
            }
            true
        }
    }

    private fun getCompleteData() {
        progress.visibility = VISIBLE
        viewModel.complete("").observe(this@AutoCompleteResultsActivity,
                Observer<List<AutoComplete>> { autoComplete ->
                    progress.visibility = GONE
                    autoCompleteList.visibility = VISIBLE
                    when (autoCompletesAdapter) {
                        null -> addAdapterToList(autoComplete!!)
                        else -> {
                            autoCompletesAdapter?.updateList(getSectionsHeaders(autoComplete!!))
                        }
                    }
                })


    }

    private fun addAdapterToList(autoCompleteResource: List<AutoComplete>) {
        autoCompletesAdapter = AutoCompletesResultsAdapter(this@AutoCompleteResultsActivity,
                getSectionsHeaders(autoCompleteResource), this::onItemSelectedListener, this::onProfileSelectedListener)
        autoCompleteList.adapter = autoCompletesAdapter
    }

    private fun onItemSelectedListener(text: String, params: String) {
        HomeNavigator.naviagteBack(this, arrayOf(SELECTED_TEXT_KEY, SELECTED_PARAMS_KEY), arrayOf(text, params))
    }

    private fun onProfileSelectedListener(id: String, profileType: Int) {
        Navigator.navigate(this, getString(R.string.profileKey), arrayListOf(HealthProvidersProfileConverter.convert(id, profileType)),
                HealthProvidersProfileActivity::class.java)
    }

    private fun getSectionsHeaders(autoCompleteResource: List<AutoComplete>): ArrayList<AutoCompleteSectionHeader> {
        val sectionsHeaders = ArrayList<AutoCompleteSectionHeader>()
        val size = autoCompleteResource.size
        (0 until size).forEach {
            sectionsHeaders.add(AutoCompleteSectionHeader(autoCompleteResource[it].values, autoCompleteResource[it].tite))
        }
        return sectionsHeaders
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    companion object {
        const val SELECTED_TEXT_KEY = "selectedText"
        const val SELECTED_PARAMS_KEY = "selectedParams"
    }


}
