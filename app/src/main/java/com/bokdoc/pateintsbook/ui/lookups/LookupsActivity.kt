package com.bokdoc.pateintsbook.ui.lookups

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.autoCompleteResults.*
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.wdullaer.materialdatetimepicker.Utils
import kotlinx.android.synthetic.main.activity_lookups.*

class LookupsActivity : ParentActivity() {

    private lateinit var lookupsViewModel: LookupsViewModel
    private var lookupsViewAdapter: LookupsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lookups)
        //titleText.text = intent?.getStringExtra(getString(R.string.lookupsTitle))
        search.hint = intent?.getStringExtra(getString(R.string.lookupsTitle))
        iv_return.setOnClickListener {
            finish()
        }

        autoCompleteList.layoutManager = LinearLayoutManager(this)

        clearIcon.setOnClickListener {
            search.setText("")
        }

        progress.indeterminateDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        lookupsViewModel = InjectionUtil.getLookUpsViewModel(this)
        lookupsViewModel.query = intent?.getStringExtra(getString(R.string.lookupsQuery))!!

        lookupsViewModel.query.let {
            if(it.contains(getString(R.string.lookupDiseases)) || it.contains(getString(R.string.lookupMedications))){
                lookupsViewModel.isAddLookup = true
            }
            if(it.contains(getString(R.string.lookupDiseases))){
                tv_lookup_empty.text = getString(R.string.message_not_found_lookups_placeholder,getString(R.string.disease))
                et_add_lookup.hint = getString(R.string.type_your_lookup_placeholder,getString(R.string.disease))
            }
            if(it.contains(getString(R.string.lookupMedications))){
                tv_lookup_empty.text = getString(R.string.message_not_found_lookups_placeholder,getString(R.string.medication))
                et_add_lookup.hint = getString(R.string.type_your_lookup_placeholder,getString(R.string.medication))
            }
        }

        lookupsViewModel.lookups()
        lookupsViewModel.progress.observe(this, Observer {
            progress.visibility = it!!
            if (it == VISIBLE) {
                search.isEnabled = false
            }
        })

        if (intent?.hasExtra(getString(R.string.selected_lookups))!!) {
            lookupsViewModel.selectedLookupTypes =
                    intent?.getParcelableArrayListExtra(getString(R.string.selected_lookups))!!
        }

        lookupsViewModel.messageErrorLiveData.observe(this, Observer {
            com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, it!!)
        })

        lookupsViewModel.listLiveData.observe(this, Observer {
            search.isEnabled = true
            lookupsViewAdapter = LookupsAdapter(it as ArrayList<LookupsType>,lookupsViewModel.selectedLookupTypes as ArrayList<LookupsType>
                    ,this::onItemSelectedListener)
            autoCompleteList.adapter = lookupsViewAdapter
        })

        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                lookupsViewAdapter?.filter(text.toString())
                if(lookupsViewModel.isAddLookup && lookupsViewAdapter?.itemCount == 0){
                    add_lookup_group.visibility = VISIBLE
                    autoCompleteList.visibility = GONE
                }else{
                    autoCompleteList.visibility = VISIBLE
                    add_lookup_group.visibility = GONE
                }
                if (text!!.isNotEmpty()) {
                    clearIcon.visibility = View.VISIBLE
                } else {
                    clearIcon.visibility = View.GONE
                }
            }

        })


        search.setOnEditorActionListener { _, actionId,_ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                com.bokdoc.pateintsbook.utils.Utils.hideKeyboard(this)
            }
            true
        }

        btn_add_lookup.setOnClickListener {
            if(et_add_lookup.text.toString().isEmpty())
                return@setOnClickListener

            switchBgProgressVisibility(VISIBLE)
            if(lookupsViewModel.query.contains(getString(R.string.lookupDiseases))){
                lookupsViewModel.addLookup.disase = LookUpsParcelable().apply {
                    name = et_add_lookup.text.toString()
                    idLookup = ""
                }
            }else{
                lookupsViewModel.addLookup.medication = LookUpsParcelable().apply {
                    name = et_add_lookup.text.toString()
                    idLookup = ""
                }
            }
            lookupsViewModel.addLookup().observe(this, Observer {
                switchBgProgressVisibility(GONE)
                if(it!=null && it.status == DataResource.SUCCESS){
                    if(it.meta.message.isNotEmpty()){
                        com.bokdoc.pateintsbook.utils.Utils.showLongToast(this,it.meta.message)
                    }else{

                    }
                    if(it?.data!=null && it!!.data!!.isNotEmpty()){
                        val lookups = ArrayList<LookUpsParcelable>().apply {
                            add((LookUpsParcelable().apply {
                                idLookup = it.data!![0].idLookup
                                name = it.data!![0].name
                            }))
                        }

                     Navigator.naviagteBack(this,getString(R.string.selected_lookups),lookups as ArrayList<Parcelable>)
                    }
                }else{
                    if(it!!.errors.isNotEmpty())
                        com.bokdoc.pateintsbook.utils.Utils.showLongToast(this,it.errors[0].detail)
                }
            })

        }

        com.bokdoc.pateintsbook.utils.Utils.hideKeyboard(search)

        et_add_lookup.setOnEditorActionListener { _, actionId,_ ->
            if(actionId == EditorInfo.IME_ACTION_SEND){
                btn_add_lookup.performClick()
                com.bokdoc.pateintsbook.utils.Utils.hideKeyboard(this)
            }
            true
        }
    }

    private fun onItemSelectedListener(lookupsType: LookupsType) {
        if(!lookupsViewModel.isMulti) {
            val lookups = ArrayList<LookUpsParcelable>().apply {
                add((LookUpsParcelable().apply {
                    idLookup = lookupsType.idLookup
                    name = lookupsType.name
                }))
            }

            Navigator.naviagteBack(this,getString(R.string.selected_lookups),lookups as ArrayList<Parcelable>)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}
