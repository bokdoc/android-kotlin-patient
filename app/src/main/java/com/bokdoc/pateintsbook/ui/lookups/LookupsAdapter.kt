package com.bokdoc.pateintsbook.ui.lookups

import android.arch.lifecycle.LiveData
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.ILookupsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_lookups_row.view.*

class LookupsAdapter(private val lookups: ArrayList<LookupsType>,private var selectedLookups: ArrayList<LookupsType>,
                     private val selectListener: (LookupsType) -> Unit)
    : RecyclerView.Adapter<LookupsAdapter.LookupsViewHolder>() {

    private var filteredLookups: ArrayList<LookupsType> = lookups

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LookupsViewHolder {
        return LookupsViewHolder(parent.inflateView(R.layout.item_lookups_row))
    }

    override fun getItemCount(): Int {
        return filteredLookups.size
    }

    override fun onBindViewHolder(holder: LookupsViewHolder, position: Int) {
        holder.itemView.text?.text = filteredLookups[position].name

        if (search(selectedLookups as ArrayList<LookupsType>, filteredLookups[position].idLookup) != -1) {
            holder.itemView.iv_selected_mark.visibility = View.VISIBLE
        } else {
            holder.itemView.iv_selected_mark.visibility = View.GONE
        }

    }

    fun search(list: ArrayList<LookupsType>, id: String): Int {
        (0 until list.size).forEach {
            if (list[it].idLookup == id) {
                return it
            }
        }
        return -1
    }

    fun filter(text: String) {
        if (text.isEmpty()) {
            filteredLookups = lookups
        } else {
            filteredLookups = ArrayList()
            lookups.forEach {
                if (it.name.contains(text,true)) {
                    filteredLookups.add(it)
                }
            }
        }
        notifyDataSetChanged()
    }




    inner class LookupsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                selectListener.invoke(lookups[adapterPosition])
            }
        }
    }
}