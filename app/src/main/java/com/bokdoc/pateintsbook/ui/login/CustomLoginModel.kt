package com.bokdoc.pateintsbook.ui.login

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompletesViewModel

class CustomLoginModel(private val context: Context): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(context) as T
    }
}