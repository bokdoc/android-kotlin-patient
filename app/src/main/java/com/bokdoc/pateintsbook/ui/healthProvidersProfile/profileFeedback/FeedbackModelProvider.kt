package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileFeedback.ProfileFeedbackRepository

class FeedbackModelProvider(private val repository:ProfileFeedbackRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProfileFeedbackViewModel(repository) as T
    }
}