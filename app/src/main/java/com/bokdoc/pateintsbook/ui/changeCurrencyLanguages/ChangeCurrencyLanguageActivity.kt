package com.bokdoc.pateintsbook.ui.changeCurrencyLanguages

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.CurrencyLanguageRow
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.repositories.changeCurrencyLanguage.ChangeCurrencyLanguageRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages.ChangeCurrencyLanguageWebservice
import com.bokdoc.pateintsbook.navigators.LookupsNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.lookups.LookupsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.activity_search_results.*
import kotlinx.android.synthetic.main.include_list_with_save_btn.*
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.ref.WeakReference
import java.util.*

class ChangeCurrencyLanguageActivity : ParentActivity() {

    private lateinit var changeCurrencyLanguageViewModel: ChangeCurrencyLanguageViewModel
    private lateinit var addEditAdapter: AddEditAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_currency_language)
        title = getString(R.string.currency_language)
        btn_save.visibility = GONE
        enableBackButton()
        changeCurrencyLanguageViewModel = ViewModelsCustomProvider(ChangeCurrencyLanguageViewModel(WeakReference(this),
                ChangeCurrencyLanguageRepository(UserSharedPreference(baseContext),
                        ChangeCurrencyLanguageWebservice(WebserviceManagerImpl.instance())))).create(ChangeCurrencyLanguageViewModel::class.java)
        addEditAdapter = AddEditAdapter(changeCurrencyLanguageViewModel.getRows(), this::onRowClickListener)
        rv_all.adapter = addEditAdapter

        btn_save.setOnClickListener {
            switchBgProgressVisibility(VISIBLE)
            changeCurrencyLanguageViewModel.save().observe(this, Observer {
                if (it?.status == DataResource.SUCCESS && it.data != null) {
                    com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, it.meta.message)
                    changeCurrencyLanguageViewModel.changeCurrencyLanguageRepository.userSharedPreferences.sharedPrefrenceManger.saveData(getString(R.string.language_key),
                            it.data!![0].languageString)
                    changeCurrencyLanguageViewModel.languageKey = it.data!![0].languageString
                    changeCurrencyLanguageViewModel.currencyLookup = it.data!![0].currencyLookup
                    changeCurrencyLanguageViewModel.countryLookup = it.data!![0].countryLookup
                    changeCurrencyLanguageViewModel.languageLookup = it.data!![0].languageLookup
                    LocaleManager.setLocale(baseContext)
                    refreshLayout()
                  //  finish()
                  //  Navigator.navigateWithClearStack(this, MainActivity::class.java)
                } else {
                    it?.message?.let {message->
                        if(message.isNotEmpty()){
                            Utils.showLongToast(this,message)
                        }else{
                            Utils.showLongToast(this,R.string.errorNotSaved)
                        }
                    }
                    //return to last status before update
                    applyLastScopeStatus()
                }
                switchBgProgressVisibility(GONE)
            })
        }
    }


    private fun applyLastScopeStatus(){
        changeCurrencyLanguageViewModel.getUserScope()
        when {
            changeCurrencyLanguageViewModel.selectedPosition == 0 -> {
                addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                        changeCurrencyLanguageViewModel.currencyLookup!!.name)
            }
            changeCurrencyLanguageViewModel.selectedPosition==1 -> {
                addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                        changeCurrencyLanguageViewModel.countryLookup!!.name)
            }
            changeCurrencyLanguageViewModel.selectedPosition==2 -> {
                addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                        changeCurrencyLanguageViewModel.languageLookup!!.name)
            }
        }
    }

    private fun onRowClickListener(position: Int, slug: String) {
        changeCurrencyLanguageViewModel.selectedPosition = position
        when (slug) {
            CurrencyLanguageRow.CURRENCY_SLUG -> {
                LookupsNavigator.navigate(this, getString(R.string.currency), getString(R.string.currency_lookup_query),
                        arrayListOf(changeCurrencyLanguageViewModel.currencyLookup!!), CURRENCY_REQUEST)
            }
            CurrencyLanguageRow.COUNTRY_SLUG -> {
                LookupsNavigator.navigate(this, getString(R.string.country), getString(R.string.countries_lookup_query),
                        arrayListOf(changeCurrencyLanguageViewModel.countryLookup!!), COUNTRY_REQUEST)
            }
            CurrencyLanguageRow.LANGUAGE_SLUG -> {
                LookupsNavigator.navigate(this, getString(R.string.language), getString(R.string.languages_lookup_query),
                        arrayListOf(changeCurrencyLanguageViewModel.languageLookup!!), LANGUAGE_REQUEST)
            }
        }
    }

    private fun refreshLayout(){
           if(changeCurrencyLanguageViewModel.languageKey == "en"){
               window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
           }else if(changeCurrencyLanguageViewModel.languageKey =="ar"){
               window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
           }
           addEditAdapter = AddEditAdapter(changeCurrencyLanguageViewModel.getRows(), this::onRowClickListener)
           rv_all.adapter = addEditAdapter
           title = baseContext.getString(R.string.currency_language)
           returnArrow.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_arrow_back))

       changeCurrencyLanguageViewModel.isRefreshLayout = false
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            // addEditAdapter.select(changeCurrencyLanguageViewModel.selectedPosition)
            when (requestCode) {
                CURRENCY_REQUEST -> {
                    changeCurrencyLanguageViewModel.currencyLookup =
                            data?.getIntegerArrayListExtra(getString(R.string.selected_lookups))!![0] as LookUpsParcelable

                    addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                            changeCurrencyLanguageViewModel.currencyLookup!!.name)

                    btn_save.performClick()

                }
                COUNTRY_REQUEST -> {
                    changeCurrencyLanguageViewModel.countryLookup =
                            data?.getIntegerArrayListExtra(getString(R.string.selected_lookups))!![0] as LookUpsParcelable

                    addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                            changeCurrencyLanguageViewModel.countryLookup!!.name)

                    btn_save.performClick()


                }
                LANGUAGE_REQUEST -> {
                    changeCurrencyLanguageViewModel.languageLookup =
                            data?.getIntegerArrayListExtra(getString(R.string.selected_lookups))!![0] as LookUpsParcelable

                    addEditAdapter.updateNameWithSelected(changeCurrencyLanguageViewModel.selectedPosition,
                            changeCurrencyLanguageViewModel.languageLookup!!.name)
                    changeCurrencyLanguageViewModel.isRefreshLayout = true

                    btn_save.performClick()
            }
            }
        }
    }


    companion object {
        const val CURRENCY_REQUEST = 1
        const val COUNTRY_REQUEST = 2
        const val LANGUAGE_REQUEST = 3
    }
}
