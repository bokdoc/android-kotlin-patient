package com.bokdoc.pateintsbook.ui.autoCompleteResults

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.repositories.autoComplete.AutoCompleteRepository
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.AutoCompleteResource
import com.bokdoc.pateintsbook.utils.constants.AutoCompletesConstants

class AutoCompletesViewModel(val context: Context) : ViewModel() {
    private lateinit var autoCompleteRepository: AutoCompleteRepository
    private lateinit var screenType: String
    private var completeType: Int = 0

    init {
        autoCompleteRepository = AutoCompleteRepository(context)
    }

    //    fun complete(keyword: String):LiveData<List<AutoComplete>>{
//        if(keyword!="") {
//            return if (completeType == AutoCompletesConstants.SPECIALITY) {
//                completeSpeciality(keyword)
//            } else
//                if (completeType == AutoCompletesConstants.LOCATIONS) {
//                    completeLocations(keyword)
//                } else {
//                    completeInsurance(keyword)
//                }
//        }else{
//            val emptyLiveData = MutableLiveData<List<AutoComplete>>()
//            emptyLiveData.value = ArrayList()
//            return emptyLiveData
//        }
//
//    }
    fun complete(keyword: String): LiveData<List<AutoComplete>> {

        return if (completeType == AutoCompletesConstants.SPECIALITY) {
            completeSpeciality(keyword)
        } else {
            completeInsurance(keyword)
        }
    }

    fun completeSpeciality(keyword: String): LiveData<List<AutoComplete>> {
        return autoCompleteRepository.getSpecialityAutoResults(keyword, screenType)
    }

    fun completeLocations(keyword: String): LiveData<List<AutoComplete>> {
        return autoCompleteRepository.getLocationAutoResults(keyword)
    }

    fun completeInsurance(keyword: String): LiveData<List<AutoComplete>> {
        return autoCompleteRepository.getInsuranceAutoResults(keyword)
    }

    fun setScreenType(screenType: String) {
        this.screenType = screenType
    }

    fun getScreenType(): String {
        return screenType
    }

    fun setCompleteType(completeType: Int) {
        this.completeType = completeType
    }

    fun getCompleteType(): Int {
        return completeType
    }

}