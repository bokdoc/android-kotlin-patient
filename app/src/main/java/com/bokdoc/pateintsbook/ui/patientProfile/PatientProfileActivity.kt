package com.bokdoc.pateintsbook.ui.patientProfile

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.changeCurrencyLanguages.ChangeCurrencyLanguageActivity
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.imagesViewer.ImagesViewerActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.disease.DiseaseActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.medications.MedicationsActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.speciality.SpecialityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.patientCompares.CompareActivity
import com.bokdoc.pateintsbook.ui.patientFavorites.FavoritesActivity
import com.bokdoc.pateintsbook.ui.patientRequests.OngoingRequestsFragment
import com.bokdoc.pateintsbook.ui.patientRequests.PastRequestsFragment
import com.bokdoc.pateintsbook.ui.patientRequests.RequestsActivity
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.updateProfile.UpdateProfileActivity
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bumptech.glide.Glide
import com.facebook.login.LoginFragment
import com.wdullaer.materialdatetimepicker.Utils
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.activity_patient_profile.*
import kotlinx.android.synthetic.main.item_profile_shimmer.*
import kotlinx.android.synthetic.main.patient_profile_bottom_sheet.view.*
import kotlinx.android.synthetic.main.toolbar.*

class PatientProfileActivity : ParentActivity() {


    private lateinit var patientProfileViewModel: PatientProfileViewModel
    private var fragments = ArrayList<PatientProfileFragment>()
    private var bottomSheetDialog: Dialog? = null
    lateinit var userSharedPreference: UserSharedPreference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_profile)
        enableBackButton()
        title = getString(R.string.my_porfile)
        userSharedPreference = UserSharedPreference(this)

        getViewModel()
        initializeViews()
        updateUi()
        moreIcon.visibility = VISIBLE
        moreText.setOnClickListener(this::onClick)
        moreIcon.setOnClickListener(this::onClick)
        editProfileImage.setOnClickListener(this::onClick)
        mediaList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        //lay_edit.setOnClickListener(this)
        setupMediaObservers()
        mediaProgress.visibility = VISIBLE
        patientProfileViewModel.getMedia()

    }

    private fun showBottomSheet() {
        if (bottomSheetDialog == null) {
            val view = layoutInflater.inflate(R.layout.patient_profile_bottom_sheet, null)
            view.mySpecialities.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(this, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), SpecialityActivity::class.java)
            }
            view.myDiseases.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(this, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), DiseaseActivity::class.java)
            }
            view.myMedications.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(this, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), MedicationsActivity::class.java)
            }
            bottomSheetDialog = Dialog(this, R.style.MaterialDialogSheet)
            bottomSheetDialog?.setContentView(view)
            bottomSheetDialog?.setContentView(view)
            bottomSheetDialog?.setCancelable(true)
            bottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            bottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        }
        bottomSheetDialog?.show()

    }

    private fun setupMediaObservers() {
        patientProfileViewModel.mediaLiveData.observe(this, Observer {
            mediaProgress.visibility = GONE
            if (it?.status == DataResource.SUCCESS) {
                if (it.data?.isNotEmpty()!!) {
                    mediaList.adapter = PatientMediaAdapter(it.data!!) { view, position ->
                        Navigator.navigate(this, arrayOf(getString(R.string.selected_position)),
                                arrayOf(position.toString()), getString(R.string.images), patientProfileViewModel.getMediaUris(),
                                ImagesViewerActivity::class.java, -1)
                    }
                } else {
                    noMediaFound.visibility = VISIBLE
                }
            } else {
                it?.message?.isNotEmpty()?.let { isEmpty ->
                    if (!isEmpty)
                        com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, it.message)
                }
            }
        })
        patientProfileViewModel.remainingMediaLiveData.observe(this, Observer {
            if (it != null)
                moreText.text = it
        })
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            R.id.moreIcon -> {
                showBottomSheet()
            }
            R.id.reviewButton -> {
                Navigator.navigate(this, ReviewsActivity::class.java)
            }
            R.id.userScope -> {
                Navigator.navigate(this, ChangeCurrencyLanguageActivity::class.java, CHANGE_SCOPE_REQUEST)
            }
            R.id.moreText -> {
                Navigator.navigate(this, arrayOf(getString(R.string.is_show_only)),
                        arrayOf(getString(R.string.is_show_only)), getString(R.string.media_key),
                        patientProfileViewModel.allMedia as ArrayList<out Parcelable>
                        , AttachmentMediaActivity::class.java,-1)

            }
            R.id.editProfileImage -> {
                Navigator.navigate(this, UpdateProfileActivity::class.java)
            }
            /*
            lay_edit.id -> {
                Navigator.navigate(this, UpdateProfileActivity::class.java)
            }
            */
        }
    }

    override fun onResume() {
        super.onResume()
        if (patientProfileViewModel.patientProfileRepository.myFavoritesRepository.userSharedPreference.sharedPrefrenceManger
                        .getData(getString(R.string.language), "en") == "en") {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            navigation.layoutDirection = View.LAYOUT_DIRECTION_LTR
        } else {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
            navigation.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }
    }

    private fun initializeViews() {

        //signout view
        //icon.setImageResource(ic_sing_out)

        userSharedPreference.getUser()?.let {
            patientName.text = userSharedPreference.getUser()!!.name
            Glide.with(this).load(it.picture).into(patientImage)

            userScope.text = StringsConstants.buildString(arrayListOf(it.countryLookup.name, "/", it.languageLookup.name,
                    "/", it.currencyLookup.name))

            patientGender.text = it.gender

            patientAge.text = it.age

            setupTabs()
        }


        reviewButton.setOnClickListener(this::onClick)
        userScope.setOnClickListener(this::onClick)

        /*
        include_signout.setOnClickListener {
            patientProfileViewModel.logout()
            Navigator.navigateBack(this)
        }
        */
    }

    private fun setupTabs() {
        var adapter = TabsPagerAdapter(supportFragmentManager)
        infoPager.adapter = adapter
        patient_tabs.setupWithViewPager(infoPager)
        //infoPager.currentItem =0

        patient_tabs.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorAccent))
        val tabLayout = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout.text = getString(R.string.my_favourites)
        tabLayout.compoundDrawablePadding = 10
        tabLayout.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_grey_heart, 0, 0)
        patient_tabs.getTabAt(0)?.customView = tabLayout

/*
        val tabLayout1 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout1.text = getString(R.string.my_compares)
        tabLayout1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_grey_compare, 0, 0)
        //patient_tabs.getTabAt(1)?.customView = tabLayout1
        patient_tabs.getTabAt(1)?.customView = tabLayout1
        */

        val tabLayout2 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout2.text = getString(R.string.my_requests)
        tabLayout.compoundDrawablePadding = 10
        tabLayout2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_grey_requests, 0, 0)
        //patient_tabs.getTabAt(2)?.customView = tabLayout2
        patient_tabs.getTabAt(2)?.customView = tabLayout2
    }


    private fun getViewModel() {
        patientProfileViewModel = InjectionUtil.getPatientsViewModel(this)
    }


    private fun updateUi() {
        /*
        rv_patient_profile.adapter = PatientProfileMenuAdapter(patientProfileViewModel.getPatientProfileMenu(activity),
                this::onPatientProfileMenuClic)
                */
    }

    private fun onPatientProfileMenuClic(title: String) {
        when (title) {
            /*
            getString(R.string.my_favourites) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), FavoritesActivity::class.java)
            getString(R.string.my_requests) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), RequestsActivity::class.java)
            getString(R.string.choronic) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), DiseaseActivity::class.java)
            getString(R.string.specaialist_interest) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), SpecialityActivity::class.java)
            getString(R.string.my_reviews) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), ReviewsActivity::class.java)
            getString(R.string.my_compare) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), CompareActivity::class.java)
            getString(R.string.medications) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey)), arrayOf(user.id), MedicationsActivity::class.java)
            getString(R.string.medical_files) -> Navigator.navigate(this, arrayOf(getString(R.string.idKey),getString(R.string.load_user_media)),
                    arrayOf(user.id,getString(R.string.load_user_media)), AttachmentMediaActivity::class.java)
                    */
        }
    }


//    companion object {
//        @JvmStatic
//        fun newInstance(): PatientProfileActivity {
//            val patientProfileFragment = PatientProfileActivity()
//            return patientProfileFragment
//        }
//
//    }

    inner class TabsPagerAdapter(supportFragmentManage: android.support.v4.app.FragmentManager
    ) : FragmentPagerAdapter(supportFragmentManage) {


        override fun getItem(position: Int): Fragment {
            when (position) {

                0 -> return PatientFavoritesFragment()
                1 -> return PatientRequestsFragment()
            }
            return Fragment()
        }

        override fun getCount(): Int {
            return 2
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        }

        fun addFragment(fragment: Fragment, title: String) {
            // mFragmentList.add(fragment)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CHANGE_SCOPE_REQUEST && resultCode == Activity.RESULT_OK) {
            initializeViews()
        } else if (requestCode == UPDATE_PROFILE_REQUEST && resultCode == Activity.RESULT_OK) {
            initializeViews()
        } else if(requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK){

        }
    }


    companion object {
        const val CHANGE_SCOPE_REQUEST = 1
        const val UPDATE_PROFILE_REQUEST = 2
        const val BOOK_REQUEST = 3
    }

}