package com.bokdoc.pateintsbook.ui.searchResults

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.FeesSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_search_card_button.view.*

class CardButtonsAdapter(private val dynamicLinks: List<DynamicLinks>, private val onClickListener: (DynamicLinks) -> Unit) :
        RecyclerView.Adapter<CardButtonsAdapter.CardLinksViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CardLinksViewHolder {
        return CardLinksViewHolder(parent.inflateView(R.layout.item_search_card_button))
    }

    override fun getItemCount(): Int {
        return dynamicLinks.size
    }

    override fun onBindViewHolder(viewHolder: CardLinksViewHolder, position: Int) {
        dynamicLinks[position].let {
            if (it.fees.feesAverage!!.isNotEmpty() && it.fees.feesAverage != "0"  && it.fees.feesAverage != null) {
                viewHolder.itemView.tv_value.text = getTextWithPrice(viewHolder.itemView.context, it.fees.feesAverage!!,
                        it.fees.currencySymbols!!, R.string.two_texts_placeholder, it.label,it.fees.oldAmount)
            } else {
                viewHolder.itemView.tv_value.text = it.label
            }

            if (it.icon != null) {
                viewHolder.itemView.iv_image.visibility = VISIBLE
                when (it.icon) {
                    DynamicLinks.KEY_CLINIC_ICON -> {
                        Glide.with(viewHolder.itemView.context).load(R.drawable.ic_appointments).into(viewHolder.itemView.iv_image)
                    }
                    DynamicLinks.KEY_ONLINE_CONSULTING_ICON -> {
                        Glide.with(viewHolder.itemView.context).load(R.drawable.ic_consulting_fees).into(viewHolder.itemView.iv_image)
                    }
                    DynamicLinks.KEY_SURGERIES_ICON -> {
                        Glide.with(viewHolder.itemView.context).load(R.drawable.ic_surgery_fees).into(viewHolder.itemView.iv_image)
                    }
                    else -> {
                        viewHolder.itemView.iv_image.visibility = GONE
                    }
                }

            } else {
                viewHolder.itemView.iv_image.visibility = GONE
            }
        }
    }

    private fun getTextWithPrice(context: Context, feesAverage: String, currentSymbols: String,
                                 stringRes: Int, label: String,oldFees:String?, color: Int = R.color.colorPrimary): Spannable {

        val price = context.getString(R.string.pricePlaceHolder, feesAverage,
                currentSymbols)
        var consulting = context.getString(stringRes, label, price)

        var oldFeesCurrency = ""
        oldFees?.let {
            if(it.isNotEmpty() && it!="0") {
                oldFeesCurrency = "$it $currentSymbols"
                consulting = context.getString(R.string.placeHolder, consulting,oldFeesCurrency)
            }
        }

        return FeesSpannable.getSpannable(consulting, price, ContextCompat.getColor(context, color),oldFeesCurrency)
    }


    inner class CardLinksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onClickListener.invoke(dynamicLinks[adapterPosition])
            }
        }
    }
}