package com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_qualification.view.*

class QualificationsAdapter(private val qualifications: List<Qualifications>) : RecyclerView.Adapter<QualificationsAdapter.QualificationsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): QualificationsViewHolder {
        return QualificationsViewHolder(parent.inflateView(R.layout.item_qualification))
    }

    override fun getItemCount(): Int {
        return qualifications.size
    }

    override fun onBindViewHolder(holder: QualificationsViewHolder, position: Int) {
        qualifications[position].let {
            if (it.title.isNotEmpty()) {
                holder.itemView.tv_qualification_title.text = ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                        holder.itemView.context.getString(R.string.titleDots), it.title), it.title, ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
            } else {
                holder.itemView.tv_qualification_title.visibility = View.GONE
            }

            if (it.years.isNotEmpty()) {
                holder.itemView.tv_qualification_year.text = ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                        holder.itemView.context.getString(R.string.yearDots), it.years), it.years, ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
            } else {
                holder.itemView.tv_qualification_year.visibility = View.GONE
            }

            if (it.university_id.isNotEmpty()) {
                holder.itemView.tv_qualification_university.text = ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                        holder.itemView.context.getString(R.string.universityDots), it.university_id), it.university_id, ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
            } else {
                holder.itemView.tv_qualification_university.visibility = View.GONE
            }

            if (it.content.isNotEmpty()) {
                holder.itemView.tv_qualification_content.text = it.content
            } else {
                holder.itemView.tv_qualification_content.visibility = View.GONE
            }

            if (it.description.isNotEmpty()) {
                holder.itemView.tv_qualification_description.text = it.description
            } else {
                holder.itemView.tv_qualification_description.visibility = View.GONE
            }

            if (it.icon.isNotEmpty()) {
                holder.itemView.iv_accreditation.visibility = View.VISIBLE

                Glide.with(holder.itemView.context).load(it.icon).into(holder.itemView.iv_accreditation)
             } else {
                holder.itemView.iv_accreditation.visibility = View.GONE
            }

            if(itemCount == 1 || position == itemCount-1){
                holder.itemView.line.visibility = GONE
            }else{
                holder.itemView.line.visibility = VISIBLE
            }

        }
    }


    inner class QualificationsViewHolder(view: View) : RecyclerView.ViewHolder(view)

}