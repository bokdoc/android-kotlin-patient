package com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.toolbar.*

class ProfileConsultingActivity : ParentActivity() {

    lateinit var consultingViewModel: ConsultingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()
        setTitle()
        //title = getString(R.string.onlineConsulting)
        getViewModel()
        getDataFromIntent()
        getConsulting()
    }

    private fun setTitle(){
        if (intent?.hasExtra(getString(R.string.typeKey))!!) {
            val profileKey = intent?.getStringExtra(getString(R.string.typeKey))!!
            when (profileKey) {
                Profile.DOCTOR.toString() -> {
                    title = getString(R.string.consulting_list_doc_place_holder, Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
                    showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                }
            }
            try {
                if(Profile.isHospitalCategory(profileKey.toInt())){
                    title = getString(R.string.consulting_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
                    showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                }
            }catch (e:Exception){

            }
        }
    }

    private fun getConsulting() {
        progress.visibility = View.VISIBLE
        consultingViewModel.getConsulting().observe(this, Observer { it ->
            when (it!!.status) {
                DataResource.SUCCESS -> {
//                    rightTextView.visibility= View.VISIBLE
//                    rightTextView.text = it!!.data!!.size.toString() + " "+getString(R.string.consulting)
                    when (it.data!!.isEmpty()) {
                        true -> {
                            showEmpty(getString(R.string.no_consulting), "", null)
                        }
                        false -> {
                            rv_list.adapter = ConsultingAdapter(it.data!!, this::itemCLickListener)
                            progress.visibility = View.GONE

                        }
                    }


                }
                DataResource.INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE

                }
            }
        })
    }

    fun itemCLickListener(url: String, nv: NextAvailabilitiesParcelable, position: Int) {

        if (nv.book_button!!.endpointUrl.isEmpty()) {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key)), arrayOf(url), getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nv), BookAppointmentsActivity::class.java, BOOK_REQUEST)
        } else {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)), arrayOf(nv.book_button!!.endpointUrl, url),
                    getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nv), NextAvailabilityActivity::class.java, BOOK_REQUEST)
        }


    }

    private fun getDataFromIntent() {
        consultingViewModel.id = intent?.getStringExtra(getString(R.string.idKey))!!
        consultingViewModel.serviceType = getString(R.string.consultingWithNextAviInclude)
    }

    private fun getViewModel() {

        consultingViewModel = InjectionUtil.getConsultingViewModel(this)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode== BOOK_REQUEST && resultCode == Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }
}