package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.utils.constants.SocialConstants
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import kotlinx.android.synthetic.main.accommadation_header.view.*
import kotlinx.android.synthetic.main.accommdation_childlren.view.*

class AccommodationAdapter(context: Context, var sectionItemList: List<AccommodationSections>, var selectedListener:
(view: View) -> Unit) :
        SectionRecyclerViewAdapter<AccommodationSections, String,
                AccommodationAdapter.SectionViewHolder, AccommodationAdapter.ChildViewHolder>(context, sectionItemList) {

    override fun onCreateSectionViewHolder(viewGroup: ViewGroup?, p1: Int): SectionViewHolder {
        return SectionViewHolder(viewGroup?.inflateView(R.layout.accommadation_header)!!)
    }

    override fun onBindSectionViewHolder(sectionViewHolder: SectionViewHolder?, position: Int, accommodationSections: AccommodationSections?) {
        sectionViewHolder?.view?.header?.text = accommodationSections?.title
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?, position: Int): ChildViewHolder {
        return ChildViewHolder(viewGroup?.inflateView(R.layout.accommdation_childlren)!!)
    }

    override fun onBindChildViewHolder(viewHolder: ChildViewHolder?, sectionPosition: Int, childPosition: Int, resource: String?) {
        if (sectionItemList[sectionPosition].title == AccommodationSections.SOCIAL_NETWORKS_SECTION) {
            viewHolder?.view?.text?.visibility = GONE
            val socialNetworks = resource?.split(StringsConstants.SPACE)
            socialNetworks?.forEach {
                if (it.contains(SocialConstants.FACEBOOK))
                    viewHolder?.view?.facebookImage.let { initializeSocialImage(it!!, resource) }
                if (it.contains(SocialConstants.TWITTER))
                    viewHolder?.view?.twitterImage.let { initializeSocialImage(it!!, resource) }
                if (it.contains(SocialConstants.GOOGLE_PLUS))
                    viewHolder?.view?.googlePlusImage.let { initializeSocialImage(it!!, resource) }
                if (it.contains(SocialConstants.INSTAGRAM))
                    viewHolder?.view?.instagramImage.let { initializeSocialImage(it!!, resource) }
                if (it.contains(SocialConstants.LINKEDIN))
                    viewHolder?.view?.linkedInImage.let { initializeSocialImage(it!!, resource) }
            }
        } else {
            viewHolder?.view?.facebookImage?.visibility = GONE
            viewHolder?.view?.text?.visibility = VISIBLE
            viewHolder?.view?.text?.text = resource

            if (sectionItemList[sectionPosition].title == AccommodationSections.LOCATION_ROOM_SECTION) {
                viewHolder?.view?.locationImage?.visibility = VISIBLE
            } else {
                viewHolder?.view?.locationImage?.visibility = GONE
            }

            if (sectionItemList[sectionPosition].title == AccommodationSections.BEDS_NUMBER_SECTION) {
                viewHolder?.view?.text?.text = viewHolder?.view?.context?.getString(R.string.placeHolder, resource,
                        viewHolder.view.context.getString(R.string.beds))
            }

            if (sectionItemList[sectionPosition].title == AccommodationSections.AMBULANCE_SECTION) {
                viewHolder?.view?.text?.text = viewHolder?.view?.context?.getString(R.string.placeHolder, resource,
                        viewHolder.view.context.getString(R.string.ambulances))
            }

            if (sectionItemList[sectionPosition].title == AccommodationSections.ACCOMPANYING_PERSON_SECTION) {
                viewHolder?.view?.text?.text = viewHolder?.view?.context?.getString(R.string.placeHolder, resource,
                        viewHolder.view.context.getString(R.string.persons))
            }
        }
    }

    private fun initializeSocialImage(view: ImageView, url: String) {
        view.visibility = VISIBLE
        view.tag = url
        view.setOnClickListener { selectedListener.invoke(it) }
    }

    /*
    fun updateList(newSectionItemList: List<AutoCompleteSectionHeader>){
        sectionItemList = newSectionItemList
        notifyDataChanged(newSectionItemList)
    }
    */

    class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    }

    class ChildViewHolder(val view: View) : RecyclerView.ViewHolder(view)


}