package com.bokdoc.pateintsbook.ui.contactUs

import android.arch.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.ui.contactUs.ContactUsViewModel
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_contact_us.*

class ContactUsActivity : ParentActivity() {

    lateinit var contactUsViewModel: ContactUsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        enableBackButton()
        title = getString(R.string.contact_us)
        Utils.hideKeyboard(this)
        getViewModel()
        btn_contact_us.setOnClickListener(this)
        setupHintTextColors()
    }

    private fun setupHintTextColors(){

        et_contact_us_name.hint = ColoredTextSpannable.getSpannable(getString(R.string.nameWithStar),getString(R.string.star),
                ContextCompat.getColor(activity?.applicationContext!!,R.color.colorErrorRed))

        et_contact_us_mail.hint = ColoredTextSpannable.getSpannable(getString(R.string.emailWithStar),getString(R.string.star),
                ContextCompat.getColor(activity?.applicationContext!!,R.color.colorErrorRed))

        et_contact_us_message.hint = ColoredTextSpannable.getSpannable(getString(R.string.messageWithStar),getString(R.string.star),
                ContextCompat.getColor(activity?.applicationContext!!,R.color.colorErrorRed))
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_contact_us.id -> {
                contactUsViewModel.message = et_contact_us_message.text.toString()
                contactUsViewModel.name = et_contact_us_name.text.toString()
                contactUsViewModel.email = et_contact_us_mail.text.toString()
                sendFeedBack()
            }
        }
    }

    private fun sendFeedBack() {
        switchSendingProgressVisibility(View.VISIBLE)
        contactUsViewModel.contactUs().observe(this, Observer {
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    switchSendingProgressVisibility(View.GONE)
                    Utils.showShortToast(this, getString(R.string.done))
                    finish()
                }
                DataResource.FAIL -> {
                    switchSendingProgressVisibility(View.GONE)
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wromg))
                }
            }
        })
    }

    private fun getViewModel() {
        contactUsViewModel = InjectionUtil.getContactUsViewModel(this)
    }
}