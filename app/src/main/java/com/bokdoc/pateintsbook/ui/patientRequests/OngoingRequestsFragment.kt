package com.bokdoc.pateintsbook.ui.patientRequests

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.fragment_ongoing_request.*
import kotlinx.android.synthetic.main.include_error.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.Serializable

class OngoingRequestsFragment : Fragment(), View.OnClickListener {


    lateinit var requestsViewModel: RequestsViewModel
    lateinit var meta: Meta

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ongoing_request, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (Utils.hasConnection(activity!!)) {
            true -> getData("")
            false -> showErrorOrEmpty(activity!!.getString(R.string.no_connection))
        }
    }

    private fun getData(ServiceType: String) {
        requestsViewModel.getRequests(ServiceType)

        requestsViewModel.metaLiveData.observe(activity!!, Observer {
            meta = it!!
        })

        requestsViewModel.errorMessageLiveData.observe(activity!!, Observer {
            Utils.showShortToast(activity!!, it.toString())
        })
        requestsViewModel.progressLiveData.observe(activity!!, Observer {
            when (it) {
                false -> progress?.visibility = View.GONE
                true -> progress?.visibility = View.VISIBLE
            }
        })

        requestsViewModel.requestsNewLiveData.observe(activity!!, Observer {
            when (it!!.size) {
                0 -> showErrorOrEmpty(activity!!.getString(R.string.no_requests))
                else -> {
                    showMain()
                }
            }

        })

    }


    private fun getViewModel() {
        requestsViewModel = InjectionUtil.getRequestsViewModel(activity!!)
    }

    companion object {

        fun newInstance(): OngoingRequestsFragment {
            val fragment = OngoingRequestsFragment()
            return fragment
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            rightTextView.id -> {
                val intent = Intent(activity, FilterActivity::class.java).putExtra("list", meta.serviceTypes as Serializable)
                activity!!.startActivity(intent)

            }
        }
    }

    fun showErrorOrEmpty(message: String) {
        lay_container.visibility = View.GONE
        lay_error.visibility = View.VISIBLE
        tv_error.text = message
    }

    fun showMain() {
        lay_container.visibility = View.VISIBLE
        lay_error.visibility = View.GONE
    }



}