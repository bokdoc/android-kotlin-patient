package com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.provider.ContactsContract
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.OverviewRow
import com.bokdoc.pateintsbook.data.webservice.models.Profile

class OverViewViewModel(val application: Application, val overViewRepository: OverViewRepository) : ViewModel() {

    var profileId: String = ""
    var services: String = ""

    fun getProfileOverView(): LiveData<DataResource<OverviewRow>> {
        return overViewRepository.getOverView(profileId, services)
    }
}