package com.bokdoc.pateintsbook.ui.patientRequests.dialogs

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.text.InputType
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.bokdoc.pateintsbook.R
import kotlinx.android.synthetic.main.input_dialog.*

class InputDialog(context:Context,val hint:String,val inputType:Int = InputType.TYPE_CLASS_TEXT): AppCompatDialog(context) {

    var callback: OnDialogClickedListener? = null
    var text = ""



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.input_dialog)

        et_input.hint = hint
        et_input.inputType = inputType

        if(text.isNotEmpty())
            et_input.setText(text)


        ok.setOnClickListener {
            callback?.onDialogClicked(et_input.text.toString())
        }

    }






    override fun show() {

        super.show()

        val layoutParams = WindowManager.LayoutParams()

        layoutParams.copyFrom(window!!.attributes)

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT

        window!!.attributes = layoutParams


    }



    fun setOnDialogClickedListener(l:OnDialogClickedListener) {

        callback = l

    }



    interface OnDialogClickedListener {

        fun onDialogClicked(reason:String)

    }

}