package com.bokdoc.pateintsbook.ui.linksCardsDetails

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.DOCTOR
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.ClinicsAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting.ConsultingAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries.SurgeriesAdapter
import com.bokdoc.pateintsbook.ui.maps.MapShowActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_links_cards_details.*

class LinksCardsDetailsActivity : ParentActivity() {

    private lateinit var linksCardsDetailsViewModel: LinksCardsDetailsViewModel
    var clinicsAdapter: ClinicsAdapter? = null
    var surgeriesAdapter: SurgeriesAdapter? = null

    private var isLoading: Boolean = false
    private var isLastPage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_links_cards_details)
        enableBackButton()

        getViewModel()
        getData()


    }

    private fun getData() {
        linksCardsDetailsViewModel.getDetails()
        progress.visibility = VISIBLE

        linksCardsDetailsViewModel.clinicsRows.observe(this, Observer {
            if (it?.data != null) {

                if (intent?.hasExtra(getString(R.string.profileKey))!!) {
                    val profileKey = intent?.getStringExtra(getString(R.string.profileKey))!!
                    when (profileKey) {
                        DOCTOR.toString() -> {
                            title = getString(R.string.clinics_list_doc_place_holder,
                                    Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    }
                    try {
                        if (Profile.isHospitalCategory(profileKey.toInt())) {
                            title = getString(R.string.clinics_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    } catch (e: Exception) {

                    }
                }
                clinicsAdapter = ClinicsAdapter((it.data as ArrayList<ClinicRow>?)!!, this::onNextAvailabilitiesClickListener, this::onMapClickListener
                        , this::mediaCLickListener)
                linksDetailsRecycler.adapter = clinicsAdapter
            } else {
                if (it?.message!!.isNotEmpty()) {
                    Utils.showLongToast(this, it.message)
                }
            }
            progress.visibility = GONE
        })

        linksCardsDetailsViewModel.surgeriesRows.observe(this, Observer {
            if (it?.data != null) {
                //  title = linksCardsDetailsViewModel.screenTitle
                if (intent?.hasExtra(getString(R.string.profileKey))!!) {
                    val profileKey = intent?.getStringExtra(getString(R.string.profileKey))!!
                    when (profileKey) {
                        DOCTOR.toString() -> {
                            title = getString(R.string.surgeries_list_doc_place_holder,
                                    Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))

                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    }
                    try {
                        if (Profile.isHospitalCategory(profileKey.toInt())) {
                            title = getString(R.string.surgeries_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    } catch (e: Exception) {

                    }

                }

                surgeriesAdapter = SurgeriesAdapter((it.data as ArrayList<SurgeriesRow>?)!!, this::onNextAvailabilitiesClickListener,
                        this::onSurgeriesSelectedListener, {
                    Navigator.navigate(this, getString(R.string.locatioKey), it, MapShowActivity::class.java)
                }, {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(it.endpointUrl, ""),
                            BookAppointmentsActivity::class.java, BOOK_REQUEST)
                })
                linksDetailsRecycler.adapter = surgeriesAdapter
            } else {
                if (it?.message!!.isNotEmpty()) {
                    Utils.showLongToast(this, it.message)
                }
            }
            progress.visibility = GONE
        })

        linksCardsDetailsViewModel.consultingRows.observe(this, Observer {
            if (it?.data != null) {
                // title = linksCardsDetailsViewModel.screenTitle
                //  title = getString(R.string.clinics_list_doc_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
                if (intent?.hasExtra(getString(R.string.profileKey))!!) {
                    val profileKey = intent?.getStringExtra(getString(R.string.profileKey))!!
                    when (profileKey) {
                        DOCTOR.toString() -> {
                            title = getString(R.string.consulting_list_doc_place_holder,
                                    Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))

                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    }
                    try {
                        if (Profile.isHospitalCategory(profileKey.toInt())) {
                            title = getString(R.string.consulting_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
                            showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
                        }
                    } catch (e: Exception) {

                    }

                }
                linksDetailsRecycler.adapter = ConsultingAdapter(it.data!!, this::onNextAvailabilitiesClickListener)
            } else {
                if (it?.message!!.isNotEmpty()) {
                    Utils.showLongToast(this, it.message)
                }
            }
            progress.visibility = GONE
        })

        setUpPagination()

    }

    private fun setUpPagination() {
        linksDetailsRecycler.addOnScrollListener(object : PaginationScrollListener(linksDetailsRecycler.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                linksCardsDetailsViewModel.currentPage = linksCardsDetailsViewModel.currentPage.plus(1)
                if (linksCardsDetailsViewModel.currentPage <= linksCardsDetailsViewModel.totalPages) {
                    linksCardsDetailsViewModel.url = linksCardsDetailsViewModel.url + "&page=" + linksCardsDetailsViewModel.currentPage.toString()
                    isLoading = true

                    if (clinicsAdapter != null) {
                        clinicsAdapter!!.addLoadingFooter()
                        linksCardsDetailsViewModel.clinicsRows.observe(this@LinksCardsDetailsActivity, Observer {
                            when (it!!.status) {
                                SUCCESS -> {
                                    if (clinicsAdapter == null) {
                                        clinicsAdapter = ClinicsAdapter((it.data as ArrayList<ClinicRow>?)!!
                                                , this@LinksCardsDetailsActivity::onNextAvailabilitiesClickListener
                                                , this@LinksCardsDetailsActivity::onMapClickListener
                                                , this@LinksCardsDetailsActivity::mediaCLickListener)
                                        linksDetailsRecycler.adapter = clinicsAdapter
                                    } else {
                                        if (isLoading) {
                                            clinicsAdapter!!.removeLoadingFooter()
                                            isLoading = false
                                            if (it.data != null)
                                                clinicsAdapter!!.addAll(it.data as ArrayList<ClinicRow>)
                                        } else {
                                            if (it.data != null)
                                                clinicsAdapter!!.updateList(it!!.data as ArrayList<ClinicRow>)
                                        }

                                    }
                                }
                                INTERNAL_SERVER_ERROR -> {
                                    Utils.showShortToast(this@LinksCardsDetailsActivity, getString(R.string.some_thing_went_wrong))
                                    progress.visibility = View.GONE
                                }
                            }

                        })
                    } else if (surgeriesAdapter != null) {
                        surgeriesAdapter!!.addLoadingFooter()
                        linksCardsDetailsViewModel.surgeriesRows.observe(this@LinksCardsDetailsActivity, Observer {
                            when (it!!.status) {
                                SUCCESS -> {
                                    if (surgeriesAdapter == null) {
                                        surgeriesAdapter = SurgeriesAdapter((it.data as ArrayList<SurgeriesRow>?)!!, this@LinksCardsDetailsActivity::onNextAvailabilitiesClickListener,
                                                this@LinksCardsDetailsActivity::onSurgeriesSelectedListener, {
                                            Navigator.navigate(this@LinksCardsDetailsActivity, getString(R.string.locatioKey), it, MapShowActivity::class.java)
                                        }, {
                                            Navigator.navigate(this@LinksCardsDetailsActivity, arrayOf(getString(R.string.url_key),
                                                    getString(R.string.service_type)), arrayOf(it.endpointUrl, ""),
                                                    BookAppointmentsActivity::class.java, BOOK_REQUEST)
                                        })
                                        linksDetailsRecycler.adapter = surgeriesAdapter
                                    } else {
                                        if (isLoading) {
                                            surgeriesAdapter!!.removeLoadingFooter()
                                            isLoading = false
                                            if (it.data != null)
                                                surgeriesAdapter!!.addAll(it.data as ArrayList<SurgeriesRow>)
                                        } else {
                                            if (it.data != null)
                                                surgeriesAdapter!!.updateList(it!!.data as ArrayList<SurgeriesRow>)
                                        }

                                    }
                                }
                                INTERNAL_SERVER_ERROR -> {
                                    Utils.showShortToast(this@LinksCardsDetailsActivity, getString(R.string.some_thing_went_wrong))
                                    progress.visibility = View.GONE
                                }
                            }

                        })
                    }


                } else
                    isLastPage = true
            }


        })
    }

    private fun getViewModel() {
        linksCardsDetailsViewModel = InjectionUtil.getLinkCardDetailsViewModel(this)
        linksCardsDetailsViewModel.url = intent?.getStringExtra(getString(R.string.url_key))!!

        if (intent?.hasExtra(getString(R.string.total_pages))!!) {
            linksCardsDetailsViewModel.totalPages = intent?.getStringExtra(getString(R.string.total_pages))!!.toInt()
        }
    }

    private fun onMapClickListener(location: Location) {
        Navigator.navigate(this, getString(R.string.locatioKey), location, MapShowActivity::class.java)

    }

    fun mediaCLickListener(allMedia: List<IMediaParcelable>) {
        Navigator.navigate(activity!!, arrayOf(getString(R.string.is_show_only)),
                arrayOf("true"), getString(R.string.media_key),
                allMedia as ArrayList<out Parcelable>
                , AttachmentMediaActivity::class.java, -1)
    }

    private fun onNextAvailabilitiesClickListener(url: String, nextAvailabilities: NextAvailabilitiesParcelable, position: Int) {

        if (nextAvailabilities.book_button!!.endpointUrl.isEmpty()) {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                    getString(R.string.service_type)), arrayOf(url, ""), getString(R.string.nextAvailabilitiesKey), arrayListOf(nextAvailabilities),
                    BookAppointmentsActivity::class.java, BOOK_REQUEST)

        } else {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)), arrayOf(nextAvailabilities.book_button!!.endpointUrl, url),
                    getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nextAvailabilities), NextAvailabilityActivity::class.java, BOOK_REQUEST)
        }

    }

    private fun onSurgeriesSelectedListener(surgeriesRow: SurgeriesRow) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }
}
