package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.utils.PriceTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.services_row.view.*

class ServicesAdapter(val services:List<Service>, private val selectedListener: ((view: View) -> Unit)? = null):
        RecyclerView.Adapter<ServicesAdapter.ServicesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesViewHolder {
        return ServicesViewHolder(parent.inflateView(R.layout.services_row))
    }

    override fun getItemCount(): Int {
        return services.size
    }

    override fun onBindViewHolder(holder: ServicesViewHolder, position: Int) {
        services[position].let {
            Glide.with(holder.itemView.context).load(it.picture).into(holder.itemView.image)
            holder.itemView.name.text = it.name
            holder.itemView.summary.text = it.summary
            holder.itemView.fee.text  =  PriceTextSpannable.getTextWithPrice(holder.itemView.context,it.feesService?.amount!!,
                    it.feesService?.symbols!!, R.string.feePlaceholder)
            //holder.itemView.rating.rating = it..toFloat()
        }
    }


    inner class ServicesViewHolder(view: View): RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                selectedListener?.invoke(it!!)
            }
        }
    }
}