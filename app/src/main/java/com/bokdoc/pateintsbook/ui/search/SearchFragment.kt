package com.bokdoc.pateintsbook.ui.search

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompleteResultsActivity
import com.bokdoc.pateintsbook.ui.homeSearch.AppointementFragment
import com.bokdoc.pateintsbook.utils.constants.AutoCompletesConstants
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.listeners.HomeSearchEditsListeners
import kotlinx.android.synthetic.main.home_search.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SearchFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var completeEditType:Int  = 0
    lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            //param1 = it.getString(com.bokdoc.pateintsbook.ui.home.ARG_PARAM1)
           // param2 = it.getString(com.bokdoc.pateintsbook.ui.home.ARG_PARAM2)
        }
        searchViewModel =  ViewModelProviders.of(this, SearchProvider(activity!!))
                .get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        searchViewModel.speciality.let {
            if(it.isNotEmpty())
                specialityEdit.setText(it)
        }

        specialityEdit.setOnTouchListener(object: HomeSearchEditsListeners(activity!!,arrayOf(getString(R.string.screenType)
                ,getString(R.string.completeType)),context!!.getString(searchViewModel.getWebserviceScreenType()), AutoCompletesConstants.SPECIALITY,
                AUTO_COMPLETE_REQUEST){
            override fun passCompletedType(completeType: Int) {
                completeEditType = completeType
            }

        })

        searchViewModel.regionCountry.let {
            if(it.isNotEmpty())
                cityEdit.setText(it)
        }


        searchViewModel.screenType.let {
            if(it == SearchScreenTypes.CONSULTING){
                //cityInput.visibility = GONE
            }
        }

        cityEdit.setOnTouchListener(object : HomeSearchEditsListeners(activity!!,arrayOf(getString(R.string.screenType)
                ,getString(R.string.completeType)),context!!.getString(searchViewModel.getWebserviceScreenType()), AutoCompletesConstants.LOCATIONS,
                AUTO_COMPLETE_REQUEST){
            override fun passCompletedType(completeType: Int) {
                completeEditType = completeType
            }

        })


        searchViewModel.insuranceCarrier.let {
            if(it.isNotEmpty())
                insuranceEdit.setText(it)
        }


        insuranceEdit.setOnTouchListener(object: HomeSearchEditsListeners(activity!!,arrayOf(getString(R.string.screenType)
                ,getString(R.string.completeType)),context!!.getString(searchViewModel.getWebserviceScreenType()), AutoCompletesConstants.INSURANCE
                , AUTO_COMPLETE_REQUEST){
            override fun passCompletedType(completeType: Int) {
                completeEditType = completeType
            }

        })

        search.setOnClickListener {
            searchViewModel.saveDataState()
            Navigator.naviagteBack(activity!!, arrayOf(getString(R.string.query),getString(R.string.screenType)),
                    arrayOf(searchViewModel.getQuery(),searchViewModel.screenType))
        }

        //searchTypeInput.visibility = VISIBLE
        searchTypeParent.visibility = VISIBLE
        searchTypeEdit.isEnabled = false
        searchTypeEdit.setText(searchViewModel.screenType)
        edit.setOnClickListener {
            showDialog()
        }

    }

    private fun showDialog(){
           val builder = AlertDialog.Builder(activity!!)

            builder.setSingleChoiceItems(SearchScreenTypes.values,SearchScreenTypes.getPosition(searchViewModel.screenType)
            ) { dialog, position ->
                searchTypeEdit.setText(SearchScreenTypes.values[position])
                searchViewModel.screenType = SearchScreenTypes.values[position]
                when(position){
                    /*
                    SearchScreenTypes.CONSULTING_POSITION -> cityInput.visibility = GONE
                    else -> cityInput.visibility = VISIBLE
                    */
                }
                dialog.dismiss()
            }

        builder.create().show()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == AUTO_COMPLETE_REQUEST && resultCode == Activity.RESULT_OK){
            val selectedText = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
            when (completeEditType) {
                AutoCompletesConstants.SPECIALITY -> {
                    specialityEdit.setText(selectedText)
                    searchViewModel.speciality = selectedText
                }
                AutoCompletesConstants.LOCATIONS -> {
                    cityEdit.setText(selectedText)
                    searchViewModel.regionCountry = selectedText
                }
                else -> {
                    insuranceEdit.setText(selectedText)
                    searchViewModel.insuranceCarrier = selectedText
                }
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            // throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AppointementFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
                AppointementFragment().apply {
                    arguments = Bundle().apply {
                        //putString(com.bokdoc.pateintsbook.ui.home.ARG_PARAM1, param1)
                        //putString(com.bokdoc.pateintsbook.ui.home.ARG_PARAM2, param2)
                    }
                }

        const val AUTO_COMPLETE_REQUEST = 0
    }
}
