package com.bokdoc.pateintsbook.ui.patientRequests

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.widget.TableLayout
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_requests.*
import kotlinx.android.synthetic.main.activity_tutorials.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.Serializable
import java.util.ArrayList
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import org.w3c.dom.Text


class RequestsActivity : ParentActivity() {

    lateinit var requestsViewModel: RequestsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_requests)
        setTitle(getString(R.string.my_requests))
        //setRightText(getString(R.string.filter))
        enableBackButton()
        showFilter()
        filterIcon.setOnClickListener(this::onClick)
        getViewModel()
        setUpTabs()
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            filterIcon.id -> {
                goToFilterActivity()
            }
        }
    }


    private fun goToFilterActivity() {
        if (requestsViewModel.metaLiveData.value?.filter != null && requestsViewModel.metaLiveData.value?.filter!!.isNotEmpty()) {
            val intent = Intent(activity, FilterActivity::class.java).putParcelableArrayListExtra("list",
                    requestsViewModel.metaLiveData.value?.filter?.get(0)?.data as ArrayList<out Parcelable>)
            activity.startActivityForResult(intent, FILTER_REQUEST)
        }
    }


    private fun getViewModel() {
        requestsViewModel = InjectionUtil.getRequestsViewModel(activity)
    }


    private fun setUpTabs() {
        //setup pager
        viewPager.adapter = TabsPagerAdapter(supportFragmentManager)
        /*
        tab_requests.setupWithViewPager(viewPager)
        */
        viewPager.currentItem = 0


        tab_requests.getTabAt(0)?.customView = (viewPager.adapter as TabsPagerAdapter).getTabView(this, getString(R.string.ongoing_request))

        tab_requests.getTabAt(1)?.customView = (viewPager.adapter as TabsPagerAdapter).getTabView(this, getString(R.string.past_requests), false)

        tab_requests.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                (tab.customView as TextView).setBackgroundResource(R.drawable.requests_custom_selected_tabs)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                (tab.customView as TextView).setBackgroundColor(ContextCompat.getColor(this@RequestsActivity, R.color.colorTransparent))
            }
        })

        /*
        val tabLayout1 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout1.text = getString(R.string.past_requests)
        tabLayout1.setTextColor(getResColor(R.color.colorWhite))
        tab_requests.getTabAt(1)?.customView = tabLayout1
        */

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==FILTER_REQUEST && resultCode == Activity.RESULT_OK) {
            requestsViewModel.currentPage = 1
            requestsViewModel.getRequests(data!!.getStringExtra("selectedParam"))
        }else if(requestCode==BOOK_REQUEST && resultCode == Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val FILTER_REQUEST = 100
        const val BOOK_REQUEST = 1
    }
}