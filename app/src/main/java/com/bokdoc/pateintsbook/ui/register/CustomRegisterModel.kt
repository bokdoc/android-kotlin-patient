package com.bokdoc.pateintsbook.ui.register

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompletesViewModel

class CustomRegisterModel(private val context: Context): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(context) as T
    }
}