package com.bokdoc.pateintsbook.ui.patientProfile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.FilterModel
import kotlinx.android.synthetic.main.switch_option_row.view.*

class FilterAdapter(private val ServiceTypeList: List<FilterModel>, private var lastSelectedPosition: Int = -1) : RecyclerView.Adapter<FilterAdapter.FilterViewHolder>() {

    private var onBind: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.switch_option_row, parent, false)
        return FilterViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        ServiceTypeList[position].let {
            holder.view.optionTitle.text = it.value
            onBind = true
            holder.view.switchButton.isChecked = lastSelectedPosition == position
            onBind = false
        }

    }


    override fun getItemCount(): Int {
        return ServiceTypeList.size
    }


    private fun onChecked(isChecked: Boolean, position: Int) {
        if (isChecked) {
            lastSelectedPosition = position
        } else {
            lastSelectedPosition = -1
        }
    }

    fun getSelectedParam(): String {
        return when {
            lastSelectedPosition != -1 ->
                ServiceTypeList[lastSelectedPosition].key
            else -> ""
        }

    }

    fun resetSelection() {
        if (lastSelectedPosition != -1) {
            val position = lastSelectedPosition
            lastSelectedPosition = -1
            notifyItemChanged(position)
        }
    }

    inner class FilterViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.switchButton.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (!onBind) {
                    onChecked(isChecked, layoutPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }
}