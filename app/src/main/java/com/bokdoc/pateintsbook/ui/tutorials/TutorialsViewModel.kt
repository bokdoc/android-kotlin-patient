package com.bokdoc.pateintsbook.ui.tutorials

import android.app.Application
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.TutorialsItemInfo

class TutorialsViewModel(private val application: Application) : ViewModel() {

    var colorBackground = R.color.colorWhite
    //  var descriptionAll = application.getString(R.string.searchForAppointmentsText)

    fun getTutorialsInfoItems(): List<TutorialsItemInfo> {
        val tutorialsItemInfo = ArrayList<TutorialsItemInfo>()

        tutorialsItemInfo.add(TutorialsItemInfo().apply {
            this.backgroundColor = colorBackground
            title = application.getString(R.string.first_slider_title)
            this.description = application.getString(R.string.first_slider_desc)
            imageRes = R.drawable.online_consulting_slider
            buttonText = application.getString(R.string.first_slider_btn_title)
        })

        tutorialsItemInfo.add(TutorialsItemInfo().apply {
            this.backgroundColor = colorBackground
            title = application.getString(R.string.second_slider_title)
            this.description = application.getString(R.string.second_slider_desc)
             imageRes = R.drawable.appointments_slider
            buttonText = application.getString(R.string.second_slider_btn_title)
        })

        tutorialsItemInfo.add(TutorialsItemInfo().apply {
            this.backgroundColor = colorBackground
            title = application.getString(R.string.third_slider_title)
            this.description = application.getString(R.string.third_slider_desc)
            imageRes = R.drawable.surgeries_slider
            buttonText = application.getString(R.string.third_slider_btn_title)
        })

        return tutorialsItemInfo
    }
}