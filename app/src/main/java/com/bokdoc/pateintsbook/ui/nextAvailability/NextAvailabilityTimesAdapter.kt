package com.bokdoc.pateintsbook.ui.nextAvailability

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_next_availablitity_time.view.*

class NextAvailabilityTimesAdapter(private val nextAvailablitities: List<NextAvailabilitiesParcelable>,
                                   var itemCLickListener: (nextAvi: NextAvailabilitiesParcelable, position: Int) -> Unit,
                                   var selectedNextAvailabilitiesParcelable: NextAvailabilitiesParcelable? = null)

    : RecyclerView.Adapter<NextAvailabilityTimesAdapter.NextAvailabilitiesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NextAvailabilitiesViewHolder {
        val itemView = parent.inflateView(R.layout.item_next_availablitity_time)
        val viewHolder: NextAvailabilitiesViewHolder = NextAvailabilitiesViewHolder(itemView)
        return viewHolder

    }

    override fun getItemCount(): Int {
        return nextAvailablitities.size
    }

    override fun onBindViewHolder(holder: NextAvailabilitiesViewHolder, position: Int) {
        nextAvailablitities[position].let {
            holder.itemView.tv_next_time.text = it.from
            when (it.status) {
                false -> {
                    holder.itemView.tv_next_time.background = ContextCompat.getDrawable(holder.itemView.context, R.drawable.next_availability_dumb_bg)
                    holder.itemView.tv_next_time.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                }
                true->{
                    selectedNextAvailabilitiesParcelable?.let {selected->
                        if(selected.idNextAvailabilities == it.idNextAvailabilities){
                            holder.itemView.tv_next_time.background = ContextCompat.getDrawable(holder.itemView.context,
                                    R.drawable.next_availability_selected_bg)
                            holder.itemView.tv_next_time.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.colorWhite))
                        }else{
                            holder.itemView.tv_next_time.background = ContextCompat.getDrawable(holder.itemView.context,
                                    R.drawable.next_availability_bg)
                            holder.itemView.tv_next_time.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorPrimary))

                        }
                    }
                }
            }

        }
    }

    inner class NextAvailabilitiesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                itemCLickListener.invoke(nextAvailablitities[adapterPosition], adapterPosition)
            }
        }
    }
}