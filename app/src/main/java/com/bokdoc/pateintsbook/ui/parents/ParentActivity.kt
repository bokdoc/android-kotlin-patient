package com.bokdoc.pateintsbook.ui.parents

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.login.LoginActivity
import com.bokdoc.pateintsbook.ui.patientProfile.PatientProfileActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.include_error.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.view_empty.*
import kotlinx.android.synthetic.main.view_progress.*

abstract class ParentActivity : AppCompatActivity(), View.OnClickListener {
    protected lateinit var activity: AppCompatActivity
    // private var rootView: FrameLayout? = null
    private var rootView: FrameLayout? = null
    private var toolbar: Toolbar? = null
    private var tvToolbarTitle: TextView? = null
    private var menuId: Int = 0
    private var enableBack: Boolean = false
    private var iconResId: Int = 0
    private var toolbarTitle: String? = null
    private lateinit var userRepository: UserSharedPreference
    var moreMenuRes: Int = -1
    lateinit var moreIconsClickListener: PopupMenu.OnMenuItemClickListener
    open var returnArrowClickListener = View.OnClickListener {
        existActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity = this
        rootView = findViewById<View>(android.R.id.content) as FrameLayout
        setUpNavigationListner()
    }

    private fun setUpNavigationListner() {
        // BottomNavigationViewHelper.disableShiftMode(navigation)
    }


    override fun onClick(p0: View?) {

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        // refresh your views here
        super.onConfigurationChanged(newConfig)
    }

    fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.lay_main_container, fragment)
        transaction.commit()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // init the view_toolbar
        toolbar = findViewById(R.id.toolbar)

        // check the view_toolbar
        if (toolbar != null) {
            // view_toolbar is available >> handle it
            setSupportActionBar(toolbar)
            titleText!!.text = title
            toolbar!!.title = ""
            tvToolbarTitle = toolbar!!.findViewById<View>(R.id.titleText) as TextView
            tvToolbarTitle!!.text = title

            if (enableBack) {
                returnArrow.visibility = VISIBLE
                returnArrow.setOnClickListener {
                    existActivity()
                    // check if enable back
                    if (enableBack) {
                        // set the suitable back icon
                        /*
                        if (iconResId != 0) {
                            toolbar!!.setNavigationIcon(iconResId)
                        } else {
                            toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back)
                        }
                        */
                    } else if (iconResId != 0) {
                        // set this icon
                        toolbar!!.setNavigationIcon(iconResId)
                    }
                }
            }
        }
        if (moreMenuRes != -1) {
            moreIcon.visibility = VISIBLE
            moreIcon.setOnClickListener {
                val popup = PopupMenu(this, it)
                popup.setOnMenuItemClickListener(moreIconsClickListener)
                popup.inflate(moreMenuRes)
                popup.show()
            }
        }
        returnArrow.setOnClickListener(returnArrowClickListener)
    }

    fun existActivity() {
        finish()
    }

    fun getResColor(id: Int): Int {
        return resources.getColor(id)
    }

    fun loadFragment(container: Int, fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(container, fragment)
                .commitAllowingStateLoss()
    }

    fun hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView!!)
        }
    }

    override fun setTitle(title: CharSequence) {
        toolbarTitle = title.toString()
        if (tvToolbarTitle != null) {
            tvToolbarTitle!!.text = title
        } else {
            super.setTitle(title)
        }
    }

    fun setRightText(string: String) {
        rightTextView.setOnClickListener(this)
        rightTextView.visibility = View.VISIBLE
        rightTextView.text = string
    }

    fun hideToolbarTitle() {
        if (tvToolbarTitle != null) {
            tvToolbarTitle!!.visibility = View.GONE
        }
    }

    fun showToolbarTitle() {
        if (tvToolbarTitle != null) {
            tvToolbarTitle!!.visibility = View.VISIBLE
        }
    }

    fun createOptionsMenu(menuId: Int) {
        this.menuId = menuId
        invalidateOptionsMenu()
    }

    fun removeOptionsMenu() {
        menuId = 0
        invalidateOptionsMenu()
    }

    fun enableBackButton() {
        enableBack = true
    }

    fun setToolbarIcon(iconResId: Int) {
        this.iconResId = iconResId
    }


    fun hasToolbar(): Boolean {
        return toolbar != null
    }

    fun hasInternetConnection(): Boolean {
        return Utils.hasConnection(this)
    }

    fun switchSendingProgressVisibility(visibility: Int) {
        view_progress.visibility = visibility
        if (visibility == View.VISIBLE) {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            view_progress.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }


    fun switchBgProgressVisibility(visibility: Int) {
        bgProgress.visibility = visibility
        if (visibility == View.VISIBLE) {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            bgProgress.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    fun showToolbarImage(image: String) {
        iv_toolbar_image.visibility = View.VISIBLE
        Glide.with(this).load(image).into(iv_toolbar_image)
    }

    fun showFilter() {
        filterIcon.visibility = View.VISIBLE
    }

    fun showError(message: String, view: View) {
        view.visibility = View.GONE
        // lay_error.visibility = View.VISIBLE
        //tv_error.text = message
    }

    fun showMain(view: View) {
        view.visibility = View.VISIBLE
        // lay_error.visibility = View.GONE
    }

    fun setView(viewId: Int) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val contentView = inflater.inflate(viewId, null, false)
        contentView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        container.addView(contentView, 0)

    }

    fun showEmpty(message: String, emptyTitle: String = "", to: Class<*>?, requestCode: Int = -1) {
        view_empty.visibility = View.VISIBLE
        if (emptyTitle.isEmpty())
            tv_not_avail.visibility = View.GONE
        tv_empty.text = message
        tv_not_avail.text = emptyTitle
        if (to != null) {
            btn_get_started.visibility = View.VISIBLE
            btn_get_started.setOnClickListener {
                // Navigator.navigate(this, arrayOf(getString(R.string.slug)), arrayOf(slug), to, requestCode)
            }
        }
    }


    fun hideEmpty() {
        view_empty.visibility = View.GONE
        tv_not_avail.visibility = View.GONE
    }

}