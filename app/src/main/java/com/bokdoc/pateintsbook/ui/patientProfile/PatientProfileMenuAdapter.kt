package com.bokdoc.pateintsbook.ui.patientProfile

import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersMenuAdapter
import com.bokdoc.pateintsbook.utils.extensions.inflateView

class PatientProfileMenuAdapter(menuOptions: List<ImageTextRow>, clickListener: (String) -> Unit) : HealthProvidersMenuAdapter(menuOptions, clickListener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return MenuViewHolder(parent.inflateView(R.layout.item_patient_profile))

    }
}