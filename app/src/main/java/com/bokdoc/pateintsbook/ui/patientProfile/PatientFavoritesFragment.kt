package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.patientFavorites.FavoritesActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes.APPOINTMENTS_POSITION
import kotlinx.android.synthetic.main.fragment_patient_profile.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PatientFavoritesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PatientFavoritesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PatientFavoritesFragment : PatientProfileFragment(), View.OnClickListener {

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            searchButton.id -> {
                Navigator.navigate(activity!!, arrayOf(getString(R.string.screenType), getString(R.string.query)),
                        arrayOf(SearchScreenTypes.getString(APPOINTMENTS_POSITION),
                               ""), SearchResultsActivity::class.java, HomeSearchActivity.SEARCH_REQUEST)
            }

        }

    }

    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchButton.setOnClickListener(this)
        viewAllText.setOnClickListener {
            Navigator.navigate(activity!!, FavoritesActivity::class.java,MainPatientProfileFragment.BOOK_REQUEST)
        }
    }


    override fun onResume() {
        super.onResume()
        patientProfileViewModel.getFavorites().observe(this, Observer {
            if (it?.data != null) {
                if (it.data!!.isNotEmpty()) {
                    dataGroup.visibility = VISIBLE
                    profilesList.adapter = PatientProfilesInfoAdapter(it.data!!, this::onClickListener)
                } else {
                    noDataGroup.visibility = VISIBLE
                }
            } else {
                if (it?.message?.isNotEmpty()!!) {
                    activity?.applicationContext?.let { it1 -> Utils.showLongToast(it1, it.message) }
                }
            }
            progress?.visibility = GONE
        })
    }

    private fun onClickListener(healthProvidersProfile: HealthProvidersProfile, position: Int) {
        Navigator.navigate(activity!!, getString(R.string.profileKey),
                arrayListOf(healthProvidersProfile),
                HealthProvidersProfileActivity::class.java,MainPatientProfileFragment.BOOK_REQUEST)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PatientFavoritesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
                PatientFavoritesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_REQUEST_TYPE, PatientProfileFragment.GET_MY_FAVORITES_ARG)
                    }
                }
    }
}
