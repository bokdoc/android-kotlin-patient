package com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.doctors.DoctorsRepository

class DoctorsViewModelProvider(private val doctorsRepository: DoctorsRepository):
        ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DoctorsViewModel(doctorsRepository) as T
    }
}