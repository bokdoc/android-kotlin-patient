package com.bokdoc.pateintsbook.ui.healthProvidersProfile.insuranceCarriers

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*

class InsuranceCarriersActivity : ParentActivity() {

    lateinit var insuranceCarriersViewModel: InsuranceCarriersViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()


        val typeKey = intent?.getStringExtra(getString(R.string.typeKey))!!
        when (typeKey) {
            Profile.DOCTOR.toString() -> {
                title = getString(R.string.insurance_list_doc_place_holder, Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
            }
        }

        try {
            if(Profile.isHospitalCategory(typeKey.toInt())){
                title = getString(R.string.insurance_list_hos_place_holder,
                        intent?.getStringExtra(getString(R.string.docNameSlug))!!)
            }
        }catch (e:Exception){

        }


        showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)

         getViewModel()
        getDataFromIntent()
        getInsuranceData()
    }

    private fun getInsuranceData() {

        progress.visibility = View.VISIBLE
        insuranceCarriersViewModel.getInsurance().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    rv_list.adapter = InsuranceCarriersAdapter(it!!.data!!)
                    progress.visibility = View.GONE

                }
                INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE

                }
            }
        })
    }

    private fun getDataFromIntent() {
        insuranceCarriersViewModel.id = intent?.getStringExtra(getString(R.string.idKey))!!
        insuranceCarriersViewModel.serviceType = "insuranceCarriers"

    }

    private fun getViewModel() {
        insuranceCarriersViewModel = InjectionUtil.getInsuranceCarriersViewModel(this)

    }
}