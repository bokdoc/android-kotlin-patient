package com.bokdoc.pateintsbook.ui.notifications

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.repositories.notifications.NotificationsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.notifications.NotificationsWebservice
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.patientRequests.RequestsViewModel
import com.bokdoc.pateintsbook.ui.patientRequests.dialogs.InputDialog
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioLoadingActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.view_empty.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NotificationsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NotificationsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class NotificationsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var notificationsViewModel: NotificationsViewModel? = null
    private var requestsViewModel: RequestsViewModel?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        if (activity != null) {
            notificationsViewModel = ViewModelsCustomProvider(NotificationsViewModel(
                    NotificationsRepository(UserSharedPreference(activity?.applicationContext!!),
                            NotificationsWebservice(WebserviceManagerImpl.instance())))).create(NotificationsViewModel::class.java)
        }
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getViewModel()

        titleText.text = getString(R.string.notifications)
        progress?.visibility = VISIBLE
        notificationsViewModel!!.getNotifications().observe(this, Observer {
            progress?.visibility = GONE
            if (it?.status == DataResource.SUCCESS) {
                it.data?.let {
                    if (it.isNotEmpty()) {
                        rv_notifications.adapter = NotificationsAdapter(it, this::onClickListener)
                    } else {
                        showEmpty(getString(R.string.empty_notifications), "", null, -1)
                    }
                }
            } else {
                it?.message?.let {
                    if (it.isNotEmpty()) {
                        if (activity != null)
                            Utils.showLongToast(activity!!, it)
                    } else {
                        if (activity != null)
                            Utils.showLongToast(activity!!, getString(R.string.server_error))
                    }
                }
            }
        })

    }

    fun showEmpty(message: String, emptyTitle: String = "", to: Class<*>?, requestCode: Int = -1) {
        view_empty.visibility = View.VISIBLE
        if (emptyTitle.isEmpty())
            tv_not_avail.visibility = View.GONE
        tv_empty.text = message
        tv_not_avail.text = emptyTitle
        if (to != null) {
            btn_get_started.visibility = View.VISIBLE
            btn_get_started.setOnClickListener {
                // Navigator.navigate(this, arrayOf(getString(R.string.slug)), arrayOf(slug), to, requestCode)
            }
        }
    }

    private fun showCancellationDialog(url: String) {
        if(activity==null || requestsViewModel ==null)
            return

        val inputDialog = InputDialog(activity!!, getString(R.string.enter_reason_message))
        inputDialog.text = requestsViewModel!!.cancellationReason
        inputDialog.callback = object : InputDialog.OnDialogClickedListener {
            override fun onDialogClicked(reason: String) {
                progress.visibility = View.VISIBLE
                inputDialog.dismiss()
                requestsViewModel?.cancelRequest(url, reason)?.observe(this@NotificationsFragment, Observer {
                    if (it != null) {
                        it.meta.message.let {
                            if (it.isNotEmpty() && activity != null)
                                Utils.showLongToast(activity!!, it)
                            progress.visibility = View.GONE
                        }
                        if (it.status == DataResource.INTERNAL_SERVER_ERROR && activity != null) {
                            Utils.showLongToast(activity!!, R.string.server_error)
                            showCancellationDialog(url)
                            progress.visibility = View.GONE
                        }
                        if (it.status == DataResource.SUCCESS) {
                            Navigator.navigate(activity!!, MainActivity::class.java)
                            activity!!.finish()
                            progress.visibility = View.GONE
                        }
                    }
                })
            }

        }
        inputDialog.show()

    }

    private fun getViewModel() {
        activity?.applicationContext?.let {
            requestsViewModel = InjectionUtil.getRequestsViewModel(this,it.applicationContext)
        }
    }

    private fun onClickListener(notifications: INotifications, dynamicLink: DynamicLinks, position: Int) {

        when (dynamicLink.targetScreen) {
            PAYMENT -> {
                if(activity!=null) {
                    val intent = Intent(activity!!, WebViewActivity::class.java)
                    intent.putExtra(getString(R.string.url_key), dynamicLink.endpointUrl)
                    intent.putExtra(getString(R.string.title_key), getString(R.string.payment))
                    startActivity(intent)
                }
            }
            CANCEL -> {
                showCancellationDialog(dynamicLink.endpointUrl)
            }
            REVIEW -> {
                if(activity!=null) {
                    val intent = Intent(activity!!, WebViewActivity::class.java)
                    intent.putExtra(getString(R.string.url_key), dynamicLink.endpointUrl)
                    intent.putExtra(getString(R.string.title_key), getString(R.string.add_reviews))
                    startActivity(intent)
                }
            }
            CONSULTING, CONSULTING_NOW -> {
                if(activity!=null) {
                    val intent = Intent(activity!!, TwillioLoadingActivity::class.java)
                    intent.putExtra(getString(R.string.request_id_key), notifications.params!!.id)
                    intent.putExtra(getString(R.string.twillio_token_key), notifications.params!!.token)
                    startActivity(intent)
                }
            }
            SEARCH -> {
                if(activity!=null) {
                    Navigator.navigate(activity!!, arrayOf(getString(R.string.screenType),
                            getString(R.string.query)), arrayOf("online_consulting", ""), SearchResultsActivity::class.java, HomeSearchActivity.SEARCH_REQUEST)
                }
            }

        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        const val PAYMENT = "payment"
        const val CANCEL = "cancel"
        const val REVIEW = "review"
        const val CONSULTING = "consulting_room"
        const val SEARCH = "search"
        const val CONSULTING_NOW = "consult_now"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                NotificationsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
