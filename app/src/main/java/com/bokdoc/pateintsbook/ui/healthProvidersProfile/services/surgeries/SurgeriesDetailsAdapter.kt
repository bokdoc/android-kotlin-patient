package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.surgeries

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.data.models.healthProviders.services.ISurgeriesSections
import com.bokdoc.pateintsbook.data.models.healthProviders.services.SurgeriesSections
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation.AccommodationAdapter
import com.bokdoc.pateintsbook.utils.constants.SocialConstants
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import kotlinx.android.synthetic.main.surgeries_header.view.*
import kotlinx.android.synthetic.main.surgeries_children.view.*

class SurgeriesDetailsAdapter(context: Context, var sectionItemList: List<SurgeriesSections>, var selectedListener:
(view: View)->Unit) :
        SectionRecyclerViewAdapter<SurgeriesSections, String,
                SurgeriesDetailsAdapter.SectionViewHolder, SurgeriesDetailsAdapter.ChildViewHolder>(context,sectionItemList) {

    override fun onCreateSectionViewHolder(viewGroup: ViewGroup?, p1: Int): SectionViewHolder {
        return SectionViewHolder(viewGroup?.inflateView(R.layout.surgeries_header)!!)
    }

    override fun onBindSectionViewHolder(sectionViewHolder: SectionViewHolder?, position: Int, accommodationSections: SurgeriesSections?) {
        sectionViewHolder?.view?.header?.text = accommodationSections?.title
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?, position: Int): ChildViewHolder {
        return ChildViewHolder(viewGroup?.inflateView(R.layout.surgeries_children)!!)
    }

    override fun onBindChildViewHolder(viewHolder: ChildViewHolder?, sectionPosition: Int, childPosition: Int, resource: String?) {

        if(sectionItemList[sectionPosition].sectionType == ISurgeriesSections.TEXT_SECTION){
            viewHolder?.view?.text?.visibility = View.VISIBLE
            viewHolder?.view?.text?.text = resource
            viewHolder?.view?.locationImage?.visibility = View.GONE
            viewHolder?.view?.arrow?.visibility = View.GONE
            viewHolder?.view?.button?.visibility = View.GONE
        }else
            if(sectionItemList[sectionPosition].sectionType == ISurgeriesSections.LOCATION_SECTION){
                viewHolder?.view?.text?.visibility = View.VISIBLE
                viewHolder?.view?.text?.text = resource
                viewHolder?.view?.locationImage?.visibility = View.VISIBLE
                viewHolder?.view?.arrow?.visibility = View.GONE
                viewHolder?.view?.button?.visibility = View.GONE
            }
        else
            if(sectionItemList[sectionPosition].sectionType == ISurgeriesSections.PHOTOS_VIDEOS_SECTION){
                viewHolder?.view?.text?.visibility = View.VISIBLE
                viewHolder?.view?.text?.text = resource
                viewHolder?.view?.locationImage?.visibility = View.GONE
                viewHolder?.view?.arrow?.visibility = View.VISIBLE
                viewHolder?.view?.button?.visibility = View.GONE
            }
            else
                if(sectionItemList[sectionPosition].sectionType == ISurgeriesSections.NEXT_AVAILABILITY_SECTION){
                    viewHolder?.view?.locationImage?.visibility = View.GONE
                    viewHolder?.view?.arrow?.visibility = View.GONE
                    viewHolder?.view?.text?.visibility = View.GONE
                    viewHolder?.view?.button?.visibility = View.VISIBLE
                    viewHolder?.view?.button?.text = resource
                }
    }
    class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    class ChildViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}