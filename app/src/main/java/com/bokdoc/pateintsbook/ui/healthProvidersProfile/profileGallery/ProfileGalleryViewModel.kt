package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileGallery.ProfileGalleryRepository

class ProfileGalleryViewModel(val profileGalleryRepository: ProfileGalleryRepository):ViewModel() {
    val progressLiveDate = MutableLiveData<Int>()
    val errorMessageLiveData = MutableLiveData<String>()
    val galleryLiveData = MutableLiveData<List<MediaParcelable>>()


    fun getMedia(id:String){
        progressLiveDate.value = VISIBLE
        profileGalleryRepository.getGallery(id).observeForever {
            if(it?.data!=null){
                galleryLiveData.value = it.data
            }else{
                errorMessageLiveData.value = it?.message
            }
            progressLiveDate.value = GONE
        }
    }
}