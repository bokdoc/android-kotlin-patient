package com.bokdoc.pateintsbook.ui.patientProfile

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bumptech.glide.Glide

class PatientMediaAdapter(val media:List<MediaParcelable>,val onClickListener:(View,Int)->Unit)
    : RecyclerView.Adapter<PatientMediaAdapter.MediaViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MediaViewHolder {
        return MediaViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.patient_media_image,
                viewGroup, false))
    }

    override fun getItemCount(): Int {
        return media.size
    }

    override fun onBindViewHolder(viewHolder: MediaViewHolder, position: Int) {
        media[position].let {
            Log.i("url",it.url)
            Glide.with(viewHolder.itemView).load(it.url).into(viewHolder.itemView as ImageView)
        }
    }


    inner class MediaViewHolder(view: View) : RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                onClickListener.invoke(view,adapterPosition)
            }
        }
    }
}