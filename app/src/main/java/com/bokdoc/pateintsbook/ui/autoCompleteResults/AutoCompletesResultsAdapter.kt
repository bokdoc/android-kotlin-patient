package com.bokdoc.pateintsbook.ui.autoCompleteResults

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.AutoCompleteValue
import com.bumptech.glide.Glide
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter


class AutoCompletesResultsAdapter(context: Context, var sectionItemList: ArrayList<AutoCompleteSectionHeader>, var textSelectedListener:
(text: String, params: String) -> Unit, var profileSelectedListener: (id: String, profileType: Int) -> Unit) :
        SectionRecyclerViewAdapter<AutoCompleteSectionHeader, AutoCompleteValue,
                AutoCompletesResultsAdapter.SectionViewHolder, AutoCompletesResultsAdapter.ChildViewHolder>(context, sectionItemList) {


    private var filteredSectionItemList: ArrayList<AutoCompleteSectionHeader> = sectionItemList


    override fun onCreateSectionViewHolder(p0: ViewGroup?, p1: Int): SectionViewHolder {
        val view = LayoutInflater.from(p0?.context).inflate(R.layout.auto_complete_result_title, p0, false)
        return SectionViewHolder(view)
    }

    override fun onBindSectionViewHolder(sectionViewHolder: SectionViewHolder?, position: Int, p2: AutoCompleteSectionHeader?) {
      //  sectionViewHolder?.titleTextView?.text = filteredSectionItemList[position].sectionText
        sectionViewHolder?.view?.visibility = View.GONE
        sectionViewHolder?.titleTextView?.visibility = View.GONE
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?, position: Int): ChildViewHolder {
        val view = LayoutInflater.from(viewGroup?.context).inflate(R.layout.auto_complete_result_text_image, viewGroup, false)
        return ChildViewHolder(view)
    }

    override fun onBindChildViewHolder(viewHolder: ChildViewHolder?, sectionPosition: Int, childPosition: Int, resource: AutoCompleteValue?) {
        if (resource!!.profileId.isNotEmpty()) {
            viewHolder?.text?.text = resource.text
            viewHolder?.image?.visibility = VISIBLE
            viewHolder!!.view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            if (resource.picture.isNotEmpty()) {
                Glide.with(viewHolder?.view?.context!!).load(resource?.picture).into(viewHolder.image)
            } else {
                viewHolder?.image?.visibility = GONE
            }
            viewHolder?.view?.setOnClickListener {
                profileSelectedListener.invoke(resource.profileId, resource.profileType)
            }
        } else {
            viewHolder?.image?.visibility = GONE
            viewHolder?.text?.text = resource?.text
            viewHolder?.view?.setOnClickListener {
                textSelectedListener.invoke(resource.text, resource.params)
            }
        }


    }


    fun filter(text: String) {
        if (text.trim().isEmpty()) {
            filteredSectionItemList = sectionItemList
        } else {
            filteredSectionItemList = ArrayList()
            sectionItemList.forEach {
                it.childList.forEach {
                    if (it.text.contains(text, true)) {
                        filteredSectionItemList.add(AutoCompleteSectionHeader(listOf(it),""))
                    }
                }

            }

        }
        notifyDataChanged(filteredSectionItemList)
    }

    fun updateList(newSectionItemList: ArrayList<AutoCompleteSectionHeader>) {
        sectionItemList = newSectionItemList
        notifyDataChanged(newSectionItemList)
    }

    class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView = view.findViewById<TextView>(R.id.tv_title)
        val collapseArrow = view.findViewById<ImageView>(R.id.collapseArrow)
    }

    class ChildViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val text = view.findViewById<TextView>(R.id.text)
        val image = view.findViewById<ImageView>(R.id.image)
        val line = view.findViewById<View>(R.id.line)
    }


}