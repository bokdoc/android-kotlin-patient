package com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.qualifications.QualificationsRepository

class QualificationsViewModelFactory(private val qualificationsRepository: QualificationsRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return QualificationsDetailsViewModel(qualificationsRepository) as T
    }
}