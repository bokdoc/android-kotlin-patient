package com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.doctor.Doctor
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.doctors.DoctorsRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DoctorRow

class DoctorsViewModel(private val doctorsRepository: DoctorsRepository) : ViewModel() {
    val progressLiveData = MutableLiveData<Int>()
    lateinit var id: String
    var serviceType: HashMap<String, String>? = HashMap()
    var isDepartment: Boolean = false
    var meta: Meta = Meta()
    var currentPage: Int = 1

    fun getAlldoctors(): LiveData<DataResource<DoctorRow>> {
        serviceType!!.put("page", currentPage.toString())
        return doctorsRepository.getAllDoctors(id, serviceType!!, isDepartment)
    }
}