package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.tests

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesViewModel
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_services.*
import kotlinx.android.synthetic.main.toolbar.*

class TestsActivity : AppCompatActivity() {

    private lateinit var servicesViewModel: ServicesViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tests)
        titleText.text = getString(R.string.tests)
        returnArrow.visibility = View.VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }

        servicesList.layoutManager = LinearLayoutManager(this)

        servicesViewModel = InjectionUtil.getServicesViewModel(this, Service.TEST_TYPE)

        servicesViewModel.getServices(intent?.getStringExtra(getString(R.string.idKey))!!)

        servicesViewModel.progress.observe(this, Observer {
            progress.visibility = it!!
        })

        servicesViewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this,it!!, Toast.LENGTH_LONG).show()
        })

        servicesViewModel.servicesList.observe(this, Observer {
            servicesList.adapter = ServicesAdapter(it!!)
        })
    }
}
