package com.bokdoc.pateintsbook.ui.search

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsViewModel

class SearchProvider(private val context: Context):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchViewModel(context) as T
    }
}