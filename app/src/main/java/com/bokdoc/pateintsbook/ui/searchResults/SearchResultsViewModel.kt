package com.bokdoc.pateintsbook.ui.searchResults

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.bookAppointement.HealthProvidersUIBookInfoImp
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.repositories.SearchResults.SearchResultsRepository
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.webservice.models.Fees
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.common.profileActions.ProfileActionsViewModel
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes

class SearchResultsViewModel(private val profileActionsRepository: ProfileSocialActionsRepository,
                             context: Context) : ProfileActionsViewModel(profileActionsRepository) {

    private val searchResultsRepository = SearchResultsRepository(context)
    // inject from outside
    var searchType: String = ""
    private var screenTitle: String? = null
    var query: String = ""
    var queryMap: HashMap<String, String> = HashMap()
    var filtersQueries = Array(0) { "" }
    var profileSelectedPosition: Int = -1

    var profileIdShare: String = ""
    var profileTypeShare: Int = 0
    var isResetAdapter = false
    var profilesResultsLiveData = MutableLiveData<DataResource<Profile>>()

    var ignoredKeysNumber = 0

    fun search(page: Int = 1) {
        //check if passed query is not empty add its value to ignored query key
        if (query.isNotEmpty() && query != "0")
            queryMap[SearchResultsRepository.IGNORED_QUERY_KEY] = query

        return when (searchType) {
            SearchScreenTypes.APPOINTMENTS -> searchResultsRepository.searchAppointment(queryMap, page).observeForever {
                profilesResultsLiveData.value = it
            }
            SearchScreenTypes.SURGERIES -> searchResultsRepository.searchSurgeries(queryMap, page).observeForever {
                profilesResultsLiveData.value = it
            }
            else -> searchResultsRepository.searchConsulting(queryMap, page).observeForever {
                profilesResultsLiveData.value = it
            }
        }
    }


    fun getScreenTitle(context: Context): String {
        if (screenTitle == null) {
            when (searchType) {
                SearchScreenTypes.APPOINTMENTS -> screenTitle = context.getString(R.string.appointment)
                SearchScreenTypes.SURGERIES -> screenTitle = context.getString(R.string.surgeries)
                SearchScreenTypes.CONSULTING -> screenTitle = context.getString(R.string.consulting)
            }
        }
        return screenTitle!!
    }

    fun getServiceType(context: Context): String {
        when (searchType) {
            SearchScreenTypes.APPOINTMENTS -> return context.getString(R.string.appointementBookKey)
            SearchScreenTypes.SURGERIES -> return context.getString(R.string.surgeryType)
            SearchScreenTypes.CONSULTING -> return context.getString(R.string.consultingType)
        }
        return ""
    }

    fun getHealthProviderProfile(profile: Profile, isLoadNextAvailabilities: Boolean = false): HealthProvidersProfile {
        return searchResultsRepository.getHealthProvidersProfile(profile, isLoadNextAvailabilities)
    }

    fun getHealthProviderBookInfo(profile: Profile): HealthProvidersUIBookInfoImp {
        return searchResultsRepository.getHealthProvidersBookInfo(profile)
    }

    fun getArrayList(profiles: List<Profile>): ArrayList<Profile> {
        val profilesArray = ArrayList<Profile>()
        val size = profiles.size
        (0 until size).forEach {
            val profile = Profile()
            profile.picture = profiles[it].picture
            profile.name = profiles[it].name
            val fees = Fees()
            fees.feesAverage = profiles[it].fees.feesAverage
            fees.currencySymbols = profiles[it].fees.currencySymbols
            profile.fees = fees
            profile.rating = profiles[it].rating
            profile.location = profiles[it].location
            profile.bookButton = profiles[it].bookButton
            profile.bookButtons = profiles[it].bookButtons
            profile.lat = profiles[it].lat
            profile.profileTypeId = profiles[it].profileTypeId
            profile.lng = profiles[it].lng
            if (profiles[it].mainSpeciality != null)
                profile.mainSpeciality = profiles[it].mainSpeciality
            profilesArray.add(profile)
        }
        return profilesArray
    }

}