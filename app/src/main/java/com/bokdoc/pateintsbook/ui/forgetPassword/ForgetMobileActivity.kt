package com.bokdoc.pateintsbook.ui.forgetPassword

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_add_mobile.*


class ForgetMobileActivity : ParentActivity() {

    lateinit var forgetPasswordViewModel: ForgetPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_mobile)
        enableBackButton()
        title = getString(R.string.enter_mobile_num)
        getViewModel()
        //et_mobile_num.setDefaultCountry("EG")
        et_mobile_num.setHint(R.string.mobile)
        btn_save.setOnClickListener(this)
        phoneNumberEdit.hint = ColoredTextSpannable.getSpannable(getString(R.string.mobile_number_with_star),getString(R.string.star), ContextCompat.getColor(this,R.color.colorErrorRed))
    }


    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_save.id -> {
                when (phoneNumberEdit.text.isNullOrEmpty()) {
                    false -> {
                        forgetPasswordViewModel.forgetPassword.mobileNumber = phoneNumberEdit.text.toString()
                        sendNumber()
                    }
                    true -> {
                        //ti_mobile_num.error = getString(R.string.enter_mobile_num)
                        Utils.showLongToast(this,getString(R.string.enter_mobile_num))
                    }
                }
            }
        }
    }

    private fun sendNumber() {
        switchSendingProgressVisibility(View.VISIBLE)
        forgetPasswordViewModel.forgetMobile().observe(this, Observer {
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    switchSendingProgressVisibility(View.GONE)
                    Navigator.navigate(this, getString(R.string.forgetPassword), forgetPasswordViewModel.forgetPassword, VerificationForgetActivity::class.java, 1)
                }
                DataResource.FAIL -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wromg))
                    switchSendingProgressVisibility(View.GONE)
                }
                DataResource.VALIDATION_ERROR -> {
                    Utils.showShortToast(this, it.errors[0].detail)
                    switchSendingProgressVisibility(View.GONE)
                }
            }
        })

    }

    private fun getViewModel() {
        forgetPasswordViewModel = InjectionUtil.getForgetPasswordViewModel(this)
    }
}