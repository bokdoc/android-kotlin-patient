package com.bokdoc.pateintsbook.ui.updateProfile

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.User
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaViewModel
import com.bokdoc.pateintsbook.ui.imagesViewer.ImagesViewerActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.ImagesUtils
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.ui.PercentageDialog
import com.bokdoc.pateintsbook.utils.ui.ProgressBarDialog
import com.bokdoc.pateintsbook.utils.validate.Validator
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_update_profile_activity.*
import kotlinx.android.synthetic.main.update_profile_image_bottom_sheet.view.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class UpdateProfileActivity : ParentActivity() {

    lateinit var updateProfileViewModel: UpdateProfileViewModel
    var cal = Calendar.getInstance()
    private var genderDialog: PercentageDialog? = null
    private var mBottomSheetDialog: Dialog? = null
    private lateinit var attachmentMediaViewModel: AttachmentMediaViewModel
    private var progressBarDialog: ProgressBarDialog? = null
    private val dateTimeDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile_activity)
        enableBackButton()
        title = getString(R.string.update_profile)
        getViewModel()
        editProfileImage.setOnClickListener(this)
        btn_update.setOnClickListener(this)
        setupAttachmentViewModel()
       // setupHintTextColors()
        setupEditTextsWatchers()
        setupEditTouchListener()
        bindUserData()
    }

    private fun setupAttachmentViewModel() {
        attachmentMediaViewModel = InjectionUtil.getAttachmentMediaViewModel(this)

        attachmentMediaViewModel.uploadProgressLiveData.observe(this, android.arch.lifecycle.Observer {
            progressBarDialog!!.onProgress(it!!)
        })
        attachmentMediaViewModel.dataLiveData.observe(this, android.arch.lifecycle.Observer {
            progressBarDialog!!.dismiss()
            if (it != null && it.isNotEmpty()) {
                Utils.showLongToast(this, R.string.message_success_upload)
            }
            mBottomSheetDialog?.dismiss()
            updateProfileViewModel.user.picture = Picture().apply {
                if(it!=null && it.isNotEmpty()){
                    mediaId = it[0].mediaId
                    url = it[0].url
                    status = it[0].status
                }
            }
            Glide.with(this).load(updateProfileViewModel.user.picture.url).into(patientImage)
        })
    }


    /*
    private fun setupHintTextColors() {
        if (activity == null)
            return

        fullNameEdit.hint = ColoredTextSpannable.getSpannable(getString(R.string.fullNameWithStar), getString(R.string.star),
                ContextCompat.getColor(activity?.applicationContext!!, R.color.colorErrorRed))

        fullNameEdit.setSelection(fullNameEdit.text.length)



        phoneNumberEdit.hint = ColoredTextSpannable.getSpannable(getString(R.string.mobile_number_with_star), getString(R.string.star),
                ContextCompat.getColor(activity?.applicationContext!!, R.color.colorErrorRed))


    }
    */


    private fun bindUserData() {
        updateProfileViewModel.user.let {
            if (it.email.isNotEmpty())
                emailEdit.setText(it.email)

            if (it.name.isNotEmpty())
                fullNameEdit.setText(it.name)

            if (it.phone.isNotEmpty())
                phoneNumberEdit.setText(it.phone)

            if (it.birthDate.isNotEmpty())
                birthDateEdit.setText(it.birthDate)

            if (it.gender.isNotEmpty())
                gender.setText(it.gender)

            if (it.birthDate.isNotEmpty())
                birthDateEdit.setText(it.birthDate)

            if (it.picture.url.isNotEmpty())
                Glide.with(this).load(it.picture.url).into(patientImage)

        }
    }

    private fun setupEditTouchListener() {
        gender.setOnTouchListener { view, motionEvent ->
            showGenderPopUp()
            true
        }
        birthDateEdit.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                showDatePicker()
            }
            true

        }
    }

    private fun validate(): Boolean {
        var isValid = true

        if (fullNameEdit.text.isEmpty()) {
            isValid = false
            //passwordEdit.error = getString(R.string.errorFillField)
            fullNameEdit.setHintTextColor(ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))
        }

        phoneNumberEdit?.text?.isEmpty()?.let {
            if (it) {
                isValid = false
                phoneNumberEdit.setHintTextColor(ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))
            }
        }

        return isValid
    }

    private fun showGenderPopUp() {
        if (genderDialog == null) {
            genderDialog = PercentageDialog(this)
            genderDialog?.selectedValues = User.GENDER
            genderDialog?.callback = object : PercentageDialog.OnDialogClickedListener {
                override fun onDialogClicked(position: Int, name: String) {
                    updateProfileViewModel.user.gender = name
                    gender.setText(name)
                }
            }
        }
        genderDialog?.selectedValue = updateProfileViewModel.user.gender
        genderDialog!!.show()
    }

    private fun showDatePicker() {

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "yyyy-MM-dd" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            updateProfileViewModel.user.birthDate = sdf.format(cal.time)
            birthDateEdit.setText(updateProfileViewModel.user.birthDate)
            updateProfileViewModel.isDataTimeDialogShow = false
        }

        when (updateProfileViewModel.user.birthDate.isNotEmpty()) {
            false -> {
                DatePickerDialog(this, dateSetListener,
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show()
            }
            true -> {
                val datesParts = updateProfileViewModel.user.birthDate.split(StringsConstants.DASH)
                DatePickerDialog(this, dateSetListener, datesParts[0].toInt(), datesParts[1].toInt() - 1,
                        datesParts[2].toInt()).show()
            }
        }
    }

    private fun setupEditTextsWatchers() {
        fullNameEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fullNameEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
               // setupHintTextColors()
            }

        })

        phoneNumberEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneNumberEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                //setupHintTextColors()
            }
        })
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_update.id -> {
                if (validate()) {
                    switchBgProgressVisibility(VISIBLE)
                    updateProfileViewModel.user.name = fullNameEdit.text.toString()
                    updateProfileViewModel.user.email = emailEdit.text.toString()
                    updateProfileViewModel.user.phone = phoneNumberEdit.text.toString()
                    updateProfileViewModel.update(this).observe(this, Observer {
                        switchBgProgressVisibility(GONE)
                        it?.errors?.let {
                            if (it.isNotEmpty())
                                Utils.showLongToast(applicationContext, it[0].detail)
                        }
                        if (it?.status == DataResource.SUCCESS) {
                            Navigator.navigateBack(this)
                        } else
                            if (it?.status == DataResource.INTERNAL_SERVER_ERROR) {
                                Utils.showLongToast(applicationContext, R.string.server_error)
                            }
                    })
                }
            }
            editProfileImage.id -> {
                showSheet(editProfileImage)
            }
        }
    }

    private fun getViewModel() {
        updateProfileViewModel = InjectionUtil.getUpdateProfileViewModel(this)

    }

    fun showSheet(view: View) {
        val view = layoutInflater.inflate(R.layout.update_profile_image_bottom_sheet, null)

        mBottomSheetDialog = Dialog(this, R.style.MaterialDialogSheet)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()


        view.tv_sheet_view_current.setOnClickListener {
            updateProfileViewModel.user.picture.let {
                if (it.url.isNotEmpty()) {
                    Navigator.navigate(this@UpdateProfileActivity, arrayOf(getString(R.string.selected_position)),
                            arrayOf(0.toString()), getString(R.string.images), arrayListOf(Uri.parse(it.url)),
                            ImagesViewerActivity::class.java, -1)
                }
            }
        }


        view.tv_sheet_select_image.setOnClickListener {
            pickFileFromStorage()
        }


        view.tv_sheet_open_camera.setOnClickListener {
            val snackPermissionDenied =
                    SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(update_profile_parent,
                            R.string.open_camera)
                            .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                            .build()


            val permissionListeners = object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    val images = ArrayList<Image>()
                    val paths = ArrayList<String>()
                    if (report!!.areAllPermissionsGranted()) {
                        try {
                            updateProfileViewModel.cameraIntentPath = ImagesUtils.createImageFile(applicationContext)
                        } catch (ex: IOException) {
                            // Error occurred while creating the File
                            Utils.showLongToast(applicationContext, R.string.error_creating_file)
                        }

                        if (updateProfileViewModel.cameraIntentPath != null) {
                            /*
                            val photoURI = FileProvider.getUriForFile(this@UpdateProfileActivity, packageName, updateProfileViewModel.cameraIntentPath!!)
                            val pictureIntent = Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE)
                            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    photoURI)
                            startActivityForResult(pictureIntent,
                                    CAMERA_REQUEST)
                                    */
                            val photoURI: Uri
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                                photoURI = Uri.fromFile(updateProfileViewModel.cameraIntentPath)
                            } else {
                                photoURI = FileProvider.getUriForFile(this@UpdateProfileActivity, packageName, updateProfileViewModel.cameraIntentPath!!)
                            }

                            val pictureIntent = Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE)
                            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    photoURI)
                            startActivityForResult(pictureIntent,
                                    CAMERA_REQUEST)
                            /*
                        val pictureIntent = Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(pictureIntent,
                                CAMERA_REQUEST)
                                */
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                    showPermissionRationaleDialog(token!!)
                }
            }

            val compositeListener = CompositeMultiplePermissionsListener(permissionListeners, snackPermissionDenied)

            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(compositeListener).check()


        }
    }

    private fun showPermissionRationaleDialog(token: PermissionToken) {
        AlertDialog.Builder(this)
                .setTitle(R.string.storage_permission_title)
                .setMessage(R.string.storage_rationale_message)
                .setPositiveButton(android.R.string.ok) { dialog, p1 ->
                    dialog?.dismiss()
                    token.continuePermissionRequest()
                }
                .setNegativeButton(R.string.cancel) { dialog, p1 ->
                    dialog?.dismiss()
                    token.cancelPermissionRequest()
                }
                .setOnDismissListener {
                    it?.dismiss()
                    token.cancelPermissionRequest()
                }.show()
    }


    private fun pickFileFromStorage() {

        val snackPermissionDenied =
                SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(update_profile_parent,
                        R.string.storage_permissions_denied_feedback)
                        .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                        .build()

        val permissionListeners = object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                val images = ArrayList<Image>()
                val paths = ArrayList<String>()
                if (report!=null && report.areAllPermissionsGranted()) {
                    images.addAll(updateProfileViewModel.profileImages)
                    pickFile(0, images, paths)
                }
            }

            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                token?.let {
                    showPermissionRationaleDialog(it)
                }
            }
        }

        val compositeListener = CompositeMultiplePermissionsListener(permissionListeners, snackPermissionDenied)

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(compositeListener).check()

    }


    fun pickFile(fileType: Int, selectedImages: List<Image>, selectedPaths: List<String> = ArrayList(), requestCode: Int = -1) {

        ImagePicker.create(this)
                .returnMode(ReturnMode.NONE) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .multi() // multi mode (default mode)
                .single()
                .origin(selectedImages as ArrayList<Image>)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .enableLog(false) // disabling log
                .start() // start image picker activity with request code
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images = ImagePicker.getImages(data)
            if(!images.isNullOrEmpty()){
                updateProfileViewModel.profileImages = images

                progressBarDialog = ProgressBarDialog(this, {
                    attachmentMediaViewModel.cancelUpload()
                }, {
                    attachmentMediaViewModel.uploadFile(images[0].path)
                })
                progressBarDialog?.setCanceledOnTouchOutside(false)
                progressBarDialog?.setCancelable(false)
                progressBarDialog?.show()
                attachmentMediaViewModel.uploadFile(ImagesUtils.getCompressed(this, images[0].path))
            }

        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            galleryAddPic()
            openCameraAndSelectFile()
        }

    }

    private fun openCameraAndSelectFile() {
        //val photo = image as Bitmap
        // val tempUri = getImageUri(applicationContext, photo)
        progressBarDialog = ProgressBarDialog(this, {
            attachmentMediaViewModel.cancelUpload()
        }, {
            attachmentMediaViewModel.uploadFile(ImagesUtils.getCompressed(this, updateProfileViewModel.cameraIntentPath?.absolutePath!!))
            //attachmentMediaViewModel.uploadFile(ImagesUtils.getCompressed(applicationContext,getRealPathFromURI(tempUri)))
        })
        progressBarDialog?.setCanceledOnTouchOutside(false)
        progressBarDialog?.setCancelable(false)
        progressBarDialog?.show()
        attachmentMediaViewModel.uploadFile(ImagesUtils.getCompressed(this, updateProfileViewModel.cameraIntentPath?.absolutePath!!))
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            mediaScanIntent.data = Uri.fromFile(updateProfileViewModel.cameraIntentPath)
            sendBroadcast(mediaScanIntent)
        }
    }


    companion object {
        const val CAMERA_REQUEST = 1
    }
}