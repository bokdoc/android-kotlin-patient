package com.bokdoc.pateintsbook.ui.healthProvidersProfile


import android.arch.lifecycle.LiveData
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.ProvidersProfileRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.OverviewRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.common.profileActions.ProfileActionsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewRepository

class HealthProvidersViewModel(val healthProvidersRepository: ProvidersProfileRepository
                               , private val profileSocialActionsRepository: ProfileSocialActionsRepository
                               , val overViewRepository: OverViewRepository) :
        ProfileActionsViewModel(profileSocialActionsRepository) {


    var profileId: String = ""

    var isProfileLoaded = true

    var overviewRow = OverviewRow()

    fun getProfileOverView(profileId: String, services: String): LiveData<DataResource<OverviewRow>> {
        return overViewRepository.getOverView(profileId, services)
    }

    fun getProfileAddress(): LiveData<DataResource<Location>> {
        return overViewRepository.getAddress(profileId)
    }


    fun getSpokenLanguage(language: List<String>): String {
        var languages: String = ""
        language.forEach {
            languages += "$it,"
        }
        return languages
    }

    fun getPaymentMethods(payments: List<String>): String {
        var payment: String = ""
        payments.forEach {
            payment += "$it,"
        }
        return payment
    }

    fun hasOverview(overviewRow: OverviewRow): Boolean {
        var isValid = false
        if (getSpokenLanguage(overviewRow.spokenLanguage).isNotEmpty() || getPaymentMethods(overviewRow.paymentOptions).isNotEmpty()
                || overviewRow.bedsNumber != "0" || overviewRow.ambulanceCount != "0") {
            isValid = true
        }
        return isValid
    }

    fun hasAccomedations(overviewRow: OverviewRow): Boolean {
        var isValid = false
        if (overviewRow.accommodations.patientRoom.isNotEmpty() || overviewRow.accommodations.accompaniedPersons.isNotEmpty()
                || overviewRow.accommodations.meals.isNotEmpty() || overviewRow.accommodations.rooms.isNotEmpty()) {
            isValid = true
        }
        return isValid
    }

    fun getProfileActionsMenu(context: Context, profile: HealthProvidersProfile): List<ImageTextRow> {
        val imagesTextsList = ArrayList<ImageTextRow>()
        profile.isLiked.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_like_selected, context.getString(R.string.like)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_like, context.getString(R.string.like)))
            }
        }
        profile.isFavourite.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_favorite_selected, context.getString(R.string.favorite)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_favorite, context.getString(R.string.favorite)))
            }
        }

        /*
        profile.isCompare.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_compare_selected, context.getString(R.string.compare)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_compare, context.getString(R.string.compare)))
            }
        }
        */

        imagesTextsList.add(ImageTextRow(R.drawable.ic_share, context.getString(R.string.share)))
        return imagesTextsList
    }


}