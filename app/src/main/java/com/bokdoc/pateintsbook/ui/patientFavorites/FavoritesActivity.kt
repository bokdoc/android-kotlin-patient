package com.bokdoc.pateintsbook.ui.patientFavorites

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.ProfileActionsSheetAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments.DepartmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsActivity
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.actitivty_my_favorites.*
import kotlinx.android.synthetic.main.sheet_profile_actions.view.*

class FavoritesActivity : ParentActivity() {

    lateinit var favoritesViewModel: FavoritesViewModel
    var favoritesAdapter: FavoritesAdapter? = null
    private var mBottomSheetDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actitivty_my_favorites)
        enableBackButton()
        title = getString(R.string.my_favourites)

        getViewModel()

        when (hasInternetConnection()) {
            false -> showError(getString(R.string.no_connection), lay_container)
            true -> getData()
        }

    }


    private fun setupPagination() {
        rv_my_favorites.addOnScrollListener(object : PaginationScrollListener(rv_my_favorites.layoutManager as LinearLayoutManager,
                favoritesViewModel.pagination) {
            override fun isLastPage(): Boolean {
                return true
            }

            override fun isLoading(): Boolean {
                return false
            }

            override fun loadMoreItems() {
                favoritesAdapter?.addLoadingFooter()
                favoritesViewModel.currentPage += 1
                favoritesViewModel.getFavorites()
            }

        })
    }


    private fun getData() {

        favoritesViewModel.getFavorites()

        favoritesViewModel.errorMessageLiveData.observe(this, Observer {
            if (it != null) {
                showError(it, lay_container)
            } else {
                showError(getString(R.string.some_thing_went_wromg), lay_container)
            }
        })

        favoritesViewModel.progressLiveData.observe(this, Observer {
            when (it) {
                false -> progress.visibility = View.GONE
                true -> progress.visibility = View.VISIBLE
            }
        })

        favoritesViewModel.favoritesLiveData.observe(this, Observer {
            if (it == null)
                return@Observer

            when (it.size) {
                0 -> showEmpty(getString(R.string.no_fav), "", null)
                else -> {
                    if (favoritesAdapter == null) {
                        favoritesAdapter = FavoritesAdapter(it, this::onAdapterCLickListener)
                        rv_my_favorites.adapter = favoritesAdapter
                        setupPagination()
                    } else {
                        favoritesAdapter?.addAll(it as MutableList<Profile>)
                        favoritesAdapter?.removeLoadingFooter()
                    }
                }
            }
        })
    }

    private fun getViewModel() {
        favoritesViewModel = InjectionUtil.getFavoriteViewModel(this)
    }

    private fun onAdapterCLickListener(view: View, profile: Profile, selectedPosition: Int) {
        when (view.id) {
            R.id.profileImage -> Navigator.navigate(this, getString(R.string.profileKey),
                    arrayListOf(favoritesViewModel.getHealthProviderProfile(profile)),
                    HealthProvidersProfileActivity::class.java,BOOK_REQUEST)

            R.id.viewMore -> {
                favoritesViewModel.setProfile(profile)
                /*
                val popupMenu = CustomListPopupMenu(this, view, searchViewModel.getSocialMenuRows(this)
                        as ArrayList<ImageTextRow>)
                popupMenu.selectedListener = this::onMenuItemSelected
                popupMenu.show()
                */
                showSheet()
            }
            R.id.bookAppointment -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            ""),
                            BookAppointmentsActivity::class.java,BOOK_REQUEST)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl,""),
                            LinksCardsDetailsActivity::class.java,BOOK_REQUEST)
                }


                /*
                Navigator.navigate(this, view.context?.getString(R.string.profileKey)!!,
                        arrayListOf(searchViewModel.getHealthProviderBookInfo(profile)),
                        arrayOf(getString(R.string.isSurgriesBookKey), getString(R.string.bookTypeKey)),
                        arrayOf(searchViewModel.searchType == SearchScreenTypes.SURGERIES, getString(R.string.appointementBookKey)),
                        BookAppointmentsActivity::class.java)
                        */
            }
            R.id.rv_card_links, R.id.rv_card_buttons -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                            arrayOf(tag.endpointUrl),
                            BookAppointmentsActivity::class.java,BOOK_REQUEST)
                } else if (tag.targetScreen == DynamicLinks.KEY_DEPARTMENTS) {
                    Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                            arrayOf(profile.id, getString(R.string.doctors_include), profile.name, profile.picture.url),
                            DepartmentsActivity::class.java,BOOK_REQUEST)

                } else if (tag.targetScreen == DynamicLinks.KEY_DOCTORS) {
                    Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                            arrayOf(profile.id, getString(R.string.doctors_include), profile.name,
                                    profile.picture.url), DoctorsActivity::class.java,BOOK_REQUEST)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug), getString(R.string.profileKey)),
                            arrayOf(tag.endpointUrl, profile.name, profile.picture.url, profile.profileTypeId.toString()),
                            LinksCardsDetailsActivity::class.java,BOOK_REQUEST)
                }

            }
        }
    }


    private fun showSheet() {
        val view = layoutInflater.inflate(R.layout.sheet_profile_actions, null)
        view.rv_list.layoutManager = LinearLayoutManager(this)
        view.rv_list.adapter = ProfileActionsSheetAdapter(favoritesViewModel.getSocialMenuRows(this), this::onMenuItemSelected)

        mBottomSheetDialog = Dialog(this, R.style.MaterialDialogSheet)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()

    }

    private fun onMenuItemSelected(imageTextRow: ImageTextRow) {
        mBottomSheetDialog?.dismiss()
        when (imageTextRow.text) {
            getString(R.string.share) -> Navigator.share(this, favoritesViewModel.getSharableProfileUrl())
            getString(R.string.like) -> favoritesViewModel.toggleLike()
            getString(R.string.favorite) -> favoritesViewModel.toggleFavorite()
            getString(R.string.compare) -> favoritesViewModel.toggleCompare()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val PROFILE_DETAILS_REQUEST = 1
        const val BOOK_REQUEST = 2
    }
}