package com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments

import android.os.Bundle
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity

class DepartmentsMediaActivity : ParentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_media)
    }
}