package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.details

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.support.v4.content.ContextCompat
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.ClinicsSections
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.IClinicsParacable
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.IClinicsSections
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees
import com.bokdoc.pateintsbook.data.repositories.common.StringsRepository
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.PriceTextSpannable
import com.bokdoc.pateintsbook.utils.constants.StringsConstants

class ClinicsDetailsViewModel(private val context:Context,
                              private val stringsRepository: StringsRepository):ViewModel() {

    lateinit var clinic:IClinicsParacable

    fun getSections():List<IClinicsSections>{

        val surgeriesSections = ArrayList<ClinicsSections>()
        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.ClinicsRes.CLINICS_NAME), arrayListOf(clinic.name),
                IClinicsSections.TEXT_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.ClinicsRes.ABOUT_AS),arrayListOf(clinic.overview),
                IClinicsSections.TEXT_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.ClinicsRes.CLINIC_LOCATION),arrayListOf(clinic.location),
                IClinicsSections.LOCATION_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.HealthProvidersProfileRes.PHOTOS_VIDEOS),arrayListOf(
                stringsRepository.getStringRes(context, StringsRepository.HealthProvidersProfileRes.PHOTOS_VIDEOS)),
                IClinicsSections.PHOTOS_VIDEOS_SECTION))

        surgeriesSections.add(ClinicsSections(
                " ",arrayListOf(
                stringsRepository.getStringRes(context, StringsRepository.HealthProvidersProfileRes.NEXT_AVAILABILITY)),
                IClinicsSections.NEXT_AVAILABILITY_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.HealthProvidersProfileRes.FEES), arrayListOf(
                        getFeesStyled(clinic.fees)),IClinicsSections.FEES_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.HealthProvidersProfileRes.PAYMENT_METHODS), arrayListOf(
               " "),IClinicsSections.TEXT_SECTION))

        surgeriesSections.add(ClinicsSections(
                stringsRepository.getStringRes(context, StringsRepository.ClinicsRes.FREE_APPOINTMENTS), arrayListOf(
                clinic.appointmentsFreeCount.toString()),IClinicsSections.APPOINTEMENT_SECTION))

        return surgeriesSections
    }

    private fun getFeesStyled(fees:Fees):String{
        return StringsConstants.buildString(arrayListOf(fees.amount.toString()," ",fees.symbols))
    }
}
