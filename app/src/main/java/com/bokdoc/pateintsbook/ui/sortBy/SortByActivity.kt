package com.bokdoc.pateintsbook.ui.sortBy

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.register.SortbyModelFactory
import com.bokdoc.pateintsbook.ui.register.SortbyViewModel
import com.bokdoc.pateintsbook.utils.Utils
import kotlinx.android.synthetic.main.activity_sort_by.*
import kotlinx.android.synthetic.main.toolbar.*

class SortByActivity : ParentActivity() {

    lateinit var sortViewModel: SortbyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sort_by)
        title = getString(R.string.sortBy)
        returnArrow.setOnClickListener {
            val selectedParam = (sortList.adapter as SortbyAdapter).getSelectedParam()
            sortViewModel.saveSelected(selectedParam)
            Navigator.naviagteBack(this, arrayOf(SELECTED_PARAM), arrayOf(selectedParam))
        }
        sortList.layoutManager = LinearLayoutManager(this)

        sortViewModel = ViewModelProviders.of(this, SortbyModelFactory(this))
                .get(SortbyViewModel::class.java)

        when (Utils.hasConnection(this)) {
            true -> {
                getData()
            }
            false -> {
                Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
            }
        }

        done.setOnClickListener {
            val selectedParam = (sortList.adapter as SortbyAdapter).getSelectedParam()
            sortViewModel.saveSelected(selectedParam)
            Navigator.naviagteBack(this, arrayOf(SELECTED_PARAM), arrayOf(selectedParam))
        }

    }

    private fun getData() {
        progress.visibility = VISIBLE
        sortViewModel.getOptions(intent.getStringExtra(getString(R.string.screenType))).observe(this, Observer<List<TextWithParam>> {
            progress.visibility = GONE
            if (!it.isNullOrEmpty()) {
                sortList.adapter = SortbyAdapter(it!!, sortViewModel.getIndexOfSelected(it)) {
                    val selectedParam = (sortList.adapter as SortbyAdapter).getSelectedParam()
                    sortViewModel.saveSelected(selectedParam)
                    Navigator.naviagteBack(this, arrayOf(SELECTED_PARAM), arrayOf(selectedParam))
                }
            } else {
                progress.visibility = GONE
                Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
            }
        })

    }

    companion object {
        const val SELECTED_PARAM = "selectedParam"
    }
}
