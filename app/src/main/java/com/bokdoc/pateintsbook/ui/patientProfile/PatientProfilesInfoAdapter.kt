package com.bokdoc.pateintsbook.ui.patientProfile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.patientProfile.ProfilesInfo
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_profile_image_text.view.*
import java.text.FieldPosition

class PatientProfilesInfoAdapter(val profiles:List<HealthProvidersProfile>,val onCLickListener:(HealthProvidersProfile,Int)->Unit):
        RecyclerView.Adapter<PatientProfilesInfoAdapter.ProfilesViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ProfilesViewHolder {
        return ProfilesViewHolder(LayoutInflater.from(viewGroup.context).
                inflate(R.layout.item_profile_image_text,viewGroup,false))
    }

    override fun getItemCount(): Int {
        return profiles.size
    }

    override fun onBindViewHolder(viewHolder: ProfilesViewHolder, position: Int) {
       profiles[position].let {
           Glide.with(viewHolder.itemView).load(it.picture).into(viewHolder.itemView.image)
           viewHolder.itemView.name.text = it.name
       }
    }


    inner class ProfilesViewHolder(view:View): RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                onCLickListener.invoke(profiles[adapterPosition],adapterPosition)
            }
        }
    }


}