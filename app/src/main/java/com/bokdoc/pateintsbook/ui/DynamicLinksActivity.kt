package com.bokdoc.pateintsbook.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.webkit.*
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioLoadingActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import kotlinx.android.synthetic.main.activity_web_view.*


class DynamicLinksActivity : ParentActivity() {

    var document = "http://drive.google.com/viewerng/viewer?embedded=true&url="
    var paymentUrl: String = "https://payment.bokdoc.com/view-options/"
    var devPaymentUrl: String = "https://payment.dev.bokdoc.com"
    var url: String = ""
    var token: String = ""
    var requestId: String = ""
    var orderId: String = ""
    var isPayment: String = ""
    lateinit var webserviceManagerImpl: WebserviceManagerImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //getDataFromIntent()
        setContentView(R.layout.activity_web_view)
        enableBackButton()
        progress.visibility = VISIBLE
        var screenType = ""
        //LocaleManager.setLocale(baseContext)

        val uri = intent?.data
        if (uri != null) {

            try {
                screenType = uri.getQueryParameter(SCREEN_TYPE)
                if (screenType == SEARCH) {
                    progress.visibility = GONE
                    val searchType = uri.getQueryParameter(SEARCH_TYPE)
                    val params = uri.getQueryParameter(PARAMS)
                    if (searchType != null) {
                        Navigator.navigate(this, arrayOf(getString(R.string.screenType),
                                getString(R.string.query)), arrayOf(searchType, params), SearchResultsActivity::class.java)
                        finish()
                    }
                } else if (screenType == LIST_REQUESTS) {
                    Navigator.navigate(this, arrayOf(getString(R.string.requests_screen)),
                            arrayOf(getString(R.string.requests_screen)), MainActivity::class.java)
                    finish()
                } else if (screenType == CONSULTING_ROOM) {
                    if (uri.getQueryParameter("request_id") != null)
                        requestId = uri.getQueryParameter("request_id")
                    if (uri.getQueryParameter("token") != null)
                        token = uri.getQueryParameter("token")

                    Navigator.navigate(this, arrayOf(getString(R.string.request_id_key), getString(R.string.twillio_token_key)), arrayOf(requestId, token),
                            TwillioLoadingActivity::class.java)
                    finish()
                } else if (screenType == "requestReview") {
                    url = uri.getQueryParameter("url")
                    setupWebView(url)
                } else if (screenType == SHARE_PROFILE) {

                    val id = uri.getQueryParameter("profile_id")
                    val typeID = uri.getQueryParameter("profile_type").toInt()

                    Navigator.navigate(this, getString(R.string.profileKey), arrayListOf(HealthProvidersProfileConverter.convert(
                            id, typeID)),
                            HealthProvidersProfileActivity::class.java
                            , -1)
                    finish()
                } else if (screenType == "message") {
                    url = uri.getQueryParameter("url")

                } else {
                    orderId = uri.getQueryParameter("order_id")
                    isPayment = uri.getQueryParameter("is_payment")
                    paymentUrl += orderId + "?is_payment=" + isPayment
                    setupWebView(paymentUrl)
                }
            } catch (e: Exception) {
                Navigator.navigate(this, MainActivity::class.java)
                finish()
            }

        }


    }


    private fun getDataFromIntent() {
        url = intent.getStringExtra(getString(R.string.url_key))
        when (intent.hasExtra(getString(R.string.pdf_key))) {
            true -> {
                url = document + url
            }
        }
        when (intent.hasExtra(getString(R.string.title_key))) {
            true -> {
                title = intent.getStringExtra(getString(R.string.title_key))
            }
        }
    }

    companion object {
        const val PARAMS = "params"
        const val SCREEN_TYPE = "screen_type"
        const val SEARCH_TYPE = "search_type"
        const val SEARCH = "search"
        const val LIST_REQUESTS = "list_requests"
        const val SHARE_PROFILE = "share_profile"
        const val CONSULTING_ROOM = "consulting_room"
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView(pathUrl: String) {
        if (webView != null) {
            val webSettings = webView!!.settings
            webView.settings.javaScriptEnabled = true
            webSettings.builtInZoomControls = false
            webSettings.loadWithOverviewMode = true
            webSettings.allowFileAccess = true
            //  webView.addJavascriptInterface(WebAppInterface(this), "interface")
            webView.settings.useWideViewPort = true
            webView.settings.loadWithOverviewMode = true
            webView!!.webViewClient = object : WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && request?.url != null) {
                        view?.loadUrl(request.url.toString())
                    }
                    progress.visibility = View.GONE

                    return true
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    progress.visibility = View.GONE

                }
            }
            webView!!.loadUrl(pathUrl)

        }
    }

    /** Instantiate the interface and set the context  */
    class WebAppInterface(private val context: Context) {

        /** Show a toast from the web page  */
        @JavascriptInterface
        fun callFromJS(id: String) {
            Toast.makeText(context, id, Toast.LENGTH_SHORT).show()
            Navigator.navigate(context as Activity, HomeMenuListActivity::class.java)
        }
    }
}
