package com.bokdoc.pateintsbook.ui.maps

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MapWrapperLayout: RelativeLayout {
    /**
     * Reference to a GoogleMap object
     */
    private var googleMap:GoogleMap?=null

    /**
     * Vertical offset in pixels between the bottom edge of our InfoWindow
     * and the marker position (by default it's bottom edge too).
     * It's a good idea to use custom markers and also the InfoWindow frame,
     * because we probably can't rely on the sizes of the default marker and frame.
     */
    private var bottomOffsetPixels:Int=0

    /**
     * A currently selected marker
     */
    private var marker: Marker?=null

    /**
     * Our custom view which is returned from either the InfoWindowAdapter.getInfoContents
     * or InfoWindowAdapter.getInfoWindow
     */
    private var infoWindow: View?=null

    constructor(context: Context) : super(context){

    }

    constructor(context: Context, attrSets: AttributeSet):super(context,attrSets){
        //val a = context.obtainStyledAttributes(attrSets, R.styleable.CrystalRanger,0, 0)
        //rangerType = a.getInt(R.styleable.CrystalRanger_rangerTypes,0)
    }

    constructor(context: Context, attrSets: AttributeSet, defStyle:Int):super(context,attrSets,defStyle){
        //val a = context.obtainStyledAttributes(attrSets, R.styleable.CrystalRanger, defStyle, 0)
        //rangerType = a.getInt(R.styleable.CrystalRanger_rangerTypes,0)
    }


    /**
     * Must be called before we can route the touch events
     */
     fun init(map:GoogleMap, bottomOffsetPixels:Int) {
        this.googleMap = map;
        this.bottomOffsetPixels = bottomOffsetPixels;
    }

    /**
     * Best to be called from either the InfoWindowAdapter.getInfoContents
     * or InfoWindowAdapter.getInfoWindow.
     */
    public fun setMarkerWithInfoWindow(marker:Marker, infoWindow:View) {
        this.marker = marker;
        this.infoWindow = infoWindow;
    }



    override fun dispatchTouchEvent(ev: MotionEvent):Boolean {
        var ret:Boolean = false;
        // Make sure that the infoWindow is shown and we have all the needed references
        if (marker != null && marker!!.isInfoWindowShown() && googleMap != null && infoWindow != null) {
            // Get a marker position on the screen
            val point = googleMap?.projection?.toScreenLocation(marker?.position);

            point?.let {
                val copyEv = MotionEvent.obtain(ev);
                copyEv.offsetLocation(-it.x.toFloat() + (infoWindow!!.getWidth() / 2),
                        -point.y.toFloat() + infoWindow!!.height + bottomOffsetPixels)

                // Dispatch the adjusted MotionEvent to the infoWindow
                ret = infoWindow!!.dispatchTouchEvent(copyEv);
            }
            // Make a copy of the MotionEvent and adjust it's location
            // so it is relative to the infoWindow left top corner

        }
        // If the infoWindow consumed the touch event, then just return true.
        // Otherwise pass this event to the super class and return it's result
        return ret || super.dispatchTouchEvent(ev)
    }
}