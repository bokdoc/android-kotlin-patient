package com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.healthProviders.department.Department
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.departments.DepartmentsRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow

class DepartmentsViewModel(private val departmentsRepository: DepartmentsRepository):ViewModel() {
    val progressLiveData = MutableLiveData<Int>()
    val departmentsLiveData = MutableLiveData<List<DepartmentRow>>()
    val errorLiveData = MutableLiveData<String>()


    fun getDepartments(id:String){
        progressLiveData.value = VISIBLE
        departmentsRepository.getDepartments(id).observeForever {
            if(it?.data!=null){
                departmentsLiveData.value = it.data
            }else{
                errorLiveData.value = it?.message
            }
            progressLiveData.value = GONE
        }
    }
}