package com.bokdoc.pateintsbook.ui.searchResultsMap

import android.content.Context
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import java.lang.ref.WeakReference
import android.widget.TextView
import android.R.attr.name
import android.app.Activity
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsActivity
import com.bokdoc.pateintsbook.ui.maps.MapWrapperLayout
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.PriceTextSpannable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_map_info_window.view.*
import android.widget.Toast
import com.bokdoc.pateintsbook.data.webservice.models.Fees
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.maps.OnInfoWindowElemTouchListener
import com.bokdoc.pateintsbook.utils.FeesSpannable


class CustomInfoWindowSearch(val context:WeakReference<Context>,
                             val mapWrapperLayout:WeakReference<MapWrapperLayout>?=null):GoogleMap.InfoWindowAdapter{

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun getInfoWindow(marker: Marker?): View {
        val view = (context.get() as Activity).layoutInflater
                .inflate(R.layout.profile_map_info_window, null)


        val infoWindowData = marker?.tag as Profile

        Glide.with(context.get()!!).load(infoWindowData.picture.url).into(view.profileImage)


        view.profileName.text = infoWindowData.name

        if (infoWindowData.mainSpeciality.isNotEmpty()) {
            view.profileSpeciality.text = infoWindowData.mainSpeciality
        } else {
            view.profileSpeciality.visibility = GONE
        }

        view.rating.setIsIndicator(true)

        view.rating.rating = infoWindowData.rating


        if(infoWindowData.fees.feesAverage!=null && infoWindowData.fees.feesAverage!="0") {
            view.fee.text = PriceTextSpannable.getTextWithPrice(context.get()!!, infoWindowData.fees.feesAverage!!, infoWindowData.fees.currencySymbols!!,
                    R.string.feePlaceholder)
        }else{
            view.fee.visibility = GONE
        }

        if (infoWindowData.bookButton != null) {

            if (infoWindowData.bookButton?.fees?.feesAverage != "0" && infoWindowData.bookButton?.fees?.feesAverage != null) {
                view.btn_book.text = getTextWithPrice(context.get()!!,
                        infoWindowData.bookButton?.fees!!.feesAverage!!,
                        infoWindowData.bookButton?.fees!!.currencySymbols!!,
                        R.string.two_texts_placeholder, infoWindowData.bookButton!!.label, R.color.colorYellow)
            } else {
                view.btn_book.text = infoWindowData.bookButton?.label
            }

            getFees(infoWindowData.bookButton!!.fees)?.let {
                view.tv_old_fees.visibility = VISIBLE
                view.tv_old_fees.text = it
            }


            val touchListener =
                    object : OnInfoWindowElemTouchListener(view.btn_book) {
                    override fun onClickConfirmed(v: View, marker: Marker) {
                    // Here we can perform some action triggered after clicking the button
                        if (infoWindowData.bookButton!!.isTargetBookAppointments) {
                            Navigator.navigate(context.get()!! as Activity, arrayOf(context.get()!!.getString(R.string.url_key)),
                                    arrayOf(infoWindowData.bookButton!!.endpointUrl),
                                    BookAppointmentsActivity::class.java)
                        } else {
                            Navigator.navigate(context.get()!! as Activity, arrayOf(context.get()!!.getString(R.string.url_key),
                                    context.get()!!.getString(R.string.docNameSlug), context.get()!!.getString(R.string.docPicSlug),
                                    context.get()!!.getString(R.string.profileKey)),
                                    arrayOf(infoWindowData.bookButton!!.endpointUrl,
                                            infoWindowData.name, infoWindowData.picture.url,
                                            infoWindowData.profileTypeId.toString()),
                                    LinksCardsDetailsActivity::class.java)
                        }
                }
            }
            view.btn_book.setOnTouchListener(touchListener)

        }else{
            view.btn_book.visibility = GONE
        }

        mapWrapperLayout?.get()?.setMarkerWithInfoWindow(marker,view)

        view.location.text = infoWindowData.location.address

        return view
    }

    private fun getTextWithPrice(context: Context, feesAverage: String, currentSymbols: String,
                                 stringRes: Int, label: String, color: Int = R.color.colorPrimary): Spannable {
        val price = context.getString(R.string.pricePlaceHolder, feesAverage,
                currentSymbols)
        val consulting = context.getString(stringRes, label, price)
        return ColoredTextSpannable.getSpannable(consulting, price, ContextCompat.getColor(context, color))
    }

    private fun getFees(fees:Fees?):Spannable?{
        var amount = ""
        var currency=""
        fees?.oldAmount?.let {
            if(it!="0")
            amount = it
        }
        fees?.currencySymbols?.let {
            currency = it
        }

        if(amount.isNotEmpty()){
            return FeesSpannable.getStrokedFees("$amount $currency")
        }

        return null
    }


}