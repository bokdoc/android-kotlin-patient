package com.bokdoc.pateintsbook.ui.patientRequests

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.webservice.models.Actions
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.ui.patientRequests.dialogs.InputDialog
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.fragment_past_request.*
import kotlinx.android.synthetic.main.include_error.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.ArrayList

class PastRequestsFragment : Fragment(), View.OnClickListener {
    lateinit var requestsViewModel: RequestsViewModel
    private var isLoading: Boolean = false
    private var isLastPage = false
    private var requestsAdapter: RequestsAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_past_request, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity != null && activity is RequestsActivity) {
            toolbar.visibility = GONE
        } else {
            titleText.text = getString(R.string.my_requests)
            filterIcon.visibility = VISIBLE
            filterIcon.setOnClickListener(this::onClick)
        }

        getViewModel()
        when (Utils.hasConnection(activity!!)) {
            true -> getData()
            false -> showErrorOrEmpty(activity!!.getString(R.string.no_connection))
        }
        setUpPagination()
    }

    private fun getData() {
        requestsViewModel.getRequests("")

        requestsViewModel.errorMessageLiveData.observe(this, Observer {
            Utils.showShortToast(activity!!, it.toString())
        })
        requestsViewModel.progressLiveData.observe(this, Observer {
            when (it) {
                false -> progress?.visibility = View.GONE
                true -> progress?.visibility = View.VISIBLE
            }
        })

        requestsViewModel.requestsLiveData.observe(this, Observer {
            if (it != null) {
                when (it.size) {
                    0 -> {
                        filterIcon.visibility = GONE

                        showErrorOrEmpty(activity!!.getString(R.string.no_requests))
                    }
                    else -> {
                        showMain()
                        if (requestsAdapter == null) {
                            requestsAdapter = RequestsAdapter(activity!!, it, this::onActionsClickListener) { _, request ->
                                request.doctor_data.get(request.document)?.let {
                                    Navigator.navigate(
                                            activity!!, getString(R.string.profileKey),
                                            arrayListOf(HealthProvidersProfileConverter.convert(
                                                    it.id, it.profileTypeId)), HealthProvidersProfileActivity::class.java
                                            , RequestsActivity.BOOK_REQUEST)
                                }
                            }
                            rv_past_requests.adapter = requestsAdapter
                        } else {
                            if (isLoading) {
                                requestsAdapter!!.removeLoadingFooter()
                                isLoading = false
                                requestsAdapter!!.add(it)
                            } else {
                                requestsAdapter!!.updateList(it)
                            }
                        }
                    }
                }
            }
        })
    }

    private fun onActionsClickListener(actions: Actions) {
        actions.let {
            if (it.slug != null && it.label != null && it.url != null) {
                when (it.slug) {
                    Actions.REVIEW_SLUG -> {
                        Navigator.navigate(activity!!,
                                arrayOf(getString(R.string.title_key), getString(R.string.url_key)),
                                arrayOf(it.label, it.url),
                                WebViewActivity::class.java)
                    }
                    Actions.CANCEL_SLUG -> {
                        showCancellationDialog(it.url)
                    }
                }
            }
        }
    }

    fun setUpPagination() {
        rv_past_requests.addOnScrollListener(object : PaginationScrollListener(rv_past_requests.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                requestsViewModel.currentPage = requestsViewModel.currentPage.plus(1)
                requestsViewModel.metaLiveData.value?.pagination?.totalPages?.let {
                    if (requestsViewModel.currentPage < it) {
                        isLoading = true
                        (rv_past_requests.adapter as RequestsAdapter).addLoadingFooter()
                        requestsViewModel.getRequests("")
                    } else {
                        isLastPage = true
                    }
                }
            }


        })
    }


    private fun showCancellationDialog(url: String) {
        val inputDialog = InputDialog(activity!!, getString(R.string.enter_reason_message))
        inputDialog.text = requestsViewModel.cancellationReason
        inputDialog.callback = object : InputDialog.OnDialogClickedListener {
            override fun onDialogClicked(reason: String) {
                inputDialog.dismiss()
                requestsViewModel.cancelRequest(url, reason).observe(this@PastRequestsFragment, Observer {
                    requestsViewModel.progressLiveData.value = false
                    if (it != null) {
                        it.meta.message.let {
                            if (it.isNotEmpty() && activity != null)
                                Utils.showLongToast(activity!!, it)
                        }
                        if (it.status == DataResource.INTERNAL_SERVER_ERROR && activity != null) {
                            Utils.showLongToast(activity!!, R.string.server_error)
                            showCancellationDialog(url)
                        }
                        if (it.status == DataResource.SUCCESS) {
                            requestsViewModel.currentPage = 1
                            getData()
                        }
                    }
                })
            }

        }
        inputDialog.show()

    }

    private fun getViewModel() {
        requestsViewModel = InjectionUtil.getRequestsViewModel(activity!!)
    }

    companion object {
        fun newInstance(): PastRequestsFragment {
            val fragment = PastRequestsFragment()
            return fragment
        }
    }

    fun showErrorOrEmpty(message: String) {
        lay_container.visibility = View.GONE
        lay_error.visibility = View.VISIBLE
        tv_error.text = message
    }

    fun showMain() {
        lay_container.visibility = View.VISIBLE
        lay_error.visibility = View.GONE
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            filterIcon.id -> {
                if (requestsViewModel.metaLiveData.value?.filter != null && requestsViewModel.metaLiveData.value?.filter!!.isNotEmpty()) {
                    val intent = Intent(activity, FilterActivity::class.java).putParcelableArrayListExtra("list",
                            requestsViewModel.metaLiveData.value?.filter?.get(0)?.data as ArrayList<out Parcelable>)
                    if (activity != null && activity is RequestsActivity) {
                        activity!!.startActivityForResult(intent, RequestsActivity.FILTER_REQUEST)
                    } else {
                        startActivityForResult(intent, RequestsActivity.FILTER_REQUEST)
                    }

                }
            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            requestsViewModel.currentPage = 1
            requestsViewModel.getRequests(data!!.getStringExtra("selectedParam"))
        }
    }


}