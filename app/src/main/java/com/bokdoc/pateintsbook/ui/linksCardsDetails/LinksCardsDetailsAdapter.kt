package com.bokdoc.pateintsbook.ui.linksCardsDetails

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.text_with_line_row.view.*

class LinksCardsDetailsAdapter(private val linksCardsDetails:List<LinksCardsDetails.RowsInfo>,
                               private val clickListener:(LinksCardsDetails.RowsInfo,Int)->Unit)
    : RecyclerView.Adapter<LinksCardsDetailsAdapter.LinksCardsDetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinksCardsDetailsViewHolder {
        return LinksCardsDetailsViewHolder(parent.inflateView(R.layout.text_arrow_with_line_row))
    }

    override fun getItemCount(): Int {
        return linksCardsDetails.size
    }

    override fun onBindViewHolder(holder: LinksCardsDetailsViewHolder, position: Int) {
        holder.itemView.tv_title.text = linksCardsDetails[position].name
        if(position == itemCount-1){
            holder.itemView.line.visibility = View.GONE
        }else{
            holder.itemView.line.visibility = View.VISIBLE
        }
    }


    inner class LinksCardsDetailsViewHolder(view:View):RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                clickListener.invoke(linksCardsDetails[adapterPosition],adapterPosition)
            }

        }
    }
}