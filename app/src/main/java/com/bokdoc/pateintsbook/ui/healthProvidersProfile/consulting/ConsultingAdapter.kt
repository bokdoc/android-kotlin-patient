package com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.NextAvailabilitiesAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bokdoc.pateintsbook.utils.formatters.FeesFormatter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_clinic.view.*

class ConsultingAdapter(private val consulting: List<ConsultingRow>,
                        var itemCLickListener: (url: String, nextAvi: NextAvailabilitiesParcelable, position: Int) -> Unit) : RecyclerView.Adapter<ConsultingAdapter.ConsultingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ConsultingViewHolder {
        return ConsultingViewHolder(parent.inflateView(R.layout.item_clinic))
    }

    override fun getItemCount(): Int {
        return consulting.size
    }

    override fun onBindViewHolder(holder: ConsultingViewHolder, position: Int) {
        consulting[position].let {

            holder.itemView.tv_periodic_text.visibility = View.GONE
            holder.itemView.tv_media_view_all.visibility = View.GONE
            holder.itemView.tv_surgery_name.visibility = View.GONE
            holder.itemView.iv_icon.visibility = View.GONE


            if(Profile.isHospitalCategory(it.profileType)){
                if (it.doctorName?.isNotEmpty()!!) {
                    holder.itemView.group_doctor_data.visibility = View.VISIBLE
                    holder.itemView.profileName.text = it.doctorName
                    holder.itemView.profileDesc.text = holder.itemView.context.getString(R.string.two_texts_placeholder,
                            it.doctorTitle, it.speciality)
                    Glide.with(holder.itemView.context).load(it.doctorPhoto).into(holder.itemView.profileImage)
                } else {
                    holder.itemView.group_doctor_data.visibility = View.GONE
                    holder.itemView.tv_surgery_name.visibility = View.VISIBLE
                    holder.itemView.tv_surgery_name.text = it.speciality
                }
            }

            if(it.profileType == Profile.DOCTOR){
                holder.itemView.group_doctor_data.visibility = View.GONE
            }



            FeesFormatter.setFees(it.nationalFees, holder.itemView.tv_surgery_fees_from,
                    holder.itemView.tv_surgery_fees_to,
                    it.interNationalFees, holder.itemView.tv_surgery_fees_national_from,
                    holder.itemView.tv_surgery_fees_national_to, holder.itemView.iv_fees)

            it.nationalFees?.let {fees->
                if(FeesFormatter.isMainFeesHasValue(fees)){
                    holder.itemView.tv_surgery_fees_from.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.colorBlack))
                    holder.itemView.tv_surgery_fees_from.textSize =
                            holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_12).toFloat()
                    holder.itemView.tv_surgery_fees_to.textSize =
                            holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_12).toFloat()
                }else{
                    holder.itemView.tv_surgery_fees_from.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.colorDescription))
                    holder.itemView.tv_surgery_fees_to.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.colorDescription))
                    holder.itemView.tv_surgery_fees_from.textSize =
                            holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_9).toFloat()
                    holder.itemView.tv_surgery_fees_to.textSize =
                            holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_9).toFloat()
                }
            }



            val layoutManger = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
            holder.itemView.rv_next_next_availabilities.layoutManager = layoutManger
            holder.itemView.rv_next_next_availabilities.smoothScrollToPosition(0)
            holder.itemView.rv_next_next_availabilities.adapter = NextAvailabilitiesAdapter(it.nextAvailabilities, {
                if (consulting[position].bookButton != null)
                    itemCLickListener.invoke(consulting[position].bookButton!!.endpointUrl, it, position)

            }, { _, _ -> }, true)
            holder.itemView.rv_next_next_availabilities.smoothScrollToPosition(0)

            holder.itemView.iv_right.setOnClickListener {

                val totalItemCount = holder.itemView.rv_next_next_availabilities.adapter!!.getItemCount()
                if (totalItemCount <= 0) return@setOnClickListener
                val lastVisibleItemIndex = layoutManger.findLastVisibleItemPosition()

                if (lastVisibleItemIndex >= totalItemCount) return@setOnClickListener
                layoutManger.smoothScrollToPosition(holder.itemView.rv_next_next_availabilities, null, lastVisibleItemIndex + 1)


            }

            holder.itemView.iv_left.setOnClickListener {

                val firstVisibleItemIndex = layoutManger.findFirstCompletelyVisibleItemPosition()
                if (firstVisibleItemIndex > 0) {
                    layoutManger.smoothScrollToPosition(holder.itemView.rv_next_next_availabilities, null, firstVisibleItemIndex - 1)
                }
                // holder.itemView.rv_next_next_availabilities.layoutManager!!.scrollToPosition(layoutManger.findLastVisibleItemPosition() - 1)

            }
            if (it.location.latitude.isEmpty() && it.location.longitude.isEmpty()) {
                holder.itemView.iv_location.visibility = View.GONE
            }

        }
    }

    fun getValue(resource:Int,context: Context):Float{
        val outValue = TypedValue()
        context.resources.getValue(resource, outValue, true)
        return outValue.float
    }

    inner class ConsultingViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}