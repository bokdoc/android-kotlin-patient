package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_profile_feedback.*
import kotlinx.android.synthetic.main.toolbar.*

class ProfileFeedbackActivity : ParentActivity() {

    lateinit var feedbackViewModel: ProfileFeedbackViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_feedback)
        titleText.text = getString(R.string.feedBack)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
        feedbackList.layoutManager = LinearLayoutManager(this)
        rightTextView.visibility = VISIBLE
        feedbackViewModel = InjectionUtil.getFeedbackViewModel(this)
        feedbackViewModel.progressLiveData.observe(this, Observer {
            progress.visibility = it!!
        })
        feedbackViewModel.errorLiveData.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        feedbackViewModel.getFeedback(intent?.getStringExtra(getString(R.string.idKey))!!)

        feedbackViewModel.feedbackLiveData.observe(this, Observer {
            rightTextView.text = (getString(R.string.resultsPlaceHolder, it?.size.toString()))
            feedbackList.adapter = FeedbackAdapter(it!!)
        })
    }
}
