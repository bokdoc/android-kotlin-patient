package com.bokdoc.pateintsbook.ui.homeMenuList

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_home_list_row.view.*

class HomeMenuListAdapter(private var imagesTextsRow:List<ImageTextRow>,private val onClickListener:(slug:String,position:Int)->Unit):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HomeMenuViewHolder(parent.inflateView(R.layout.item_home_list_row))
    }

    override fun getItemCount(): Int {
        return imagesTextsRow.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        imagesTextsRow[position].let {
            Glide.with(holder.itemView).load(it.image).into(holder.itemView.image)
            holder.itemView.text.text = it.text
            if(position == itemCount-1){
                holder.itemView.bottomLine.visibility = GONE
            }else{
                holder.itemView.bottomLine.visibility = VISIBLE
            }
        }
    }


    inner class HomeMenuViewHolder(view:View):RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                onClickListener.invoke(imagesTextsRow[adapterPosition].slug,adapterPosition)
            }
        }
    }
}