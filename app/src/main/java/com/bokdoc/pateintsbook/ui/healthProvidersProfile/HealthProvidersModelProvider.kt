package com.bokdoc.pateintsbook.ui.healthProvidersProfile

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.filter.FilterRepositories
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.ProvidersProfileRepository
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewRepository

class HealthProvidersModelProvider(private val repository: ProvidersProfileRepository,
                                   private val socialActionsRepository: ProfileSocialActionsRepository
                                   , val overViewRepository: OverViewRepository):
        ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HealthProvidersViewModel(repository,socialActionsRepository,overViewRepository) as T
    }
}