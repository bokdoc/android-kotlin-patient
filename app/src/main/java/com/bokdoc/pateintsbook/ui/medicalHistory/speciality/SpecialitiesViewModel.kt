package com.bokdoc.pateintsbook.ui.medicalHistory.speciality

import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.SpecialitiesRepository
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel

class SpecialitiesViewModel(specialitiesRepository: SpecialitiesRepository)
    : MedicalHistoryViewModel(specialitiesRepository)