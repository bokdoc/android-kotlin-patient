package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.clinics.ClinicsRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow

class ClinicsViewModel(private val clinicsRepository: ClinicsRepository) : ViewModel() {
    val data = MutableLiveData<List<ClinicRow>>()
    lateinit var id: String

    var meta: Meta = Meta()
    var currentPage: Int = 1
    var serviceType: HashMap<String, String>? = HashMap()


    fun getClinics(): LiveData<DataResource<ClinicRow>> {
        serviceType!!.put("page", currentPage.toString())
        serviceType!!.put("include", "nextAvailabilities,parent")
        return clinicsRepository.getClinics(id, serviceType!!)
    }
}