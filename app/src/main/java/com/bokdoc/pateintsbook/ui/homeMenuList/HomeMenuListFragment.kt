package com.bokdoc.pateintsbook.ui.homeMenuList

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.navigators.AutoCompleteNavigator
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.LookupsNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompleteResultsActivity
import com.bokdoc.pateintsbook.ui.changeCurrencyLanguages.ChangeCurrencyLanguageActivity
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeSearch.AppointementFragment
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.utils.constants.AutoCompletesConstants
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.listeners.HomeSearchEditsListeners
import kotlinx.android.synthetic.main.activity_home_menu_list.*
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.home_search.*
import kotlinx.android.synthetic.main.toolbar.*

class HomeMenuListFragment : Fragment(), View.OnClickListener {
    private lateinit var homeMenuListViewModel: HomeMenuListViewModel
    private lateinit var specialityTouchListener: HomeSearchEditsListeners
    private lateinit var locationsTouchListener: HomeSearchEditsListeners
    private lateinit var insuranceTouchListener: HomeSearchEditsListeners

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_home_menu_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewModel()
        setupTouchListeners()
        setupViewsClickListener()
        setupViews()
        updateServicesContent()
        homeMenuListViewModel.sendFcmToken()
    }


    private fun getViewModel() {
        homeMenuListViewModel = ViewModelsCustomProvider(HomeMenuListViewModel(activity!!,
                SharedPreferencesManagerImpl(activity!!),
                SplashRepository(activity!!.applicationContext,
                        UserSharedPreference(activity!!.applicationContext),
                        AddFcmWebService(WebserviceManagerImpl.instance())), TokenRepository(activity!!.applicationContext))).create(HomeMenuListViewModel::class.java)
    }

    private fun setupViews() {
        if (activity != null && activity?.intent?.hasExtra(getString(R.string.defaultTab))!!) {
            homeMenuListViewModel.defaultSelected = activity!!.intent?.getStringExtra(getString(R.string.defaultTab))!!.toInt()
            homeMenuListViewModel.screenType = SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)
        }else {
            homeMenuListViewModel.getDefaultSelectedFromCache()
            updateTexts()
        }
        clearSpeciality.setOnClickListener {
            specialityEdit.setText("")
            it.visibility = GONE
            homeMenuListViewModel.speciality = ""
            homeMenuListViewModel.specialityParams = ""
            homeMenuListViewModel.saveDataState()

        }

        clearCity.setOnClickListener {
            cityEdit.setText("")
            homeMenuListViewModel.regionCountry = ""
            homeMenuListViewModel.regionCountryParams = ""
            homeMenuListViewModel.saveDataState()
            it.visibility = GONE
        }

        clearInsurance.setOnClickListener {
            insuranceEdit.setText("")
            homeMenuListViewModel.insuranceCarrier = ""
            homeMenuListViewModel.insuranceCarrierParams = ""
            homeMenuListViewModel.saveDataState()
            it.visibility = GONE
        }



        specialityEdit.setOnTouchListener(specialityTouchListener)
//        cityEdit.setOnTouchListener(locationsTouchListener)
        insuranceEdit.setOnTouchListener(insuranceTouchListener)

        search.setOnClickListener {
            homeMenuListViewModel.screenType = SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)
            homeMenuListViewModel.saveDataState()

            Navigator.navigate(activity!!, arrayOf(getString(R.string.screenType), getString(R.string.query)),
                    arrayOf(SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected),
                            homeMenuListViewModel.getQuery()), SearchResultsActivity::class.java, HomeSearchActivity.SEARCH_REQUEST)
        }
    }

    private fun setupTouchListeners() {

        specialityTouchListener = object : HomeSearchEditsListeners(activity!!, arrayOf(getString(R.string.screenType)
                , getString(R.string.completeType)), SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), AutoCompletesConstants.SPECIALITY,
                AppointementFragment.AUTO_COMPLETE_REQUEST, this, specialityEdit.hint.toString()) {
            override fun passCompletedType(completeType: Int) {
                homeMenuListViewModel.completeEditType = completeType
                AutoCompleteNavigator.navigate(this@HomeMenuListFragment, keys,
                        SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), completeType, requestType,
                        specialityEdit.hint.toString())
            }
        }


        insuranceTouchListener = object : HomeSearchEditsListeners(activity!!, arrayOf(getString(R.string.screenType), getString(R.string.completeType)),
                SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected),
                AutoCompletesConstants.INSURANCE, AppointementFragment.AUTO_COMPLETE_REQUEST, this,insuranceEdit.hint.toString()) {
            override fun passCompletedType(completeType: Int) {


                homeMenuListViewModel.completeEditType = completeType
                AutoCompleteNavigator.navigate(this@HomeMenuListFragment, keys,
                        SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), completeType, requestType,
                        insuranceEdit.hint.toString())
            }

        }

        locationsTouchListener = object : HomeSearchEditsListeners(activity!!, arrayOf(getString(R.string.screenType)
                , getString(R.string.completeType)), SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), AutoCompletesConstants.LOCATIONS,
                AppointementFragment.AUTO_COMPLETE_REQUEST, this, getString(R.string.regionCityCountry)) {
            override fun passCompletedType(completeType: Int) {
                homeMenuListViewModel.completeEditType = completeType
                AutoCompleteNavigator.navigate(this@HomeMenuListFragment, keys,
                        SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), completeType, requestType, hint)
            }

        }

    }


    private fun setupViewsClickListener() {
        card_view_online_consulting.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.CONSULTING_POSITION
            updateServicesContent()
        }
        card_view_appointments.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.APPOINTMENTS_POSITION
            updateServicesContent()
        }
        card_view_surgeries.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.SURGERIES_POSITION
            updateServicesContent()
        }

    }

    private fun updateTexts() {

        homeMenuListViewModel.screenType = SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)

        homeMenuListViewModel.getData()


        homeMenuListViewModel.speciality.let {
            if (it.isNotEmpty() && it!="0") {
                specialityEdit.setText(it)
                clearSpeciality.visibility = VISIBLE
            }else{
                specialityEdit.setText(StringsConstants.EMPTY)
                clearSpeciality.visibility = GONE
            }
        }

        homeMenuListViewModel.regionCountry.let {
            if (it.isNotEmpty()&&it!="0") {
                cityEdit.setText(it)
                clearCity.visibility = VISIBLE
            }else{
                cityEdit.setText(StringsConstants.EMPTY)
                clearCity.visibility = GONE
            }
        }

        homeMenuListViewModel.insuranceCarrier.let {
            if (it.isNotEmpty() && it!="0") {
                insuranceEdit.setText(it)
                clearInsurance.visibility = VISIBLE
            }else{
                insuranceEdit.setText(StringsConstants.EMPTY)
                clearInsurance.visibility = GONE
            }
        }
        /*
        if (homeMenuListViewModel.screenType == SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)) {

        } else {
            specialityEdit.setText(StringsConstants.EMPTY)
            clearSpeciality.visibility = GONE
            cityEdit.setText(StringsConstants.EMPTY)
            clearCity.visibility = GONE
            insuranceEdit.setText(StringsConstants.EMPTY)
            clearInsurance.visibility = GONE
        }
        */
    }

    private fun updateServicesContent() {
        when {
            homeMenuListViewModel.defaultSelected == HomeMenuListViewModel.APPOINTMENTS_POSITION -> {
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.appointments_desc_main_screen)
                specialityEdit.hint = getString(R.string.specialityConditionDoctor)
                insuranceEditParent.visibility = View.VISIBLE
                regionEditParent.visibility = View.VISIBLE

            }
            homeMenuListViewModel.defaultSelected == HomeMenuListViewModel.SURGERIES_POSITION -> {
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.surgeries_desc_main_screen)
                specialityEdit.hint = getString(R.string.surgeries_hint)

                insuranceEditParent.visibility = View.VISIBLE
                regionEditParent.visibility = View.VISIBLE
            }
            else -> {
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.consulting_desc_main_screen)
                specialityEdit.hint = getString(R.string.specialityConditionDoctor)
                insuranceEditParent.visibility = View.GONE
                regionEditParent.visibility = View.GONE

            }
        }
        updateTexts()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppointementFragment.AUTO_COMPLETE_REQUEST && resultCode == Activity.RESULT_OK) {
            when (homeMenuListViewModel.completeEditType) {
                AutoCompletesConstants.SPECIALITY -> {
                    specialityEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    clearSpeciality.visibility = VISIBLE
                    homeMenuListViewModel.speciality = data.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.specialityParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                }
                AutoCompletesConstants.LOCATIONS -> {
                    cityEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    clearCity.visibility = VISIBLE
                    homeMenuListViewModel.regionCountry = data.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.regionCountryParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                }
                else -> {
                    insuranceEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    homeMenuListViewModel.insuranceCarrier = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.insuranceCarrierParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                    clearInsurance.visibility = VISIBLE
                }
            }
            homeMenuListViewModel.saveDataState()
        }
    }

    override fun onClick(p0: View?) {

    }

    companion object {

        @JvmStatic
        fun newInstance() =
                HomeMenuListFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}