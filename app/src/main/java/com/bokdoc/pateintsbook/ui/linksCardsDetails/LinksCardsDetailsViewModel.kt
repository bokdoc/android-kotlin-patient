package com.bokdoc.pateintsbook.ui.linksCardsDetails

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.linksCardsDetails.LinksCardsDetailsRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails

class LinksCardsDetailsViewModel(private val linksCardsDetailsRepository: LinksCardsDetailsRepository,
                                 private val context: Application) : ViewModel() {

    var screenTitle = ""
    var url = ""
    var serviceType = ""
    var clinicsRows = MutableLiveData<DataResource<ClinicRow>>()
    var surgeriesRows = MutableLiveData<DataResource<SurgeriesRow>>()
    var consultingRows = MutableLiveData<DataResource<ConsultingRow>>()

    var meta: Meta = Meta()
    var currentPage: Int = 1
    var totalPages: Int = 1

    fun getDetails() {
        return linksCardsDetailsRepository.getDetails(url).observeForever { dataResource ->
            dataResource?.data?.forEach {
                screenTitle = it.title
                when {
                    it.slug == LinksCardsDetails.CONSULTING_SLUG -> {
                        consultingRows.value = DataResource<ConsultingRow>().apply {
                            data = linksCardsDetailsRepository.getConsultingRows(it)
                            meta = dataResource.meta
                            errors = dataResource.errors
                            message = dataResource.message
                            status = dataResource.status
                        }
                    }
                    it.slug == LinksCardsDetails.SURGERIES_SLUG -> {
                        surgeriesRows.value = DataResource<SurgeriesRow>().apply {
                            data = linksCardsDetailsRepository.getSurgeriesRows(it)
                            meta = dataResource.meta
                            errors = dataResource.errors
                            message = dataResource.message
                            status = dataResource.status
                        }
                    }
                    else -> {
                        clinicsRows.value = DataResource<ClinicRow>().apply {
                            data = linksCardsDetailsRepository.getClinicsRows(it)
                            meta = dataResource.meta
                            errors = dataResource.errors
                            message = dataResource.message
                            status = dataResource.status
                        }
                    }
                }
            }
        }
    }


}