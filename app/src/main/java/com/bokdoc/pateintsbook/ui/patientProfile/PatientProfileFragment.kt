package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.patientCompares.CompareActivity
import com.bokdoc.pateintsbook.ui.patientRequests.RequestsActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.fragment_patient_profile.*
import kotlinx.android.synthetic.main.view_progress.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
const val ARG_REQUEST_TYPE = "requestType"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PatientProfileFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PatientProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
abstract class PatientProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    var requestType: String? = null
    private var listener: OnFragmentInteractionListener? = null
    lateinit var patientProfileViewModel: PatientProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            requestType = it.getString(ARG_REQUEST_TYPE)
            //param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        if (requestType != null)
            Utils.showLongToast(activity!!, requestType!!)

        val rootView = inflater.inflate(R.layout.fragment_patient_profile, container, false)
        rootView.findViewById<View>(R.id.progress).visibility = VISIBLE

        /*
            patientProfileViewModel.listLiveDataResource.observe(this, Observer {
            if(it?.data!=null){
                if(it.data!!.isNotEmpty()){
                    dataGroup.visibility = VISIBLE
                    profilesList.adapter = PatientProfilesInfoAdapter(it.data!!)
                }else{
                    noDataGroup.visibility = VISIBLE
                }
            }else{
                if(it?.message?.isNotEmpty()!!){
                    activity?.applicationContext?.let { it1 -> Utils.showLongToast(it1,it.message) }
                }
            }
            progress?.visibility = GONE
        })
        */

        return rootView
    }


    @Override
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        profilesList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        /*
        viewAllText.setOnClickListener {
            when (requestType) {
                GET_MY_FAVORITES_ARG -> {
                    Navigator.navigate(activity!!,FavoritesActivity::class.java)
                }
                GET_MY_COMPARES_ARG -> {
                    Navigator.navigate(activity!!,CompareActivity::class.java)
                }
                else -> {
                    Navigator.navigate(activity!!,RequestsActivity::class.java)
                }
            }
        }
        */
    }

    fun getData() {
        progress?.visibility = VISIBLE
        when (requestType) {
            GET_MY_FAVORITES_ARG -> {
                patientProfileViewModel.getFavorites()
            }
            GET_MY_COMPARES_ARG -> {
                patientProfileViewModel.getCompares()
            }
            else -> {
                patientProfileViewModel.getRequests()
            }
        }
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //setup the listener for the fragment A
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun setViewModel(patientProfileViewModel: PatientProfileViewModel) {
        //this.patientProfileViewModel = patientProfileViewModel
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        patientProfileViewModel = InjectionUtil.getPatientsViewModel(activity!!)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param requestType Parameter 1.
         * @return A new instance of fragment PatientProfileFragment.
         */
        // TODO: Rename and change types and number of parameters


        const val GET_MY_COMPARES_ARG = "myCompares"
        const val GET_MY_FAVORITES_ARG = "myFavorites"
        const val GET_MY_REQUESTS = "myRequests"
    }


}
