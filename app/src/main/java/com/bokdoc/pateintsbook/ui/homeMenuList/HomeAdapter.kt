package com.bokdoc.pateintsbook.ui.homeMenuList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.healthproviders.data.models.homeMenu.HomeMenu
import com.bokdoc.pateintsbook.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_more.view.*

class HomeAdapter(var moreList: List<HomeMenu>,
                  var itemCLickListener: (id: Int) -> Unit) :
        RecyclerView.Adapter<HomeAdapter.HomeMenuViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): HomeMenuViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_more, parent, false)
        return HomeMenuViewHolder(view)
    }

    override fun getItemCount(): Int {
        return moreList.size
    }

    override fun onBindViewHolder(holder: HomeMenuViewHolder, position: Int) {
        moreList[position].let {
            holder.itemView.tv_title_more.text = it.title
            Glide.with(holder.itemView.context).load(it.image).into(holder.itemView.iv_menu_header)
        }
        if (moreList.size == position + 1) {
            holder.itemView.line.visibility = View.GONE
        }

    }

    fun update() {
        notifyDataSetChanged()
    }

    inner class HomeMenuViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                itemCLickListener.invoke(moreList[adapterPosition].id)
            }
        }
    }
}