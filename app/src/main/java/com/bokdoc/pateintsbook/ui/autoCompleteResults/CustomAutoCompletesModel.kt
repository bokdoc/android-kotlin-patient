package com.bokdoc.pateintsbook.ui.autoCompleteResults

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context

class CustomAutoCompletesModel(private val context: Context):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AutoCompletesViewModel(context) as T
    }
}