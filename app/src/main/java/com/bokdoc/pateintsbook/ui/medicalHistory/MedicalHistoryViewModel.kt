package com.bokdoc.pateintsbook.ui.medicalHistory

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.PatientApp
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.MedicalHistoryRepository

abstract class MedicalHistoryViewModel(private val medicalHistoryRepository: MedicalHistoryRepository)
    :AndroidViewModel(PatientApp()) {

    lateinit var key:String
    val progressLiveData = MutableLiveData<Int>()
    val deletedLiveData = MutableLiveData<Boolean>()
    val addedLiveData = MutableLiveData<Boolean>()
    val messageLiveData = MutableLiveData<String>()
    val listLiveData = MutableLiveData<List<MedicalHistory>>()
    var addedOrRemovedPosition:Int = -1
    lateinit var addedOrRemovedMedicalHistory:MedicalHistory

    fun get(userId:String){
        progressLiveData.value = VISIBLE
        medicalHistoryRepository.get(userId).observeForever {
            if(it?.data!=null){
                listLiveData.value = it.data!!
            }else{
                if(it!=null && it.errors.isNotEmpty())
                    messageLiveData.value = it.errors[0].detail
            }
            progressLiveData.value = GONE
        }
    }

    fun add(medicalHistory:MedicalHistory){
       // messageLiveData.value = getApplication<PatientApp>().getString(R.string.messageAdding)
        medicalHistoryRepository.add(medicalHistory.historyId).observeForever {dataResource->
            (dataResource?.status == DataResource.SUCCESS).let {
                if(it && dataResource!=null && dataResource.meta.message.isNotEmpty()){
                    messageLiveData.value = dataResource.meta.message
                }else{
                    if(dataResource!=null && dataResource.errors.isNotEmpty())
                        messageLiveData.value = dataResource.errors[0].detail
                }
                if(it) {
                    (listLiveData.value as MutableList).add(0, medicalHistory)
                }
                addedLiveData.value = it
            }
        }
    }

    fun delete(position:Int){
       // messageLiveData.value = getApplication<PatientApp>().getString(R.string.messageDeleting)
        listLiveData.value?.let {
            medicalHistoryRepository.delete(it[position].historyId).observeForever {dataResource->
                (dataResource?.status == DataResource.SUCCESS).let {
                    if(it && dataResource!=null && dataResource.meta.message.isNotEmpty()){
                        messageLiveData.value = dataResource.meta.message
                    }else{
                        if(dataResource!=null && dataResource.errors.isNotEmpty())
                            messageLiveData.value = dataResource.errors[0].detail
                    }
                    if(it){
                        (listLiveData.value as MutableList).removeAt(position)
                        listLiveData.value = listLiveData.value
                       // listLiveData.value = listLiveData.value
                    }
                    deletedLiveData.value = it
                }
            }
        }

    }

    fun getLookups():ArrayList<LookUpsParcelable>{
        val lookups = ArrayList<LookUpsParcelable>()
        listLiveData.value?.forEach {
            lookups.add(LookUpsParcelable().apply {
                idLookup = it.historyId
                name = it.name
            })
        }
        return lookups
    }
}