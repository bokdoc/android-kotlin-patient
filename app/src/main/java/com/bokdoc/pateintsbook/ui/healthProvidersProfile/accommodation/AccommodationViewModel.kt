package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accommodation.AccommodationRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccommodationsRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow

class AccommodationViewModel(val accommodationRepository: AccommodationRepository) : ViewModel() {

//    val progressLiveData = MutableLiveData<Boolean>()
//    val accommodationsLiveData = MutableLiveData<List<AccommodationsRow>>()
//    val errorMessageLiveData = MutableLiveData<String>()

    lateinit var id: String
    lateinit var serviceType: String

    fun getAccommodation(): LiveData<DataResource<AccommodationsRow>> {
        return accommodationRepository.getAccommodations(id, serviceType)

    }
}