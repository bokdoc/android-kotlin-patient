package com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DoctorRow
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_list_with_progress.*

class DoctorsActivity : ParentActivity() {

    private lateinit var doctorsViewModel: DoctorsViewModel
    private var isLoading: Boolean = false
    private var isLastPage = false
    var doctorsAdapter: DoctorsAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()
        title = getString(R.string.doctors)
        getViewModel()
        getDataFromIntent()
        getDoctors()
    }

    private fun getDoctors() {
        progress.visibility = View.VISIBLE
        isLoading = false

        doctorsViewModel.getAlldoctors().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    if (doctorsAdapter == null) {
                        if (!it.data.isNullOrEmpty()) {
                            doctorsAdapter = DoctorsAdapter((it.data as ArrayList<DoctorRow>?)!!, this::onBookClickListner)
                            rv_list.adapter = doctorsAdapter
                            doctorsViewModel.meta.pagination = it.meta.pagination
                            progress.visibility = View.GONE
                        } else {
                            progress.visibility = View.GONE
                            Utils.showShortToast(this, getString(R.string.empty_message))
                        }
                    } else {
                        if (isLoading) {
                            doctorsAdapter!!.removeLoadingFooter()
                            isLoading = false
                            doctorsAdapter!!.add(it!!.data as ArrayList<DoctorRow>)
                        } else {
                            doctorsAdapter!!.updateList(it!!.data as ArrayList<DoctorRow>)
                        }
                        progress.visibility = View.GONE


                    }
                }
                INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE
                }
            }

        })

        setUpPagination()

    }

    private fun setUpPagination() {
        rv_list.addOnScrollListener(object : PaginationScrollListener(rv_list.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                doctorsViewModel.currentPage = doctorsViewModel.currentPage.plus(1)
                if (doctorsViewModel.currentPage <= doctorsViewModel.meta.pagination.totalPages!!) {
                    isLoading = true
                    doctorsAdapter!!.addLoadingFooter()
                    doctorsViewModel.getAlldoctors().observe(this@DoctorsActivity, Observer {
                        when (it!!.status) {
                            SUCCESS -> {
                                if (doctorsAdapter == null) {
                                    doctorsAdapter = DoctorsAdapter((it!!.data as ArrayList<DoctorRow>?)!!, this@DoctorsActivity::onBookClickListner)
                                    rv_list.adapter = doctorsAdapter
                                    doctorsViewModel.meta.pagination = it.meta.pagination
                                } else {
                                    if (isLoading) {
                                        doctorsAdapter!!.removeLoadingFooter()
                                        isLoading = false
                                        if (it.data != null)
                                            doctorsAdapter!!.addAll(it.data as ArrayList<DoctorRow>)
                                    } else {
                                        if (it.data != null)
                                            doctorsAdapter!!.updateList(it!!.data as ArrayList<DoctorRow>)
                                    }

                                }
                            }
                            INTERNAL_SERVER_ERROR -> {
                                Utils.showShortToast(this@DoctorsActivity, getString(R.string.some_thing_went_wrong))
                                progress.visibility = View.GONE
                            }
                        }

                    })
                } else
                    isLastPage = true
            }


        })

    }


    private fun getDataFromIntent() {
        doctorsViewModel.id = intent.getStringExtra(getString(R.string.idKey))

        if (intent.hasExtra(getString(R.string.department_key))) {
            doctorsViewModel.serviceType!!.put(DEPARTMENT_ID, intent.getStringExtra(getString(R.string.department_id)))
        }
    }

    private fun getViewModel() {
        doctorsViewModel = InjectionUtil.getDoctorsViewModel(this)
    }


    private fun onBookClickListner(doctor: DoctorRow) {
        Navigator.navigate(this, arrayOf(getString(R.string.url_key)), arrayOf(doctor.bookButton!!.endpointUrl),
                BookAppointmentsActivity::class.java, BOOK_REQUEST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


    companion object {
        const val BOOK_REQUEST = 1
        const val DEPARTMENT_ID = "department_id"
    }

}
