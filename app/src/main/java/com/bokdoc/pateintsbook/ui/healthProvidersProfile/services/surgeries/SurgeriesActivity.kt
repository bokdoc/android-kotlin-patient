package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.surgeries

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesViewModel
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_surgries.*
import kotlinx.android.synthetic.main.toolbar.*

class SurgeriesActivity : ParentActivity() {

    private lateinit var servicesViewModel: ServicesViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_surgries)
        enableBackButton()
        titleText.text = getString(R.string.surgeries)


        surgeriesList.layoutManager = LinearLayoutManager(this)

        servicesViewModel = InjectionUtil.getServicesViewModel(this, Service.SURGERY_TYPE)

        servicesViewModel.getServices(intent?.getStringExtra(getString(R.string.idKey))!!)

        servicesViewModel.progress.observe(this, Observer {
            progress.visibility = it!!
        })

        servicesViewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        servicesViewModel.servicesList.observe(this, Observer {
            surgeriesList.adapter = ServicesAdapter(it!!) {
                Navigator.navigate(this, arrayOf(""), arrayOf(""), SurgeriesDetailsActivity::class.java)
            }
        })
    }
}
