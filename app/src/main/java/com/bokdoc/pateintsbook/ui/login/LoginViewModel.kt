package com.bokdoc.pateintsbook.ui.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.autoComplete.AutoCompleteRepository
import com.bokdoc.pateintsbook.data.repositories.login.UserLoginRepository
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse

class LoginViewModel(context: Context) : ViewModel() {
    private var loginRepository = UserLoginRepository(context)

    fun login(email: String, password: String): LiveData<DataResource<UserResponse>> {
        return loginRepository.login(email, password)
    }

    fun resetPassword(email: String): LiveData<DataResource<Boolean>> {
        return loginRepository.resetPassword(email)
    }

    fun loginWithSocials(providerName: String, providerId: String, token: String,
                         secret:String="",name:String="",picture:String="",email:String=""): LiveData<DataResource<UserResponse>> {
        return loginRepository.loginWithSocials(providerName, token, providerId,secret,name,picture,email)
    }


}