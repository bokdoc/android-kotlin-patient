package com.bokdoc.pateintsbook.ui.register

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.data.repositories.Sortby.SortByRepositories
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl

class SortbyViewModel(val context: Context) : ViewModel() {
    private var sortRepository = SortByRepositories(context)
    private var sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(context)


    fun getOptions(screenType: String): LiveData<List<TextWithParam>> {
        return sortRepository.getOptions(screenType)
    }


    fun saveSelected(selected: String) {
        sharedPreferencesManagerImpl.saveData(context.getString(R.string.selectedSortId), selected)
    }

    fun getSelected(): String {
        return sharedPreferencesManagerImpl.getData(context.getString(R.string.selectedSortId), "")
    }


    fun getIndexOfSelected(textWithParam: List<TextWithParam>?): Int {
        val size = textWithParam?.size
        val selected = getSelected()
        if (selected.isNotEmpty()) {
            (0 until size!!).forEach { position ->
                textWithParam[position].let {
                    if (it.params == selected)
                        return position
                }
            }
        }
        return -1
    }

}