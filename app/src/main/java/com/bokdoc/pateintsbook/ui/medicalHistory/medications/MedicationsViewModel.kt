package com.bokdoc.pateintsbook.ui.medicalHistory.medications

import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.MedicationsRepository
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel

class MedicationsViewModel(medicationsRepository: MedicationsRepository):
        MedicalHistoryViewModel(medicationsRepository)