package com.bokdoc.pateintsbook.ui.common.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.text_with_line_row.view.*

class TextsArrowsAdapter(private val texts:Array<String>,private val onClickListener:(String)->Unit)
    :RecyclerView.Adapter<TextsArrowsAdapter.TextsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextsViewHolder {
        return TextsViewHolder(parent.inflateView(R.layout.text_arrow_with_line_row))
    }

    override fun getItemCount(): Int {
        return texts.size
    }

    override fun onBindViewHolder(holder: TextsViewHolder, position: Int) {
        holder.itemView.tv_title.text = texts[position]
        if(position == itemCount-1){
            holder.itemView.line.visibility = GONE
        }else{
            holder.itemView.line.visibility = VISIBLE
        }
    }

    inner class TextsViewHolder(view:View):RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                onClickListener.invoke(texts[adapterPosition])
            }
        }
    }
}