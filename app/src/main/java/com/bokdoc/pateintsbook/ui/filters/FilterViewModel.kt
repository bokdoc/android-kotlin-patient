package com.bokdoc.pateintsbook.ui.filters

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.data.repositories.filter.FilterRepositories
import com.bokdoc.pateintsbook.utils.TimesDatesUtils
import com.bokdoc.pateintsbook.utils.constants.StringsConstants

class FilterViewModel(val filterRepositories: FilterRepositories) : ViewModel() {
    lateinit var dateResource: LiveData<DataResource<FiltersSectionHeader>>
    var filtersSelection: HashMap<String, ArrayList<String>>? = null
    var selectedKey = ""
    var selectedPosition = -1
    fun getFilters(screenType: String): LiveData<DataResource<FiltersSectionHeader>> {
        var type = screenType
        if (type == "Online Consulting")
            type = "online_consulting"

        Log.i("screenType", screenType)
        return filterRepositories.getFilters(type)
    }


    fun updateFiltersSelection(key: String, type: String, values: ArrayList<String>) {
        if (filtersSelection == null)
            filtersSelection = HashMap()

        when (type) {
            Filters.CHECKBOX_TYPE -> updateCheckboxesSelections(key, values[0])
            else -> filtersSelection?.getOrPut(key) { values }
        }

    }

    private fun updateCheckboxesSelections(key: String, value: String) {
        val list = filtersSelection?.getOrElse(key) {
            arrayListOf()
        }

        if (list!!.isEmpty()) {
            filtersSelection?.put(key, arrayListOf(value))
        } else {
            val index = list?.indexOf(value)
            if (index == -1) {
                list.add(value)
            } else {
                list!![index!!].let {
                    when (it) {
                        "true", "false" -> {
                            list[index] = (!it.toBoolean()).toString()
                        }
                        else -> {
                            list.removeAt(index)
                        }
                    }
                }
            }
        }

    }


    fun getSelectedDateParts(key:String):List<String>{
        if(filtersSelection!=null && filtersSelection!!.containsKey(key)){
            return TimesDatesUtils.getDateParts(filtersSelection!![key]!![0],StringsConstants.DASH)
        }
        return arrayListOf()
    }

    fun addDate(key:String,type:String,year:Int,monthOfYear:Int,day:Int){
      updateFiltersSelection(key,type,arrayListOf(TimesDatesUtils.getFormattedDayWithSplitter(year,monthOfYear,day,StringsConstants.DASH)))
    }

    fun getMapRange(key:String):String {
        var mapValue="1"
        if (filtersSelection != null && filtersSelection!!.containsKey(key)) {
            mapValue = filtersSelection!![key]!![0]
        }
        if(mapValue.isNotEmpty() && mapValue!="1")
            mapValue = mapValue.substring(mapValue.lastIndexOf(",")+1,mapValue.length)

        return mapValue
    }

    fun getFilterQuery(): String {
        return filterRepositories.getFilterQuery(filtersSelection!!)
    }

    fun resetFilterQueries() {
        filtersSelection = null
        selectedKey = ""
        selectedPosition = -1
    }


}