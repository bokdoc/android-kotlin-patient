package com.bokdoc.pateintsbook.ui.lookups

import android.app.DownloadManager
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.AddLookup
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.ILookupsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.repositories.lookups.LookupsRepository

class LookupsViewModel(private val lookupsRepository: LookupsRepository):ViewModel() {
    val messageErrorLiveData = MutableLiveData<String>()
    val listLiveData = MutableLiveData<List<LookupsType>>()
    val progress = MutableLiveData<Int>()
    lateinit var query:String
    var selectedLookupTypes: ArrayList<ILookupsParcelable> = ArrayList()
    var isMulti = false
    var addLookup = AddLookup()
    var isAddLookup = false

    fun lookups(){
        progress.value = VISIBLE
        lookupsRepository.lookups(query).observeForever {
            if(it?.data!=null){
                listLiveData.value  = it.data
            }else{
                messageErrorLiveData.value = it?.message
            }
            progress.value = GONE
        }
    }

    fun addLookup(): LiveData<DataResource<LookupsType>> {
        return lookupsRepository.addLookups(addLookup)
    }
}