package com.bokdoc.pateintsbook.ui.medicalHistory

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_medical_file.view.*

class MedicalHistoryListAdapter(private var medicalHistories: ArrayList<MedicalHistory>,
                                private val onAdapterClickListener: (MedicalHistory, position: Int, action: Int) -> Unit)
    : RecyclerView.Adapter<MedicalHistoryListAdapter.MedicalHistoryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicalHistoryViewHolder {
        return MedicalHistoryViewHolder(parent.inflateView(R.layout.item_medical_file))
    }

    override fun getItemCount(): Int {
        return medicalHistories.size
    }

    override fun onBindViewHolder(holder: MedicalHistoryViewHolder, position: Int) {
        holder.view.name.text = medicalHistories[position].name
    }

    fun add(medicalHistory: MedicalHistory) {
        medicalHistories.add(medicalHistory)
        notifyItemInserted(itemCount - 1)
    }

    fun addAt(position: Int, medicalHistory: MedicalHistory) {
        medicalHistories.add(position, medicalHistory)
        notifyItemInserted(position)
    }

    fun replaceAt(position: Int, medicalHistory: MedicalHistory, replaceableSize: Int = 2) {
        if (itemCount < replaceableSize) {
            addAt(position, medicalHistory)
        } else
            if (position < itemCount) {
                medicalHistories[position] = medicalHistory
                notifyItemChanged(position)
            }
    }


    fun removeAt(position: Int) {
        if (position < itemCount) {
            medicalHistories.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun update(arrayListMedical: ArrayList<MedicalHistory>) {
        medicalHistories = arrayListMedical
        notifyDataSetChanged()
    }

    fun getAt(position: Int): MedicalHistory {
        return medicalHistories[position]
    }


    inner class MedicalHistoryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.delete_button.setOnClickListener {
                onAdapterClickListener.invoke(medicalHistories[layoutPosition], adapterPosition, ACTION_REMOVED)
                removeAt(layoutPosition)
            }
        }
    }

    companion object {
        const val ACTION_REMOVED = 0
    }

}