package com.bokdoc.pateintsbook.ui.register

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.navigators.LoginNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.login.LoginFragment
import com.bokdoc.pateintsbook.ui.login.LoginFragment.Companion.NO_SHOW_TAG
import com.bokdoc.pateintsbook.ui.login.LoginFragment.Companion.SHOW_TAG
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.LoginSignUpSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import kotlinx.android.synthetic.main.fragment_register.*
import okhttp3.internal.Util
import org.json.JSONArray
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RegisterFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RegisterFragment : Fragment(), GoogleApiClient.OnConnectionFailedListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var registerViewModel: RegisterViewModel
    private lateinit var callbackManager: CallbackManager
    private lateinit var mGoogleApiClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        registerViewModel =
                ViewModelProviders.of(this, CustomRegisterModel(activity!!))
                        .get(RegisterViewModel::class.java)

        callbackManager = CallbackManager.Factory.create()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLoginFacebook()
        setupTwitter()
        setupGmail()
        countryCodePicker.registerPhoneNumberTextView(phoneNumberEdit)
        phoneNumberEdit.hint = getString(R.string.mobile)
        alreadyHaveAccount.movementMethod = LinkMovementMethod.getInstance()
        alreadyHaveAccount.text = LoginSignUpSpannable.getClickableSpannable(getString(R.string.alreadyHaveAccount), getString(R.string.signIn),
                ContextCompat.getColor(activity!!, R.color.colorHomeCardsYellowBorder)) {

            val loginFragment = LoginFragment.newInstance("", "")
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)

            transaction.replace(R.id.lay_main_container, loginFragment)
            transaction.commit()
            // activity!!.finish()

        }
        iv_show_password.setOnClickListener {
            when (iv_show_password.tag) {
                SHOW_TAG -> {
                    iv_show_password.tag = NO_SHOW_TAG
                    passwordEdit.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    iv_show_password.setImageResource(R.drawable.ic_hide_password)
                }
                NO_SHOW_TAG -> {
                    iv_show_password.tag = SHOW_TAG
                    passwordEdit.transformationMethod = PasswordTransformationMethod.getInstance()
                    iv_show_password.setImageResource(R.drawable.ic_show_password)
                }
            }
        }

        register.setOnClickListener {
            if (validateData()) {
                showProgress()
                registerViewModel.register(emailEdit.text.toString(), passwordEdit.text.toString(),
                        countryCodePicker.fullNumberWithPlus, fullNameEdit.text.toString()).observe(activity!!, Observer {
                    hideProgress()
                    if (it?.status == DataResource.SUCCESS) {
                        if (it.meta.message.isNotEmpty()) {
                            if (context != null)
                                Utils.showLongToast(context!!, it.meta.message)
                        }

                        if(activity!=null)
                            LocaleManager.setLocale(activity?.baseContext!!)

                        Navigator.navigate(activity!!, MainActivity::class.java)
                        activity!!.finishAffinity()


                    } else {
                        if (context != null) {
                            if (!it?.meta?.message.isNullOrEmpty()) {
                                Utils.showLongToast(context!!, it?.meta!!.message)
                            } else {
                                Utils.showLongToast(context!!,R.string.server_error)
                            }
                        }
                    }
                })
            }
        }

        facebookLoginText.setOnClickListener {
            facebookRegisterButton.performClick()
        }

        twitterLoginText.setOnClickListener {
            twitterRegisterButton.performClick()
        }

        googleLoginText.setOnClickListener {
            signInGmail()
        }

        setupHintTextColors()
        setupEditTextsWatchers()
    }

    private fun setupHintTextColors() {
        if (activity == null)
            return

        fullNameEdit.hint = getString(R.string.fullName)

        emailEdit.hint = getString(R.string.emailWithDots)


        passwordEdit.hint = getString(R.string.password)
    }

    private fun setupEditTextsWatchers() {
        fullNameEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fullNameEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                setupHintTextColors()
            }

        })
        passwordEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                setupHintTextColors()
            }
        })
        phoneNumberEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneNumberEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                setupHintTextColors()
            }
        })
    }

    private fun setupLoginFacebook() {
        facebookRegisterButton.fragment = this
        facebookRegisterButton.setReadPermissions(listOf(LoginFragment.EMAIL, LoginFragment.PUBLIC_PROFILE))
        facebookRegisterButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                // loginViewModel.loginWithSocials(getString(R.string.facebook))
                showProgress()
                val graphRequest = GraphRequest.newMeRequest(
                        result?.accessToken, object : GraphRequest.GraphJSONArrayCallback, GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(jsonObject: JSONObject?, response: GraphResponse?) {
                        sendLoginRequest(result?.accessToken?.token, result?.accessToken?.userId,
                                jsonObject?.optString("name"), jsonObject?.optString("email"),
                                jsonObject?.optString("picture"), "")
                    }

                    override fun onCompleted(objects: JSONArray?, response: GraphResponse?) {
                        Log.i("", "")
                    }

                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email")
                graphRequest.parameters = parameters
                graphRequest.executeAsync()
                graphRequest.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
                hideProgress()
                Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
            }

        })
    }


    private fun sendLoginRequest(accessToken: String?, providerId: String?, name: String?, email: String?, picture: String?, secretKey: String = "") {
        if (accessToken != null && providerId != null && name != null && picture != null && email != null) {
            registerViewModel.registerWithSocials(getString(R.string.facebook), providerId, accessToken, secretKey, name, picture, email
            ).observe(activity!!, Observer<DataResource<UserResponse>> {
                hideProgress()
                if (it?.status == DataResource.SUCCESS) {
                    if (it.meta.message.isNotEmpty()) {
                        if (context != null)
                            Utils.showLongToast(context!!, it.meta.message)
                    }

                    if(activity!=null)
                        LocaleManager.setLocale(activity?.baseContext!!)

                    Navigator.navigate(activity!!, MainActivity::class.java)
                    activity!!.finishAffinity()
                } else {
                    if (context != null) {
                        if (!it?.meta?.message.isNullOrEmpty()) {
                            Utils.showLongToast(context!!, it?.meta!!.message)
                        } else {
                            Utils.showLongToast(context!!,R.string.server_error)
                        }
                    }
                }
            })
        }
    }

    private fun setupTwitter() {
        twitterRegisterButton.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>?) {
                showProgress()
                if (result == null) {
                    showProgress()
                    Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                    return
                }

                TwitterAuthClient().requestEmail(result?.data, object : com.twitter.sdk.android.core.Callback<String>() {
                    override fun success(emailResult: Result<String>) {
                        val email = emailResult.data
                        if (email != null && result.data?.authToken != null) {
                            registerViewModel.registerWithSocials(getString(R.string.twitter), result.data?.userId.toString(),
                                    result.data?.authToken!!.token, result.data?.authToken!!.secret, result.data.userName, "", email)
                                    .observe(activity!!, Observer<DataResource<UserResponse>> {
                                        hideProgress()
                                        if (it?.status == DataResource.SUCCESS) {
                                            if (it.meta.message.isNotEmpty()) {
                                                if (context != null)
                                                    Utils.showLongToast(context!!, it.meta.message)
                                            }
                                            if(activity!=null)
                                                LocaleManager.setLocale(activity?.baseContext!!)

                                            Navigator.navigate(activity!!, MainActivity::class.java)
                                            activity!!.finishAffinity()
                                        } else {
                                            if (context != null) {
                                                if (!it?.meta?.message.isNullOrEmpty()) {
                                                    Utils.showLongToast(context!!, it?.meta!!.message)
                                                } else {
                                                    Utils.showLongToast(context!!,R.string.server_error)
                                                }
                                            }
                                        }
                                    })
                        } else {
                            hideProgress()
                            Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                        }

                    }

                    override fun failure(e: TwitterException) {
                        hideProgress()
                        Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                    }
                })


            }

            override fun failure(exception: TwitterException?) {
                hideProgress()
                Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
            }

        }

    }

    private fun setupGmail() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .build()
        mGoogleApiClient = GoogleSignIn.getClient(activity!!, gso)


    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private fun signInGmail() {
        val signInIntent = mGoogleApiClient.signInIntent
        startActivityForResult(signInIntent, LoginFragment.RC_SIGN_IN)

    }

    fun showProgress() {
        view_progress.visibility = View.VISIBLE
        activity!!.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun hideProgress() {
        view_progress.visibility = View.GONE
        activity!!.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun validateData(): Boolean {
        var isVaild = true
        val name = fullNameEdit.text.toString()
        if (name.isEmpty()) {
            isVaild = false
            isVaild = false
            fullNameEdit.setHintTextColor(resources.getColor(R.color.colorErrorRed))
            //fullNameInput.error = getString(R.string.errorFillField)
        }
        val password = passwordEdit.text
        if (password.isEmpty()) {
            isVaild = false
            passwordEdit.setHintTextColor(resources.getColor(R.color.colorErrorRed))
            // passwordInput.error = getString(R.string.errorFillField)
        }

        val phone = phoneNumberEdit.text
        if (phone != null) {
            if (phone.isEmpty()) {
                isVaild = false
                //phoneNumberEdit.error = getString(R.string.errorFillField)
                if (activity != null)
                    phoneNumberEdit.setHintTextColor(resources.getColor(R.color.colorErrorRed))
            }
        }
//
//        val email = emailEdit.text
//        if (email.isEmpty()) {
//            isVaild = false
//            //emailInput.error = getString(R.string.errorFillField)
//            if(activity!=null)
//            emailEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!,R.color.colorErrorRed))
//        } else
//            if (!com.bokdoc.pateintsbook.utils.validate.Validator.validateEmail(email.toString())) {
//                isVaild = false
//                //emailInput.error = getString(R.string.errorEmailNotValid)
//                if(activity!=null)
//                Utils.showLongToast(activity?.applicationContext!!,R.string.errorEmailNotValid)
//            }
        return isVaild
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            // throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == LoginFragment.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task?.getResult(ApiException::class.java)
                showProgress()
                registerViewModel.registerWithSocials(getString(R.string.google), account?.id!!,
                        account.idToken!!, "", account.displayName!!,
                        account.photoUrl?.toString()!!, account.email!!).observe(activity!!,
                        Observer<DataResource<UserResponse>> {
                            if (it?.status == DataResource.SUCCESS) {
                                if (it.meta.message.isNotEmpty()) {
                                    if (context != null)
                                        Utils.showLongToast(context!!, it.meta.message)

                                }
                                if(activity!=null)
                                    LocaleManager.setLocale(activity?.baseContext!!)

                                Navigator.navigate(activity!!, MainActivity::class.java)
                                activity!!.finish()
                            } else {
                                if (context != null) {
                                    if (!it?.meta?.message.isNullOrEmpty()) {
                                        Utils.showLongToast(context!!, it?.meta!!.message)
                                    } else {
                                        Utils.showLongToast(context!!,R.string.server_error)
                                    }
                                }
                            }
                        })
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                // ...
                Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data)
        twitterRegisterButton.onActivityResult(requestCode, resultCode, data)
        //mTwitterAuthClient.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                RegisterFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
