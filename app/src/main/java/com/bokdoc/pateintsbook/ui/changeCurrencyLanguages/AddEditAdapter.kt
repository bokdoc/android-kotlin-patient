package com.bokdoc.pateintsbook.ui.changeCurrencyLanguages

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.AdapterView

import android.widget.ArrayAdapter
import android.widget.AdapterView.OnItemSelectedListener
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.IAddEditRow
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide

import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_text_with_arrow.view.*


class AddEditAdapter(private val addEditsRows: List<IAddEditRow>,
                     private val rowCLickListener: (position: Int, slug: String) -> Unit = { _, _ -> }
                     , private val editValueListener: (slug: String, text: String) -> Unit = { _, _ -> },
                     private val switchValueListener: (slug: String, value: Boolean) -> Unit = { s: String, b: Boolean -> ""; true },
                     private val rangesEditValueListener: (slug: String, text: String, rangesType: String) -> Unit = { _, _, _ -> })
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var isSwitchInvoked = false

    var onBind = false

    var selectedView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

          return TextArrowViewHolder(parent.inflateView(R.layout.item_text_with_arrow))
    }

    override fun getItemCount(): Int {
        return addEditsRows.size
    }

    fun updateBackground(position: Int, backgroundColor: Int = R.color.colorTransparent) {
        addEditsRows[position].background = backgroundColor
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            IAddEditRow.TEXT_ARROW_ROW -> (viewHolder as TextArrowViewHolder).bind(addEditsRows[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return addEditsRows[position].type
    }

    fun updateEditText(position: Int, text: String) {
        addEditsRows[position].editTextValue = text
        notifyDataSetChanged()
    }

    fun addAddEditRow(position: Int, addEditRow: IAddEditRow) {
        if (!(addEditsRows as MutableList<IAddEditRow>).contains(addEditRow)) {
            addEditsRows.add(position, addEditRow)
            notifyItemInserted(position)
        }
    }

    fun updateVisibility(position: Int, visibility: Boolean) {
        addEditsRows[position].visibility = visibility
        notifyDataSetChanged()
    }


    fun updateSwitchValues(position: List<Int>, values: List<Boolean>) {
        position.forEach {
            addEditsRows[it].switchValue = values[it]
        }
        notifyDataSetChanged()
    }

    fun updateSwitchValue(position: Int, value: Boolean) {
        addEditsRows[position].switchValue = value
        notifyDataSetChanged()
    }

    fun updateIsFieldEnabled(position: Int, value: Boolean) {
        addEditsRows[position].isFieldEnabled = value
        notifyDataSetChanged()
    }

    fun removeAddEditRow(position: Int) {
        (addEditsRows as MutableList<*>).removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateSelectedText(position: Int, selectedText: String) {
        addEditsRows[position].selectedText = selectedText
        notifyDataSetChanged()
    }

    fun updateSelectedNameText(position: Int, selectedText: String) {
        addEditsRows[position].name = selectedText
        notifyDataSetChanged()
    }

    fun select(position: Int) {
        addEditsRows[position].isSelected = true
        addEditsRows[position].nameStatus = IAddEditRow.NAME_STATUS_SELECTED
        notifyDataSetChanged()
    }

    fun unSelect(position: Int) {
        addEditsRows[position].isSelected = false
        addEditsRows[position].nameStatus = IAddEditRow.NAME_STATUS_SELECTED
        notifyDataSetChanged()

    }

    fun changeNameStatus(position: List<Int>, status: Int = IAddEditRow.NAME_STATUS_REQUIRED) {
        position.forEach {
            addEditsRows[it].nameStatus = status
        }
        notifyDataSetChanged()
    }

    fun updateNameWithSelected(position: Int,text: String){
        addEditsRows[position].name = text
        addEditsRows[position].isSelected = true
        addEditsRows[position].nameStatus = IAddEditRow.NAME_STATUS_SELECTED
        notifyDataSetChanged()
    }



    inner class TextArrowViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            itemView.setOnClickListener {
                rowCLickListener.invoke(adapterPosition, addEditsRows[adapterPosition].slug)
            }
        }

        fun bind(addEditRow: IAddEditRow) {
            itemView.tv_text.text = addEditRow.name

            if (addEditRow.rowDescription.isNotEmpty()) {
                itemView.tv_item_arrow_description.visibility = VISIBLE
                itemView.tv_item_arrow_description.text = addEditRow.rowDescription
            }


            if (addEditRow.visibility) {
                itemView.visibility = VISIBLE
            } else {
                itemView.visibility = GONE
            }

            /*
            if (addEditRow.description.isNotEmpty()) {
                itemView.tv_item_arrow_description.visibility = VISIBLE
                itemView.tv_item_arrow_description.text = addEditRow.description
            } else {
                itemView.tv_item_arrow_description.visibility = GONE
            }
            */
            if (addEditRow.selectedText.isNotEmpty()) {
                itemView.tv_text_selected.visibility = VISIBLE
                itemView.tv_text_selected.text = addEditRow.selectedText
            } else {
                itemView.tv_text_selected.visibility = GONE
            }

            if (addEditRow.isSelected) {
                itemView.iv_tick.visibility = VISIBLE
            } else {
                itemView.iv_tick.visibility = GONE
            }


            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, addEditRow.background))
        }
    }


}