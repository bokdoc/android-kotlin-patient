package com.bokdoc.pateintsbook.ui.twilioVideoConsulting

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import kotlinx.android.synthetic.main.activity_dialing_activity.*
import android.view.WindowManager



class DialingActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            iv_end_call.id -> {
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)

        setContentView(R.layout.activity_dialing_activity)


        var mediaPlayer = MediaPlayer.create(this, R.raw.ringing)
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        mediaPlayer.isLooping = true
        mediaPlayer.start()

        iv_end_call.setOnClickListener(this)
    }


}