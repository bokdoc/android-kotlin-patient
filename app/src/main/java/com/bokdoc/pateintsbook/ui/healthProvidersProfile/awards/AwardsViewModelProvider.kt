package com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.awards.AwardsRepository

class AwardsViewModelProvider(private val awardsRepository: AwardsRepository):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AwardsViewModel(awardsRepository) as T
    }

}