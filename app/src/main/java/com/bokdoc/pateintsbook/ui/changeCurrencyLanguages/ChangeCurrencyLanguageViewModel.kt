package com.bokdoc.pateintsbook.ui.changeCurrencyLanguages

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.CurrencyLanguageRow
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.repositories.changeCurrencyLanguage.ChangeCurrencyLanguageRepository
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import java.lang.ref.WeakReference


class ChangeCurrencyLanguageViewModel(private val context: WeakReference<Context>
                                      , val changeCurrencyLanguageRepository: ChangeCurrencyLanguageRepository) : ViewModel() {

    var currencyLookup: LookUpsParcelable? = LookUpsParcelable()
    var languageLookup: LookUpsParcelable? = LookUpsParcelable()
    var countryLookup: LookUpsParcelable? = LookUpsParcelable()
    var selectedPosition = -1
    var languageKey = ""
    var isRefreshLayout = false

    init {
        getUserScope()
    }

    fun getRows(): List<CurrencyLanguageRow> {
        return ArrayList<CurrencyLanguageRow>().apply {
            add(CurrencyLanguageRow().apply {
                slug = CurrencyLanguageRow.CURRENCY_SLUG
                name = currencyLookup!!.name
                isSelected = currencyLookup!!.idLookup.isNotEmpty()
            })

            add(CurrencyLanguageRow().apply {
                slug = CurrencyLanguageRow.COUNTRY_SLUG
                name =  countryLookup!!.name
                isSelected = countryLookup!!.idLookup.isNotEmpty()
            })

            add(CurrencyLanguageRow().apply {
                slug = CurrencyLanguageRow.LANGUAGE_SLUG
                name = languageLookup!!.name
                isSelected = languageLookup!!.idLookup.isNotEmpty()
            })
        }
    }

    fun getUserScope(){
        currencyLookup = changeCurrencyLanguageRepository.userSharedPreferences.userCurrency
        languageLookup = changeCurrencyLanguageRepository.userSharedPreferences.userLanguage
        countryLookup = changeCurrencyLanguageRepository.userSharedPreferences.userCountry
    }



    fun save(): LiveData<DataResource<UserResponse>> {
        return changeCurrencyLanguageRepository.change(currencyLookup!!, languageLookup!!, countryLookup!!)
    }

}