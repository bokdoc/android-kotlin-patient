package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileGallery.ProfileGalleryRepository

class ProfileGalleryModelProvider(private val repository: ProfileGalleryRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProfileGalleryViewModel(repository) as T
    }
}