package com.bokdoc.pateintsbook.ui.updateProfile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.User
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.repositories.attachmentMedia.AttachmentsMediaRepository
import com.bokdoc.pateintsbook.data.repositories.updateProfile.UpdateProfileRepository
import java.io.File

class UpdateProfileViewModel(val updateProfileRepository: UpdateProfileRepository) : ViewModel() {

    var user = updateProfileRepository.getUser()

    //camera intent path
    var cameraIntentPath: File? = null

    var profileImages: MutableList<com.esafirm.imagepicker.model.Image> = ArrayList()

    var isDataTimeDialogShow = false

    fun update(context: Context): LiveData<DataResource<User>> {
        if(user.gender == context.getString(R.string.male)){
            user.gender = User.MALE
        }else if(user.gender == context.getString(R.string.female)){
            user.gender = User.FEMALE
        }
        return updateProfileRepository.update(user)
    }

    fun getPicturesFromMedia(media: List<IMedia>): ArrayList<Picture> {
        val pictures = ArrayList<Picture>()
        media.forEach {
            pictures.add(Picture().apply {
                mediaType = it.mediaType
                url = it.url
                mediaId = it.mediaId
            })
        }
        return pictures
    }


}