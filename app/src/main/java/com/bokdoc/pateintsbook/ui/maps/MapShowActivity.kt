package com.bokdoc.pateintsbook.ui.maps

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.FAIL
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.ProvidersProfileRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView.OverViewWebService
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewRepository
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_location_maps.*
import com.google.android.gms.maps.model.LatLngBounds


class MapShowActivity : ParentActivity(), OnMapReadyCallback {
    var mapView: View? = null
    private var latLng: LatLng? = null
    private lateinit var mMap: GoogleMap
    var myMarker: Marker? = null
    var location: Location? = null
    private lateinit var providersViewModel: HealthProvidersViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_maps)
        enableBackButton()
        getViewModel()

        title = getString(R.string.clinicLocation)
        hideLocationOptions()
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.view
        getLocationFromIntent()
    }

    private fun getViewModel() {
        providersViewModel = ViewModelProviders.of(this, HealthProvidersModelProvider(ProvidersProfileRepository(this.application,
                SharedPreferencesManagerImpl(this)),
                ProfileSocialActionsRepository(applicationContext, SharedPreferencesManagerImpl(this), CompareSharedPreferences(this),
                        ProfileSocailActionsWebservice(WebserviceManagerImpl.instance())),
                OverViewRepository(this, OverViewWebService(WebserviceManagerImpl.instance()), UserSharedPreference(this)))).get(HealthProvidersViewModel::class.java)
    }


    private fun getLocationFromIntent() {
        if (intent.hasExtra(getString(R.string.locatioKey))) {
            location = intent.getParcelableExtra(getString(R.string.locatioKey))
        } else if (intent.hasExtra(getString(R.string.idKey))) {
            providersViewModel.profileId = intent.getStringExtra(getString(R.string.idKey))
        }
    }

    private fun hideLocationOptions() {
        btn_select_location.visibility = View.GONE
        iv_location.visibility = View.GONE
        frame_place.visibility = View.GONE
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        mMap.setMinZoomPreference(1F)
        mMap.setMaxZoomPreference(13F)
        if (location != null && location!!.latitude.isNotEmpty() && location!!.longitude.isNotEmpty()) {
            drawCircle(location!!.latitude.toDouble(), location!!.longitude.toDouble(), 2000.0)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude.toDouble(), location!!.longitude.toDouble()), 13F))
        }
        if (providersViewModel.profileId.isNotEmpty()) {
            getLocations()
        }
    }

    private fun getLocations() {
        view_progress.visibility = View.VISIBLE
        providersViewModel.getProfileAddress().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    view_progress.visibility = View.GONE

                    it.data!!.forEach {
                        drawCircle(it.latitude.toDouble(), it.longitude.toDouble(), 2000.0)
                    }

                    val builder = LatLngBounds.Builder()
                    it.data!!.forEach {

                        builder.include(LatLng(it.latitude.toDouble(), it.longitude.toDouble()))
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 0))

                }
                FAIL -> {
                    view_progress.visibility = View.GONE

                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
                INTERNAL_SERVER_ERROR -> {
                    view_progress.visibility = View.GONE


                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
            }

        })

    }


    private fun generateBitmapDescriptorFromRes(resId: Int): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, resId)
        drawable!!.setBounds(
                0,
                0,
                drawable.intrinsicWidth,
                drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun drawCircle(Lat: Double?, Lng: Double?, radius: Double) {
        mMap.addCircle(CircleOptions().center(LatLng(Lat!!, Lng!!)).radius(radius)
                .strokeColor(ContextCompat.getColor(this, R.color.colorMapCircle))
                .strokeWidth(2F).fillColor(ContextCompat.getColor(this, R.color.colorMapCircle)))
    }

    private fun drawCurrentLocation(Lat: Double?, Lng: Double?, zoom: Int) {
        myMarker = mMap.addMarker(MarkerOptions()
                .position(LatLng(Lat!!, Lng!!)).draggable(true))
        myMarker!!.position = LatLng(Lat, Lng)
        myMarker!!.setIcon(generateBitmapDescriptorFromRes(R.drawable.ic_map_marker_default))
        val cameraPosition = CameraPosition.Builder()
                .target(LatLng(Lat, Lng)).zoom(zoom.toFloat()).build()
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition))
    }


}