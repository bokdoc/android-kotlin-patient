package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accommodation.AccommodationRepository

class AccommodationModelProvider(private val repository: AccommodationRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccommodationViewModel(repository) as T
    }
}