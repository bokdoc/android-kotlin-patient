package com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil

class OverViewActivity : ParentActivity() {

    lateinit var overViewViewModel: OverViewViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_providers_profile)
        title = getString(R.string.overView)
        enableBackButton()

        getViewModel()

        overViewViewModel.getProfileOverView().observe(this, Observer {

        })
    }

    private fun getViewModel() {
        overViewViewModel = InjectionUtil.getPOverViewViewModel(this)
        overViewViewModel.profileId = intent.getStringExtra(getString(R.string.idKey))
    }
}