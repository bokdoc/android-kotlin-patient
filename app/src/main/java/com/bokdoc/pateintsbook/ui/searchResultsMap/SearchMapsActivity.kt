package com.bokdoc.pateintsbook.ui.searchResultsMap

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.ref.WeakReference
import android.support.v4.content.ContextCompat
import android.graphics.drawable.Drawable
import com.google.android.gms.maps.model.BitmapDescriptor
import kotlinx.android.synthetic.main.activity_maps.*
import com.hanks.htextview.base.DisplayUtils.getDisplayMetrics
import android.view.ViewGroup
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsActivity


class SearchMapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var mMapViewholder: SearchMapsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        titleText.text = getString(R.string.mapView)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }

        val profiles = intent?.getParcelableArrayListExtra<Profile>(getString(R.string.profiles))
        mMapViewholder = ViewModelProviders.of(this).get(SearchMapsViewModel::class.java)
        mMapViewholder.profiles = profiles!!
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


    }

    private fun drawMarkers() {
        val locations = mMapViewholder.getLocations()
        if(!locations.isNullOrEmpty()) {
            rightTextView.visibility = VISIBLE
            rightTextView.text = getString(R.string.resultsPlaceHolder, locations.size.toString())
            var marker: Marker? = null
            val size = locations.size
            (0 until size).forEach {
                marker = mMap.addMarker(MarkerOptions().position(locations[it]).icon(
                        generateBitmapDescriptorFromRes(mMapViewholder.getMarkerImage(mMapViewholder.getProfileAt(it).profileTypeId))))

                marker?.tag = mMapViewholder.getProfileAt(it)
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker?.position, 14.0f))
        }
    }

    private fun generateBitmapDescriptorFromRes(resId: Int): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, resId)
        drawable!!.setBounds(
                0,
                0,
                drawable.intrinsicWidth,
                drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMinZoomPreference(1F)
        mMap.setMaxZoomPreference(13F)
        map_relative_layout.init(googleMap, getPixelsFromDp(this, 39f + 20f))
        mMap.setInfoWindowAdapter(CustomInfoWindowSearch(WeakReference(this), WeakReference(map_relative_layout)))
        mMap.setOnInfoWindowClickListener {
            if (it != null) {
                val profile = (it.tag as Profile)
                val bookButton = profile.bookButton
                bookButton?.let {
                    if (bookButton.isTargetBookAppointments) {
                        Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                                arrayOf(bookButton.endpointUrl),
                                BookAppointmentsActivity::class.java, BOOK_REQUEST)
                    } else {
                        Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                                getString(R.string.docNameSlug), getString(R.string.docPicSlug),
                                getString(R.string.profileKey)),
                                arrayOf(bookButton.endpointUrl,
                                        profile.name, profile.picture.url,
                                        profile.profileTypeId.toString()),
                                LinksCardsDetailsActivity::class.java, BOOK_REQUEST)
                    }
                }
            }
        }
        drawMarkers()
        // Add a marker in Sydney and move the camera
        /*
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        */
    }

    private fun getPixelsFromDp(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == BOOK_REQUEST && resultCode== Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


    companion object {
        const val BOOK_REQUEST = 1
    }

}
