package com.bokdoc.pateintsbook.ui.splash

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.FAIL
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.navigators.TutorialsNavigator
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService.Companion.NOTIFICATION_TYPE_MESSAGE
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService.Companion.PAYMENT_TYPE_MESSAGE
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService.Companion.SCREEN_TYPE_MESSAGE
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService.Companion.SCREEN_TYPE_REQUEST
import com.bokdoc.pateintsbook.notifications.MyFirebaseMessagingService.Companion.SEARCH_TYPE_MESSAGE
import com.bokdoc.pateintsbook.notifications.notificationParser.NotificationParser
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioLoadingActivity
import com.bokdoc.pateintsbook.utils.DeviceIdGeneration
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


class SplashActivity : AppCompatActivity() {

    private lateinit var splashViewModel: SplashViewModel
    private lateinit var webserviceManagerImpl: WebserviceManagerImpl
    val handler = Handler()
    var runnable: Runnable? = null
    var paymentUrl: String = "https://payment.bokdoc.com/view-options/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkIsLiveForFabrics()
        getViewModel()
        handelPushNotification()

//        runnable = Runnable {
//            if (splashViewModel.isTokenEndCalled) {
//                if (splashViewModel.isOpenTutorials()) {
//                    splashViewModel.updateFirstAppOpenedInSharedPreferences()
//                    TutorialsNavigator.navigate(this@SplashActivity)
//                } else {
//                    Navigator.navigate(this, MainActivity::class.java)
//                }
//                finish()
//            }
//
//        }

    }

    private fun checkIsLiveForFabrics() {
        webserviceManagerImpl = WebserviceManagerImpl()
        if (!webserviceManagerImpl.isStaging()) {
            Fabric.with(this, Crashlytics())
        }
    }

    private fun handelPushNotification() {
        val bundle = intent.extras
        if (bundle != null) {
            val type = bundle.getString("screen_type")
            when (type) {
                SEARCH_TYPE_MESSAGE -> {
                    Navigator.navigate(this, arrayOf(getString(R.string.screenType), getString(R.string.query)),
                            arrayOf("online_consulting", ""), SearchResultsActivity::class.java, HomeSearchActivity.SEARCH_REQUEST)
                    finish()
                }
                SCREEN_TYPE_REQUEST -> {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra(getString(R.string.requests_screen), getString(R.string.requests_screen))
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                }
                PAYMENT_TYPE_MESSAGE -> {
                    val url = intent.extras!!.getString("params")
                    val notification = NotificationParser.parseNotification(url)
                    paymentUrl += notification.id + "?is_payment=true"
                    val intent = Intent(this, WebViewActivity::class.java)
                    intent.putExtra(getString(R.string.url_key), paymentUrl)
                    intent.putExtra(getString(R.string.title_key), getString(R.string.payment))
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                }
                SCREEN_TYPE_MESSAGE -> {
                    val intent = Intent(this, WebViewActivity::class.java)
                    val url = bundle.getString("url")
                    intent.putExtra(getString(R.string.url_key), url)
                    intent.putExtra(getString(R.string.title_key), getString(R.string.chat_with_doctor))
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                }
                NOTIFICATION_TYPE_MESSAGE -> {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra(getString(R.string.notification_screen), getString(R.string.notification_screen))
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                MyFirebaseMessagingService.CONSULTING_TYPE -> {

                    val url = intent.extras!!.getString("params")
                    val notification = NotificationParser.parseNotification(url)
                    Navigator.navigate(this,
                            arrayOf(getString(R.string.request_id_key), getString(R.string.twillio_token_key)),
                            arrayOf(notification.id, notification.token),
                            TwillioLoadingActivity::class.java)
                    finish()
                }
                else -> {
                    TokenRepository(this)
                            .getToken(DeviceIdGeneration.generateDeviceId(this, SharedPreferencesManagerImpl(this))).observe(this, Observer {
                                when (it!!.status) {
                                    DataResource.SUCCESS -> {
                                        LocaleManager.setLocale(baseContext)
                                        checkForUpdate()
                                    }
                                    DataResource.FAIL -> {

                                    }
                                }


                            })

                }
            }
        }


    }

    fun getUrlWithToken(url: String): String {
        val userSharedPreferences = UserSharedPreference(applicationContext)
        return """$url&&token=${userSharedPreferences.getToken()}"""
    }

    private fun getViewModel() {
        splashViewModel = ViewModelsCustomProvider(SplashViewModel(SplashRepository(this,
                UserSharedPreference(this), AddFcmWebService(WebserviceManagerImpl()))))
                .create(SplashViewModel::class.java)
    }

    private fun checkForUpdate() {
        splashViewModel.checkUpdate().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    navigate(it)
                }
                FAIL -> {
                    navigate(it)
                }
            }
        })
    }

    private fun navigate(checkUpdate: DataResource<CheckUpdate>?) {
        splashViewModel.isTokenEndCalled = true
        if (splashViewModel.isOpenTutorials()) {
            splashViewModel.updateFirstAppOpenedInSharedPreferences()
            TutorialsNavigator.navigate(this@SplashActivity)
            finish()
        } else {
            if (checkUpdate?.data != null && checkUpdate.data?.isNotEmpty()!!) {
                Navigator.navigate(this, getString(R.string.check_key),
                        checkUpdate.data!![0], MainActivity::class.java)
                finish()
            } else {
                Navigator.navigate(this, MainActivity::class.java)
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnable, SPLASH_TIME)
    }


    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(runnable)
    }

    companion object {
        const val SPLASH_TIME = 2000L
    }
}
