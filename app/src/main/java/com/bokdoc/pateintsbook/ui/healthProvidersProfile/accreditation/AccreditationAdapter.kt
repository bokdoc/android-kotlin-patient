package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accreditation

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_clinic.view.*

class AccreditationAdapter(private val accreditations: List<AccreditationRow>) : RecyclerView.Adapter<AccreditationAdapter.AccreditationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): AccreditationViewHolder {
        return AccreditationViewHolder(parent.inflateView(R.layout.item_clinic))
    }

    override fun getItemCount(): Int {
        return accreditations.size
    }

    override fun onBindViewHolder(holder: AccreditationViewHolder, position: Int) {
        accreditations[position].let {

        }
    }


    inner class AccreditationViewHolder(view: View) : RecyclerView.ViewHolder(view)
}