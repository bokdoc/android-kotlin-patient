package com.bokdoc.pateintsbook.ui.medicalHistory.speciality

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil

class SpecialityActivity: MedicalHistoryActivity() {
    override fun getViewModel(): MedicalHistoryViewModel {
        return InjectionUtil.getSpecialitiesViewModel(this)
    }

    override fun getLookupType(): String {
        return getString(R.string.lookupSpecialities)
    }

    override fun title(): String {
        return getString(R.string.mySpecialities)
    }

    override fun emptyDataMessage(): String {
        return getString(R.string.specialities_profile_empty_text)
    }
}