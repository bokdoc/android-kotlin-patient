package com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.common.adapters.TextsArrowsAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details.QualificationsDetailsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import kotlinx.android.synthetic.main.activity_qualifications.*
import kotlinx.android.synthetic.main.toolbar.*

class QualificationsActivity : ParentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualifications)
        titleText.text = getString(R.string.qualifications)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
        qualificationsList.layoutManager = LinearLayoutManager(this)
        qualificationsList.adapter = TextsArrowsAdapter(resources.getStringArray(R.array.qualificationSections), this::onAdapterClickListener)
    }

    private fun onAdapterClickListener(selectedText: String) {
        Navigator.navigate(this, arrayOf(getString(R.string.selectedTextKey), getString(R.string.idKey)),
                arrayOf(selectedText, intent?.getStringExtra(getString(R.string.idKey))!!), QualificationsDetailsActivity::class.java)
    }

}
