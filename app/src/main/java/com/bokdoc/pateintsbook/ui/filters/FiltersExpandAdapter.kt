package com.bokdoc.pateintsbook.ui.filters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.data.models.filters.expandFilters.FiltersExpand
import com.bokdoc.pateintsbook.utils.CustomCrystalRangeSeekbar
import com.bokdoc.pateintsbook.utils.TimesDatesUtils
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.filter_child.view.*
import kotlinx.android.synthetic.main.filter_row.view.*
import java.util.ArrayList
import kotlin.math.roundToInt




class FiltersExpandAdapter(groups: List<ExpandableGroup<*>>, var itemCLickListener: (key: String, type: String, values: ArrayList<String>) -> Unit)
    : ExpandableRecyclerViewAdapter<FiltersExpandAdapter.FiltersGroupViewHolder, FiltersChildrenViewHolder>(groups), DatePickerDialog.OnDateSetListener {


    var oldSelectedFlatPosition = -1

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): FiltersGroupViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.filter_row, parent, false)
        return FiltersGroupViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): FiltersChildrenViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.filter_child, parent, false)
        return FiltersChildrenViewHolder(view)
    }

    override fun onBindChildViewHolder(holder: FiltersChildrenViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?, childIndex: Int) {
        val filterHeader = (group as FiltersExpand)
        val filtersChildren = filterHeader.options[childIndex]
        when (filterHeader.dataType) {

            Filters.CHECKBOX_TYPE -> {
                holder?.itemView?.text?.visibility = View.VISIBLE
                holder?.itemView?.text?.text = filtersChildren.name
                if (filtersChildren.isSelected) {
                    holder?.itemView?.iv_tick?.visibility = View.VISIBLE
                } else {
                    holder?.itemView?.iv_tick?.visibility = View.GONE
                }
                holder?.itemView?.ranger?.visibility = View.GONE
                holder?.itemView?.rating?.visibility = View.GONE
                holder?.itemView?.select_your_rating?.visibility = View.GONE


                holder?.itemView?.setOnClickListener {
                    if (filterHeader.key == Filters.FREE_APPOINTMENT || filterHeader.key == Filters.IS_HOME_VISIT) {
                        itemCLickListener.invoke(filterHeader.key, filterHeader.dataType,
                                arrayListOf(filtersChildren.isSelected.toString()))
                    } else {
                        itemCLickListener.invoke(filterHeader.key, filterHeader.dataType,
                                arrayListOf(filtersChildren.params))
                    }

                    filtersChildren.isSelected.let {
                        filtersChildren.isSelected = !it
                    }
                    notifyDataSetChanged()
                }

            }
            Filters.RANGER_TYPE -> {
                holder?.itemView?.ranger?.visibility = View.VISIBLE
                setupRangesBar(holder?.itemView!!, filtersChildren.name, filtersChildren.unit)
                holder.itemView.ranger?.setRangeSeekListener { minValue, maxValue ->
                    passUpdatedRange(filterHeader.key, minValue, maxValue)
                }
                holder.itemView.iv_tick?.visibility = View.GONE
                holder.itemView.text?.visibility = View.GONE
            }

            Filters.TIMES_RANGER_TYPE -> {
                holder?.itemView?.ranger?.visibility = View.VISIBLE
                holder?.itemView?.rating?.visibility = View.GONE
                holder?.itemView?.select_your_rating?.visibility = View.GONE
                holder?.itemView?.text?.visibility = View.GONE
                holder?.itemView?.ranger?.setRangeSeekListener { minValue, maxValue ->
                    passUpdatedRange(filterHeader.key, minValue, maxValue)
                }
                setupRangesBar(holder?.itemView!!, filtersChildren.name, filtersChildren.unit, true)
                holder.itemView.iv_tick.visibility = View.GONE
            }

            Filters.STAR_TYPE -> {
                holder?.itemView?.ranger?.visibility = View.GONE
                holder?.itemView?.rating?.visibility = View.VISIBLE
                holder?.itemView?.select_your_rating?.visibility = View.VISIBLE
                holder?.itemView?.rating?.setOnRatingChangeListener { baseRatingBar, rating ->
                    itemCLickListener.invoke(filterHeader.key, "",
                            arrayListOf(rating.roundToInt().toString()))
                }
                holder?.itemView?.rating?.rating = filtersChildren.rating
                holder?.itemView?.iv_tick?.visibility = View.GONE
                holder?.itemView?.text?.visibility = View.GONE
            }
        }
    }

    private fun passUpdatedRange(key: String, minValue: String, maxValue: String) {
        itemCLickListener.invoke(key, "", arrayListOf(minValue, maxValue))
    }

    private fun setupRangesBar(view: View, values: String, units: String, isTimeRange: Boolean = false) {
        val unitsArray = units.split(",")
        val ranges = values.split(",")

        if (isTimeRange) {
            view.select_your_rating?.visibility = View.GONE

            view.rating?.visibility = View.GONE
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_TIME)
            view.ranger.minAppendText = unitsArray[0]
            view.ranger.maxAppenedText = unitsArray[1]
            view.ranger.initializeMinMax(TimesDatesUtils.convertTimeToFloat(ranges[0], TimesDatesUtils.FORMAT_24_TYPE)
                    , TimesDatesUtils.convertTimeToFloat(ranges[1], TimesDatesUtils.FORMAT_24_TYPE))
        } else {
            view.select_your_rating?.visibility = View.GONE
            view.rating?.visibility = View.GONE
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_DEFAULT)
            view.ranger.minAppendText = unitsArray[0]
            view.ranger.maxAppenedText = unitsArray[1]
            view.ranger.initializeMinMaxForExperiance(ranges[0], ranges[1])
        }
    }

    override fun onBindGroupViewHolder(holder: FiltersGroupViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?) {
        holder?.itemView?.filterHeader?.text = (group as FiltersExpand).name
        val selectedGroup = groups[calculateRealGroupPosition(group,flatPosition)] as FiltersExpand
        if (selectedGroup.options.isEmpty()) {
            holder?.itemView?.setOnClickListener {
                itemCLickListener.invoke(selectedGroup.key, selectedGroup.dataType, arrayListOf())
            }
        }

    }


    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val date = year.toString() + "-" + monthOfYear + "-" + dayOfMonth
        itemCLickListener.invoke(view?.tag!!, "", arrayListOf(date))
    }


    inner class FiltersGroupViewHolder(itemView: View) : GroupViewHolder(itemView) {

    }

    private fun calculateRealGroupPosition(group: ExpandableGroup<*>, flatPosition: Int): Int {
        var realPos = flatPosition

        val groupsBefore = arrayListOf<ExpandableGroup<*>>()
        val groups = groups

        for (expandableGroup in groups) {
            if (expandableGroup == group) {
                break
            }
            groupsBefore.add(expandableGroup)
        }

        for (i in 0 until groupsBefore.size) {
            val groupItem = groupsBefore[i]
            if (isGroupExpanded(groupItem)) {
                realPos -= groupItem.itemCount
            }
        }

        return realPos
    }

    override fun onGroupExpanded(positionStart: Int, itemCount: Int) {
        if (itemCount > 0) {
            val groupIndex = expandableList.getUnflattenedPosition(positionStart).groupPos
            notifyItemRangeInserted(positionStart, itemCount)
            for (grp in groups) {
                if (grp !== groups.get(groupIndex)) {
                    if (this.isGroupExpanded(grp)) {
                        this.toggleGroup(grp)
                        this.notifyDataSetChanged()
                    }
                }
            }
        }
    }
}