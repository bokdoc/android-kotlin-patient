package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.hanks.htextview.base.DisplayUtils.getScreenWidth
import kotlinx.android.synthetic.main.item_next_availablitity.view.*

import android.view.ViewTreeObserver
import android.support.v7.widget.StaggeredGridLayoutManager
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.support.v7.widget.LinearLayoutManager


class NextAvailabilitiesAdapter(val nextAvailablitities: List<NextAvailabilitiesParcelable>,
                                var onBookClickListner: ((nextAvailability: NextAvailabilitiesParcelable) -> Unit),
                                var onBookClickListnerPosition: ((nextAvailability: NextAvailabilitiesParcelable, position: Int) -> Unit)? =
                                        null, var isConsulting: Boolean = false) : RecyclerView.Adapter<NextAvailabilitiesAdapter.NextAvailabilitiesViewHolder>() {

    var lastSelectedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NextAvailabilitiesAdapter.NextAvailabilitiesViewHolder {
        val itemView = parent.inflateView(R.layout.item_next_availablitity)

        /*
        itemView.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                val layoutParams = itemView.layoutParams
                val recyclerViewParams = layoutParams as RecyclerView.LayoutParams
                recyclerViewParams.width = itemView.width / 2
                itemView.layoutParams = recyclerViewParams
                itemView.viewTreeObserver.removeOnPreDrawListener(this)
                return true
            }
        })
        */
        //view.layoutParams = RecyclerView.LayoutParams(171,100)
        /*
        val vto = parent.viewTreeObserver
        var viewHolder:NextAvailabilitiesAdapter.NextAvailabilitiesViewHolder? = null
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.layoutParams = RecyclerView.LayoutParams(parent.measuredWidth/2, 100)
                parent.viewTreeObserver.removeOnGlobalLayoutListener(this)
                Log.i("width",""+parent.width/2)
            }
        })
        */

        val viewHolder: NextAvailabilitiesAdapter.NextAvailabilitiesViewHolder = NextAvailabilitiesViewHolder(itemView)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return nextAvailablitities.size
    }

    fun getFormToTimes(position: Int): Array<String?> {
        return arrayOf(nextAvailablitities[position].from,nextAvailablitities[position].to)
    }

    override fun onBindViewHolder(holder: NextAvailabilitiesAdapter.NextAvailabilitiesViewHolder, position: Int) {
        nextAvailablitities[position].let {

            if(it.isVisible) {
                holder.itemView.visibility = VISIBLE
                when (it.status) {
                    true -> {
                        holder.itemView.group_nv.visibility = View.VISIBLE
                        holder.itemView.tv_na_not_avi.visibility = View.GONE
                        if (it.isSelected) {
                            lastSelectedPosition = position
                            holder.itemView.tv_na_day.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                            holder.itemView.tv_na_from.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                            holder.itemView.tv_na_to.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                            holder.itemView.selected_tick.visibility = VISIBLE
                            holder.itemView.bg.setBackgroundResource(R.drawable.next_avalability_selected_border)
                            holder.itemView.tv_na_from.text = holder.itemView.context.getString(R.string.two_texts_placeholder,
                                    holder.itemView.context.getString(R.string.from), it.from)
                            holder.itemView.tv_na_to.text = holder.itemView.context.getString(R.string.two_texts_placeholder,
                                    holder.itemView.context.getString(R.string.to), it.to)
                            holder.itemView.btn_na_book.text = holder.itemView.context.getString(R.string.booked)
                            holder.itemView.btn_na_book.setBackgroundResource(R.drawable.booked_bg)
                            holder.itemView.btn_na_book.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorPrimary))
                        } else {
                            holder.itemView.tv_na_day.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorNextAvailability))
                            holder.itemView.tv_na_from.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
                            holder.itemView.tv_na_to.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
                            holder.itemView.selected_tick.visibility = GONE
                            holder.itemView.bg.setBackgroundResource(R.drawable.next_avalability_border)
                            holder.itemView.tv_na_from.text = ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                                    holder.itemView.context.getString(R.string.from), it.from),
                                    it.from!!, ContextCompat.getColor(holder.itemView.context, R.color.colorPrimary))
                            holder.itemView.tv_na_to.text = it.to?.let { it1 ->
                                ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                                        holder.itemView.context.getString(R.string.to), it.to),
                                        it1, ContextCompat.getColor(holder.itemView.context, R.color.colorPrimary))
                            }
                            holder.itemView.btn_na_book.text = holder.itemView.context.getString(R.string.bookNow)
                            holder.itemView.btn_na_book.setBackgroundResource(R.drawable.consult_now_bg)
                            holder.itemView.btn_na_book.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                        }
                    }
                    false -> {
                        holder.itemView.group_nv.visibility = View.GONE
                        holder.itemView.tv_na_not_avi.visibility = View.VISIBLE
                        holder.itemView.btn_na_book.isClickable = false
                        // holder.itemView.bg.setBackgroundResource(R.drawable.next_avalability_border)
                        holder.itemView.btn_na_book.setBackgroundResource(R.drawable.consult_now_bg)
                        holder.itemView.btn_na_book.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
                    }
                }
            }else{
                holder.itemView.visibility = GONE
            }


            if(it.to?.isEmpty()!!){
                holder.itemView.tv_na_to.text = ""
                holder.itemView.tv_na_from.visibility = GONE
                holder.itemView.tv_na_from.text = it.from
            }else{
                holder.itemView.tv_na_to.visibility = VISIBLE
                holder.itemView.tv_na_from.visibility = VISIBLE
            }

            /*
            if (isConsulting) {
                holder.itemView.btn_na_book.text = holder.itemView.context.getString(R.string.consult_now)
            }
            */
            holder.itemView.btn_na_book.text = it.book_button?.label
            if(!it.buttonText.isNullOrEmpty())
                holder.itemView.btn_na_book.text = it.buttonText

            holder.itemView.tv_na_day.text = it.dateNormalized
        }
    }


    fun updateSelectedFrom(fromTime:String,toTime:String,position: Int){
            nextAvailablitities[position].from = fromTime
            nextAvailablitities[position].to = toTime
            notifyItemChanged(position)
    }


    fun hideUnSelected(){
        nextAvailablitities.forEach {
            it.isVisible = lastSelectedPosition!=-1 && (
                    nextAvailablitities[lastSelectedPosition].idNextAvailabilities == it.idNextAvailabilities
                            || nextAvailablitities[lastSelectedPosition].dateId == it.dateId)
            it.isSelectedAfterClick = false
        }
        notifyDataSetChanged()
    }



    inner class NextAvailabilitiesViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        init {
            view.btn_na_book.setOnClickListener {
                if (nextAvailablitities[adapterPosition].isSelectedAfterClick &&
                        nextAvailablitities[adapterPosition].book_button?.endpointUrl.isNullOrEmpty()) {

                    if (lastSelectedPosition != -1) {
                        nextAvailablitities[lastSelectedPosition].isSelected = false
                        notifyItemChanged(lastSelectedPosition)
                    }

                    lastSelectedPosition = adapterPosition
                    nextAvailablitities[adapterPosition].isSelected = true
                    notifyItemChanged(lastSelectedPosition)
                    onBookClickListner.invoke(nextAvailablitities[adapterPosition])
                }
                onBookClickListner.invoke(nextAvailablitities[adapterPosition])
                onBookClickListnerPosition?.invoke(nextAvailablitities[adapterPosition],adapterPosition)
            }

            view.setOnClickListener {
                if (nextAvailablitities[adapterPosition].isSelectedAfterClick &&
                        nextAvailablitities[adapterPosition].book_button?.endpointUrl.isNullOrEmpty()) {
                    if (lastSelectedPosition != -1) {
                        nextAvailablitities[lastSelectedPosition].isSelected = false
                        notifyItemChanged(lastSelectedPosition)
                    }

                    lastSelectedPosition = adapterPosition
                    nextAvailablitities[adapterPosition].isSelected = true
                    notifyItemChanged(lastSelectedPosition)

                  //  onBookClickListnerPosition.invoke(nextAvailablitities[lastSelectedPosition],lastSelectedPosition)
                }
                onBookClickListner.invoke(nextAvailablitities[adapterPosition])
                onBookClickListnerPosition?.invoke(nextAvailablitities[adapterPosition],adapterPosition)
            }

        }

    }
}