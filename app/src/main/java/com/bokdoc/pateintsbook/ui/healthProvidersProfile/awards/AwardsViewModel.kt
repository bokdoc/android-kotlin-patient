package com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.healthProviders.awards.Awards
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.awards.AwardsRepository

class AwardsViewModel(private val awardsRepository: AwardsRepository):ViewModel() {
    val progressLiveData = MutableLiveData<Int>()
    val errorMessageLiveData = MutableLiveData<String>()
    val awardsList = MutableLiveData<List<Awards>>()


    fun getAwards(id:String){
        progressLiveData.value = VISIBLE
        awardsRepository.getAwards(id).observeForever {
            if(it?.data!=null){
                awardsList.value = it.data
            }else{
                errorMessageLiveData.value = it?.message
            }
            progressLiveData.value = GONE
        }
    }

}