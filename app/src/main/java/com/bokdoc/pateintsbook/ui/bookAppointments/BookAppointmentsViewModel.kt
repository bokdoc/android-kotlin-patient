package com.bokdoc.pateintsbook.ui.bookAppointments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.net.Uri
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.bookAppointement.BookInfoImpl
import com.bokdoc.pateintsbook.data.bookAppointement.HealthProvidersBookUIInfo
import com.bokdoc.pateintsbook.data.bookAppointement.HealthProvidersUIBookInfoImp
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.models.promo.PromoCode
import com.bokdoc.pateintsbook.data.repositories.bookAppointment.BookAppointmentRepository
import com.bokdoc.pateintsbook.data.repositories.common.StringsRepository
import com.bokdoc.pateintsbook.data.repositories.linksCardsDetails.LinksCardsDetailsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.utils.constants.StringsConstants

class BookAppointmentsViewModel(private val appContext: Context,
                                private val stringsRepository: StringsRepository,
                                private val bookAppointmentRepository: BookAppointmentRepository,
                                private val userRepository: UserSharedPreference,
                                private val cardsDetailsRepository: LinksCardsDetailsRepository) : ViewModel() {
    var healthProvidersProfile: HealthProvidersBookUIInfo = HealthProvidersUIBookInfoImp()
    var bookInfo: BookInfoImpl = BookInfoImpl()
    var bookingInfoLiveData = MutableLiveData<String>()
    var secondaryDurationBookingInfoLive = MutableLiveData<String>()
    var isSurgeries = false
    var bookingInfoUrl = ""
    var discountId = ""
    var patient = userRepository.getUser()
    var isSurgeriesLiveData = MutableLiveData<Boolean>()
    var selectedNextAvailability: NextAvailabilitiesParcelable? = null
    var selectedNextAvailabilityPosition = -1
    var selectedNextAvailabilityFrom = ""
    var selectedNextAvailabilityTo = ""
    var isAgeEditHasData = false
    var isNameEditHasData = false
    var isMobileEditHasData = false
    var isNextAvailabilitiesFound = true
    var promoCode = PromoCode()
    var isLoadSecondaryDuration: Boolean = false
    var secondaryDurationsQuery = ""
    var coupon: String = ""


    fun getBookingInfo() {
        cardsDetailsRepository.getDetails(bookingInfoUrl).observeForever {
            if (it?.data != null && it.data!!.size == 1) {
                it.data!![0].let {
                    healthProvidersProfile.name = it.mainProfileInfo.profileName
                    healthProvidersProfile.profileId = it.mainProfileInfo.profileId.toInt()
                    healthProvidersProfile.picture = it.mainProfileInfo.profilePicture.url
                    healthProvidersProfile.specialty = it.mainProfileInfo.profileMainSpeciality
                    healthProvidersProfile.location = it.mainProfileInfo.location
                    healthProvidersProfile.serviceSlug = it.slug
                    healthProvidersProfile.rotators = it.rotators
                    if (it.rowsInfo.isNotEmpty()) {
                        healthProvidersProfile.serviceInfo = it.rowsInfo[0]
                        healthProvidersProfile.serviceId = it.rowsInfo[0].id.toInt()
                        bookInfo.bookSecondaryDuration = it.rowsInfo[0].bookSecondaryDuration
                    }
                    healthProvidersProfile.profileTypeId = it.mainProfileInfo.profileType
                    Log.i("profile", it.mainProfileInfo.profileType.toString())
                    healthProvidersProfile.gender = it.mainProfileInfo.gender

                    bookInfo.serviceType = it.serviceType
                    bookInfo.bookType = it.bookType
                    Log.i("slug", it.slug)

                    isSurgeries = it.slug == LinksCardsDetails.SURGERIES_SLUG
                    if (it.rowsInfo.isNotEmpty()) {
                        it.rowsInfo[0].nextAvailabilities = initializeSelectedNextAvailability(it.rowsInfo[0].nextAvailabilities)
                    }
                    initializeBookInfoImpl()
                    isSurgeriesLiveData.value = isSurgeries

                    healthProvidersProfile.secondaryDurationConsulting = it.rowsInfo[0].secondaryDurationConsulting
                    healthProvidersProfile.secondaryDurations = it.rowsInfo[0].secondaryDurations

                    if (!it.rowsInfo.isNullOrEmpty()) {
                        healthProvidersProfile.secondaryDurationConsulting = it.rowsInfo[0].secondaryDurationConsulting
                        healthProvidersProfile.secondaryDurations = it.rowsInfo[0].secondaryDurations
                        if (!healthProvidersProfile.secondaryDurations.isNullOrEmpty())
                            secondaryDurationsQuery = healthProvidersProfile.secondaryDurations[0].key
                    }


                }
                bookingInfoLiveData.value = StringsConstants.EMPTY
            } else {
                bookingInfoLiveData.value = appContext.getString(R.string.server_error)
            }
        }
    }


    fun getSecondaryDurationBookingDetails(secondaryDurationSelectedPosition: Int) {
        secondaryDurationsQuery = healthProvidersProfile.secondaryDurations[secondaryDurationSelectedPosition].key
        val query = bookingInfoUrl + secondaryDurationsQuery
        if (coupon.isNotEmpty()) {
            cardsDetailsRepository.getDetails(query, coupon).observeForever {
                handleDetailsObserver(it)
            }
        } else {
            cardsDetailsRepository.getDetails(query).observeForever {
                handleDetailsObserver(it)
            }

        }

    }

    private fun handleDetailsObserver(dataResource: DataResource<LinksCardsDetails>?) {
        if (dataResource?.data != null && dataResource.data!!.size == 1) {
            selectedNextAvailability = null
            bookInfo.nextAvailabilityId = ""
            dataResource.data?.let { data ->
                if (data[0].rowsInfo.isNotEmpty()) {
                    healthProvidersProfile.serviceInfo.nextAvailabilities =
                            initializeSelectedNextAvailability(data[0].rowsInfo[0].nextAvailabilities)
                    healthProvidersProfile.serviceInfo.mainFees = data[0].rowsInfo[0].mainFees
                    healthProvidersProfile.serviceInfo.subMainFees = data[0].rowsInfo[0].subMainFees
                    bookInfo.bookSecondaryDuration = data[0].rowsInfo[0].bookSecondaryDuration
                }
            }
            secondaryDurationBookingInfoLive.value = StringsConstants.EMPTY
        } else {
            secondaryDurationBookingInfoLive.value = appContext.getString(R.string.server_error)

        }
    }

    private fun initializeBookInfoFromUrl() {

        val bookUrl = Uri.parse(bookingInfoUrl)
        bookUrl?.let {
            bookUrl.getQueryParameter("service_pivot_id").let { servicePivotId ->
                if (servicePivotId != null) {
                    try {
                        bookInfo.servicePivotId = servicePivotId.toInt()
                    } catch (e: Exception) {
                        bookingInfoLiveData.value = appContext.getString(R.string.server_error)
                    }
                } else {
                    bookingInfoLiveData.value = appContext.getString(R.string.server_error)
                }
            }

            bookUrl.getQueryParameter("service_pivot_type").let { servicePivotType ->
                if (servicePivotType != null) {
                    bookInfo.servicePivotType = servicePivotType
                } else {
                    bookingInfoLiveData.value = appContext.getString(R.string.server_error)
                }
            }

            it.getQueryParameter("reappointment")?.let {
                bookInfo.bookRequestType = it
            }
        }
    }

    /*
    fun isHospitalCategory():Boolean{
        return userRepository
    }
    */

    private fun initializeSelectedNextAvailability(nextAvailabilities: List<NextAvailabilitiesParcelable>): List<NextAvailabilitiesParcelable> {

        val size = nextAvailabilities.size
        isNextAvailabilitiesFound = size != 0
        (0 until size).forEach {
            /*
            if(selectedNextAvailability!=null){
                nextAvailabilities[it].isVisible = false
            }else{
                nextAvailabilities[it].isSelectedAfterClick = true
            }
            */
            nextAvailabilities[it].isSelectedAfterClick = true

            if (selectedNextAvailability != null && (
                            nextAvailabilities[it].dateId == selectedNextAvailability!!.dateId ||
                                    nextAvailabilities[it].idNextAvailabilities == selectedNextAvailability!!.idNextAvailabilities)) {
                nextAvailabilities[it].isSelected = true
                nextAvailabilities[it].isVisible = true
                nextAvailabilities[it].isSelectedAfterClick = false
                nextAvailabilities[it].buttonText = appContext.getString(R.string.you_selected)
                selectedNextAvailabilityPosition = it
                if (selectedNextAvailability!!.to?.isEmpty()!!) {
                    selectedNextAvailabilityFrom = nextAvailabilities[it].from!!
                    selectedNextAvailabilityTo = nextAvailabilities[it].to!!
                    nextAvailabilities[it].to = ""
                    nextAvailabilities[it].from = selectedNextAvailability!!.from
                }
                return arrayListOf(nextAvailabilities[it])
            }
        }
        return nextAvailabilities
    }


    fun userId(): String {
        return userRepository.getId()
    }

    private fun initializeBookInfoImpl() {
        initializeBookInfoFromUrl()
        //bookInfo.bookType = appContext.getString(R.string.appointementBookKey)
        bookInfo.profileId = healthProvidersProfile.profileId
        bookInfo.clinicId = healthProvidersProfile.clinicId
        if (healthProvidersProfile.departmentId != null)
            bookInfo.departmentId = healthProvidersProfile.departmentId!!
        if (healthProvidersProfile.serviceId != null)
            bookInfo.serviceId = healthProvidersProfile.serviceId!!
        if (healthProvidersProfile.paymentTypeId != null)
            bookInfo.paymentTypeId = healthProvidersProfile.paymentTypeId!!
        if (healthProvidersProfile.paymentTypeMethod != null)
            bookInfo.paymentTypeMethod = healthProvidersProfile.paymentTypeMethod!!
        if (healthProvidersProfile.doctorId != null)
            bookInfo.doctorId = healthProvidersProfile.doctorId!!
        bookInfo.profileTypeName = healthProvidersProfile.profileTypeName
        bookInfo.specialty = healthProvidersProfile.specialty
        bookInfo.name = healthProvidersProfile.name
        bookInfo.location = healthProvidersProfile.location
        //bookInfo.location = healthProvidersProfile.location
        bookInfo.picture = healthProvidersProfile.picture
        bookInfo.deviceType = appContext.getString(R.string.device_type)


        if (selectedNextAvailability?.idNextAvailabilities != null)
            bookInfo.nextAvailabilityId = selectedNextAvailability!!.idNextAvailabilities!!

        if (patient != null) {
            bookInfo.patientImage = patient!!.picture.url
            bookInfo.patientName = patient!!.name
            if (patient?.phone != null)
                bookInfo.patientMobile = patient!!.phone
            if (patient?.email != null)
                bookInfo.patientEmail = patient!!.email
            if (!patient?.gender.isNullOrEmpty()) {
                bookInfo.patientGender = patient!!.gender
            } else {
                bookInfo.patientGender = UserResponse.GENDER_MALE
            }
        }
    }

    fun validate(): List<String> {
        val error = ArrayList<String>()
        bookInfo.bookType.let { bookType ->
            if (bookType == stringsRepository.getStringRes(appContext, R.string.appointementBookKey) ||
                    bookType == stringsRepository.getStringRes(appContext, R.string.useFreeBookKey)) {
                bookInfo.patientName.let {
                    if (it.isEmpty()) {
                        isNameEditHasData = false
                        error.add(NAME_REQUIRED)
                    }
                }

                /*
                bookInfo.patientEmail.let {
                    if(it.isEmpty())
                        error.add(EMAIL_REQUIRED)
                }
                */

                bookInfo.patientAge.let {
                    if (it.isEmpty()) {
                        error.add(AGE_REQUIRED)
                        isAgeEditHasData = false
                    }
                }

                /*
                bookInfo.patientGender.let {
                    if (it == "")
                        error.add(GENDER_REQUIRED)
                }
                */

                bookInfo.patientMobile.let {
                    if (it.isEmpty()) {
                        error.add(MOBILE_REQUIRED)
                        isMobileEditHasData = false
                    }
                }

                if (isNextAvailabilitiesFound) {
                    bookInfo.nextAvailabilityId.let {
                        if (it.isEmpty())
                            error.add(RESERVATION_DATE_REQUIRED)
                    }
                }

                /*
                if(isSurgeries&& bookInfo.surgeriesCaseDescription!!.isEmpty())
                    if(it.isEmpty())
                        error.add(CASE_DESCRIPTION_REQUIRED)
                        */
            }
        }
        return error
    }

    fun confirm(): LiveData<DataResource<String>> {
        //loadSurgeriesAttachments()
        /*
        bookInfo.reservationTime = "11:00"
        bookInfo.reservationDate = "2018-05-01"
        */
        if (bookInfo.patientGender == appContext.getString(R.string.male)) {
            bookInfo.patientGender = UserResponse.GENDER_MALE
        } else if (bookInfo.patientGender == appContext.getString(R.string.female)) {
            bookInfo.patientGender = UserResponse.GENDER_FEMALE
        }
        return bookAppointmentRepository.confirm(bookInfo, coupon)
    }

    fun addPromoCode(): LiveData<DataResource<ArrayList<String>>> {
        return bookAppointmentRepository.addPromoCode(promoCode)
    }


    /*
    private fun loadSurgeriesAttachments(){
        val attachmentsUrl = ArrayList<String>()
        attachments.isNotEmpty().let {
            if(it){
                attachments.forEach {
                    attachmentsUrl.add(it.url)
                }
            }
            bookInfo.surgeriesMedia = attachmentsUrl
        }

    }
    */


    companion object {
        const val NAME_REQUIRED = "name"
        const val MOBILE_REQUIRED = "mobile"
        const val EMAIL_REQUIRED = "email"
        const val GENDER_REQUIRED = "gender"
        const val AGE_REQUIRED = "age"
        const val RESERVATION_DATE_REQUIRED = "reservation"
        const val CASE_DESCRIPTION_REQUIRED = "caseDescription"
    }

}