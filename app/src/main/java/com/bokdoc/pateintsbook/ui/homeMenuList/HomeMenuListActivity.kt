package com.bokdoc.pateintsbook.ui.homeMenuList

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.*
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompleteResultsActivity
import com.bokdoc.pateintsbook.ui.changeCurrencyLanguages.ChangeCurrencyLanguageActivity
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeSearch.AppointementFragment
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.AutoCompletesConstants
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.listeners.HomeSearchEditsListeners
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import kotlinx.android.synthetic.main.activity_home_menu_list.*
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.home_search.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

class HomeMenuListActivity : ParentActivity() {

    private lateinit var homeMenuListViewModel: HomeMenuListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_home_menu_list)
        title = getString(R.string.home)



        homeMenuListViewModel = ViewModelsCustomProvider(HomeMenuListViewModel(baseContext,
                SharedPreferencesManagerImpl(applicationContext),
                SplashRepository(activity!!.applicationContext,
                        UserSharedPreference(activity!!.applicationContext),
                        AddFcmWebService(WebserviceManagerImpl.instance())), TokenRepository(applicationContext))).create(HomeMenuListViewModel::class.java)


        homeMenuListViewModel.defaultSelected = intent?.getIntExtra(getString(R.string.defaultTab), 0)!!

        updateServicesContent()

        rightTextView.visibility = VISIBLE
        rightTextView.text = getString(R.string.change_currency)

        rightTextView.setOnClickListener {
            Navigator.navigate(this, ChangeCurrencyLanguageActivity::class.java)
        }

        setupViewsClickListener()
        setupSearchEdits()
    }

    private fun setupViewsClickListener() {
        card_view_appointments.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.APPOINTMENTS_POSITION
            updateServicesContent()
        }
        card_view_surgeries.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.SURGERIES_POSITION
            updateServicesContent()
        }
        card_view_online_consulting.setOnClickListener {
            homeMenuListViewModel.defaultSelected = SearchScreenTypes.CONSULTING_POSITION
            updateServicesContent()
        }
    }

    private fun setupSearchEdits() {
        updateTexts()

        specialityEdit.setOnTouchListener(object : HomeSearchEditsListeners(this, arrayOf(getString(R.string.screenType)
                , getString(R.string.completeType)), SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), AutoCompletesConstants.SPECIALITY,
                AppointementFragment.AUTO_COMPLETE_REQUEST) {
            override fun passCompletedType(completeType: Int) {
                homeMenuListViewModel.completeEditType = completeType
            }

        })

        clearSpeciality.setOnClickListener {
            specialityEdit.setText("")
            it.visibility = GONE
            homeMenuListViewModel.speciality = ""
            homeMenuListViewModel.specialityParams = ""
            homeMenuListViewModel.saveDataState()

        }

        clearCity.setOnClickListener {
            cityEdit.setText("")
            homeMenuListViewModel.regionCountry = ""
            homeMenuListViewModel.regionCountryParams = ""
            homeMenuListViewModel.saveDataState()
            it.visibility = GONE
        }

        clearInsurance.setOnClickListener {
            insuranceEdit.setText("")
            homeMenuListViewModel.insuranceCarrier = ""
            homeMenuListViewModel.insuranceCarrierParams = ""
            homeMenuListViewModel.saveDataState()
            it.visibility = GONE
        }



        cityEdit.setOnTouchListener(object : HomeSearchEditsListeners(this, arrayOf(getString(R.string.screenType)
                , getString(R.string.completeType)),SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), AutoCompletesConstants.LOCATIONS,
                AppointementFragment.AUTO_COMPLETE_REQUEST) {
            override fun passCompletedType(completeType: Int) {
                homeMenuListViewModel.completeEditType = completeType
            }

        })



        insuranceEdit.setOnTouchListener(object : HomeSearchEditsListeners(this, arrayOf(getString(R.string.screenType)
                , getString(R.string.completeType)), SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected), AutoCompletesConstants.INSURANCE
                , AppointementFragment.AUTO_COMPLETE_REQUEST) {
            override fun passCompletedType(completeType: Int) {
                homeMenuListViewModel.completeEditType = completeType
            }

        })

        search.setOnClickListener {
            homeMenuListViewModel.screenType = SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)
            homeMenuListViewModel.saveDataState()
            Navigator.navigate(activity!!, arrayOf(getString(R.string.screenType), getString(R.string.query)),
                    arrayOf(SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected),
                            homeMenuListViewModel.getQuery()), SearchResultsActivity::class.java, HomeSearchActivity.SEARCH_REQUEST)
        }
    }

    fun updateTexts() {
        if (homeMenuListViewModel.screenType == SearchScreenTypes.getString(homeMenuListViewModel.defaultSelected)) {
            homeMenuListViewModel.speciality.let {
                if (it.isNotEmpty()) {
                    specialityEdit.setText(it)
                    clearSpeciality.visibility = VISIBLE
                }
            }

            homeMenuListViewModel.regionCountry.let {
                if (it.isNotEmpty()) {
                    cityEdit.setText(it)
                    clearCity.visibility = VISIBLE
                }
            }

            homeMenuListViewModel.insuranceCarrier.let {
                if (it.isNotEmpty()) {
                    insuranceEdit.setText(it)
                    clearInsurance.visibility = VISIBLE
                }
            }
        } else {
            specialityEdit.setText(StringsConstants.EMPTY)
            clearSpeciality.visibility = GONE
            cityEdit.setText(StringsConstants.EMPTY)
            clearCity.visibility = GONE
            insuranceEdit.setText(StringsConstants.EMPTY)
            clearInsurance.visibility = GONE
        }
    }

    private fun updateServicesContent() {
        when {
            homeMenuListViewModel.defaultSelected == HomeMenuListViewModel.APPOINTMENTS_POSITION -> {
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.appointments)
                insuranceEditParent.visibility = VISIBLE
                regionEditParent.visibility =  VISIBLE

            }
            homeMenuListViewModel.defaultSelected == HomeMenuListViewModel.SURGERIES_POSITION -> {
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.surgeries)
                insuranceEditParent.visibility = VISIBLE
                regionEditParent.visibility =  VISIBLE

            }
            else -> {
                online_consulting_view.setBackgroundResource(R.drawable.home_services_cards_selected_bg)
                surgeries_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                appointments_view.setBackgroundResource(R.drawable.home_services_cards_bg)
                searchText.text = getString(R.string.profile_includes)
                insuranceEditParent.visibility = View.GONE
                regionEditParent.visibility =  View.GONE
            }
        }
        updateTexts()
    }




    override fun onConfigurationChanged(newConfig: Configuration) {

    }


    private fun onAdapterClickListener(slug: String, position: Int) {
        /*
        when(slug){
            HomeMenuListViewModel.APPOINTMENTS_SLUG->{

            }
            HomeMenuListViewModel.SURGERIES_SLUG->{
                HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
            }
            HomeMenuListViewModel.ONLINE_CONSULTING_SLUG->{
                HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
            }
        }
        */
        HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppointementFragment.AUTO_COMPLETE_REQUEST && resultCode == Activity.RESULT_OK) {
            when (homeMenuListViewModel.completeEditType) {
                AutoCompletesConstants.SPECIALITY -> {
                    specialityEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    clearSpeciality.visibility  = VISIBLE
                    homeMenuListViewModel.speciality = data.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.specialityParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                }
                AutoCompletesConstants.LOCATIONS -> {
                    cityEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    clearCity.visibility  = VISIBLE
                    homeMenuListViewModel.regionCountry = data.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.regionCountryParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                }
                else -> {
                    insuranceEdit.setText(data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY))
                    homeMenuListViewModel.insuranceCarrier = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_TEXT_KEY)
                    homeMenuListViewModel.insuranceCarrierParams = data!!.getStringExtra(AutoCompleteResultsActivity.SELECTED_PARAMS_KEY)
                    clearInsurance.visibility  = VISIBLE
                }
            }
            homeMenuListViewModel.saveDataState()
        }
    }


}
