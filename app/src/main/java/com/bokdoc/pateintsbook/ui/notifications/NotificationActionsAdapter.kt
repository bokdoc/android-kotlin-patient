package com.bokdoc.pateintsbook.ui.notifications

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.ui.notifications.NotificationsFragment.Companion.CANCEL
import com.bokdoc.pateintsbook.ui.notifications.NotificationsFragment.Companion.PAYMENT
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_notification_action.view.*

class NotificationActionsAdapter(private val dynamicLinks: List<DynamicLinks>, private val onClickListener: (DynamicLinks) -> Unit) :
        RecyclerView.Adapter<NotificationActionsAdapter.CardLinksViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CardLinksViewHolder {
        return CardLinksViewHolder(parent.inflateView(R.layout.item_notification_action))
    }

    override fun getItemCount(): Int {
        return dynamicLinks.size
    }

    override fun onBindViewHolder(viewHolder: CardLinksViewHolder, position: Int) {
        dynamicLinks[position].let {
            when (it.targetScreen) {
                CANCEL -> {
                    viewHolder.itemView.btn_notification.background = ContextCompat.getDrawable(viewHolder.itemView.context, R.drawable.btn_cancel_bg)
                }
                else -> {
                    viewHolder.itemView.btn_notification.background = ContextCompat.getDrawable(viewHolder.itemView.context, R.drawable.btn_green_bg)
                }
            }
            viewHolder.itemView.btn_notification.visibility = View.VISIBLE
            viewHolder.itemView.btn_notification.text = it.label
        }
    }

    inner class CardLinksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.btn_notification.setOnClickListener {
                onClickListener.invoke(dynamicLinks[adapterPosition])
            }
        }
    }
}