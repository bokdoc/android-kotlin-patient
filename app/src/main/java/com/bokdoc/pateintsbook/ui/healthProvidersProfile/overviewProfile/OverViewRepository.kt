package com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.AddressConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.OverviewConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView.OverViewWebService
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.OverviewRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.ProfileAddressResource
import com.bokdoc.pateintsbook.data.webservice.models.Publication


class OverViewRepository(val context: Context, val overViewWebService: OverViewWebService, val userRepository: UserSharedPreference) {

    val preferenceManger = SharedPreferencesManagerImpl(context)


    fun getOverView(id: String, services: String): LiveData<DataResource<OverviewRow>> {

        val liveData = MutableLiveData<DataResource<OverviewRow>>()
        val dataResource = DataResource<OverviewRow>()

        overViewWebService.getProfileOverView(id, preferenceManger.getData(context.getString(R.string.token), ""), services)
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (!it.data.isNullOrEmpty()) {
                        dataResource.data = arrayListOf(OverviewConverter.convert(it.data!![0]))
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java
                        , PaymentOptions::class.java
                        , FeedbackResource::class.java
                        , LanguageSpoken::class.java
                        , Publication::class.java
                        , FeedbackPersonaResource::class.java
                        , ProfileType::class.java
                        , InsuranceCarrierResource::class.java
                        , Certification::class.java
                        , Accommodation::class.java
                        , Membership::class.java
                        , Educations::class.java
                        , Accreditation::class.java)))

        return liveData
    }

    fun getAddress(id: String): LiveData<DataResource<Location>> {

        val liveData = MutableLiveData<DataResource<Location>>()
        val dataResource = DataResource<Location>()

        overViewWebService.getProfileAddess(id, preferenceManger.getData(context.getString(R.string.token), ""))
                .enqueue(CustomResponse<ProfileAddressResource>({
                    dataResource.status = it.status
                    if (!it.data.isNullOrEmpty()) {
                        dataResource.data = AddressConverter.convert(it.data!!)
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(ProfileAddressResource::class.java)))

        return liveData
    }

}