package com.bokdoc.pateintsbook.ui.filters

import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.intrusoft.sectionedrecyclerview.Section

class FiltersSectionHeader(val sectionText: String, private val childList: List<FilterChild>,
                           val dataType: String, val key: String, var isSelected:Boolean=false) : Section<FilterChild> {
    override fun getChildItems(): List<FilterChild> {
        return childList
    }

}