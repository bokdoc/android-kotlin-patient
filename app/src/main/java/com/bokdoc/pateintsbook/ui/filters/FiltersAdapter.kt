package com.bokdoc.pateintsbook.ui.filters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.utils.CustomCrystalRangeSeekbar
import com.bokdoc.pateintsbook.utils.TimesDatesUtils
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.filter_child.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class FiltersAdapter(context: Context, var sectionItemList: List<FiltersSectionHeader>,var itemCLickListener:(key:String,type:String,values:ArrayList<String>)-> Unit):
        SectionRecyclerViewAdapter<FiltersSectionHeader, FilterChild,
                FiltersAdapter.SectionViewHolder, FiltersAdapter.ChildViewHolder>(context,sectionItemList), DatePickerDialog.OnDateSetListener {

    override fun onCreateSectionViewHolder(viewGroup: ViewGroup?, p1: Int): FiltersAdapter.SectionViewHolder {
        val view = LayoutInflater.from(viewGroup?.context).inflate(R.layout.filter_header,viewGroup,false)
        return SectionViewHolder(view)
    }

    override fun onBindSectionViewHolder(sectionViewHolder: FiltersAdapter.SectionViewHolder?, position: Int, p2: FiltersSectionHeader?) {
        sectionViewHolder?.titleTextView?.text = sectionItemList[position].sectionText
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?, p1: Int): FiltersAdapter.ChildViewHolder {
        val view = LayoutInflater.from(viewGroup?.context).inflate(R.layout.filter_child,viewGroup,false)
        return ChildViewHolder(view)
    }

    override fun onBindChildViewHolder(childViewHolder: FiltersAdapter.ChildViewHolder?, sectionPosition: Int,
                                       childPosition: Int, filterChild: FilterChild?) {
            //childViewHolder?.view?.visibility = VISIBLE
            when(sectionItemList[sectionPosition].dataType){
                Filters.CHECKBOX_TYPE -> {
                    childViewHolder?.view?.checkBox?.visibility = VISIBLE
                    childViewHolder?.view?.checkBox?.text = filterChild?.name
                    childViewHolder?.view?.ranger?.visibility = GONE
                    childViewHolder?.view?.rating?.visibility = GONE
                    childViewHolder?.view?.checkBox?.setOnCheckedChangeListener {_, isSelected ->
                        sectionItemList[sectionPosition].key.let {
                            if(it == Filters.FREE_APPOINTMENT || it == Filters.IS_HOME_VISIT){
                                itemCLickListener.invoke(it,sectionItemList[sectionPosition].dataType,arrayListOf(isSelected.toString()))
                            }else{
                                itemCLickListener.invoke(sectionItemList[sectionPosition].key,sectionItemList[sectionPosition].dataType,
                                        arrayListOf(sectionItemList[sectionPosition].childItems[childPosition].params))
                            }
                        }
                    }
                }
                Filters.RANGER_TYPE -> {
                    childViewHolder?.view?.checkBox?.visibility = GONE
                    childViewHolder?.view?.ranger?.visibility = VISIBLE
                    setupRangesBar(childViewHolder?.view!!,filterChild?.name!!,filterChild.unit)
                    childViewHolder.view.ranger?.setRangeSeekListener { minValue, maxValue->
                        passUpdatedRange(sectionItemList[sectionPosition].key,minValue,maxValue)
                    }
                    childViewHolder.view.rating?.visibility = GONE
                }

                Filters.TIMES_RANGER_TYPE ->{
                    childViewHolder?.view?.checkBox?.visibility = GONE
                    childViewHolder?.view?.ranger?.visibility = VISIBLE
                    childViewHolder?.view?.ranger?.setRangeSeekListener { minValue, maxValue->
                        passUpdatedRange(sectionItemList[sectionPosition].key,minValue,maxValue)
                    }
                    setupRangesBar(childViewHolder?.view!!,filterChild?.name!!,filterChild.unit,true)
                    childViewHolder.view.rating?.visibility = GONE
                }

                Filters.STAR_TYPE->{
                    childViewHolder?.view?.checkBox?.visibility = GONE
                    childViewHolder?.view?.ranger?.visibility = GONE
                    childViewHolder?.view?.rating?.visibility = VISIBLE
                }

                Filters.MAP_TYPE->{

                }

                Filters.CALENDAR_TYPE->{
                    val now = Calendar.getInstance()
                    val dpd = DatePickerDialog.newInstance(this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    )
                    dpd.show((childViewHolder?.view?.context as Activity).fragmentManager,sectionItemList[sectionPosition].key)
                }

            }
    }

    private fun passUpdatedRange(key:String,minValue:String,maxValue:String){
        itemCLickListener.invoke(key,"", arrayListOf(minValue,maxValue))
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val date = year.toString() + "-" + monthOfYear + "-" + dayOfMonth
        itemCLickListener.invoke(view?.tag!!,"", arrayListOf(date))
    }

    private fun setupRangesBar(view: View,values:String,units:String,isTimeRange:Boolean = false){
        val unitsArray =  units.split(",")
        val ranges = values.split(",")

        if(isTimeRange){
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_TIME)
            view.ranger.initializeMinMax(TimesDatesUtils.convertTimeToFloat(ranges[0],TimesDatesUtils.FORMAT_24_TYPE)
                    ,TimesDatesUtils.convertTimeToFloat(ranges[1],TimesDatesUtils.FORMAT_24_TYPE))
        }else{
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_DEFAULT)
            view.ranger.minAppendText = unitsArray[0]
            view.ranger.maxAppenedText = unitsArray[1]
            view.ranger.initializeMinMax(ranges[0].toFloat(),ranges[1].toFloat())
        }
    }


    inner class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val titleTextView = view.findViewById<TextView>(R.id.filterHeader)
        val collapseArrow = view.findViewById<ImageView>(R.id.collapseArrow)
        val line = view.findViewById<View>(R.id.line)
        /*
        init {
            collapseArrow.tag = true
            view.setOnClickListener {
                val isCollapse = collapseArrow.tag as Boolean
                when {
                    isCollapse -> {
                        collapseArrow.setImageResource(R.drawable.ic_arrow_down)
                        line.visibility = VISIBLE
                        collapseArrow.tag = false
                        //sectionItemList[adapterPosition].isChildrenVisible = true
                    }
                    else -> {
                        collapseArrow.setImageResource(R.drawable.ic_arrow_right)
                        line.visibility = VISIBLE
                        collapseArrow.tag = true
                        //sectionItemList[adapterPosition].isChildrenVisible = false
                    }
                }
            }
        }
        */
    }

    class ChildViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}