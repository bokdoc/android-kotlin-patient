package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accreditation

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.item_patient_profile.*

class AccreditationActivity : ParentActivity() {

    lateinit var accreditationViewModel: AccreditationViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()
        title = getString(R.string.accreedation)

        getViewModel()

        getDataFromIntent()
        getAccreditation()
    }

    private fun getAccreditation() {
        progress.visibility = View.VISIBLE
        accreditationViewModel.getAccreditation().observe(this, Observer { it ->
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    rv_list.adapter = AccreditationAdapter(it.data!!)
                    progress.visibility = View.GONE

                    Utils.showShortToast(this, getString(R.string.done))
                }
                DataResource.INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE

                }
            }
        })

    }

    private fun getDataFromIntent() {
        accreditationViewModel.id = intent?.getStringExtra(getString(R.string.idKey))!!
        accreditationViewModel.serviceType = intent?.getStringExtra(getString(R.string.include_key))!!
    }

    private fun getViewModel() {

        accreditationViewModel = InjectionUtil.getAccreditationViewModel(this)

    }
}