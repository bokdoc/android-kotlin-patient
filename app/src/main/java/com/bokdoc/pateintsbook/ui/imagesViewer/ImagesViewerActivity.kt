package com.bokdoc.pateintsbook.ui.imagesViewer

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_images_viewer.*
import kotlinx.android.synthetic.main.item_image_viewer.view.*
import kotlinx.android.synthetic.main.toolbar.*

class ImagesViewerActivity : ParentActivity() {
    private lateinit var attachmentMediaViewModel: ImagesViewerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images_viewer)
        title = getString(R.string.media)
        enableBackButton()
        attachmentMediaViewModel = ViewModelsCustomProvider(ImagesViewerViewModel()).create(ImagesViewerViewModel::class.java)
        if (intent?.hasExtra(getString(R.string.selected_position))!!) {
            attachmentMediaViewModel.selectedPosition = intent?.getStringExtra(getString(R.string.selected_position))!!.toInt()
        }

        attachmentMediaViewModel.images = intent?.getParcelableArrayListExtra(getString(R.string.images))!!

        rightTextView.visibility = View.VISIBLE
        rightTextView.text = attachmentMediaViewModel.getTotalNumberWithSelected(application)
        setupViewPager()

    }

    private fun setupViewPager() {
        imagesPager.adapter = ImagesViewPagerAdapter(attachmentMediaViewModel.images)
        imagesPager.currentItem = attachmentMediaViewModel.selectedPosition
        imagesPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {
                attachmentMediaViewModel.selectedPosition = position
                rightTextView.text = attachmentMediaViewModel.getTotalNumberWithSelected(application)
            }

        })
    }


    class ImagesViewPagerAdapter(val imagesUris: ArrayList<Uri>) : PagerAdapter() {
        override fun isViewFromObject(view: View, p1: Any): Boolean {
            return view == p1
        }

        override fun getCount(): Int {
            return imagesUris.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): View {
            val view = LayoutInflater.from(container.context).inflate(R.layout.item_image_viewer, container, false)
            Glide.with(container.context).load(imagesUris[position]).into(view.iv_image)
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }


    }
}