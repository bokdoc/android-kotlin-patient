package com.bokdoc.pateintsbook.ui.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.bokdoc.pateintsbook.R
import android.R.attr.data
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.toolbar.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        titleText.text = getString(R.string.login)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val fragment = supportFragmentManager.findFragmentById(R.id.loginFragment)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }
}
