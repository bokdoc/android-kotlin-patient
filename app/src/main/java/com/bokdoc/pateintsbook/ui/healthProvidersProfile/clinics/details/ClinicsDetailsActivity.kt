package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.details

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.ClinicsSections
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.IClinicsParacable
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.IClinicsSections
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.navigators.Navigator
 import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery.ProfileGalleryActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_clinics_details.*
import kotlinx.android.synthetic.main.toolbar.*

class ClinicsDetailsActivity : AppCompatActivity() {

    private lateinit var clinicsDetailsViewModel: ClinicsDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clinics_details)
        titleText.text = getString(R.string.clinics)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
        clinicsDetailsViewModel = InjectionUtil.getClinicsDetailsViewModel(this)
        clinicsDetailsViewModel.clinic = intent?.getParcelableArrayListExtra<IClinicsParacable>(getString(R.string.clinicKey))!![0]
        detailsList.layoutManager = LinearLayoutManager(this)
        detailsList.adapter = ClinicsDetailsAdapter(this, clinicsDetailsViewModel.getSections() as List<ClinicsSections>){
            if(it == IClinicsSections.PHOTOS_VIDEOS_SECTION){
                Navigator.navigate(this,getString(R.string.profileMediaKey),
                        clinicsDetailsViewModel.clinic.profileMediaParacable as ArrayList<out MediaParcelable>,
                        ProfileGalleryActivity::class.java)
            }else
                if(it == IClinicsSections.NEXT_AVAILABILITY_SECTION){

                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       if(requestCode == NEXT_AVAILABILITES_REQUEST && resultCode == Activity.RESULT_OK ){
           val data = data?.getStringExtra(getString(R.string.selectedDateKey))
           (detailsList.adapter as ClinicsDetailsAdapter).let {
               it.updateNextAvaliabilityText(data!!)
           }
       }
    }



    companion object {
        const val NEXT_AVAILABILITES_REQUEST = 1
    }
}
