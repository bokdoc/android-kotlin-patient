package com.bokdoc.pateintsbook.ui.forgetPassword

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.login.LoginActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_new_password.*

class NewPasswordActivity : ParentActivity() {
    lateinit var forgetPasswordViewModel: ForgetPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_password)
        enableBackButton()
        title = getString(R.string.new_password)
        getViewModel()
        getDataFromIntent()
        btn_save.setOnClickListener(this)
        et_new_password.hint = ColoredTextSpannable.getSpannable(getString(R.string.passwordWithStar), getString(R.string.star), ContextCompat.getColor(this, R.color.colorErrorRed))
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_save.id -> {
                if (valid()) {
                    forgetPasswordViewModel.forgetPassword.newPassword = et_new_password.text.toString()
                    changePassword()
                }
            }
        }
    }

    private fun changePassword() {
        switchSendingProgressVisibility(View.VISIBLE)
        forgetPasswordViewModel.forgetNew().observe(this, Observer {
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    switchSendingProgressVisibility(View.GONE)
                    Navigator.navigate(this, LoginActivity::class.java)
                    finishAffinity()
                }

                DataResource.FAIL -> {
                    switchSendingProgressVisibility(View.GONE)
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wromg))
                }
            }
        })
    }

    fun valid(): Boolean {
        var isValid = true
        if (Utils.isEmpty(et_new_password)) {
            et_new_password.error = "   "
            isValid = false
        }


        return isValid
    }

    private fun getDataFromIntent() {
        forgetPasswordViewModel.forgetPassword = intent.getParcelableExtra(getString(R.string.forgetPassword))
    }

    private fun getViewModel() {
        forgetPasswordViewModel = InjectionUtil.getForgetPasswordViewModel(this)
    }

}