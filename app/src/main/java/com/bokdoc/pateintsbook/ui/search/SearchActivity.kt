package com.bokdoc.pateintsbook.ui.search

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import kotlinx.android.synthetic.main.home_search.*
import kotlinx.android.synthetic.main.toolbar.*

class SearchActivity : ParentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        titleText.text = getString(R.string.searchResults)
        returnArrow.visibility = VISIBLE

        returnArrow.setOnClickListener {
            finish()
        }
    }




}
