package com.bokdoc.pateintsbook.ui.filters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.utils.CustomCrystalRangeSeekbar
import com.bokdoc.pateintsbook.utils.TimesDatesUtils
import kotlinx.android.synthetic.main.filter_child.view.*
import kotlin.math.roundToInt

class FiltersChildrenAdapter(val optionsList:List<FilterChild>,val key:String
                             ,val dataType:String,var itemCLickListener:(key:String,datType:String,values:ArrayList<String>)-> Unit):
RecyclerView.Adapter<FiltersChildrenAdapter.ChildrenViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FiltersChildrenAdapter.ChildrenViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.filter_child,parent,false)
        return ChildrenViewHolder(view)
    }

    override fun getItemCount(): Int {
        return optionsList.size
    }

    override fun onBindViewHolder(holder: FiltersChildrenAdapter.ChildrenViewHolder, position: Int) {
        //childViewHolder?.view?.visibility = VISIBLE
        val filtersChildren = optionsList[position]
        when (dataType) {
            Filters.CHECKBOX_TYPE -> {
                holder.itemView.text?.visibility = View.VISIBLE
                holder.itemView.text?.text = filtersChildren.name
                if (filtersChildren.isSelected) {
                    holder.itemView.iv_tick?.visibility = View.VISIBLE
                } else {
                    holder.itemView.iv_tick.visibility = View.GONE
                }
                holder.itemView.ranger.visibility = View.GONE
                holder.itemView.rating.visibility = View.GONE
                holder.itemView.select_your_rating.visibility = View.GONE


                holder.itemView.setOnClickListener {
                    if (key == Filters.FREE_APPOINTMENT ||key == Filters.IS_HOME_VISIT) {
                        itemCLickListener.invoke(key,dataType,
                                arrayListOf(filtersChildren.isSelected.toString()))
                    } else {
                        itemCLickListener.invoke(key,dataType,
                                arrayListOf(filtersChildren.params))
                    }

                    filtersChildren.isSelected.let {
                        filtersChildren.isSelected = !it
                    }
                    notifyDataSetChanged()
                }
                if(position == itemCount-1){
                   holder.itemView.line.visibility = GONE
                }else{
                    holder.itemView.line.visibility = VISIBLE
                }

            }
            Filters.RANGER_TYPE -> {
                holder.itemView.ranger.visibility = View.VISIBLE
                setupRangesBar(holder.itemView, filtersChildren.name, filtersChildren.unit)
                holder.itemView.ranger?.setRangeSeekListener { minValue, maxValue ->
                    passUpdatedRange(key, minValue, maxValue)
                }
                holder.itemView.iv_tick?.visibility = View.GONE
                holder.itemView.text?.visibility = View.GONE
                holder.itemView.line.visibility = GONE
            }

            Filters.TIMES_RANGER_TYPE -> {
                holder.itemView.ranger?.visibility = View.VISIBLE
                holder.itemView.rating?.visibility = View.GONE
                holder.itemView.select_your_rating?.visibility = View.GONE
                holder.itemView.text?.visibility = View.GONE
                setupRangesBar(holder.itemView, filtersChildren.name, filtersChildren.unit, true)
                holder.itemView.ranger.setRangeSeekListener { minValue, maxValue ->
                    passUpdatedRange(key, minValue, maxValue)
                }
                holder.itemView.iv_tick.visibility = View.GONE
                holder.itemView.line.visibility = GONE
            }

            Filters.STAR_TYPE -> {
                holder.itemView.ranger?.visibility = View.GONE
                holder.itemView.rating.visibility = View.VISIBLE
                holder.itemView.select_your_rating?.visibility = View.VISIBLE
                holder.itemView.rating.setOnRatingChangeListener { baseRatingBar, rating ->
                    itemCLickListener.invoke(key, "",
                            arrayListOf(rating.roundToInt().toString()))
                    filtersChildren.rating = rating
                }
                holder.itemView.rating?.rating = filtersChildren.rating
                holder.itemView.iv_tick?.visibility = View.GONE
                holder.itemView.text?.visibility = View.GONE
                holder.itemView.line.visibility = GONE
            }
        }
    }


    private fun passUpdatedRange(key:String,minValue:String,maxValue:String){
        itemCLickListener.invoke(key,"", arrayListOf(minValue,maxValue))
    }

    private fun setupRangesBar(view: View, values: String, units: String, isTimeRange: Boolean = false) {
        val unitsArray = units.split(",")
        val ranges = values.split(",")

        if (isTimeRange) {
            view.select_your_rating?.visibility = View.GONE

            view.rating?.visibility = View.GONE
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_TIME)
            view.ranger.minAppendText = unitsArray[0]
            view.ranger.maxAppenedText = unitsArray[1]
            view.ranger.initializeMinMax(TimesDatesUtils.convertTimeToFloat(ranges[0], TimesDatesUtils.FORMAT_24_TYPE)
                    , TimesDatesUtils.convertTimeToFloat(ranges[1], TimesDatesUtils.FORMAT_24_TYPE))
        } else {
            view.select_your_rating?.visibility = View.GONE
            view.rating?.visibility = View.GONE
            view.ranger.setRangeType(CustomCrystalRangeSeekbar.RANGER_DEFAULT)
            view.ranger.minAppendText = unitsArray[0]
            view.ranger.maxAppenedText = unitsArray[1]
            view.ranger.initializeMinMaxForExperiance(ranges[0], ranges[1])
        }
    }

    inner class ChildrenViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    fun reset() {
        optionsList.forEach {
            when (dataType) {
                Filters.CHECKBOX_TYPE -> {
                    it.isSelected = false
                    /*
                    holder.view.checkBox.setOnCheckedChangeListener { _, isSelected ->
                        if (key == Filters.FREE_APPOINTMENT || key == Filters.IS_HOME_VISIT) {
                            itemCLickListener.invoke(key, dataType, arrayListOf(isSelected.toString()))
                        } else {
                            itemCLickListener.invoke(key, dataType,
                                    arrayListOf(child.params))
                        }
                    }
                    */
                }
                Filters.RANGER_TYPE -> {
                    it.ranges = Array(0){0}
                }

                Filters.TIMES_RANGER_TYPE -> {
                    it.ranges = Array(0){0}
                }

                Filters.STAR_TYPE -> {
                    it.rating = 0.0f
                }
            }

        }
    }



}