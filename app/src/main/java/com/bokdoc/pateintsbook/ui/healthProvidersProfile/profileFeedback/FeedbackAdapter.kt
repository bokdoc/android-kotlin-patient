package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_review.view.*

class FeedbackAdapter(private val feedback: List<IFeedback>) : RecyclerView.Adapter<FeedbackAdapter.FeedbackViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackViewHolder {
        return FeedbackViewHolder(parent.inflateView(R.layout.item_review))
    }

    override fun getItemCount(): Int {
        return feedback.size
    }

    override fun onBindViewHolder(holder: FeedbackViewHolder, position: Int) {
        feedback[position].let {
            Glide.with(holder.itemView.context).load(it.picture).into(holder.itemView.image)
            holder.itemView.name.text = it.name
            holder.itemView.rating.rating = it.rate.toFloat()
            holder.itemView.description.text = it.content
            holder.itemView.dateTime.text = it.date
            holder.itemView.tv_review_time.text = it.time
        }

        if (position == itemCount - 1) {
            holder.itemView.divider.visibility = GONE
        } else {
            holder.itemView.divider.visibility = VISIBLE
        }

    }

    class FeedbackViewHolder(view: View) : RecyclerView.ViewHolder(view)
}


