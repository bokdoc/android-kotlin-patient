package com.bokdoc.pateintsbook.ui.notifications

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bokdoc.pateintsbook.ui.searchResults.CardLinksAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_notification.view.*
import kotlinx.android.synthetic.main.item_progress.view.*

class NotificationsAdapter(private var notifications: List<INotifications>, private val onClickListener: (INotifications, DynamicLinks, Int) -> Unit)
    : PaginationRecyclerViewAdapter<INotifications>(R.layout.item_notification, notifications as MutableList<INotifications>) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            IListItemType.Types.ITEM.ordinal -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = NotificationsViewHolder(itemView)
            }
            IListItemType.Types.PAGINATION.ordinal -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }
        }
        return (viewHolder)!!
    }

    override fun getItemViewType(position: Int): Int {
        //notifications[position] as Notifications)
        return 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            IListItemType.Types.ITEM.ordinal -> {
                notifications[position].let { notification ->

                    if (notification.actionUrl != null) {

                        holder.itemView.rv_notification_buttons.visibility = View.VISIBLE

                        holder.itemView.rv_notification_buttons.adapter = NotificationActionsAdapter(notification.actionUrl!!) {
                            onClickListener.invoke(notification, it, position)

                        }
                    }

                    Glide.with(holder.itemView.context).load(notification.profile!!.picture!!.url).into(holder.itemView.image)
                    holder.itemView.name.text = notification.title

                    notification.dateTime?.let {
                        holder.itemView.tv_date.text = it.date
                        holder.itemView.tv_time.text = it.time
                    }
                    notification.content?.let {
                        holder.itemView.description.text = it
                    }
                }


            }
            LOADING -> {
                if (retryPageLoad) {
                    holder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    holder.itemView.loadmore_progress.visibility = View.GONE

                } else {
                    holder.itemView.loadmore_errorlayout.visibility = View.GONE
                    holder.itemView.loadmore_progress.visibility = View.VISIBLE
                }

            }

        }
    }


    inner class NotificationsViewHolder(val view: View) : PaginationRecyclerViewAdapter.DataViewHolder(view)

    inner class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)
}