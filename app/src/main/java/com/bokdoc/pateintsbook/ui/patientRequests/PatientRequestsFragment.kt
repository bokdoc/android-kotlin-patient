package com.bokdoc.pateintsbook.ui.patientRequests

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_requests.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.ArrayList

class PatientRequestsFragment : Fragment(), View.OnClickListener {

    lateinit var requestsViewModel: RequestsViewModel

    override fun onClick(p0: View?) {

        when (p0!!.id) {
            filterIcon.id -> {
                goToFilterActivity()
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_requests, container, false)
    }

    private fun goToFilterActivity() {
        if (requestsViewModel.metaLiveData.value?.filter != null && requestsViewModel.metaLiveData.value?.filter!!.isNotEmpty()) {
            val intent = Intent(activity, FilterActivity::class.java).putParcelableArrayListExtra("list",
                    requestsViewModel.metaLiveData.value?.filter?.get(0)?.data as ArrayList<out Parcelable>)
            startActivityForResult(intent, RequestsActivity.FILTER_REQUEST)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleText.text = getString(R.string.my_requests)
        showFilter()
        filterIcon.setOnClickListener(this::onClick)
        getViewModel()
        setUpTabs()
    }

    fun showFilter() {
        filterIcon.visibility = View.VISIBLE
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
                 requestsViewModel.getRequests(data!!.getStringExtra("selectedParam"))
        }
    }


    private fun getViewModel() {
        requestsViewModel = InjectionUtil.getRequestsViewModel(activity!!)
    }


    private fun setUpTabs() {
        //setup pager
        viewPager.adapter = TabsPagerAdapter(activity!!.supportFragmentManager)
        tab_requests.setupWithViewPager(viewPager)
        viewPager.currentItem = 0


        //setup tabs
        /*
        val tabLayout = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout.text = getString(R.string.ongoing_request)
        tabLayout.setTextColor(getResColor(R.color.colorWhite))
        */
        tab_requests.getTabAt(0)?.customView = (viewPager.adapter as TabsPagerAdapter).getTabView(activity!!, getString(R.string.ongoing_request))

        tab_requests.getTabAt(1)?.customView = (viewPager.adapter as TabsPagerAdapter).getTabView(activity!!, getString(R.string.past_requests), false)

        tab_requests.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                (tab.customView as TextView).setBackgroundResource(R.drawable.requests_custom_selected_tabs)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                (tab.customView as TextView).setBackgroundColor(ContextCompat.getColor(activity!!, R.color.colorTransparent))
            }
        })

        /*
        val tabLayout1 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout1.text = getString(R.string.past_requests)
        tabLayout1.setTextColor(getResColor(R.color.colorWhite))
        tab_requests.getTabAt(1)?.customView = tabLayout1
        */

    }


    companion object {

        @JvmStatic
        fun newInstance() =
                PatientRequestsFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}