package com.bokdoc.pateintsbook.ui.filterMap

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.searchResultsMap.SearchMapsViewModel
import com.bokdoc.pateintsbook.utils.CustomCrystalRangeSeekbar
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_filter_map.*
import kotlinx.android.synthetic.main.ranger_view.view.*
import com.bokdoc.pateintsbook.R.id.location
import com.bokdoc.pateintsbook.utils.DialogCreator
import com.bokdoc.pateintsbook.utils.LocationEnabledChecker
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.Circle
import com.google.zxing.common.StringUtils
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import kotlinx.android.synthetic.main.filter_child.*


class FilterMapActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var mMapViewholder: SearchMapsViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var latLng: LatLng? = null
    private lateinit var locationCallback: LocationCallback
    private var circle: Circle? = null
    private lateinit var autocompleteFragment: PlaceAutocompleteFragment
    private lateinit var autoCompleteEditText: EditText
    private var lastMarker: Marker? = null
    var mapView: View? = null
    var passedSelectedValue = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_map)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (LocaleManager.isReverse(this)) {
            kiloMetersRanger.setRightPosition()
        }

        if(intent.hasExtra(getString(R.string.selected_map_range))){
            passedSelectedValue = intent.getStringExtra(getString(R.string.selected_map_range)).toFloat()
        }

       locationCallback = object : LocationCallback() {
           override fun onLocationResult(locationResult: LocationResult?) {
               locationResult ?: return
               for (location in locationResult.locations) {
                  latLng = LatLng(location.latitude, location.longitude)
                  drawMarkerAndUpdateCircle()
               }
          }
      }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.view;
        kiloMetersRanger.setRangeType(CustomCrystalRangeSeekbar.DEFAULT)
        kiloMetersRanger.minAppendText = getString(R.string.kiloMeters)
        kiloMetersRanger.initializeMinMax(MIN_KILO_METERS.toFloat(), MAX_KILO_METERS.toFloat(),
                passedSelectedValue)

        kiloMetersRanger.setRangeSeekListener { min, max ->
            drawCircle(min.toInt())
        }

        apply.setOnClickListener {
            getBoundaryText().let {
                if (it.isNotEmpty() && !it.contains("null")) {
                    Navigator.naviagteBack(this, arrayOf(FILTER_BOUNDARY), arrayOf(it))
                } else {
                    Utils.showLongToast(this, R.string.select_your_location_first)
                }
            }

        }

        cancel.setOnClickListener {
            finish()
        }

        setupPlaceAutoComplete()

    }

    private fun drawMarkerAndUpdateCircle() {
        if (latLng != null) {
            lastMarker?.remove()
            lastMarker = mMap.addMarker(MarkerOptions().position(latLng!!).title(getString(R.string.current_location)))
            drawCircle(MIN_KILO_METERS)
        }
    }

    private fun setupPlaceAutoComplete() {
        autocompleteFragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment
        autoCompleteEditText = autocompleteFragment.view!!.findViewById(R.id.place_autocomplete_search_input)
        autoCompleteEditText.setTextSize(12.0f)
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                latLng = place.latLng
                /*
                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(12.0f).build()
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition))
                        */
                drawMarkerAndUpdateCircle()
            }

            override fun onError(status: Status) {

            }
        })

    }

    private fun getBoundaryText(): String {
        if (latLng?.latitude != null && latLng?.longitude != null) {
//            val minRange = kiloMetersRanger.minRangeTextView.text.split(" ")
//            val newMinRange = StringsConstants.buildString(arrayListOf(minRange[0], " ", "km"))
            return latLng?.latitude.toString() + "," + latLng?.longitude + "," + kiloMetersRanger.selectedValue.toInt()
                    .toString()
        }
        return StringsConstants.EMPTY
    }

    private fun drawCircle(radius: Int) {
        circle?.remove()

        if (latLng == null)
            return

        circle = mMap.addCircle(CircleOptions().center(latLng).radius((radius * 1000).toDouble()).fillColor(ContextCompat.getColor(this, R.color.colorMapCircle)).strokeColor(ContextCompat.getColor(this, R.color.colorAccent)))

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, getZoomLevel(circle)))
    }

    fun getZoomLevel(circle: Circle?): Float {
        if (circle != null) {
            val radius = circle.radius
            val scale = radius / 300
            return (16 - Math.log(scale) / Math.log(2.0)).toFloat()
        }
        return 0.0f
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMinZoomPreference(1F)
        mMap.setMaxZoomPreference(13F)
        // Get the button view
        val locationButton = (mapView!!.findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(Integer.parseInt("2"))
        val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 150);
        checkPermissions()
    }

    @SuppressLint("MissingPermission")
    private fun checkPermissions() {

        val snackPermissionDenied = SnackbarOnDeniedPermissionListener.Builder.with(map_parent,
                R.string.location_permissions_message)
                .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                .build()

        val permissionListener = object : PermissionListener {
            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                mMap.isMyLocationEnabled = true

                getCurrentLocation()
            }

            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                DialogCreator.show(this@FilterMapActivity, R.string.location_permissions_message, R.string.mdtp_ok, {
                    token!!.cancelPermissionRequest()
                }, {
                    token!!.cancelPermissionRequest()
                }, R.string.cancel, {
                    token!!.cancelPermissionRequest()
                })
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {

            }

        }

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(CompositePermissionListener(permissionListener, snackPermissionDenied))
                .withErrorListener { Utils.showShortToast(this@FilterMapActivity, R.string.some_thing_went_wrong) }
                .check()

    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        if (LocationEnabledChecker.isLocationEnabled(this)) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    latLng = LatLng(it.latitude, it.longitude)
                    /*
                    val cameraPosition = CameraPosition.Builder()
                            .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(15.0f).build()
                    mMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition))
                            */

                    drawMarkerAndUpdateCircle()

                } else {
                    fusedLocationClient.requestLocationUpdates(LocationRequest().apply {
                        interval = 10000
                        fastestInterval = 5000
                        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    }, locationCallback, null)
                }
            }
        } else {
            DialogCreator.show(this, R.string.gps_network_not_enabled
                    , R.string.settings) {
                Navigator.navigate(this, Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTINGS_REQUEST)
            }
        }
    }

    companion object {
        const val MIN_KILO_METERS = 1
        const val MAX_KILO_METERS = 100
        const val FILTER_BOUNDARY = "boundary"
        const val LOCATION_SETTINGS_REQUEST = 1

    }


}