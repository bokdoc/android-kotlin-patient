package com.bokdoc.pateintsbook.ui.healthProvidersProfile

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Review
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback.FeedbackAdapter
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.patientProfile.FilterActivity
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsActivity
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsAdapter
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsViewModel
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.Exception
import java.util.ArrayList

class AllProvidersReviewsActivity : ParentActivity() {

    lateinit var reviewsViewModel: ReviewsViewModel

    private var reviewsAdapter: FeedbackAdapter? = null
    var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()

        val typeKey = intent?.getStringExtra(getString(R.string.typeKey))!!

        when (typeKey) {
            Profile.DOCTOR.toString() -> {
                title = getString(R.string.reviews_list_doc_place_holder, Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
            }
        }


        try {
            if(Profile.isHospitalCategory(typeKey.toInt()))
                title = getString(R.string.reviews_list_hos_place_holder,
                        intent?.getStringExtra(getString(R.string.docNameSlug))!!)
        }catch (e:Exception){

        }



        showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)

        getViewModel()
        getDataFromIntent()
        getAllReviews()
        showFilter()
        filterIcon.setOnClickListener {
            goToFilterActivity()
        }
        setupViewModelObservers()
    }

    private fun setupViewModelObservers() {

        reviewsViewModel.nextReviewsLiveData.observe(this, Observer {
            isLoading = false
            if (it != null && it.isNotEmpty()) {

            }
        })

        reviewsViewModel.firstFeedbacksLiveData.observe(this, Observer {
            progress.visibility = View.GONE
            when (it!!.size) {
                0 -> showEmpty("No Reviews Added Yet", "", null)
                else -> {
                    showMain(lay_container)
                    hideEmpty()
                    rightTextView.visibility = View.VISIBLE
                    reviewsAdapter = FeedbackAdapter((it as MutableList<Feedback>?)!!)
                    rv_list.adapter = reviewsAdapter
                }
            }
        })

        reviewsViewModel.metaLiveData.observe(this, Observer {

            /*
            TOTAL_PAGES = it!!.pagination.totalPages!!

            if (currentPage <= TOTAL_PAGES) {
                reviewsAdapter?.addLoadingFooter()
            } else
                isLastPage = true
                */
        })



        reviewsViewModel.progressLiveData.observe(this, Observer {
            when (it) {
                false -> {
                    rv_list.visibility = View.VISIBLE
                    progress.visibility = View.GONE
                }

                true -> {
                    rv_list.visibility = View.GONE
                    progress.visibility = View.VISIBLE

                }
            }
        })

        reviewsViewModel.errorMessageLiveData.observe(this, Observer {
            showError(getString(R.string.some_thing_went_wrong), lay_container)
        })

    }

    private fun goToFilterActivity() {
        if (reviewsViewModel.metaLiveData.value?.filter != null && reviewsViewModel.metaLiveData.value?.filter!!.isNotEmpty()) {
            val intent = Intent(activity, FilterActivity::class.java).putParcelableArrayListExtra("list",
                    reviewsViewModel.metaLiveData.value?.filter?.get(0)?.data as ArrayList<out Parcelable>)
            activity.startActivityForResult(intent, FILTER_REQUEST)
        }
    }

    private fun getAllReviews() {
        reviewsViewModel.getProviderReviews()
    }

    private fun getViewModel() {
        reviewsViewModel = InjectionUtil.getReviewsViewModel(this)
    }

    private fun getDataFromIntent() {
        if (intent.hasExtra(getString(R.string.idKey))) {
            reviewsViewModel.providerId = intent.getStringExtra(getString(R.string.idKey))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILTER_REQUEST && resultCode == Activity.RESULT_OK) {
//            (rv_patient_reviews.adapter as ReviewsAdapter).clear()
            reviewsViewModel.serviceType = (data!!.getStringExtra("selectedParam"))
            reviewsViewModel.currentPage = 1
            reviewsViewModel.getProviderReviews()
        }
    }

    companion object {
        private val PAGE_START = 1
        private val FILTER_REQUEST = 2
    }


}