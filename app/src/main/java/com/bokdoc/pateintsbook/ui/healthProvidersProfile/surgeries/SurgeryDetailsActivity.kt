package com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SingleSurgery
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.NextAvailabilitiesAdapter
import com.bokdoc.pateintsbook.ui.imagesViewer.ImagesViewerActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.patientProfile.PatientMediaAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.formatters.FeesFormatter
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_surgeries_details.*
import kotlinx.android.synthetic.main.include_procedure_rules.*
import kotlinx.android.synthetic.main.include_surgery_main_data.*
import kotlinx.android.synthetic.main.include_treatmaent_plan_rules.*
import kotlinx.android.synthetic.main.item_clinic.view.*

class SurgeryDetailsActivity : ParentActivity() {

    lateinit var surgeriesViewModel: SurgeriesViewModel
    var surgeriesRow: SurgeriesRow? = null
    lateinit var profileType: String
    var bookUrl: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_surgeries_details)
        mediaList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        enableBackButton()
        title = getString(R.string.details_surgery_title)
        getViewModel()
        getDataFromIntentAndBindFirstData()
        getSurgeryDetails()
        btn_book_now.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_book_now.id -> {
                when (bookUrl.isNotEmpty()) {
                    true -> {
                        Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                                arrayOf(bookUrl),
                                BookAppointmentsActivity::class.java,BOOK_REQUEST)
                    }

                }
            }
        }
    }


    private fun getSurgeryDetails() {
        showShimmer()
        lay_shimmer.startShimmerAnimation()
        surgeriesViewModel.showSurgeryDetails().observe(this, Observer {
            when (it!!.status) {
                INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    hideShimmer()
                }
                SUCCESS -> {
                    hideShimmer()
                    it.data!![0].let {
                        bindLoadedData(it)
                    }
                }
                else -> {
                    Utils.showShortToast(this, it.errors[0].detail)
                 }
            }
        })
    }

    private fun bindLoadedData(it: SingleSurgery) {



        bindNextAvailabilities(it)

        it.bookButton?.let {
            if(rv_next_avil.visibility == GONE) {
                bookUrl = it.endpointUrl
                btn_book_now.visibility = VISIBLE
            }
        }


        // fees check

        FeesFormatter.setFees(it.nationalFees,tv_surgery_fees_from,
                tv_surgery_fees_to,
                it.interNationalFees,tv_surgery_fees_national_from,
                tv_surgery_fees_national_to,iv_fees)

        // proceduer check
        if (!surgeriesViewModel.hasProcedureRules(it)) {
            include_procedure_rules_profile.visibility = View.GONE
        }

        if (it.approvalType.isNotEmpty()) {
            tv__approval_type_surgery.text = it.approvalType
        } else {
            group_approval_type.visibility = View.GONE
        }

        if (it.paymentType.isNotEmpty()) {
            tv_payment_type_surgery.text = it.paymentType
        } else {
            group_payment_type.visibility = View.GONE
        }

        if (it.watingQueu.isNotEmpty()) {
            tv__waiting_queue_surgery.text = it.watingQueu
        } else {
            group_waiting_queue.visibility = View.GONE
        }


        //treatment plan check
        if (!surgeriesViewModel.hasTreatmentPlan(it)) {
            include_treatmaent_plan_profile.visibility = View.GONE
        }

        if (it.laboratoryTestIncluded.isNotEmpty()) {
            tv_included_laboratory_radiology.text = android.text.TextUtils.join(",", it.laboratoryTestIncluded)
        } else {
            group_included_laboratory_radiology.visibility = View.GONE
        }

        if (it.preOrderRadiologyScans.isNotEmpty()) {
            tv_pre_laboratory_radiology.text = android.text.TextUtils.join(",", it.preOrderRadiologyScans)
        } else {
            group_pre_laboratory_radiology.visibility = View.GONE
        }

        if (it.anesthesiaTypes.isNotEmpty()) {
            tv_anesthesia_type.text = android.text.TextUtils.join(",", it.anesthesiaTypes)
        } else {
            group_anesthesia_type.visibility = View.GONE
        }

        if (it.duration.isNotEmpty()) {
            tv_operation_duration.text = it.duration
        } else {
            group_operation_duration.visibility = View.GONE
        }

        if (it.programDuration.isNotEmpty()) {
            tv_program_duration.text = it.duration
        } else {
            group_program_duration.visibility = View.GONE
        }

        setupMediaData(it.media)

    }


    private fun bindNextAvailabilities(singleSurgery: SingleSurgery){

        if(singleSurgery.nextAvailabilities.isEmpty()){
            iv_right.visibility = GONE
            iv_left.visibility = GONE
            rv_next_avil.visibility = GONE
            line.visibility = GONE
        }else {
            val layoutManger = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            rv_next_avil.layoutManager = layoutManger
            rv_next_avil.smoothScrollToPosition(0)
            rv_next_avil.adapter = NextAvailabilitiesAdapter(singleSurgery.nextAvailabilities, this::onBookClickListner)

            iv_right.setOnClickListener {

                val totalItemCount = rv_next_avil.adapter!!.getItemCount()
                if (totalItemCount <= 0) return@setOnClickListener
                val lastVisibleItemIndex = layoutManger.findLastVisibleItemPosition()

                if (lastVisibleItemIndex >= totalItemCount) return@setOnClickListener
                layoutManger.smoothScrollToPosition(rv_next_avil, null, lastVisibleItemIndex + 1)


                //  holder.itemView.rv_next_next_availabilities.layoutManager!!.scrollToPosition(layoutManger.findLastVisibleItemPosition() + 1)

            }

            iv_left.setOnClickListener {

                val firstVisibleItemIndex = layoutManger.findFirstCompletelyVisibleItemPosition()
                if (firstVisibleItemIndex > 0) {
                    layoutManger.smoothScrollToPosition(rv_next_avil, null, firstVisibleItemIndex - 1)
                }
                // holder.itemView.rv_next_next_availabilities.layoutManager!!.scrollToPosition(layoutManger.findLastVisibleItemPosition() - 1)

            }
        }
    }

    private fun setupMediaData(media: List<MediaParcelable>) {

        mediaList?.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)

        if(media.isNotEmpty()){
            mediaList?.adapter = PatientMediaAdapter(surgeriesViewModel.getMediaDetails(media as ArrayList<MediaParcelable>)) { view, position ->
                Navigator.navigate(activity, arrayOf(getString(R.string.selected_position)),
                        arrayOf(position.toString()), getString(R.string.images),
                        surgeriesViewModel.getMediaUris(),
                        ImagesViewerActivity::class.java, -1)
            }
        }else{
            noMediaFound?.visibility = View.VISIBLE
        }

        surgeriesViewModel.remainingMediaLiveData.observe(this, Observer {
            if (it != null && moreText!=null)
                moreText?.text = it
        })

    }

    private fun onBookClickListner(nextAvailability: NextAvailabilitiesParcelable) {
        if (nextAvailability.book_button?.endpointUrl.isNullOrEmpty()) {
            surgeriesRow?.bookButton?.let {
                Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                        arrayOf(it.endpointUrl), getString(R.string.nextAvailabilitiesKey),
                        arrayListOf(nextAvailability), BookAppointmentsActivity::class.java,BOOK_REQUEST)
            }
        } else {
            surgeriesRow?.bookButton?.let {
                Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                        getString(R.string.book_url_key)), arrayOf(nextAvailability.book_button!!.endpointUrl,
                        it.endpointUrl)
                        , NextAvailabilityActivity::class.java, BOOK_REQUEST)
            }
        }
    }

    private fun getDataFromIntentAndBindFirstData() {
        if (intent.hasExtra(getString(R.string.surgery_details))) {
            surgeriesRow = intent.getParcelableExtra(getString(R.string.surgery_details))
            surgeriesViewModel.url = surgeriesRow!!.showUrl
            profileType = intent.getStringExtra(getString(R.string.typeKey))
            bindData()
        }
    }

    private fun bindData() {
        if(Profile.isHospitalCategory(profileType.toInt())){
            if(!surgeriesRow?.doctorName.isNullOrEmpty()) {
                group_doctor_data.visibility = View.VISIBLE
                profileName.text = surgeriesRow!!.doctorName
                surgeriesRow?.doctorPhoto.let {
                    Glide.with(this).load(surgeriesRow?.doctorPhoto).into(profileImage)
                }
            }
            tv_surgery_name.text = surgeriesRow!!.surgeryName
            profileDesc.text = surgeriesRow!!.speciality
        }
        if(profileType.toInt() == Profile.DOCTOR){
            group_doctor_data.visibility = View.GONE
            tv_surgery_name.text = surgeriesRow!!.surgeryName
            if(!surgeriesRow?.location?.address.isNullOrEmpty()){
                tv_address.visibility = VISIBLE
                tv_address.text = surgeriesRow!!.location.address
            }
        }
        //bind fees
    }

    fun showShimmer() {
        lay_shimmer.visibility = View.VISIBLE
        include_procedure_rules_profile.visibility = View.GONE
        include_treatmaent_plan_profile.visibility = View.GONE
    }

    fun hideShimmer() {
        lay_shimmer.visibility = View.GONE
        include_procedure_rules_profile.visibility = View.VISIBLE
        include_treatmaent_plan_profile.visibility = View.VISIBLE
    }

    private fun getViewModel() {
        surgeriesViewModel = InjectionUtil.getSurgeriesViewModel(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == BOOK_REQUEST && resultCode== Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


    companion object {
        const val BOOK_REQUEST = 1
    }
}