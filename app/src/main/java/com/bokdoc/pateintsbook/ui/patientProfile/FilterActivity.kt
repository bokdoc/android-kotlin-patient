package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.ServiceTypeConverter
import com.bokdoc.pateintsbook.data.models.FilterModel
import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.data.models.meta.FilterData
import com.bokdoc.pateintsbook.data.models.meta.ServiceType
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import kotlinx.android.synthetic.main.activity_sort_by.*
import kotlinx.android.synthetic.main.toolbar.*

class FilterActivity : ParentActivity() {

    lateinit var serviceTypes: List<FilterData>
    lateinit var filterViewModel: FilterViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sort_by)
        enableBackButton()
        title = getString(R.string.filter_my_requests)
        done.setOnClickListener(this)
        getDataFromIntent()
        getViewModel()

        setData()


        rightTextView.visibility = VISIBLE
        rightTextView.text = getString(R.string.done)
        rightTextView.setOnClickListener {
            val selectedParam = (sortList.adapter as FilterAdapter).getSelectedParam()
            filterViewModel.saveSelected(selectedParam)
            Navigator.naviagteBack(this, arrayOf(SELECTED_PARAM), arrayOf(selectedParam))
        }
    }

    private fun getViewModel() {
        filterViewModel = ViewModelProviders.of(this, FilterModelFactory(this)).get(FilterViewModel::class.java)
    }

    private fun setData() {
        sortList.adapter = FilterAdapter(ServiceTypeConverter.convert(serviceTypes), filterViewModel.getIndexOfSelected(ServiceTypeConverter.convert(serviceTypes)))

    }

    private fun getDataFromIntent() {
        serviceTypes = intent.getParcelableArrayListExtra("list")
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            done.id -> {
                val selectedParam = (sortList.adapter as FilterAdapter).getSelectedParam()
                filterViewModel.saveSelected(selectedParam)
                Navigator.naviagteBack(this, arrayOf(SELECTED_PARAM), arrayOf(selectedParam))
            }
        }
    }

    companion object {
        const val SELECTED_PARAM = "selectedParam"
    }

}