package com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.R.id.group_doctor_data
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.NextAvailabilitiesAdapter
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bokdoc.pateintsbook.utils.formatters.FeesFormatter
import com.bumptech.glide.Glide
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import kotlinx.android.synthetic.main.item_clinic.view.*
import kotlinx.android.synthetic.main.item_progress.view.*


class SurgeriesAdapter(var surgeries: ArrayList<SurgeriesRow>,
                       var itemCLickListener: (url: String, nextAvi: NextAvailabilitiesParcelable, position: Int) -> Unit,
                       private val selectedListener: (surgery: SurgeriesRow) -> Unit, private var onMapClickListener: (location: Location) -> Unit
                       , private var onBookClickListener: (dynamicLink: DynamicLinks) -> Unit)
    : PaginationRecyclerViewAdapter<SurgeriesRow>(R.layout.item_clinic, surgeries) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = SurgeriesViewHolder(itemView)
            }

            LOADING -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }

    override fun getItemViewType(position: Int): Int {
        return if (surgeries[position].type == 0) {
            ITEM
        } else {
            LOADING
        }
    }

    override fun getItemCount(): Int {
        return surgeries.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (getItemViewType(position)) {
            ITEM -> {
                surgeries[position].let {
                    holder.itemView.tv_periodic_text.visibility = View.GONE
                    holder.itemView.tv_media_view_all.visibility = View.GONE

                    Glide.with(holder.itemView.context).load(ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_all_surgeries))
                            .into(holder.itemView.iv_icon)

                    if (Profile.isHospitalCategory(it.profileType)) {
                        if (it.doctorName?.isEmpty()!!) {
                            holder.itemView.group_doctor_data.visibility = View.GONE
                            holder.itemView.profileName.visibility = View.GONE
                            holder.itemView.profileImage.visibility = View.GONE
                        } else {
                            holder.itemView.group_doctor_data.visibility = View.VISIBLE
                            holder.itemView.profileName.text = it.doctorName
                            Glide.with(holder.itemView.context).load(it.doctorPhoto).into(holder.itemView.profileImage)
                        }
                        holder.itemView.profileDesc.text = holder.itemView.context.getString(R.string.two_texts_placeholder,
                                it.doctorTitle, it.speciality)
                    }

                    if (it.profileType == Profile.DOCTOR) {
                        holder.itemView.group_doctor_data.visibility = View.GONE
                        holder.itemView.address_group.visibility = View.VISIBLE
                    }

                    if (it.nextAvailabilities.isEmpty()) {
                        holder.itemView.nextGroup.visibility = View.GONE
                        holder.itemView.btn_book_surgery.visibility = View.VISIBLE
                        holder.itemView.btn_book_surgery.text = it.bookButton?.label
                    } else {
                        holder.itemView.nextGroup.visibility = View.VISIBLE
                        holder.itemView.btn_book_surgery.visibility = View.GONE
                    }

                    if (it.appointmentReservation.isNotEmpty()) {
                        holder.itemView.tv_service_entrance.visibility = View.VISIBLE
                        holder.itemView.tv_service_entrance.text = it.appointmentReservation

                    }
                    holder.itemView.tv_surgery_name.text = it.surgeryName
                    //holder.itemView.iv_fees.visibility = View.VISIBLE

                    holder.itemView.tv_address.text = ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                            holder.itemView.context.getString(R.string.address), it.location.address), it.location.address, ContextCompat.getColor(holder.itemView.context, R.color.colorDescription))



                    FeesFormatter.setFees(it.nationalFees, holder.itemView.tv_surgery_fees_from,
                            holder.itemView.tv_surgery_fees_to,
                            it.interNationalFees, holder.itemView.tv_surgery_fees_national_from,
                            holder.itemView.tv_surgery_fees_national_to, holder.itemView.iv_fees)

                    it.nationalFees?.let { fees ->
                        if (FeesFormatter.isMainFeesHasValue(fees)) {
                            holder.itemView.tv_surgery_fees_from.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
                            holder.itemView.tv_surgery_fees_from.textSize = getValue(R.dimen.text_size_float_12, holder.itemView.context)
                            holder.itemView.tv_surgery_fees_to.textSize = getValue(R.dimen.text_size_float_12, holder.itemView.context)
                        } else {
                            holder.itemView.tv_surgery_fees_from.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorDescription))
                            holder.itemView.tv_surgery_fees_to.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorDescription))
                            holder.itemView.tv_surgery_fees_from.textSize =
                                    holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_9).toFloat()
                            holder.itemView.tv_surgery_fees_to.textSize =
                                    holder.itemView.tv_surgery_fees_from.context.resources.getInteger(R.integer.value_9).toFloat()
                        }
                    }


                    val layoutManger = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
                    holder.itemView.rv_next_next_availabilities.layoutManager = layoutManger
                    holder.itemView.rv_next_next_availabilities.smoothScrollToPosition(0)
                    holder.itemView.rv_next_next_availabilities.adapter = NextAvailabilitiesAdapter(it.nextAvailabilities, {
                        if (surgeries[position].bookButton != null)
                            itemCLickListener.invoke(surgeries[position].bookButton!!.endpointUrl, it, position)
                    })



                    holder.itemView.iv_right.setOnClickListener {

                        val totalItemCount = holder.itemView.rv_next_next_availabilities.adapter!!.getItemCount()
                        if (totalItemCount <= 0) return@setOnClickListener
                        val lastVisibleItemIndex = layoutManger.findLastVisibleItemPosition()

                        if (lastVisibleItemIndex >= totalItemCount) return@setOnClickListener
                        layoutManger.smoothScrollToPosition(holder.itemView.rv_next_next_availabilities, null, lastVisibleItemIndex + 1)


                        //  holder.itemView.rv_next_next_availabilities.layoutManager!!.scrollToPosition(layoutManger.findLastVisibleItemPosition() + 1)

                    }

                    holder.itemView.iv_left.setOnClickListener {

                        val firstVisibleItemIndex = layoutManger.findFirstCompletelyVisibleItemPosition()
                        if (firstVisibleItemIndex > 0) {
                            layoutManger.smoothScrollToPosition(holder.itemView.rv_next_next_availabilities, null, firstVisibleItemIndex - 1)
                        }
                    }

                    if (it.location.latitude.isEmpty() && it.location.longitude.isEmpty()) {
                        holder.itemView.iv_location.visibility = View.GONE
                    }

                    if (it.location.address.isEmpty()) {
                        holder.itemView.address_group.visibility = View.GONE
                    }
                }
            }
            LOADING -> {
                if (retryPageLoad) {
                    holder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    holder.itemView.loadmore_progress.visibility = View.GONE

                } else {
                    holder.itemView.loadmore_errorlayout.visibility = View.GONE
                    holder.itemView.loadmore_progress.visibility = View.VISIBLE
                }
            }
        }


    }

    fun getValue(resource: Int, context: Context): Float {
        val outValue = TypedValue()
        context.resources.getValue(R.dimen.text_size_float_12, outValue, true)
        return outValue.float

    }


    fun updateList(surgeriesRow: ArrayList<SurgeriesRow>) {
        surgeries = surgeriesRow
        notifyDataSetChanged()
    }

    fun add(surgeriesRow: ArrayList<SurgeriesRow>) {
        val lastPosition = itemCount
        surgeries.addAll(surgeriesRow)
        notifyItemRangeInserted(lastPosition, itemCount)
    }


    fun remove(position: Int) {
        surgeries.removeAt(position)
        notifyItemRemoved(position)
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(SurgeriesRow().apply {
            id = ""
            surgeryName = ""
            doctorName = ""
            gender = ""
            doctorPhoto = ""
            doctorTitle = ""
            speciality = ""
            nationalFees = null
            interNationalFees = null
            nationalFeesLable = ""
            internationalFeesLable = ""
            appointmentReservation = ""
            feesCurrancy = ""
            showUrl = ""
            nextAvailabilities = ArrayList()
            location = Location()
            profileType = 1
            bookButton = null
            type = 1
        })
    }


    inner class SurgeriesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                selectedListener.invoke(surgeries[adapterPosition])
            }
            view.iv_location.setOnClickListener {
                onMapClickListener.invoke(surgeries[adapterPosition].location)
            }
            view.btn_book_surgery.setOnClickListener {
                surgeries[adapterPosition].bookButton?.let { it1 -> onBookClickListener.invoke(it1) }
            }
        }
    }
}