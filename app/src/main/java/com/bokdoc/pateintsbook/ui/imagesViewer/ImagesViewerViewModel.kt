package com.bokdoc.pateintsbook.ui.imagesViewer

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.R

class ImagesViewerViewModel : ViewModel() {
    var selectedPosition = 0
    var images = ArrayList<Uri>()

    fun getTotalNumberWithSelected(context: Context, resPlaceHolder:Int= R.string.slash_place_holder):String{
        return context.
                getString(resPlaceHolder,selectedPosition.plus(1).toString(),images.size.toString())
    }
}