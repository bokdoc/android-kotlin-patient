package com.bokdoc.pateintsbook.ui.patientRequests

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import com.bokdoc.pateintsbook.R


class TabsPagerAdapter(supportFragmentManage: android.support.v4.app.FragmentManager) : FragmentPagerAdapter(supportFragmentManage) {


    override fun getItem(position: Int): Fragment {
        return PastRequestsFragment.newInstance()
    }

    fun getTabView(context:Context,title:String,isSelected:Boolean=true): View {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        val v = LayoutInflater.from(context).inflate(R.layout.request_custom_tab, null)
        val tv = v  as TextView
        tv.text = title
        if(isSelected)
            tv.setBackgroundResource(R.drawable.requests_custom_selected_tabs)
        return v
    }

    override fun getCount(): Int {
        return 1
    }
}