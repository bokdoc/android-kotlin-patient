package com.bokdoc.pateintsbook.ui.splash

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmEncoder
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.data.webservice.addFcm.CheckUpdateEncoder
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashRepository(private val appContext: Context,
                       val sharedPreferencesManagerImpl: UserSharedPreference,
                       val addFcmWebService: AddFcmWebService) {

    fun isOpenTutorialsScreen(): Boolean {
        return sharedPreferencesManagerImpl.sharedPrefrenceManger.getData(IS_FIRST_APP_OPENED, true)
    }

    fun updateFirstAppOpenedInSharedPreferences(isFirstOpened: Boolean = false) {
        sharedPreferencesManagerImpl.sharedPrefrenceManger.saveData(IS_FIRST_APP_OPENED, isFirstOpened)
    }

    fun isFcmSent(): Boolean {
        return sharedPreferencesManagerImpl.sharedPrefrenceManger.getData(IS_FCM_SENT, true)
    }



    fun addFcmToken(): LiveData<DataResource<Boolean>> {
        val liveData = MutableLiveData<DataResource<Boolean>>()

        if (sharedPreferencesManagerImpl.getUser() != null && sharedPreferencesManagerImpl.getUser()?.token!!.isNotEmpty()) {

            val json = AddFcmEncoder.getJson(
                    sharedPreferencesManagerImpl.sharedPrefrenceManger.getData(appContext.getString(R.string.fcm_token), ""))
            addFcmWebService.addFcm(sharedPreferencesManagerImpl.sharedPrefrenceManger
                    .getData(appContext.getString(R.string.token), ""), json).enqueue(AddFcmResponse(liveData, sharedPreferencesManagerImpl.sharedPrefrenceManger))
        } else {
            liveData.postValue(DataResource())
        }
        return liveData
    }


    class AddFcmResponse(val liveData: MutableLiveData<DataResource<Boolean>>, val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl) : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            val dataResource = DataResource<Boolean>()
            dataResource.status = DataResource.FAIL
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            val dataResource = DataResource<Boolean>()
            if (response?.code() == 200) {
                dataResource.status = DataResource.SUCCESS
                sharedPreferencesManagerImpl.saveData(IS_FCM_SENT, true)
            } else {
                dataResource.status = DataResource.FAIL
            }
            liveData.value = dataResource
        }

    }


    fun checkUpdate(): LiveData<DataResource<CheckUpdate>> {
        val liveData = MutableLiveData<DataResource<CheckUpdate>>()
        if (sharedPreferencesManagerImpl.getUser()?.token!!.isNotEmpty()) {
            val json = CheckUpdateEncoder.getJson(Utils.getVirsionCode().toString(),sharedPreferencesManagerImpl.sharedPrefrenceManger.getData( appContext.getString(R.string.token), ""))
            addFcmWebService.checkUpdate(sharedPreferencesManagerImpl.sharedPrefrenceManger.getData(appContext.getString(R.string.token), ""), json)
                    .enqueue(CheckUpdateResponse(liveData))
        } else {
            liveData.value = DataResource()
        }
        return liveData
    }


    class CheckUpdateResponse(val liveData: MutableLiveData<DataResource<CheckUpdate>>) : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            val dataResource = DataResource<CheckUpdate>()
            dataResource.status = DataResource.FAIL
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            val dataResource = DataResource<CheckUpdate>()
            if (response?.code() == 200) {
                val checkUpdate = Parser.parseJson(response?.body()!!.string(), arrayOf(CheckUpdate::class.java))
                dataResource.status = DataResource.SUCCESS
                dataResource.data = checkUpdate as List<CheckUpdate>
            } else {
                dataResource.status = DataResource.FAIL
            }
            liveData.value = dataResource
        }

    }


    companion object {
        const val IS_FIRST_APP_OPENED = "isFirstAppOpened"
        const val IS_FCM_SENT = "isFcmSent"
    }
}