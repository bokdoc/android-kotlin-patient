package com.bokdoc.pateintsbook.ui.healthProvidersProfile.insuranceCarriers

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.InsuranceCarriers.InsuranceCarriersRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.InsuranceCarriersRow
import com.bokdoc.pateintsbook.data.webservice.models.InsuranceCarrier

class InsuranceCarriersViewModel(val insuranceCarriersRepository: InsuranceCarriersRepository) : ViewModel() {

    lateinit var id: String
    lateinit var serviceType: String

    fun getInsurance(): LiveData<DataResource<InsuranceCarriersRow>> {
        return insuranceCarriersRepository.getInsuranceCarriers(id, serviceType)
    }

}