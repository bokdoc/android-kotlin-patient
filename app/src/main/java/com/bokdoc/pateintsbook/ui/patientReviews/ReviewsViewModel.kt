package com.bokdoc.pateintsbook.ui.patientReviews

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.models.meta.PaginationMeta
import com.bokdoc.pateintsbook.data.repositories.reviews.ReviewsRepository
import com.bokdoc.pateintsbook.data.webservice.models.Review

class ReviewsViewModel(val reviewsRepository: ReviewsRepository) : ViewModel() {

    val progressLiveData = MutableLiveData<Boolean>()
    val firstReviewsLiveData = MutableLiveData<List<Review>>()
    val nextReviewsLiveData = MutableLiveData<List<Review>>()
    val firstFeedbacksLiveData = MutableLiveData<List<Feedback>>()
    val nextFeedbacksLiveData = MutableLiveData<List<Feedback>>()
    val metaLiveData = MutableLiveData<Meta>()
    val errorMessageLiveData = MutableLiveData<String>()
    var serviceType = ""
    var currentPage = 1
    var totalPages = 0
    var providerId = ""

    fun getReviews() {
        if (currentPage == 1) {
            progressLiveData.value = true
        }
        reviewsRepository.getReviews(providerId, serviceType, currentPage).observeForever {
            if (it?.data != null) {
                when (currentPage) {
                    1 -> {
                        progressLiveData.value = false
                        firstReviewsLiveData.value = it.data
                    }
                    else -> {
                        progressLiveData.value = false
                        nextReviewsLiveData.value = it.data
                    }
                }
                totalPages = it.meta.pagination.totalPages!!
                metaLiveData.value = it.meta
            } else {
                errorMessageLiveData.value = it?.message
                progressLiveData.value = false
            }
        }
    }

    fun getUserId(): String {
        return reviewsRepository.getUserId()
    }



    fun getProviderReviews() {
        if (currentPage == 1) {
            progressLiveData.value = true
        }
        reviewsRepository.getProviderReviews(providerId, serviceType, currentPage).observeForever {
            if (it?.data != null) {
                when (currentPage) {
                    1 -> {
                        progressLiveData.value = false
                        firstFeedbacksLiveData.value = it.data
                    }
                    else -> {
                        progressLiveData.value = false
                        nextFeedbacksLiveData.value = it.data
                    }
                }
                if(it.meta.pagination.totalPages!=null)
                totalPages = it.meta.pagination.totalPages!!

                metaLiveData.value = it.meta
            } else {
                errorMessageLiveData.value = it?.message
                progressLiveData.value = false
            }
        }
    }


}