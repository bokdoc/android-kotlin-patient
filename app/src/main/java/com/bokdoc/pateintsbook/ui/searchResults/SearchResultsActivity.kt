package com.bokdoc.pateintsbook.ui.searchResults

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.SearchResults.SearchResultsRepository
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks.CREATOR.KEY_DEPARTMENTS
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks.CREATOR.KEY_DOCTORS
import com.bokdoc.pateintsbook.data.webservice.models.Fees
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.filters.FiltersActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.ProfileActionsSheetAdapter
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments.DepartmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsActivity
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.searchResultsMap.SearchMapsActivity
import com.bokdoc.pateintsbook.ui.sortBy.SortByActivity
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.FilterSearchConstants
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.paginationUtils.OnVerticalScrollListener
import kotlinx.android.synthetic.main.activity_search_results.*
import kotlinx.android.synthetic.main.sheet_profile_actions.view.*
import kotlinx.android.synthetic.main.toolbar.*


class SearchResultsActivity : ParentActivity(), View.OnClickListener {

    lateinit var searchViewModel: SearchResultsViewModel
    var searchApdater: SearchResultsAdapter? = null
    private var mBottomSheetDialog: Dialog? = null

    lateinit var meta: Meta

    private var isLoading = false
    private var isLastPage = false
    private var TOTAL_PAGES: Int = 0
    private var currentPage = PAGE_START

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)
        enableBackButton()
        searchViewModel = ViewModelProviders.of(this, SearchModelProvider(this,
                ProfileSocialActionsRepository(applicationContext, SharedPreferencesManagerImpl(this), CompareSharedPreferences(this), ProfileSocailActionsWebservice(
                        WebserviceManagerImpl.instance()))))
                .get(SearchResultsViewModel::class.java)

        searchViewModel.searchType = intent.getStringExtra(getString(R.string.screenType))
        searchViewModel.query = intent.getStringExtra(getString(R.string.query))

        Log.i("query", searchViewModel.query)

        Log.i("screen_type", searchViewModel.searchType)

        // social actions listener
        searchViewModel.likedToggleLiveDate.observe(this, Observer {
            searchApdater?.toggleLike(searchViewModel.profileSelectedPosition)
            mBottomSheetDialog?.dismiss()
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.favouritesToggleLiveDate.observe(this, Observer {
            searchApdater?.toggleFavoriate(searchViewModel.profileSelectedPosition)
            mBottomSheetDialog?.dismiss()
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.comparedToggleLiveData.observe(this, Observer {
            searchApdater?.toggleCompare(searchViewModel.profileSelectedPosition)
            mBottomSheetDialog?.dismiss()
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.errorMessageLiveDate.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        title = searchViewModel.getScreenTitle(this)

        returnArrow.setOnClickListener {
            val passedIntent = Intent()
            passedIntent.putExtra(getString(R.string.defaultTab),
                    SearchScreenTypes.getPosition(searchViewModel.searchType))
            setResult(Activity.RESULT_OK, passedIntent)
            finish()
        }
        profileList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        setupListScrollListener()
        setupProfilesObserver()
        updateUIVisibility(true)
        searchViewModel.search()
    }


    private fun setupListScrollListener(){
        profileList.addOnScrollListener(object : PaginationScrollListener(profileList.layoutManager as LinearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                currentPage += 1
                if (currentPage <= TOTAL_PAGES) {
                    (profileList.adapter as SearchResultsAdapter).addLoadingFooter()
                    isLoading = true
                    searchViewModel.isResetAdapter = false
                    searchViewModel.search(currentPage)
                } else {
                    isLastPage = true
                }
            }
        })
    }


    private fun setupProfilesObserver(){
        searchViewModel.profilesResultsLiveData.observe(this, Observer {
            updateUIVisibility(false)
            if(it?.status == DataResource.SUCCESS) {
                if (!it.data.isNullOrEmpty()) {
                    if (currentPage == 1 && it.data!!.size==1 && !it.data!![0].is_message) {
                        Navigator.navigate(this, getString(R.string.profileKey), arrayListOf(searchViewModel.getHealthProviderProfile(it.data!![0])),
                                HealthProvidersProfileActivity::class.java, PROFILE_DETAILS_REQUEST)
                    }else {
                        if(currentPage>1){
                            (profileList.adapter as SearchResultsAdapter).removeLoadingFooter()
                            updateAdapter(it.data!!, searchViewModel.isResetAdapter,true)
                            isLoading=false
                        }else {
                            updateAdapter(it.data!!, searchViewModel.isResetAdapter)
                        }
                    }
                    if (it.paginationMeta.pagination.total != null)
                        updateTotalResults(it.paginationMeta.pagination.total!!)

                    if (it.paginationMeta.pagination.totalPages != null)
                        TOTAL_PAGES = it.paginationMeta.pagination.totalPages!!
                }else{
                    if(currentPage==1)
                    showEmpty(getString(R.string.no_result), "", null)
                }
            }
        })
    }

    private fun updateTotalResults(totalResults: Int) {
        rightTextView.visibility = View.VISIBLE
        rightTextView.text = getString(R.string.resultsPlaceHolder, totalResults.toString())
    }



    private fun updateUIVisibility(isProgressVisible: Boolean) {
        if (isProgressVisible) {
            progress.visibility = VISIBLE
            profileList.visibility = GONE
        } else {
            progress.visibility = GONE
            profileList.visibility = VISIBLE
        }
    }

    private fun updateAdapter(profiles: List<Profile>, isResetAdapter: Boolean = false,isAppendToList:Boolean=false) {

        if (searchApdater == null || isResetAdapter) {
            searchApdater = SearchResultsAdapter(profiles, this::onAdapterCLickListener)
            profileList.adapter = searchApdater
        }else if(isAppendToList){
            searchApdater?.addAll(profiles as MutableList<Profile>)
        } else {
            searchApdater?.updateList(profiles)
        }
    }

    private fun onAdapterCLickListener(view: View, profile: Profile, selectedPosition: Int) {
        searchViewModel.profileSelectedPosition = selectedPosition
        when (view.id) {
            R.id.profileImage -> Navigator.navigate(this, getString(R.string.profileKey),
                    arrayListOf(searchViewModel.getHealthProviderProfile(profile)),
                    HealthProvidersProfileActivity::class.java, PROFILE_DETAILS_REQUEST)

            R.id.viewMore -> {
                searchViewModel.setProfile(profile)
                showSheet(profile.id, profile.profileTypeId)
            }
            R.id.bookAppointment -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type), getString(R.string.docNameSlug)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this), profile.name),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.docNameSlug),
                            getString(R.string.docPicSlug), getString(R.string.profileKey)),
                            arrayOf(tag.endpointUrl, profile.name, profile.picture.url, profile.profileTypeId.toString()),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            R.id.rv_card_links, R.id.rv_card_buttons -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                            arrayOf(tag.endpointUrl),
                            BookAppointmentsActivity::class.java)
                } else if (tag.targetScreen == KEY_DEPARTMENTS) {
                    Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                            arrayOf(profile.id, getString(R.string.doctors_include), profile.name, profile.picture.url), DepartmentsActivity::class.java)

                } else if (tag.targetScreen == KEY_DOCTORS) {
                    Navigator.navigate(this, arrayOf(getString(R.string.idKey), getString(R.string.include_key), getString(R.string.docNameSlug), getString(R.string.docPicSlug)),
                            arrayOf(profile.id, getString(R.string.doctors_include), profile.name, profile.picture.url), DoctorsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.docNameSlug),
                            getString(R.string.docPicSlug), getString(R.string.profileKey)),
                            arrayOf(tag.endpointUrl, profile.name, profile.picture.url, profile.profileTypeId.toString()),
                            LinksCardsDetailsActivity::class.java)
                }

            }
            R.id.tv_clinics_value, R.id.iv_clinics -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type), getString(R.string.docNameSlug), getString(R.string.docPicSlug), getString(R.string.profileKey)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext), profile.name, profile.picture.url, profile.profileTypeId.toString()),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            R.id.tv_surgeries_value, R.id.iv_surgeries -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type), getString(R.string.docNameSlug), getString(R.string.docPicSlug), getString(R.string.profileKey)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext), profile.name, profile.picture.url, profile.profileTypeId.toString()),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            R.id.consultingImage, R.id.consultingText -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }
            }
        }
    }

    private fun onMenuItemSelected(imageTextRow: ImageTextRow) {
        mBottomSheetDialog?.dismiss()
        when (imageTextRow.text) {
            getString(R.string.share) -> Navigator.share(this,
                    "https://bokdocpatient.page.link/?link=https%3A//bokdocpatient.page.link/share_profile%3Fscreen_type=share_profile%26profile_id=" + searchViewModel.profileIdShare + "%26profile_type=" + searchViewModel.profileTypeShare
            )

            //   getString(R.string.share) -> Navigator.share(this, searchViewModel.getSharableProfileUrl())
            getString(R.string.like) -> searchViewModel.toggleLike()
            getString(R.string.favorite) -> searchViewModel.toggleFavorite()
            getString(R.string.compare) -> searchViewModel.toggleCompare()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SORT_BY_REQUEST && resultCode == Activity.RESULT_OK) {
            searchViewModel.queryMap[FilterSearchConstants.SORT_BY] = data?.getStringExtra(SortByActivity.SELECTED_PARAM)!!
            searchAfterReset()
        } else
            if (requestCode == SEARCH_REQUEST && resultCode == Activity.RESULT_OK) {
                /*
                if(searchViewModel.queryMap.containsKey(FilterSearchConstants.SORT_BY)){
                    searchViewModel.queryMap.remove(FilterSearchConstants.SORT_BY)
                }
                */
                searchViewModel.ignoredKeysNumber+=1
                searchViewModel.queryMap.put(SearchResultsRepository.IGNORED_QUERY_KEY+searchViewModel.ignoredKeysNumber,
                        data?.getStringExtra(getString(R.string.query))!!)
                searchViewModel.searchType = data.getStringExtra(getString(R.string.screenType))
                titleText.text = searchViewModel.getScreenTitle(this)
                searchAfterReset()

            } else if (requestCode == FILTER_REQUEST && resultCode == Activity.RESULT_OK) {
                if(searchViewModel.queryMap.containsKey(FilterSearchConstants.SORT_BY)){
                    searchViewModel.queryMap.remove(FilterSearchConstants.SORT_BY)
                }
                searchViewModel.queryMap[SearchResultsRepository.IGNORED_QUERY_KEY+1] =
                        data?.getStringExtra(getString(R.string.query))!!
                searchAfterReset()
            }
//            else if (requestCode == PROFILE_DETAILS_REQUEST) {
//                val data = data?.getParcelableExtra<HealthProvidersProfile>(getString(R.string.profileKey))
//                searchApdater?.updateCompare(searchViewModel.profileSelectedPosition, data!!.isCompare)
//                searchApdater?.updateLike(searchViewModel.profileSelectedPosition, data!!.isLiked)
//                searchApdater?.updateFavoriate(searchViewModel.profileSelectedPosition, data!!.isFavourite)
//            }

    }

    private fun searchAfterReset(){
        updateUIVisibility(true)
        searchViewModel.isResetAdapter = true
        currentPage = 1
        isLastPage = false
        searchViewModel.search()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.mapText, R.id.mapImage -> {
                searchApdater?.data?.let {
                    Navigator.navigate(this@SearchResultsActivity,
                            getString(R.string.profiles),
                            searchViewModel.getArrayList(it).apply {
                                add(Profile())
                            },
                            SearchMapsActivity::class.java)
                }

            }
            R.id.filterText, R.id.filterImage -> {
                Navigator.navigate(this, arrayOf(getString(R.string.screenType)), arrayOf(searchViewModel.searchType),
                        FiltersActivity::class.java, FILTER_REQUEST)
            }
            R.id.sortByText, R.id.sortByImage -> {
                Navigator.navigate(this, arrayOf(getString(R.string.screenType)), arrayOf(searchViewModel.searchType), SortByActivity::class.java,
                        SORT_BY_REQUEST)
            }
            R.id.searchText, R.id.searchImage -> {
                finish()
                //Navigator.navigate(this, arrayOf(""), arrayOf(""), SearchActivity::class.java, SEARCH_REQUEST)
            }
        }
    }

    private fun showSheet(profileId: String, profileType: Int) {
        searchViewModel.profileIdShare = profileId
        searchViewModel.profileTypeShare = profileType

        val view = layoutInflater.inflate(R.layout.sheet_profile_actions, null)
        view.rv_list.layoutManager = LinearLayoutManager(this)
        view.rv_list.adapter = ProfileActionsSheetAdapter(searchViewModel.getSocialMenuRows(this), this::onMenuItemSelected)

        mBottomSheetDialog = Dialog(this, R.style.MaterialDialogSheet)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()

    }


    override fun onDestroy() {
        super.onDestroy()
        searchViewModel.saveSelectedCompare()
    }

    override fun onBackPressed() {
        val passedIntent = Intent()
        passedIntent.putExtra(getString(R.string.defaultTab),
                SearchScreenTypes.getPosition(searchViewModel.searchType))
        setResult(Activity.RESULT_OK, passedIntent)
        finish()
    }

    companion object {
        const val SORT_BY_REQUEST = 0
        const val SEARCH_REQUEST = 1
        const val FILTER_REQUEST = 2
        const val PROFILE_DETAILS_REQUEST = 3
        private val PAGE_START = 1
    }

}
