package com.bokdoc.pateintsbook.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.os.Bundle
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import android.location.Geocoder
import com.google.android.gms.maps.model.*
import java.util.*
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.LocationEnabledChecker
import kotlinx.android.synthetic.main.activity_maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.bokdoc.pateintsbook.utils.DialogCreator
import com.bokdoc.pateintsbook.utils.Utils
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.*
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import kotlinx.android.synthetic.main.activity_attachment_media.*
import kotlinx.android.synthetic.main.activity_location_maps.*
import kotlin.collections.ArrayList
import com.bokdoc.pateintsbook.R


class MapsActivity : ParentActivity(), OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private val LOCATION_REQUEST_CODE = 101
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var latLng: LatLng? = null
    private lateinit var locationCallback: LocationCallback
    var address: String? = ""
    var city: String? = ""
    var state: String? = ""
    var country: String? = ""
    private var mapLocation: Location? = null
    private lateinit var autocompleteFragment: PlaceAutocompleteFragment
    private lateinit var autoCompleteEditText: EditText

    var mapView: View? = null


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_maps)
        enableBackButton()
        title = getString(R.string.select_address)
        btn_select_location.setOnClickListener(this)
        //setDescription(getString(R.string.map_desc))


        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    latLng = LatLng(location.latitude, location.longitude)

                    val cameraPosition = CameraPosition.Builder()
                            .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(15.0f).build()
                    mMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition))
                }
                fusedLocationClient.removeLocationUpdates(this)
            }
        }
        if (intent?.hasExtra(getString(R.string.latlng_key))!!) {
            latLng = intent?.getParcelableExtra(getString(R.string.latlng_key))
        }
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.view

        setupPlaceAutoComplete()
    }

    private fun checkPermissions() {

        val snackPermissionDenied = SnackbarOnDeniedPermissionListener.Builder.with(map_parent,
                R.string.location_permissions_message)
                .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                .build()

        val permissionListener = object : PermissionListener {
            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                getCurrentLocation()
            }

            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                DialogCreator.show(this@MapsActivity, R.string.location_permissions_message, R.string.mdtp_ok, {
                    token!!.cancelPermissionRequest()
                }, {
                    token!!.cancelPermissionRequest()
                }, R.string.cancel, {
                    token!!.cancelPermissionRequest()
                })
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {

            }

        }

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(CompositePermissionListener(permissionListener, snackPermissionDenied))
                .withErrorListener { Utils.showShortToast(this@MapsActivity, R.string.some_thing_went_wrong) }
                .check()

    }

    private fun setupPlaceAutoComplete() {
        autocompleteFragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment
        autoCompleteEditText = autocompleteFragment.view!!.findViewById<EditText>(R.id.place_autocomplete_search_input)
        autoCompleteEditText.setTextSize(10.0f)
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                latLng = place.latLng
                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(12.0f).build()
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition))
            }

            override fun onError(status: Status) {

            }
        })

    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        if (LocationEnabledChecker.isLocationEnabled(this)) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    latLng = LatLng(it.latitude, it.longitude)
                    val cameraPosition = CameraPosition.Builder()
                            .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(15.0f).build()
                    mMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition))
                    btn_select_location.isClickable = true

                } else {
                    fusedLocationClient.requestLocationUpdates(LocationRequest().apply {
                        interval = 10000
                        fastestInterval = 5000
                        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    }, locationCallback, null)
                }
            }
        } else {
            DialogCreator.show(this, R.string.gps_network_not_enabled
                    , R.string.settings) {
                Navigator.navigate(this, Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTINGS_REQUEST)
            }
        }
    }

    @SuppressLint("MissingPermission", "ResourceType")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (mMap != null) {
            val permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)

            if (permission == PackageManager.PERMISSION_GRANTED) {
                mMap.isMyLocationEnabled = true
            } else {
                requestPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        LOCATION_REQUEST_CODE)
            }
        }
        // Get the button view

        val locationButton = (mapView!!.findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(Integer.parseInt("2"))
        val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 150);
        mMap.setOnCameraIdleListener(this)
        if (latLng != null) {
            val cameraPosition = CameraPosition.Builder()
                    .target(LatLng(latLng!!.latitude, latLng!!.longitude)).zoom(15.0f).build()
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition))
        } else {
            checkPermissions()
        }
    }

    override fun onCameraIdle() {

        if (latLng == null)
            return

        Handler().postDelayed({
            val geocoder: Geocoder
            val addresses: List<Address>
            geocoder = Geocoder(this, Locale.getDefault())
            val initialLoc = mMap.cameraPosition.target ?: return@postDelayed

            if (initialLoc == null)
                return@postDelayed

            addresses = geocoder.getFromLocation(initialLoc.latitude, initialLoc.longitude, 1) // Here 1 represent max mapLocation result to returned, by documents it recommended 1 to 5

            if (addresses.isEmpty())
                return@postDelayed

            address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses[0].getLocality()
            state = addresses[0].getCountryName()
            country = addresses[0].getAdminArea()

            // progress.visibility = View.GONE
            // tv_location.text = address + " , " + city + " , " + country
            val addressParts = ArrayList<String>()
            if (address != null)
                addressParts.add(address!!)
            if (city != null)
                addressParts.add(city!!)
            if (country != null)
                addressParts.add(country!!)
            autoCompleteEditText.setText(address)
            mapLocation = Location(initialLoc.latitude.toString(), initialLoc.longitude.toString(), address.toString(),
                    state.toString(), country.toString(), city.toString())


        }, 100)
    }

    override fun onClick(view: View?) {
        super.onClick(view)
        when (view!!.id) {
            R.id.btn_select_location -> {
                if (mapLocation != null && mapLocation?.longitude?.isNotEmpty()!! &&  mapLocation?.latitude?.isNotEmpty()!!) {
                    Navigator.naviagteBack(this, getString(R.string.selected_map_location), mapLocation!!)
                }else{
                    Utils.showLongToast(this,R.string.select_your_location_first)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            LOCATION_SETTINGS_REQUEST -> getCurrentLocation()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            LOCATION_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            "Unable to show location - permission required",
                            Toast.LENGTH_LONG).show()
                } else {

                    val mapFragment = supportFragmentManager
                            .findFragmentById(R.id.map) as SupportMapFragment
                    mapFragment.getMapAsync(this)
                }
            }
        }
    }

    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode)
    }


    companion object {
        const val LOCATION_SETTINGS_REQUEST = 1
    }


}


