package com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.department.Department
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.toolbar.*

class DepartmentsActivity : ParentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        title = getString(R.string.departments)
        enableBackButton()


        val viewModel = InjectionUtil.getDepartmentsViewModel(this)
        viewModel.progressLiveData.observe(this, Observer {
            progress.visibility = it!!
        })

        viewModel.getDepartments(intent?.getStringExtra(getString(R.string.idKey))!!)

        viewModel.errorLiveData.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        viewModel.departmentsLiveData.observe(this, Observer {
            rv_list.adapter = DepartmentsAdapter(it!!, this::onDoctorsClick, this::onMediaClick, this::onBookClickListner)
        })

    }

    private fun onDoctorsClick(department: DepartmentRow) {
        Navigator.navigate(this, arrayOf(getString(R.string.department_key), getString(R.string.idKey), getString(R.string.department_id)),
                arrayOf(getString(R.string.department_key), intent?.getStringExtra(getString(R.string.idKey))!!, department.id), DoctorsActivity::class.java)
    }

    private fun onMediaClick(department: DepartmentRow) {
        Navigator.navigate(this, getString(R.string.department_key), department, AttachmentMediaActivity::class.java)
    }

    private fun onBookClickListner(department: DepartmentRow) {
        Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                arrayOf(department.bookButton!!.endpointUrl), BookAppointmentsActivity::class.java, BOOK_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }
}
