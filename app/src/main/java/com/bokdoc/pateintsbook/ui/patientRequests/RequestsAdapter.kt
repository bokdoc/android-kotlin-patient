package com.bokdoc.pateintsbook.ui.patientRequests

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.webservice.models.Actions
import com.bokdoc.pateintsbook.data.webservice.models.Actions.Companion.CANCEL_SLUG
import com.bokdoc.pateintsbook.data.webservice.models.Actions.Companion.REVIEW_SLUG
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Request
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_progress.view.*
import kotlinx.android.synthetic.main.item_request.view.*

class RequestsAdapter(val context: Context, var requests: List<Request>,
                      val clickListener: (actions: Actions) -> Unit,
                      val imageClickListener: (View, Request) -> Unit)
    : PaginationRecyclerViewAdapter<Request>(R.layout.item_request, requests as MutableList<Request>) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            IListItemType.Types.ITEM.ordinal -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = RequestViewHolder(itemView)
            }

            IListItemType.Types.PAGINATION.ordinal -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            IListItemType.Types.ITEM.ordinal -> {
                requests[position].let { request ->
                    request.doctor_data.get(request.document).let {
                        if (it != null) {
                            holder.itemView.name.text = it.name
                            holder.itemView.dateText.text = request.reservation_date_normalize
                            holder.itemView.timeText.text = request.reservation_time_normalize

                            Log.i("idRequest", request.id)

                            if (it.profileType != null && it.profileType?.get(it.document)!!.id.toInt() == Profile.DOCTOR) {
                                holder.itemView.description.text = it.mainSpeciality
                            } else {
                                holder.itemView.description.text = it.location.address
                            }

                            holder.itemView.requestType.text = request.service_label

                            // if(it.reviewsCount!=0)
                            if(it.reviewsCount!=0) {
                                holder.itemView.views.text = holder.itemView.context.getString(R.string.reviewsPlaceHolder,
                                        it.reviewsCount.toString())
                                holder.itemView.views.visibility = VISIBLE
                            }else{
                                holder.itemView.views.visibility = GONE
                            }

                            if(it.rating!=0f){
                                holder.itemView.rating.rating = it.rating
                                holder.itemView.rating.visibility = VISIBLE
                            }else{
                                holder.itemView.rating.visibility = GONE
                            }
                            Glide.with(holder.itemView.context).load(it.picture.url).into(holder.itemView.image)

                            request.actions?.let {
                                if (it.label != null)
                                    when (it.slug) {
                                        REVIEW_SLUG -> {
                                            holder.itemView.request_action.text = it.label

                                        }
                                        CANCEL_SLUG -> {
                                            holder.itemView.request_action.text = it.label
                                            holder.itemView.request_action.setBackgroundColor(ContextCompat.getColor(holder.itemView.context,R.color.colorErrorRed))
                                        }
                                    }
                            }

                            if (request.actions != null) {
                                holder.itemView.request_action.visibility = VISIBLE
                            } else {
                                holder.itemView.request_action.visibility = GONE
                            }

                            holder.itemView.request_status.text = request.status_label

                            when (request.request_status) {

                                Request.REQUEST_APPROVED_BY_HP, Request.REQUEST_STATUS_CONFIRMED_BY_PATIENT -> {
                                    holder.itemView.request_status.visibility = VISIBLE
                                    holder.itemView.status_circle.visibility = VISIBLE
                                    holder.itemView.status_circle.setBackgroundResource(R.drawable.green_circle)
                                    holder.itemView.request_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorRequestsGreenStatus))
                                }

                                Request.REQUEST_STATUS_PENDING -> {
                                    holder.itemView.request_status.visibility = VISIBLE
                                    holder.itemView.status_circle.visibility = VISIBLE
                                    holder.itemView.status_circle.setBackgroundResource(R.drawable.blue_sky_circle)
                                    holder.itemView.request_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorBlueSky))
                                }
                                Request.REQUEST_STATUS_CANCELLED_BY_HP, Request.REQUEST_STATUS_CANCELLED_BY_PATIENT -> {
                                    holder.itemView.request_status.visibility = VISIBLE
                                    holder.itemView.status_circle.visibility = VISIBLE
                                    holder.itemView.status_circle.setBackgroundResource(R.drawable.red_request_status)
                                    holder.itemView.request_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorRequestRedStatus))
                                }
                                Request.REQUEST_STATUS_GARBAGE -> {
                                    holder.itemView.request_status.visibility = VISIBLE
                                    holder.itemView.status_circle.visibility = VISIBLE
                                    holder.itemView.status_circle.setBackgroundResource(R.drawable.red_request_status)
                                    holder.itemView.request_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorRequestRedStatus))
                                }
                                Request.REQUEST_STATUS_FINISHED -> {
                                    holder.itemView.request_status.visibility = VISIBLE
                                    holder.itemView.status_circle.visibility = VISIBLE
                                    holder.itemView.status_circle.setBackgroundResource(R.drawable.green_circle)
                                    holder.itemView.request_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorRequestsGreenStatus))
                                    /*
                            holder.itemView.request_status.visibility = VISIBLE
                            holder.itemView.request_status.text = request.status_label
                            holder.itemView.request_status.setBackgroundResource(R.drawable.green_request_status)
                            */
                                }
                                else -> {
                                    holder.itemView.request_status.visibility = GONE
                                    holder.itemView.status_circle.visibility = GONE
                                }
                            }

                        }
                    }

                }
            }
            LOADING -> {
                if (retryPageLoad) {
                    holder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    holder.itemView.loadmore_progress.visibility = View.GONE

                } else {
                    holder.itemView.loadmore_errorlayout.visibility = View.GONE
                    holder.itemView.loadmore_progress.visibility = View.VISIBLE
                }

            }

        }
    }

    fun updateList(inRequests: List<Request>) {
        requests = inRequests
        data = requests as MutableList<Request>
        notifyDataSetChanged()
    }

    fun add(inRequests: List<Request>) {
        val lastPosition = itemCount
        (requests as MutableList).addAll(inRequests)
        data = requests as MutableList<Request>
        notifyItemRangeInserted(lastPosition, itemCount)
    }


    fun remove(position: Int) {
        (requests as MutableList).removeAt(position)
        data = requests as MutableList<Request>
        notifyItemRemoved(position)
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Request().apply {
            type = IListItemType.Types.PAGINATION
        })
        notifyItemInserted(itemCount - 1)
    }

    override fun getItemViewType(position: Int): Int {
        return requests[position].type.ordinal
    }

    inner class RequestViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.request_action.setOnClickListener {
                requests[adapterPosition].actions?.let {
                    clickListener.invoke(it)
                }
            }
            view.image.setOnClickListener {
                imageClickListener.invoke(view.image, requests[adapterPosition])
            }
        }
    }


}

