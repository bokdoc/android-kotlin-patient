package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.net.Uri
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.patientProfile.ProfilesInfo

class PatientProfileViewModel(val patientProfileRepository: PatientProfileRepository):ViewModel() {

    val listLiveDataResource = MutableLiveData<DataResource<ProfilesInfo>>()
    val mediaLiveData = MutableLiveData<DataResource<MediaParcelable>>()
    var allMedia:List<MediaParcelable>?=null
    val remainingMediaLiveData= MutableLiveData<String>()
    var maxMediaListNumber = 5
    var requestType = ""

    fun getMedia() {
        patientProfileRepository.getMedia().observeForever {dataResource->
            (dataResource?.status == DataResource.SUCCESS).let { isSuccess ->
                if (isSuccess) dataResource?.data?.let { media ->
                   if(media.isNotEmpty()){
                       allMedia = media
                       val mediaSize = media.size
                       if(mediaSize>5){
                           remainingMediaLiveData.value = "+"+(mediaSize - 5).toString()
                           dataResource.data = (media as MutableList).subList(0,5)
                       }
                   }
                }
            }
            mediaLiveData.value= dataResource
        }
    }

    fun getFavorites():LiveData<DataResource<HealthProvidersProfile>>{
       return patientProfileRepository.getMyFavorites()
    }

    fun getCompares():LiveData<DataResource<HealthProvidersProfile>>{
       return patientProfileRepository.getCompares()
    }

    fun getRequests():LiveData<DataResource<HealthProvidersProfile>>{
        return patientProfileRepository.getRequests()
    }

    fun getMediaUris(): ArrayList<Uri> {
        val uris = ArrayList<Uri>()
        mediaLiveData.value?.data?.forEach {
            uris.add(Uri.parse(it.url))
        }
        return uris
    }


    fun getId(): String {
        return patientProfileRepository.getId()
    }


}