package com.bokdoc.pateintsbook.ui.homeMenuList

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.VideoActivity
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.changeCurrencyLanguages.ChangeCurrencyLanguageActivity
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.contactUs.ContactUsActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.ABOUTUS_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.CONTECTUS_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.FAQS_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.LOGOUT_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.PRIVACY_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.SETTING_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.SHARE_ID
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListViewModel.Companion.twilio
import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioLoadingActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import kotlinx.android.synthetic.main.fragment_list_more.*
import kotlinx.android.synthetic.main.toolbar.*

class MoreFragment : Fragment(), View.OnClickListener {

    private lateinit var homeMenuListViewModel: HomeMenuListViewModel
    private lateinit var userSharedPreference: UserSharedPreference
    private lateinit var sharedPreferencesManagerImpl: SharedPreferencesManagerImpl
    private var lang: String = ""

    override fun onClick(p0: View?) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleText.text = getString(R.string.more)
        getViewModel()
        rv_list.adapter = HomeAdapter(homeMenuListViewModel.moreData(), this::itemClickListner)
        userSharedPreference = UserSharedPreference(activity!!.applicationContext)
        sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(activity!!.applicationContext)
        lang = sharedPreferencesManagerImpl.getData(activity!!.getString(R.string.language_key), "en")
    }

    private fun getViewModel() {
        homeMenuListViewModel = ViewModelsCustomProvider(HomeMenuListViewModel(activity!!,
                SharedPreferencesManagerImpl(activity!!),
                SplashRepository(activity!!.applicationContext,
                        UserSharedPreference(activity!!.applicationContext),
                        AddFcmWebService(WebserviceManagerImpl.instance())),
                TokenRepository(activity!!.applicationContext))).create(HomeMenuListViewModel::class.java)
    }


    override fun onResume() {
        super.onResume()
        if (titleText != null) {
            titleText.text = getString(R.string.more)
            rv_list.adapter = HomeAdapter(homeMenuListViewModel.moreData(), this::itemClickListner)
        }
    }

    private fun itemClickListner(id: Int) {

        when (id) {
            SETTING_ID -> {
                Navigator.navigate(activity!!, ChangeCurrencyLanguageActivity::class.java)

            }
            CONTECTUS_ID -> {
                Navigator.navigate(activity!!, ContactUsActivity::class.java)
            }
            ABOUTUS_ID -> {
                Navigator.navigate(activity!!, arrayOf(getString(R.string.title_key),
                        getString(R.string.url_key)), arrayOf(getString(R.string.aboutAs),
                        getString(R.string.about_us_url)), WebViewActivity::class.java)
            }
            FAQS_ID -> {
                Navigator.navigate(activity!!, arrayOf(getString(R.string.title_key),
                        getString(R.string.url_key)), arrayOf(getString(R.string.faq),
                        getString(R.string.faq_url)), WebViewActivity::class.java)

            }
            PRIVACY_ID -> {

                Navigator.navigate(activity!!, arrayOf(getString(R.string.title_key),
                        getString(R.string.url_key)), arrayOf(getString(R.string.privacy),
                        getString(R.string.privacy_url)), WebViewActivity::class.java)
            }
            SHARE_ID -> {
                Utils.shareText(activity!!, Utils.grtPlayStoreReferrerlAppUrl(activity!!, ""))

            }
            LOGOUT_ID -> {
                view_progress.visibility = VISIBLE
                homeMenuListViewModel.logout().observe(this, Observer {
                    view_progress.visibility = GONE
                    if (it?.status == DataResource.SUCCESS) {
                        if(activity!=null) {
                            LocaleManager.setLocale(activity!!.baseContext)
                            Navigator.navigate(activity!!, MainActivity::class.java)
                            activity!!.finishAffinity()
                        }
                    }
                    it?.message?.let {
                        if (it.isNotEmpty())
                            Utils.showLongToast(activity!!.applicationContext, it)
                    }
                })
            }
            twilio -> {
                Navigator.navigate(activity!!, VideoActivity::class.java)

            }

        }
    }


    companion object {

        private const val ENGLISH = "en"
        private const val ARABIC = "ar"

        @JvmStatic
        fun newInstance() =
                MoreFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }

}