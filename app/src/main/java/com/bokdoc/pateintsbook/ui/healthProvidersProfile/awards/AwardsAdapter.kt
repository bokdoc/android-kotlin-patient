package com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.awards.Awards
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.awards_row.view.*

class AwardsAdapter(val awards: List<Awards>) : RecyclerView.Adapter<AwardsAdapter.AwardsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AwardsViewHolder {
        return AwardsViewHolder(parent.inflateView(R.layout.awards_row))
    }

    override fun getItemCount(): Int {
        return awards.size
    }

    override fun onBindViewHolder(holder: AwardsViewHolder, position: Int) {
        awards[position].let {
            Glide.with(holder.itemView.context).load(it.image).into(holder.itemView.image)
            holder.itemView.name.text = it.title
            holder.itemView.summary.text = it.content
        }
    }


    class AwardsViewHolder(view: View) : RecyclerView.ViewHolder(view)
}