package com.bokdoc.pateintsbook.ui.searchResults

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompletesViewModel

class SearchModelProvider(private val context: Context,private val profileSocialActionsRepository: ProfileSocialActionsRepository): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchResultsViewModel(profileSocialActionsRepository,context) as T
    }
}