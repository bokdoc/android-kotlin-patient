package com.bokdoc.pateintsbook.ui.search

import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.repositories.search.SearchRepository
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes

open class SearchViewModel(val context: Context):ViewModel() {

    private val searchRepository = SearchRepository(context)
    var completeEditType:Int  = 0

    var screenType:String
        set(value){
            searchRepository.screenType = value
        }
        get() = searchRepository.screenType


    var speciality: String
        set(value) {
            searchRepository.specialities = value
        }
        get()=searchRepository.specialities

    var specialityParams: String
        set(value) {
            searchRepository.specialityParams = value
        }
        get()=searchRepository.specialityParams

    var regionCountry:String
        set(value) {
            searchRepository.regionCountry = value
        }
       get() = searchRepository.regionCountry

    var regionCountryParams: String
        set(value) {
            searchRepository.regionCountryParams = value
        }
        get()=searchRepository.regionCountryParams

    var insuranceCarrier:String
        set(value) {
            searchRepository.insuranceCarrier = value
        }
        get() = searchRepository.insuranceCarrier

    var insuranceCarrierParams: String
        set(value) {
            searchRepository.insuranceCarrierParams = value
        }
        get()=searchRepository.insuranceCarrierParams


    fun saveDataState(){
        searchRepository.save()
    }

    fun getData(){
        searchRepository.get()
    }

    fun getQuery():String{
        return searchRepository.getQuery()
    }

    fun getWebserviceScreenType():Int{
        return if(screenType == SearchScreenTypes.APPOINTMENTS){
            R.string.appointmentType
        }else
            if(screenType == SearchScreenTypes.SURGERIES){
                R.string.surgeryType
            }else{
                R.string.consultingType
            }
    }

    fun reload(){
        searchRepository.reload()
    }

    fun getScreenTypePosition():Int{
        return SearchScreenTypes.getPosition(screenType)
    }
}