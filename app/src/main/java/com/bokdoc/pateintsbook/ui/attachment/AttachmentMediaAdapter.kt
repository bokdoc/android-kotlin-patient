package com.bokdoc.pateintsbook.ui.attachment

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bokdoc.pateintsbook.utils.files.FilesUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_file.view.*
import kotlinx.android.synthetic.main.media.view.*

class AttachmentMediaAdapter(private var uris: ArrayList<Uri>,
                             private val onAdapterClickListener: (uri: ArrayList<Uri>, Int, position: Int) -> Unit, private val isShowOnly: Boolean)
    : RecyclerView.Adapter<AttachmentMediaAdapter.AttachmentGalleryViewHolder>() {

    var viewType = MEDIA_VIEW_TYPE

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttachmentGalleryViewHolder {
        if (viewType == MEDIA_VIEW_TYPE) {
            return AttachmentGalleryViewHolder(parent.inflateView(R.layout.media))
        } else {
            return AttachmentGalleryViewHolder(parent.inflateView(R.layout.item_file))
        }
    }

    override fun getItemCount(): Int {
        return uris.size
    }

    override fun onBindViewHolder(holder: AttachmentGalleryViewHolder, position: Int) {
        if (viewType == MEDIA_VIEW_TYPE) {
            if (isShowOnly) {
                holder.view.delete.visibility = GONE
            }
            Glide.with(holder.view.context).load(uris[position]).into(holder.view.mediaImage)
        } else {
            if (isShowOnly) {
                holder.view.deleteFile.visibility = GONE
            }
            holder.view.fileName.text = FilesUtils.getFileName(uris[position])
        }

    }

    fun addUris(inputUris: List<Uri>) {
        var oldSize = itemCount
        if (oldSize == 0)
            oldSize = 1
        (uris as ArrayList).addAll(inputUris)
        notifyItemRangeInserted(oldSize, itemCount)
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    fun removeUri(position: Int) {
        uris.removeAt(position)
        notifyItemRemoved(position)
    }

    companion object {
        private const val SHOW_DETAILS_ACTION = 0
        private const val DELETE_ACTION = 1
        const val MEDIA_VIEW_TYPE = 0
        const val FILES_VIEW_TYPE = 1
    }

    /*I create two functions to remove dependencies between any component in system
     and constants in adapter*/
    fun isShowImageFullAction(action: Int): Boolean {
        return action == SHOW_DETAILS_ACTION
    }

    fun isDeleteImageAction(action: Int): Boolean {
        return action == DELETE_ACTION
    }

    fun update(uris: ArrayList<Uri>) {
        this.uris = uris
        notifyDataSetChanged()
    }


    inner class AttachmentGalleryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            if (viewType == MEDIA_VIEW_TYPE) {
                view.delete.visibility = VISIBLE
                view.setOnClickListener {
                    onAdapterClickListener.invoke(uris, SHOW_DETAILS_ACTION, adapterPosition)
                }
                view.delete.setOnClickListener {
                    onAdapterClickListener.invoke(uris, DELETE_ACTION, adapterPosition)
                }
            } else {
                view.deleteFile.setOnClickListener {
                    onAdapterClickListener.invoke(uris, DELETE_ACTION, adapterPosition)
                }
            }
        }
    }


}