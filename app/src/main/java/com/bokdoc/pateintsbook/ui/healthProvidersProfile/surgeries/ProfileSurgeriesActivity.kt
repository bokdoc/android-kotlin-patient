package com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.INTERNAL_SERVER_ERROR
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.ClinicsActivity
import com.bokdoc.pateintsbook.ui.maps.MapShowActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_list_with_progress.*
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.Exception

class ProfileSurgeriesActivity : ParentActivity() {

    lateinit var surgeriesViewModel: SurgeriesViewModel
    lateinit var profileType: String
    private var isLoading: Boolean = false
    private var isLastPage = false
    var surgeriesAdapter: SurgeriesAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        enableBackButton()
        getViewModel()

        getDataFromIntent()
        getSurgeries()
    }

    private fun getDataFromIntent() {
        surgeriesViewModel.id = intent?.getStringExtra(getString(R.string.idKey))!!
        profileType = intent?.getStringExtra(getString(R.string.typeKey))!!

        when (profileType) {
            Profile.DOCTOR.toString() -> {
                title = getString(R.string.surgeries_list_doc_place_holder, Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!))
            }
        }

        try {
            if (Profile.isHospitalCategory(profileType.toInt())) {
                title = getString(R.string.surgeries_list_hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!)
            }
        } catch (e: Exception) {

        }
        showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)
    }

    private fun getSurgeries() {
        progress.visibility = View.VISIBLE
        surgeriesViewModel.getSurgeries().observe(this, Observer {
            when (it?.status) {
                SUCCESS -> {
                    when (it.data.isNullOrEmpty()) {
                        true -> {
                            showEmpty(getString(R.string.no_surgeries), "", null)
                            progress.visibility = View.GONE
                        }
                        false -> {
                            surgeriesAdapter = SurgeriesAdapter((it.data as ArrayList<SurgeriesRow>?)!!, this::itemCLickListener, this::onSurgeryClickListner, {
                                Navigator.navigate(this, getString(R.string.locatioKey), it, MapShowActivity::class.java)
                            }, {
                                Navigator.navigate(this, arrayOf(getString(R.string.url_key)), arrayOf(it.endpointUrl),
                                        BookAppointmentsActivity::class.java, BOOK_REQUEST)
                            })
                            surgeriesViewModel.meta.pagination = it.meta.pagination

                            rv_list.adapter = surgeriesAdapter
                            progress.visibility = View.GONE
                        }
                    }
                }
                INTERNAL_SERVER_ERROR -> {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                    progress.visibility = View.GONE
                }
            }
        })

        setUpPagination()

    }

    private fun setUpPagination() {
        rv_list.addOnScrollListener(object : PaginationScrollListener(rv_list.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                surgeriesViewModel.currentPage = surgeriesViewModel.currentPage.plus(1)
                if (surgeriesViewModel.currentPage <= surgeriesViewModel.meta.pagination.totalPages!!) {
                    isLoading = true
                    surgeriesAdapter!!.addLoadingFooter()
                    surgeriesViewModel.getSurgeries().observe(this@ProfileSurgeriesActivity, Observer {
                        when (it!!.status) {
                            SUCCESS -> {
                                if (surgeriesAdapter == null) {
                                    surgeriesAdapter = SurgeriesAdapter((it.data as ArrayList<SurgeriesRow>?)!!, this@ProfileSurgeriesActivity::itemCLickListener, this@ProfileSurgeriesActivity::onSurgeryClickListner, {
                                        Navigator.navigate(this@ProfileSurgeriesActivity, getString(R.string.locatioKey), it, MapShowActivity::class.java)
                                    }, {
                                        Navigator.navigate(this@ProfileSurgeriesActivity, arrayOf(getString(R.string.url_key)), arrayOf(it.endpointUrl),
                                                BookAppointmentsActivity::class.java, BOOK_REQUEST)
                                    })
                                    rv_list.adapter = surgeriesAdapter
                                    surgeriesViewModel.meta.pagination = it.meta.pagination
                                } else {
                                    if (isLoading) {
                                        surgeriesAdapter!!.removeLoadingFooter()
                                        isLoading = false
                                        if (it.data != null)
                                            surgeriesAdapter!!.addAll(it.data as ArrayList<SurgeriesRow>)
                                    } else {
                                        if (it.data != null)
                                            surgeriesAdapter!!.updateList(it!!.data as ArrayList<SurgeriesRow>)
                                    }

                                }
                            }
                            INTERNAL_SERVER_ERROR -> {
                                Utils.showShortToast(this@ProfileSurgeriesActivity, getString(R.string.some_thing_went_wrong))
                                progress.visibility = View.GONE
                            }
                        }

                    })
                } else
                    isLastPage = true
            }


        })

    }

    fun itemCLickListener(url: String, nv: NextAvailabilitiesParcelable, position: Int) {
        if (nv.book_button!!.endpointUrl.isEmpty()) {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key)), arrayOf(url), getString(R.string.nextAvailabilitiesKey),
                    arrayListOf(nv), BookAppointmentsActivity::class.java, BOOK_REQUEST)
        } else {
            Navigator.navigate(this, arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)), arrayOf(nv.book_button!!.endpointUrl, url)
                    , NextAvailabilityActivity::class.java, BOOK_REQUEST)
        }
    }

    fun onSurgeryClickListner(surgery: SurgeriesRow) {
        val intent = Intent(this, SurgeryDetailsActivity::class.java)
        intent.putExtra(getString(R.string.surgery_details), surgery)
        intent.putExtra(getString(R.string.typeKey), profileType)
        startActivityForResult(intent, BOOK_REQUEST)
    }

    private fun getViewModel() {
        surgeriesViewModel = InjectionUtil.getSurgeriesViewModel(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BOOK_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val BOOK_REQUEST = 1
    }
}