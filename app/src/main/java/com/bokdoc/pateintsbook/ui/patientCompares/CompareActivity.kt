package com.bokdoc.pateintsbook.ui.patientCompares

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchModelProvider
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsViewModel
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.ui.CustomListPopupMenu
import kotlinx.android.synthetic.main.activity_compare.*

class CompareActivity : ParentActivity() {

    lateinit var compareViewModel: CompareViewModel
    lateinit var searchViewModel: SearchResultsViewModel
    lateinit var compareAdapter: CompareAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compare)
        title = getString(R.string.my_compares)
        // setRightText(getString(R.string.clear))
        enableBackButton()

        getViewModel()

        when (hasInternetConnection()) {
            false -> showError(getString(R.string.no_connection), lay_container)
            true -> getData()
        }
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
    }

    private fun getData() {

        compareViewModel.getCompares()

        compareViewModel.errorMessageLiveData.observe(this, Observer {
            showError(getString(R.string.some_thing_went_wrong), lay_container)
        })

        compareViewModel.compareLiveData.observe(this, Observer {
            when (it!!.size) {
                0 -> showError(getString(R.string.no_compares), lay_container)
                else -> {
                    compareAdapter = CompareAdapter(it, this::onAdapterCLickListener)
                    rv_patient_compares.adapter = CompareAdapter(it, this::onAdapterCLickListener)

                }
            }
        })

        compareViewModel.progressLiveData.observe(this, Observer
        {
            when (it) {
                false -> progress.visibility = View.GONE
                true -> progress.visibility = View.VISIBLE
            }
        })

        searchViewModel = ViewModelProviders.of(this, SearchModelProvider(this,
                ProfileSocialActionsRepository(applicationContext, SharedPreferencesManagerImpl(this), CompareSharedPreferences(this), ProfileSocailActionsWebservice(
                        WebserviceManagerImpl.instance()))))
                .get(SearchResultsViewModel::
                class.java)

        // social actions listener
        searchViewModel.likedToggleLiveDate.observe(this, Observer
        {
            //     compareAdapter?.toggleLike(searchViewModel.profileSelectedPosition)
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.favouritesToggleLiveDate.observe(this, Observer
        {
            //     compareAdapter?.toggleFavoriate(searchViewModel.profileSelectedPosition)
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.comparedToggleLiveData.observe(this, Observer
        {
            //    compareAdapter?.toggleCompare(searchViewModel.profileSelectedPosition)
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
        searchViewModel.errorMessageLiveDate.observe(this, Observer
        {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })
    }

    private fun onAdapterCLickListener(view: View, profile: Profile, selectedPosition: Int) {

        searchViewModel.profileSelectedPosition = selectedPosition
        when (view.id) {
            R.id.profileImage -> Navigator.navigate(this, getString(R.string.profileKey),
                    arrayListOf(searchViewModel.getHealthProviderProfile(profile)),
                    HealthProvidersProfileActivity::class.java, SearchResultsActivity.PROFILE_DETAILS_REQUEST)

            R.id.viewMore -> {
                searchViewModel.setProfile(profile)
                val popupMenu = CustomListPopupMenu(this, view, searchViewModel.getSocialMenuRows(this)
                        as ArrayList<ImageTextRow>)
                popupMenu.selectedListener = this::onMenuItemSelected
                popupMenu.show()
            }
            R.id.bookAppointment -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }
                /*
                Navigator.navigate(this, view.context?.getString(R.string.profileKey)!!,
                        arrayListOf(searchViewModel.getHealthProviderBookInfo(profile)),
                        arrayOf(getString(R.string.isSurgriesBookKey), getString(R.string.bookTypeKey)),
                        arrayOf(searchViewModel.searchType == SearchScreenTypes.SURGERIES, getString(R.string.appointementBookKey)),
                        BookAppointmentsActivity::class.java)
                        */
            }
            R.id.tv_clinics_value, R.id.iv_clinics -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            R.id.tv_surgeries_value, R.id.iv_surgeries -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            R.id.consultingImage, R.id.consultingText -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }
            }
            /*
            R.id.homeVisitsImage, R.id.homeVisitsText -> {
                val tag = view.tag as DynamicLinks
                if (tag.isTargetBookAppointments) {
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                            getString(R.string.service_type)), arrayOf(tag.endpointUrl,
                            searchViewModel.getServiceType(this)),
                            BookAppointmentsActivity::class.java)
                } else {
                    Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                            getString(R.string.service_type)),
                            arrayOf(getString(R.string.clinics), tag.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                            LinksCardsDetailsActivity::class.java)
                }

            }

            R.id.freeText, R.id.freeImage -> {
                profile.freeButtons!![0].let {
                    if (it.isTargetBookAppointments) {
                        Navigator.navigate(this, arrayOf(getString(R.string.url_key),
                                getString(R.string.service_type)), arrayOf(it.endpointUrl,
                                searchViewModel.getServiceType(this)),
                                BookAppointmentsActivity::class.java)
                    } else {
                        Navigator.navigate(this, arrayOf(getString(R.string.title_key), getString(R.string.url_key),
                                getString(R.string.service_type)),
                                arrayOf(getString(R.string.clinics), it.endpointUrl, searchViewModel.getServiceType(applicationContext)),
                                LinksCardsDetailsActivity::class.java)
                    }

                }
            }
            */
        }
    }

    private fun onMenuItemSelected(imageTextRow: ImageTextRow) {
        when (imageTextRow.text) {
            getString(R.string.share) -> Navigator.share(this, searchViewModel.getSharableProfileUrl())
            getString(R.string.like) -> searchViewModel.toggleLike()
            getString(R.string.favorite) -> searchViewModel.toggleFavorite()
            getString(R.string.compare) -> {
                searchViewModel.toggleCompare()
                compareAdapter.data.removeAt(searchViewModel.profileSelectedPosition)
                compareAdapter.notifyItemRemoved(searchViewModel.profileSelectedPosition)
                if (compareAdapter.itemCount == 0) {
                    showError(getString(R.string.no_compares), lay_container)
                }
            }
        }
    }

    private fun getViewModel() {
        compareViewModel = InjectionUtil.getComparesViewModel(this)
    }
}