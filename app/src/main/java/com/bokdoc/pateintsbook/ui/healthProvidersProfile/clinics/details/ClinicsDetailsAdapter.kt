package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.details

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.ClinicsSections
import com.bokdoc.pateintsbook.data.models.healthProviders.clinics.IClinicsSections
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import kotlinx.android.synthetic.main.clinics_children.view.*
import kotlinx.android.synthetic.main.section_header.view.*

class ClinicsDetailsAdapter(context: Context, var sectionItemList: List<ClinicsSections>, var selectedListener:
        (section: String)->Unit) :
SectionRecyclerViewAdapter<ClinicsSections, String,
        ClinicsDetailsAdapter.SectionViewHolder, ClinicsDetailsAdapter.ChildViewHolder>(context,sectionItemList) {

    private var lastSelectedPosition = -1

    override fun onCreateSectionViewHolder(viewGroup: ViewGroup?, p1: Int): SectionViewHolder {
        return SectionViewHolder(viewGroup?.inflateView(R.layout.surgeries_header)!!)
    }

    override fun onBindSectionViewHolder(sectionViewHolder: SectionViewHolder?, position: Int, accommodationSections: ClinicsSections?) {
        if(accommodationSections?.title!!.isNotEmpty()) {
            sectionViewHolder?.view?.header?.visibility = VISIBLE
            sectionViewHolder?.view?.header?.text = accommodationSections.title
        }else{
            sectionViewHolder?.view?.header?.visibility = GONE
        }
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?, position: Int): ChildViewHolder {
        return ChildViewHolder(viewGroup?.inflateView(R.layout.surgeries_children)!!)
    }

    override fun onBindChildViewHolder(viewHolder: ChildViewHolder?, sectionPosition: Int, childPosition: Int, resource: String?) {

        if(sectionItemList[sectionPosition].sectionType == IClinicsSections.TEXT_SECTION){
            viewHolder?.view?.text?.visibility = View.VISIBLE
            viewHolder?.view?.text?.text = resource
            viewHolder?.view?.locationImage?.visibility = View.GONE
            viewHolder?.view?.arrow?.visibility = View.GONE
            viewHolder?.view?.button?.visibility = View.GONE
        }else
            if(sectionItemList[sectionPosition].sectionType == IClinicsSections.FEES_SECTION){
                viewHolder?.view?.text?.visibility = View.VISIBLE
                val texts = resource?.split(StringsConstants.SPACE)
                viewHolder?.view?.text?.text = ColoredTextSpannable.getSpannable(resource!!, texts!![1],
                        ContextCompat.getColor(viewHolder?.view!!.context,R.color.colorFees))
                viewHolder?.view?.locationImage?.visibility = View.GONE
                viewHolder?.view?.arrow?.visibility = View.GONE
                viewHolder?.view?.button?.visibility = View.GONE
            }else
                if(sectionItemList[sectionPosition].sectionType == IClinicsSections.APPOINTEMENT_SECTION){
                    viewHolder?.view?.text?.visibility = View.VISIBLE
                    viewHolder?.view?.text?.text = ColoredTextSpannable.getSpannable(resource!!,
                            ContextCompat.getColor(viewHolder?.view!!.context,R.color.colorFees))
                    viewHolder?.view?.locationImage?.visibility = View.GONE
                    viewHolder?.view?.arrow?.visibility = View.GONE
                    viewHolder?.view?.button?.visibility = View.GONE
                }
            else
            if(sectionItemList[sectionPosition].sectionType == IClinicsSections.LOCATION_SECTION){
                viewHolder?.view?.text?.visibility = View.VISIBLE
                viewHolder?.view?.text?.text = resource
                viewHolder?.view?.locationImage?.visibility = View.VISIBLE
                viewHolder?.view?.arrow?.visibility = View.GONE
                viewHolder?.view?.button?.visibility = View.GONE
            }
            else
                if(sectionItemList[sectionPosition].sectionType == IClinicsSections.PHOTOS_VIDEOS_SECTION){
                    viewHolder?.view?.setOnClickListener {
                        selectedListener.invoke(IClinicsSections.PHOTOS_VIDEOS_SECTION)
                    }
                    viewHolder?.view?.text?.visibility = View.VISIBLE
                    viewHolder?.view?.text?.text = resource
                    viewHolder?.view?.locationImage?.visibility = View.GONE
                    viewHolder?.view?.arrow?.visibility = View.VISIBLE
                    viewHolder?.view?.button?.visibility = View.GONE
                }
                else
                    if(sectionItemList[sectionPosition].sectionType == IClinicsSections.NEXT_AVAILABILITY_SECTION){
                        viewHolder?.view?.button?.setOnClickListener {
                            selectedListener.invoke(IClinicsSections.NEXT_AVAILABILITY_SECTION)
                            lastSelectedPosition = sectionPosition
                        }
                        viewHolder?.view?.locationImage?.visibility = View.GONE
                        viewHolder?.view?.arrow?.visibility = View.GONE
                        viewHolder?.view?.text?.visibility = View.GONE
                        viewHolder?.view?.button?.visibility = View.VISIBLE
                        viewHolder?.view?.button?.text = resource
                    }
        if(sectionItemList[sectionPosition].sectionType == IClinicsSections.NEXT_AVAILABILITY_SECTION){
            viewHolder?.view?.setBackgroundColor(ContextCompat.getColor(viewHolder.view.context!!,R.color.colorTransparent))
        }else{
            viewHolder?.view?.setBackgroundColor(ContextCompat.getColor(viewHolder.view.context!!,R.color.colorWhite))
        }
    }

    fun updateNextAvaliabilityText(data: String){
        sectionItemList[lastSelectedPosition].children = arrayListOf(data)
        notifyDataChanged(sectionItemList)
    }

    class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view)

   inner class ChildViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}