package com.bokdoc.pateintsbook.ui.patientProfile

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.SparseArray
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistoryImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.navigators.LookupsNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.changeCurrencyLanguages.ChangeCurrencyLanguageActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListFragment
import com.bokdoc.pateintsbook.ui.imagesViewer.ImagesViewerActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryListAdapter
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryPatientsViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.disease.DiseaseActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.disease.DiseaseViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.medications.MedicationsActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.medications.MedicationsViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.speciality.SpecialitiesViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.speciality.SpecialityActivity
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.updateProfile.UpdateProfileActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_patient_profile.*
import kotlinx.android.synthetic.main.include_reviews_section.view.*
import kotlinx.android.synthetic.main.included_patient_medicals_history.view.*
import kotlinx.android.synthetic.main.medical_history_view.*
import kotlinx.android.synthetic.main.patient_profile_bottom_sheet.view.*
import kotlinx.android.synthetic.main.toolbar.*

class MainPatientProfileFragment : Fragment(), View.OnClickListener {

    private lateinit var patientProfileViewModel: PatientProfileViewModel
    private lateinit var specialitiesViewModel: SpecialitiesViewModel
    private lateinit var medicationsViewModel: MedicationsViewModel
    private lateinit var diseaseViewModel: DiseaseViewModel
    private var fragments = ArrayList<PatientProfileFragment>()
    private var bottomSheetDialog: Dialog? = null
    lateinit var userSharedPreference: UserSharedPreference
    var registeredFragments =  SparseArray<Fragment>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_patient_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userSharedPreference = UserSharedPreference(activity!!)
        titleText.text = getString(R.string.profile)
        getViewModel()
        initializeViews()
        setupTabs()
        //moreIcon.visibility = View.VISIBLE
        moreText.setOnClickListener(this)
        //moreIcon.setOnClickListener(this)
        editProfileImage.setOnClickListener(this)
        mediaList.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        //lay_edit.setOnClickListener(this)
        setupMediaObservers()
        mediaProgress.visibility = View.VISIBLE
        patientProfileViewModel.getMedia()
        setupMedicalHistoryObservers()
        setupMedicalsHistory()
        setupMedicalClickListeners()
        //setupSwipeToDismiss()
    }

    private fun setupMedicalsHistory() {
        chronic_diseases_included.medicals_history_text.text = getString(R.string.my_diseases)
        Glide.with(this).load(R.drawable.ic_diseases).into(chronic_diseases_included.medicals_history_icon)
        //hronic_diseases_included.progress.visibility = VISIBLE
        userSharedPreference.getUser()?.let {
            diseaseViewModel.get(it.id)
        }
        chronic_diseases_included.tv_not_found.text = getString(R.string.chronic_diseases_profile_empty_text)

        specialities_included.medicals_history_text.text = getString(R.string.my_specialities)
        Glide.with(this).load(R.drawable.ic_specialities).into(specialities_included.medicals_history_icon)
        specialities_included.progress.visibility = VISIBLE
        userSharedPreference.getUser()?.let {
            specialitiesViewModel.get(it.id)
        }
        specialities_included.tv_not_found.text = getString(R.string.specialities_profile_empty_text)

        medications_included.medicals_history_text.text = getString(R.string.my_medications)
        Glide.with(this).load(R.drawable.ic_patients_medications).into(medications_included.medicals_history_icon)
        medications_included.progress.visibility = VISIBLE
        userSharedPreference.getUser()?.let {
            medicationsViewModel.get(it.id)
        }
        medications_included.tv_not_found.text = getString(R.string.medications_profile_empty_text)

    }


    private fun setupMedicalHistoryObservers() {
        specialitiesViewModel.progressLiveData.observe(this, Observer {
            specialities_included.progress.visibility = it!!
        })

        specialitiesViewModel.messageLiveData.observe(this, Observer {
            Toast.makeText(activity!!, it!!, Toast.LENGTH_LONG).show()
        })

        specialitiesViewModel.listLiveData.observe(this, Observer {
            if (it!!.isNotEmpty()) {
                val subList = ArrayList<MedicalHistory>().apply {
                    if (it.size <= 2) {
                        addAll(it)
                    } else {
                        add(it[0])
                        add(it[1])
                    }
                }
                specialities_included.medicals_history_list.adapter = MedicalHistoryListAdapter(subList, this::onSpecialitesCLickListener)
                specialities_included.view_all.visibility = VISIBLE
            } else {
                // emptyText.visibility = VISIBLE
                specialities_included.medicals_history_list.adapter = MedicalHistoryListAdapter(arrayListOf(), this::onSpecialitesCLickListener)
                specialities_included.tv_not_found.visibility = VISIBLE
            }
        })

        specialitiesViewModel.addedLiveData.observe(this, Observer {
            if (!it!!) {
//                (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(
//                        specialitiesViewModel.addedOrRemovedPosition)

//                if (specialities_included.medicals_history_list.adapter?.itemCount == 0) {
//                    switchMedicalHistoryContentVisibility(false, specialities_included)
//                }


            }
        })

        specialitiesViewModel.deletedLiveData.observe(this, Observer {
            if (!it!! && specialitiesViewModel.addedOrRemovedPosition < 2) {
                (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).addAt(
                        specialitiesViewModel.addedOrRemovedPosition, specialitiesViewModel.addedOrRemovedMedicalHistory)

                switchMedicalHistoryContentVisibility(true, specialities_included)
            }
        })

        diseaseViewModel.progressLiveData.observe(this, Observer {
            chronic_diseases_included.progress.visibility = it!!
        })

        diseaseViewModel.messageLiveData.observe(this, Observer {
            Toast.makeText(activity!!, it!!, Toast.LENGTH_LONG).show()
        })

        diseaseViewModel.listLiveData.observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                val subList = ArrayList<MedicalHistory>().apply {
                    if (it.size <= 2) {
                        addAll(it)
                    } else {
                        add(it[0])
                        add(it[1])
                    }
                }
                chronic_diseases_included.medicals_history_list.adapter = MedicalHistoryListAdapter(subList, this::onDiecesseCLickListener)
                chronic_diseases_included.view_all.visibility = VISIBLE
            } else {
                chronic_diseases_included.medicals_history_list.adapter = MedicalHistoryListAdapter(arrayListOf(), this::onDiecesseCLickListener)
                // emptyText.visibility = VISIBLE
                chronic_diseases_included.tv_not_found.visibility = VISIBLE
            }
        })

        diseaseViewModel.addedLiveData.observe(this, Observer {
            if ( it!!) {
                (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).replaceAt(0, diseaseViewModel.addedOrRemovedMedicalHistory)
                switchMedicalHistoryContentVisibility(true, chronic_diseases_included)
            }
        })

        diseaseViewModel.deletedLiveData.observe(this, Observer {
            if (!it!! && diseaseViewModel.addedOrRemovedPosition < 2) {
                (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).addAt(diseaseViewModel.addedOrRemovedPosition,
                        diseaseViewModel.addedOrRemovedMedicalHistory)

                switchMedicalHistoryContentVisibility(true, chronic_diseases_included)
            }
        })

        medicationsViewModel.progressLiveData.observe(this, Observer {
            medications_included.progress.visibility = it!!
        })

        medicationsViewModel.messageLiveData.observe(this, Observer {
            Toast.makeText(activity!!, it!!, Toast.LENGTH_LONG).show()
        })

        medicationsViewModel.listLiveData.observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                val subList = ArrayList<MedicalHistory>().apply {
                    if (it.size <= 2) {
                        addAll(it)
                    } else {
                        add(it[0])
                        add(it[1])
                    }
                }
                medications_included.medicals_history_list.adapter = MedicalHistoryListAdapter(subList, this::onMedicationCLickListener)
                medications_included.view_all.visibility = VISIBLE
            } else {
//                emptyText.visibility = VISIBLE
                medications_included.tv_not_found.visibility = VISIBLE
                medications_included.medicals_history_list.adapter = MedicalHistoryListAdapter(arrayListOf(), this::onMedicationCLickListener)
            }
        })

        medicationsViewModel.addedLiveData.observe(this, Observer {
            if (it!!) {

                (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).replaceAt(0, medicationsViewModel.addedOrRemovedMedicalHistory)
                switchMedicalHistoryContentVisibility(true, medications_included)
            }
        })

        medicationsViewModel.deletedLiveData.observe(this, Observer {

            if (!it!! && medicationsViewModel.addedOrRemovedPosition < 2) {
                (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).addAt(medicationsViewModel.addedOrRemovedPosition,
                        medicationsViewModel.addedOrRemovedMedicalHistory)
                switchMedicalHistoryContentVisibility(true, medications_included)
            }
        })
    }

//    private fun setupSwipeToDismiss() {
//        val medicationsListener = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
//            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
//                return true
//            }
//
//            override fun onSwiped(holder: RecyclerView.ViewHolder, position: Int) {
//                val history = (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(holder.adapterPosition)
//                medicationsViewModel.addedOrRemovedPosition = holder.adapterPosition
//                medicationsViewModel.addedOrRemovedMedicalHistory = history
//                (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(holder.adapterPosition)
//                if (medications_included.medicals_history_list.adapter?.itemCount == 0) {
//                    switchMedicalHistoryContentVisibility(false, medications_included)
//                }
//                medicationsViewModel.delete(history.historyId)
//            }
//
//        }
//        ItemTouchHelper(medicationsListener).attachToRecyclerView(medications_included.medicals_history_list)
//
//        val specialitiesListener = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
//            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
//                return true
//            }
//
//            override fun onSwiped(holder: RecyclerView.ViewHolder, position: Int) {
//                val history = (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(holder.adapterPosition)
//                specialitiesViewModel.addedOrRemovedMedicalHistory = history
//                specialitiesViewModel.addedOrRemovedPosition = holder.adapterPosition
//                specialitiesViewModel.delete(holder.adapterPosition)
//                (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(holder.adapterPosition)
//                if (specialities_included.medicals_history_list.adapter?.itemCount == 0) {
//                    switchMedicalHistoryContentVisibility(false, specialities_included)
//                }
//            }
//
//        }
//        ItemTouchHelper(specialitiesListener).attachToRecyclerView(specialities_included.medicals_history_list)
//
//
//        val diseasesListener = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
//            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
//                return true
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
//                val history = (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(viewHolder.adapterPosition)
//                diseaseViewModel.addedOrRemovedMedicalHistory = history
//                diseaseViewModel.addedOrRemovedPosition = viewHolder.adapterPosition
//                diseaseViewModel.delete( viewHolder.adapterPosition)
//                (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(viewHolder.adapterPosition)
//                if (chronic_diseases_included.medicals_history_list.adapter?.itemCount == 0) {
//                    switchMedicalHistoryContentVisibility(false, chronic_diseases_included)
//                }
//            }
//
//        }
//        ItemTouchHelper(diseasesListener).attachToRecyclerView(chronic_diseases_included.medicals_history_list)
//
//    }

    private fun setupMedicalClickListeners() {
        medications_included.view_all.setOnClickListener {
            Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
                    arrayOf(patientProfileViewModel.getId()), MedicationsActivity::class.java, MEDICATIONS_LIST_REQUEST)
        }

//        specialities_included.view_all.setOnClickListener {
//            Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
//                    arrayOf(patientProfileViewModel.getId()), SpecialityActivity::class.java)
//        }

        chronic_diseases_included.view_all.setOnClickListener {
            Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
                    arrayOf(patientProfileViewModel.getId()), DiseaseActivity::class.java, DISEASES_LIST_REQUEST)
        }

        medications_included.add_button.setOnClickListener {
            medicationsViewModel.addedOrRemovedPosition = 0
            LookupsNavigator.navigate(this, getString(R.string.myMedications), getString(R.string.lookupMedications),
                    arrayListOf(), MEDICATIONS_LOOKUP_REQUEST)
        }

        specialities_included.add_button.setOnClickListener {
            specialitiesViewModel.addedOrRemovedPosition = 0
            LookupsNavigator.navigate(this, getString(R.string.my_specialities), getString(R.string.lookupSpecialities),
                    arrayListOf(), SPECIALITIES_LOOKUP_REQUEST)
        }

        chronic_diseases_included.add_button.setOnClickListener {
            diseaseViewModel.addedOrRemovedPosition = 0
            LookupsNavigator.navigate(this, getString(R.string.myDiseases), getString(R.string.lookupDiseases),
                    arrayListOf(), DISEASES_LOOKUP_REQUEST)
        }
    }

    private fun switchMedicalHistoryContentVisibility(isShowContent: Boolean, view: View) {
        if (isShowContent) {
            view.tv_not_found.visibility = GONE
            view.view_all.visibility = VISIBLE
        } else {
            view.tv_not_found.visibility = VISIBLE
            view.view_all.visibility = GONE
        }
    }


    private fun showBottomSheet() {
        if (bottomSheetDialog == null) {
            val view = layoutInflater.inflate(R.layout.patient_profile_bottom_sheet, null)
            view.mySpecialities.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), SpecialityActivity::class.java)
            }
            view.myDiseases.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), DiseaseActivity::class.java)
            }
            view.myMedications.setOnClickListener {
                bottomSheetDialog?.dismiss()
                Navigator.navigate(activity!!, arrayOf(getString(R.string.idKey)),
                        arrayOf(userSharedPreference.getId()), MedicationsActivity::class.java)
            }
            bottomSheetDialog = Dialog(activity!!, R.style.MaterialDialogSheet)
            bottomSheetDialog?.setContentView(view)
            bottomSheetDialog?.setContentView(view)
            bottomSheetDialog?.setCancelable(true)
            bottomSheetDialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            bottomSheetDialog?.window!!.setGravity(Gravity.BOTTOM)
        }
        bottomSheetDialog?.show()

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.moreIcon -> {
                showBottomSheet()
            }
            R.id.reviewButton -> {
                Navigator.navigate(activity!!, ReviewsActivity::class.java)
            }
            R.id.userScope -> {
                Navigator.navigate(activity!!, ChangeCurrencyLanguageActivity::class.java, PatientProfileActivity.CHANGE_SCOPE_REQUEST)
            }
            R.id.moreText -> {
                if (activity != null)
                    Navigator.navigate(activity!!, arrayOf(getString(R.string.is_show_only)),
                            arrayOf("true"), getString(R.string.media_key),
                            patientProfileViewModel.allMedia as ArrayList<out Parcelable>
                            , AttachmentMediaActivity::class.java, -1)

            }
            R.id.editProfileImage -> {
                Navigator.navigate(this, UpdateProfileActivity::class.java, UPDATE_PROFILE_REQUEST)
            }
            /*
            lay_edit.id -> {
                Navigator.navigate(this, UpdateProfileActivity::class.java)
            }
            */
        }

    }


    private fun onSpecialitesCLickListener(medicalHistory: MedicalHistory, position: Int, action: Int) {
        val history = (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(position)
        specialitiesViewModel.addedOrRemovedMedicalHistory = history
        specialitiesViewModel.addedOrRemovedPosition = position
        (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(position)
        if (specialities_included.medicals_history_list.adapter?.itemCount == 0) {
            switchMedicalHistoryContentVisibility(false, specialities_included)
        }
        specialitiesViewModel.delete(position)
    }


    private fun onDiecesseCLickListener(medicalHistory: MedicalHistory, position: Int, action: Int) {
        val history = (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(position)
        diseaseViewModel.addedOrRemovedMedicalHistory = history
        (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(position)
        if (chronic_diseases_included.medicals_history_list.adapter?.itemCount == 0) {
            switchMedicalHistoryContentVisibility(false, chronic_diseases_included)
        }
        diseaseViewModel.addedOrRemovedPosition = position
        diseaseViewModel.delete(position)
    }

    private fun onMedicationCLickListener(medicalHistory: MedicalHistory, position: Int, action: Int) {
        val history = (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).getAt(position)
        medicationsViewModel.addedOrRemovedPosition = position
        medicationsViewModel.addedOrRemovedMedicalHistory = history
        (medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).removeAt(position)
        if (medications_included.medicals_history_list.adapter?.itemCount == 0) {
            switchMedicalHistoryContentVisibility(false, medications_included)
        }
        medicationsViewModel.delete(position)
    }


    private fun setupMediaObservers() {
        patientProfileViewModel.mediaLiveData.observe(activity!!, Observer {
            mediaProgress?.visibility = View.GONE
            if (it?.status == DataResource.SUCCESS) {
                if (it.data?.isNotEmpty()!!) {
                    mediaList?.adapter = PatientMediaAdapter(it.data!!) { view, position ->
                        Navigator.navigate(activity!!, arrayOf(getString(R.string.selected_position)),
                                arrayOf(position.toString()), getString(R.string.images), patientProfileViewModel.getMediaUris(),
                                ImagesViewerActivity::class.java, -1)
                    }
                } else {
                    noMediaFound?.visibility = View.VISIBLE
                }
            } else {
                it?.message?.isNotEmpty()?.let { isEmpty ->
                    if (!isEmpty&& activity!=null)
                        Utils.showLongToast(activity!!, it.message)
                }
            }
        })
        patientProfileViewModel.remainingMediaLiveData.observe(activity!!, Observer {
            if (it != null && moreText != null)
                moreText?.text = it
        })
    }

    private fun initializeViews() {

        //signout view
        //icon.setImageResource(ic_sing_out)

        userSharedPreference.getUser()?.let {
            patientName.text = userSharedPreference.getUser()!!.name

            Glide.with(this).load(it.picture.url).into(patientImage)

            userScope.text = StringsConstants.buildString(arrayListOf(it.countryLookup.name, "/", it.languageLookup.name,
                    "/", it.currencyLookup.name))

            patientGender.text = it.gender

            if (it.age.isNotEmpty() && it.age != "0") {
                patientAge.text = it.age
            }

            // setupTabs()
        }


        reviewButton.setOnClickListener(this::onClick)
        userScope.setOnClickListener(this::onClick)

        /*
        include_signout.setOnClickListener {
            patientProfileViewModel.logout()
            Navigator.navigateBack(this)
        }
        */
    }

    private fun setupTabs() {
        var adapter = TabsPagerAdapter(activity!!.supportFragmentManager)
        infoPager.adapter = adapter
        patient_tabs.setupWithViewPager(infoPager)
        //infoPager.currentItem =0

        patient_tabs.setSelectedTabIndicatorColor(ContextCompat.getColor(activity!!, R.color.colorAccent))
        val tabLayout = LayoutInflater.from(activity!!).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout.text = getString(R.string.my_favourites)
        tabLayout.compoundDrawablePadding = 5
        tabLayout.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_grey_heart, 0, 0)
        patient_tabs.getTabAt(0)?.customView = tabLayout


        val tabLayout2 = LayoutInflater.from(activity!!).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout2.text = getString(R.string.my_requests)
        tabLayout.compoundDrawablePadding = 5
        tabLayout2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_patients_requests, 0, 0)
        //patient_tabs.getTabAt(2)?.customView = tabLayout2
        patient_tabs.getTabAt(1)?.customView = tabLayout2

        infoPager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                /*
                 val fragment =
                         registeredFragments[position]

                if(fragment is PatientFavoritesFragment){
                    patientProfileViewModel.getFavorites()
                }else if(fragment is PatientRequestsFragment){
                    patientProfileViewModel.getRequests()
                }
                   */
            }

        })
    }


    private fun getViewModel() {
        patientProfileViewModel = InjectionUtil.getPatientsViewModel(activity!!)
        specialitiesViewModel = InjectionUtil.getSpecialitiesViewModel(activity!!)
        medicationsViewModel = InjectionUtil.getMedicationsViewModel(activity!!)
        diseaseViewModel = InjectionUtil.getDiseaseViewModel(activity!!)
    }

    inner class TabsPagerAdapter(supportFragmentManage: android.support.v4.app.FragmentManager
    ) : FragmentStatePagerAdapter(supportFragmentManage) {


        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return PatientFavoritesFragment()
                1 -> return PatientRequestsFragment()
            }
            return Fragment()
        }

        override fun getCount(): Int {
            return 2
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val fragment =  super.instantiateItem(container, position) as Fragment
            //registeredFragments.put(position, fragment)
            return fragment
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            //registeredFragments.remove(position)
            super.destroyItem(container, position, `object`)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CHANGE_SCOPE_REQUEST && resultCode == Activity.RESULT_OK) {
            initializeViews()
        } else if (requestCode == UPDATE_PROFILE_REQUEST && resultCode == Activity.RESULT_OK) {
            initializeViews()
        } else if (requestCode == MEDICATIONS_LOOKUP_REQUEST && resultCode == Activity.RESULT_OK) {
            val lookup = data?.getParcelableArrayListExtra<LookUpsParcelable>(getString(R.string.selected_lookups))
            val medicalHistory = MedicalHistoryImpl().apply {
                this.historyId = lookup!![0].idLookup
                this.name = lookup[0].name
            }
           // medicationsViewModel.addedOrRemovedPosition = 0
            medicationsViewModel.addedOrRemovedMedicalHistory = medicalHistory
          //  switchMedicalHistoryContentVisibility(true, medications_included)
            //(medications_included.medicals_history_list.adapter as MedicalHistoryListAdapter).replaceAt(0, medicalHistory)
            medicationsViewModel.add(medicalHistory)

        }
        else if (requestCode == MEDICATIONS_LIST_REQUEST && resultCode == Activity.RESULT_OK) {
            val medications = data?.getParcelableArrayListExtra<MedicalHistoryImpl>(getString(R.string.medicalsHistoriesKey))
            medicationsViewModel.listLiveData.value = medications
        }

        else if (requestCode == DISEASES_LOOKUP_REQUEST && resultCode == Activity.RESULT_OK) {
            val lookup = data?.getParcelableArrayListExtra<LookUpsParcelable>(getString(R.string.selected_lookups))
            val medicalHistory = MedicalHistoryImpl().apply {
                if (lookup != null) {
                    this.historyId = lookup!![0].idLookup
                    this.name = lookup[0].name
                }

            }
           // diseaseViewModel.addedOrRemovedPosition = 0
            diseaseViewModel.addedOrRemovedMedicalHistory = medicalHistory
//            switchMedicalHistoryContentVisibility(true, chronic_diseases_included)
//            (chronic_diseases_included.medicals_history_list.adapter as MedicalHistoryListAdapter).replaceAt(0, medicalHistory)
            diseaseViewModel.add(medicalHistory)


            // emptyText.visibility = View.GONE

        }
        else if (requestCode == DISEASES_LIST_REQUEST && resultCode == Activity.RESULT_OK) {
            val desises = data?.getParcelableArrayListExtra<MedicalHistoryImpl>(getString(R.string.medicalsHistoriesKey))
            diseaseViewModel.listLiveData.value = desises
        }

        else if (requestCode == SPECIALITIES_LOOKUP_REQUEST && resultCode == Activity.RESULT_OK) {
            val lookup = data?.getParcelableArrayListExtra<LookUpsParcelable>(getString(R.string.selected_lookups))
            val medicalHistory = MedicalHistoryImpl().apply {
                this.historyId = lookup!![0].idLookup
                this.name = lookup[0].name
            }

            //medicationsViewModel.addedOrRemovedPosition = 0
            specialitiesViewModel.addedOrRemovedMedicalHistory = medicalHistory
//            switchMedicalHistoryContentVisibility(true, specialities_included)
//            (specialities_included.medicals_history_list.adapter as MedicalHistoryListAdapter).replaceAt(0, medicalHistory)
            specialitiesViewModel.add(medicalHistory)

            // emptyText.visibility = View.GONE
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        //activity?.supportFragmentManager?.findFragmentByTag()
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onResume() {
        super.onResume()
    }


    companion object {
        const val CHANGE_SCOPE_REQUEST = 1
        const val UPDATE_PROFILE_REQUEST = 2
        const val MEDICATIONS_LOOKUP_REQUEST = 3
        const val DISEASES_LOOKUP_REQUEST = 4
        const val DISEASES_LIST_REQUEST = 7
        const val MEDICATIONS_LIST_REQUEST = 8
        const val SPECIALITIES_LOOKUP_REQUEST = 6
        const val BOOK_REQUEST = 7
        @JvmStatic
        fun newInstance() =
                MainPatientProfileFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }


}