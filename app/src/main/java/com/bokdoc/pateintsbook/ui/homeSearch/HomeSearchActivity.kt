package com.bokdoc.pateintsbook.ui.homeSearch

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View.VISIBLE
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.home_image_text_tab.view.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*


class HomeSearchActivity : ParentActivity() {


    lateinit var sharedPrefrenceManger: SharedPreferencesManagerImpl
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        title = getString(R.string.home)
        sharedPrefrenceManger = SharedPreferencesManagerImpl(this)
        toolbar.moreIcon.visibility = VISIBLE
        toolbar.moreIcon.setOnClickListener {
        }
        toolbar.rightTextView.visibility = VISIBLE
        toolbar.rightTextView.text = "EN/EG/EGP"
        toolbar.rightTextView.setOnClickListener {

        }

        viewPager.adapter = TabsPagerAdapter(supportFragmentManager)
       // homeTabs.setupWithViewPager(viewPager)
        viewPager.currentItem = intent?.getIntExtra(getString(R.string.defaultTab), 0)!!

        indicatorsView.setSelectedIndicator(viewPager.currentItem)

        when(viewPager.currentItem) {
            APPOINTMENT_TAB -> {
                Glide.with(application).load(R.drawable.ic_appointmets_bar).into(image)
                text.text = baseContext.getString(R.string.appointments)
            }
            SURGERIES_TAB -> {
                Glide.with(application).load(R.drawable.ic_surgery_bar).into(image)
                text.text = baseContext.getString(R.string.surgeries)
            }
            ONLINE_CONSULTATION_TAB -> {
                Glide.with(application).load(R.drawable.ic_online_consultation_bar).into(image)
                text.text = baseContext.getString(R.string.onlineConsultation)
            }
        }

        viewPager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                indicatorsView.setSelectedIndicator(position)
                when(position){
                    APPOINTMENT_TAB->{
                        Glide.with(application).load(R.drawable.ic_appointmets_bar).into(image)
                        text.text = baseContext.getString(R.string.appointments)
                    }
                    SURGERIES_TAB->{
                        Glide.with(application).load(R.drawable.ic_surgery_bar).into(image)
                        text.text = baseContext.getString(R.string.surgeries)
                    }
                    ONLINE_CONSULTATION_TAB->{
                        Glide.with(application).load(R.drawable.ic_online_consultation_bar).into(image)
                        text.text = baseContext.getString(R.string.onlineConsultation)
                    }
                }
            }

        })
        setupTabs()
    }


    private fun setupTabs() {
        homeTabs.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorAccent))
        val tabLayout = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout.text = getString(R.string.appointment)
        tabLayout.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_appointmets_bar, 0, 0)
        homeTabs.getTabAt(0)?.customView = tabLayout

        val tabLayout1 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout1.text = getString(R.string.surgeries)
        tabLayout1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_surgery_bar, 0, 0)
        homeTabs.getTabAt(1)?.customView = tabLayout1

        val tabLayout2 = LayoutInflater.from(this).inflate(R.layout.home_image_text_tab, null) as TextView
        tabLayout2.tabText.text = getString(R.string.consulting)
        tabLayout2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_online_consultation_bar, 0, 0)
        homeTabs.getTabAt(2)?.customView = tabLayout2
    }


    inner class TabsPagerAdapter(supportFragmentManage: android.support.v4.app.FragmentManager)
        : FragmentPagerAdapter(supportFragmentManage) {
        override fun getItem(position: Int): android.support.v4.app.Fragment {
            return when (position) {
                APPOINTMENT_TAB -> {
                    AppointementFragment.newInstance()
                }
                SURGERIES_TAB -> {
                    SurgiesFragment.newInstance()
                }
                else -> {
                    OnlineConsultationFragment.newInstance()
                }
            }
        }


        override fun getCount(): Int {
            return TABS_NUM
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SEARCH_REQUEST) {
            viewPager.currentItem = data?.getIntExtra(getString(R.string.defaultTab), 0)!!
            val page = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.pager + ":" + viewPager.currentItem)
            // based on the current position you can then cast the page to the correct
            // class and call the method:
            if (viewPager.currentItem == 0 && page != null) {
                (page is AppointementFragment).let {
                    (page as AppointementFragment).let {
                        it.searchViewModel.reload()
                        it.updateTexts()
                    }
                }
                (page is SurgiesFragment).let {
                    (page as SurgiesFragment).let {
                        it.searchViewModel.reload()
                        it.updateTexts()
                    }
                }
                (page is OnlineConsultationFragment).let {
                    (page as OnlineConsultationFragment).let {
                        it.searchViewModel.reload()
                        it.updateTexts()
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    companion object {
        const val TABS_NUM = 3
        const val APPOINTMENT_TAB = 0
        const val SURGERIES_TAB = 1
        const val ONLINE_CONSULTATION_TAB = 2
        const val SEARCH_REQUEST = 1
    }

}
