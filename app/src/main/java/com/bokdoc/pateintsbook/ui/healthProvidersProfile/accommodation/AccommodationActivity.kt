package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_accommodation.*
import kotlinx.android.synthetic.main.toolbar.*
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsAdapter
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils


class AccommodationActivity : ParentActivity() {
    lateinit var accommodationViewModel: AccommodationViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accommodation)
        enableBackButton()
        title = getString(R.string.accommodation)
        accommodationViewModel = InjectionUtil.getAccommodationViewModel(this)
        getDataFromIntent()
        getAccommodationsData()
//
//        accommodationViewModel.progressLiveData.observe(this, Observer<Boolean> {
//            if (it!!) {
//                progress.visibility = VISIBLE
//            } else {
//                progress.visibility = GONE
//            }
//        })
//
//        accommodationViewModel.accommodationsLiveData.observe(this, Observer<List<AccommodationSections>> {
//            accommodationList.adapter = AccommodationAdapter(this, it!!, this::onAdapterClickListener)
//        })
//
//        accommodationViewModel.errorMessageLiveData.observe(this, Observer<String> {
//            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
//        })
//
//        accommodationViewModel.getAccommodation(intent?.getStringExtra(getString(R.string.idKey))!!)
    }

    private fun getAccommodationsData() {
        progress.visibility = View.VISIBLE
        accommodationViewModel.getAccommodation().observe(this, Observer {
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    progress.visibility = View.GONE
                    tv_details.text = it.data!![0].rooms + " " + it.data!![0].patientRoom + " " + it.data!![0].meals + " " + it.data!![0].accompaniedPersons
                }
                DataResource.INTERNAL_SERVER_ERROR -> {
                    progress.visibility = View.GONE

                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
            }
        })

    }

    private fun getDataFromIntent() {
        accommodationViewModel.id = intent.getStringExtra(getString(R.string.idKey))
        accommodationViewModel.serviceType = intent.getStringExtra(getString(R.string.include_key))
    }

    private fun onAdapterClickListener(view: View) {
        Navigator.navigateToBrowser(this, view.tag.toString())
    }


}
