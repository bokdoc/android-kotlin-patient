package com.bokdoc.pateintsbook.ui.splash

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate

class SplashViewModel(val splashRepository: SplashRepository) : ViewModel() {
    var isTokenEndCalled = false

    fun isOpenTutorials(): Boolean {
        return splashRepository.isOpenTutorialsScreen()
    }

//    fun addFcmToken(): LiveData<DataResource<Boolean>> {
//        return splashRepository.addFcmToken()
//
//    }

    fun updateFirstAppOpenedInSharedPreferences(isFirstOpened: Boolean = false) {
        return splashRepository.updateFirstAppOpenedInSharedPreferences(isFirstOpened)
    }


    fun checkUpdate(): LiveData<DataResource<CheckUpdate>> {
        return splashRepository.checkUpdate()
    }

}