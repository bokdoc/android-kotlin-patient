package com.bokdoc.pateintsbook.ui.notifications

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.repositories.notifications.NotificationsRepository
import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants

class NotificationsViewModel(private val notificationsRepository: NotificationsRepository) : ViewModel() {
    val notificationsLiveData = MutableLiveData<DataResource<INotifications>>()
    val queriesMap = HashMap<String, String>()
    val page = 1

    fun getNotifications(): LiveData<DataResource<INotifications>> {
        queriesMap.put(WebserviceConstants.PAGE_QUERY, page.toString())
        return notificationsRepository.getNotifications(queriesMap)
    }


}