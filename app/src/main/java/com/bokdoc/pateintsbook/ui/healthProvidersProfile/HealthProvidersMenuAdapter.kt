package com.bokdoc.pateintsbook.ui.healthProvidersProfile

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.health_provider_menu_row.view.*

open class HealthProvidersMenuAdapter(private val menuOptions: List<ImageTextRow>, private val clickListener: (String) -> Unit) :
        RecyclerView.Adapter<HealthProvidersMenuAdapter.MenuViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return MenuViewHolder(parent.inflateView(R.layout.health_provider_menu_row))
    }

    override fun getItemCount(): Int {
        return menuOptions.size
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        menuOptions[position].let { imageText ->

            holder.itemView.let { view ->
                view.icon.setImageResource(imageText.image)
                view.tv_title.text = imageText.text
            }

        }
    }

    inner class MenuViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                clickListener.invoke(menuOptions[adapterPosition].text)
            }
        }
    }
}







