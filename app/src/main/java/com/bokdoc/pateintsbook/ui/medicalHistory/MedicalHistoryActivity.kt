package com.bokdoc.pateintsbook.ui.medicalHistory

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistoryImpl
import com.bokdoc.pateintsbook.navigators.LookupsNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.lookups.LookupsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import kotlinx.android.synthetic.main.activity_patient_profile.*
import kotlinx.android.synthetic.main.medical_history_view.*
import kotlinx.android.synthetic.main.toolbar.*

abstract class MedicalHistoryActivity : ParentActivity() {
    private lateinit var viewModel: MedicalHistoryViewModel
    abstract fun getViewModel(): MedicalHistoryViewModel
    abstract fun getLookupType(): String
    abstract fun title(): String
    abstract fun emptyDataMessage():String
    private lateinit var medicalHistoryAdapter: MedicalHistoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medical_history_view)
        medicalHistoryAdapter = MedicalHistoryListAdapter(arrayListOf(), this::onAdapterCLickListener)
        medicalHistoryList.adapter = medicalHistoryAdapter
        viewModel = getViewModel()
        viewModel.key = getLookupType()
        title = title()
        returnArrow.visibility = VISIBLE
        returnArrowClickListener = View.OnClickListener {
            onBackPressed()
        }
        viewModel.get(intent?.getStringExtra(getString(R.string.idKey)!!)!!)

        viewModel.progressLiveData.observe(this, Observer {
            progress.visibility = it!!
        })

        viewModel.messageLiveData.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        viewModel.listLiveData.observe(this, Observer {
            if (it!!.isNotEmpty()) {
                contentGroup.visibility = VISIBLE
                val arrayListMedical = ArrayList<MedicalHistory>().apply {
                    this.addAll(it)
                }
                medicalHistoryAdapter.update(arrayListMedical)
            } else {

                showEmpty(emptyDataMessage(),"",null)
            }
        })

        viewModel.addedLiveData.observe(this, Observer {
            if (!it!!) {
                medicalHistoryAdapter.removeAt(viewModel.addedOrRemovedPosition)
            }
        })

        viewModel.deletedLiveData.observe(this, Observer {
            if (!it!!) {
                medicalHistoryAdapter.addAt(viewModel.addedOrRemovedPosition, viewModel.addedOrRemovedMedicalHistory)
            }
        })

        add.setOnClickListener {
            LookupsNavigator.navigate(this, title(), viewModel.key,viewModel.getLookups(), LOOKUP_REQUEST)
        }

       // setupSwipeListener()
    }

    private fun onAdapterCLickListener(medicalHistory: MedicalHistory, position: Int, action: Int) {
        if (action == MedicalHistoryListAdapter.ACTION_REMOVED) {
            val history = medicalHistoryAdapter.getAt(position)
            viewModel.addedOrRemovedPosition =position
            viewModel.addedOrRemovedMedicalHistory = history
            viewModel.delete(position)
            medicalHistoryAdapter.removeAt(position)
        }
    }

//    private fun setupSwipeListener(){
//        val swipeListener =  object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
//            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
//                return true
//            }
//
//            override fun onSwiped(holder: RecyclerView.ViewHolder, position: Int) {
//                val history = medicalHistoryAdapter.getAt(holder.adapterPosition)
//                viewModel.addedOrRemovedPosition = holder.adapterPosition
//                viewModel.addedOrRemovedMedicalHistory = history
//                viewModel.delete(holder.adapterPosition)
//                medicalHistoryAdapter.removeAt(holder.adapterPosition)
//            }
//
//
//        }
//        ItemTouchHelper(swipeListener).attachToRecyclerView(medicalHistoryList)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LOOKUP_REQUEST && resultCode == Activity.RESULT_OK) {
            val lookup = data?.getParcelableArrayListExtra<LookUpsParcelable>(getString(R.string.selected_lookups))
            val medicalHistory = MedicalHistoryImpl().apply {
                this.historyId = lookup!![0].idLookup
                this.name = lookup[0].name
            }
            medicalHistoryAdapter.add(medicalHistory)
            viewModel.add(medicalHistory)
            viewModel.addedOrRemovedPosition = medicalHistoryAdapter.itemCount - 1
            viewModel.addedOrRemovedMedicalHistory = medicalHistory
            hideEmpty()
        }
    }

    override fun onBackPressed() {
        if(viewModel.listLiveData.value!=null){
            Navigator.naviagteBack(this,getString(R.string.medicalsHistoriesKey),viewModel.listLiveData.value
                    as ArrayList<out Parcelable>)
        }else{
            Navigator.naviagteBack(this, arrayOf(), arrayOf())
        }
    }

    companion object {
        const val LOOKUP_REQUEST = 0
    }
}