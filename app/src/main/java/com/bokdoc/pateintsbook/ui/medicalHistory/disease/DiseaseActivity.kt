package com.bokdoc.pateintsbook.ui.medicalHistory.disease

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryActivity
import com.bokdoc.pateintsbook.ui.medicalHistory.MedicalHistoryViewModel
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil

class DiseaseActivity : MedicalHistoryActivity() {

    override fun title(): String {
        return getString(R.string.myDiseases)
    }

    override fun getViewModel(): MedicalHistoryViewModel {
        return InjectionUtil.getDiseaseViewModel(this)
    }

    override fun getLookupType(): String {
        return getString(R.string.lookupDiseases)
    }

    override fun emptyDataMessage(): String {
        return getString(R.string.chronic_diseases_profile_empty_text)
    }

}
