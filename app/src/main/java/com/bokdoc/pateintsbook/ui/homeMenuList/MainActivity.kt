package com.bokdoc.pateintsbook.ui.homeMenuList

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.bottomnavigation.LabelVisibilityMode
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeMenuList.dialog.UpdateDialog
import com.bokdoc.pateintsbook.ui.login.LoginFragment
import com.bokdoc.pateintsbook.ui.notifications.NotificationsFragment
import com.bokdoc.pateintsbook.ui.patientProfile.MainPatientProfileFragment
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity

import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.ui.tutorials.TutorialsActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import kotlinx.android.synthetic.main.activity_home_menu_list.*
import kotlinx.android.synthetic.main.activity_navigation.*


class MainActivity : AppCompatActivity() {
    private lateinit var homeMenuListViewModel: HomeMenuListViewModel
    private lateinit var userRepository: UserSharedPreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        getViewModel()

        userRepository = UserSharedPreference(applicationContext)
        val sharedPreferences: SharedPreferencesManagerImpl = SharedPreferencesManagerImpl(this)
        val token = sharedPreferences.getData(this.getString(R.string.fcm_token), "")
        val fcmToken = sharedPreferences.getData(this.getString(R.string.token), "")
        val id = sharedPreferences.getData(this.getString(R.string.deviceId), "")
        Log.wtf("ss", fcmToken)
        Log.wtf("ss", id)

        setUpNavigationListener()

        if (intent?.hasExtra(getString(R.string.requests_screen))!!) {
            openFragment(NotificationsFragment.newInstance("", ""))
        } else {
            openFragment(HomeMenuListFragment.newInstance())
        }


        checkUpdate()


    }


    private fun checkUpdate() {
        if (intent.hasExtra(getString(R.string.check_key))) {
            if (intent.getParcelableExtra<CheckUpdate>(getString(R.string.check_key)) != null) {
                UpdateDialog(this, intent.getParcelableExtra(getString(R.string.check_key))) {
                    if(it == UpdateDialog.LOGOUT){
                        homeMenuListViewModel.logout().observe(this, Observer {
                            view_progress.visibility = View.GONE
                            if (it?.status == DataResource.SUCCESS) {
                                LocaleManager.setLocale(baseContext)
                                Navigator.navigate(this, TutorialsActivity::class.java)
                                finish()
                            }
                            it?.message?.let {
                                if (it.isNotEmpty()) {
                                    Utils.showLongToast(baseContext, it)
                                }else{
                                    Utils.showLongToast(baseContext, R.string.errorSent)
                                }
                            }
                        })
                    }
                }.
                show()
            }
        }
    }

    private fun setUpNavigationListener() {


        if (intent?.hasExtra(getString(R.string.requests_screen))!!) {
            navigation.menu.getItem(1).isChecked = true
        }
        navigation.itemIconTintList = null
        navigation.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        navigation.setOnNavigationItemSelectedListener { item ->
            when {
                item.itemId == R.id.ic_nav_home -> {
                    val homeFragment = HomeMenuListFragment.newInstance()
                    openFragment(homeFragment)
                    return@setOnNavigationItemSelectedListener true
                }
                item.itemId == R.id.ic_notifications -> {
                    val patientRequestsFragment = NotificationsFragment.newInstance("", "")
                    openFragment(patientRequestsFragment)
                    return@setOnNavigationItemSelectedListener true

                }
                item.itemId == R.id.ic_nav_profile -> {
                    checkUser()
                    return@setOnNavigationItemSelectedListener true
                }
                item.itemId == R.id.ic_nav_more -> {
                    val moreFragment = MoreFragment.newInstance()
                    openFragment(moreFragment)
                    return@setOnNavigationItemSelectedListener true
                }
            }
            return@setOnNavigationItemSelectedListener false
        }

        when (userRepository.ifUserLogin()) {
            true -> {
                navigation.menu.getItem(2).setIcon(R.drawable.login_icon_selector)
                navigation.menu.getItem(2).title = getString(R.string.profile)
            }
            false -> {
                navigation.menu.getItem(2).setIcon(R.drawable.home_login_selector)
                navigation.menu.getItem(2).title = getString(R.string.login)
            }
        }
    }

    private fun checkUser() {
        userRepository = UserSharedPreference(applicationContext)
        when (userRepository.ifUserLogin()) {
            false -> {
                val loginFragment = LoginFragment.newInstance("", "")
                openFragment(loginFragment)
            }
            true -> {
                val patientProfileFragment = MainPatientProfileFragment.newInstance()
                openFragment(patientProfileFragment)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == MainPatientProfileFragment.BOOK_REQUEST && resultCode==Activity.RESULT_OK){
            Navigator.navigate(this, arrayOf(getString(R.string.screenType), getString(R.string.query)),
                    arrayOf(SearchScreenTypes.getString(SearchScreenTypes.APPOINTMENTS_POSITION),
                            ""), SearchResultsActivity::class.java)
        }else {
            val fragment = supportFragmentManager.findFragmentById(R.id.lay_main_container)
            fragment?.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getViewModel() {
        homeMenuListViewModel = ViewModelsCustomProvider(HomeMenuListViewModel(this,
                SharedPreferencesManagerImpl(this), SplashRepository(applicationContext,
                UserSharedPreference(applicationContext),
                AddFcmWebService(WebserviceManagerImpl.instance())), TokenRepository(applicationContext))).create(HomeMenuListViewModel::class.java)
        homeMenuListViewModel.defaultSelected = intent?.getIntExtra(getString(R.string.defaultTab), 0)!!
    }

    fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.lay_main_container, fragment)
        transaction.commit()
    }

    override fun onResume() {
        super.onResume()
        if (homeMenuListViewModel.sharedPreferencesManagerImpl.getData(getString(R.string.language_key), "en") == "en") {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            navigation.layoutDirection = View.LAYOUT_DIRECTION_LTR
        } else {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
            navigation.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }

        if (menuList != null) {
            menuList.adapter = HomeMenuListAdapter(homeMenuListViewModel.getList(), this::onAdapterClickListener)

            title = getString(R.string.bokdoc)
            navigation.menu.findItem(R.id.ic_nav_home).title = getString(R.string.home)
            //  navigation.menu.findItem(R.id.ic_nav_reservation).title = getString(R.string.reservations)
            navigation.menu.findItem(R.id.ic_notifications).title = getString(R.string.notifications)
            navigation.menu.findItem(R.id.ic_nav_more).title = getString(R.string.more)
            //  navigation.menu.findItem(R.id.ic_nav_reservation).title = getString(R.string.reservations)

            when (userRepository.ifUserLogin()) {
                true -> {
                    navigation.menu.getItem(2).setIcon(R.drawable.login_icon_selector)
                    navigation.menu.getItem(2).title = getString(R.string.profile)
                }
                false -> {
                    navigation.menu.getItem(2).setIcon(R.drawable.home_login_selector)
                    navigation.menu.getItem(2).title = getString(R.string.login)
                }
            }
        }
    }

    private fun onAdapterClickListener(slug: String, position: Int) {
        /*
        when(slug){
            HomeMenuListViewModel.APPOINTMENTS_SLUG->{

            }
            HomeMenuListViewModel.SURGERIES_SLUG->{
                HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
            }
            HomeMenuListViewModel.ONLINE_CONSULTING_SLUG->{
                HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
            }
        }
        */
        HomeNavigator.navigate(this, getString(R.string.defaultTab), position)
    }

}