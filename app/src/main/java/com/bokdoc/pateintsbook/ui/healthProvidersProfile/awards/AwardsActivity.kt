package com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_awards.*
import kotlinx.android.synthetic.main.toolbar.*

class AwardsActivity : ParentActivity() {

    private lateinit var awardsViewModel: AwardsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_awards)
        titleText.text = getString(R.string.awards)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
        awardsList.layoutManager = LinearLayoutManager(this)

        awardsViewModel = InjectionUtil.getAwardsViewModel(this)

        awardsViewModel.progressLiveData.observe(this, Observer {
            progress.visibility = it!!
        })

        awardsViewModel.errorMessageLiveData.observe(this, Observer {
            Toast.makeText(this,it,Toast.LENGTH_LONG).show()
        })

        awardsViewModel.getAwards(intent?.getStringExtra(getString(R.string.idKey))!!)

        awardsViewModel.awardsList.observe(this, Observer {
            awardsList.adapter = AwardsAdapter(it!!)
        })

    }
}
