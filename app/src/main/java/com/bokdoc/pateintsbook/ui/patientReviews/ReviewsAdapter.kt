package com.bokdoc.pateintsbook.ui.patientReviews

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.webservice.models.Review
import com.bokdoc.pateintsbook.utils.paginationUtils.PaginationAdapterCallback
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_progress.view.*
import kotlinx.android.synthetic.main.item_review.view.*

class ReviewsAdapter(val context: Context, val reviews: MutableList<Review>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var errorMsg: String? = null
    private var isLoadingAdded = false
    private var retryPageLoad = false

    private val mCallback: PaginationAdapterCallback

    init {
        this.mCallback = context as PaginationAdapterCallback

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            IListItemType.Types.ITEM.ordinal -> {
                val itemView = inflater.inflate(R.layout.item_review, parent, false)
                viewHolder = ReviewsViewHolder(itemView)
            }

            IListItemType.Types.PAGINATION.ordinal -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = ReviewsLoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return reviews.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        when (getItemViewType(position)) {
            IListItemType.Types.ITEM.ordinal -> {
                val reviewViewHolder = holder as ReviewsViewHolder

                reviews[position].let {
                    if(it.persona.get(it.document)!=null && it.persona.get(it.document)!=null) {
                        if(it.persona.get(it.document).picture!=null)
                        Glide.with(holder.itemView.context).load(it.persona.get(it.document).picture!!.url).
                                into(reviewViewHolder.itemView.image)
                        reviewViewHolder.itemView.name.text = it.persona.get(it.document).name
                    }
                    reviewViewHolder.itemView.dateTime.text = it.date_time.toString
                    reviewViewHolder.itemView.description.text = it.content
                    reviewViewHolder.itemView.rating.rating = it.rate.toFloat()
                }
            }

            IListItemType.Types.PAGINATION.ordinal -> {
                val loadingViewHolder = holder as ReviewsLoadingViewHolder
                if (retryPageLoad) {
                    loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    loadingViewHolder.itemView.loadmore_progress.visibility = View.GONE

                    loadingViewHolder.itemView.loadmore_errortxt.text = if (errorMsg != null)
                        errorMsg
                    else
                        context.getString(R.string.some_thing_went_wrong)

                } else {
                    loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.GONE
                    loadingViewHolder.itemView.loadmore_progress.visibility = View.VISIBLE
                }
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return reviews[position].type.ordinal
    }

    //Helpers


    fun add(review: Review) {
        reviews!!.add(review)
        notifyItemInserted(reviews!!.size - 1)
    }

    fun addAll(reviews: MutableList<Review>) {
        val oldSize: Int = itemCount
        this.reviews.addAll(reviews)
        notifyItemRangeInserted(oldSize, itemCount)
    }

    fun remove(shop: Review?) {
        val position = reviews!!.indexOf(shop)
        if (position > -1) {
            reviews!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Review().apply {
            type = IListItemType.Types.PAGINATION
        })
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = reviews!!.size - 1

        reviews!!.removeAt(position)
        notifyItemRemoved(position)

    }

    fun getItem(position: Int): Review? {
        return reviews!![position]
    }

    fun clear() {
        val size = reviews.size
        reviews.clear()
        notifyItemRangeRemoved(0, size)
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     */


    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(reviews!!.size - 1)

        if (errorMsg != null) this.errorMsg = errorMsg
    }


    inner class ReviewsViewHolder(view: View) : RecyclerView.ViewHolder(view){

    }
    inner class ReviewsLoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)
}