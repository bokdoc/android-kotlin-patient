package com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.qualifications.QualificationsRepository

class QualificationsDetailsViewModel(private val qualificationsRepository: QualificationsRepository) : ViewModel() {

    val errorLiveData = MutableLiveData<String>()
    val detailsLiveData = MutableLiveData<List<Qualifications>>()
    val progressLiveData = MutableLiveData<Int>()
    lateinit var sectionText: String

    fun getDetails(id: String) {
        progressLiveData.value = View.VISIBLE
        qualificationsRepository.getQualifications(id, sectionText).observeForever {
            if (it?.data != null) {
                detailsLiveData.value = it.data
            } else {
                errorLiveData.value = it?.message
            }
            progressLiveData.value = View.GONE
        }
    }
}