package com.bokdoc.pateintsbook.ui.patientFavorites

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.models.meta.Pagination
import com.bokdoc.pateintsbook.data.repositories.favoritesRepository.FavoritesRepository
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.common.profileActions.ProfileActionsViewModel
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes

class FavoritesViewModel(val favoritesRepository: FavoritesRepository, val profileSocialActionsRepository:
    ProfileSocialActionsRepository) : ProfileActionsViewModel(profileSocialActionsRepository) {

    val progressLiveData = MutableLiveData<Boolean>()
    val favoritesLiveData = MutableLiveData<List<Profile>>()
    val errorMessageLiveData = MutableLiveData<String>()

    var pagination:Pagination?=null

    var isLoadingMoreItems = false

    var currentPage = 1


    fun getFavorites() {
        if(currentPage==1)
        progressLiveData.value = true

        favoritesRepository.getPatientFavorites(currentPage.toString()).observeForever {
            if (it?.data != null) {
                if(pagination == null) {
                    pagination = it.meta.pagination
                }else{
                    it.meta.pagination.let {
                        pagination?.currentPage = it.currentPage
                    }
                }
                favoritesLiveData.value = it.data
                if(currentPage==1)
                progressLiveData.value = false
            } else {
                if(it?.errors!=null && it.errors.isNotEmpty())
                errorMessageLiveData.value = it.errors[0].detail
                if(currentPage==1)
                progressLiveData.value = false
            }
        }
    }


    fun removeFavourit(id: String): LiveData<DataResource<String>> {
        return profileSocialActionsRepository.toggleFavorite(id)
    }

    fun getHealthProviderProfile(profile: Profile, isLoadNextAvailabilities: Boolean = false): HealthProvidersProfile {
        return favoritesRepository.getHealthProvidersProfile(profile, isLoadNextAvailabilities)
    }

}