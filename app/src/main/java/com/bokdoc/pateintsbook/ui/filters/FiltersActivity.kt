package com.bokdoc.pateintsbook.ui.filters

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.data.models.filters.expandFilters.FilterExpandChild
import com.bokdoc.pateintsbook.data.models.filters.expandFilters.FiltersExpand
import com.bokdoc.pateintsbook.data.repositories.filter.FilterRepositories
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.filterResults.FilterWebservice
import com.bokdoc.pateintsbook.data.webservice.models.MapFilter
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompletesViewModel
import com.bokdoc.pateintsbook.ui.autoCompleteResults.CustomAutoCompletesModel
import com.bokdoc.pateintsbook.ui.filterMap.FilterMapActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.ui.sortBy.SortByActivity
import com.bokdoc.pateintsbook.ui.sortBy.SortbyAdapter
import com.bokdoc.pateintsbook.utils.TimesDatesUtils
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_filters.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FiltersActivity : ParentActivity(), DatePickerDialog.OnDateSetListener,FiltersChildrenDialog.OnDialogClickedListener {


    lateinit var filtesViewModel: FilterViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)
        enableBackButton()
        title = getString(R.string.filter)

        titleText.text = getString(R.string.filter)
        titleText.visibility = VISIBLE
        toolbar.returnArrow.visibility = VISIBLE
        toolbar.returnArrow.setOnClickListener {
            finish()
        }

        filtersList.layoutManager = LinearLayoutManager(this)

        filtesViewModel = ViewModelProviders.of(this, FiltersModelProvider(FilterRepositories(this.application,
                SharedPreferencesManagerImpl(this), FilterWebservice()))).get(FilterViewModel::class.java)

        progress.visibility = VISIBLE
        filtesViewModel.getFilters(intent?.getStringExtra(getString(R.string.screenType))!!)
                .observe(this, Observer<DataResource<FiltersSectionHeader>> {
                    progress.visibility = GONE
                    if (it?.data != null) {
                        //filtersList.adapter = FiltersExpandAdapter(getGroups(it.data!!), this::onAdapterClickListener)
                        filtersList.adapter = FiltersNewAdapter(it.data!!, this::onAdapterClickListener)
                        setupReset()
                    } else {
                        Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                        done.visibility = View.GONE
                    }
                })

        done.setOnClickListener {
            if (filtesViewModel.filtersSelection == null) {
                Navigator.naviagteBack(this, arrayOf(getString(R.string.query)), arrayOf(""))
                return@setOnClickListener
            }
            Navigator.naviagteBack(this, arrayOf(getString(R.string.query)), arrayOf(filtesViewModel.getFilterQuery()))
        }

    }

    private fun setupReset(){
        rightTextView.visibility = VISIBLE
        rightTextView.text = getString(R.string.reset)
        rightTextView.setOnClickListener {
            filtesViewModel.resetFilterQueries()
            (filtersList.adapter as FiltersNewAdapter)?.reset()
        }
    }
    private fun getGroups(filters: List<FiltersSectionHeader>): List<FiltersExpand> {
        val groups = ArrayList<FiltersExpand>()
        var filtersExpand: FiltersExpand
        filters.forEach {
            if (it.dataType == Filters.MAP_TYPE || it.dataType == Filters.CALENDAR_TYPE) {
                filtersExpand = FiltersExpand(it.sectionText, arrayListOf(), it.dataType, it.key)
            } else {
                filtersExpand = FiltersExpand(it.sectionText, getExpandChildren(it.childItems), it.dataType, it.key)
            }
            groups.add(filtersExpand)
        }

        return groups
    }

    private fun getExpandChildren(filterChildren: List<FilterChild>): List<FilterExpandChild> {
        val filterExpandChildren = ArrayList<FilterExpandChild>()
        filterChildren.forEach {
            filterExpandChildren.add(FilterExpandChild(it.name, it.params, it.unit, it.isSelected, it.rating, it.ranges))
        }
        return filterExpandChildren
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == MAP_FILTER_REQUEST && resultCode == Activity.RESULT_OK) {
            (filtersList.adapter as FiltersNewAdapter).updateAtPosition(filtesViewModel.selectedPosition,true)
            val passedData = data?.getStringExtra(FilterMapActivity.FILTER_BOUNDARY)
            filtesViewModel.updateFiltersSelection(filtesViewModel.selectedKey, Filters.MAP_TYPE, arrayListOf(passedData!!))
        }
    }


    fun onAdapterClickListener(key: String, type: String = "", values: List<FilterChild>,position:Int) {
        filtesViewModel.selectedPosition = position
        when (type) {
            Filters.MAP_TYPE -> {
                filtesViewModel.selectedKey = key
                Navigator.navigate(this, arrayOf(getString(R.string.selected_map_range)),
                        arrayOf(filtesViewModel.getMapRange(key)), FilterMapActivity::class.java, MAP_FILTER_REQUEST)
            }
            Filters.CALENDAR_TYPE -> {
                filtesViewModel.selectedKey = key
                val dateParts = filtesViewModel.getSelectedDateParts(key)
                val dpd:DatePickerDialog
                dpd = if(dateParts.isNotEmpty() && dateParts.size==3){
                    DatePickerDialog.newInstance(this,
                            dateParts[0].toInt(),
                            dateParts[1].toInt()-1,
                            dateParts[2].toInt()
                    )
                }else{
                    val now = Calendar.getInstance()
                    DatePickerDialog.newInstance(this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    )
                }

                dpd.show(fragmentManager, key)
            }
            else ->{
               val dialog  = FiltersChildrenDialog(this,values,key,type)
               dialog.callback = this
               dialog.show()
            }
        }
    }

    override fun onDialogClicked(key: String, dataType: String, values: ArrayList<String>) {
        filtesViewModel.updateFiltersSelection(key, dataType, values)
            if (!filtesViewModel.filtersSelection.isNullOrEmpty() && filtesViewModel.filtersSelection!!.containsKey(key)) {
                if (filtesViewModel.filtersSelection!![key].isNullOrEmpty()) {
                    (filtersList.adapter as FiltersNewAdapter).updateAtPosition(filtesViewModel.selectedPosition, false)
                } else {
                    (filtersList.adapter as FiltersNewAdapter).updateAtPosition(filtesViewModel.selectedPosition, true)

                }
            }
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        /*
        val date = TimesDatesUtils.getFormattedDayWithSplitter(year,monthOfYear,dayOfMonth,StringsConstants.DASH)
        filtesViewModel.updateFiltersSelection(filtesViewModel.selectedKey,Filters.CALENDAR_TYPE, arrayListOf(date))
        */
        filtesViewModel.addDate(filtesViewModel.selectedKey,Filters.CALENDAR_TYPE,year,monthOfYear,dayOfMonth)
        (filtersList.adapter as FiltersNewAdapter).updateAtPosition(filtesViewModel.selectedPosition,true)
    }

    companion object {
        const val MAP_FILTER_REQUEST = 0
    }


}
