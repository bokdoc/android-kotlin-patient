package com.bokdoc.pateintsbook.ui.searchResults

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_search_card_link.view.*

class CardLinksAdapter(private val dynamicLinks: List<DynamicLinks>, private val onClickListener: (DynamicLinks) -> Unit) :
        RecyclerView.Adapter<CardLinksAdapter.CardLinksViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CardLinksViewHolder {
        return CardLinksViewHolder(parent.inflateView(R.layout.item_search_card_link))
    }

    override fun getItemCount(): Int {
        return dynamicLinks.size
    }

    override fun onBindViewHolder(viewHolder: CardLinksViewHolder, position: Int) {
        dynamicLinks[position].let {

            viewHolder.itemView.tv_value.visibility = VISIBLE
            viewHolder.itemView.tv_value.text = it.label


            if (it.count.isNotEmpty() && it.count != "0") {
                viewHolder.itemView.tv_value.visibility = VISIBLE
                viewHolder.itemView.tv_value.text = ColoredTextSpannable.getSpannable(
                        viewHolder.itemView.context.getString(R.string.two_texts_placeholder,
                                it.count, it.label), it.count, ContextCompat.getColor(viewHolder.itemView.context, R.color.colorAccent))



                if (it.icon != null) {
                    when (it.icon) {
                        DynamicLinks.KEY_CLINIC_ICON -> {
                            viewHolder.itemView.tv_value.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_appointments, 0, 0, 0)
                        }
                        DynamicLinks.KEY_ONLINE_CONSULTING_ICON -> {
                            viewHolder.itemView.tv_value.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_consulting_fees, 0, 0, 0)
                        }
                        DynamicLinks.KEY_SURGERIES_ICON -> {
                            viewHolder.itemView.tv_value.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_surgery_fees, 0, 0, 0)
                        }
                    }

                }
            }
        }

    }


    inner class CardLinksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onClickListener.invoke(dynamicLinks[adapterPosition])
            }
        }
    }
}