package com.bokdoc.pateintsbook.ui.autoCompleteResults

import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.models.AutoCompleteValue
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.ResourceWithNameAttr
import com.intrusoft.sectionedrecyclerview.Section
import moe.banana.jsonapi2.HasMany

class AutoCompleteSectionHeader( val childList: List<AutoCompleteValue>, val sectionText: String) : Section<AutoCompleteValue> {

    override fun getChildItems(): List<AutoCompleteValue> {
        return childList
    }

}