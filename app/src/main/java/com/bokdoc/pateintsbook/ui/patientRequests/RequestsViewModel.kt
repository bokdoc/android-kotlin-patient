package com.bokdoc.pateintsbook.ui.patientRequests

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.requests.RequestsRepository
import com.bokdoc.pateintsbook.data.webservice.models.Request

class RequestsViewModel(val requestsRepository: RequestsRepository) : ViewModel() {

    val progressLiveData = MutableLiveData<Boolean>()

    val metaLiveData = MutableLiveData<Meta>()

    val requestsLiveData = MutableLiveData<List<Request>>()
    val requestsNewLiveData = MutableLiveData<List<Request>>()
    val errorMessageLiveData = MutableLiveData<String>()

    var cancellationReason = ""

    var currentPage = 1

    fun getRequests(serviceType: String) {

        progressLiveData.value = true
        requestsRepository.getRequests(serviceType, currentPage).observeForever {
            if (it?.data != null) {
                requestsLiveData.value = it.data
                metaLiveData.value = it.meta
                progressLiveData.value = false
            } else {
                errorMessageLiveData.value = it?.message
                progressLiveData.value = false
            }
        }

        /*
        if (isNew) {

        } else {
            progressLiveData.value = true
            requestsRepository.getNewRequests(isNew, ServiceType).observeForever {
                if (it?.data != null) {
                    requestsLiveData.value = it.data
                    metaLiveData.value = it.meta
                    progressLiveData.value = false
                } else {
                    errorMessageLiveData.value = it?.message
                    progressLiveData.value = false
                }
            }
        }
        */

    }

    fun cancelRequest(url: String, reason: String): MutableLiveData<DataResource<String>> {
        progressLiveData.value = true
        return requestsRepository.cancelRequest(url, reason)
    }


}