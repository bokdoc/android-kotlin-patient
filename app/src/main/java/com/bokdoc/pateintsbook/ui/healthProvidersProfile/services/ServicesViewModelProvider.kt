package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.ServicesRepository

class ServicesViewModelProvider(private val servicesRepository: ServicesRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ServicesViewModel(servicesRepository) as T
    }
}