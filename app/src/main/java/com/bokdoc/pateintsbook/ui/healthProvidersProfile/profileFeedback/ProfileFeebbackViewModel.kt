package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileFeedback.ProfileFeedbackRepository

class ProfileFeedbackViewModel(private val feedbackRepository: ProfileFeedbackRepository) : ViewModel() {
    val errorLiveData = MutableLiveData<String>()
    val progressLiveData = MutableLiveData<Int>()
    val feedbackLiveData = MutableLiveData<List<IFeedback>>()


    fun getFeedback(id: String) {
        progressLiveData.value = VISIBLE
        feedbackRepository.getFeedback(id).observeForever {
            if (it?.data != null) {
                feedbackLiveData.value = it.data
            } else {
                errorLiveData.value = it?.message
            }
            progressLiveData.value = GONE
        }
    }
}