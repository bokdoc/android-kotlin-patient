package com.bokdoc.pateintsbook.ui.filters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.models.filters.Filters
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.filterMap.FilterMapActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsAdapter
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.filter_row.view.*
import java.util.*

class FiltersNewAdapter(var sectionItemList: List<FiltersSectionHeader>,
                        var itemCLickListener: (key: String, type: String,values: List<FilterChild>,position:Int) -> Unit) :
        RecyclerView.Adapter<FiltersNewAdapter.FiltersViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FiltersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.filter_row, parent, false)
        return FiltersViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sectionItemList.size
    }

    override fun onBindViewHolder(holder: FiltersViewHolder, position: Int) {
        holder.view.filterHeader.text = sectionItemList[position].sectionText
        if(sectionItemList[position].isSelected){
            holder.view.tv_mark.visibility = VISIBLE
        }else{
            holder.view.tv_mark.visibility = GONE
        }
        /*
        val childLayoutManager = LinearLayoutManager(holder.view.childrenList.context, LinearLayout.VERTICAL, false)

        childLayoutManager.initialPrefetchItemCount = 11

        holder.view.childrenList.visibility = VISIBLE
        holder.view.childrenList.setHasFixedSize(true)
        holder.view.childrenList.apply {
            layoutManager = childLayoutManager
            adapter = FiltersChildrenAdapter(sectionItemList[position].childItems,sectionItemList[position].key,
                    sectionItemList[position].dataType,itemCLickListener)
            setRecycledViewPool(viewPool)
        }
        */
    }

    fun reset(){
        sectionItemList.forEach {
            it.childItems.forEach {
                it.isSelected = false
                it.ranges = Array(0){0}
                it.isSelected = false
                it.rating = 0.0f
            }
            it.isSelected = false
        }
        notifyDataSetChanged()
    }

    fun updateAtPosition(position:Int,isSelected:Boolean){
        sectionItemList[position].isSelected = isSelected
        notifyItemChanged(position)
    }

    inner class FiltersViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            itemView.setOnClickListener {
                itemCLickListener.invoke(sectionItemList[adapterPosition].key,
                        sectionItemList[adapterPosition].dataType,sectionItemList[adapterPosition].childItems,
                        adapterPosition)
            }

        }
        /*
        init {
        val collapseArrow = view.findViewById<ImageView>(R.id.collapseArrow)
        val line = view.findViewById<View>(R.id.line)
            collapseArrow.tag = true
            view.setOnClickListener {
                if (sectionItemList[adapterPosition].dataType == Filters.MAP_TYPE ||
                        sectionItemList[adapterPosition].dataType == Filters.CALENDAR_TYPE) {
                    itemCLickListener.invoke(sectionItemList[adapterPosition].key, sectionItemList[adapterPosition].dataType, arrayListOf())
                } else {
                    val isCollapse = view.collapseArrow.tag as Boolean
                    when {
                        isCollapse -> {
                            view.collapseArrow.setImageResource(R.drawable.ic_arrow_up)
                            view.line.visibility = GONE
                            //view.childrenList.visibility = VISIBLE
                            collapseArrow.tag = false
                        }
                        else -> {
                            view.collapseArrow.setImageResource(R.drawable.ic_arrow_down)
                            view.line.visibility = VISIBLE
                            //view.childrenList.visibility = GONE
                            collapseArrow.tag = true
                        }
                    }
                }
            }
        }
        */
    }


    /*
    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val date = year.toString() + "-" + monthOfYear + "-" + dayOfMonth
        itemCLickListener.invoke(view?.tag!!, "", listOf(date))
    }
    */

}