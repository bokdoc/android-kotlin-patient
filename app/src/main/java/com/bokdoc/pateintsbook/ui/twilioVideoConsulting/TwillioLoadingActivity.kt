package com.bokdoc.pateintsbook.ui.twilioVideoConsulting

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.WindowManager
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_twillio_loading.*
import kotlinx.android.synthetic.main.toolbar.*
import java.text.SimpleDateFormat
import java.util.*


class TwillioLoadingActivity : ParentActivity() {

    lateinit var twilioViewModel: TwillioViewModel
    var mediaPlayer: MediaPlayer? = null
    var ringingTime: Long = 30000
    lateinit var sharedPreferencesManagerImpl: SharedPreferencesManagerImpl
    var countDownTimer: CountDownTimer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
            val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            keyguardManager.requestDismissKeyguard(this, null)
        } else {
            this.window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        }

//        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
//                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
//                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
//                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        setContentView(R.layout.activity_twillio_loading)

        enableBackButton()
        title = getString(R.string.onlineConsultation)
        getViewModel()

        getDataFromIntent()

        btn_consult_now.setOnClickListener(this)
        getData()
        switchButton.setOnCheckedChangeListener { buttonView, isChecked ->
            twilioViewModel.isVideoActive = isChecked
        }

    }

    private fun startRinginigTimer() {
        countDownTimer = object : CountDownTimer(ringingTime, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                ringingTime = millisUntilFinished
            }

            override fun onFinish() {
                val intent = Intent(this@TwillioLoadingActivity, TwillioLoadingActivity::class.java)
                intent.putExtra(getString(R.string.request_id_key), sharedPreferencesManagerImpl.getData(getString(R.string.request_id_key), ""))
                intent.putExtra(getString(R.string.twillio_token_key), sharedPreferencesManagerImpl.getData(getString(R.string.twillio_token_key), ""))
                Utils.showNotification(this@TwillioLoadingActivity, 1, "Missed Consulting From BokDoc",
                        "You Missed Your Consulting Time Hurry Up Now To Join Your Doctor", intent)
                finish()
            }
        }.start()
    }

    private fun startCounter(counterTime: Int) {
        var time: Long = (counterTime * 60 * 1000).toLong()

        tv_consulting_counter.visibility = View.VISIBLE
        object : CountDownTimer(time, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                time = millisUntilFinished
                val minutes = (time / 1000) / 60
                val seconds = (time / 1000) % 60
                val timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
                tv_consulting_counter.text = timeLeftFormatted
            }

            override fun onFinish() {
                tv_consulting_counter.visibility = View.GONE
            }
        }.start()

    }

    private fun getDataFromIntent() {
        sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(this)
        if (intent.hasExtra(getString(R.string.request_id_key))) {
            twilioViewModel.requestId = intent.getStringExtra(getString(R.string.request_id_key))
            sharedPreferencesManagerImpl.saveData(getString(R.string.request_id_key), intent.getStringExtra(getString(R.string.request_id_key)))
        }
        if (intent.hasExtra(getString(R.string.twillio_token_key))) {
            twilioViewModel.token = intent.getStringExtra(getString(R.string.twillio_token_key))
            sharedPreferencesManagerImpl.saveData(getString(R.string.twillio_token_key), intent.getStringExtra(getString(R.string.twillio_token_key)))
        }
        if (intent.hasExtra(getString(R.string.is_dialing))) {
            toolbar.visibility = View.GONE
            startRinginigTimer()
            mediaPlayer = MediaPlayer.create(this, R.raw.ringing)
            mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mediaPlayer?.isLooping = true
            mediaPlayer?.start()
            content.startRippleAnimation()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            mediaPlayer?.stop()
        }

        countDownTimer?.cancel()

    }


    private fun getData() {
//        val sharedPreferencesManagerImpl = UserSharedPreference(this)
//        twilioViewModel.token = sharedPreferencesManagerImpl.getToken()

        view_progress.visibility = View.VISIBLE
        twilioViewModel.getTwilliotoken().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    view_progress.visibility = View.GONE
                    if (it.data != null) {
                        it.data!![0].let { consulting ->
                            Glide.with(this).load(consulting.persona.picture.media_url).into(iv_doctor)
                            tv_doctor_name.text = consulting.persona.name
                            twilioViewModel.twillioToken = consulting.video_token.token
                            twilioViewModel.chatUrl = consulting.chat_url
                            twilioViewModel.twillioRoom = consulting.room_name
                            twilioViewModel.duration = consulting.duration
                            twilioViewModel.leaveUrl = consulting.leave_url
                            btn_consult_now.text = consulting.button_title
                            getRemaingTime(consulting.date_time.time).let { min ->
                                if (min != 0) {
                                    startCounter(min)
                                }
                            }
                        }
                    }
                }
                else -> {
                    view_progress.visibility = View.GONE
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
            }
        })
    }

    private fun getViewModel() {
        twilioViewModel = InjectionUtil.getTwillioViewModel(this)
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_consult_now.id -> {
                if (twilioViewModel.twillioToken.isNotEmpty()) {

                    if (mediaPlayer != null)
                        mediaPlayer?.stop()

                    countDownTimer?.cancel()

                    Navigator.navigate(this, arrayOf(getString(R.string.twillio_token), getString(R.string.twillioRoom), getString(R.string.chat_url), getString(R.string.leave_url), getString(R.string.isVideoEnable), getString(R.string.consulting_duration)),
                            arrayOf(twilioViewModel.twillioToken
                                    , twilioViewModel.twillioRoom
                                    , twilioViewModel.chatUrl
                                    , twilioViewModel.leaveUrl
                                    , twilioViewModel.isVideoActive.toString()
                                    , twilioViewModel.duration)
                            , VideoActivity::class.java)
                    finish()

                } else {
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (mediaPlayer != null)
            mediaPlayer?.stop()

        countDownTimer?.cancel()

    }

    @SuppressLint("SimpleDateFormat")
    fun getRemaingTime(startTime: String): Int {
        var min = 0
        val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
        val startDate = simpleDateFormat.parse(startTime)
        val cal = Calendar.getInstance()
        val date = cal.time
        val dateFormat = SimpleDateFormat("HH:mm:ss")
        val formattedDate = dateFormat.format(date)
        val endDate = simpleDateFormat.parse(formattedDate)

        var difference = startDate.time - endDate.time
        if (difference > 0) {
            val days = (difference / (1000 * 60 * 60 * 24)).toInt()
            val hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60)).toInt()
            min = (difference - (1000 * 60 * 60 * 24 * days).toLong() - (1000 * 60 * 60 * hours).toLong()).toInt() / (1000 * 60)
        }
        return min
    }

}