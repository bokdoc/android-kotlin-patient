package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.surgeries

import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.data.models.healthProviders.services.ISurgeriesSections
import com.bokdoc.pateintsbook.data.models.healthProviders.services.SurgeriesSections
import com.bokdoc.pateintsbook.data.repositories.common.StringsRepository

class SurgeriesDetailsViewModel(private val context:Context,private val stringsRepository: StringsRepository):ViewModel() {

    fun getSections():List<ISurgeriesSections>{

        val surgeriesSections = ArrayList<SurgeriesSections>()
        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.SURGERY_NAME), arrayListOf("SUrgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.SURGERY_DESCRIPTION),arrayListOf("SUrgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.SURGERY_LOCATION),arrayListOf("Surgery"),
                ISurgeriesSections.LOCATION_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.PHOTOS_VIDEOS),arrayListOf(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.PHOTOS_VIDEOS)),
                ISurgeriesSections.PHOTOS_VIDEOS_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.NEXT_AVAILABILITY),arrayListOf(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.NEXT_AVAILABILITY)),
                ISurgeriesSections.NEXT_AVAILABILITY_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.SURGERY_METHODS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.USED_TOOLS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.USED_TOOLS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.PRE_ORDER_TESTS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.POST_TEST),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.PRE_ORDER_SCANS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.POST_SCANS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.FEES),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION
        ))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.PAYMENT_METHODS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.FREE_SURGERIES),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        surgeriesSections.add(SurgeriesSections(
                stringsRepository.getStringRes(context,StringsRepository.SurgeriesRes.SURGERY_METHODS),arrayListOf("Surgery"),
                ISurgeriesSections.TEXT_SECTION))

        return surgeriesSections
    }
}