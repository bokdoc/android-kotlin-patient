package com.bokdoc.pateintsbook.ui.patientCompares

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.repositories.patientCompareRepository.CompareRepository
import com.bokdoc.pateintsbook.data.webservice.models.Profile

class CompareViewModel(val compareRepository: CompareRepository) : ViewModel() {

    val progressLiveData = MutableLiveData<Boolean>()
    val compareLiveData = MutableLiveData<List<Profile>>()
    val errorMessageLiveData = MutableLiveData<String>()

    fun getCompares( ) {
        compareRepository.compareSharedPreferences.getComparesList().isNotEmpty().let {
            if(it){
                progressLiveData.value = true
                compareRepository.getCompares().observeForever {
                    if (it?.data != null) {
                        compareLiveData.value = it.data
                        progressLiveData.value = false
                    } else {
                        errorMessageLiveData.value = it?.message
                        progressLiveData.value = false
                    }
                }
            }else{
                compareLiveData.value = ArrayList()
            }
        }

    }


}