package com.bokdoc.pateintsbook.ui.nextAvailability

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.repositories.nextavailability.NextAvailabilityRepository

class NextAvailabilityViewModel(val nextAvailabilityRepository: NextAvailabilityRepository) : ViewModel() {

    var url: String = ""
    var selectedNextAvailability:NextAvailabilitiesParcelable?=null
    var isOpenBookForm = true


    fun getNextAvailabilityTimes(): LiveData<DataResource<NextAvailabilitiesParcelable>> {
        return nextAvailabilityRepository.getNextAvailability(url)
    }
}