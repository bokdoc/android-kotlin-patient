package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.media.view.*

open class ProfileGalleryAdapter(val media:List<MediaParcelable>, val onClickListener: ((MediaParcelable) -> Unit)?):
        RecyclerView.Adapter<ProfileGalleryAdapter.GalleryViewHolder>() {

    lateinit var  onClickListenerWithView:(MediaParcelable,View)->Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        return GalleryViewHolder(parent.inflateView(R.layout.media))
    }

    override fun getItemCount(): Int {
        return media.size
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        //Glide.with(holder.view.context).load(media[position]).into(holder.view.mediaImage)
    }

    fun isShowDeleteIcon():Boolean{
        return false
    }


    inner class GalleryViewHolder(val view: View):RecyclerView.ViewHolder(view){
        init {
            if(isShowDeleteIcon()){
                view.delete.visibility = VISIBLE
                view.delete.setOnClickListener {
                    onClickListenerWithView.invoke(media[adapterPosition],it)
                }
                view.setOnClickListener {
                    onClickListenerWithView.invoke(media[adapterPosition],it)
                }
            }else {
                view.setOnClickListener {
                    onClickListener?.invoke(media[adapterPosition])
                }
            }

        }
    }
}