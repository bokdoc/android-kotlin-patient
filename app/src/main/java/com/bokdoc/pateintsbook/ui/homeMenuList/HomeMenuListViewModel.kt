package com.bokdoc.pateintsbook.ui.homeMenuList

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.healthproviders.data.models.homeMenu.HomeMenu
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.repositories.token.TokenRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate
import com.bokdoc.pateintsbook.ui.search.SearchViewModel
import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.utils.DeviceIdGeneration

class HomeMenuListViewModel(private val application: Context,
                            val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                            val splashRepository: SplashRepository,
                            val tokenRepository: TokenRepository) : SearchViewModel(application) {
    var defaultSelected = 0
    val userSharedPreference = UserSharedPreference(context)


    fun getList(): List<ImageTextRow> {
        val imagesTextsRows = ArrayList<ImageTextRow>()
        return imagesTextsRows
    }

    fun getDefaultSelectedFromCache() {
        defaultSelected = getScreenTypePosition()
        if (defaultSelected == -1)
            defaultSelected = 0
    }

    fun sendFcmToken() {
        splashRepository.addFcmToken()
    }

    fun checkUpdate(): LiveData<DataResource<CheckUpdate>> {
        return splashRepository.checkUpdate()
    }

    fun moreData(): List<HomeMenu> {
        val homeMenuParent = ArrayList<HomeMenu>()

        //clinics
        var menuHeader = HomeMenu(SETTING_ID,
                context.getString(R.string.settings)
                , R.drawable.ic_more_setting)
        homeMenuParent.add(menuHeader)

        menuHeader = HomeMenu(FAQS_ID,
                context.getString(R.string.faq)
                , R.drawable.ic_more_faqs)
        homeMenuParent.add(menuHeader)

        menuHeader = HomeMenu(PRIVACY_ID,
                context.getString(R.string.privacy)
                , R.drawable.ic_more_privacy)
        homeMenuParent.add(menuHeader)


        menuHeader = HomeMenu(CONTECTUS_ID,
                context.getString(R.string.contact_us)
                , R.drawable.ic_more_contact)
        homeMenuParent.add(menuHeader)

        menuHeader = HomeMenu(ABOUTUS_ID,
                context.getString(R.string.aboutAs)
                , R.drawable.ic_more_about)
        homeMenuParent.add(menuHeader)

        menuHeader = HomeMenu(SHARE_ID,
                context.getString(R.string.share)
                , R.drawable.ic_more_share)
        homeMenuParent.add(menuHeader)




        if (userSharedPreference.ifUserLogin()) {
            menuHeader = HomeMenu(LOGOUT_ID,
                    context.getString(R.string.logout)
                    , R.drawable.ic_more_logout)
            homeMenuParent.add(menuHeader)
        }
        return homeMenuParent

    }


    fun logout(): MutableLiveData<DataResource<UserResponse>> {
        userSharedPreference.userLogOut()
        return tokenRepository.getToken(DeviceIdGeneration.generateDeviceId(application, sharedPreferencesManagerImpl))
    }


    companion object {
        const val SETTING_ID = 1
        const val CONTECTUS_ID = 2
        const val ABOUTUS_ID = 3
        const val FAQS_ID = 4
        const val PRIVACY_ID = 5
        const val SHARE_ID = 7
        const val LOGOUT_ID = 8
        const val twilio = 9
        const val APPOINTMENTS_POSITION = 1
        const val SURGERIES_POSITION = 2
        const val ONLINE_CONSULTING_POSITION = 0
    }
}