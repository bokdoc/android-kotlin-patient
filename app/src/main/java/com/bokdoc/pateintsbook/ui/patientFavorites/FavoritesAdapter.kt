package com.bokdoc.pateintsbook.ui.patientFavorites

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.PriceTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_favorite.view.*
import kotlinx.android.synthetic.main.item_progress.view.*

class FavoritesAdapter(val profiles: List<Profile>, val onClickListener: (View, Profile, Int) -> Unit)
    : SearchResultsAdapter(profiles, onClickListener) {

    fun removeAt(position: Int) {
        (profiles as MutableList).removeAt(position)
        notifyItemRemoved(position)
    }

    /*
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = FavoritesViewHolder(itemView)
            }

            LOADING -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return (viewHolder as RecyclerView.ViewHolder?)!!
    }

    override fun getItemCount(): Int {
        return profiles.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (getItemViewType(position)) {
            ITEM -> {
                profiles[position].let {
                    Glide.with(holder.itemView.context).load(it.picture.url).into(holder.itemView.doctorsImage)
                    holder.itemView.doctorName.text = it.name
                    holder.itemView.doctorFee.text = PriceTextSpannable.getTextWithPrice(holder.itemView.context, it.fees?.feesAverage!!,
                            it.fees?.currencySymbols!!, R.string.feePlaceholder)
                    holder.itemView.doctorLocation.text = it.mainSpeciality
                    holder.itemView.rating.rating = it.rating
                    holder.itemView.doctorName.text = it.name
                }
            }
            LOADING -> {
                val loadingViewHolder = holder as SearchResultsAdapter.LoadingViewHolder
                if (retryPageLoad) {
                    loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    loadingViewHolder.itemView.loadmore_progress.visibility = View.GONE

                } else {
                    loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.GONE
                    loadingViewHolder.itemView.loadmore_progress.visibility = View.VISIBLE
                }

            }
        }


    }

    fun addLoadingFooter() {
        add(Profile())
    }


    fun removeAt(position: Int) {
        (profiles as MutableList).removeAt(position)
        notifyItemRemoved(position)
    }

    override fun add(profile: Profile) {
        (profiles as MutableList).add(profile)
        notifyDataSetChanged()
    }

    inner class FavoritesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.iv_delete_fav.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.bookAppointment.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
        }
    }
    */
}