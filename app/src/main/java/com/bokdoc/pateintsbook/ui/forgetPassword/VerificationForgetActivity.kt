package com.bokdoc.pateintsbook.ui.forgetPassword

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_verification.*

class VerificationForgetActivity : ParentActivity() {
    lateinit var forgetPasswordViewModel: ForgetPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        enableBackButton()
        title = getString(R.string.verify_number)
        getViewModel()
        getDataFromIntent()
        btn_verify.setOnClickListener(this)
        tv_re_send.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {
            btn_verify.id -> {
                when (txt_pin_entry.text.isNullOrEmpty()) {
                    true -> {
                        Utils.showShortToast(this, getString(R.string.enter_verification_code))
                    }
                    false -> {
                        forgetPasswordViewModel.forgetPassword.mobileVerificationCode = txt_pin_entry.text.toString()
                        sendVerification()
                    }
                }
            }
            tv_re_send.id -> {
                switchSendingProgressVisibility(View.VISIBLE)
                forgetPasswordViewModel.forgetMobile().observe(this, Observer {
                    when (it!!.status) {
                        DataResource.SUCCESS -> {
                            switchSendingProgressVisibility(View.GONE)
                            Utils.showShortToast(this, getString(R.string.sms_sent_successfully))
                        }
                        DataResource.FAIL -> {
                            switchSendingProgressVisibility(View.GONE)
                            Utils.showShortToast(this, getString(R.string.some_thing_went_wromg))
                        }
                        DataResource.VALIDATION_ERROR -> {
                            switchSendingProgressVisibility(View.GONE)
                            Utils.showShortToast(this, it.errors[0].detail)
                        }

                    }
                })
            }
        }
    }

    private fun sendVerification() {
        switchSendingProgressVisibility(View.VISIBLE)
        forgetPasswordViewModel.forgetVerification().observe(this, Observer {
            when (it!!.status) {
                DataResource.SUCCESS -> {
                    switchSendingProgressVisibility(View.GONE)
                    Navigator.navigate(this, getString(R.string.forgetPassword), forgetPasswordViewModel.forgetPassword, NewPasswordActivity::class.java, 1)
                }
                DataResource.FAIL -> {
                    switchSendingProgressVisibility(View.GONE)
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wromg))
                }
                DataResource.VALIDATION_ERROR -> {
                    switchSendingProgressVisibility(View.GONE)
                    Utils.showShortToast(this, it.errors[0].detail)
                }

            }
        })

    }

    private fun getDataFromIntent() {
        forgetPasswordViewModel.forgetPassword = intent.getParcelableExtra(getString(R.string.forgetPassword))
    }

    private fun getViewModel() {
        forgetPasswordViewModel = InjectionUtil.getForgetPasswordViewModel(this)
    }

}