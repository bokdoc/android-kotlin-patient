package com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.consulting.ProfileConsultingRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow

class ConsultingViewModel(val consultingRepository: ProfileConsultingRepository) : ViewModel() {

    lateinit var id: String
    lateinit var serviceType: String

    fun getConsulting(): LiveData<DataResource<ConsultingRow>> {
        return consultingRepository.getConsulting(id, serviceType)
    }

}