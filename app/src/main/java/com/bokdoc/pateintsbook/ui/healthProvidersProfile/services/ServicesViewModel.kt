package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.ServicesRepository

class ServicesViewModel(private val servicesRepository: ServicesRepository):ViewModel() {
    val progress = MutableLiveData<Int>()
    val errorMessage = MutableLiveData<String>()
    val servicesList = MutableLiveData<List<Service>>()

    fun getServices(id:String){
        progress.value = VISIBLE
        servicesRepository.getServices(id).observeForever {
            if(it?.data!=null){
                servicesList.value = it.data
            }else{
                errorMessage.value = it?.message
            }
            progress.value = GONE
        }
    }
}