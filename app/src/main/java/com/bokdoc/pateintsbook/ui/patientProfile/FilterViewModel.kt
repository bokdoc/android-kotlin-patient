package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.FilterModel
import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl

class FilterViewModel(val context: Context) : ViewModel() {
    private var sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(context)

    fun saveSelected(selected: String) {
        sharedPreferencesManagerImpl.saveData(context.getString(R.string.selectedRequestFilterId), selected)
    }

    fun getSelected(): String {
        return sharedPreferencesManagerImpl.getData(context.getString(R.string.selectedRequestFilterId), "")
    }


    fun getIndexOfSelected(ServiceTypeList: List<FilterModel>?): Int {
        val size = ServiceTypeList?.size
        val selected = getSelected()
        if (selected.isNotEmpty()) {
            (0 until size!!).forEach { position ->
                ServiceTypeList[position].let {
                    if (it.key == selected)
                        return position
                }
            }
        }
        return -1
    }
}