package com.bokdoc.pateintsbook.ui.forgetPassword

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.healthproviders.data.repositories.forgetPassword.ForgetPasswordRepository
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.forgetPassword.ForgetPassword

class ForgetPasswordViewModel(val forgetPasswordRepository: ForgetPasswordRepository) : ViewModel() {

    var forgetPassword = ForgetPassword()

    fun forgetMobile(): LiveData<DataResource<Boolean>> {
        return forgetPasswordRepository.passwordMobile(forgetPassword)
    }


    fun forgetVerification(): LiveData<DataResource<Boolean>> {
        return forgetPasswordRepository.passwordVerification(forgetPassword)
    }


    fun forgetNew(): LiveData<DataResource<Boolean>> {
        return forgetPasswordRepository.passwordNew(forgetPassword)
    }
}