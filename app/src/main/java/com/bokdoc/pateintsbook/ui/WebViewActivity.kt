package com.bokdoc.pateintsbook.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import kotlinx.android.synthetic.main.activity_web_view.*
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListActivity
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import kotlinx.android.synthetic.main.toolbar.*
import java.net.URLDecoder


class WebViewActivity : ParentActivity() {

    private val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1

    var document = "http://drive.google.com/viewerng/viewer?embedded=true&url="
    var url: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        getDataFromIntent()
        LocaleManager.setLocale(baseContext)
        setupWebView()
        progress.visibility = View.VISIBLE
    }

    private fun requestPermissionForFileSystem() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Toast.makeText(this,
                    R.string.permissions_needed,
                    Toast.LENGTH_LONG).show()
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        }
    }

    private fun checkPermissionForCameraAndMicrophone(): Boolean {
        val resultSTORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return resultSTORAGE == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            var cameraAndMicPermissionGranted = true

            for (grantResult in grantResults) {
                cameraAndMicPermissionGranted = cameraAndMicPermissionGranted and
                        (grantResult == PackageManager.PERMISSION_GRANTED)
            }
        }
    }

    private fun getDataFromIntent() {

        when (intent.hasExtra(getString(R.string.url_key))) {
            true -> {
                url = intent.getStringExtra(getString(R.string.url_key))
            }
        }

        when (intent.hasExtra(getString(R.string.pdf_key))) {
            true -> {
                url = document + url
            }
        }
        when (intent.hasExtra(getString(R.string.title_key))) {
            true -> {
                title = intent.getStringExtra(getString(R.string.title_key))
            }
        }

        when (intent.hasExtra(getString(R.string.typeKey))) {
            true -> {
                enableBackButton()
                returnArrow.setImageResource(R.drawable.ic_close)
                requestPermissionForFileSystem()
            }
            false -> {
                enableBackButton()

            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkPermissionForCameraAndMicrophone()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        if (webView != null) {
            val webSettings = webView!!.settings
            webView.settings.javaScriptEnabled = true
            webSettings.allowFileAccess = true
            webSettings.builtInZoomControls = false
            webSettings.loadWithOverviewMode = true
            webView.addJavascriptInterface(WebAppInterface(this), "interface")
            webView!!.webViewClient = object : WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && request?.url != null) {
                        view?.loadUrl(request.url.toString())
                    }
                    progress.visibility = View.GONE

                    return true
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    progress.visibility = View.GONE

                }
            }
            webView!!.loadUrl(URLDecoder.decode(url, "UTF-8"), true)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        webView.onActivityResult(requestCode, resultCode, data)
    }

    /** Instantiate the interface and set the context  */
    class WebAppInterface(private val context: Context) {

        /** Show a toast from the web page  */
        @JavascriptInterface
        fun review() {
            Navigator.navigate(context as Activity, HomeMenuListActivity::class.java)
        }
    }
}