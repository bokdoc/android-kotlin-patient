package com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.VISIBLE
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.constants.MediaTypes
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_profile_gallery.*
import kotlinx.android.synthetic.main.toolbar.*

class ProfileGalleryActivity : ParentActivity() {

    lateinit var viewModel: ProfileGalleryViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_gallery)
        titleText.text = getString(R.string.gallery)
        returnArrow.visibility = VISIBLE
        returnArrow.setOnClickListener {
            finish()
        }
        galleryList.layoutManager = LinearLayoutManager(this)
        viewModel = InjectionUtil.getGalleryViewModel(this)
        if(intent?.hasExtra(getString(R.string.idKey))!!) {
            viewModel.getMedia(intent?.getStringExtra(getString(R.string.idKey))!!)
        }else{
            val profileMedia = intent?.getParcelableArrayListExtra<MediaParcelable>(getString(R.string.profileMediaKey))
            galleryNumber.text = getString(R.string.placeHolder,profileMedia!!.size.toString(),getString(R.string.photos))
            galleryList.adapter = ProfileGalleryAdapter(profileMedia,this::onAdapterClickListener)
        }
        viewModel.errorMessageLiveData.observe(this,Observer<String>{
            Toast.makeText(this,it!!,Toast.LENGTH_LONG).show()
        })

        viewModel.progressLiveDate.observe(this,Observer<Int>{
            progress.visibility = it!!
        })

        viewModel.galleryLiveData.observe(this,Observer<List<MediaParcelable>>{
            galleryNumber.text = getString(R.string.placeHolder,it!!.size.toString(),getString(R.string.photos))
            galleryList.adapter = ProfileGalleryAdapter(it,this::onAdapterClickListener)
        })

    }

    private fun onAdapterClickListener(profileMedia: MediaParcelable){
        when(profileMedia.mediaType){
            IMedia.IMAGE_TYPE ->{

            }
            IMedia.VIDEO_TYPE->{
                Navigator.navigateToVideoPlayer(this,profileMedia.url,MediaTypes.VIDEO_MP4)
            }
        }
    }

}
