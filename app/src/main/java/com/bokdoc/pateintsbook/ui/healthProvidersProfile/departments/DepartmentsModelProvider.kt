package com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.departments.DepartmentsRepository

class DepartmentsModelProvider(private val departmentsRepository: DepartmentsRepository):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DepartmentsViewModel(departmentsRepository) as T
    }
}