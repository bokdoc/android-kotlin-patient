package com.bokdoc.pateintsbook.ui.bookAppointments

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.bookAppointement.HealthProvidersUIBookInfoImp
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.VALIDATION_ERROR
import com.bokdoc.pateintsbook.data.models.KeyValue
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.HealthProvidersProfileActivity
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.NextAvailabilitiesAdapter
import com.bokdoc.pateintsbook.ui.maps.MapShowActivity
import com.bokdoc.pateintsbook.ui.maps.MapsActivity
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.formatters.FeesFormatter
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.listeners.NextAvailabilitesClickListener
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_book_appointments.*
import kotlinx.android.synthetic.main.include_book_service.*
import kotlinx.android.synthetic.main.include_promo_code_view.*
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.ref.WeakReference


class BookAppointmentsActivity : ParentActivity() {

    private lateinit var bookAppointmentsViewModel: BookAppointmentsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointments)
        enableBackButton()
        initalizeInputs()
        bookAppointmentsViewModel = InjectionUtil.getBookAppointementsViewModel(this)
        Utils.hideKeyboard(this)
        btn_activate_promo.setOnClickListener(this)
        btn_renter_promo.setOnClickListener(this)

        if (intent?.hasExtra(getString(R.string.profileKey))!!) {
            intent?.getParcelableArrayListExtra<HealthProvidersUIBookInfoImp>(getString(R.string.profileKey)).let {
                bookAppointmentsViewModel.healthProvidersProfile = it!![0]
                bindProfile(it[0])
            }
        }

        if (intent?.hasExtra(getString(R.string.nextAvailabilitiesKey))!!) {
            intent?.getParcelableArrayListExtra<NextAvailabilitiesParcelable>((getString(R.string.nextAvailabilitiesKey)))
                    .let {
                        bookAppointmentsViewModel.selectedNextAvailability = it!![0]
                    }
        }

        if (intent?.hasExtra(getString(R.string.url_key))!!) {
            bookAppointmentsViewModel.bookingInfoUrl = intent?.getStringExtra(getString(R.string.url_key))!!
            getBookingDetails()
        }

        nextAvailabilityBtn.setOnClickListener(NextAvailabilitesClickListener(WeakReference(this),
                bookAppointmentsViewModel.healthProvidersProfile.nextAvailabilities, NEXT_AVALIABILITES_REQUEST))

        setupMaleFemaleButtons()
        confirmCode.setOnClickListener {
            setBookInfo()
            bookAppointmentsViewModel.validate().let {
                if (it.isEmpty()) {
                    book_parent.visibility = GONE
                    confirmCode.visibility = GONE
                    switchBgProgressVisibility(VISIBLE)
                    bookAppointmentsViewModel.confirm().observe(this, Observer {
                        if (it?.status == DataResource.SUCCESS && it.meta.links.isNotEmpty()) {
                            Log.i("url", it.meta.links)
                            if (it.message.isNotEmpty())
                                Utils.showLongToast(this, it.message)

                            Navigator.navigate(this, arrayOf(getString(R.string.title_key),
                                    getString(R.string.url_key)), arrayOf(titleText.text.toString(),
                                    it.meta.links), WebViewActivity::class.java)
                            setResult(Activity.RESULT_OK)
                            finish()
                        } else {
                            if (it != null && it.errors.isNotEmpty()) {
                                com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, it.errors[0].detail)
                            } else {
                                com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, getString(R.string.server_error))
                            }
                            book_parent.visibility = VISIBLE
                            confirmCode.visibility = VISIBLE
                            switchBgProgressVisibility(GONE)
                        }
                    })
                } else {
                    showError(it)
                }
            }
        }
        if (bookAppointmentsViewModel.isSurgeries) {
            surgeriesGroup.visibility = VISIBLE
            addAttachmentView.setOnClickListener {
                Navigator.navigate(this, arrayOf(getString(R.string.is_show_only)),
                        arrayOf(false.toString()), getString(R.string.media_key),
                        bookAppointmentsViewModel.bookInfo.surgeriesMedia as ArrayList<out Parcelable>
                        , AttachmentMediaActivity::class.java, MEDIA_REQUEST)
            }
        }

        openMap.setOnClickListener {
            if (bookAppointmentsViewModel.bookInfo.location?.latitude!!.isNotEmpty()) {
                Navigator.navigate(this, getString(R.string.latlng_key), LatLng(
                        bookAppointmentsViewModel.bookInfo.location!!.latitude.toDouble(),
                        bookAppointmentsViewModel.bookInfo.location!!.longitude.toDouble()) as Parcelable,
                        MapsActivity::class.java, MAP_REQUEST)
            } else {
                Navigator.navigate(this, MapsActivity::class.java, MAP_REQUEST)
            }
        }


        setupLiveDataListeners()
        setupBookingInfoFields()


    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        if (p0 != null) {
            when (p0.id) {
                btn_activate_promo.id -> {
                    verifyPromoCode()
                }
                btn_renter_promo.id -> {
                    updatePromoView()
                }
            }
        }
    }

    private fun updatePromoView() {
        pb_promo_status.visibility = View.GONE
        lay_promo_status.visibility = View.GONE
        lay_enter_promo.visibility = View.VISIBLE
        et_promo.requestFocus()
    }

    private fun verifyPromoCode() {
        pb_promo_status.visibility = View.VISIBLE
        if (!et_promo.text.isNullOrEmpty()) {
            lay_enter_promo.visibility = View.GONE
            bookAppointmentsViewModel.promoCode.apply {
                coupon = et_promo.text.toString()
                patientId = bookAppointmentsViewModel.userId()
                servicePivotType = bookAppointmentsViewModel.bookInfo.servicePivotType
                servicePivotId = bookAppointmentsViewModel.bookInfo.servicePivotId.toString()
                bookSecondaryDuration = bookAppointmentsViewModel.bookInfo.bookSecondaryDuration

            }
            bookAppointmentsViewModel.addPromoCode().observe(this, Observer {
                when (it?.status) {
                    SUCCESS -> {
                        FeesFormatter.setFees(it.meta.coupon?.mainFees, tv_surgery_fees_from, tv_surgery_fees_to,
                                it.meta.coupon?.subMainFees, tv_surgery_fees_national_from, tv_surgery_fees_national_to, iv_fees)
                        tv_promo_status.visibility = View.VISIBLE
                        tv_promo_status.text = it.meta.message

                        if(it.meta.coupon?.discountId!=null) {
                            bookAppointmentsViewModel.discountId = it.meta.coupon!!.discountId!!
                            bookAppointmentsViewModel.coupon = et_promo.text.toString()
                        }

                        tv_promo_status.setTextColor(ContextCompat.getColor(this,R.color.colorGreen))
                        pb_promo_status.visibility = View.GONE
                        lay_promo_status.visibility = View.VISIBLE

                        if(it.meta.coupon?.discountId!=null)
                        bookAppointmentsViewModel.bookInfo.discountId = it.meta.coupon!!.discountId
                    }
                    VALIDATION_ERROR -> {
                        tv_promo_status.setTextColor(ContextCompat.getColor(this, R.color.colorErrorRed))
                        tv_promo_status.visibility = View.VISIBLE
                        tv_promo_status.text = it.message
                        pb_promo_status.visibility = View.GONE
                        lay_promo_status.visibility = View.VISIBLE
                        //reset coupon
                        bookAppointmentsViewModel.coupon = ""
                    }
                }
            })
        } else {
            pb_promo_status.visibility = View.GONE

        }
    }

    private fun setupSelectDurationViews() {
        if (bookAppointmentsViewModel.healthProvidersProfile.secondaryDurationConsulting) {
            tv_select_duration.visibility = VISIBLE
            secondary_durations_spinner.visibility = VISIBLE
            secondary_durations_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                    if (bookAppointmentsViewModel.isLoadSecondaryDuration) {
                        secondary_durations_progress.visibility = VISIBLE
                        rv_next_next_availabilities.isClickable = false
                        confirmCode.isClickable = false
                        secondary_durations_spinner.isClickable = false
                        bookAppointmentsViewModel.getSecondaryDurationBookingDetails(position)
                    } else {
                        bookAppointmentsViewModel.isLoadSecondaryDuration = true
                    }
                }

            }
        }
        val dataAdapter = ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, KeyValue.toValues(bookAppointmentsViewModel.healthProvidersProfile.secondaryDurations))
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        secondary_durations_spinner.setAdapter(dataAdapter)
    }

    private fun setupBookingInfoFields() {

        tvUserName.text = ColoredTextSpannable.getSpannable(getString(R.string.nameWithStar), getString(R.string.star),
                ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))

        tvMobile.text = ColoredTextSpannable.getSpannable(getString(R.string.phoneWithStar), getString(R.string.star),
                ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))

        tvAge.text = ColoredTextSpannable.getSpannable(getString(R.string.ageWithStar), getString(R.string.star),
                ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))


    }

    private fun setEditTextTouchListeners() {
        etUserName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!bookAppointmentsViewModel.isNameEditHasData) {
                    tvUserName.setTextColor(ContextCompat.getColor(this@BookAppointmentsActivity, R.color.colorBlack))
                    tvUserName.text = ColoredTextSpannable.getSpannable(getString(R.string.nameWithStar), getString(R.string.star),
                            ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))
                    //etUserName.removeTextChangedListener(this)
                    bookAppointmentsViewModel.isNameEditHasData = true
                }
            }

        })

        phoneNumberEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!bookAppointmentsViewModel.isMobileEditHasData) {
                    tvMobile.setTextColor(ContextCompat.getColor(this@BookAppointmentsActivity, R.color.colorBlack))
                    tvMobile.text = ColoredTextSpannable.getSpannable(getString(R.string.phoneWithStar), getString(R.string.star),
                            ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))
                    //phoneNumberEdit.removeTextChangedListener(this)
                    bookAppointmentsViewModel.isMobileEditHasData = true
                }
            }

        })

        etAge.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!bookAppointmentsViewModel.isAgeEditHasData) {
                    tvAge.setTextColor(ContextCompat.getColor(this@BookAppointmentsActivity, R.color.colorBlack))
                    tvAge.text = ColoredTextSpannable.getSpannable(getString(R.string.ageWithStar), getString(R.string.star),
                            ContextCompat.getColor(activity.applicationContext!!, R.color.colorErrorRed))
                    // etAge.removeTextChangedListener(this)
                    bookAppointmentsViewModel.isAgeEditHasData = true
                }
            }

        })

    }

    private fun setupLiveDataListeners() {
        bookAppointmentsViewModel.isSurgeriesLiveData.observe(this, Observer {
            if (it != null && it)
                surgeriesGroup.visibility = VISIBLE
            addAttachmentView.setOnClickListener {
                Navigator.navigate(this, arrayOf(getString(R.string.is_show_only)), arrayOf(false.toString()), getString(R.string.media_key),
                        bookAppointmentsViewModel.bookInfo.surgeriesMedia as ArrayList<out Parcelable>
                        , AttachmentMediaActivity::class.java, MEDIA_REQUEST)
            }
        })

        bookAppointmentsViewModel.secondaryDurationBookingInfoLive.observe(this, Observer {
            secondary_durations_progress.visibility = GONE
            rv_next_next_availabilities.isClickable = true
            bookAppointmentsViewModel.isLoadSecondaryDuration = true
            secondary_durations_spinner.isClickable = true
            confirmCode.isClickable = true
            if (it.isNullOrEmpty()) {
                iv_right.visibility = VISIBLE
                iv_left.visibility = VISIBLE
                rv_next_next_availabilities.adapter =
                        NextAvailabilitiesAdapter(bookAppointmentsViewModel.healthProvidersProfile.serviceInfo
                                .nextAvailabilities,
                                this::onNextAvailabilitiesClickListener, this::onNextAvailabilitiesClickListener)
                tv_surgery_fees_from.text = ""
                tv_surgery_fees_to.text = ""
                tv_surgery_fees_national_from.text = ""
                tv_surgery_fees_national_to.text = ""

                FeesFormatter.setFees(bookAppointmentsViewModel.healthProvidersProfile.serviceInfo.mainFees,
                        tv_surgery_fees_from, tv_surgery_fees_to,
                        bookAppointmentsViewModel.healthProvidersProfile.serviceInfo.subMainFees,
                        tv_surgery_fees_national_from, tv_surgery_fees_national_to, iv_fees)

            } else {
                Utils.showLongToast(this, it)
            }
        })
    }

    private fun getBookingDetails() {
        //  switchBgProgressVisibility(VISIBLE)
        bookAppointmentsViewModel.getBookingInfo()
        bookAppointmentsViewModel.bookingInfoLiveData.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                setBookLabels(bookAppointmentsViewModel.healthProvidersProfile.serviceSlug)
                bindProfile(bookAppointmentsViewModel.healthProvidersProfile as HealthProvidersUIBookInfoImp)
                bindPatient()
                book_parent.visibility = VISIBLE
                confirmCode.visibility = VISIBLE
            } else {
                com.bokdoc.pateintsbook.utils.Utils.showLongToast(this, it)
            }
            //  switchBgProgressVisibility(GONE)
            lay_shimmer.visibility = View.GONE
        })
    }

    private fun setBookLabels(slug: String) {
        when (slug) {
            LinksCardsDetails.CLINICS_SLUG -> {
                title = getString(R.string.book_appointment_title)
                confirmCode.text = getString(R.string.book_appointment_button)
                enterBookingInfoTv.text = getString(R.string.form_label_appointment)
            }
            LinksCardsDetails.CONSULTING_SLUG -> {
                title = getString(R.string.book_consulting_title)
                confirmCode.text = getString(R.string.book_consulting_button)
                enterBookingInfoTv.text = getString(R.string.form_label_consulting)
            }
            LinksCardsDetails.SURGERIES_SLUG -> {
                title = getString(R.string.book_surgery_title)
                confirmCode.text = getString(R.string.book_surgery_button)
                enterBookingInfoTv.text = getString(R.string.form_label_surgery)
            }
        }
    }

    private fun bindPatient() {
        /*
        bookAppointementsViewModel.bookInfo.patientName.let {
            if (it.isNotEmpty())
                etUserName.setText(it)
        }
        bookAppointementsViewModel.bookInfo.patientMobile.let {
            if(it.isNotEmpty())
                phoneNumberEdit.setText(it)
        }
        bookAppointementsViewModel.bookInfo.patientEmail.let {
            if(it.isNotEmpty())
                etEmail.setText(it)
        }
        bookAppointementsViewModel.bookInfo.patientAge.let {
            if(it.isNotEmpty())
                etAge.setText(it)
        }
        bookAppointementsViewModel.bookInfo.patientGender.let {
            if(it.isNotEmpty()){
                updateMaleFemaleButtons(it)
            }

        }
        */
    }

    private fun initalizeInputs() {
        lay_shimmer.startShimmerAnimation()
        userNameInput.onFocusChangeListener = View.OnFocusChangeListener { _, isFoucs ->
            userNameInput.error = null
        }

        emailInput.onFocusChangeListener = View.OnFocusChangeListener { _, isFoucs ->
            emailInput.error = null
        }

        ageInput.onFocusChangeListener = View.OnFocusChangeListener { _, isFoucs ->
            ageInput.error = null
        }

    }

    private fun setBookInfo() {
        bookAppointmentsViewModel.bookInfo.patientName = etUserName?.text.toString()
        bookAppointmentsViewModel.bookInfo.patientMobile = phoneNumberEdit?.text.toString()
        bookAppointmentsViewModel.bookInfo.patientAge = etAge?.text.toString()
        bookAppointmentsViewModel.bookInfo.patientEmail = etEmail?.text.toString()
        bookAppointmentsViewModel.bookInfo.surgeriesCaseDescription = surgeriesCaseDescription?.text.toString()
        Log.i("caseDescription", surgeriesCaseDescription?.text.toString())
    }

    private fun showError(errorType: List<String>) {
        errorType.forEach {
            when (it) {
                /*
                BookAppointmentsViewModel.NAME_REQUIRED->userNameInput.error = getString(R.string.errorFillField)
                BookAppointmentsViewModel.MOBILE_REQUIRED->Utils.showLongToast(this,R.string.enter_mobile_num)
                BookAppointmentsViewModel.AGE_REQUIRED->ageInput.error=getString(R.string.errorFillField)
                BookAppointmentsViewModel.EMAIL_REQUIRED->emailInput.error=getString(R.string.errorFillField)
                BookAppointmentsViewModel.CASE_DESCRIPTION_REQUIRED->getString(R.string.errorFillField)
                BookAppointmentsViewModel.RESERVATION_DATE_REQUIRED->Utils.showLongToast(this,R.string.select_next_next_availabilities)
                */
                // BookAppointmentsViewModel.GENDER_REQUIRED->Utils.showLongToast(this,R.string.select_gender_first)
                BookAppointmentsViewModel.NAME_REQUIRED -> {
                    tvUserName.setTextColor(ContextCompat.getColor(this, R.color.colorErrorRed))
                    setEditTextTouchListeners()
                }

                BookAppointmentsViewModel.MOBILE_REQUIRED -> {
                    tvMobile.setTextColor(ContextCompat.getColor(this, R.color.colorErrorRed))
                    setEditTextTouchListeners()
                }

                BookAppointmentsViewModel.AGE_REQUIRED -> {
                    tvAge.setTextColor(ContextCompat.getColor(this, R.color.colorErrorRed))
                    setEditTextTouchListeners()
                }
                //BookAppointmentsViewModel.EMAIL_REQUIRED->emailInput.error=getString(R.string.errorFillField)
                //BookAppointmentsViewModel.CASE_DESCRIPTION_REQUIRED->getString(R.string.errorFillField)
                BookAppointmentsViewModel.RESERVATION_DATE_REQUIRED -> Utils.showLongToast(this, R.string.select_next_next_availabilities)
            }
        }


    }

    private fun bindProfile(healthProvidersProfile: HealthProvidersUIBookInfoImp) {
        Glide.with(this).load(healthProvidersProfile.picture).into(mainProfileImage)
        mainProfileImage.setOnClickListener {
            Navigator.navigate(this, getString(R.string.profileKey), arrayListOf(HealthProvidersProfileConverter.convert(
                    healthProvidersProfile.profileId.toString(), healthProvidersProfile.profileTypeId)),
                    HealthProvidersProfileActivity::class.java
                    , -1)
        }
        if (healthProvidersProfile.name.isEmpty()) {
            mainProfileName.visibility = View.GONE

        } else {
            mainProfileName.text = healthProvidersProfile.name

        }
        if (Profile.isHospitalCategory(healthProvidersProfile.profileTypeId)) {
            if (healthProvidersProfile.serviceSlug != LinksCardsDetails.CONSULTING_SLUG) {
                mainProfileLocation.visibility = VISIBLE
                mainProfileLocation.text = healthProvidersProfile.location?.address
                if (healthProvidersProfile.location != null &&
                        healthProvidersProfile.location?.longitude!!.isNotEmpty() &&
                        healthProvidersProfile.location?.latitude!!.isNotEmpty()) {
                    iv_main_profile_location.visibility = VISIBLE
                    iv_main_profile_location.setOnClickListener {
                        Navigator.navigate(this, getString(R.string.locatioKey), healthProvidersProfile.location!!, MapShowActivity::class.java)
                    }
                }

            }
        } else {
            mainProfileSpeciality.visibility = VISIBLE
            mainProfileSpeciality.text = healthProvidersProfile.specialty
        }

        bindServiceDetails(healthProvidersProfile)
    }

    private fun bindServiceDetails(healthProvidersProfile: HealthProvidersUIBookInfoImp) {
        Log.i("mainProfile", healthProvidersProfile.profileTypeId.toString())
        if (Profile.isHospitalCategory(healthProvidersProfile.profileTypeId)) {
            setProfileInfo(healthProvidersProfile.serviceInfo.profileInfo)
        }
        setServicesInfo(healthProvidersProfile.serviceSlug, healthProvidersProfile.serviceInfo, healthProvidersProfile.profileTypeId,
                healthProvidersProfile.rotators)
        rv_next_next_availabilities.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        if (bookAppointmentsViewModel.selectedNextAvailabilityPosition != -1) {
            switchNextAvailabilitiesOneItemMode()
        } else {
            rv_next_next_availabilities.smoothScrollToPosition(0)
        }

        rv_next_next_availabilities.setHasFixedSize(true)
        rv_next_next_availabilities.isNestedScrollingEnabled = false

        rv_next_next_availabilities.adapter = NextAvailabilitiesAdapter(healthProvidersProfile.serviceInfo.nextAvailabilities,
                this::onNextAvailabilitiesClickListener, this::onNextAvailabilitiesClickListener)


        if (healthProvidersProfile.serviceInfo.nextAvailabilities.isEmpty()) {
            iv_left.visibility = GONE
            iv_right.visibility = GONE
        }

        iv_right.setOnClickListener {

            val firstVisibleItemIndex = (rv_next_next_availabilities.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
            if (firstVisibleItemIndex > 0) {
                (rv_next_next_availabilities.layoutManager as LinearLayoutManager).smoothScrollToPosition(rv_next_next_availabilities, null, firstVisibleItemIndex + 1)
            }

        }

        iv_left.setOnClickListener {

            val firstVisibleItemIndex = (rv_next_next_availabilities.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
            if (firstVisibleItemIndex > 0) {
                (rv_next_next_availabilities.layoutManager as LinearLayoutManager).smoothScrollToPosition(rv_next_next_availabilities, null, firstVisibleItemIndex - 1)
            }

        }

        setupSelectDurationViews()

        if (healthProvidersProfile.rotators.isNotEmpty()) {
            tv_rotator.visibility = View.VISIBLE
            tv_rotator.isSelected = true
            tv_rotator.text = android.text.TextUtils.join(" /  ", healthProvidersProfile.rotators)
        }

    }

    private fun switchNextAvailabilitiesOneItemMode() {
        //rv_next_next_availabilities.smoothScrollToPosition(bookAppointmentsViewModel.selectedNextAvailabilityPosition)
        iv_right.visibility = View.GONE
        iv_left.visibility = View.GONE
        //rv_next_next_availabilities.layoutManager = CustomLinearLayoutManager(this)
    }

    private fun onNextAvailabilitiesClickListener(nextAvailabilitiesParcelable: NextAvailabilitiesParcelable) {

        /*
        if (nextAvailabilitiesParcelable.book_button!!.endpointUrl.isEmpty()) {
            // Utils.showLongToast(this,nextAvailabilitiesParcelable.idNextAvailabilities)
            bookAppointementsViewModel.selectedNextAvailability = nextAvailabilitiesParcelable
            bookAppointementsViewModel.bookInfo.nextAvailabilityId = nextAvailabilitiesParcelable.idNextAvailabilities
            //rv_next_next_availabilities.smoothScrollToPosition()
        } else {
            if(bookAppointementsViewModel.selectedNextAvailability!=null) {
                Navigator.navigate(this,
                        arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)),
                        arrayOf(nextAvailabilitiesParcelable.book_button!!.endpointUrl, bookAppointementsViewModel.bookingInfoUrl),
                        getString(R.string.nextAvailabilitiesKey),
                        arrayListOf(bookAppointementsViewModel.selectedNextAvailability!!),
                        NextAvailabilityActivity::class.java, NEXT_AVALIABILITES_REQUEST)
            }else{
                Navigator.navigate(this,
                        arrayOf(getString(R.string.url_key), getString(R.string.book_url_key)),
                        arrayOf(nextAvailabilitiesParcelable.book_button!!.endpointUrl, bookAppointementsViewModel.bookingInfoUrl),
                        NextAvailabilityActivity::class.java, NEXT_AVALIABILITES_REQUEST)
            }
        }

*/

    }

    private fun onNextAvailabilitiesClickListener(nextAvailabilitiesParcelable: NextAvailabilitiesParcelable, position: Int) {
        bookAppointmentsViewModel.selectedNextAvailabilityPosition = position
        if (nextAvailabilitiesParcelable.book_button!!.endpointUrl.isEmpty()) {
            // Utils.showLongToast(this,nextAvailabilitiesParcelable.idNextAvailabilities)
            bookAppointmentsViewModel.selectedNextAvailability = nextAvailabilitiesParcelable
            nextAvailabilitiesParcelable.idNextAvailabilities?.let {
                bookAppointmentsViewModel.bookInfo.nextAvailabilityId = it
            }
            (rv_next_next_availabilities.adapter as NextAvailabilitiesAdapter).let {
                it.nextAvailablitities[position].isSelectedAfterClick = false
                it.nextAvailablitities[position].buttonText = getString(R.string.you_selected)
                rv_next_next_availabilities.adapter = NextAvailabilitiesAdapter(
                        arrayListOf(it.nextAvailablitities[position]), this::onNextAvailabilitiesClickListener,
                        this::onNextAvailabilitiesClickListener)
            }
            switchNextAvailabilitiesOneItemMode()
            //rv_next_next_availabilities.smoothScrollToPosition()
        } else {
            if (nextAvailabilitiesParcelable.isSelectedAfterClick) {
                if (bookAppointmentsViewModel.selectedNextAvailability != null) {
                    Navigator.navigate(this,
                            arrayOf(getString(R.string.url_key), getString(R.string.book_url_key), getString(R.string.is_open_book)),
                            arrayOf(nextAvailabilitiesParcelable.book_button!!.endpointUrl,
                                    bookAppointmentsViewModel.bookingInfoUrl, false.toString()),
                            getString(R.string.nextAvailabilitiesKey),
                            arrayListOf(bookAppointmentsViewModel.selectedNextAvailability!!),
                            NextAvailabilityActivity::class.java, NEXT_AVALIABILITES_REQUEST)
                } else {
                    Navigator.navigate(this,
                            arrayOf(getString(R.string.url_key), getString(R.string.book_url_key), getString(R.string.is_open_book)),
                            arrayOf(nextAvailabilitiesParcelable.book_button!!.endpointUrl,
                                    bookAppointmentsViewModel.bookingInfoUrl, false.toString()),
                            NextAvailabilityActivity::class.java, NEXT_AVALIABILITES_REQUEST)
                }
            }

        }


        // Utils.showLongToast(this,nextAvailabilitiesParcelable.idNextAvailabilities)

    }

    private fun setServicesInfo(serviceSlug: String, info: LinksCardsDetails.RowsInfo, profileType: Int, rotators: List<String>) {

        /*
        tv_periodic_text.setAnimationListener {
            if(rotators.size>lastPosition) {
                tv_periodic_text.animateText(rotators[lastPosition])
                lastPosition = lastPosition++
            }else{
                lastPosition = 0
            }
        }
        */

//        tv_periodic_text.animateText(rotators[lastPosition])

        //format fees
        FeesFormatter.setFees(info.mainFees, tv_surgery_fees_from, tv_surgery_fees_to,
                info.subMainFees, tv_surgery_fees_national_from, tv_surgery_fees_national_to, iv_fees)

        when (serviceSlug) {
            LinksCardsDetails.SURGERIES_SLUG -> {
                if (profileType == Profile.DOCTOR)
                    showAddressInfo(info.location)

                //showDurationInfo(info.duration)
                iv_icon_service.setImageResource(R.drawable.ic_surgery_fees)
                serviceInfo.text = getString(R.string.information_surgery)
                // setFees(true, Fees(), info.mainFees, info.subMainFees)
                tv_surgery_name.visibility = VISIBLE
                tv_surgery_name.text = info.name
            }
            LinksCardsDetails.CLINICS_SLUG -> {
                if (profileType == Profile.DOCTOR)
                    showAddressInfo(info.location)

                iv_icon_service.setImageResource(R.drawable.ic_appointment_fees)
                serviceInfo.text = getString(R.string.information_clinic)
                //setFees(false, info.fees, info.mainFees, info.subMainFees)
                tv_surgery_name.visibility = VISIBLE
                tv_surgery_name.text = info.name
            }
            LinksCardsDetails.CONSULTING_SLUG -> {
                iv_icon_service.visibility = GONE
                tv_surgery_name.visibility = GONE
                //showDurationInfo(info.duration.toString())
                serviceInfo.text = getString(R.string.information_consulting)
                //setFees(false, info.fees, info.mainFees, MainFees())
            }
        }
    }

    private fun showAddressInfo(location: Location) {

        if (location.address.isNotEmpty()) {
            address_group.visibility = VISIBLE
            tv_address.text = location.address
        }

        if (location.latitude.isNotEmpty() && location.longitude.isNotEmpty()) {
            iv_location.setOnClickListener {
                Navigator.navigate(this, getString(R.string.locatioKey), location, MapShowActivity::class.java)
            }
            iv_location.visibility = VISIBLE
        }
    }

    private fun setProfileInfo(profile: LinksCardsDetails.ProfileInfo) {
        if (profile.profileName.isEmpty()) {
            return
        }

        group_doctor_data.visibility = VISIBLE

        if (profile.profileName.isEmpty()) {
            profileName.visibility = GONE
        } else {
            profileName.text = profile.profileName
        }

        if (profile.profilePicture.url.isEmpty()) {
            profileImage.visibility = GONE
        } else {
            Glide.with(this).load(profile.profilePicture.url).into(profileImage)
            profileImage.setOnClickListener {
                Navigator.navigate(this, getString(R.string.profileKey), arrayListOf(HealthProvidersProfileConverter.convert(
                        profile.profileId, profile.profileType)), HealthProvidersProfileActivity::class.java
                        , -1)
            }
        }

        profileDesc.text = profile.profileMainSpeciality
    }

    private fun setupMaleFemaleButtons() {
        male.setOnClickListener {
            bookAppointmentsViewModel.bookInfo.patientGender = getString(R.string.male)
            updateMaleFemaleButtons(getString(R.string.male))
        }
        female.setOnClickListener {
            bookAppointmentsViewModel.bookInfo.patientGender = getString(R.string.female)
            updateMaleFemaleButtons(getString(R.string.female))
        }
    }

    private fun updateMaleFemaleButtons(gender: String) {
        if (gender == getString(R.string.male)) {
            female.background = ColorDrawable(ContextCompat.getColor(this, R.color.colorTransparent))
            female.setTextColor(ContextCompat.getColor(this, R.color.colorMaleFemaleBtn))
            male.setBackgroundResource(R.drawable.accent_male_female_start_rounded)
            male.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))

        } else {
            female.setBackgroundResource(R.drawable.accent_male_female_end_rounded)
            female.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            male.setTextColor(ContextCompat.getColor(this, R.color.colorMaleFemaleBtn))
            male.background = ColorDrawable(ContextCompat.getColor(this, R.color.colorTransparent))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEXT_AVALIABILITES_REQUEST && resultCode == Activity.RESULT_OK) {
            val passedData = data?.getParcelableExtra<NextAvailabilitiesParcelable>(getString(R.string.nextAvailabilitiesKey))

            if (bookAppointmentsViewModel.selectedNextAvailability?.idNextAvailabilities == passedData?.idNextAvailabilities) {
                return
            }

            bookAppointmentsViewModel.selectedNextAvailability = passedData
            if (passedData?.idNextAvailabilities != null)
                bookAppointmentsViewModel.bookInfo.nextAvailabilityId = passedData.idNextAvailabilities!!

            val adapter = rv_next_next_availabilities.adapter as NextAvailabilitiesAdapter
            adapter.lastSelectedPosition = bookAppointmentsViewModel.selectedNextAvailabilityPosition
            adapter.nextAvailablitities[adapter.lastSelectedPosition].isSelected = true
            adapter.nextAvailablitities[adapter.lastSelectedPosition].isSelectedAfterClick = false
            adapter.nextAvailablitities[adapter.lastSelectedPosition].buttonText = getString(R.string.you_selected)
            adapter.nextAvailablitities[adapter.lastSelectedPosition].from =
                    bookAppointmentsViewModel.selectedNextAvailability!!.from
            adapter.nextAvailablitities[adapter.lastSelectedPosition].to = ""

            rv_next_next_availabilities.adapter = NextAvailabilitiesAdapter(
                    arrayListOf(adapter.nextAvailablitities[adapter.lastSelectedPosition]),
                    this::onNextAvailabilitiesClickListener, this::onNextAvailabilitiesClickListener)


            switchNextAvailabilitiesOneItemMode()

        } else if (requestCode == MAP_REQUEST && resultCode == Activity.RESULT_OK) {
            bookAppointmentsViewModel.bookInfo.location = data?.getParcelableExtra(getString(R.string.selected_map_location))
        } else if (requestCode == MEDIA_REQUEST && resultCode == Activity.RESULT_OK) {
            bookAppointmentsViewModel.bookInfo.surgeriesMedia = data?.getParcelableArrayListExtra(getString(R.string.media_key))
            if (bookAppointmentsViewModel.bookInfo.surgeriesMedia!!.isNotEmpty()) {
                iv_tick.visibility = VISIBLE
            } else {
                iv_tick.visibility = GONE
            }
        }
    }

    companion object {
        private const val NEXT_AVALIABILITES_REQUEST = 1
        private const val MAP_REQUEST = 3
        private const val MEDIA_REQUEST = 4
    }
}
