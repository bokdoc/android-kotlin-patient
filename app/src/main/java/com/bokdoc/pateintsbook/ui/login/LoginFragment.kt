package com.bokdoc.pateintsbook.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.utils.LoginSignUpSpannable
import com.bokdoc.pateintsbook.utils.validate.Validator
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.fragment_login.*
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat.getDrawable
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.navigators.RegisterNavigator
import com.bokdoc.pateintsbook.ui.forgetPassword.ForgetMobileActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.register.RegisterFragment
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.facebook.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LoginFragment : Fragment(), GoogleApiClient.OnConnectionFailedListener {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var callbackManager: CallbackManager
    private lateinit var mTwitterAuthClient: TwitterAuthClient
    private lateinit var mGoogleApiClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        loginViewModel =
                ViewModelProviders.of(this, CustomLoginModel(activity!!))
                        .get(LoginViewModel::class.java)
        callbackManager = CallbackManager.Factory.create()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPrefrenceManger = SharedPreferencesManagerImpl(activity!!)

        setupLoginFacebook()
        setupTwitter()
        setupGmail()
        alreadyHaveAccount.movementMethod = LinkMovementMethod.getInstance()

        alreadyHaveAccount.text = LoginSignUpSpannable.getClickableSpannable(alreadyHaveAccount.text.toString(),
                getString(R.string.signUp), ContextCompat.getColor(activity!!, R.color.colorHomeCardsYellowBorder)) {
            val loginFragment = RegisterFragment.newInstance("", "")
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
            transaction.replace(R.id.lay_main_container, loginFragment)
            transaction.commit()
            //       RegisterNavigator.navigateWithExits(activity!!)
        }

        iv_show_password.setOnClickListener {
            when (iv_show_password.tag) {
                SHOW_TAG -> {
                    iv_show_password.tag = NO_SHOW_TAG
                    passwordEdit.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    iv_show_password.setImageResource(R.drawable.ic_show_password)
                    passwordEdit.setSelection(passwordEdit.getText().length)
                }
                NO_SHOW_TAG -> {
                    iv_show_password.tag = SHOW_TAG
                    passwordEdit.transformationMethod = PasswordTransformationMethod.getInstance()
                    iv_show_password.setImageResource(R.drawable.ic_hide_password)
                    passwordEdit.setSelection(passwordEdit.getText().length)

                }
            }
        }

        login.setOnClickListener {
            if (checkEmailAndPassword()) {
                showProgress(View.VISIBLE)
                loginViewModel.login(emailOrMobileEdit.text.toString(),
                        passwordEdit.text.toString()).observe(activity!!,
                        Observer<DataResource<UserResponse>> {
                            showProgress(View.GONE)
                            if (it?.status == DataResource.SUCCESS) {
                                if (activity != null)
                                    LocaleManager.setLocale(activity?.baseContext!!)
                                if (it.meta.message.isNotEmpty()) {
                                    if (context != null)
                                        Utils.showLongToast(context!!, it.meta.message)
                                }

                                Navigator.navigate(activity!!, MainActivity::class.java)
                                activity!!.finishAffinity()

                            } else {
                                it?.message?.let {message->
                                    if(message.isNotEmpty()){
                                        if(activity!=null)
                                        Utils.showLongToast(activity!!,message)
                                    }else{
                                        if(activity!=null)
                                        Utils.showLongToast(activity!!,R.string.server_error)
                                    }
                                }
                            }
                        })
            }
        }

        forgetPassword.setOnClickListener {
            goToForgerPassword()

        }

        facebookLoginText.setOnClickListener {
            facebookLoginButton.performClick()
        }

        googleLoginText.setOnClickListener {
            signInGmail()
        }

        twitterLoginText.setOnClickListener {
            twitterLoginButton.performClick()
        }
        setupHintTextColors()
        setupEditTextsWatchers()
    }

    private fun setupHintTextColors() {
        if (activity == null)
            return

        // emailOrMobileEdit.hint = getString(R.string.emailMobile)

        //  passwordEdit.hint = getString(R.string.password)
    }

    private fun setupEditTextsWatchers() {
        emailOrMobileEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                emailOrMobileEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                setupHintTextColors()
            }

        })
        passwordEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordEdit.setHintTextColor(ContextCompat.getColor(activity?.applicationContext!!, R.color.colorLoginHint))
                setupHintTextColors()
            }
        })
    }

    private fun goToForgerPassword() {
        Navigator.navigate(activity!!, ForgetMobileActivity::class.java)
    }

    fun showProgress(visibility: Int) {
        login_progress.visibility = visibility
        if (visibility == VISIBLE) {
            activity!!.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    private fun setupLoginFacebook() {
        facebookLoginButton.fragment = this
        facebookLoginButton.setReadPermissions(listOf(EMAIL))
        facebookLoginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                // loginViewModel.loginWithSocials(getString(R.string.facebook))
                showProgress(VISIBLE)
                val graphRequest = GraphRequest.newMeRequest(
                        result?.accessToken, object : GraphRequest.GraphJSONArrayCallback, GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(jsonObject: JSONObject?, response: GraphResponse?) {
                        try {

                            sendLoginRequest(result?.accessToken?.token, result?.accessToken?.userId,
                                    jsonObject?.optString("name"), jsonObject?.optString("email"),
                                    getFacebookProfilePicture(jsonObject?.optString("id")!!))
                        } catch (e: Exception) {

                        }
                    }

                    override fun onCompleted(objects: JSONArray?, response: GraphResponse?) {
                        Log.i("", "")
                    }

                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email")
                graphRequest.parameters = parameters
                graphRequest.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {

            }

        })
    }

    private fun getFacebookProfilePicture(userId: String): String {
        return URL("https://graph.facebook.com/$userId/picture?width=500&height=500").toString()
    }

    private fun sendLoginRequest(accessToken: String?, providerId: String?, name: String?, email: String?, picture: String?, secretKey: String = "") {
        if (accessToken != null && providerId != null && name != null && picture != null && email != null) {
            loginViewModel.loginWithSocials(getString(R.string.facebook), providerId, accessToken, secretKey, name, picture, email
            ).observe(activity!!, Observer<DataResource<UserResponse>> {
                showProgress(GONE)
                if (it?.status == DataResource.SUCCESS) {

                    if (activity != null)
                        LocaleManager.setLocale(activity?.baseContext!!)

                    if (it.meta.message.isNotEmpty()) {
                        if (context != null)
                            Utils.showLongToast(context!!, it.meta.message)
                    }

                    Navigator.navigate(activity!!, MainActivity::class.java)
                    activity!!.finishAffinity()

                } else {
                    if (context != null) {
                        if (!it?.meta?.message.isNullOrEmpty()) {
                            Utils.showLongToast(context!!, it?.meta!!.message)
                        } else {
                            Utils.showLongToast(context!!,R.string.server_error)
                        }
                    }
                }
            })
        }
    }

    private fun setupTwitter() {
        twitterLoginButton.callback = object : Callback<TwitterSession>() {

            override fun success(result: Result<TwitterSession>?) {

                showProgress(VISIBLE)
                if (result == null) {
                    showProgress(GONE)
                    Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                    return
                }

                TwitterAuthClient().requestEmail(result?.data, object : com.twitter.sdk.android.core.Callback<String>() {
                    override fun success(emailResult: Result<String>) {
                        val email = emailResult.data
                        if (email != null && result.data?.authToken != null) {
                            loginViewModel.loginWithSocials(getString(R.string.twitter), result.data?.userId.toString(),
                                    result.data?.authToken!!.token, result.data?.authToken!!.secret, result.data.userName, "", email)
                                    .observe(activity!!, Observer<DataResource<UserResponse>> {
                                        showProgress(GONE)
                                        if (it?.status == DataResource.SUCCESS) {
                                            if (it.meta.message.isNotEmpty()) {
                                                if (context != null)
                                                    Utils.showLongToast(context!!, it.meta.message)
                                            }

                                            if (activity != null)
                                                LocaleManager.setLocale(activity?.baseContext!!)

                                            Navigator.navigate(activity!!, MainActivity::class.java)
                                            activity!!.finishAffinity()


                                        } else {
                                            if (context != null) {
                                                if (!it?.meta?.message.isNullOrEmpty()) {
                                                    Utils.showLongToast(context!!, it?.meta!!.message)
                                                } else {
                                                    Utils.showLongToast(context!!,R.string.server_error)
                                                }
                                            }
                                        }
                                    })
                        } else {
                            showProgress(GONE)
                            Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                        }

                    }

                    override fun failure(e: TwitterException) {
                        showProgress(GONE)
                        Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
                    }
                })


            }

            override fun failure(exception: TwitterException?) {
                showProgress(GONE)
                Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
            }

        }

    }

    private fun setupGmail() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_client_server_id))
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleSignIn.getClient(activity!!, gso)

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private fun signInGmail() {
        val signInIntent = mGoogleApiClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkEmailAndPassword(): Boolean {
        var isValid = true
        val emailOrMobile = emailOrMobileEdit.text.toString()
        if (emailOrMobile.isEmpty()) {
            emailOrMobileInput.error = getString(R.string.errorFillField)
            isValid = false
            if (activity != null) {
                emailOrMobileEdit.setHintTextColor(resources.getColor(R.color.colorErrorRed))
            }
        }

        /*
        if (emailOrMobile.isNotEmpty()) {
            if (!Validator.validateEmail(emailOrMobile)) {
                if (activity != null)
                    Utils.showLongToast(activity?.applicationContext!!, getString(R.string.empty_phone))
                isValid = false
            }
        }
        */

        if (passwordEdit.text.isEmpty()) {
            isValid = false
            //passwordEdit.error = getString(R.string.errorFillField)
            passwordEdit.setHintTextColor(resources.getColor(R.color.colorErrorRed))
        }
        return isValid
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task?.getResult(ApiException::class.java)
                showProgress(VISIBLE)
                loginViewModel.loginWithSocials(getString(R.string.google), account?.id!!,
                        account.idToken!!, "", account.displayName!!,
                        account.photoUrl?.toString()!!, account.email!!).observe(activity!!,
                        Observer<DataResource<UserResponse>> {
                            if (it?.status == DataResource.SUCCESS) {
                                if (it.meta.message.isNotEmpty()) {
                                    if (context != null)
                                        Utils.showLongToast(context!!, it.meta.message)
                                }

                                if (activity != null)
                                    LocaleManager.setLocale(activity?.baseContext!!)

                                Navigator.navigate(activity!!, MainActivity::class.java)
                                activity!!.finish()
                            } else {
                                if (context != null) {
                                    if (!it?.meta?.message.isNullOrEmpty()) {
                                        Utils.showLongToast(context!!, it?.meta!!.message)
                                    } else {
                                        Utils.showLongToast(context!!,R.string.server_error)
                                    }
                                }
                            }
                        })
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                // ...
                Toast.makeText(activity!!, getString(R.string.errorSent), Toast.LENGTH_LONG).show()
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data)
        twitterLoginButton.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                LoginFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        const val EMAIL = "email"
        const val PUBLIC_PROFILE = "public_profile"
        const val RC_SIGN_IN = 0
        const val SHOW_TAG = "show"
        const val NO_SHOW_TAG = "notShow"
    }
}
