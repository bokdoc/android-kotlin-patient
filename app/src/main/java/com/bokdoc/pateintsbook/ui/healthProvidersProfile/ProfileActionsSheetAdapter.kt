package com.bokdoc.pateintsbook.ui.healthProvidersProfile

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_image_text_row.view.*

class ProfileActionsSheetAdapter(private val items: List<ImageTextRow>,
                                 var itemCLickListener: (item: ImageTextRow) -> Unit) : RecyclerView.Adapter<ProfileActionsSheetAdapter.ActionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ActionsViewHolder {
        return ActionsViewHolder(parent.inflateView(R.layout.item_image_text_row))

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ActionsViewHolder, position: Int) {
        items[position].let {
            holder.itemView.text.text = it.text
            holder.itemView.image.setImageResource(it.image)

        }
    }


    inner class ActionsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                itemCLickListener.invoke(items[adapterPosition])
            }
        }
    }
}