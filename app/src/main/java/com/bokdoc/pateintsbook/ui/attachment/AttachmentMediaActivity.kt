package com.bokdoc.pateintsbook.ui.attachment

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.TabLayout
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.imagesViewer.ImagesViewerActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import com.bokdoc.pateintsbook.utils.ui.ProgressBarDialog
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.obsez.android.lib.filechooser.ChooserDialog
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.activity_attachment_media.*
import kotlinx.android.synthetic.main.toolbar.*

class AttachmentMediaActivity : ParentActivity() {

    private lateinit var attachmentMediaViewModel: AttachmentMediaViewModel
    private lateinit var attachmentMediaAdapter: AttachmentMediaAdapter
    private var progressBarDialog: ProgressBarDialog? = null
    private var departmentRow = DepartmentRow()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attachment_media)
        enableBackButton()
        setTitle(R.string.addAttachment)
        getViewModel()

        mediaList.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.attachment_media_span_count),
                GridLayoutManager.VERTICAL, false)

        returnArrowClickListener = View.OnClickListener {
            onBackPressed()
        }

        if (intent?.hasExtra(getString(R.string.is_show_only))!!) {
            attachmentMediaViewModel.isShowOnly = intent?.getStringExtra(getString(R.string.is_show_only))!!.toBoolean()
            if (attachmentMediaViewModel.isShowOnly) {
                setTitle(R.string.media)
                done.visibility = View.GONE
                mediaTabs.visibility = View.GONE
            }
        }


        if (intent?.hasExtra(getString(R.string.media_key))!!) {
            setUpAddMediaButton()
            rightTextView.visibility = View.VISIBLE
            if (attachmentMediaViewModel.isShowOnly) {
                rightTextView.text = intent?.getParcelableArrayListExtra<MediaParcelable>(getString(R.string.media_key))!!.size.toString() +
                        " " + getString(R.string.picture)
            }
            attachmentMediaViewModel.loadFromMedia(intent?.getParcelableArrayListExtra(getString(R.string.media_key))!!)
        }



        if (intent?.hasExtra(getString(R.string.idKey))!!) {
            attachmentMediaViewModel.isShowOnly = true
            setUpAddMediaButton()
            switchBgProgressVisibility(VISIBLE)
            attachmentMediaViewModel.getUserMedia().observe(this, Observer {
                switchBgProgressVisibility(GONE)
                if (it?.status == DataResource.SUCCESS && it.data != null) {
                    //Utils.showLongToast(this, "Media" + it.data!!.size.toString())
                    attachmentMediaViewModel.loadFromMedia(it.data as List<MediaParcelable>)
                    attachmentMediaAdapter = AttachmentMediaAdapter(attachmentMediaViewModel.getAllImagesFiles(),
                            this::onAdapterClickListener, attachmentMediaViewModel.isShowOnly)
                } else {
                    if (it?.errors!!.isNotEmpty()) {
                        Utils.showLongToast(this, it.errors[0].detail)
                    }
                }
            })
        } else {
            attachmentMediaAdapter = AttachmentMediaAdapter(attachmentMediaViewModel.getAllImagesFiles(), this::onAdapterClickListener,
                    attachmentMediaViewModel.isShowOnly)
            mediaList.adapter = attachmentMediaAdapter
        }

        if (intent?.hasExtra(getString(R.string.department_key))!!) {
            title = getString(R.string.media)
            attachmentMediaViewModel.isShowOnly = true
            done.visibility = GONE
            mediaTabs.visibility = GONE
            rightTextView.visibility = GONE
            progress.visibility = View.VISIBLE

            departmentRow = intent?.getParcelableExtra(getString(R.string.department_key))!!
            attachmentMediaViewModel.mediaUrl = departmentRow.showUrl + ",singleDepartment.media"
            attachmentMediaViewModel.showMedia().observe(this, Observer {
                if (it?.status == DataResource.SUCCESS && it.data != null) {
                    progress.visibility = GONE
                    when (it.data!!.isEmpty()) {
                        true -> {
                            showEmpty("No Media Available", "", null)
                        }
                        false -> {
                            attachmentMediaViewModel.loadFromMedia(it.data as List<MediaParcelable>)
                            attachmentMediaAdapter = AttachmentMediaAdapter(attachmentMediaViewModel.getAllImagesFiles(),
                                    this::onAdapterClickListener, attachmentMediaViewModel.isShowOnly)
                            mediaList.adapter = attachmentMediaAdapter
                        }

                    }

                }
            })
        }

        setupTabs()
        setupObservers()
        done.setOnClickListener(this::onClickListener)

        if (attachmentMediaAdapter.itemCount == 0) {
            done.visibility = GONE
        }

    }


    private fun setUpAddMediaButton() {
        if (!attachmentMediaViewModel.isShowOnly) {
            rightTextView.visibility = VISIBLE
            rightTextView.text = getString(R.string.addMedia)
            rightTextView.setOnClickListener(this::onClickListener)
        }
    }

    private fun getViewModel() {
        attachmentMediaViewModel = InjectionUtil.getAttachmentMediaViewModel(this)
    }

    private fun setupObservers() {
        attachmentMediaViewModel.uriFilesLiveData.observe(this, Observer {
            attachmentMediaAdapter.update(it!!)
        })

        attachmentMediaViewModel.uriImagesLiveData.observe(this, Observer {
            attachmentMediaAdapter.update(it!!)
        })

        attachmentMediaViewModel.errorMessageLiveData.observe(this, Observer {
            if (progressBarDialog != null) {
                progressBarDialog?.stopProgressWithError()
            } else {
                Utils.showLongToast(this, it!!)
            }
        })

        attachmentMediaViewModel.uploadProgressLiveData.observe(this, Observer {
            runOnUiThread {
                progressBarDialog?.updateProgress(it!!)
            }
        })
        attachmentMediaViewModel.dataLiveData.observe(this, Observer {
            progressBarDialog?.dismiss()
            Navigator.naviagteBack(this, getString(R.string.media_key), (it as ArrayList<MediaParcelable>))
        })

        attachmentMediaViewModel.messageLiveData.observe(this, Observer {
            if (it != null && it.isNotEmpty())
                Utils.showLongToast(this, it)
        })
    }


    private fun setupTabs() {
        mediaTabs.addTab(mediaTabs.newTab().setText(R.string.media))
        mediaTabs.addTab(mediaTabs.newTab().setText(R.string.files))
        mediaTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == MEDIA_TAB) {
                    if (!attachmentMediaViewModel.isShowOnly) {
                        rightTextView.text = getString(R.string.addMedia)
                    }
                    attachmentMediaAdapter.viewType = MEDIA_TAB
                    attachmentMediaAdapter.update(attachmentMediaViewModel.getAllImagesFiles())
                } else {
                    if (!attachmentMediaViewModel.isShowOnly) {
                        rightTextView.text = getString(R.string.addFiles)
                    }
                    attachmentMediaAdapter.viewType = FILES_TAB
                    attachmentMediaAdapter.update(attachmentMediaViewModel.getAllDocFiles())
                }
            }
        })
    }

    private fun onAdapterClickListener(uris: ArrayList<Uri>, action: Int, position: Int) {
        if (attachmentMediaAdapter.isDeleteImageAction(action)) {
            val uri = uris[position]
            if (mediaTabs.selectedTabPosition == MEDIA_TAB) {
                attachmentMediaAdapter.removeUri(position)
                attachmentMediaViewModel.remove(uri, AttachmentMediaViewModel.IMAGE_URI)
            } else {
                attachmentMediaAdapter.removeUri(position)
                attachmentMediaViewModel.remove(uri, AttachmentMediaViewModel.FILES_URI)
            }
            if (attachmentMediaAdapter.itemCount == 0 && attachmentMediaViewModel.urisImagesUploaded.isEmpty()
                    && attachmentMediaViewModel.urisFilesUploaded.isEmpty() && attachmentMediaViewModel.urisImages.isEmpty() && attachmentMediaViewModel.urisFiles.isEmpty()) {
                done.visibility = GONE
            }
        } else {
            Navigator.navigate(this, arrayOf(getString(R.string.selected_position)),
                    arrayOf(position.toString()), getString(R.string.images), uris,
                    ImagesViewerActivity::class.java, -1)
        }
    }

    private fun onClickListener(view: View) {
        when (view.id) {
            R.id.rightTextView -> {
                val filePicker = FilePickerBuilder.getInstance()
                filePicker.setActivityTheme(R.style.LibAppTheme)
                filePicker.setActivityTitle(getString(R.string.addAttachment))
                filePicker.enableCameraSupport(true)
                filePicker.enableSelectAll(true)
                if (mediaTabs?.selectedTabPosition == MEDIA_TAB) {
                    rightTextView.text = getString(R.string.addMedia)
                    pickImage()
                    /*
                    filePicker
                            .setSelectedFiles(attachmentMediaViewModel.getPaths(attachmentMediaViewModel.urisImages.toTypedArray()))
                            .pickPhoto(this)
                            */
                } else {
                    rightTextView.text = getString(R.string.addFiles)
                    /*
                    filePicker
                            .setSelectedFiles(attachmentMediaViewModel.getPaths(attachmentMediaViewModel.urisFiles.toTypedArray()))
                            .addFileSupport(getString(R.string.onlySupported), attachmentMediaViewModel.getSupportedFilesTypes())
                            .pickFile(this)
                            */
                    pickFile()
                }
            }
            R.id.done -> {
                if (attachmentMediaViewModel.isUrisFound()) {
                    /*
                    content?.visibility = GONE
                    progressLayout.visibility = VISIBLE
                    */
                    progressBarDialog = ProgressBarDialog(this, {
                        attachmentMediaViewModel.cancelUpload()
                    }, {
                        attachmentMediaViewModel.uploadFiles()
                    })
                    progressBarDialog?.setCanceledOnTouchOutside(false)
                    progressBarDialog?.setCancelable(false)
                    progressBarDialog?.show()
                    attachmentMediaViewModel.uploadFiles()
                }else{
                    onBackPressed()
                }
            }
            R.id.returnArrow -> {
                onBackPressed()
            }
        }
    }

    private fun pickImage() {
        val imagePicker = ImagePicker.create(this)
                .returnMode(ReturnMode.NONE) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .multi() // multi mode (default mode)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .enableLog(false) // disabling log // start image picker activity with request code


        /*
        if (attachmentMediaViewModel.imagesMax.isNotEmpty()) {
            imagePicker.limit(attachmentMediaViewModel.imagesMax.toInt())
        }
        */

        imagePicker.start()

        /*
        if (mediaTabs?.selectedTabPosition == MEDIA_TAB) {
            rightTextView.text = getString(R.string.addMedia)
            filePicker
                    .setSelectedFiles(attachmentMediaViewModel.getPaths(attachmentMediaViewModel.urisImages.toTypedArray()))
                    .pickPhoto(this)
        } else {
            rightTextView.text = getString(R.string.addFiles)
            filePicker
                    .setSelectedFiles(attachmentMediaViewModel.getPaths(attachmentMediaViewModel.urisFiles.toTypedArray()))
                    .addFileSupport(getString(R.string.onlySupported), attachmentMediaViewModel.getSupportedFilesTypes())
                    .pickFile(this)
        }
        */
    }

    private fun pickFile() {
        ChooserDialog(this)
                .withFilter(false, false, "pdf")
                .withResources(R.string.choose_file, R.string.title_choose, R.string.dialog_cancel)
                .withChosenListener { path, file ->
                    attachmentMediaViewModel.addPaths(arrayListOf(path), AttachmentMediaViewModel.FILES_URI)
                    done.visibility = VISIBLE
                    // progressBarDialog?.setCanceledOnTouchOutside(false)
                    // progressBarDialog?.setCancelable(false)
                    // progressBarDialog?.show()
                    //attachmentMediaViewModel.u
                }
                .build()
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images = ImagePicker.getImages(data)
            attachmentMediaViewModel.addImagesPaths(images, AttachmentMediaViewModel.IMAGE_URI)

            /*
            if (attachmentMediaViewModel.imagesMax.isNotEmpty() && images.isNotEmpty()) {
                rightTextView.visibility = GONE
            }
            */

            done.visibility = VISIBLE

            // attachmentMediaViewModel.uploadFile(images[0].path)
            // or get a single image only
        }
        when (requestCode) {
            FilePickerConst.REQUEST_CODE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val paths = data?.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)
                    if (paths != null) {
                        attachmentMediaViewModel.addPaths(paths, AttachmentMediaViewModel.IMAGE_URI)
                        if (paths.isNotEmpty())
                            done.visibility = VISIBLE
                    }
                }
            }
            FilePickerConst.REQUEST_CODE_DOC -> {
                if (resultCode == Activity.RESULT_OK) {
                    val paths = data?.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)
                    if (paths != null)
                        attachmentMediaViewModel.addPaths(paths, AttachmentMediaViewModel.FILES_URI)

                    if (paths != null && paths.isNotEmpty())
                        done.visibility = VISIBLE
                }
            }
        }
    }

    override fun onBackPressed() {
        Navigator.naviagteBack(this, getString(R.string.media_key), attachmentMediaViewModel.passedData as ArrayList<out Parcelable>)
    }


    interface OnProgressListener {
        fun onProgress(progress: Double)
    }


    companion object {
        const val MEDIA_TAB = 0
        const val FILES_TAB = 1
    }
}
