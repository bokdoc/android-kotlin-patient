package com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.clinics.ClinicsRepository

class ClinicsViewModelProvider(private val clinicsRepository: ClinicsRepository):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClinicsViewModel(clinicsRepository) as T
    }
}