package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.repositories.attachmentMedia.AttachmentsMediaRepository
import com.bokdoc.pateintsbook.data.repositories.favoritesRepository.FavoritesRepository
import com.bokdoc.pateintsbook.data.repositories.patientCompareRepository.CompareRepository
import com.bokdoc.pateintsbook.data.repositories.requests.RequestsRepository

class PatientProfileRepository(val myFavoritesRepository: FavoritesRepository,
                               val myRequestsRepository: RequestsRepository,
                               val compareRepository: CompareRepository,
                               val attachmentsMediaRepository: AttachmentsMediaRepository) {


    fun getMyFavorites():LiveData<DataResource<HealthProvidersProfile>>{
        val liveData = MutableLiveData<DataResource<HealthProvidersProfile>>()
        val dataResource = DataResource<HealthProvidersProfile>()

        myFavoritesRepository.getPatientFavorites().observeForever {
            if(it?.data!=null){
                dataResource.data = HealthProvidersProfileConverter.convert((it.data!!))
            }
            liveData.value = dataResource
        }

        return liveData
    }

    fun getCompares():LiveData<DataResource<HealthProvidersProfile>>{
        val liveData = MutableLiveData<DataResource<HealthProvidersProfile>>()
        val dataResource = DataResource<HealthProvidersProfile>()

        compareRepository.getCompares().observeForever {
            if(it?.data!=null){
                dataResource.data = HealthProvidersProfileConverter.convert((it.data!!))
            }
            liveData.value = dataResource
        }

        return liveData
    }

    fun getRequests():LiveData<DataResource<HealthProvidersProfile>>{
        val liveData = MutableLiveData<DataResource<HealthProvidersProfile>>()
        val dataResource = DataResource<HealthProvidersProfile>()

        myRequestsRepository.getRequests("",1).observeForever {
            if(it?.data!=null){
                dataResource.data = HealthProvidersProfileConverter.convertRequests((it.data!!))
            }
            liveData.value = dataResource
        }

        return liveData
    }

    fun getMedia():LiveData<DataResource<MediaParcelable>> {
        val liveData = MutableLiveData<DataResource<MediaParcelable>>()
        attachmentsMediaRepository.getUserMedia().observeForever {
            liveData.value = it as DataResource<MediaParcelable>
        }
        return liveData
    }

    fun getId(): String {
        return myFavoritesRepository.userSharedPreference.getId()
    }

}