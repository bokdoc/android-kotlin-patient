package com.bokdoc.pateintsbook.ui.searchResultsMap

import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.google.android.gms.maps.model.LatLng

class SearchMapsViewModel : ViewModel() {
    lateinit var profiles: ArrayList<Profile>
    var locationsProfiles: java.util.ArrayList<LatLng>? = null

    fun getLocations(): ArrayList<LatLng>? {

        if (locationsProfiles == null) {
            val size = profiles.size
            locationsProfiles = ArrayList()
            (0 until size).forEach {
                profiles[it].location.let {
                    if (it.latitude.isNotEmpty() && it.longitude.isNotEmpty()) {
                        locationsProfiles?.add(LatLng(it.latitude.toDouble(), it.longitude.toDouble()))
                    }
                }
            }
        }
        return locationsProfiles
    }

    fun getProfileAt(position: Int): Profile {
        return profiles[position]
    }

    fun getMarkerImage(profileType: Int): Int {
        return when (profileType) {
            Profile.HOSPITAL -> return R.drawable.ic_marker_hospital
            Profile.POL_CLINIC -> return R.drawable.ic_marker_polyclinic
            Profile.DOCTOR -> return R.drawable.ic_marker_center
            Profile.CENTER -> return R.drawable.ic_marker_doctor
            Profile.LAB -> return R.drawable.ic_marker_lab
            Profile.RAD -> return R.drawable.ic_marker_rad
            else -> -1
        }
    }
}