package com.bokdoc.pateintsbook.ui.common.profileActions

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.webservice.models.Profile

open class ProfileActionsViewModel(private val profileSocialActionsRepository: ProfileSocialActionsRepository):ViewModel() {

    lateinit var profile: HealthProvidersProfile

    var likedToggleLiveDate: MutableLiveData<Int> = MutableLiveData()
    var favouritesToggleLiveDate: MutableLiveData<Int> = MutableLiveData()
    var comparedToggleLiveData: MutableLiveData<Int> = MutableLiveData()

    val errorMessageLiveDate = MutableLiveData<Int>()

    fun toggleLike(){
        profileSocialActionsRepository.toggleLike(profile.id).observeForever {
            if(it?.status == DataResource.SUCCESS){
                profile.isLiked = profile.isLiked.not()
                if(profile.isLiked) {
                    likedToggleLiveDate.value = R.string.successliked
                }else{
                    likedToggleLiveDate.value = R.string.successDisliked
                }
            }else{
                errorMessageLiveDate.value = R.string.errorNotSaved
            }
        }
    }

    fun toggleFavorite(){
        profileSocialActionsRepository.toggleFavorite(profile.id).observeForever {
            if(it?.status == DataResource.SUCCESS){
                profile.isFavourite = profile.isFavourite.not()
                if(profile.isFavourite) {
                    favouritesToggleLiveDate.value = R.string.successAddedFavorite
                }else{
                    favouritesToggleLiveDate.value = R.string.successRemovedFavorite
                }
            }else{
                errorMessageLiveDate.value = R.string.errorNotSaved
            }
        }
    }

    fun toggleCompare(){
        profile.isCompare = profile.isCompare.not()
        if(profile.isCompare){
            profileSocialActionsRepository.toggleCompare(profile.id)
            comparedToggleLiveData.value = R.string.successAddedCompare
        }else{
            profileSocialActionsRepository.toggleCompare(profile.id,false)
            comparedToggleLiveData.value = R.string.successRemovedCompare
        }
    }

    fun saveSelectedCompare(){
        profileSocialActionsRepository.saveCompares()
    }


    fun getSocialMenuRows(context: Context):List<ImageTextRow>{
        return profileSocialActionsRepository.getProfileActionsMenu(context,profile)
    }

    fun getSharableProfileUrl():String{
        return profileSocialActionsRepository.getShareableProfileUrl(profile)
    }

    /*
    fun getProfileFromUri(uri: Uri):HealthProvidersProfile{
      //  profile = profileSocialActionsRepository.getShareableProfile(uri)
        return profile
    }
    */

    fun setProfile(profile:Profile){
        this.profile = profileSocialActionsRepository.convertToProfile(profile)
    }

}