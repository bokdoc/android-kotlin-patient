package com.bokdoc.pateintsbook.ui.healthProvidersProfile.services

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.ui.parents.ParentActivity

class ServicesActivity : ParentActivity() {

   // private lateinit var servicesViewModel: ServicesViewModel
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)
        //servicesViewModel = ViewModelProviders.of(this,ServicesViewModelProvider())
    }

    fun getServicesTitle(): String{
        return ""
    }

    fun getServicesList():List<Service>{
        return ArrayList()
    }


}
