package com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_hospital_department.view.*

class DepartmentsAdapter(private val departments: List<DepartmentRow>,
                         private val onDepartmentClickListner: (DepartmentRow) -> Unit,
                         private val onMediaClickListener: (DepartmentRow) -> Unit,
                         private val onBookClickListener: (DepartmentRow) -> Unit)
    : RecyclerView.Adapter<DepartmentsAdapter.DepartmentsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentsViewHolder {
        return DepartmentsViewHolder(parent.inflateView(R.layout.item_hospital_department))
    }

    override fun getItemCount(): Int {
        return departments.size
    }

    override fun onBindViewHolder(holder: DepartmentsViewHolder, position: Int) {
        departments[position].let {
            holder.itemView.tv_title.text = it.name
            holder.itemView.tv_department_desc.text = it.description
            holder.itemView.tv_doc_count.text = holder.itemView.context.getString(R.string.allDoctorsPlaceHolder, it.doctorCount)
            if (it.bookButton != null) {
                holder.itemView.btn_department_book.visibility = View.VISIBLE
            }
        }

    }

    inner class DepartmentsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {

            view.view_doctors.setOnClickListener {
                onDepartmentClickListner.invoke(departments[adapterPosition])
            }
            view.view_media.setOnClickListener {
                onMediaClickListener.invoke(departments[adapterPosition])
            }
            view.btn_department_book.setOnClickListener {
                onBookClickListener.invoke(departments[adapterPosition])
            }

        }
    }
}