package com.bokdoc.pateintsbook.ui.homeMenuList.dialog


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager

import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdate
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.tutorials.TutorialsActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_list_more.*
import kotlinx.android.synthetic.main.popup_dialog.*

class UpdateDialog(val activityContext: Context, val checkUpdate: CheckUpdate,
                   val onViewClickListener:(String)->Unit={}) :
        Dialog(activityContext), View.OnClickListener {

    val userSharedPreferences = UserSharedPreference(context)
    var moreUrl = ""

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.attributes.windowAnimations = R.style.DialogTransitions
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            btn_update.id -> {
                Utils.openAppInPlayStore(context)
            }
            btn_logout.id -> {
                onViewClickListener.invoke(LOGOUT)
                /*
                homeMenuListViewModel.logout().observe(this, Observer {
                    view_progress.visibility = View.GONE
                    if (it?.status == DataResource.SUCCESS) {
                        if(activity!=null) {
                            LocaleManager.setLocale(activity!!.baseContext)
                            Navigator.navigate(activity!!, MainActivity::class.java)
                            activity!!.finishAffinity()
                        }
                    }
                    it?.message?.let {
                        if (it.isNotEmpty())
                            Utils.showLongToast(activity!!.applicationContext, it)
                    }
                })
                userSharedPreferences.userLogOut()
                Navigator.navigate(activityContext as Activity, TutorialsActivity::class.java)
                activityContext.finish()
                */

            }
            btn_more.id -> {
                Navigator.navigate(activityContext as Activity, arrayOf(context.getString(R.string.title_key),
                        context.getString(R.string.url_key)), arrayOf(context.getString(R.string.app_name), moreUrl), WebViewActivity::class.java)
            }
            btn_skip.id -> {
                dismiss()
            }

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.popup_dialog)
        setCancelable(false)
        tv_popup_title.text = checkUpdate.title
        tv_popup_description.text = checkUpdate.conent
        Glide.with(context).load(checkUpdate.imageUrl).into(iv_popup)

        btn_update.setOnClickListener(this)
        btn_logout.setOnClickListener(this)
        btn_more.setOnClickListener(this)
        btn_skip.setOnClickListener(this)

        if (checkUpdate.buttons.isNotEmpty()) {
            checkUpdate.buttons.forEach {
                when (it.action) {
                    UPDATE -> {
                        btn_update.visibility = View.VISIBLE
                        btn_update.text = it.label
                    }
                    MORE -> {
                        btn_more.visibility = View.VISIBLE
                        btn_more.text = it.label

                        moreUrl = it.url
                    }
                    LOGOUT -> {
                        btn_logout.visibility = View.VISIBLE
                        btn_logout.text = it.label
                    }
                    SKIP -> {
                        btn_skip.visibility = View.VISIBLE
                        btn_skip.text = it.label
                    }
                }
            }
        }

    }

    override fun show() {
        super.show()
        // customize the dialog width
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(window!!.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        window!!.attributes = layoutParams
    }

    companion object {
        const val UPDATE = "update"
        const val MORE = "more"
        const val LOGOUT = "logout"
        const val SKIP = "skip"
    }

}