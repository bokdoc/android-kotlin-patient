package com.bokdoc.pateintsbook.ui.register

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.login.UserLoginRepository
import com.bokdoc.pateintsbook.data.repositories.register.UserRegisterRepository
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse

class RegisterViewModel(val context: Context):ViewModel() {
    private var registerRepository = UserRegisterRepository(context)

    fun register(email:String,password:String,phone:String,name:String):LiveData<DataResource<UserResponse>>{
        return registerRepository.register(email,password,phone,name)
    }

    fun registerWithSocials(providerName: String, providerId: String, token: String,
                         secret:String="",name:String="",picture:String="",email:String=""): LiveData<DataResource<UserResponse>> {
        return registerRepository.registerWithSocials(providerName, token, providerId,secret,name,picture,email)
    }
}