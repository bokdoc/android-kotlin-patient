package com.bokdoc.pateintsbook.ui.filters

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bokdoc.pateintsbook.data.repositories.filter.FilterRepositories

class FiltersModelProvider(private val filtersRepositories: FilterRepositories): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FilterViewModel(filtersRepositories) as T
    }
}