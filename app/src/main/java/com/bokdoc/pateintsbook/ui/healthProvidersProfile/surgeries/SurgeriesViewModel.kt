package com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.net.Uri
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.surgeries.ProfileSurgeriesRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SingleSurgery
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow

class SurgeriesViewModel(val surgeriesRepository: ProfileSurgeriesRepository) : ViewModel() {

    lateinit var id: String
    lateinit var url: String
    var remainingMediaLiveData = MutableLiveData<String>()
    var allMedia: List<MediaParcelable> = ArrayList()
    var meta: Meta = Meta()
    var currentPage: Int = 1
    var serviceType: HashMap<String, String>? = HashMap()


    fun getSurgeries(): LiveData<DataResource<SurgeriesRow>> {
        serviceType!!.put("page", currentPage.toString())
        serviceType!!.put("include", "nextAvailabilities,parent")
        return surgeriesRepository.getSurgeries(id, serviceType!!)
    }

    fun showSurgeryDetails(): LiveData<DataResource<SingleSurgery>> {
        return surgeriesRepository.showSurgery(url)
    }

    fun getMediaDetails(media: ArrayList<MediaParcelable>): List<MediaParcelable> {
        if (media.isNotEmpty()) {
            allMedia = media
            val mediaSize = media.size
            if (mediaSize > 5) {
                remainingMediaLiveData.value = "+" + (mediaSize - 5).toString()
                allMedia = media.subList(0, 5)
            }
        }
        return allMedia
    }

    fun getMediaUris(): ArrayList<Uri> {
        val uris = ArrayList<Uri>()
        allMedia.forEach {
            uris.add(Uri.parse(it.url))
        }
        return uris
    }

    fun hasProcedureRules(singleSurgery: SingleSurgery): Boolean {
        var isValid = false
        if (singleSurgery.paymentType.isNotEmpty() && singleSurgery.approvalType.isNotEmpty() && singleSurgery.watingQueu.isNotEmpty()) {
            isValid = true
        }
        return isValid
    }

    fun hasTreatmentPlan(singleSurgery: SingleSurgery): Boolean {
        var isValid = false
        if (singleSurgery.duration.isNotEmpty() && singleSurgery.programDuration.isNotEmpty() && singleSurgery.preOrderRadiologyScans.isNotEmpty() && singleSurgery.anesthesiaTypes.isNotEmpty() && singleSurgery.laboratoryTestIncluded.isNotEmpty()) {
            isValid = true
        }
        return isValid
    }
}