package com.bokdoc.pateintsbook.ui.common

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ViewModelsCustomProvider(private val viewModel:ViewModel):ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModel as T
    }
}