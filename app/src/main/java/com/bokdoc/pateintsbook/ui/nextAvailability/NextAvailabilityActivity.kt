package com.bokdoc.pateintsbook.ui.nextAvailability

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_next_availability.*

class NextAvailabilityActivity : ParentActivity() {

    lateinit var nextAvailabilityViewModel: NextAvailabilityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_next_availability)
        enableBackButton()
        getViewModel()
        getDataFromIntent()
        title = getString(R.string.check_time)

    }

    private fun getData() {
        lay_shimmer.startShimmerAnimation()
        nextAvailabilityViewModel.getNextAvailabilityTimes().observe(this, Observer {
            when (it!!.status) {
                SUCCESS -> {
                    lay_shimmer.stopShimmerAnimation()
                    lay_shimmer.visibility = View.GONE
                    rv_next_next_availabilities_time.visibility = View.VISIBLE
                    val layout = GridLayoutManager(this, 3)
                    rv_next_next_availabilities_time.layoutManager = layout
                    rv_next_next_availabilities_time.adapter = NextAvailabilityTimesAdapter(it.data!!,
                            this::itemCLickListner,nextAvailabilityViewModel.selectedNextAvailability)
                }
                else -> {
                    lay_shimmer.stopShimmerAnimation()
                    Utils.showShortToast(this, getString(R.string.some_thing_went_wrong))
                }
            }
        })
    }

    private fun itemCLickListner(nv: NextAvailabilitiesParcelable, position: Int) {
        if (intent.hasExtra(getString(R.string.book_url_key))) {
            if (nv.status) {
                if(nextAvailabilityViewModel.isOpenBookForm) {
                    nv.to = ""
                    Navigator.navigate(this, arrayOf(getString(R.string.url_key)),
                            arrayOf(intent.getStringExtra(getString(R.string.book_url_key))),
                            getString(R.string.nextAvailabilitiesKey),
                            arrayListOf(nv), BookAppointmentsActivity::class.java, BOOK_REQUEST)
                }else{
                    val intent = Intent()
                    intent.putExtra(getString(R.string.nextAvailabilitiesKey),nv)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                    //Navigator.naviagteBack(this,getString(R.string.nextAvailabilitiesKey),nv)
                }
            }
        }

    }

    private fun getDataFromIntent() {
        if (intent.hasExtra(getString(R.string.url_key))) {
            nextAvailabilityViewModel.url = intent.getStringExtra(getString(R.string.url_key))
            getData()
        }
        if (intent.hasExtra(getString(R.string.nextAvailabilitiesKey))) {
            intent?.getParcelableArrayListExtra<NextAvailabilitiesParcelable>((getString(R.string.nextAvailabilitiesKey)))
                    .let {
                        nextAvailabilityViewModel.selectedNextAvailability = it!![0]
                    }
            tv_check_time.text = nextAvailabilityViewModel.selectedNextAvailability!!.date
        }

        if(intent?.hasExtra(getString(R.string.is_open_book))!!)
        nextAvailabilityViewModel.isOpenBookForm = intent?.getStringExtra(getString(R.string.is_open_book))!!.toBoolean()
    }

    private fun getViewModel() {
        nextAvailabilityViewModel = InjectionUtil.getNextAvailabilitiesViewModel(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == BOOK_REQUEST && resultCode== Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()
        }
    }


    companion object {
        const val BOOK_REQUEST = 1
    }
}