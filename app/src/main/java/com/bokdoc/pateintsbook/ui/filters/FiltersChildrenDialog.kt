package com.bokdoc.pateintsbook.ui.filters

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.utils.ui.PercentageAdapter
import com.bokdoc.pateintsbook.utils.ui.PercentageDialog
import kotlinx.android.synthetic.main.dialog_numbers.*
import kotlinx.android.synthetic.main.filters_dialog.*

class FiltersChildrenDialog(context: Context?, private val optionsList:List<FilterChild>, val key:String
                            , val dataType:String) : AppCompatDialog(context) {
    var selectedValues: ArrayList<String> = ArrayList()
    var callback: OnDialogClickedListener? = null
    var message = ""


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.filters_dialog)

        childrenList.adapter = FiltersChildrenAdapter(optionsList,key,
                dataType,this::onListClickListeners)

        ok.setOnClickListener {
            callback?.onDialogClicked(key,dataType,selectedValues)
            dismiss()
        }

    }

    fun updateList() {
        rv_numbers.adapter!!.notifyDataSetChanged()
    }

    /*
    fun onSelectNum(num: String, position: Int) {
        callback!!.onDialogClicked(position, num)
        dismiss()
    }
    */


    override fun show() {

        super.show()

        val layoutParams = WindowManager.LayoutParams()

        layoutParams.copyFrom(window!!.attributes)

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT

        window!!.attributes = layoutParams


    }

    private fun onListClickListeners(key:String,dataType:String,values:ArrayList<String>){
        callback?.onDialogClicked(key,dataType,values)
    }


    fun setOnDialogClickedListener(dialogCallback: OnDialogClickedListener) {

        callback = dialogCallback

    }


    interface OnDialogClickedListener {

        fun onDialogClicked(key:String,dataType:String,values:ArrayList<String>)

    }
}