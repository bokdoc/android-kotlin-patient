package com.bokdoc.pateintsbook.ui.patientCompares

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.profile_card.view.*

class CompareAdapter(private var profiles: List<Profile>, private val onClickListener: (View, Profile, Int) -> Unit) : SearchResultsAdapter(profiles, onClickListener) {



    private fun getTextWithPrice(context: Context, feesAverage: Double, currentSymbols: String, stringRes: Int, color: Int = R.color.colorPrimary): Spannable {
        val price = context.getString(R.string.pricePlaceHolder, feesAverage.toString(),
                currentSymbols)
        val consulting = context.getString(stringRes, price)
        return ColoredTextSpannable.getSpannable(consulting, price, ContextCompat.getColor(context, color))
    }

}