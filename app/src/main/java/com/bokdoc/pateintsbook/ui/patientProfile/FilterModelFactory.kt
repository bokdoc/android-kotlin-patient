package com.bokdoc.pateintsbook.ui.patientProfile

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context

class FilterModelFactory (private val context: Context): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FilterViewModel(context) as T
    }
}