package com.bokdoc.pateintsbook.ui.tutorials

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.View.LAYOUT_DIRECTION_LTR
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.TutorialsItemInfo
import com.bokdoc.pateintsbook.navigators.HomeNavigator
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.homeMenuList.HomeMenuListActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_tutorials.*
import kotlinx.android.synthetic.main.tutorial_item_view.view.*

class TutorialsActivity : AppCompatActivity() {

    private lateinit var tutorialsViewModel: TutorialsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorials)
        tutorialsViewModel = ViewModelsCustomProvider(TutorialsViewModel(application)).create(TutorialsViewModel::class.java)


        pager.adapter = TutorialsPagerAdapter(tutorialsViewModel.getTutorialsInfoItems())
        indicatorsView.setViewPager(pager)
        indicatorsView.setSmoothTransition(true)
        if(LocaleManager.isReverse(this)){
            indicatorsView.rotation = 180f
        }
        get_started.setOnClickListener {
            Navigator.navigate(this, arrayOf(baseContext.getString(R.string.defaultTab)),
                    arrayOf(pager.currentItem.toString()), MainActivity::class.java)
            finish()
        }
    }

    inner class TutorialsPagerAdapter(val tutorialsInfo: List<TutorialsItemInfo>) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
            return tutorialsInfo.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(container.context).inflate(R.layout.tutorial_item_view, container, false)
            tutorialsInfo[position].let {
                view.setBackgroundColor(ContextCompat.getColor(container.context, it.backgroundColor))
                Glide.with(container.context).load(it.imageRes).into(view.tutorialImage)
                view.tutorialText.text = it.title
                view.tutorialDescription.text = it.description
                view.getStarted.text = it.buttonText
            }

            view.getStarted.setOnClickListener {
                HomeNavigator.navigate(this@TutorialsActivity, baseContext.getString(R.string.defaultTab), position)
            }
            val viewPager = container as ViewPager
            viewPager.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val viewPager = container as ViewPager
            val view = `object` as View
            viewPager.removeView(view)
        }

    }
}
