package com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.Toast
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.ui.parents.ParentActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.bokdoc.pateintsbook.utils.injection.InjectionUtil
import kotlinx.android.synthetic.main.activity_list_with_progress.*

class QualificationsDetailsActivity : ParentActivity() {

    private lateinit var qualificationsDetailsViewModel: QualificationsDetailsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_with_progress)
        getViewModel()
        getDataFromIntent()
        enableBackButton()
        getData()
    }

    private fun getDataFromIntent() {
        val passedText = intent.getStringExtra(getString(R.string.selectedTextKey))

        when (intent?.getStringExtra(getString(R.string.typeKey))!!) {
            Profile.DOCTOR.toString() -> {
                title = getString(R.string.dr_place_holder, Utils.getFirstWord(intent?.getStringExtra(getString(R.string.docNameSlug))!!), passedText)
            }
            Profile.HOSPITAL.toString() -> {
                title = getString(R.string.hos_place_holder, intent?.getStringExtra(getString(R.string.docNameSlug))!!, passedText)
            }
            Profile.CENTER.toString()->{
                title = getString(R.string.three_place_holder,
                        intent?.getStringExtra(getString(R.string.docNameSlug))!!, passedText,getString(R.string.center))
            }
            Profile.POL_CLINIC.toString()->{
                title = getString(R.string.three_place_holder,
                        intent?.getStringExtra(getString(R.string.docNameSlug))!!, passedText,getString(R.string.polyclinics))
            }
        }
        showToolbarImage(intent?.getStringExtra(getString(R.string.docPicSlug))!!)


        qualificationsDetailsViewModel.sectionText = passedText
    }

    private fun getData() {
        qualificationsDetailsViewModel.errorLiveData.observe(this, Observer {
            Toast.makeText(this, it!!, Toast.LENGTH_LONG).show()
        })

        qualificationsDetailsViewModel.progressLiveData.observe(this, Observer {
            progress.visibility = it!!
        })

        qualificationsDetailsViewModel.detailsLiveData.observe(this, Observer {
            bindData(it!!)
        })

        qualificationsDetailsViewModel.getDetails(intent?.getStringExtra(getString(R.string.idKey))!!)
    }

    private fun getViewModel() {
        qualificationsDetailsViewModel = InjectionUtil.getQualificationsViewModel(this)
    }

    private fun bindData(qualifications: List<Qualifications>) {
        if (qualifications.isEmpty()) {
            return
        }

        qualifications.let {
            rv_list.adapter = QualificationsAdapter(it)
        }
    }
}
