package com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DoctorRow
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.doctor_row.view.*
import kotlinx.android.synthetic.main.item_progress.view.*

class DoctorsAdapter(var doctors: ArrayList<DoctorRow>,
                     val onBookClickListener: (DoctorRow) -> Unit) : PaginationRecyclerViewAdapter<DoctorRow>(R.layout.doctor_row, doctors) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = DoctorsViewHolder(itemView)
            }

            LOADING -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }


    override fun getItemCount(): Int {
        return doctors.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (doctors[position].type == 0) {
            ITEM
        } else {
            LOADING
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ITEM -> {
                doctors[position].let {
                    Glide.with(holder.itemView.context).load(it.photo).into(holder.itemView.iv_doctor)
                    holder.itemView.tv_doctor_name.text = it.name
                    holder.itemView.tv_doctor_speciality.text = it.speciality

                    if (it.bookButton != null) {
                        holder.itemView.btn_doctor_book.visibility = View.VISIBLE
                    }

                    if (it.spokenLanguage.isNotEmpty()) {
                        holder.itemView.tv_doctor_spoken_languages.text =
                                ColoredTextSpannable.getSpannable(holder.itemView.context.getString(R.string.two_texts_placeholder,
                                        holder.itemView.context.getString(R.string.spoken_language), android.text.TextUtils.join(",", it.spokenLanguage)), android.text.TextUtils.join(",", it.spokenLanguage), ContextCompat.getColor(holder.itemView.context, R.color.colorDescription))
                    } else {
                        holder.itemView.tv_doctor_spoken_languages.visibility = View.GONE
                    }
                }
            }

            LOADING -> {
                if (retryPageLoad) {
                    holder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                    holder.itemView.loadmore_progress.visibility = View.GONE

                } else {
                    holder.itemView.loadmore_errorlayout.visibility = View.GONE
                    holder.itemView.loadmore_progress.visibility = View.VISIBLE
                }
            }

        }

    }

    fun updateList(doctorsRow: ArrayList<DoctorRow>) {
        doctors = doctorsRow
        notifyDataSetChanged()
    }

    fun add(doctorsRow: ArrayList<DoctorRow>) {
        val lastPosition = itemCount
        doctors.addAll(doctorsRow)
        notifyItemRangeInserted(lastPosition, itemCount)
    }


    fun remove(position: Int) {
        doctors.removeAt(position)
        notifyItemRemoved(position)
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(DoctorRow().apply {
            id = ""
            name = ""
            photo = ""
            title = ""
            speciality = ""
            subSpeciality = ""
            gender = ""
            bookButton = null
            spokenLanguage = ArrayList()
            type = 1
        })
    }

    inner class DoctorsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.btn_doctor_book.setOnClickListener {
                onBookClickListener.invoke(doctors[adapterPosition])
            }


        }
    }
}