package com.bokdoc.pateintsbook.ui.contactUs

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
 import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.contactUs.ContactUsRepository

class ContactUsViewModel(val contactUsRepository: ContactUsRepository) :ViewModel() {

    lateinit var message: String
    lateinit var name: String
    lateinit var email: String

    fun contactUs(): LiveData<DataResource<Boolean>> {
        return contactUsRepository.contactUs(message,name,email)
    }

}