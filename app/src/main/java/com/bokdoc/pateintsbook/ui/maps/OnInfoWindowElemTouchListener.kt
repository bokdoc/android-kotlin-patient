package com.bokdoc.pateintsbook.ui.maps

import android.graphics.drawable.Drawable
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import com.google.android.gms.maps.model.Marker

abstract class OnInfoWindowElemTouchListener(private val view:View): View.OnTouchListener {
    private var  handler: Handler =  Handler()

    private var marker: Marker?=null
    private var pressed:Boolean  = false


     fun setMarker(marker:Marker) {
        this.marker = marker
    }


    override fun onTouch(vv:View , event: MotionEvent):Boolean {
        if (0 <= event.getX() && event.getX() <= view.getWidth() &&
                0 <= event.getY() && event.getY() <= view.getHeight())
        {
            when (event.getActionMasked()) {
                MotionEvent.ACTION_DOWN-> startPress()
                MotionEvent.ACTION_UP-> handler.postDelayed(confirmClickRunnable, 150)
                // We need to delay releasing of the view a little so it shows the pressed state on the screen
                MotionEvent.ACTION_CANCEL-> endPress()
            }
        }
        else {
            // If the touch goes outside of the view's area
            // (like when moving finger out of the pressed button)
            // just release the press
            endPress();
        }
        return false;
    }

    private fun startPress() {
        if (!pressed) {
            pressed = true;
            handler.removeCallbacks(confirmClickRunnable);
           // view.setBackground(bgDrawablePressed);
            if (marker != null)
                marker?.showInfoWindow();
        }
    }

    private fun endPress():Boolean {
        if (pressed) {
            this.pressed = false;
            handler.removeCallbacks(confirmClickRunnable);
          //  view.setBackground(bgDrawableNormal);
            if (marker != null)
                marker?.showInfoWindow()
            return true;
        }
        else
            return false;
    }

    private val confirmClickRunnable = Runnable {
        fun run() {
            if (endPress()) {
                if(marker!=null)
                onClickConfirmed(view, marker!!)
            }
        }
    };

    /**
     * This is called after a successful click
     */
    abstract fun onClickConfirmed(v:View , marker:Marker )
}