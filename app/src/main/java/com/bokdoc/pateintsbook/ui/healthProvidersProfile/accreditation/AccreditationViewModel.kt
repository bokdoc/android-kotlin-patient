package com.bokdoc.pateintsbook.ui.healthProvidersProfile.accreditation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accreditation.AccreditationRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.consulting.ProfileConsultingRepository
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow

class AccreditationViewModel(val accreditationRepository: AccreditationRepository) : ViewModel() {

    lateinit var id: String
    lateinit var serviceType: String

    fun getAccreditation(): LiveData<DataResource<AccreditationRow>> {
        return accreditationRepository.getAccreditation(id, serviceType)
    }

}