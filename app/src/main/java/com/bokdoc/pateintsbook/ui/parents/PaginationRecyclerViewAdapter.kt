package com.bokdoc.pateintsbook.ui.parents

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup

open class PaginationRecyclerViewAdapter<Item>(val LayoutId: Int, var data: MutableList<Item>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var isLoadingAdded = false
    public var retryPageLoad = false

    public var errorMsg: String? = null


    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            ITEM
        } else {
            if (position == data!!.size - 1 && isLoadingAdded) LOADING else ITEM
        }
    }


    open fun add(item: Item) {
        data.add(item)
        notifyItemInserted(data.size - 1)
    }

    fun addAll(items: MutableList<Item>) {
        val oldSize: Int = itemCount
        Log.i("itemsSize",items.size.toString())

        data.addAll(items)
        Log.i("itemsSizeAfterAdded",data.size.toString())
        notifyItemRangeInserted(oldSize, itemCount)
        Log.i("oldSize",oldSize.toString())
        Log.i("itemcount",itemCount.toString())
    }

    fun update(items: MutableList<Item>){
        data = items
        notifyDataSetChanged()
    }

    fun remove(shop: Item?) {
        val position = data.indexOf(shop)
        if (position > -1) {
            data.removeAt(position)
            notifyItemRemoved(position)
        }
    }


    fun removeLoadingFooter() {
        isLoadingAdded = false
        data.removeAt(itemCount-1)
        notifyItemRemoved(itemCount-1)
    }

    fun clear() {
        val size = data.size
        data.clear()
        notifyItemRangeRemoved(0, size)
    }

    companion object {
        //view types
        val ITEM = 0
        val LOADING = 1
        val MESSAGE = 2
    }

    open class DataViewHolder(view: View) : RecyclerView.ViewHolder(view)

    inner class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)
}