package com.bokdoc.pateintsbook.ui.searchResults

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.R.id.surgeriesFeesText
import com.bokdoc.pateintsbook.R.id.surgeriesGroup
import com.bokdoc.pateintsbook.data.common.unsafeOkHttpClient.UnsafeOkHttpClient
import com.bokdoc.pateintsbook.data.common.unsafeOkHttpClient.UnsafeOkHttpGlideModule
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Review
import com.bokdoc.pateintsbook.ui.parents.PaginationRecyclerViewAdapter
import com.bokdoc.pateintsbook.utils.ColoredTextSpannable
import com.bokdoc.pateintsbook.utils.FeesSpannable
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.squareup.picasso.Callback
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_progress.view.*
import kotlinx.android.synthetic.main.item_search_message.view.*
import kotlinx.android.synthetic.main.profile_card.view.*
import java.lang.Exception
import java.net.URLEncoder
import javax.xml.datatype.DatatypeConstants.SECONDS
import okhttp3.OkHttpClient
import okhttp3.Protocol
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


open class SearchResultsAdapter(private var profiles: List<Profile>, private val onClickListener: (View, Profile, Int) -> Unit)
    : PaginationRecyclerViewAdapter<Profile>(R.layout.profile_card, profiles as MutableList<Profile>) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val itemView = inflater.inflate(LayoutId, parent, false)
                viewHolder = ProfileViewHolder(itemView)
            }

            MESSAGE -> {
                val messageView = inflater.inflate(R.layout.item_search_message, parent, false)
                viewHolder = MessageViewHolder(messageView)
            }

            LOADING -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }

    override fun getItemViewType(position: Int): Int {
        return if ((profiles[position].id != null && !profiles[position].is_message)) {
            ITEM
        } else if (profiles[position].id != null && profiles[position].is_message) {
            MESSAGE
        } else {
            LOADING
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        val profile = profiles[position]

        if (getItemViewType(position) == ITEM) {
            holder as ProfileViewHolder

            Glide.with(holder.view.context).load(profile.picture.url).into(holder.view.profileImage)
            holder.itemView.tv_rotator.isSelected = true
            holder.view.profileName.text = profile.name

            if (profile.healthProviderMessages.isNotEmpty()) {
                holder.itemView.tv_rotator.visibility = VISIBLE
                holder.itemView.tv_rotator.text = android.text.TextUtils.join(" /  ", profile.healthProviderMessages)
            } else {
                holder.itemView.tv_rotator.visibility = GONE
            }


            if (!profile.clinicAddresses.isNullOrEmpty() && profile.profileTypeId == Profile.DOCTOR) {
                holder.itemView.tv_clinic_addresses.visibility = VISIBLE
                holder.itemView.tv_clinic_addresses.text = profile.clinicAddresses
            } else {
                holder.itemView.tv_clinic_addresses.visibility = GONE
            }


            if (profile.profileTypeId == Profile.HOSPITAL || profile.profileTypeId == Profile.POL_CLINIC ||
                    profile.profileTypeId == Profile.CENTER) {
                if (profile.location.address.isNotEmpty()) {
                    holder.view.profileDesc.visibility = VISIBLE
                    holder.view.profileDesc.text = profile.location.address
                } else {
                    holder.view.profileDesc.visibility = GONE
                }

            } else {
                if (profile.mainSpeciality.isNotEmpty()) {
                    holder.view.profileDesc.visibility = VISIBLE
                    holder.view.profileDesc.text = profile.specialityTitle
                } else {
                    holder.view.profileDesc.visibility = GONE
                }
            }


            if (profile.cardLinks != null) {
                holder.itemView.rv_card_links.visibility = VISIBLE
                holder.itemView.rv_card_links.adapter = CardLinksAdapter(profile.cardLinks!!) {
                    onClickListener.invoke(holder.itemView.rv_card_links.apply {
                        tag = it
                    }, profile, position)
                }
            } else {
                holder.itemView.rv_card_links.visibility = GONE
            }

            showServices(profile, holder)

            //holder.view.appointmentTodayText.text = "Only 7 Appointments left!"
            holder.view.appointmentTodayText.text = ""

            if(profile.rating!=0f) {
                holder.view.rating.visibility = VISIBLE
                holder.view.rating.rating = profile.rating
            }else{
                holder.view.rating.visibility = GONE
            }

            if(profile.reviewsCount.toString()!="0") {
                holder.view.reviewsNumbers.visibility = VISIBLE
                holder.view.reviewsNumbers.text = holder.view.context.getString(R.string.reviewsPlaceholder,
                        profile.reviewsCount.toString())
            }else{
                holder.view.reviewsNumbers.visibility = GONE
            }

            /*
            if(profile.reviewsCount>0){
                holder.view.reviewsNumbers.visibility = VISIBLE

            }else{
                holder.view.reviewsNumbers.visibility = GONE
            }
            */

            holder.view.profileTypeText.text = profile.profileTypeName


            when (profile.profileTypeId) {
                Profile.DOCTOR-> {
                    holder.view.profileTypeText.setBackgroundResource(R.drawable.blue_curved_rect)
                    updateColors(ContextCompat.getColor(holder.view.context, R.color.colorBlue),
                            holder.view.profileTypeText, holder.view.profileName)
                }
                Profile.LAB, Profile.RAD -> {
                    updateColors(ContextCompat.getColor(holder.view.context, R.color.colorLaboratory),
                            holder.view.profileTypeText, holder.view.profileName)
                }
                Profile.HOSPITAL -> {
                    holder.view.profileTypeText.setBackgroundResource(R.drawable.purpule_rect_curve)
                    updateColors(ContextCompat.getColor(holder.view.context, R.color.colorPurple),
                            holder.view.profileTypeText, holder.view.profileName)
                }
                Profile.POL_CLINIC->{
                    holder.view.profileTypeText.setBackgroundResource(R.drawable.pink_curved_rect)
                    updateColors(ContextCompat.getColor(holder.view.context, R.color.colorPolyclinic),
                            holder.view.profileTypeText, holder.view.profileName)
                }
                Profile.CENTER->{
                    holder.view.profileTypeText.setBackgroundResource(R.drawable.orange_curved_rect)
                    updateColors(ContextCompat.getColor(holder.view.context, R.color.colorCenter),
                            holder.view.profileTypeText, holder.view.profileName)
                }
            }
            /*
            if (profile.isBokdocGrantee) {
                holder.view.bokGuarantee.visibility = VISIBLE
            } else {
                holder.view.bokGuarantee.visibility = GONE
            }
            */
            if (profile.nextAvailability != null &&
                    profile.nextAvailability!!.isNotEmpty() && profile.nextAvailability!="0") {
                holder.view.nextAvailabilityText.visibility = VISIBLE
                holder.view.nextAvailabilityText.text = ColoredTextSpannable.getSpannable(holder.view.context.getString(R.string.nextAvailabilityPlaceHolder, profile.nextAvailability!!),
                        profile.nextAvailability!!, ContextCompat.getColor(holder.itemView.context, R.color.colorAccent))
                holder.view.nextAvailabilityText.setOnClickListener {
                    if (profile.bookButton != null) {
                        onClickListener.invoke(holder.itemView.bookAppointment.apply {
                            holder.itemView.bookAppointment.tag = profile.bookButton
                        }, profile, holder.adapterPosition)
                    }
                }
            } else {
                holder.view.nextAvailabilityText.visibility = GONE
            }

            if (profile.bookButton != null) {
                holder.view.bookAppointment.visibility = VISIBLE
                holder.view.bookAppointment.tag = profile.bookButton
                if (profile.bookButton?.fees?.feesAverage != "0" && profile.bookButton?.fees?.feesAverage != null) {
                    holder.view.bookAppointment.text = getTextWithPrice(holder.view.context,
                            profile.bookButton?.fees!!.feesAverage!!,
                            profile.bookButton?.fees!!.currencySymbols!!,
                            R.string.two_texts_placeholder, profile.bookButton!!.label,profile.bookButton?.fees?.oldAmount,R.color.colorYellow)
                } else {
                    holder.view.bookAppointment.text = profile.bookButton!!.label
                }

            } else {
                if (profile.bookButtons != null && profile.bookButtons?.isNotEmpty()!!) {
                    holder.view.bookAppointment.visibility = VISIBLE
                    holder.view.bookAppointment.tag = profile.bookButtons!![0]

                    if (profile.bookButton?.fees?.feesAverage != "0" && profile.bookButton?.fees?.feesAverage != null) {
                        holder.view.bookAppointment.text = getTextWithPrice(holder.view.context, profile.bookButtons!![0].fees.feesAverage!!,
                                profile.bookButton?.fees!!.currencySymbols!!,
                                R.string.two_texts_placeholder, profile.bookButtons!![0].label,profile.bookButton?.fees?.oldAmount,R.color.colorYellow)
                    } else {
                        holder.view.bookAppointment.text = profile.bookButtons!![0].label
                    }
                } else {
                    holder.view.bookAppointment.visibility = GONE
                }
            }


        } else if (getItemViewType(position) == LOADING) {
            val loadingViewHolder = holder as LoadingViewHolder
            if (retryPageLoad) {
                loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.VISIBLE
                loadingViewHolder.itemView.loadmore_progress.visibility = View.GONE

            } else {
                loadingViewHolder.itemView.loadmore_errorlayout.visibility = View.GONE
                loadingViewHolder.itemView.loadmore_progress.visibility = View.VISIBLE
            }
        } else {
            val messageViewHolder = holder as MessageViewHolder

            messageViewHolder.itemView.tv_search_message.text = profile.message

        }

    }

    private fun showServices(profile: Profile, holder: ProfileViewHolder) {


        val dynamicLinks: ArrayList<DynamicLinks> = ArrayList()

        if (profile.cardButtons != null) {
            dynamicLinks.addAll(profile.cardButtons!!)
        }

        if (profile.freeButtons != null) {
            dynamicLinks.addAll(profile.freeButtons!!)
        }

        if (dynamicLinks.isNotEmpty()) {

            holder.itemView.rv_card_buttons.visibility = VISIBLE
            holder.itemView.rv_card_buttons.layoutManager =
                    LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)

            holder.itemView.rv_card_buttons.adapter = CardButtonsAdapter(dynamicLinks) {
                onClickListener.invoke(holder.itemView.rv_card_buttons.apply {
                    holder.itemView.rv_card_buttons.tag = it
                }, profile, holder.adapterPosition)
            }

        } else {
            holder.itemView.rv_card_buttons.visibility = GONE
        }

        /*
         if (profile.cardButtons != null && profile.cardButtons!!.isNotEmpty()) {
             profile.cardButtons?.forEach {
                 /*
                 if(it.isKeyHomeVisits){
                     holder.view.homeVisitsText.text = getTextWithPrice(holder.view.context,it.fees.feesAverage,
                             it.fees.currencySymbols,R.string.two_texts_placeholder,it.label)
                   //  holder.view.homeVisitsText.text = it.label
                     holder.view.homeVisitsText.visibility = VISIBLE
                     holder.view.homeVisitsImage.visibility = VISIBLE
                     holder.view.homeVisitsText.tag = it
                     holder.view.homeVisitsImage.tag = it
                 }else{
                     holder.view.homeVisitsText.visibility = GONE
                     holder.view.homeVisitsImage.visibility = GONE
                 }
                 */
                 /*
                 holder.view.homeVisitsText.visibility = GONE
                 holder.view.homeVisitsImage.visibility = GONE
                 */


                 if (it.isKeyOnlineConsulting) {
                     if (it.fees.feesAverage != "0") {
                         holder.view.consultingText.text = getTextWithPrice(holder.view.context, it.fees.feesAverage,
                                 it.fees.currencySymbols, R.string.two_texts_placeholder, it.label)
                     } else {
                         holder.view.consultingText.text = it.label
                     }
                     holder.view.consultingText.visibility = VISIBLE
                     holder.view.consultingImage.visibility = VISIBLE
                     holder.view.consultingText.tag = it
                     holder.view.consultingImage.tag = it
                 } else {
                     //holder.view.consultingText.visibility = GONE
                     //holder.view.consultingImage.visibility = GONE
                 }
             }
         } else {
             //holder.view.homeVisitsText.visibility = GONE
             // holder.view.homeVisitsImage.visibility = GONE
             /*
             holder.view.consultationText.visibility = GONE
             holder.view.consultationImage.visibility = GONE
             */
         }

         /*
         holder.itemView.surgeriesFeesText.text = getTextWithPrice(holder.view.context, 255.0,
                 "EGP", R.string.two_texts_placeholder, "Surgeries")
                 */

         if (profile.freeButtons != null) {
             holder.view.freeText.text = ColoredTextSpannable.getSpannable(holder.view.context.getString(R.string.two_texts_placeholder,
                     profile.freeButtons!![0].label, profile.freeAppointment.toString()), profile.freeAppointment.toString(),
                     ContextCompat.getColor(holder.view.context, R.color.colorPrimary))
             // holder.view.freeText.text = profile.freeButtons!![0].label
             holder.view.freeText.visibility = VISIBLE
             holder.view.freeImage.visibility = VISIBLE
             holder.view.freeText.tag = profile.freeButtons!![0].endpointUrl
             holder.view.freeImage.tag = profile.freeButtons!![0].endpointUrl

         } else {
             holder.view.freeText.visibility = GONE
             holder.view.freeImage.visibility = GONE
         }
        * */


    }

    fun addLoadingFooter() {
        //isLoadingAdded = true
        //add(Profile())
        super.add(Profile())
    }

    private fun updateColors(color: Int, profileType: TextView, name: TextView) {
        //profileType.setBackgroundColor(color)
        name.setTextColor(color)
    }

    private fun getTextWithPrice(context: Context, feesAverage: String, currentSymbols: String,
                                 stringRes: Int, label: String,oldFees:String?, color: Int = R.color.colorPrimary): Spannable {

        val price = context.getString(R.string.pricePlaceHolder, feesAverage,
                currentSymbols)
        var consulting = context.getString(stringRes, label, price)

        var oldFeesCurrency = ""
        oldFees?.let {
            if(it.isNotEmpty() && it!="0") {
                oldFeesCurrency = "$it $currentSymbols"
                consulting = context.getString(R.string.placeHolder, consulting,oldFeesCurrency)
            }
        }

        return FeesSpannable.getSpannable(consulting, price, ContextCompat.getColor(context, color),oldFeesCurrency)
    }

    /*
    fun addToList(profiles: List<Profile>) {
        val lastItemCount = profiles.size
        (profiles as MutableList<Profile>).addAll(profiles)
        notifyDataSetChanged()
    }
    */

    fun updateList(profiles: List<Profile>) {
        super.update(profiles as MutableList<Profile>)
    }

    fun toggleFavoriate(position: Int) {
        this.profiles[position].isFavourite.let {
            if (it == 1) {
                profiles[position].isFavourite = 0
            } else {
                profiles[position].isFavourite = 1
            }
        }
        notifyItemChanged(position)
    }

    fun updateFavoriate(position: Int, isFavorite: Boolean) {
        if (isFavorite) {
            this.profiles[position].isFavourite = 1
        } else {
            this.profiles[position].isFavourite = 0
        }

        notifyItemChanged(position)
    }

    fun toggleCompare(position: Int) {
        this.profiles[position].isCompared.let {
            if (it == 1) {
                profiles[position].isCompared = 0
            } else {
                profiles[position].isCompared = 1
            }
        }
        notifyItemChanged(position)
    }

    fun updateCompare(position: Int, isCompare: Boolean) {
        if (isCompare) {
            this.profiles[position].isCompared = 1
        } else {
            this.profiles[position].isCompared = 0
        }

        notifyItemChanged(position)
    }

    fun toggleLike(position: Int) {
        this.profiles[position].isLiked.let {
            if (it == 1) {
                profiles[position].isLiked = 0
            } else {
                profiles[position].isLiked = 1
            }
        }
        notifyItemChanged(position)
    }

    fun updateLike(position: Int, isLiked: Boolean) {
        if (isLiked) {
            this.profiles[position].isLiked = 1
        } else {
            this.profiles[position].isLiked = 0
        }

        notifyItemChanged(position)
    }


    inner class ProfileViewHolder(val view: View) : PaginationRecyclerViewAdapter.DataViewHolder(view) {
        init {
            view.profileImage.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.viewMore.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.bookAppointment.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.iv_clinics.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.tv_clinics_value.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.tv_surgeries_value.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.iv_surgeries.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            /*
            view.homeVisitsText.setOnClickListener {
                onClickListener.invoke(it,profiles[adapterPosition],adapterPosition)
            }
            view.homeVisitsImage.setOnClickListener {
                onClickListener.invoke(it,profiles[adapterPosition],adapterPosition)
            }
            */
            view.consultingImage.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.consultingImage.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            /*
            view.freeImage.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            view.freeText.setOnClickListener {
                onClickListener.invoke(it, profiles[adapterPosition], adapterPosition)
            }
            */
        }
    }

    inner class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)


    inner class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view)

}