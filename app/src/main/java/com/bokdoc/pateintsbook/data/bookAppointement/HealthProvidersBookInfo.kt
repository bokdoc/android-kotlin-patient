package com.bokdoc.pateintsbook.data.bookAppointement

interface HealthProvidersBookInfo {
    var profileId:Int
    var paymentTypeId:Int?
    var paymentTypeMethod:Int?
    var serviceId:Int?
    var clinicId:Int?
    var departmentId:Int?
    var doctorId:Int?
}