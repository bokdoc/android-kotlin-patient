package com.bokdoc.pateintsbook.data

import android.text.Spannable
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.IAddEditRow

open class AddEditRow: IAddEditRow {
    override var type:Int = IAddEditRow.TEXT_ARROW_ROW
    override var name: String = ""
    override var isMandatory: Boolean = false
    override var description: String = "Select this field"
    override var isSelected: Boolean = false
    override var slug: String = ""
    override var isShowOptionalText: Boolean = false
    override var nameStatus:Int = IAddEditRow.NAME_STATUS_DEFAULT
    override var selectedText: String = ""
    override var editTextTitle: String = ""
    override var editTextValue: String = ""
    override var editTextHint:String = ""
    override var switchValue:Boolean = false
    override var addEditsRows: List<IAddEditRow> = ArrayList()
    override var spinnerValues: Array<String> = arrayOf()
    override var background: Int= R.color.colorTransparent
    override var visibility: Boolean = true
    override var editTextFrom: String = ""
    override var editTextTo:String = ""
    override var editTextFromHint: String = ""
    override var editTextToHint:String = ""
    override var isShowLine: Boolean = true
    override var editTextFromTitle: String = ""
    override var editTextToTitle:String = ""
    override var nameSpannable: Spannable?=null
    override var imagesRes:Array<Int> = arrayOf()
    override var isFieldEnabled: Boolean = true

    companion object {
        const val APPROVAL_TYPES_SLUG = "approvalTypes"
        const val PAYMENT_TYPES_SLUG = "paymentTypes"
        const val MOBILE_NUMBER_SLUG = "mobileNumber"
        const val RESERVATION_TYPE_SLUG = "reservationType"
        const val NAME_SLUG: String = "name"
        const val VALUE_SLUG = "value"
        const val ADDRESS_SLUG = "address"
        const val MAP_SLUG = "map"
        const val STATUS_SLUG: String = "status"
        const val DURATION_VISIT_SLUG: String = "duration"
        const val RULES_SLUG: String = "rules"
        const val RANGES_FROM:String = "rangesFrom"
        const val RANGES_TO:String = "rangesTo"
        const val PAYMENT_METHODS_SLUG = "paymentMethods"

    }
}