package com.bokdoc.pateintsbook.data.repositories.register

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.graphics.Region
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.UserCustomResponse
import com.bokdoc.pateintsbook.data.webservice.login.LoginWebservice
import com.bokdoc.pateintsbook.data.webservice.login.encoder.LoginSocialsEncoder
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.register.RegisterWebservice
import com.bokdoc.pateintsbook.data.webservice.register.encoder.UserRegisterEncoder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class UserRegisterRepository(val context: Context) {
    val userRegisterWebservice = RegisterWebservice()
    val userSharedPreference = UserSharedPreference(context)

    fun register(email: String, password: String, phone: String, name: String): LiveData<DataResource<UserResponse>> {
        val userJson = UserRegisterEncoder.getJson(email, password, phone, name)
        val liveData = MutableLiveData<DataResource<UserResponse>>()
        val callable = userRegisterWebservice.register(userJson, userSharedPreference.sharedPrefrenceManger.getData(context.getString(R.string.token), ""))

        callable.enqueue(UserCustomResponse({
            if(it.status == DataResource.SUCCESS && it.data!=null && it.data?.isNotEmpty()!!){
                userRegisterWebservice.webserviceManagerImpl.formatBaseUrl(it.data!![0].countryString,
                        it.data!![0].languageString,it.data!![0].currencyString)
            }
            liveData.value = it
        }, arrayOf(UserResponse::class.java, Country::class.java, Currency::class.java, Language::class.java),
                WeakReference(userSharedPreference), WeakReference(context)))
        return liveData
    }

    fun registerWithSocials(providerName: String, token: String, providerId: String, secret: String = "",
                            name: String, picture: String, email: String): LiveData<DataResource<UserResponse>> {

        val body = LoginSocialsEncoder.getJson(providerId, token, secret, email, name, picture)
        Log.i("socialBody", body)
        val callable = userRegisterWebservice.loginWithSocials(providerName, body,
                userSharedPreference.sharedPrefrenceManger
                .getData(context.getString(R.string.token), ""))
        val liveData = MutableLiveData<DataResource<UserResponse>>()

        callable.enqueue(UserCustomResponse({
            if(it.status == DataResource.SUCCESS && it.data!=null && it.data?.isNotEmpty()!!){
                userRegisterWebservice.webserviceManagerImpl.formatBaseUrl(it.data!![0].countryString,
                        it.data!![0].languageString,it.data!![0].currencyString)
            }
            liveData.value = it
        }, arrayOf(UserResponse::class.java, Country::class.java, Currency::class.java, Language::class.java),
                WeakReference(userSharedPreference), WeakReference(context)))


        return liveData

    }
}