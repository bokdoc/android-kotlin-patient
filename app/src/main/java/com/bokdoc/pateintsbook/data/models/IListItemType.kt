package com.bokdoc.pateintsbook.data.models

interface IListItemType {
    var type: Types
        get() = Types.ITEM
        set(value) {}

    enum class Types{
        ITEM,PAGINATION
    }
}