package com.bokdoc.pateintsbook.data.models.promo

class PromoCode : PromoCodeInfo {

    override var bookSecondaryDuration: Boolean? = null
    override var coupon: String = ""
    override var servicePivotId: String = ""
    override var servicePivotType: String = ""
    override var patientId: String = ""
}