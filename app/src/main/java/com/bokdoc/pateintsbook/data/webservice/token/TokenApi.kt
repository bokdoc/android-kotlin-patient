package com.bokdoc.pateintsbook.data.webservice.token

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Header
import retrofit2.http.POST

interface TokenApi {
    @POST("auth/get-token")
    fun generateToken(@Header("DEVICE-ID")deviceId:String):Call<ResponseBody>
}