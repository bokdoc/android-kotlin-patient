package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.QualificationsData
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.InsuranceCarriersRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.OverviewRow

object OverviewConverter {

    fun convert(singleSurgeryResource: HealthProviderProfile): OverviewRow {
        val overviewRow = OverviewRow()

        overviewRow.consultingFees = singleSurgeryResource.onlineConsultationFees
        overviewRow.consultingBookButton = singleSurgeryResource.bookButton
        overviewRow.name = singleSurgeryResource.name
        overviewRow.location = singleSurgeryResource.location
        overviewRow.doctorsNumbers = singleSurgeryResource.doctorsCount
        overviewRow.likesNumbers = singleSurgeryResource.likesCount
        overviewRow.picture = singleSurgeryResource.picture.url
        overviewRow.departmentsNumbers = singleSurgeryResource.departmentsCount
        overviewRow.establishBirthDate = singleSurgeryResource.birthDate
        overviewRow.speciality = singleSurgeryResource.mainSpeciality
        overviewRow.gender = singleSurgeryResource.gender
        overviewRow.rating = singleSurgeryResource.rating
        overviewRow.bedsNumber = singleSurgeryResource.bedsNumber
        overviewRow.reviewsNumbers = singleSurgeryResource.reviewsCount
        overviewRow.ambulanceCount = singleSurgeryResource.ambulanceCount
        overviewRow.insuranceCarriersRow = InsuranceCarrierConverter.convert(singleSurgeryResource) as ArrayList<InsuranceCarriersRow>

        singleSurgeryResource.consulting?.get(singleSurgeryResource.document)?.let {
            if(it.isNotEmpty() && it[0].bookButton!=null){
                overviewRow.bookButton = it[0].bookButton
            }
        }

        if (singleSurgeryResource.review != null) {
            singleSurgeryResource.review!!.get(singleSurgeryResource.document).forEach {
                val feedback = Feedback()
                feedback.content = it.content
                feedback.name = it.personaResource?.get(singleSurgeryResource.document)?.name
                feedback.picture = it.personaResource?.get(singleSurgeryResource.document)?.picture!!.url
                feedback.rate = it.rate
                feedback.date = it.date_time.date
                feedback.time = it.date_time.time
                overviewRow.reviews.add(feedback)
            }
        }

        if (singleSurgeryResource.accommodations != null && singleSurgeryResource.accommodations!!.size() != 0) {
            singleSurgeryResource.accommodations!!.get(singleSurgeryResource.document)[0].let {
                overviewRow.accommodations.rooms = it.rooms
                overviewRow.accommodations.accompaniedPersons = it.accompaniedPersons
                overviewRow.accommodations.meals = it.meals
                overviewRow.accommodations.patientRoom = it.patientRoom
            }
        }
        if (singleSurgeryResource.accreditation != null) {

            singleSurgeryResource.accreditation!!.get(singleSurgeryResource.document)[0].let {
                overviewRow.accreditation.add(AccreditationRow(it.title, it.icon, it.description, it.slug))
            }
        }
        if (singleSurgeryResource.languagesSpoken != null) {
            singleSurgeryResource.languagesSpoken!!.get(singleSurgeryResource.document).forEach {
                overviewRow.spokenLanguage.add(it.name)
            }
        }
        if (singleSurgeryResource.paymentOptions != null) {
            singleSurgeryResource.paymentOptions!!.get(singleSurgeryResource.document).forEach {
                overviewRow.paymentOptions.add(it.name)
            }

        }

        if (singleSurgeryResource.educations != null) {
            singleSurgeryResource.educations!!.get(singleSurgeryResource.document).forEach {
                val qualificationsData = QualificationsData()
                qualificationsData.content = it.content
                qualificationsData.university_id = it.universityId
                qualificationsData.years = it.years
                overviewRow.education.add(qualificationsData)
            }
        }

        if (singleSurgeryResource.certifications != null) {
            singleSurgeryResource.certifications!!.get(singleSurgeryResource.document).forEach {
                val qualificationsData = QualificationsData()
                qualificationsData.content = it.content
                qualificationsData.university_id = it.universityId
                qualificationsData.years = it.years
                qualificationsData.title = it.title
                overviewRow.certification.add(qualificationsData)
            }
        }

        if (singleSurgeryResource.publications != null) {
            singleSurgeryResource.publications!!.get(singleSurgeryResource.document).forEach {
                val qualificationsData = QualificationsData()
                qualificationsData.content = it.content
                qualificationsData.link = it.link
                qualificationsData.title = it.title
                overviewRow.publication.add(qualificationsData)
            }
        }


        if (singleSurgeryResource.memberships != null) {
            singleSurgeryResource.memberships!!.get(singleSurgeryResource.document).forEach {
                val qualificationsData = QualificationsData()
                qualificationsData.content = it.content
                qualificationsData.title = it.title
                overviewRow.membership.add(qualificationsData)
            }
        }




        return overviewRow
    }
}