package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.departments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.department.Department
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ClinicsConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.DepartmentsConverter
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl.Companion.SERVER_CODE_200
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.departments.DepartmentsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.ProfileType
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class DepartmentsRepository(private val appContext: Context,
                            private val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                            private val departmentsWebservice: DepartmentsWebservice) {

    fun getDepartments(id: String): LiveData<DataResource<DepartmentRow>> {

        val liveData = MutableLiveData<DataResource<DepartmentRow>>()

        departmentsWebservice.getDepartments(id, sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""))
                .enqueue(object : Callback<ResponseBody> {

                    val dataResource = DataResource<DepartmentRow>()
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        dataResource.message = t?.message!!
                        liveData.value = dataResource
                        dataResource.status = DataResource.FAIL
                    }
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.code() == SERVER_CODE_200) {
                            val json = response!!.body()!!.string()
                            val clinics = Parser.parseJsonObject(json, arrayOf(HealthProviderProfile::class.java, Departments::class.java))
                            dataResource.data = DepartmentsConverter.convert(clinics as HealthProviderProfile)
                            dataResource.status = DataResource.SUCCESS
                            liveData.value = dataResource
                        } else {
                            dataResource.status = DataResource.FAIL
                            dataResource.message = response?.errorBody()!!.string()
                        }
                        liveData.value = dataResource
                    }

                })

        return liveData
    }
}