package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HospitalDoctor
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DoctorRow

object DoctorsConverter {

    fun convert(hospitalDoctor: List<HospitalDoctor>?, isDepartment: Boolean = false): List<DoctorRow> {
        val doctors = ArrayList<DoctorRow>()
        when (isDepartment) {
            true -> {
                hospitalDoctor?.forEach {
                    val doctorRow = DoctorRow()
                    doctorRow.id = it.id
                    doctorRow.name = it.name
                    doctorRow.bookButton = it.bookButton
                    doctorRow.gender = it.gender
                    doctorRow.photo = it.picture.url
                    doctorRow.speciality = it.mainSpeciality

                    if (it.languagesSpoken != null) {
                        it.languagesSpoken!!.get(it.document).forEach {
                            doctorRow.spokenLanguage.add(it.name)
                        }
                    }
                    doctors.add(doctorRow)
                }
            }
            false -> {
                hospitalDoctor?.forEach {
                    val doctorRow = DoctorRow()
                    doctorRow.id = it.id
                    doctorRow.name = it.name
                    doctorRow.gender = it.gender
                    doctorRow.bookButton = it.bookButton

                    doctorRow.photo = it.picture.url
                    doctorRow.speciality = it.mainSpeciality
                    if (it.languagesSpoken != null) {
                        it.languagesSpoken!!.get(it.document).forEach {
                            doctorRow.spokenLanguage.add(it.name)
                        }
                    }
                    doctors.add(doctorRow)
                }
            }
        }
        return doctors
    }
}