package com.bokdoc.pateintsbook.data.webservice.models

import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "lookup")
class AddLookupsResource : Resource() {
    var diseases: HasOne<Disease>? = null
    var medications: HasOne<Medication>? = null
}


@JsonApi(type = "lookup")
class LookupSpeciality : Resource() {
    var name: String = ""

}