package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.doctors

import com.bokdoc.pateintsbook.data.models.healthProviders.doctor.Doctor
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.FeesImpl
import com.bokdoc.pateintsbook.data.webservice.models.DoctorResource
import org.json.JSONObject

object DoctorsParser {
    const val NAME = "name"
    const val LOCATION = "location"
    const val PICTURE = "picture"
    const val RATING = "rating"
    const val FEES = "fees"
    const val AMOUNT = "amount"
    const val CURRENCY_SYMBOLS = "currency_symbols"
    const val ID = "id"
    const val ATTRIBUTES = "attributes"
    const val INCLUDED = "included"
    const val PROFILE_TYPE = "profile"
    const val TYPE = "type"

    fun parse(json:String):List<Doctor>{
        val doctors = ArrayList<Doctor>()
        val doctorsArray = JSONObject(json).getJSONArray(INCLUDED)
        var doctor:DoctorResource
        val size = doctorsArray.length()
        var fee: FeesImpl
        (0 until size).forEach {
            doctor = DoctorResource()
            (doctorsArray[it] as JSONObject).let { doctorObject->
                if(doctorObject.getString(TYPE) == PROFILE_TYPE) {
                    doctor.id = doctorObject.getString(ID)

                    //attributes object
                    doctorObject.getJSONObject(ATTRIBUTES).let { attributesObject ->
                        doctor.picture = attributesObject.getString(PICTURE)
                        doctor.name = attributesObject.getString(NAME)
                        doctor.location = attributesObject.getString(LOCATION)
                        doctor.rating = attributesObject.getInt(RATING)

                        //fees object
                        attributesObject.getJSONObject(FEES).let { feesObject ->
                            fee = FeesImpl()
                            fee.amount = feesObject.getDouble(AMOUNT)
                            fee.symbols = feesObject.getString(CURRENCY_SYMBOLS)
                            doctor.fee = fee
                        }

                    }
                    doctors.add(doctor)
                }
            }
        }
        return doctors
    }
}