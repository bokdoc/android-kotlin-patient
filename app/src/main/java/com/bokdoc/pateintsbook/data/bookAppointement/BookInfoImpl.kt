package com.bokdoc.pateintsbook.data.bookAppointement

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.Location

class BookInfoImpl() : BookInfo, Parcelable {
    override var surgeriesMedia: List<MediaParcelable>? = ArrayList()
    override var serviceType: String = ""
    override var location: Location? = Location()
    override var picture: String = ""
    override var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()
    override var profileTypeId: Int = 0
    override var name: String = ""
    override var bookType: String = ""
    override var profileId = 0
    override var patientImage: String = ""
    override var patientName = ""
    override var patientMobile = ""
    override var patientEmail = ""
    override var patientGender = ""
    override var patientAge = ""
    override var paymentTypeId: Int? = 0
    override var paymentTypeMethod: Int? = 0
    override var serviceId: Int? = 0
    override var clinicId: Int? = 0
    override var departmentId: Int? = 0
    override var surgeriesCaseDescription: String? = ""
    override var code: String? = null
    override var doctorId: Int? = 0
    override var profileTypeName: String = ""
    override var specialty: String = ""
    override var reservationDate: String = ""
    override var reservationTime: String? = ""
    override var servicePivotId: Int = 0
    override var servicePivotType: String = ""
    override var nextAvailabilityId: String = ""
    override var deviceType: String = ""
    override var bookRequestType: String = ""
    override var discountId: String? = null
    override var coupon: String? = null

    override var bookSecondaryDuration: Boolean?=null

    constructor(parcel: Parcel) : this() {
        surgeriesMedia = parcel.createTypedArrayList(MediaParcelable)
        serviceType = parcel.readString()
        location = parcel.readParcelable(Location::class.java.classLoader)
        picture = parcel.readString()
        nextAvailabilities = parcel.createTypedArrayList(NextAvailabilitiesParcelable)
        profileTypeId = parcel.readInt()
        name = parcel.readString()
        bookType = parcel.readString()
        profileId = parcel.readInt()
        patientImage = parcel.readString()
        patientName = parcel.readString()
        patientMobile = parcel.readString()
        patientEmail = parcel.readString()
        patientGender = parcel.readString()
        patientAge = parcel.readString()
        paymentTypeId = parcel.readInt()
        paymentTypeMethod = parcel.readInt()
        serviceId = parcel.readInt()
        clinicId = parcel.readInt()
        departmentId = parcel.readInt()
        surgeriesCaseDescription = parcel.readString()
        code = parcel.readString()
        doctorId = parcel.readInt()
        profileTypeName = parcel.readString()
        specialty = parcel.readString()
        reservationDate = parcel.readString()
        reservationTime = parcel.readString()
        servicePivotId = parcel.readInt()
        servicePivotType = parcel.readString()
        nextAvailabilityId = parcel.readString()
        deviceType = parcel.readString()
        bookRequestType = parcel.readString()
        discountId = parcel.readString()
        coupon = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(surgeriesMedia)
        parcel.writeString(serviceType)
        parcel.writeParcelable(location, flags)
        parcel.writeString(picture)
        parcel.writeTypedList(nextAvailabilities)
        parcel.writeInt(profileTypeId)
        parcel.writeString(name)
        parcel.writeString(bookType)
        parcel.writeInt(profileId)
        parcel.writeString(patientImage)
        parcel.writeString(patientName)
        parcel.writeString(patientMobile)
        parcel.writeString(patientEmail)
        parcel.writeString(patientGender)
        parcel.writeString(patientAge)
        if (paymentTypeId != null)
            parcel.writeInt(paymentTypeId!!)

        if (paymentTypeMethod != null)
            parcel.writeInt(paymentTypeMethod!!)

        if (serviceId != null)
            parcel.writeInt(serviceId!!)

        if (clinicId != null)
            parcel.writeInt(clinicId!!)

        if (departmentId != null)
            parcel.writeInt(departmentId!!)

        parcel.writeString(surgeriesCaseDescription)
        parcel.writeString(code)
        if (doctorId != null)
            parcel.writeInt(doctorId!!)
        parcel.writeString(profileTypeName)
        parcel.writeString(specialty)
        parcel.writeString(reservationDate)
        parcel.writeString(reservationTime)
        parcel.writeInt(servicePivotId)
        parcel.writeString(servicePivotType)
        parcel.writeString(nextAvailabilityId)
        parcel.writeString(deviceType)
        parcel.writeString(bookRequestType)
        parcel.writeString(discountId)
        parcel.writeString(coupon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BookInfoImpl> {
        override fun createFromParcel(parcel: Parcel): BookInfoImpl {
            return BookInfoImpl(parcel)
        }

        override fun newArray(size: Int): Array<BookInfoImpl?> {
            return arrayOfNulls(size)
        }
    }


}