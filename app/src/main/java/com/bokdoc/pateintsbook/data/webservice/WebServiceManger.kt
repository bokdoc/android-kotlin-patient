package com.bokdoc.pateintsbook.data.webservice


interface WebServiceManger {
    //fun addConverterFactory(jsonConverterFactory: JSONConverterFactory)
    fun <S> createService(serviceClass: Class<S>, token: String): S

    fun <S> createService(serviceClass: Class<S>): S
}