package com.bokdoc.pateintsbook.data.models.forgetPassword

import android.os.Parcel
import android.os.Parcelable

class ForgetPassword() :Parcelable {

    var mobileNumber: String = ""
    var mobileVerificationCode: String = ""
    var newPassword: String = ""

    constructor(parcel: Parcel) : this() {
        mobileNumber = parcel.readString()
        mobileVerificationCode = parcel.readString()
        newPassword = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mobileNumber)
        parcel.writeString(mobileVerificationCode)
        parcel.writeString(newPassword)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ForgetPassword> {
        override fun createFromParcel(parcel: Parcel): ForgetPassword {
            return ForgetPassword(parcel)
        }

        override fun newArray(size: Int): Array<ForgetPassword?> {
            return arrayOfNulls(size)
        }
    }
}