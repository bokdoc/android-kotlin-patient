package com.bokdoc.pateintsbook.data.repositories.search

import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompleteResultsActivity
import com.bokdoc.pateintsbook.utils.constants.SearchScreenTypes
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants

class SearchRepository(val context: Context) {

    var sharedManger: SharedPreferencesManagerImpl = SharedPreferencesManagerImpl(context)
    var screenType: String = ""
    var specialities = "0"
    var specialityParams = "0"
    var regionCountry: String = "0"
    var regionCountryParams = "0"
    var insuranceCarrier: String = "0"
    var insuranceCarrierParams = "0"
    /*
    var specialityParams = sharedManger.getData(context.getString(R.string.specialitiesParams),"")
    var regionCountry: String = sharedManger.getData(context.getString(R.string.regionCountry), "")
    var regionCountryParams = sharedManger.getData(context.getString(R.string.regionCountryParams),"")
    var insuranceCarrier: String = sharedManger.getData(context.getString(R.string.insurance), "")
    var insuranceCarrierParams = sharedManger.getData(context.getString(R.string.insuranceParams),"")
    */

    init {
        screenType = sharedManger.getData(context.getString(R.string.screenType), "")
        get()
    }


    fun save() {
        sharedManger.saveData(context.getString(R.string.screenType),screenType)
        sharedManger.saveData(screenType, specialities+","+specialityParams+","+
                        regionCountry+","+regionCountryParams+","+insuranceCarrier+","+insuranceCarrierParams)
        /*
        sharedManger.saveData(screenType,HashSet<String>().apply {
            add(specialities)
            add(specialityParams)
            add(regionCountry)
            add(regionCountryParams)
            add(insuranceCarrier)
            add(insuranceCarrierParams)
        })
        */
        /*
        sharedManger.saveData(context.getString(R.string.regionCountry), regionCountry)
        sharedManger.saveData(context.getString(R.string.regionCountryParams), regionCountryParams)
        sharedManger.saveData(context.getString(R.string.specialitiesType), speciality)
        sharedManger.saveData(context.getString(R.string.specialitiesParams), specialityParams)
        sharedManger.saveData(context.getString(R.string.insurance), insuranceCarrier)
        sharedManger.saveData(context.getString(R.string.insuranceParams), insuranceCarrierParams)
        */
    }


    fun get(){
        val values = sharedManger.getData(screenType,"")
        if(values.isNotEmpty()){
            values.split(",").let {
                specialities = it[0]
                specialityParams = it[1]
                regionCountry = it[2]
                regionCountryParams = it[3]
                insuranceCarrier = it[4]
                insuranceCarrierParams = it[4]
            }

        }else{
            specialities = "0"
            specialityParams = "0"
            regionCountry = "0"
            regionCountryParams = "0"
            insuranceCarrier = "0"
            insuranceCarrierParams = "0"
        }
    }


    fun reload() {
        screenType = sharedManger.getData(context.getString(R.string.screenType), "")
        get()
    }



    fun getQuery(): String {
        val selectedTexts = ArrayList<String>()
        if (specialities.isNotEmpty() && specialities!="0") {
            selectedTexts.add(specialityParams)
        }

        if (regionCountry.isNotEmpty() && regionCountry!="0") {
            selectedTexts.add(regionCountryParams)
        }

        if (insuranceCarrier.isNotEmpty() && insuranceCarrier!="0") {
            selectedTexts.add(insuranceCarrierParams)
        }

        val size = selectedTexts.size

        var query = ""
        (0 until size).forEach {
            query += selectedTexts[it]
            if (it != size - 1)
                query += WebserviceConstants.COMMA
        }
        return query
    }


}