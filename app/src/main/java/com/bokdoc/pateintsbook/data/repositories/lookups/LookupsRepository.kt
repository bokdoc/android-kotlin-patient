package com.bokdoc.pateintsbook.data.repositories.lookups

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.LookupsConverter
import com.bokdoc.pateintsbook.data.models.AddLookup
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.lookups.LookupsWebservice
import com.bokdoc.pateintsbook.data.webservice.lookups.encoder.AddLookupEncoder
import com.bokdoc.pateintsbook.data.webservice.models.*
import moe.banana.jsonapi2.Resource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class LookupsRepository(private val appContext: Context,
                             private val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                             private val lookupsWebservice: LookupsWebservice) {

    lateinit var parserClass:Class<out Resource>

    fun lookups(query: String, parserClass: Class<out Resource>): LiveData<DataResource<LookupsType>> {
        val liveData = MutableLiveData<DataResource<LookupsType>>()
        this.parserClass = parserClass
        val dataResource = DataResource<LookupsType>()
        lookupsWebservice.getLookups(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""),
                query).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val lookups = Parser.parseJson(response.body()!!.string(), arrayOf(LookupsResource::class.java, parserClass))

                    dataResource.data = LookupsConverter.getLookups(lookups as List<LookupsResource>)
                    liveData.value = dataResource
                } else {
                    dataResource.message = response?.message()!!
                    liveData.value = dataResource
                }
            }

        })
        return liveData
    }


    fun lookups(query: String): LiveData<DataResource<LookupsType>> {
        val liveData = MutableLiveData<DataResource<LookupsType>>()
        val dataResource = DataResource<LookupsType>()
        lookupsWebservice.getLookups(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""),
                query).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val lookups = Parser.parseJson(response.body()!!.string(), arrayOf(LookupsResource::class.java,
                            Disease::class.java, Specialties::class.java, Medication::class.java,Currency::class.java,
                            Language::class.java,Country::class.java))

                    dataResource.data = LookupsConverter.getLookups(lookups as List<LookupsResource>)
                    liveData.value = dataResource
                } else {
                    dataResource.message = response?.message()!!
                    liveData.value = dataResource
                }
            }

        })
        return liveData
    }


    fun addLookups(addLookup: AddLookup): LiveData<DataResource<LookupsType>> {
        val addJson: String = AddLookupEncoder.getJson(addLookup)
        Log.i("disease",addJson)
        val liveData = MutableLiveData<DataResource<LookupsType>>()
        val dataResource = DataResource<LookupsType>()
            lookupsWebservice.addLookup(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), addJson).
                    enqueue(CustomResponse<LookupsResource>({
                        if(it.status == DataResource.SUCCESS && it.data!=null){
                            dataResource.data = it.data as List<LookupsType>
                        }
                        dataResource.status = it.status
                        dataResource.meta = it.meta
                        dataResource.errors = it.errors
                        dataResource.message = it.message
                        liveData.value = dataResource
                    }, arrayOf(LookupsResource::class.java,Disease::class.java,Medication::class.java)))
        return liveData
    }
}