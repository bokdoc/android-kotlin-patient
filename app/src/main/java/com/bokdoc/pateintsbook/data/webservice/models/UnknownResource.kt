package com.bokdoc.pateintsbook.data.webservice.models

import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "default")
class UnknownResource:Resource()