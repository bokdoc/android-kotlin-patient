package com.bokdoc.pateintsbook.data.webservice.models
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource
@JsonApi(type = "type")
class LoginCredentials : Resource() {
    @Json(name = "email_mobile")
    lateinit var email: String
    lateinit var password: String
    //used to decide which app user login from partner or patient
    @Json(name = "user_type")
    var userType = "patient"
}