package com.bokdoc.pateintsbook.data.webservice.lookups

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class LookupsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {
    fun getLookups(token:String,query:String):Call<ResponseBody>{
        return webserviceManagerImpl.createService(LookUpsApis::class.java,token).lookups(query)
    }

    fun addLookup(token: String, lookup: String): Call<ResponseBody> {
        val requestBody = RequestBody.create(MediaType.parse("application/json"), lookup)
        return webserviceManagerImpl.createService(LookUpsApis::class.java, token).addLookup(requestBody)
    }


    companion object {
        const val DISEASES_QUERY = "diseases"
        const val MEDICATIONS_QUERY = "medications"
        const val SPECIALTIES_QUERY = "specialties"
    }
}