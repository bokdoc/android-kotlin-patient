package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

open abstract class ServicesWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {

    abstract fun getQuery(): String

    fun getServices(token: String, id: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token).getProfileSection(id, getQuery())
    }

}