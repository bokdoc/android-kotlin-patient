package com.bokdoc.pateintsbook.data.webservice.patientRequests

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class RequestsWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getRequests(id: String, token: String, serviceType: String,page:Int): Call<ResponseBody> {
        val newRequestsApi = webserviceManagerImpl.createService(RequestsApi::class.java, token)

        val queriesMap = HashMap<String,String>()

        queriesMap["include"] = "parent"

        queriesMap["page"] = page.toString()

        if(serviceType.isNotEmpty())
            queriesMap["service_type"] = serviceType

        return newRequestsApi.getRequests(id,queriesMap)
    }

    fun cancelRequest(url: String,token:String,body:String): Call<ResponseBody> {
        val request = webserviceManagerImpl.createService(RequestsApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"),body)
        return request.cancelRequest(url,requestBody)
    }


}