package com.bokdoc.pateintsbook.data.webservice.nextAvailabilityTimes

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface NextAvailabilityApi {
    @GET
    fun show(@Url url: String): Call<ResponseBody>

}