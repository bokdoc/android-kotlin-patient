package com.bokdoc.pateintsbook.data.webservice.token

import okhttp3.ResponseBody
import retrofit2.Call

interface TokenWebservice {
    fun generateToken(deviceId:String):Call<ResponseBody>
}