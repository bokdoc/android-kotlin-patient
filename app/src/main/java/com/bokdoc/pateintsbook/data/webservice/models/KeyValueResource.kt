package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json

class KeyValueResource {
    @Json(name = "param")
    var key:String?=null
    @Json(name = "label")
    var value:String?=null
    @Json(name = "is_selected")
    var isSelected:Boolean?=null
}