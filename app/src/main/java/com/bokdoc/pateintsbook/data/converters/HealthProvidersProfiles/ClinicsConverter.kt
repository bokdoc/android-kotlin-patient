package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.converters.MediaConverter
import com.bokdoc.pateintsbook.data.converters.NextAvailabilitiesParcelableConverter
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.Clinic
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Profile

object ClinicsConverter {

    fun convert(allClinicsResource: List<Clinic>): List<ClinicRow> {
        val clinics = ArrayList<ClinicRow>()
        allClinicsResource.forEach {
            val clinicRow = ClinicRow()
            clinicRow.location = it.location
            clinicRow.id = it.id
            clinicRow.bookUrl = it.bookButton!!.endpointUrl

            it.parent?.let { type ->
                clinicRow.profileType = type.get(it.document).profileType.get(it.document).id.toInt()
            }

            clinicRow.name = it.name
            if (it.profile != null) {
                clinicRow.doctorName = it.profile!!.get(it.document).name
                clinicRow.doctorPhoto = it.profile!!.get(it.document).picture.url
                clinicRow.speciality = it.profile!!.get(it.document).specialityTitle
                clinicRow.gender = it.profile!!.get(it.document).gender
                if (it.profile!!.get(it.document).profileTitle != null)
                    clinicRow.doctorTitle = it.profile!!.get(it.document).profileTitle!!.get(it.document).name

            } else {
                if (clinicRow.profileType == Profile.DOCTOR) {
                    clinicRow.doctorName = it.parent!!.get(it.document).mainSpeciality
                    clinicRow.doctorPhoto = it.parent!!.get(it.document).picture.url
                    clinicRow.speciality = it.parent!!.get(it.document).mainSpeciality
                }
            }
            clinicRow.appointmentReservation = it.is_appointment_reservation
            clinicRow.feesCurrancy = it.fees.currency_symbols

            clinicRow.bookButton = it.bookButton
            clinicRow.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))

            if (it != null) {
                clinicRow.media = MediaConverter.convertMediaResource(it!!.media!!.get(it.document))
            }
            clinicRow.nationalFees = it.mainFees
            clinicRow.interNationalFees = it.subMainFees
            clinicRow.internationalFeesLabel = it.subMainFees.label
            clinicRow.nationalFeesLabel = it.mainFees.label

            clinics.add(clinicRow)
        }

        return clinics
    }

    fun convert(linksCardsDetails: LinksCardsDetails): List<ClinicRow> {
        val clinics = ArrayList<ClinicRow>()
        linksCardsDetails.rowsInfo.forEach {
            val clinicRow = ClinicRow()
            clinicRow.location = it.location
            clinicRow.id = it.id
            clinicRow.bookUrl = it.bookButton.endpointUrl
            clinicRow.profileType = linksCardsDetails.mainProfileInfo.profileType
            clinicRow.name = it.name

            if (it.profileInfo.profileId.isNotEmpty()) {
                clinicRow.doctorName = it.profileInfo.profileName
                clinicRow.doctorPhoto = it.profileInfo.profilePicture.url
                clinicRow.speciality = it.profileInfo.profileMainSpeciality
                clinicRow.gender = it.profileInfo.gender
            } else {
                if (clinicRow.profileType == Profile.DOCTOR) {
                    clinicRow.doctorName = linksCardsDetails.mainProfileInfo.profileName
                    clinicRow.doctorPhoto = linksCardsDetails.mainProfileInfo.profilePicture.url
                    clinicRow.speciality = it.profileInfo.profileMainSpeciality
                }
            }
            clinicRow.nationalFees = it.mainFees
            clinicRow.interNationalFees = it.subMainFees
            clinicRow.internationalFeesLabel = it.subMainFees.label
            clinicRow.nationalFeesLabel = it.mainFees.label

            it.mainFees.fixed_fees?.currency_symbols?.let {
                clinicRow.feesCurrancy = it
            }

            clinicRow.nextAvailabilities = it.nextAvailabilities
            clinicRow.bookButton = it.bookButton
            clinicRow.profileType = linksCardsDetails.mainProfileInfo.profileType

            clinics.add(clinicRow)
        }

        return clinics
    }
}

