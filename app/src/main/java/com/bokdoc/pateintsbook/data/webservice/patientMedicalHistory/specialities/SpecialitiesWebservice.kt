package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.specialities

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice

class SpecialitiesWebservice(webserviceManagerImpl: WebserviceManagerImpl):
        MedicalHistoryWebservice(webserviceManagerImpl) {
    override fun getType(): String {
        return "specialities"
    }
}