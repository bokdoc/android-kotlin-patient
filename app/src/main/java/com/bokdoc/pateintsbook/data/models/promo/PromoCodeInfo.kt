package com.bokdoc.pateintsbook.data.models.promo

interface PromoCodeInfo {
    var coupon: String
    var servicePivotId: String
    var servicePivotType: String
    var patientId: String
    var bookSecondaryDuration:Boolean?



}