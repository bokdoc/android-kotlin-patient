package com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.PUT
import retrofit2.http.Path

interface ChangeCurrencyLanguagesApis {

    @PUT("patients/edit/{id}")
    fun change(@Path("id") id: String, @Body requestBody: RequestBody): Call<ResponseBody>
}