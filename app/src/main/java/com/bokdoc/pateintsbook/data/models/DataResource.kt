package com.bokdoc.pateintsbook.data.models

import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.models.meta.PaginationMeta

class DataResource<T> {
    var data: List<T>? = null
    var meta: Meta = Meta()
    var paginationMeta: PaginationMeta = PaginationMeta()
    var status: String = ""
    var message: String = ""
    var code:String=""
    var errors:List<CustomError> = ArrayList()


    companion object {
        const val SUCCESS = "200"
        const val FAIL = "Fail"
        const val FAIL_NO_INTERNET = "noInternet"
        const val FAIL_TIME_OUT = "timeOut"
        const val INTERNAL_SERVER_ERROR = "500"
        const val VALIDATION_ERROR = "422"
    }
}