package com.bokdoc.pateintsbook.data.webservice.models

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "media")
class AttachmentMediaResource :Resource(),IMedia {
    @Transient
    override var mediaId: String=""
        get() {
            return id
        }

    @Transient
    override var name: String = ""

    @Json(name = "media_type")
    override var mediaType: String = ""

    @Json(name = "media_url")
    override var url: String = ""
}