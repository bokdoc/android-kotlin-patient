package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.Picture
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "consultingStream")
class ConsultingStreamResource : Resource() {

    var request_id: String = ""
    var room_name: String = ""
    var chat_url: String = ""
    var leave_url: String = ""
    var button_title: String = ""
    var duration: String = ""
    var chat_token = ChatToken()
    var video_token = ChatToken()
    var date_time = DateTime()
    var persona = ConsultingDoctroData()


}

class DateTime() {
    var time: String = ""
    var date: String = ""
}

class ConsultingDoctroData {
    var name: String = ""
    var title: String = ""
    var picture = ConsultingMedia()
}

class ConsultingMedia() {
    var media_url: String = ""
}

class ChatToken {
    var identity: String = ""
    var token: String = ""
}