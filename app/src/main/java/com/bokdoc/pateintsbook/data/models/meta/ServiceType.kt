package com.bokdoc.pateintsbook.data.models.meta

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ServiceType : Serializable {

    @SerializedName("scan")
    @Expose
    val scan: String? = null
    @SerializedName("appointment")
    @Expose
    val appointment: String? = null
    @SerializedName("test")
    @Expose
    val test: String? = null
    @SerializedName("home_visit")
    @Expose
    val homeVisit: String? = null
    @SerializedName("surgery")
    @Expose
    val surgery: String? = null
    @SerializedName("consulting")
    @Expose
    val consulting: String? = null
}