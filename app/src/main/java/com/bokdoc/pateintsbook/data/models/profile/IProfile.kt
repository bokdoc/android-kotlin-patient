package com.bokdoc.pateintsbook.data.models.profile

import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.lookups.ILookupsParcelable

interface IProfile {
    var profileId:String?
    var name:String?
    var picture:Picture?
    var mainSpeciality:String?
    var specialityTitle:String?
    var profileTitle:ILookupsParcelable?
    var profileType:ILookupsParcelable?
}