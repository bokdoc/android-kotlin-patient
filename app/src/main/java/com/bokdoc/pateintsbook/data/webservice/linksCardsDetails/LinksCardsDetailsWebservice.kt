package com.bokdoc.pateintsbook.data.webservice.linksCardsDetails

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class LinksCardsDetailsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getDetails(token:String,url:String):Call<ResponseBody>{
        val detailsApis = webserviceManagerImpl.createService(LinksCardsDetailsApis::class.java,token)
        return detailsApis.getDetails(url)
    }

    fun getDetailsWithDiscountId(token:String,url:String,discountId:String):Call<ResponseBody>{
        val detailsApis = webserviceManagerImpl.createService(LinksCardsDetailsApis::class.java,token)
        return detailsApis.getDurationWithDiscountDetails(url,discountId)
    }
}