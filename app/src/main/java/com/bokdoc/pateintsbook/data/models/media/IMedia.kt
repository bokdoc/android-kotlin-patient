package com.bokdoc.pateintsbook.data.models.media

interface IMedia {
    var mediaId: String
    var url: String
    var name: String
    var status: Boolean
        get() = true
        set(value) {}
    var mediaType:String


    companion object {
        const val IMAGE_TYPE = "image"
        const val VIDEO_TYPE = "video"
        const val FILE_TYPE = "file"
    }
}