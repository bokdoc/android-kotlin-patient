package com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.MedicalHistoryConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice
import moe.banana.jsonapi2.Resource
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

open class MedicalHistoryRepository(appContext:Context,
                                       sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                                       private val medicalHistoryWebservice: MedicalHistoryWebservice,
                                       private val resourceParser:Class<out Resource>) {

    private var token = sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token),"")


    fun add(id:String):LiveData<DataResource<Boolean>>{
        val liveData = MutableLiveData<DataResource<Boolean>>()
        medicalHistoryWebservice.add(id,token).enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))

        return liveData
    }

    fun delete(id:String):LiveData<DataResource<Boolean>>{
        val liveData = MutableLiveData<DataResource<Boolean>>()
        medicalHistoryWebservice.delete(id,token).enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))

        return liveData
    }

    fun get(id:String):LiveData<DataResource<MedicalHistory>>{
       val liveData = MutableLiveData<DataResource<MedicalHistory>>()
       medicalHistoryWebservice.get(id,token).enqueue(CustomResponse<MedicalHistory>({
           if(it.data!=null){
               it.data = MedicalHistoryConverter.convert(it.data!!)
           }
           liveData.value = it
       }, arrayOf(resourceParser)))

        return liveData
    }


    inner class GetMedicalHistoryResponse(private val liveData: MutableLiveData<DataResource<MedicalHistory>>): Callback<ResponseBody> {
        private val dataResource = DataResource<MedicalHistory>()

        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            dataResource.message = t?.message!!
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if(response?.body()!=null) {
                val medicalHistories = Parser.parseJson(response.body()!!.string(), arrayOf(resourceParser)) as List<MedicalHistory>
                dataResource.data = medicalHistories
                liveData.value = dataResource
            }else{
                dataResource.message = response?.errorBody()!!.toString()
                liveData.value = dataResource
            }
        }
    }

    inner class AddedRemovedResponse(private val liveData: MutableLiveData<DataResource<String>>): Callback<ResponseBody> {
        private val dataResource = DataResource<String>()
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            dataResource.status = DataResource.FAIL
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if(response!!.isSuccessful) {
                dataResource.status = DataResource.SUCCESS
            }else{
                dataResource.status = DataResource.FAIL
            }
            liveData.value = dataResource
        }

    }

}