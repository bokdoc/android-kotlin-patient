package com.bokdoc.pateintsbook.data.webservice.addFcm

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AddFcmApi {
    @POST(WebserviceConstants.Add_FCM)
    fun addFcm(@Body credentialsJson: RequestBody): Call<ResponseBody>

    @POST(WebserviceConstants.CHECK_UPDATE)
    fun checkUpdate(@Body credentialsJson: RequestBody): Call<ResponseBody>


}