package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.CustomDateTime
import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.models.ListItemType
import com.bokdoc.pateintsbook.data.models.Picture
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "review")
class Review : Resource(),IListItemType {
    var rate: Int = 0
    var content: String = ""
    var featured: Int = 0
    var date_time: CustomDateTime = CustomDateTime()
    var persona: HasOne<Persona> = HasOne()
    override var type: IListItemType.Types = IListItemType.Types.ITEM
}

@JsonApi(type = "persona")
class Persona : Resource() {
    var name: String?=null
    //var rating: Int = 0
    var picture: Picture?=null

}