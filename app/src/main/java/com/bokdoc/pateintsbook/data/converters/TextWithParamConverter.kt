package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.data.webservice.models.Options

object TextWithParamConverter {
    fun convert(options: List<Options>):List<TextWithParam>{
        val size =  options.size
        val textsWithParams = ArrayList<TextWithParam>()
        (0 until size).forEach {
            textsWithParams.add(TextWithParam(options[it].name,options[it].params))
        }
        return textsWithParams
    }
}