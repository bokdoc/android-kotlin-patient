package com.bokdoc.pateintsbook.data.models.notifications

import com.bokdoc.pateintsbook.data.models.DateTime
import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.models.params.IParams
import com.bokdoc.pateintsbook.data.models.params.Params
import com.bokdoc.pateintsbook.data.models.profile.IProfile
import com.bokdoc.pateintsbook.data.models.profile.ProfileNotifications
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks

class Notifications : INotifications, IListItemType {
    override var notificationsId: String? = ""
    override var title: String? = ""
    override var content: String? = ""
    override var dateTime: DateTime? = null
    override var params: Params? = null
    override var actionUrl: List<DynamicLinks>? = null
    override var deleteUrl: String? = null
    override var profile: ProfileNotifications? = null
    override var type: IListItemType.Types = IListItemType.Types.ITEM
}