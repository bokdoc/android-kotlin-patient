package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.InsuranceCarriersRow

object InsuranceCarrierConverter {
    fun convert(allProfileResource: HealthProviderProfile): List<InsuranceCarriersRow> {
        val insuranceCarriers = ArrayList<InsuranceCarriersRow>()
        allProfileResource.insuranceCarriers?.get(allProfileResource.document)?.forEach {
            val insuranceCarriersRow = InsuranceCarriersRow(it.id, it.title, it.description)
            insuranceCarriers.add(insuranceCarriersRow)
        }
        return insuranceCarriers
    }


}