package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accommodation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.AccommodationsSectionsConverter
import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.AccommodationsConverter
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accommodation.AccommodationWebservice
 import com.bokdoc.pateintsbook.data.webservice.models.HealthProfileProvidersResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.Accommodation
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.ProfileType
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccommodationsRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProvidersProfileType
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccommodationRepository(private val appContext: Context,
                              private val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                              private val accommodationWebservice: AccommodationWebservice) {


    fun getAccommodations(id: String, service: String): LiveData<DataResource<AccommodationsRow>> {

        val liveData = MutableLiveData<DataResource<AccommodationsRow>>()
        val dataResource = DataResource<AccommodationsRow>()

        accommodationWebservice.getAccommodations(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (it.data!!.isNotEmpty()) {
                        dataResource.data = arrayListOf(AccommodationsConverter.convert(it.data!![0]))
                        //dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, Accommodation::class.java, ProfileType::class.java)))

        return liveData
    }
}


//        accommodationWebservice.getAccommodations(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id).enqueue(object : Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
//                val dataResource = DataResource<AccommodationSections>()
//                dataResource.message = t?.message!!
//                liveData.value = dataResource
//            }
//
//            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
//                if (response?.body() != null) {
//                    val profiles = Parser.parseJson(response.body()?.string()!!, arrayOf(HealthProfileProvidersResource::class.java,
//                            Accommodation::class.java, HealthProvidersProfileType::class.java))
//                    val dataResource = DataResource<AccommodationSections>()
//                    dataResource.data = AccommodationsSectionsConverter.convert(profiles[0] as HealthProfileProvidersResource)
//                    liveData.value = dataResource
//                } else {
//                    val dataResource = DataResource<AccommodationSections>()
//                    dataResource.message = response?.errorBody()!!
//                    liveData.value = dataResource
//                }
//            }
//
//        })
//
//        return liveData
//


