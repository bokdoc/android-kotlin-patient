package com.bokdoc.healthproviders.data.models.homeMenu

import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

class HomeMenu(val id: Int
               , val title: String
               , val image: Int)