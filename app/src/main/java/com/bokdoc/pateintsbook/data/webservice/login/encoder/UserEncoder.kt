package com.bokdoc.pateintsbook.data.webservice.login.encoder

import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.webservice.models.LoginCredentials
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object UserEncoder {

    fun getJson(userResponse: UserResponse): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<UserResponse>()


        userResponse.country.get(userResponse.document).let {
            if(it!=null)
                userResponse.countryLookup = LookUpsParcelable().apply {
                    idLookup = it.idLookup
                    name = it.name
                }
        }

        userResponse.language.get(userResponse.document).let {
            if(it!=null)
                userResponse.languageLookup = LookUpsParcelable().apply {
                    idLookup = it.idLookup
                    name = it.name
                }
        }

        userResponse.currency.get(userResponse.document).let {
            if(it!=null)
                userResponse.currencyLookup = LookUpsParcelable().apply {
                    idLookup = it.idLookup
                    name = it.name
                }
        }


        document.set(userResponse)

        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(LoginCredentials::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }

}