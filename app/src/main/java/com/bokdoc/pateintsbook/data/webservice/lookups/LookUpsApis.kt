package com.bokdoc.pateintsbook.data.webservice.lookups

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface LookUpsApis {
    @GET("lookup")
    fun lookups(@Query("include")query:String):Call<ResponseBody>

    @POST("lookup")
    fun addLookup(@Body lookup: RequestBody): Call<ResponseBody>
}