package com.bokdoc.pateintsbook.data.models.nextAvailabilities

import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks

interface INextAvailabilities {
    val idNextAvailabilities: String?
    val dateId: Int?
    val date: String?
    val date_day: String?
    val book_button: DynamicLinks?
    val to: String?
    val from: String?
    val status: Boolean
    val dateNormalized: String?
}

