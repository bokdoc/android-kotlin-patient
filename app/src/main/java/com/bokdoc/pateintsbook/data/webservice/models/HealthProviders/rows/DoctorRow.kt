package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.squareup.moshi.Json

class DoctorRow {

    var id: String = ""
    var name: String = ""
    var photo: String = ""
    var title: String = ""
    var speciality: String = ""
    var subSpeciality: String = ""
    var gender: String = ""
    var bookButton: DynamicLinks? = null
    var spokenLanguage: ArrayList<String> = ArrayList()
    var type: Int = 0
}