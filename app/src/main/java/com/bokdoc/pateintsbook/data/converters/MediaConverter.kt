package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.webservice.models.AttachmentMediaResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.ProfileAttachmentMediaResource


object MediaConverter {
    fun convert(mediaResource: List<IMedia>, isAssigned: Boolean = false): ArrayList<MediaParcelable> {
        val media = ArrayList<MediaParcelable>()
        mediaResource.forEach {
            media.add(MediaParcelable().apply {
                mediaId = it.mediaId
                name = it.name
                url = it.url
                status = it.status
                mediaType = it.mediaType
                //this.isAssigned = isAssigned
            })
        }
        return media
    }

    fun convertResource(mediaParc: MutableList<AttachmentMediaResource>, isAssigned: Boolean = false): ArrayList<MediaParcelable> {
        val media = ArrayList<MediaParcelable>()
        mediaParc.forEach {
            media.add(MediaParcelable().apply {
                mediaId = it.mediaId
                name = it.name
                url = it.url
                status = it.status
                mediaType = it.mediaType
                //this.isAssigned = isAssigned
            })
        }
        return media
    }


    fun convertMediaResource(mediaParc: MutableList<ProfileAttachmentMediaResource>, isAssigned: Boolean = false): ArrayList<MediaParcelable> {
        val media = ArrayList<MediaParcelable>()
        mediaParc.forEach {
            media.add(MediaParcelable().apply {
                mediaId = it.mediaId
                name = it.name
                url = it.url
                status = it.status
                mediaType = it.mediaType
                //this.isAssigned = isAssigned
            })
        }
        return media
    }
}