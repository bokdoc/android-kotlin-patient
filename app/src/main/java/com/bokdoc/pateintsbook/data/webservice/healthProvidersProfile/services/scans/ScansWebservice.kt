package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.scans

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.ServicesApiConstants
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.ServicesWebservice
import com.bokdoc.pateintsbook.utils.constants.StringsConstants

class ScansWebservice(webserviceManagerImpl: WebserviceManagerImpl):ServicesWebservice(webserviceManagerImpl) {

    override fun getQuery(): String {
        return StringsConstants.buildString(arrayListOf(ServicesApiConstants.SERVICES_TYPE,StringsConstants.OPEN_BRACKET,
                ServicesApiConstants.SCAN,StringsConstants.CLOSED_BRACKET))
    }


}