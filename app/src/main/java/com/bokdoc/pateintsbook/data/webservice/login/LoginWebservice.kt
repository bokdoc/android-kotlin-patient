package com.bokdoc.pateintsbook.data.webservice.login

import android.util.Log
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.login.encoder.LoginCredentialsEncoder
import okhttp3.Credentials
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Call
import okhttp3.RequestBody



open class LoginWebservice {

    val webserviceManagerImpl = WebserviceManagerImpl.instance()

    fun login(credentialsJson:String,token:String):Call<ResponseBody>{
        val loginApi = webserviceManagerImpl.createService(LoginApi::class.java,token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"),credentialsJson)
        return loginApi.login(requestBody)
    }

    fun resetPassword(credentialsJson:String,token:String):Call<ResponseBody>{
        val loginApi = webserviceManagerImpl.createService(LoginApi::class.java,token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"),credentialsJson)
        return loginApi.resetPassword(requestBody)
    }

    fun loginWithSocials(providerName:String,tokensJson:String,token: String,secret:String=""):Call<ResponseBody>{
        val loginApi = webserviceManagerImpl.createService(LoginApi::class.java,token)
        Log.i("token",token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"),tokensJson)
        return loginApi.loginWithSocials(providerName,requestBody)
    }
}