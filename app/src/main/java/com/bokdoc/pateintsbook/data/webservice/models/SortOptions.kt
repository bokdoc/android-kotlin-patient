package com.bokdoc.pateintsbook.data.webservice.models

import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "filter")
class SortOptions:Resource() {
    lateinit var sorts:HasMany<Options>
}

@JsonApi(type = "sort")
class Options:Resource(){
    lateinit var name:String
    lateinit var params:String
}