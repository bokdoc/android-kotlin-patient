package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource


@JsonApi(type = "filter")
class FiltersOptions:Resource() {
    lateinit var header:String
    lateinit var key:String
    lateinit var slug:String
    lateinit var dataType:String
    lateinit var specialities:HasMany<SpecialityFilter>
    lateinit var fees:HasMany<Fee>
    lateinit var insuranceCarriers:HasMany<InsuranceCarrierFilter>
    lateinit var yearsExperience:HasMany<YearsExperience>
    lateinit var paymentTypes:HasMany<PaymentTypeFilter>
    lateinit var genders:HasMany<Gender>
    lateinit var languages:HasMany<LanguageFiler>
    lateinit var rating:HasMany<Rating>
    lateinit var isHomeVisit:HasMany<IsHomeVisit>
    lateinit var freeAppointment:HasMany<FreeAppointment>
    lateinit var dates:HasMany<Date>
    lateinit var times:HasMany<TimeFilter>
    lateinit var map:HasMany<MapFilter>
    lateinit var profileTypes:HasMany<ProfileTypes>
    var clinicAppointmentReservation:HasMany<IsAppointmentReservation>?=null

    companion object {
        const val FEES_SLUG = "fees"
        const val YEARS_SLUG = "yearsExperience"
        const val SPECIALITIES_SLUG = "specialities"
        const val INSURANCE_CARRIERS_SLUG = "insuranceCarriers"
        const val PAYMENT_TYPES_SLUG = "paymentTypes"
        const val GENDERS = "genders"
        const val PROFILE_TYPES = "profileTypes"
        const val LANGUAGES_SLUG = "languages"
        const val RATING_SLUG = "rating"
        const val IS_HOME_VISIT_SLUG = "isHomeVisit"
        const val FREE_APPOINTMENT_SLUG = "freeAppointment"
        const val DATES_SLUG = "dates"
        const val TIMES_SLUG = "times"
        const val MAP_SLUG = "map"
        const val CLINIC_APPOINTMENT_RESERVATION = "clinicAppointmentReservation"
    }
}

open class FilterOption:Resource(){
    lateinit var name:String
    lateinit var params:String
}

open class FilterOptionUnit:FilterOption(){
    lateinit var unit:String
}


@JsonApi(type = "years_experience")
class YearsExperience:FilterOptionUnit()

@JsonApi(type = "speciality")
class SpecialityFilter:FilterOption()

@JsonApi(type = "fee")
class Fee:FilterOptionUnit()

@JsonApi(type = "insuranceCarrier")
class InsuranceCarrierFilter:FilterOption()

@JsonApi(type = "paymentType")
class PaymentTypeFilter:FilterOption()

@JsonApi(type = "gender")
class Gender:FilterOption()

@JsonApi(type = "language")
class LanguageFiler:FilterOption()

@JsonApi(type = "rating")
class Rating:FilterOption()

@JsonApi(type = "isHomeVisit")
class IsHomeVisit:FilterOption()

@JsonApi(type = "freeAppointment")
class FreeAppointment:FilterOption()

@JsonApi(type = "date")
class Date:FilterOption()

@JsonApi(type = "time")
class TimeFilter:FilterOptionUnit()

@JsonApi(type = "map")
class MapFilter:FilterOption()

@JsonApi(type = "profileType")
class ProfileTypes:FilterOption()


@JsonApi(type = "isAppointmentReservation")
class IsAppointmentReservation:FilterOption()

