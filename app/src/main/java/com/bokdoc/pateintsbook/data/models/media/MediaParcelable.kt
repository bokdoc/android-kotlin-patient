package com.bokdoc.pateintsbook.data.models.media

import android.os.Parcel
import android.os.Parcelable

class MediaParcelable(): IMediaParcelable {
    override var mediaId: String = ""
    override var name: String = ""
    override var status: Boolean = true
    override var mediaType: String = ""
    override var url: String = ""

    constructor(parcel: Parcel) : this() {
        mediaId = parcel.readString()
        name = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        mediaType = parcel.readString()
        url = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mediaId)
        parcel.writeString(name)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeString(mediaType)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MediaParcelable> {
        override fun createFromParcel(parcel: Parcel): MediaParcelable {
            return MediaParcelable(parcel)
        }

        override fun newArray(size: Int): Array<MediaParcelable?> {
            return arrayOfNulls(size)
        }
    }

}