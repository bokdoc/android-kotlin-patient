package com.bokdoc.pateintsbook.data.sharedPreferences

import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse

class UserSharedPreference(val context: Context) {

    private var user: UserResponse? = UserResponse()
    val sharedPrefrenceManger = SharedPreferencesManagerImpl(context)


    var userCurrency: LookUpsParcelable = LookUpsParcelable()
        get() {
            val userResponse = getUser()
            if (userResponse?.currencyLookup != null)
                return userResponse.currencyLookup

            return LookUpsParcelable()
        }

    var userLanguage: LookUpsParcelable = LookUpsParcelable()
        get() {
            val userResponse = getUser()
            if (userResponse?.languageLookup != null)
                return userResponse.languageLookup

            return LookUpsParcelable()
        }

    var userCountry: LookUpsParcelable = LookUpsParcelable()
        get() {
            val userResponse = getUser()
            if (userResponse?.countryLookup != null)
                return userResponse.countryLookup

            return LookUpsParcelable()
        }


    fun getUser(): UserResponse? {
        when (sharedPrefrenceManger.getData(context.getString(R.string.user_key), "")) {
            "" -> {
                return null
            }
            else -> {
                user = UserParser.parse(sharedPrefrenceManger.getData(context.getString(R.string.user_key), ""))
                return user as UserResponse
            }
        }
    }

    fun getCurrency():String{
        getUser()?.let {
            if(it.currencyString.isNotEmpty()){
                return it.currencyString
            }
            return "egp"
        }
        return "egp"
    }

    fun getCountry():String{
        getUser()?.let {
            if(it.countryString.isNotEmpty()){
                return it.countryString
            }
            return "eg"
        }
        return "eg"
    }


    fun ifUserLogin(): Boolean {
        var login: Boolean
        when (getUser()) {
            null -> login = false
            else -> login = !user!!.isGuest
        }
        return login
    }


    fun getToken(): String {
        return getUser()!!.token
    }

    fun getId(): String {
        return getUser()!!.id
    }

    fun userLogOut() {
        //sharedPrefrenceManger.clear(context.getString(R.string.user_key))
        sharedPrefrenceManger.clear(context.getString(R.string.token))
    }

    fun saveUser(userJson: String) {
        sharedPrefrenceManger.saveData(context.getString(R.string.user_key), userJson)
    }

}