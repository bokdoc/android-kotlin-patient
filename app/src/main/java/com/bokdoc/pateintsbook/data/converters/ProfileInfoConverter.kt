package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.patientProfile.ProfilesInfo
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Request

object ProfileInfoConverter {
    fun convert(profiles:List<Profile>):ArrayList<ProfilesInfo>{
        val profilesInfo = ArrayList<ProfilesInfo>()

        profiles.forEach {
            profilesInfo.add(ProfilesInfo().apply {
                id = it.id
                name = it.name
                picture = it.picture
            })
        }


        return profilesInfo
    }



}