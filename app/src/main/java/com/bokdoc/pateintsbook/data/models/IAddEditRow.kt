package com.bokdoc.pateintsbook.data.models

import android.net.Uri
import android.text.InputType
import android.text.Spannable
import android.text.SpannableString
import com.bokdoc.pateintsbook.R

interface IAddEditRow {
    var slug: String
        get() = ""
        set(value) {}
    var rowDescription: String
        get() = ""
        set(value) {}
    var type: Int
        get() = TEXT_ARROW_ROW
        set(value) {}
    var name: String
        set(value) {}
        get() = ""
    var description: String
        set(value) {}
        get() = ""
    var selectedText: String
        get() = ""
        set(value) {}
    var editTextTitle: String
        get() = ""
        set(value) {}
    var editTextValue: String
        get() = ""
        set(value) {}
    var switchValue: Boolean
        get() = false
        set(value) {}
    var addEditsRows: List<IAddEditRow>
        get() = ArrayList()
        set(value) {}
    var editTextHint: String
        get() = ""
        set(value) {}
    var spinnerValues: Array<String>
        get() = arrayOf()
        set(value) {}
    var isShowArrow: Boolean
        get() = true
        set(value) {}
    var isSelected: Boolean
        get() = false
        set(value) {}
    var isMandatory: Boolean
        get() = false
        set(value) {}
    var isShowOptionalText: Boolean
        get() = false
        set(value) {}

    var nameStatus: Int
        get() = NAME_STATUS_DEFAULT
        set(value) {}

    var background: Int
        get() = R.color.colorWhite
        set(value) {}

    var selectedImages:List<Uri>
    get() = ArrayList()
    set(value) {}

    var documentNames:List<String>
    get() = ArrayList()
    set(value) {}

    var visibility:Boolean
    get() = true
    set(value) {}

    var editTextInputType:Int
    get() = InputType.TYPE_CLASS_TEXT
    set(value) {}

    var editTextFrom:String
        get() = ""
        set(value) {}

    var editTextTo:String
        get() = ""
        set(value) {}

    var editTextFromHint:String
    get() = ""
    set(value) {}

    var editTextToHint:String
        get() = ""
        set(value) {}

    var editTextFromTitle:String
        get() = ""
        set(value) {}

    var editTextToTitle:String
        get() = ""
        set(value) {}

    var isShowLine:Boolean
    get() = true
    set(value) {}

    var nameSpannable: Spannable?
    get() = null
    set(value) {}

    var imagesRes:Array<Int>
    get() = arrayOf()
    set(value) {}

    var isFieldEnabled:Boolean
        get() = true
        set(value) {}

    companion object {
        const val TEXT_ARROW_ROW = 1
        const val MOBILE_NUMBER_ROW = 2
        const val TEXT_ARROW_EDIT = 3
        const val SWITCH_ROW = 4
        const val MULTI_ROWS = 5
        const val EDIT_TEXT_ROW = 6
        const val TEXT_ROW = 7
        const val SPINNER_ROW = 8
        const val TEXT_WITH_DESCRIPTION_ROW = 9
        const val PROFILE_ROW = 10
        const val TEXT_WITH_ARROW_MULTI_ROW = 11
        const val NAME_STATUS_DEFAULT = 12
        const val NAME_STATUS_SELECTED = 13
        const val NAME_STATUS_REQUIRED = 14
        const val TEXT_SELECTED_IMAGE_ROW = 15
        const val TEXT_WITH_SELECTED_ROW = 16
        const val ROUNDED_EDIT_TEXT_ROW = 17
        const val ROUNDED_RANGES_EDIT_TEXTS_ROW = 18
        const val CHECK_BOX_ROW = 19
        const val TITLE_ROW = 20
    }
}