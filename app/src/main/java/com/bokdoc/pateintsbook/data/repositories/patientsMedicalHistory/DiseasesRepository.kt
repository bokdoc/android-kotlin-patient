package com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory

import android.content.Context
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.medicalHistory.Disease
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice

class DiseasesRepository(appContext:Context,
                            sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                            diseasesWebservice: MedicalHistoryWebservice):
        MedicalHistoryRepository(appContext,sharedPreferencesManagerImpl,diseasesWebservice,Disease::class.java)