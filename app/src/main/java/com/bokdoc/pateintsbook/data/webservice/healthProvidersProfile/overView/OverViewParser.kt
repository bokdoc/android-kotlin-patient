package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView

import com.bokdoc.pateintsbook.data.webservice.models.*
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ResourceAdapterFactory

object OverViewParser {

    fun parse(json: String): Profile {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(PaymentOptions::class.java)
                .add(LanguageSpoken::class.java)
                .add(Profile::class.java)
                .add(ProfileType::class.java)
                .build()

        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()
        val userResponse = moshi.adapter(Document::class.java).fromJson(json)
        if(userResponse!=null)
        return userResponse.asObjectDocument<Profile>().get()

        return Profile()
    }
}