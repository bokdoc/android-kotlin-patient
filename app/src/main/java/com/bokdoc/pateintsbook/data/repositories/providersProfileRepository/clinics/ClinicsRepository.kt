package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.clinics

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ClinicsConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics.ClinicsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import com.bokdoc.pateintsbook.data.webservice.models.Speciality


class ClinicsRepository(private val appContext: Context,
                        private val sharedPreferencesManager: SharedPreferencesManager,
                        private val clinicsWebservice: ClinicsWebservice) {


    fun getClinics(id: String, service: HashMap<String, String>): LiveData<DataResource<ClinicRow>> {
        val liveData = MutableLiveData<DataResource<ClinicRow>>()
        val dataResource = DataResource<ClinicRow>()
        clinicsWebservice.getAllClinics(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id, service).enqueue(CustomResponse<Clinic>({
            dataResource.status = it.status
            if (it.data != null && it.data!!.isNotEmpty()) {
                dataResource.data = ClinicsConverter.convert(it.data!!)
                dataResource.meta = it.meta
            } else {
                dataResource.errors = it.errors
            }
            liveData.value = dataResource
        }, arrayOf(HealthProviderProfile::class.java, ProfileAttachmentMediaResource::class.java, NextAvailabilityResource::class.java,
                Clinic::class.java, WorkingTimes::class.java, Appointments::class.java, ProfileType::class.java, ProfileTitle::class.java, Speciality::class.java)))
        return liveData
    }
}