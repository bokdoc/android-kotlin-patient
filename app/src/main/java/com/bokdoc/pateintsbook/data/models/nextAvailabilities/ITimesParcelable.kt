package com.bokdoc.pateintsbook.data.models.nextAvailabilities

import android.os.Parcelable

interface ITimesParcelable : ITimes, Parcelable