package com.bokdoc.pateintsbook.data.webservice.autoComplete

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class AutoCompleteWebserviceImpl(val token:String) : AutoCompleteWebservice {

    val webserviceManagerImpl = WebserviceManagerImpl.instance()


    override fun getSpecialityAutoResults(keyword:String,screenType:String):Call<ResponseBody>{
        val autoCompleteApi = webserviceManagerImpl.createService(AutoCompleteApi::class.java,token)
        return autoCompleteApi.completeSpeciality(keyword,screenType)
    }

    override fun getLocationsAutoResults(keyword: String):Call<ResponseBody>{
        val autoCompleteApi = webserviceManagerImpl.createService(AutoCompleteApi::class.java,token)
        return autoCompleteApi.completeLocation(keyword)
    }

    override fun getInsuranceAutoResults(keyword: String):Call<ResponseBody>{
        val autoCompleteApi = webserviceManagerImpl.createService(AutoCompleteApi::class.java,token)
        return autoCompleteApi.completeInsuranceCarrier(keyword)
    }


}