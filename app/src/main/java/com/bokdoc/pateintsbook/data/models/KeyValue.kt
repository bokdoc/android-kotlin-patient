package com.bokdoc.pateintsbook.data.models

class KeyValue {
    var key:String = ""
    var value:String = ""
    var isSelected = false

    companion object {
        fun toValues(keysValues:List<KeyValue>):List<String>{
            val values = arrayListOf<String>()
            keysValues.forEach {
                values.add(it.value)
            }
            return values
        }
    }
}