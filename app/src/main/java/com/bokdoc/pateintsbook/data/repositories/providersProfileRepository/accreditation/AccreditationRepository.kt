package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accreditation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.AccreditationConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ClinicsConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl.Companion.SERVER_CODE_200
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accreditation.AccreditationWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics.ClinicsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProvidersProfileType
import com.bokdoc.pateintsbook.data.webservice.models.Speciality
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccreditationRepository(private val appContext: Context,
                              private val sharedPreferencesManager: SharedPreferencesManager,
                              private val accreditationWebService: AccreditationWebService) {

    fun getAccreditation(id: String, service: String): LiveData<DataResource<AccreditationRow>> {
        val liveData = MutableLiveData<DataResource<AccreditationRow>>()
        val dataResource = DataResource<AccreditationRow>()

        accreditationWebService.getAccreditation(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (it.data!!.isNotEmpty()) {
                        dataResource.data = AccreditationConverter.convert(it.data!![0])
                        //dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, ProfileType::class.java, Accreditation::class.java)))

        return liveData
    }


}
