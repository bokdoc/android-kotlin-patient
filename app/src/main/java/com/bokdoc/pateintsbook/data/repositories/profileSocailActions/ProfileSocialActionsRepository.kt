package com.bokdoc.pateintsbook.data.repositories.profileSocailActions

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileActionsParser
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class ProfileSocialActionsRepository(private val appContext: Context,
                                     private val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                                     private val compareSharedPreferences: CompareSharedPreferences,
                                     private val profileSocialWebservice: ProfileSocailActionsWebservice) {


    fun toggleLike(id: String): LiveData<DataResource<String>> {
        val liveData = MutableLiveData<DataResource<String>>()
        profileSocialWebservice.toggleLike(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id).enqueue(SocialActionsResponse(liveData))
        return liveData
    }

    fun toggleFavorite(id: String): LiveData<DataResource<String>> {
        val liveData = MutableLiveData<DataResource<String>>()
        profileSocialWebservice.toggleFavorite(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id).enqueue(SocialActionsResponse(liveData))
        return liveData
    }

    fun getShareableProfileUrl(profile: HealthProvidersProfile): String {

        return Uri.parse(ProfileSocailActionsWebservice.SHARABLE_URL)
                .buildUpon()
                .appendQueryParameter(ProfileSocailActionsWebservice.IS_LIKED, profile.isLiked.toString())
                .appendQueryParameter(ProfileSocailActionsWebservice.IS_FAVOURITE, profile.isFavourite.toString())
                .appendQueryParameter(ProfileSocailActionsWebservice.IS_COMPARED, profile.isCompare.toString())
                .appendQueryParameter(ProfileSocailActionsWebservice.LOCATION_KEY, profile.location)
                .appendQueryParameter(ProfileSocailActionsWebservice.NAME_KEY, profile.name)
                .appendQueryParameter(ProfileSocailActionsWebservice.ID_KEY, profile.id)
                .appendQueryParameter(ProfileSocailActionsWebservice.PICTURE_KEY, profile.picture)
                .appendQueryParameter(ProfileSocailActionsWebservice.TYPE_KEY, profile.type.toString())
                .build().toString()

    }

    fun getProfileActionsMenu(context: Context, profile: HealthProvidersProfile): List<ImageTextRow> {
        val imagesTextsList = ArrayList<ImageTextRow>()
        profile.isLiked.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_like_selected, context.getString(R.string.like)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_like, context.getString(R.string.like)))
            }
        }
        profile.isFavourite.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_favorite_selected, context.getString(R.string.favorite)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_favorite, context.getString(R.string.favorite)))
            }
        }

        /*
        profile.isCompare.let {
            if (it) {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_compare_selected, context.getString(R.string.compare)))
            } else {
                imagesTextsList.add(ImageTextRow(R.drawable.ic_compare, context.getString(R.string.compare)))
            }
        }
        */
        imagesTextsList.add(ImageTextRow(R.drawable.ic_share, context.getString(R.string.share)))
        return imagesTextsList
    }


    private inner class SocialActionsResponse(private var liveData: MutableLiveData<DataResource<String>>) : Callback<ResponseBody> {
        var dataResource = DataResource<String>()

        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            dataResource.message = t?.message!!
            dataResource.status = DataResource.FAIL
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if (response?.body() != null) {
                val message = ProfileActionsParser.getMessage(response.body()?.string()!!)
                if (message == ProfileSocailActionsWebservice.SUCCESS_MESSAGE) {
                    dataResource.status = DataResource.SUCCESS
                } else {
                    dataResource.status = DataResource.FAIL
                }
            } else {
                dataResource.status = DataResource.FAIL
            }
            liveData.value = dataResource
        }

    }

    fun toggleCompare(id: String, isAdded: Boolean = true) {
        if (isAdded) {
            compareSharedPreferences.addCompare(id)
        } else {
            compareSharedPreferences.removeCompare(id)
        }
    }

    fun saveCompares() {
        compareSharedPreferences.saveCompares()
    }

    fun convertToProfile(profile: Profile): HealthProvidersProfile {
        return HealthProvidersProfileConverter.convert(profile)
    }


}