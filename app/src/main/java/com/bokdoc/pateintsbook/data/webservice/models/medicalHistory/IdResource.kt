package com.bokdoc.pateintsbook.data.webservice.models.medicalHistory

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "id")
class IdResource:Resource(){
    @Json(name = "id")
    var idAtt:String = ""
}