package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import android.util.Log
import com.bokdoc.pateintsbook.data.converters.NextAvailabilitiesParcelableConverter
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.Surgeries
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Profile.CREATOR.DOCTOR

object SurgeriesConverter {

    fun convert(surgeriessResource: List<Surgeries>): List<SurgeriesRow> {
        val surgeries = ArrayList<SurgeriesRow>()
        surgeriessResource.forEach {
            val surgeriesRow = SurgeriesRow()
            surgeriesRow.id = it.id
            surgeriesRow.location = it.location
            surgeriesRow.surgeryName = it.name
            surgeriesRow.showUrl = it.showUrl
            surgeriesRow.nationalFees = it.mainFees
            surgeriesRow.interNationalFees = it.subMainFees
            surgeriesRow.internationalFeesLable = it.subMainFees.label
            surgeriesRow.nationalFeesLable = it.mainFees.label
            it.mainFees.fixed_fees?.currency_symbols?.let {
                surgeriesRow.feesCurrancy = it
            }
            surgeriesRow.appointmentReservation = it.is_appointment_reservation
            surgeriesRow.bookButton = it.bookButton

            it.nextAvailabilities?.let { nv ->
                surgeriesRow.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(nv.get(it.document))
            }
            it.parent?.let { type ->
                surgeriesRow.profileType = type.get(it.document).profileType.get(it.document).id.toInt()
            }
            surgeriesRow.type = 0
            if (it.doctor != null) {
                surgeriesRow.doctorName = it.doctor!!.get(it.document).name
                surgeriesRow.doctorPhoto = it.doctor!!.get(it.document).picture.url
                surgeriesRow.speciality = it.doctor!!.get(it.document).specialityTitle
                surgeriesRow.gender = it.doctor!!.get(it.document).gender
                if (it.doctor!!.get(it.document).profileTitle != null)
                    surgeriesRow.doctorTitle = it.doctor!!.get(it.document).profileTitle!!.get(it.document).name
            } else {
                if (surgeriesRow.profileType == DOCTOR) {
                    surgeriesRow.doctorName = it.name
                    surgeriesRow.doctorPhoto = it.picture.url
                    surgeriesRow.speciality = it.speciality!!.get(it.document).name
                }
            }
            Log.i("profileType", surgeriesRow.profileType.toString())
            surgeries.add(surgeriesRow)
        }
        return surgeries
    }

    fun convert(linksCardsDetails: LinksCardsDetails): List<SurgeriesRow> {
        val surgeries = ArrayList<SurgeriesRow>()
        linksCardsDetails.rowsInfo.forEach {
            val surgeriesRow = SurgeriesRow()
            surgeriesRow.id = it.id
            surgeriesRow.location = it.location
            surgeriesRow.surgeryName = it.name
            surgeriesRow.nationalFees = it.mainFees
            surgeriesRow.interNationalFees = it.subMainFees
            surgeriesRow.showUrl = it.bookButton.endpointUrl
            surgeriesRow.bookButton = it.bookButton
            surgeriesRow.internationalFeesLable = it.subMainFees.label
            surgeriesRow.nationalFeesLable = it.mainFees.label

            it.mainFees.fixed_fees?.currency_symbols?.let {
                surgeriesRow.feesCurrancy = it
            }

            surgeriesRow.nextAvailabilities = it.nextAvailabilities
            surgeriesRow.profileType = linksCardsDetails.mainProfileInfo.profileType
            surgeriesRow.type = 0
            if (it.profileInfo.profileId.isNotEmpty()) {
                surgeriesRow.doctorName = it.profileInfo.profileName
                surgeriesRow.doctorPhoto = it.profileInfo.profilePicture.url
                surgeriesRow.speciality = it.profileInfo.profileMainSpeciality
                surgeriesRow.gender = it.profileInfo.gender

            } else {
                if (surgeriesRow.profileType == Profile.DOCTOR) {
                    surgeriesRow.doctorName = linksCardsDetails.mainProfileInfo.profileName
                    surgeriesRow.doctorPhoto = linksCardsDetails.mainProfileInfo.profilePicture.url
                    surgeriesRow.speciality = linksCardsDetails.mainProfileInfo.profileMainSpeciality
                }
            }
            surgeries.add(surgeriesRow)
        }
        return surgeries
    }
}

