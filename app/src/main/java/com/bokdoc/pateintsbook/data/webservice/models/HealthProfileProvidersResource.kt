package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.healthProviders.awards.Awards
import com.bokdoc.pateintsbook.data.models.healthProviders.department.Department
import com.bokdoc.pateintsbook.data.models.healthProviders.doctor.Doctor
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.FeedbackPersona
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class HealthProfileProvidersResource : Resource() {

    @Json(name = "beds_number")
    var bedsNumber = 0

    @Json(name = "ambulance_count")
    var ambulanceCount = 0

    @Json(name = "overview")
    var description = ""

    @Json(name = "patient_room")
    var patientRoom = ""

    var location = Location()

    @Json(name = "accompanying_persons")
    var accompanyingPersons = 0

    @Json(name = "facebook_url")
    var facebookUrl = ""

    @Json(name = "twitter_url")
    var twitterUrl = ""

    @Json(name = "googleplus_url")
    var googlePlusUrl = ""

    @Json(name = "linkedin_url")
    var linkedinUrl = ""

    @Json(name = "instagram_url")
    var instagramUrl = ""

    @Json(name = "accommodation")
    var accommodations: HasMany<Accommodation> = HasMany()

    var media: HasMany<MediaResource> = HasMany()

    @Json(name = "review")
    var feedback: HasMany<FeedbackResource> = HasMany()

    var departments = HasMany<DepartmentResource>()

    var doctors = HasMany<DoctorResource>()

    var services = HasMany<ServiceResource>()

    var awards = HasMany<AwardsResource>()

    var educations = HasMany<EducationsRes>()

    var specialities = HasMany<SubSpecialities>()

    var certifications = HasMany<Certification>()

    var memberships = HasMany<Membership>()

    var publications = HasMany<Publication>()

    var accreditation = HasMany<AccreditationRes>()

    var specialWords = HasMany<SpecialWords>()

}


@JsonApi(type = "accommodation")
class Accommodation : Resource() {
    var title = ""
    var description = ""

}


@JsonApi(type = "profileMedia")
class MediaResource : Resource(), IMedia {
    override var mediaId: String = ""
    override var name: String = ""
    @Json(name = "media_type")
    override var mediaType: String = ""

    @Json(name = "poster")
    override var url: String = ""
}

@JsonApi(type = "review")
class FeedbackResource : Resource(), IFeedback {
    override var time: String=""

    override var name: String? = null

    override var picture: String = ""

    override var rate = 0
    override var content = ""
    override var date = ""

    @Json(name = "persona")
    var personaResource = HasOne<FeedbackPersonaResource>()

    override var person: FeedbackPersona?
        get() = personaResource.get(this.document)
        set(value) {}


}

@JsonApi(type = "persona")
class FeedbackPersonaResource : Resource(), FeedbackPersona {
    override var picture: String = ""
    override var name: String = ""
}

@JsonApi(type = "profileType")
class HealthProvidersProfileType : Resource()

@JsonApi(type = "department")
class DepartmentResource : Resource(), Department {
    override val name: String = ""
    override val idDepartment: String
        get() = id
}

class FeeHealthProviders : Fees {
    override var amount: Double = 0.0
    @Json(name = "currency_symbols")
    override var symbols: String = ""
}

@JsonApi(type = "profile")
class DoctorResource : Resource(), Doctor {
    override var rating: Int = 0
    override var picture: String = ""
    override var name: String = ""
    override var location: String = ""
    @Json(name = "fees")
    override var fee: Fees? = null
}


@JsonApi(type = "service")
class ServiceResource : Resource(), Service {

    override var feesService: Fees?
        get() {
            return fees
        }
        set(value) {
            feesService = value
        }

    var fees: FeeHealthProviders = FeeHealthProviders()

    override val name: String = ""
    override val summary: String = ""
    override val picture: String = ""
    @Json(name = "type")
    lateinit var types: List<String>
    override val serviceType: String
        get() {
            return types[0]
        }

}


@JsonApi(type = "award")
class AwardsResource() : Resource(), Awards {
    override val icon: String = ""
    override val image: String = ""
    override val title: String = ""
    override val content: String = ""
}

@JsonApi(type = "accreditation")
class AccreditationRes : QualificationsRes()

open class QualificationsRes : Resource(), Qualifications {
    override var description: String = ""
    override var icon: String = ""
    override var university_id: String = ""
    override var years: String = ""
    override var title: String = ""
    override var link: String = ""
    override var content: String = ""
}

@JsonApi(type = "education")
class EducationsRes : QualificationsRes()

@JsonApi(type = "speciality")
class SubSpecialities() : QualificationsRes() {
    @Json(name = "summary")
    override var content: String = ""
    @Json(name = "name")
    override var title: String = ""
}


@JsonApi(type = "certification")
class Certification : QualificationsRes()

@JsonApi(type = "membership")
class Membership : QualificationsRes()

@JsonApi(type = "publication")
class Publication : QualificationsRes()

@JsonApi(type = "specialWords")
class SpecialWords : QualificationsRes()



