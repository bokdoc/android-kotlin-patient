package com.bokdoc.pateintsbook.data.repositories.notifications

import android.arch.lifecycle.MutableLiveData
import com.bokdoc.pateintsbook.data.converters.NotificationsConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.ProfileTitle
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.ProfileType
import com.bokdoc.pateintsbook.data.webservice.models.notifications.NotificationsResource
import com.bokdoc.pateintsbook.data.webservice.notifications.NotificationsWebservice
import retrofit2.http.Query

class NotificationsRepository(private val userSharedPreference: UserSharedPreference,
                              private val notificationsWebservice: NotificationsWebservice) {

    fun getNotifications(queriesMap:Map<String,String>):MutableLiveData<DataResource<INotifications>>{
        val liveData = MutableLiveData<DataResource<INotifications>>()
        notificationsWebservice.get(userSharedPreference.getToken(),queriesMap).enqueue(CustomResponse<INotifications>({
            if(!it.data.isNullOrEmpty()){
                it.data = NotificationsConverter.convert(it.data as List<NotificationsResource>)
            }
            liveData.value = it
        }, arrayOf(NotificationsResource::class.java, Profile::class.java,ProfileType::class.java,ProfileTitle::class.java)))
        return liveData
    }
}