package com.bokdoc.pateintsbook.data.bookAppointement

import com.bokdoc.pateintsbook.data.models.media.MediaParcelable

interface BookInfo : HealthProvidersBookUIInfo {
    var bookType: String
    var patientName: String
    var patientMobile: String
    var patientEmail: String
    var patientGender: String
    var patientAge: String
    var patientImage: String
        set(value) {}
        get() = ""

    var nextAvailabilityId: String

    var reservationDate: String
    var reservationTime: String?

    var surgeriesCaseDescription: String?
    //var surgeriesAttachmentUrls:List<String>
    var surgeriesMedia: List<MediaParcelable>?

    var code: String?
    var serviceType: String

    var servicePivotId: Int

    var servicePivotType: String

    var deviceType: String

    val bookRequestType: String?

    var discountId: String?

    var coupon: String?

    var bookSecondaryDuration:Boolean?


}