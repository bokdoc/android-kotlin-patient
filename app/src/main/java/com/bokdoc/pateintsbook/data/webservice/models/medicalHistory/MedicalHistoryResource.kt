package com.bokdoc.pateintsbook.data.webservice.models.medicalHistory

import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

open class MedicalHistoryResource() :Resource(), MedicalHistory {
    override var historyId: String = ""
        get() = id
    override var name: String = ""
    override var summary: String = ""
    override var description: String = ""
}

@JsonApi(type = "speciality")
class Speciality:MedicalHistoryResource()

@JsonApi(type = "medication")
class Medication:MedicalHistoryResource()

@JsonApi(type = "disease")
class Disease:MedicalHistoryResource()