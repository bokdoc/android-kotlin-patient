package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

class AccommodationsRow() {
    var patientRoom: String = ""
    var accompaniedPersons: String = ""
    var meals: String = ""
    var rooms: String = ""
}