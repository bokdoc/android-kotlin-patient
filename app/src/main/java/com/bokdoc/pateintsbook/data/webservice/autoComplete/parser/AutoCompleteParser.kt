package com.bokdoc.pateintsbook.data.webservice.autoComplete.parser

import com.bokdoc.pateintsbook.data.webservice.models.InsuranceCarrier
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.*
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.ResourceAdapterFactory
import com.squareup.moshi.JsonAdapter
import moe.banana.jsonapi2.ArrayDocument
import moe.banana.jsonapi2.Document


object AutoCompleteParser {

    fun parse(json:String):List<AutoCompleteResource> {
        if(json!="") {
            val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                    .add(AutoCompleteResource::class.java)
                    .add(Link::class.java)
                    .add(Service::class.java)
                    .add(Doctor::class.java)
                    .add(Speciality::class.java)
                    .add(Rad::class.java)
                    .add(Location::class.java)
                    .add(Test::class.java)
                    .add(Scan::class.java)
                    .add(Lab::class.java)
                    .add(InsuranceCarrierPlans::class.java)
                    .add(Hospital::class.java)
                    .add(Surgery::class.java)
                    .build()
            val moshi = Moshi.Builder()
                    .add(jsonApiAdapterFactory)
                    .build()
            val autoComplets = moshi.adapter(Document::class.java).fromJson(json)!!.asArrayDocument<AutoCompleteResource>()
            return autoComplets
        }else{
            return ArrayList()
        }
    }
}