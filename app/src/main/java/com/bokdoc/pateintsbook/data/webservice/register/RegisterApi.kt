package com.bokdoc.pateintsbook.data.webservice.register

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface RegisterApi {
    @Headers("Content-Type:application/json")
    @POST(WebserviceConstants.AUTH + "/register")
    fun register(@Body requestBodyJson: RequestBody, @Header("Authorization") token: String): Call<ResponseBody>

}