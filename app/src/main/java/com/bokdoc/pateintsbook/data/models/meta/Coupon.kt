package com.bokdoc.pateintsbook.data.models.meta

import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Coupon : Serializable {

    @SerializedName("main_fees")
    @Expose
    val mainFees: MainFees? = null

    @SerializedName("sub_main_fees")
    @Expose
    val subMainFees: MainFees? = null

    @SerializedName("discount_id")
    @Expose
    val discountId: String? = null

}


class CouponFees {
    var amount: String? = null
    var currency_symbols: String? = null
    @SerializedName("before_discount")
    @Expose
    var oldAmount: String? = ""
}

