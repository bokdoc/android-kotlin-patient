package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.InsuranceCarriers

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class InsuranceCarriersWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getInsuranceCarriers(token: String, id: String, service: String): Call<ResponseBody> {
        val surgeriesWebService = webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token)
        return surgeriesWebService.getProfileSection(id, service)
    }

}