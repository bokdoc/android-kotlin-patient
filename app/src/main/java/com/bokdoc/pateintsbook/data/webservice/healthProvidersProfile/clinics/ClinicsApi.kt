package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ClinicsApi {

    @GET(WebserviceConstants.PROFILES + "/{id}")
    fun getClinics(@Path("id") id: String, @Query("include") service: String): Call<ResponseBody>


    @GET(WebserviceConstants.PROFILES_WITH_SLASH + "{id}" + WebserviceConstants.CLINICS)
    fun getAllClinics(@Path("id") id: String, @QueryMap query: HashMap<String, String> = HashMap()): Call<ResponseBody>

}