package com.bokdoc.pateintsbook.data.webservice.login.encoder

import com.bokdoc.pateintsbook.data.webservice.models.LoginCredentials
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object LoginCredentialsEncoder {

    fun getJson(email: String, password: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<LoginCredentials>()
        val loginCredentials = LoginCredentials()
        loginCredentials.email = email
        loginCredentials.password = password
        document.set(loginCredentials)

        return moshi.adapter(Document::class.java).toJson(document)
    }

    fun getJson(email: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<LoginCredentials>()
        val loginCredentials = LoginCredentials()
        loginCredentials.email = email
        document.set(loginCredentials)
        return moshi.adapter(Document::class.java).toJson(document)
    }

    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(LoginCredentials::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }


}