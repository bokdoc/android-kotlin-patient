package com.bokdoc.pateintsbook.data.models

class TutorialsItemInfo {
    var imageRes:Int = -1
    var title:String = ""
    var description:String=""
    var buttonText:String=""
    var backgroundColor = -1
}