package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.qualifications

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.repositories.common.StringsRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.qualifications.QualificationsWebService
import moe.banana.jsonapi2.Resource
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class QualificationsRepository(private val context: Context,
                               private val sharedPreferencesManager: SharedPreferencesManager,
                               private val qualificationsWebService: QualificationsWebService,
                               private val stringsRepository: StringsRepository) {

    private lateinit var query: String
    private lateinit var parserClass: Class<Resource>

    fun getQualifications(id: String, selectedText: String): LiveData<DataResource<Qualifications>> {
        val liveData = MutableLiveData<DataResource<Qualifications>>()
        val dataResource = DataResource<Qualifications>()
        initialize(selectedText)
        qualificationsWebService.getQualificationsDetails(sharedPreferencesManager.getData(context.getString(R.string.token), "")
                , id, query).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val profiles = Parser.parseJson(response.body()?.string()!!, arrayOf(HealthProfileProvidersResource::class.java,
                            parserClass, HealthProvidersProfileType::class.java))[0] as HealthProfileProvidersResource
                    dataResource.data = getList(profiles)
                    liveData.value = dataResource
                } else {
                    dataResource.message = response?.errorBody()!!
                    liveData.value = dataResource
                }
            }

        })

        return liveData
    }

    private fun getList(profile: HealthProfileProvidersResource): List<Qualifications> {
        return when {
            profile.educations.size() != 0 -> profile.educations.get(profile.document)
            profile.accreditation.size() != 0 -> profile.accreditation.get(profile.document)
            profile.accreditation.size() != 0 -> profile.educations.get(profile.document)
            profile.specialities.size() != 0 -> profile.specialities.get(profile.document)
            profile.certifications.size() != 0 -> profile.certifications.get(profile.document)
            profile.memberships.size() != 0 -> profile.memberships.get(profile.document)
            profile.publications.size() != 0 -> profile.publications.get(profile.document)
             else -> profile.specialWords.get(profile.document)
        }
    }

    private fun initialize(selectedText: String) {
        when (selectedText) {
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.EDUCATION) -> {
                query = QualificationsWebService.EDUCATIONS_QUERY
                parserClass = EducationsRes::class.java as Class<Resource>
            }
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.SUB_SPECIALITY) -> {
                query = QualificationsWebService.SPECIALITIES_QUERY
                parserClass = SubSpecialities::class.java as Class<Resource>
            }
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.CERTIFICATES) -> {
                query = QualificationsWebService.CERTIFICATIONS_QUERY
                parserClass = Certification::class.java as Class<Resource>
            }
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.MEMBERSHIPS) -> {
                query = QualificationsWebService.MEMBERSHIPS_QUERY
                parserClass = Membership::class.java as Class<Resource>
            }
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.PUBLICATIONS) -> {
                query = QualificationsWebService.PUBLICATIONS_QUERY
                parserClass = Publication::class.java as Class<Resource>
            }
            stringsRepository.getStringRes(context, StringsRepository.QualificationsRes.Accreditation) -> {
                query = QualificationsWebService.ACCREDITION_QUERY
                parserClass = AccreditationRes::class.java as Class<Resource>
            }
        }
    }


}