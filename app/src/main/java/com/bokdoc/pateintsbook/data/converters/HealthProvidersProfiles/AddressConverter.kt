package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.ProfileAddressResource

object AddressConverter {

    fun convert(addressList: List<ProfileAddressResource>): List<Location> {
        val locations = ArrayList<Location>()
        addressList.forEach {
            val location = Location()
            location.latitude = it.latitude
            location.longitude = it.longitude
            locations.add(location)
        }
        return locations
    }
}

