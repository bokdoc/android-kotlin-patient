package com.bokdoc.pateintsbook.data.repositories.updateProfile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.VALIDATION_ERROR
import com.bokdoc.pateintsbook.data.models.User
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.editProfile.EditProfileEncoder
import com.bokdoc.pateintsbook.data.webservice.editProfile.EditProfileWebService
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.*

class UpdateProfileRepository(val context: Context, val userSharedPreference: UserSharedPreference, val editProfileWebService: EditProfileWebService) {

    val sharedPrefrenceManger = SharedPreferencesManagerImpl(context)


    fun update(userProfile: User): LiveData<DataResource<User>> {
        val updateJson: String = EditProfileEncoder.getJson(userProfile, userSharedPreference.getId())
        Log.i("profileJson", updateJson)
        val liveData = MutableLiveData<DataResource<User>>()
        val dataResource = DataResource<User>()
        editProfileWebService.update(userSharedPreference.getId(), userSharedPreference.getToken(), updateJson)
                .enqueue(CustomResponse<UserResponse>({
                    dataResource.status = it.status
                    when (it.status) {
                        SUCCESS -> {
                            if(it.data!=null && it.data!!.isNotEmpty()) {
                                it.data!![0].token = userSharedPreference.getToken()
                                val userJson = UserEncoder.getJson(it.data!![0])
                                sharedPrefrenceManger.saveData(context.getString(R.string.token), it.data!![0].token)
                                sharedPrefrenceManger.saveData(context.getString(R.string.user), userJson)
                            }
                        }
                        VALIDATION_ERROR -> {
                            dataResource.errors = it.errors
                        }
                    }
                    liveData.value = dataResource
                }, arrayOf(UserResponse::class.java,Country::class.java,Language::class.java,Currency::class.java)))
        return liveData
    }

    fun getUser():User{
        val user = User()
        userSharedPreference.getUser()?.let {
            user.picture = it.picture
            user.birthDate = it.birthdate
            user.email = it.email
            if(it.gender == User.MALE){
               user.gender = context.getString(R.string.male)
            }else if(it.gender == User.FEMALE){
                user.gender = context.getString(R.string.female)
            }
            user.name = it.name
            user.id = it.id
            user.phone = it.phone
        }
        return user
    }
}