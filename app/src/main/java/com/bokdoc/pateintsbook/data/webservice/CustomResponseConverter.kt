package com.bokdoc.pateintsbook.data.webservice

import com.bokdoc.pateintsbook.data.models.DataResource
import moe.banana.jsonapi2.Resource


/*
* This Layer for convert any resources from endpoint
*
*/
class CustomResponseConverter<T,V>(onResponse: (DataResource<V>) -> Unit,
                                   resources: Array<Class<out Resource>>,
                                   converter:(List<T>)->List<V>):CustomResponse<T>({
    val dataResource = DataResource<V>()
    dataResource.message = it.message
    dataResource.code = it.code
    dataResource.meta = it.meta
    dataResource.status = it.status
    if(dataResource.status == DataResource.SUCCESS){
        if(!it.data.isNullOrEmpty()) {
            dataResource.data = converter.invoke(it.data!!)
        }else{
            dataResource.data = arrayListOf()
        }
    }
    onResponse.invoke(dataResource)
},resources)