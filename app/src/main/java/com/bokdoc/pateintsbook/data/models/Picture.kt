package com.bokdoc.pateintsbook.data.models

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.squareup.moshi.Json

class Picture() :IMediaParcelable {
    override var mediaId: String = ""

    @Json(name = "media_url")
    override var url: String = ""

    override var name: String = ""

    @Json(name = "media_type")
    override var mediaType: String = ""

    constructor(parcel: Parcel) : this() {
        mediaId = parcel.readString()
        url = parcel.readString()
        name = parcel.readString()
        mediaType = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mediaId)
        parcel.writeString(url)
        parcel.writeString(name)
        parcel.writeString(mediaType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Picture> {
        override fun createFromParcel(parcel: Parcel): Picture {
            return Picture(parcel)
        }

        override fun newArray(size: Int): Array<Picture?> {
            return arrayOfNulls(size)
        }
    }

}