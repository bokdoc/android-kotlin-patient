package com.bokdoc.pateintsbook.data.webservice.attachmentMedia

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.multiPartsCreator.MultiPartCreator
import com.bokdoc.pateintsbook.data.webservice.progressableRequest.IUploadProgressable
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.File

class AttachmentMediaWebservice(private val webserviceManagerImpl: WebserviceManagerImpl,
                                private val multiPartCreator: MultiPartCreator) {


    fun uploadFiles(token: String, files: Array<File>, types: Array<String>, uploadListeners: (progress: Double) -> Unit): Call<ResponseBody> {
        val multiPartFiles = multiPartCreator.getFilesParts(files, types, object : IUploadProgressable {
            override var totalLength: Long = 0L
            override fun onProgress(byteCounts: Long) {
                uploadListeners.invoke((100.0 * byteCounts / totalLength))
            }
        })
        return webserviceManagerImpl.createService(AttachmentMediaApis::class.java, token).uploadFiles(multiPartFiles)
    }

    fun getMedia(id: String, token: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(AttachmentMediaApis::class.java, token).getMedia(id)
    }


    fun getMediaWithUrl(url: String, token: String): Call<ResponseBody> {
        val allListApis = webserviceManagerImpl.createService(AttachmentMediaApis::class.java, token)
        return allListApis.getMediaWithUrl(url)
    }

}