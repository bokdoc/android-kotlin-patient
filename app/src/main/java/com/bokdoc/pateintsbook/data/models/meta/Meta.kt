package com.bokdoc.pateintsbook.data.models.meta

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Meta : Serializable {
    @SerializedName("service_types")
    @Expose
    var serviceTypes: List<ServiceType> = ArrayList()

    @SerializedName("pagination")
    @Expose
    var pagination: Pagination = Pagination()

    @SerializedName("filter")
    @Expose
    var filter: List<FilterType>? = null

    @SerializedName("coupon")
    @Expose
    var coupon: Coupon? = null

    @SerializedName("message")
    @Expose
    var message = ""
    var links = ""
}