package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistory
import com.bokdoc.pateintsbook.data.models.medicalHistory.MedicalHistoryImpl

object MedicalHistoryConverter {
    fun convert(medicalsHistories: List<MedicalHistory>):List<MedicalHistoryImpl>{
        val medicalsHistoriesImpl = ArrayList<MedicalHistoryImpl>()

        medicalsHistories.forEach {
            medicalsHistoriesImpl.add(MedicalHistoryImpl().apply {
                historyId = it.historyId
                name = it.name
                description = it.description
                summary = it.summary
            })
        }

        return medicalsHistoriesImpl
    }
}