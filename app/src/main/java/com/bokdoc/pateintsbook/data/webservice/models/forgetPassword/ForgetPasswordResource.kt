package com.bokdoc.pateintsbook.data.webservice.models.forgetPassword

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class ForgetPasswordResource : Resource() {
    @Json(name = "recover_code")
    var recoverCode: String? = null
    @Json(name = "email_mobile")
    var emailMobile: String? = null
    var password: String? = null
}