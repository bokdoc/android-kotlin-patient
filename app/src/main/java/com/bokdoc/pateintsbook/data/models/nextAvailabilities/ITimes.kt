package com.bokdoc.pateintsbook.data.models.nextAvailabilities

interface ITimes {
    var from: String
    val to: String
}