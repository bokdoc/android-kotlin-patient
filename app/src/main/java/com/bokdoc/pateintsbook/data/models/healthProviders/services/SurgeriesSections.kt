package com.bokdoc.pateintsbook.data.models.healthProviders.services

import com.intrusoft.sectionedrecyclerview.Section

class SurgeriesSections(override val title: String, override val children: List<String>, override val sectionType: String)
    :ISurgeriesSections,Section<String> {

    override fun getChildItems(): List<String> {
        return children
    }
}