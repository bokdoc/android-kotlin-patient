package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileFeedback

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class ProfileFeedbackWebservice(val webserviceManagerImpl: WebserviceManagerImpl){

    fun getFeedback(token:String,id:String):Call<ResponseBody>{
        return webserviceManagerImpl.createService(ProfileFeedbackApi::class.java,token).getFeedback(id)
    }
}