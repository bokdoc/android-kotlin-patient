package com.bokdoc.pateintsbook.data.webservice.bookAppointements.encoder

import com.bokdoc.pateintsbook.data.bookAppointement.BookInfo
import com.bokdoc.pateintsbook.data.common.MediaResourcesEncoder
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.webservice.models.BookResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object BookEncoder {

    fun getJson(bookInfo: BookInfo, couponText: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<BookResource>()
        val bookResource = BookResource().apply {
            id = ""
            if (!bookInfo.discountId.isNullOrEmpty())
                discountId = bookInfo.discountId

            if (!bookInfo.coupon.isNullOrEmpty())
                coupon = couponText

            this.bookType = bookInfo.bookType
            serviceType = bookInfo.serviceType
            servicePivotId = bookInfo.servicePivotId
            servicePivotType = bookInfo.servicePivotType
            this.patientName = bookInfo.patientName
            this.patientMobile = bookInfo.patientMobile
            if (patientEmail.isNotEmpty())
                this.patientEmail = bookInfo.patientEmail

            this.patientGender = bookInfo.patientGender
            this.patientAge = bookInfo.patientAge

            if (!bookInfo.surgeriesCaseDescription.isNullOrEmpty())
                this.surgeriesCaseDescription = bookInfo.surgeriesCaseDescription

            // this.profileId =bookInfo.profileId
            //this.code = bookInfo.code
            //this.paymentTypeId = bookInfo.paymentTypeId
            //this.paymentTypeMethod = bookInfo.paymentTypeMethod
            //this.serviceId = bookInfo.serviceId
            //this.clinicId = bookInfo.clinicId
            //this.departmentId = bookInfo.departmentId
            //this.doctorId = bookInfo.doctorId
            reservationDate = bookInfo.reservationDate
            if (!bookInfo.reservationTime.isNullOrEmpty())
                reservationTime = bookInfo.reservationTime

            if (bookInfo.location != null && bookInfo.location?.latitude!!.isNotEmpty() && bookInfo.location?.latitude != "0")
                this.location = bookInfo.location

            if (bookInfo.nextAvailabilityId.isNotEmpty())
                nextAvailabilityIdNullable = bookInfo.nextAvailabilityId

            if (bookInfo.surgeriesMedia?.isNotEmpty()!!) {
                media = MediaResourcesEncoder.encode(document, bookInfo.surgeriesMedia as List<IMedia>)
            }

            if (!bookInfo.bookRequestType.isNullOrEmpty())
                bookRequestType = bookInfo.bookRequestType

            if (bookInfo.bookSecondaryDuration != null)
                bookSecondaryDuration = bookInfo.bookSecondaryDuration
        }
        document.set(bookResource)

        return moshi.adapter(Document::class.java).toJson(document)
    }

    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(BookResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}