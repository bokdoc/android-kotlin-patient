package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import okhttp3.ResponseBody
import retrofit2.Call

class ClinicsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {
    fun getAllClinics(token: String, id: String, service: HashMap<String, String>): Call<ResponseBody> {
        val clinicsWenService = webserviceManagerImpl.createService(ClinicsApi::class.java, token)
        return clinicsWenService.getAllClinics(id, service)
    }


}