package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.surgeries

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class ProfileSurgeriesWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getAllSurgeries(token: String, id: String, service: String): Call<ResponseBody> {
        val surgeriesWebService = webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token)
        return surgeriesWebService.getProfileSection(id, service)
    }

    fun getSurgeries(token: String, id: String, service: HashMap<String, String>): Call<ResponseBody> {
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token).getSurgeries(id, service)
    }

    fun show(url: String, token: String): Call<ResponseBody> {
        val allListApis = webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token)
        return allListApis.show(url)
    }
}