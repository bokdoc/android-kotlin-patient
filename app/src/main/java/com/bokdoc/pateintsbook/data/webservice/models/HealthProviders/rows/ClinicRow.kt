package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Location

class ClinicRow {
    var id: String = ""
    var name: String = ""
    var doctorName: String? = ""
    var doctorTitle: String? = ""
    var gender: String? = ""
    var doctorPhoto: String = ""
    var speciality: String = ""
    //fees
    var nationalFees: MainFees? = null
    var interNationalFees: MainFees? = null
    var nationalFeesLabel: String = ""
    var internationalFeesLabel: String = ""
    var bookUrl: String = ""
    var appointmentReservation: String = ""
    var feesCurrancy: String = ""
    var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()
    var location: Location = Location()
    var profileType = 1
    var bookButton: DynamicLinks? = null
    var media: List<IMediaParcelable>? = null
    var type: Int = 0
}