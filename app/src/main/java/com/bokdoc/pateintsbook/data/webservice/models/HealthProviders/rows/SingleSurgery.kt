package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Location

class SingleSurgery {
    var name: String = ""
    var nationalFees: MainFees? = null
    var interNationalFees: MainFees? = null
    var location: Location? = null
    var speciality: String = ""
    var doctorImage: String = ""
    var doctorName: String = ""
    var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()
    var duration: String = ""
    var paymentType: String = ""
    var approvalType: String = ""
    var watingQueu: String = ""
    var programDuration: String = ""
    var devices: String = ""
    var bookButton: DynamicLinks? = null


    var anesthesiaTypes: ArrayList<String> = ArrayList()
    var preOrderRadiologyScans: ArrayList<String> = ArrayList()
    var laboratoryTestIncluded: ArrayList<String> = ArrayList()
    var preOrderLabTests: ArrayList<String> = ArrayList()
    var media: ArrayList<MediaParcelable> = ArrayList()

}