package com.bokdoc.pateintsbook.data.repositories.nextavailability

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ClinicsConverter
import com.bokdoc.pateintsbook.data.converters.NextAvailabilitiesParcelableConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl.Companion.SERVER_CODE_200
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics.ClinicsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.AttachmentMediaResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import com.bokdoc.pateintsbook.data.webservice.models.Speciality
import com.bokdoc.pateintsbook.data.webservice.nextAvailabilityTimes.NextavailabilityWebService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NextAvailabilityRepository(private val appContext: Context,
                                 private val sharedPreferencesManager: SharedPreferencesManager,
                                 private val nextavailabilityWebService: NextavailabilityWebService) {


    fun getNextAvailability(url: String): LiveData<DataResource<NextAvailabilitiesParcelable>> {
        val liveData = MutableLiveData<DataResource<NextAvailabilitiesParcelable>>()

        nextavailabilityWebService.get(url, sharedPreferencesManager.getData(appContext.getString(R.string.token), ""))
                .enqueue(object : Callback<ResponseBody> {
                    val dataResource = DataResource<NextAvailabilitiesParcelable>()
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        dataResource.message = t?.message!!
                        liveData.value = dataResource
                        dataResource.status = DataResource.FAIL

                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.code() == SERVER_CODE_200) {
                            val json = response!!.body()!!.string()
                            val nextAvailability = Parser.parseJson(json, arrayOf(NextAvailabilityResource::class.java))
                            dataResource.data = NextAvailabilitiesParcelableConverter.convert(nextAvailability as List<NextAvailabilityResource>)
                            dataResource.status = DataResource.SUCCESS
                            liveData.value = dataResource
                        } else {
                            dataResource.status = DataResource.FAIL
                            dataResource.message = response?.errorBody()!!.string()
                        }
                        liveData.value = dataResource
                    }

                })


        return liveData
    }
}