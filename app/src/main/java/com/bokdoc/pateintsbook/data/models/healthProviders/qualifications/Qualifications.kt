package com.bokdoc.pateintsbook.data.models.healthProviders.qualifications

interface Qualifications {
    var title: String
    var link: String
    var content: String
    var university_id: String
    var years: String
    var icon: String
    var description: String
}