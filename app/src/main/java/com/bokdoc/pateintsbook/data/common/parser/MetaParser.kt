package com.bokdoc.pateintsbook.data.common.parser

import com.bokdoc.pateintsbook.data.models.meta.Meta
import com.bokdoc.pateintsbook.data.models.meta.PaginationMeta
import org.json.JSONObject
import com.google.gson.Gson
import com.google.gson.JsonObject


object MetaParser {

    fun paresMeta(json: String): Meta {
        if (json.isNotEmpty() && !json.contains("<script>")) {
            var data: JSONObject? = null
            if (JSONObject(json).has("meta")) {
                if (JSONObject(json).optJSONObject("meta") != null) {
                    data = JSONObject(json).getJSONObject("meta")
                } else if (JSONObject(json).optJSONArray("meta") != null) {
                    data = JSONObject(json).getJSONArray("meta").getJSONObject(0)
                }
            }
            if (data != null)
                return Gson().fromJson(data.toString(), Meta::class.java)
        }
        return Meta()
    }

    fun paresPaginationMeta(json: String): PaginationMeta {
        val data = JSONObject(json).getJSONObject("meta")
        val meta = Gson().fromJson(data.toString(), PaginationMeta::class.java)
        return meta
    }


}