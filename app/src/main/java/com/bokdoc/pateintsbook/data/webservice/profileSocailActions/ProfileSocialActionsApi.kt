package com.bokdoc.pateintsbook.data.webservice.profileSocailActions

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Path

interface ProfileSocialActionsApi {

    @POST(WebserviceConstants.PATIENTS_URL + "toggle-like/{id}")
    fun toggleLike(@Path(value = "id") id: String): Call<ResponseBody>

    @POST(WebserviceConstants.PATIENTS_URL + "toggle-favorite/{id}")
    fun toggleFavorite(@Path(value = "id") id: String): Call<ResponseBody>
}