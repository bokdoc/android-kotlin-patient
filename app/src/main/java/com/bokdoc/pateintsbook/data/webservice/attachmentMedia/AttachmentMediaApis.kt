package com.bokdoc.pateintsbook.data.webservice.attachmentMedia

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface AttachmentMediaApis {
    @Multipart
    @POST("media/file/create")
    fun uploadFiles(@Part files: List<MultipartBody.Part>): Call<ResponseBody>

    @GET(WebserviceConstants.PATIENTS_URL + "{id}/media")
    fun getMedia(@Path("id") id: String): Call<ResponseBody>

    @GET
    fun getMediaWithUrl(@Url url: String): Call<ResponseBody>

}