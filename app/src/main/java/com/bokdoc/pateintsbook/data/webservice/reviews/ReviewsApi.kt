package com.bokdoc.pateintsbook.data.webservice.reviews

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ReviewsApi {

    @GET(WebserviceConstants.PATIENTS + "/{id}/reviews")
    fun getReviews(@Path("id") id: String, @Query("include",encoded = true) include: String,
                   @Query("service_type",encoded = true) ServiceType: String, @Query("page") page: Int): Call<ResponseBody>


    @GET(WebserviceConstants.PROFILES + "/{id}")
    fun getProvidersReviews(@Path("id") id: String, @Query("include",encoded = true) include: String, @Query("page") page: Int): Call<ResponseBody>
}
