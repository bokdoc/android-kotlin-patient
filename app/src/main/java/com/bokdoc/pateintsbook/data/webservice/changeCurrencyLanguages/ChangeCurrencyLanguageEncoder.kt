package com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages


import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object ChangeCurrencyLanguageEncoder {

    fun encode(currency: LookupsType, language: LookupsType, country: LookupsType, userId: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<LanguageCurrencyResource>()
        val languageCurrencyResource = LanguageCurrencyResource()
        languageCurrencyResource.id = userId
        if (currency.idLookup.isNotEmpty())
            languageCurrencyResource.currency = HasOne(Currency().apply {
                id = currency.idLookup
                name = currency.name
            })

        if (language.idLookup.isNotEmpty())
            languageCurrencyResource.language = HasOne(Language().apply {
                id = language.idLookup
                name = language.name
            })

        if (country.idLookup.isNotEmpty())
            languageCurrencyResource.country = HasOne(Country().apply {
                id = country.idLookup
                name = country.name
            })

        document.set(languageCurrencyResource)

        val json = moshi.adapter(Document::class.java).toJson(document)
        return json
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(LanguageCurrencyResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}