package com.bokdoc.pateintsbook.data.webservice.patientRequests

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface RequestsApi {

    /*
    @GET(WebserviceConstants.PATIENTS + "/{id}/requests")
    fun getRequests(@Path("id") id: String
                              , @QueryMap include: String
                             , @Query("service_type") ServiceType: String,
                              @Query("page") page: String="7"): Call<ResponseBody>
                              */

    @GET(WebserviceConstants.PATIENTS + "/{id}/requests")
    fun getRequests(@Path("id") id: String,@QueryMap queries:Map<String,String>): Call<ResponseBody>

    @PUT
    fun cancelRequest(@Url url:String,@Body body:RequestBody):Call<ResponseBody>

}