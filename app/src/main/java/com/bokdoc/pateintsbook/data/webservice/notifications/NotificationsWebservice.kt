package com.bokdoc.pateintsbook.data.webservice.notifications

import com.bokdoc.pateintsbook.data.webservice.WebServiceManger
import com.bokdoc.pateintsbook.data.webservice.nextAvailabilityTimes.NextAvailabilityApi
import okhttp3.ResponseBody
import retrofit2.Call

class NotificationsWebservice(val webserviceManagerImpl: WebServiceManger) {

    fun get(token: String,queriesMap:Map<String,String>): Call<ResponseBody> {
        val nextAvailabilityApi = webserviceManagerImpl.createService(NotificationsApis::class.java, token)
        return nextAvailabilityApi.getNotifications(queriesMap)
    }
}