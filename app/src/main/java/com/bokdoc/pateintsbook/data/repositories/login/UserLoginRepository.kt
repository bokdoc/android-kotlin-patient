package com.bokdoc.pateintsbook.data.repositories.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.graphics.Picture
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.UserCustomResponse
import com.bokdoc.pateintsbook.data.webservice.login.LoginWebservice
import com.bokdoc.pateintsbook.data.webservice.login.encoder.LoginCredentialsEncoder
import com.bokdoc.pateintsbook.data.webservice.login.encoder.LoginSocialsEncoder
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.Country
import com.bokdoc.pateintsbook.data.webservice.models.Currency
import com.bokdoc.pateintsbook.data.webservice.models.Language
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class UserLoginRepository(val context: Context) {
    val loginWebservice = LoginWebservice()
    val userSharedPreference = UserSharedPreference(context)

    fun login(email: String, password: String): LiveData<DataResource<UserResponse>> {
        val body = LoginCredentialsEncoder.getJson(email, password)
        val callable = loginWebservice.login(body, userSharedPreference.sharedPrefrenceManger.getData(context.getString(R.string.token), ""))
        val liveData = MutableLiveData<DataResource<UserResponse>>()

        callable.enqueue(UserCustomResponse({
            if(it.status == DataResource.SUCCESS && it.data!=null && it.data?.isNotEmpty()!!){
                loginWebservice.webserviceManagerImpl.formatBaseUrl(it.data!![0].countryString,
                        it.data!![0].languageString,it.data!![0].currencyString)
            }
            liveData.value = it
        }, arrayOf(UserResponse::class.java, Country::class.java, Currency::class.java, Language::class.java),
                WeakReference(userSharedPreference), WeakReference(context)))

        return liveData
    }

    fun resetPassword(emailOrMobile: String): LiveData<DataResource<Boolean>> {
        val body = LoginCredentialsEncoder.getJson(emailOrMobile)
        val callable = loginWebservice.resetPassword(body, userSharedPreference.sharedPrefrenceManger
                .getData(context.getString(R.string.token), ""))
        val liveData = MutableLiveData<DataResource<Boolean>>()
        callable.enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

    fun loginWithSocials(providerName: String, token: String, providerId: String, secret: String = "",
                         name: String, picture: String, email: String): LiveData<DataResource<UserResponse>> {

        val body = LoginSocialsEncoder.getJson(providerId, token, secret, email, name, picture)
        Log.i("socialJson", body)
        val callable = loginWebservice.loginWithSocials(providerName, body, userSharedPreference.sharedPrefrenceManger
                .getData(context.getString(R.string.token), ""))
        val liveData = MutableLiveData<DataResource<UserResponse>>()

        callable.enqueue(UserCustomResponse({
            if(it.status == DataResource.SUCCESS && it.data!=null && it.data?.isNotEmpty()!!){
                loginWebservice.webserviceManagerImpl.formatBaseUrl(it.data!![0].countryString,
                        it.data!![0].languageString,it.data!![0].currencyString)
            }
            liveData.value = it
        }, arrayOf(UserResponse::class.java, Country::class.java, Currency::class.java, Language::class.java),
                WeakReference(userSharedPreference), WeakReference(context)))


        return liveData

    }
}