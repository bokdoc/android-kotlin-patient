package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.*
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.SurgeryFees
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails

object LinkCardDetailsConverter {

    fun convert(linksCardsDetailsResources: List<LinksCardsDetailsResource>): List<LinksCardsDetails> {
        var cardsLinksDetails = ArrayList<LinksCardsDetails>()
        cardsLinksDetails.add(LinksCardsDetails())
        linksCardsDetailsResources.forEach { linksResource ->
            cardsLinksDetails[0].apply {
                mainProfileInfo = LinksCardsDetails.ProfileInfo().apply {
                    profileId = linksResource.id

                    linksResource.name?.let {
                        profileName = it
                    }

                    linksResource.location?.let {
                        location = it
                    }

                    linksResource.profileType?.get(linksResource.document).let {
                        if (it != null) {
                            profileType = it.id.toInt()
                        }
                    }
                    /*
                    linksResource.profileTitle?.get(linksResource.document)?.let {
                        if (it.name.isNotEmpty())
                            profileName = it.name + " " + linksResource.name
                    }
                    */

                    linksResource.gender?.let {
                        gender = it
                    }

                    /*
                    linksResource.mainSpeciality?.let {
                        profileMainSpeciality = it
                    }
                    */

                    linksResource.specialityTitle?.let {
                        profileMainSpeciality = it
                    }

                    linksResource.picture?.let {
                        profilePicture = it
                    }


                }
                linksResource.rotators?.let {
                    rotators = it
                }

                linksResource.serviceType?.let {
                    serviceType = it
                }

                linksResource.bookType?.let {
                    bookType = it
                }



            }
            linksResource.filterRelations?.get(linksResource.document).let {
                if (it?.clinics != null) {
                    cardsLinksDetails[0].title = it.clinics?.get(linksResource.document)?.name!!
                    val sectionDataResource = it.clinics?.get(linksResource.document)?.data?.get(linksResource.document)
                    if (sectionDataResource != null)
                        sectionDataResource.slug?.let {
                            cardsLinksDetails[0].slug = it
                        }

                    val clinicsResources = sectionDataResource?.clinics?.get(sectionDataResource.document)

                    if (clinicsResources != null)
                        cardsLinksDetails[0].rowsInfo = getDetailsFromClinics(clinicsResources)
                } else {
                    val sectionDataResource = it?.bokDocServices?.get(it.document)?.data?.get(it.document)
                    if (it?.bokDocServices != null && sectionDataResource != null) {
                        it.bokDocServices?.get(it.document)?.name?.let {
                            cardsLinksDetails[0].title = it
                        }
                        sectionDataResource.slug?.let {
                            cardsLinksDetails[0].slug = it
                        }

                        if (sectionDataResource.surgeries != null) {
                            sectionDataResource.surgeries?.get(sectionDataResource.document)?.let {
                                cardsLinksDetails[0].rowsInfo = getDetailsFromSurgeries(it)
                            }

                        } else
                            if (sectionDataResource.consulting != null) {
                                sectionDataResource.consulting?.get(sectionDataResource.document)?.let {
                                    cardsLinksDetails[0].rowsInfo = getDetailsFromOnlineConsulting(it)
                                }

                            } else if (sectionDataResource.homeVisits != null) {
                                sectionDataResource.homeVisits?.get(sectionDataResource.document)?.let {
                                    cardsLinksDetails[0].rowsInfo = getDetailsFromHomeVisits(it)
                                }

                            }
                            else if (sectionDataResource.clinics != null) {
                                sectionDataResource.clinics?.get(sectionDataResource.document)?.let {
                                    cardsLinksDetails[0].rowsInfo = getDetailsFromClinics(it)
                                }

                            }
                    }
                }

            }
        }

        return cardsLinksDetails
    }

    private fun getDetailsFromClinics(clinicsCardsDetails: List<ClinicsCardsDetails>): ArrayList<LinksCardsDetails.RowsInfo> {
        val linksCardsDetails = ArrayList<LinksCardsDetails.RowsInfo>()
        clinicsCardsDetails.forEach {
                linksCardsDetails.add(LinksCardsDetails.RowsInfo().apply {
                    this.id = it.id
                    it.name?.let {
                        this.name = it
                    }

                    it.location?.let {location->
                        this.location = location
                    }


                    it.mainFees?.let {
                        mainFees = it
                    }

                    it.subMainFees?.let {
                        subMainFees = it
                    }


                    it.speciality?.get(it.document).let {
                        if (it != null) {
                            this.speciality = it.name
                        }
                    }
                    //  rotators = it.rotators
                    this.profileInfo = LinksCardsDetails.ProfileInfo().apply {
                        if (it.profile != null)
                            it.profile?.get(it.document).let {
                                if (it == null)
                                    return@forEach

                                profileId = it.id

                                it.name?.let {
                                    profileName = it
                                }

                             //   profileMainSpeciality = it.mainSpeciality + " " + it.profileTitle?.get(it.document)?.name

                                it.specialityTitle?.let {
                                    profileMainSpeciality = it
                                }

                                it.picture?.let {
                                    profilePicture = it
                                }

                                it.gender?.let {
                                    gender = it
                                }

                                it.profileType?.get(it.document).let {
                                    if (it != null) {
                                        profileType = it.id.toInt()
                                    }
                                }
//                                it.profileTitle?.get(it.document)?.let {
//                                    if (it.name.isNotEmpty())
//                                        profileName = it.name + " " + profileName
//                                }
                            }

                    }
                    if (it.duration != null)
                        duration = it.duration!!
                    isAppointmentReservation = it.isAppointmentReservation
                    this.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))
                    if (it.bookButton != null)
                        bookButton = it.bookButton!!

                    if(it.secondaryDurationConsulting!=null)
                        secondaryDurationConsulting = it.secondaryDurationConsulting!!

                    if(it.secondaryDurations!=null)
                        secondaryDurations = KeysValuesConverter.convert(it.secondaryDurations!!)
                })
        }

        return linksCardsDetails
    }

    private fun getDetailsFromSurgeries(clinicsCardsDetails: List<SurgeryCardDetails>): ArrayList<LinksCardsDetails.RowsInfo> {
        val linksCardsDetails = ArrayList<LinksCardsDetails.RowsInfo>()
        clinicsCardsDetails.forEach {

                linksCardsDetails.add(LinksCardsDetails.RowsInfo().apply {
                    this.id = it.id
                    it.name?.let {
                        this.name = it
                    }

                    it.location?.let {
                        this.location = it
                    }

                    if (it.mainFees != null)
                       mainFees = it.mainFees!!
                    // rotators = it.rotators

                    if (it.subMainFees != null)
                        subMainFees = it.subMainFees!!

                    it.speciality?.get(it.document).let {
                        if (it != null) {
                            this.speciality = it.name
                        }
                    }
                    if (it.duration != null)
                        duration = it.duration!!
                    isAppointmentReservation = it.isAppointmentReservation
                    this.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))
                    this.profileInfo = LinksCardsDetails.ProfileInfo().apply {
                        if (it.profile != null)
                            it.profile?.get(it.document).let {
                                if (it == null)
                                    return@forEach

                                profileId = it.id
                                it.name?.let {
                                    profileName = it
                                }

                               // profileMainSpeciality = it.mainSpeciality + " " + it.profileTitle?.get(it.document)?.name

                                it.specialityTitle?.let {
                                    profileMainSpeciality = it
                                }

                                it.gender?.let {
                                    gender = it
                                }


                                it.picture?.let {
                                    profilePicture = it
                                }

                                it.profileType?.get(it.document).let {
                                    if (it != null) {
                                        profileType = it.id.toInt()
                                    }
                                }
//                                it.profileTitle?.get(it.document)?.let {
//                                    if (it.name.isNotEmpty())
//                                        profileName = it.name + " " + profileName
//                                }
                            }

                    }
                    if (it.bookButton != null)
                        bookButton = it.bookButton!!

                    if(it.secondaryDurationConsulting!=null)
                        secondaryDurationConsulting = it.secondaryDurationConsulting!!

                    if(it.secondaryDurations!=null)
                        secondaryDurations = KeysValuesConverter.convert(it.secondaryDurations!!)
                })
            }
        return linksCardsDetails
    }


    private fun getDetailsFromHomeVisits(clinicsCardsDetails: List<HomeVisitsService>): ArrayList<LinksCardsDetails.RowsInfo> {
        val linksCardsDetails = ArrayList<LinksCardsDetails.RowsInfo>()
        clinicsCardsDetails.forEach {
                linksCardsDetails.add(LinksCardsDetails.RowsInfo().apply {
                    this.id = it.id
                    it.name?.let {
                        this.name = it
                    }

                    this.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))
                    if (it.bookButton != null)
                        bookButton = it.bookButton!!
                })
        }
        return linksCardsDetails
    }

    private fun getDetailsFromOnlineConsulting(clinicsCardsDetails: List<ConsultingService>): ArrayList<LinksCardsDetails.RowsInfo> {
        val linksCardsDetails = ArrayList<LinksCardsDetails.RowsInfo>()
        clinicsCardsDetails.forEach {
            linksCardsDetails.add(LinksCardsDetails.RowsInfo().apply {
                this.id = it.id
                it.name?.let {
                    name = it
                }

                it.speciality?.get(it.document).let {
                    if (it != null) {
                        this.speciality = it.name
                    }
                }
                //rotators = it.rotators

                this.profileInfo = LinksCardsDetails.ProfileInfo().apply {
                    if (it.profile != null)
                        it.profile?.get(it.document).let {
                            if (it == null)
                                return@forEach

                            profileId = it.id
                            it.name?.let {
                                profileName = it
                            }

                            it.gender?.let {
                                gender = it
                            }

                           // profileMainSpeciality = it.mainSpeciality + " " + it.profileTitle?.get(it.document)?.name

                            it.picture?.let {
                                profilePicture = it
                            }
                            /*
                            it.mainSpeciality?.let {
                                profileMainSpeciality = it
                            }
                            */

                            it.specialityTitle?.let {
                                profileMainSpeciality = it
                            }

                            it.profileType?.get(it.document).let {
                                if (it != null) {
                                    profileType = it.id.toInt()
                                }
                            }

//                            it.profileTitle?.get(it.document)?.let {
//                                if (it.name.isNotEmpty())
//                                    profileName = it.name + " " + profileName
//                            }


                        }

                }

                 it.mainFees?.let {
                    mainFees = it
                 }

                it.subMainFees?.let {
                    subMainFees = it
                }


                if (it.duration != null)
                    duration = it.duration!!
                isAppointmentReservation = it.isAppointmentReservation

                this.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))

                if (it.bookButton != null)
                    bookButton = it.bookButton!!

                if(it.secondaryDurationConsulting!=null)
                    secondaryDurationConsulting = it.secondaryDurationConsulting!!

                if(it.secondaryDurations!=null)
                    secondaryDurations = KeysValuesConverter.convert(it.secondaryDurations!!)

                if(it.bookSecondaryDuration!=null)
                    bookSecondaryDuration = it.bookSecondaryDuration

            })
        }
        return linksCardsDetails
    }

}