package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.promo.PromoCodeInfo
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "coupon")
class PromoCodeResource : Resource(), PromoCodeInfo {

    @Json(name = "book_secondary_duration")
    override var bookSecondaryDuration: Boolean? = null


    override var coupon: String = ""

    @Json(name = "service_pivot_id")
    override var servicePivotId: String = ""

    @Json(name = "service_pivot_type")
    override var servicePivotType: String = ""

    @Json(name = "patient_id")
    override var patientId: String = ""


}