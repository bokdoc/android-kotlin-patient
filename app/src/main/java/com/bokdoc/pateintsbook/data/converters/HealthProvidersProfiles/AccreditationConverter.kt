package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccreditationRow

object AccreditationConverter {

    fun convert(allClinicsResource: HealthProviderProfile): List<AccreditationRow> {
        val clinics = ArrayList<AccreditationRow>()
        allClinicsResource.accreditation?.get(allClinicsResource.document)?.forEach {
            clinics.add(AccreditationRow(it.title
                    , it.icon
                    , it.description
                    , it.slug))
        }
        return clinics
    }
}

