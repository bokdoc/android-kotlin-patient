package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "lookup")
class LookupsResource: Resource() {
    var diseases: HasMany<Disease>? = null
    var medications: HasMany<Medication>? = null
    var specialties: HasMany<Specialties>? = null
    var countries: HasMany<Country>? = null
    var currencies:HasMany<Currency>?=null
    var languages: HasMany<Language>? = null
}

@JsonApi(type = "disease")
class Disease:Resource(),LookupsType{
    override var idLookup: String = ""
    get() = id
    override var name: String = ""
}

@JsonApi(type = "medication")
class Medication:Resource(),LookupsType{
    override var idLookup: String = ""
    get() = id
    override var name:String = ""
}

@JsonApi(type = "speciality")
class Specialties:Resource(),LookupsType{
    override var idLookup: String = ""
    get() = id
    override var name: String = ""
}

