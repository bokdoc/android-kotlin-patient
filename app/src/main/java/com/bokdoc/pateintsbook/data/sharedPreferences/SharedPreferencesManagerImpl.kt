package com.bokdoc.pateintsbook.data.sharedPreferences

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

open class SharedPreferencesManagerImpl(context: Context) : SharedPreferencesManager {

    override fun clear(key: String) {

        sharedPreferences.edit().remove(key)
                .apply()
    }

    var sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE)
    }

    override fun saveData(key: String, value: String) {
        sharedPreferences.edit().putString(key, value)
                .apply()
    }

    override fun getData(key: String, defaultValue: String): String {
        sharedPreferences.getString(key, defaultValue)?.let {
            return it
        }
        return ""
    }

    override fun saveData(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value)
                .apply()
    }

    override fun getData(key: String, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key,defaultValue)
    }

    override fun saveData(key: String, values: Set<String>) {
        sharedPreferences.edit().putStringSet(key, values)
                .apply()
    }

    override fun getData(key: String, defaultValues: Set<String>): Set<String> {
        sharedPreferences.getStringSet(key,defaultValues)?.let {
            return it
        }
        return HashSet()
    }


    companion object {
        const val PREFERENCES_NAME = "BokDocPreferencesPatient"
    }
}