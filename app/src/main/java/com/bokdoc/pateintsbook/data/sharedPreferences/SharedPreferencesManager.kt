package com.bokdoc.pateintsbook.data.sharedPreferences

interface SharedPreferencesManager {
    fun saveData(key:String,value:String)
    fun getData(key:String,defaultValue:String):String
    fun saveData(key:String,value:Boolean)
    fun getData(key:String,defaultValue:Boolean):Boolean
    fun saveData(key:String,values:Set<String>)
    fun getData(key:String,defaultValues:Set<String>):Set<String>
    fun clear(key: String)

}