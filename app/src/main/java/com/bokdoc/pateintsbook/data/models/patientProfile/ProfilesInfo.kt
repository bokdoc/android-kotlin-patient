package com.bokdoc.pateintsbook.data.models.patientProfile

import com.bokdoc.pateintsbook.data.models.Picture

class ProfilesInfo{
    var id:String = ""
    var name = ""
    var picture:Picture = Picture()
}