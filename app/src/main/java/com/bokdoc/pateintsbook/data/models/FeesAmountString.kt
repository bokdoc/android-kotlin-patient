package com.bokdoc.pateintsbook.data.models

import com.squareup.moshi.Json

class FeesAmountString {
    var amount: String = ""
    @Json(name = "currency_symbols")
    var currencySymbols: String = ""
}