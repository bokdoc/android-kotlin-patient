package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.Location
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.Request

object HealthProvidersProfileConverter {

    fun convert(profile: Profile, isLoadNextAvailabilities: Boolean = false): HealthProvidersProfile {
        if (isLoadNextAvailabilities) {
            return HealthProvidersProfile(profile.id, profile.mainSpeciality, profile.profileTypeId, profile.picture.url, profile.name, profile.gender, profile.location.address,
                    profile.doctorsCount.toString(), profile.departmentsCount.toString(), profile.rating, profile.reviewsCount.toString(), profile.birthDate, profile.location, profile.likesCount.toString(),
                    profile.isLiked == 1, profile.isFavourite == 1, profile.isCompared == 1,
                    getNextAvaliabilites(profile))
        } else {
            return HealthProvidersProfile(profile.id, profile.mainSpeciality, profile.profileTypeId, profile.picture.url, profile.name, profile.gender, profile.location.address,
                    profile.doctorsCount.toString(), profile.departmentsCount.toString(), profile.rating, profile.reviewsCount.toString(), profile.birthDate, profile.location, profile.likesCount.toString(),
                    profile.isLiked == 1, profile.isFavourite == 1, profile.isCompared == 1)
        }
    }

    fun convert(id: String, profileType: Int): HealthProvidersProfile {
        return HealthProvidersProfile(id, "", profileType, "", "", "", "",
                "0", "0", 1f, "", "", Location(), "", true, true, false)

    }


    fun convert(profiles: List<Profile>): List<HealthProvidersProfile> {
        val healthProvidersProfiles = ArrayList<HealthProvidersProfile>()
        profiles.forEach {
            healthProvidersProfiles.add(convert(it))
        }
        return healthProvidersProfiles
    }

    fun convertRequests(requests: List<Request>): ArrayList<HealthProvidersProfile> {
        val profilesInfo = ArrayList<HealthProvidersProfile>()

        requests.forEach {
            it.doctor_data.get(it.document).let {
                profilesInfo.add(convert(it))
            }

        }


        return profilesInfo
    }

    fun getNextAvaliabilites(profile: Profile): List<NextAvailabilitiesParcelable> {
        val nextAvailabilitesParceable = ArrayList<NextAvailabilitiesParcelable>()

        profile.nextAvailabilities.forEach { nextAvailabilites ->
            nextAvailabilitesParceable.add(NextAvailabilitiesParcelable().apply {
                this.date = nextAvailabilites.date
                //this.times = nextAvailabilites.times as List<ITimesParcelable>
            })
        }

        return nextAvailabilitesParceable
    }
}