package com.bokdoc.pateintsbook.data.repositories.SearchResults

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.bookAppointement.HealthProvidersUIBookInfoImp
import com.bokdoc.pateintsbook.data.common.parser.MetaParser
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.SearchResults.SearchWebservice
import com.bokdoc.pateintsbook.data.webservice.models.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchResultsRepository(val context: Context) {
    val preferenceManger = SharedPreferencesManagerImpl(context)
    val comparisonManger = CompareSharedPreferences(context)
    val searchWebservice = SearchWebservice()


    fun searchAppointment(values: Map<String, String>, page: Int = 1): MutableLiveData<DataResource<Profile>> {
        (values as MutableMap<String, String>)["page"] = page.toString()
        val callable = searchWebservice.searchAppointment(preferenceManger.getData(context.getString(R.string.token), ""),
                values)
        val liveData = MutableLiveData<DataResource<Profile>>()
        callable.enqueue(getCustomResponse(liveData))
        return liveData
    }

    fun searchSurgeries(values: Map<String, String>, page: Int = 1): MutableLiveData<DataResource<Profile>> {
        (values as MutableMap<String, String>).put("page", page.toString())

        val callable = searchWebservice.searchSurgeries(preferenceManger.getData(context.getString(R.string.token), ""),
                values)
        val liveData = MutableLiveData<DataResource<Profile>>()
        callable.enqueue(getCustomResponse(liveData))
        return liveData
    }

    fun searchConsulting(values: Map<String, String>, page: Int = 1): MutableLiveData<DataResource<Profile>> {
        (values as MutableMap<String, String>).put("page", page.toString())

        val callable = searchWebservice.searchConsulting(preferenceManger.getData(context.getString(R.string.token), ""),
                values)
        val liveData = MutableLiveData<DataResource<Profile>>()
        callable.enqueue(getCustomResponse(liveData))
        return liveData
    }

    private fun getCustomResponse(liveData: MutableLiveData<DataResource<Profile>>): CustomResponse<Profile> {
        return CustomResponse({
            if (it.status == DataResource.SUCCESS && it.data != null) {
                initializeProfile(it.data!!)
            }
            it.paginationMeta.pagination = it.meta.pagination
            liveData.value = it
        }, arrayOf(Profile::class.java,
                ProfileType::class.java,
                Language::class.java, Speciality::class.java, Service::class.java, PaymentMethods::class.java,
                InsuranceCarrier::class.java, PaymentType::class.java, Department::class.java))
    }

    private fun initializeProfile(profiles: List<Profile>) {

        val savedCompareIds = comparisonManger.getComparesList()
        profiles.forEach { profile ->
            profile.profileType?.get(profile.document)?.name?.let { name ->
                profile.profileTypeName = name
            }
            savedCompareIds.forEach {
                if (it == profile.id)
                    profile.isCompared = 1
            }
        }
    }


    fun getHealthProvidersProfile(profile: Profile, isLoadNextAvailabilities: Boolean): HealthProvidersProfile {
        return HealthProvidersProfileConverter.convert(profile, isLoadNextAvailabilities)
    }

    fun getHealthProvidersBookInfo(profile: Profile): HealthProvidersUIBookInfoImp {
        return HealthProvidersUIBookInfoImp().apply {
            this.profileId = profile.id.toInt()
            this.profileTypeId = profile.profileTypeId
            profile.profileType?.get(profile.document)?.name?.let {
                this.profileTypeName = it
            }
            this.specialty = ""
            this.name = profile.name
            this.location = profile.location
            this.picture = profile.picture.url
            if (profile.clinics.isNotEmpty())
                this.clinicId = profile.clinics[0].id.toInt()
            if (profile.departmentsIds.isNotEmpty())
                this.departmentId = profile.departmentsIds[0]
            if (profile.serviceIds.isNotEmpty())
                this.serviceId = profile.serviceIds[0]
            if (profile.paymentTypes.size() != 0)
                this.paymentTypeId = profile.paymentTypes.get(profile.document)[0].id.toInt()
            if (profile.paymentMethods.size() != 0)
                this.paymentTypeMethod = profile.paymentMethods.get(profile.document)[0].id.toInt()
            this.nextAvailabilities = getNextAvaliabilites(profile)
            this.doctorId = profile.id.toInt()

        }
    }

    fun getNextAvaliabilites(profile: Profile): List<NextAvailabilitiesParcelable> {
        val nextAvailabilitesParceable = ArrayList<NextAvailabilitiesParcelable>()

        profile.nextAvailabilities.forEach { nextAvailabilites ->
            nextAvailabilitesParceable.add(NextAvailabilitiesParcelable().apply {
                this.date = nextAvailabilites.date
                // this.times = nextAvailabilites.times as List<ITimesParcelable>
            })
        }

        return nextAvailabilitesParceable
    }


    class SearchResponse(val liveData: MutableLiveData<DataResource<Profile>>) : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if (response?.body() != null) {
                val dataResource = DataResource<Profile>()

                val json = response.body()!!.string()
                try {
                    val profiles = Parser.parseJson(json, arrayOf(Profile::class.java,
                            ProfileType::class.java,
                            Language::class.java, Speciality::class.java, Service::class.java, PaymentMethods::class.java,
                            InsuranceCarrier::class.java, PaymentType::class.java, Department::class.java))

                    dataResource.data = profiles as MutableList<Profile>
                    dataResource.paginationMeta = MetaParser.paresPaginationMeta(json)
                    liveData.value = dataResource
                } catch (e: Exception) {
                    //  dataResource.data = arrayListOf()
                    dataResource.status = "Fail"
                    Log.wtf("Error in Data", e.message)
                }


            }
        }

    }

    companion object {
        const val IGNORED_QUERY_KEY = "IgnoredQueryKey"
    }


}