package com.bokdoc.pateintsbook.data.repositories.filter

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.FiltersChildrenConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.webservice.filterResults.FilterWebservice
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.ui.filters.FiltersSectionHeader
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilterRepositories(
        val context: Application,
        val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
        val filterWebservice: FilterWebservice) {

    fun getFilters(screenType: String): LiveData<DataResource<FiltersSectionHeader>> {
        val liveData = MutableLiveData<DataResource<FiltersSectionHeader>>()
        val dataResource = DataResource<FiltersSectionHeader>()
        filterWebservice.getFilterOptions(screenType, sharedPreferencesManagerImpl.getData(context.getString(R.string.token), ""))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        dataResource.message = t?.message!!
                        liveData.value = dataResource
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.body() != null) {
                            val filtersResource = Parser.parseJson(response?.body()?.string()!!, arrayOf(FiltersOptions::class.java,
                                    YearsExperience::class.java, SpecialityFilter::class.java, Fee::class.java,
                                    InsuranceCarrierFilter::class.java, PaymentTypeFilter::class.java, Gender::class.java,
                                    LanguageFiler::class.java, Rating::class.java, IsHomeVisit::class.java, FreeAppointment::class.java,
                                    Date::class.java, TimeFilter::class.java, MapFilter::class.java, ProfileTypes::class.java,
                                    IsAppointmentReservation::class.java))

                            dataResource.data = FiltersChildrenConverter.convert(filtersResource as List<FiltersOptions>)
                            liveData.value = dataResource
                        } else {
                            dataResource.message = response?.message()!!
                            liveData.value = dataResource
                        }
                    }

                })
        return liveData
    }

    fun getFilterQuery(filtersSelection: Map<String, ArrayList<String>>): String {
        return filterWebservice.getFilterQuery(filtersSelection)
    }
}