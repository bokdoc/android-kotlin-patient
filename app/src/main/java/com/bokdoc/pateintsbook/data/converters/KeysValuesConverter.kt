package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.KeyValue
import com.bokdoc.pateintsbook.data.webservice.models.KeyValueResource

object KeysValuesConverter {
    fun convert(keysValuesResources:List<KeyValueResource>):List<KeyValue>{
        val keysValues = arrayListOf<KeyValue>()
        keysValuesResources.forEach {
            keysValues.add(KeyValue().apply {
                if(it.key!=null)
                    key = it.key!!

                if(it.value!=null)
                    value = it.value!!

                if(it.isSelected!=null)
                    isSelected = it.isSelected!!
            })
        }

        return keysValues
    }

}