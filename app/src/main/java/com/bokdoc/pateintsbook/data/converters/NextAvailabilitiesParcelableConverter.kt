package com.bokdoc.pateintsbook.data.converters

import android.util.Log
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource

object NextAvailabilitiesParcelableConverter {

    fun convert(nextAvailabilities: List<NextAvailabilityResource>): List<NextAvailabilitiesParcelable> {
        val nextAvailabilitesParceable = ArrayList<NextAvailabilitiesParcelable>()

        nextAvailabilities.forEach { nextAvailabilites ->
            nextAvailabilitesParceable.add(NextAvailabilitiesParcelable().apply {
                this.idNextAvailabilities = nextAvailabilites.id
                dateId = nextAvailabilites.dateId
                Log.i("dateId",dateId.toString())
                this.date = nextAvailabilites.date
                this.date_day = nextAvailabilites.date_day
                this.from = nextAvailabilites.from
                this.to = nextAvailabilites.to
                this.status = nextAvailabilites.status
                this.book_button = nextAvailabilites.book_button
                this.dateNormalized = nextAvailabilites.dateNormalized
            })
        }
        return nextAvailabilitesParceable
    }

}