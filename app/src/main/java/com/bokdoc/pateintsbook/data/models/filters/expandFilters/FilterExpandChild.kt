package com.bokdoc.pateintsbook.data.models.filters.expandFilters

import android.os.Parcel
import android.os.Parcelable

class FilterExpandChild(val name:String, val params:String, val unit:String="", var isSelected:Boolean=false,
                        var rating:Float = 0.0f, var ranges:Array<Int> = Array(0){0}):Parcelable{



    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readFloat())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(params)
        parcel.writeString(unit)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeFloat(rating)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilterExpandChild> {
        override fun createFromParcel(parcel: Parcel): FilterExpandChild {
            return FilterExpandChild(parcel)
        }

        override fun newArray(size: Int): Array<FilterExpandChild?> {
            return arrayOfNulls(size)
        }
    }

}