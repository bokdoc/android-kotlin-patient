package com.bokdoc.pateintsbook.data.models.nextAvailabilities

import android.os.Parcelable

interface INextAvailabilitiesParcelable : INextAvailabilities, Parcelable {
}