package com.bokdoc.pateintsbook.data.webservice.forgetPassword.encoder

import com.bokdoc.pateintsbook.data.models.forgetPassword.ForgetPassword
 import com.bokdoc.pateintsbook.data.webservice.models.forgetPassword.ForgetPasswordResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object ForgetPasswordEncoder {

    fun getJson(forgetPassword: ForgetPassword): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<ForgetPasswordResource>()
        val forgetPasswordResource = ForgetPasswordResource()
        forgetPasswordResource.id = "1"
        forgetPasswordResource.emailMobile = forgetPassword.mobileNumber
        if (forgetPassword.newPassword.isNotEmpty())
            forgetPasswordResource.password = forgetPassword.newPassword

        if (forgetPassword.mobileVerificationCode.isNotEmpty())
            forgetPasswordResource.recoverCode = forgetPassword.mobileVerificationCode
        document.set(forgetPasswordResource)
        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(ForgetPasswordResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}