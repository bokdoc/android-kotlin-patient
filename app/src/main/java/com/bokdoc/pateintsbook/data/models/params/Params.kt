package com.bokdoc.pateintsbook.data.models.params

class Params : IParams {
    override var token: String? = ""
    override var id: String? = ""
    override var screenType: String? = ""
}