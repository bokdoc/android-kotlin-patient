package com.bokdoc.pateintsbook.data.models.healthProviders

import com.intrusoft.sectionedrecyclerview.Section

class AccommodationSections(val title:String,val children:List<String>): Section<String> {

    override fun getChildItems(): List<String> {
        return children
    }

    companion object {
        const val ABOUT_SECTION = "About"
        const val PATIENT_ROOM_SECTION = "Patient Room"
        const val LOCATION_ROOM_SECTION = "Location"
        const val SOCIAL_NETWORKS_SECTION = "Social Networks"
        const val PAYMENTS_METHODS_SECTION = "Payments Methods" // not used now but used in future
        const val AMBULANCE_SECTION = "Ambulance"
        const val BEDS_NUMBER_SECTION = "Beds Number"
        const val ACCOMPANYING_PERSON_SECTION = "Accompanying Person"
        const val ACCOMMODATIONS_SECTION = "accommodations"
    }
}