package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.medications

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice

class MedicationsWebservice(webserviceManagerImpl: WebserviceManagerImpl):MedicalHistoryWebservice
(webserviceManagerImpl) {
    override fun getType(): String {
        return "medications"
    }
}