package com.bokdoc.pateintsbook.data.webservice.models

import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profileAddress")
class ProfileAddressResource : Resource() {
    var address: String = ""
    var longitude: String = ""
    var latitude: String = ""
    var country: String = ""
    var city: String = ""
    var region: String = ""
}