package com.bokdoc.pateintsbook.data.models.medicalHistory

import android.os.Parcel
import android.os.Parcelable

class MedicalHistoryImpl() :MedicalHistory,Parcelable {
    override var historyId: String = ""
    override var name: String = ""
    override var summary: String = ""
    override var description: String = ""

    constructor(parcel: Parcel) : this() {
        historyId = parcel.readString()
        name = parcel.readString()
        summary = parcel.readString()
        description = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(historyId)
        parcel.writeString(name)
        parcel.writeString(summary)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MedicalHistoryImpl> {
        override fun createFromParcel(parcel: Parcel): MedicalHistoryImpl {
            return MedicalHistoryImpl(parcel)
        }

        override fun newArray(size: Int): Array<MedicalHistoryImpl?> {
            return arrayOfNulls(size)
        }
    }
}