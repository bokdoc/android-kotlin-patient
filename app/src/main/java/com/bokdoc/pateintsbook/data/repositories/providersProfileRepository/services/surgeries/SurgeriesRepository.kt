package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.surgeries

import android.content.Context
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.ServicesRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.surgeries.SurgeriesWebservice

class SurgeriesRepository(appContext:Context,
                          sharedPreferencesManager: SharedPreferencesManager,
                          surgeriesWebservice: SurgeriesWebservice):ServicesRepository(appContext,sharedPreferencesManager,surgeriesWebservice) {

}