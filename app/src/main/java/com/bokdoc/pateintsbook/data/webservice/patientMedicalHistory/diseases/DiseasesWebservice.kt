package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.diseases

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice

class DiseasesWebservice(webserviceManagerImpl: WebserviceManagerImpl):MedicalHistoryWebservice(webserviceManagerImpl) {
    override fun getType(): String {
        return "diseases"
    }
}