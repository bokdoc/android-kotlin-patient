package com.bokdoc.pateintsbook.data.models

data class AutoCompleteValue(var text: String,
                             var params: String,
                             var picture: String = "",
                             var location: String = "",
                             var profileId:String = "",
                             var profileType:Int = 0)