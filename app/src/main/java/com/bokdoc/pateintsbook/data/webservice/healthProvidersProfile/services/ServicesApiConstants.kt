package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services

object ServicesApiConstants {
    const val SERVICES_TYPE = "services:type"
    const val SCAN = "scan"
    const val TEST = "Test"
    const val SURGERY = "surgery"
}