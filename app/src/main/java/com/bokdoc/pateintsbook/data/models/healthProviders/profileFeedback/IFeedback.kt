package com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback

interface IFeedback {
    var rate: Int
    var content: String
    var date: String
    var time: String
    var name: String?
    var picture: String
    var person: FeedbackPersona?
        get() {
            return null
        }
        set(value) {}
}