package com.bokdoc.pateintsbook.data.webservice.SearchResults

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import android.R.attr.scheme
import android.R.attr.fragment
import android.net.Uri


class SearchWebservice {
    var webserviceManagerImpl = WebserviceManagerImpl.instance()


    fun searchAppointment(token: String, query: String = "", page: Int = 1): Call<ResponseBody> {
        val searchApi = webserviceManagerImpl
        /*
        var newQuery = ""
        if(query.isNotEmpty())
            newQuery = "${WebserviceConstants.QUERY_MARK}$query"
            */
       // val url = webserviceManagerImpl.formatBaseUrlWithQueries(WebserviceConstants.SEARCH_PROFILES_URL + "/appointments",)
        val hashMap = HashMap<String,String>()
        hashMap.put("",query)
        hashMap.put("page",page.toString())
        return searchAppointment(token,hashMap)
    }



    fun searchAppointment(token: String, queriesMap: Map<String, String>): Call<ResponseBody> {
        val url = webserviceManagerImpl.formatBaseUrlWithQueries(arrayOf(WebserviceConstants.PROFILES,WebserviceConstants.SEARCH,
                "appointments"),
                queriesMap)
        return webserviceManagerImpl.createService(SearchApi::class.java, token).search(url)
    }

    fun searchSurgeries(token: String, query: String = "", page: Int = 1): Call<ResponseBody> {
        val hashMap = HashMap<String,String>()
        hashMap.put("",query)
        hashMap.put("page",page.toString())
        return searchSurgeries(token,hashMap)
    }


    fun searchSurgeries(token: String, queriesMap: Map<String, String>): Call<ResponseBody> {
        val url = webserviceManagerImpl.formatBaseUrlWithQueries(arrayOf(WebserviceConstants.PROFILES,WebserviceConstants.SEARCH,
                "surgeries"),
                queriesMap)
        return webserviceManagerImpl.createService(SearchApi::class.java, token).search(url)
    }


    fun searchConsulting(token: String, query: String = "", page: Int = 1): Call<ResponseBody> {
        val hashMap = HashMap<String,String>()
        hashMap.put("",query)
        hashMap.put("page",page.toString())
        return searchConsulting(token,hashMap)
    }


    fun searchConsulting(token: String, queriesMap: Map<String, String>): Call<ResponseBody> {
        val url = webserviceManagerImpl.formatBaseUrlWithQueries(arrayOf(WebserviceConstants.PROFILES,WebserviceConstants.SEARCH,
                "consultings"),
                queriesMap)
        return webserviceManagerImpl.createService(SearchApi::class.java, token).search(url)
    }

}