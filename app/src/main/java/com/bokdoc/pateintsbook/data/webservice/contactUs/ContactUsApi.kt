package com.bokdoc.pateintsbook.data.webservice.contactUs

 import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ContactUsApi {

    @POST(WebserviceConstants.CONTACT_US)
    fun contactUs(@Body credentialsJson: RequestBody): Call<ResponseBody>

}