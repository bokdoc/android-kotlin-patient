package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.notifications.Notifications
import com.bokdoc.pateintsbook.data.models.params.Params
import com.bokdoc.pateintsbook.data.models.profile.IProfile
import com.bokdoc.pateintsbook.data.models.profile.ProfileNotifications
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.notifications.NotificationsResource
import moe.banana.jsonapi2.HasOne

object NotificationsConverter {
    fun convert(inNotifications: List<NotificationsResource>): List<Notifications> {
        val notifications = ArrayList<Notifications>()
        inNotifications.forEach {
            notifications.add(Notifications().apply {
                notificationsId = it.id
                title = it.title
                dateTime = it.dateTime
                actionUrl = it.actionUrl
                content = it.content
                it.params?.let { inParams ->
                    params = Params().apply {
                        id = inParams.id
                        screenType = inParams.screenType
                    }
                }
                it.profileSource?.get(it.document)?.let {
                    profile = getProfile(it)
                }

            })
        }
        return notifications
    }

    private fun getProfile(profileResource: Profile): ProfileNotifications {
        val profile = ProfileNotifications()
        profile.profileId = profileResource.id
        profile.name = profileResource.name
        profile.mainSpeciality = profileResource.mainSpeciality
        profile.specialityTitle = profileResource.specialityTitle
        profile.picture = profileResource.picture
        profile.profileType = LookUpsParcelable().apply {
            profileResource.profileType?.get(profileResource.document).let {
                idLookup = it.toString()
                if (it?.name != null)
                    name = it.name!!
            }

        }
//        profile.profileTitle = LookUpsParcelable().apply {
//            profileResource.profileTitle.get(profileResource.document).let {
//                idLookup = it.toString()
//                if (it != null)
//                    name = it.name
//            }
//        }
        return profile
    }
}