package com.bokdoc.pateintsbook.data.models

import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.MainFeesWithoutFixed
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class LinksCardsDetailsResource : Resource() {
    var profileType: HasOne<ProfileType>? = null
    var profileTitle:HasOne<ProfileTitle>?=null
    var name:String?=null
    var picture:Picture?=null

    var location:Location?=null

    @Json(name = "main_speciality")
    var mainSpeciality:String?=null

    @Json(name = "speciality_title")
    var specialityTitle:String?=null

    var filterRelations: HasOne<FilterRelations>? = null
    var gender: String?=null
    @Json(name = "service_type")
    var serviceType:String?=null
    @Json(name = "book_type")
    var bookType:String?=null

    @Json(name = "next_availability")
    var nextAvailability:String?=null


    @Json(name = "health_provider_messages")
    var rotators: List<String>?=null



    //var speciality:HasOne<Speciality>?=null
}




@JsonApi(type = "filterRelations")
class FilterRelations : Resource() {
    var slug: String?=null
    var clinics: HasOne<ClinicsSections>? = null
    var bokDocServices: HasOne<BokDocServicesSection>? = null
}

// clinics

@JsonApi(type = "clinicsSection")
class ClinicsSections : Resource() {
    var name:String?=null
    var data: HasOne<SectionData>? = null
}

@JsonApi(type = "sectionData")
class SectionData : Resource() {
    var slug:String?=null
    var clinics: HasMany<ClinicsCardsDetails>? = null
    var surgeries: HasMany<SurgeryCardDetails>? = null
    var homeVisits: HasMany<HomeVisitsService>? = null
    var consulting: HasMany<ConsultingService>? = null

}

@JsonApi(type = "clinic")
class ClinicsCardsDetails : ProfileDetailsResource() {
    var name:String?=null
    var location:Location?=null
}

//surgeries
@JsonApi(type = "bokDocServicesSection")
class BokDocServicesSection : Resource() {
    var name:String?=null
    var data: HasOne<SectionData>? = null
}

@JsonApi(type = "Surgery")
class SurgeryCardDetails : ProfileDetailsResource() {
    var name:String?=null
    var location:Location?=null
}


//homeVisits
@JsonApi(type = "homeVisitService")
class HomeVisitsService : Resource() {
    var name:String?=null
    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null
    var fees: Fees? = null
    var nextAvailabilities: HasMany<NextAvailabilityResource>? = null

}

//consulting
@JsonApi(type = "consultingService")
class ConsultingService : ProfileDetailsResource() {
    var name:String?=null
    /*
    @Json(name = "main_fees")
    var mainFees: MainFeesWithoutFixed? = null
    */
}

@JsonApi(type = "department")
class Department : Resource()

@JsonApi(type = "sectionFilters")
class SectionFilters : Resource()

@JsonApi(type = "sectionFilter")
class SectionFilter : Resource()


open class ProfileDetailsResource : Resource() {
    var profile: HasOne<LinksCardsDetailsResource>? = null

    var speciality: HasOne<Speciality>? = null
    @Json(name = "next_availability")
    var nextAvailability:String?=null

    /*
    @Json(name = " fees")
    var fees: MainFees? = null
    */

    @Json(name = "main_fees")
    var mainFees: MainFees? = null

    @Json(name = "other_fees")
    var otherFees: MainFees? = null

    @Json(name = "sub_main_fees")
    var subMainFees: MainFees? = null

    @Json(name = "is_appointment_reservation")
    var isAppointmentReservation = false

    @Json(name = "duration")
    var duration: String? = null

    var nextAvailabilities: HasMany<NextAvailabilityResource>? = HasMany()

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null


    // this return in case of consulting specially in sexology speciality
    @Json(name = "secondary_durations_consulting")
    var secondaryDurations:List<KeyValueResource>?=null

    @Json(name = "is_secondary_durations_consulting")
    var secondaryDurationConsulting:Boolean?=null

    @Json(name = "book_secondary_duration")
    var bookSecondaryDuration:Boolean?=null

    /*
    var rotators = ArrayList<String>().apply {
        add("Test Rotators 1")
        add("Test Rotators 2")
        add("Test Rotators 3")
    }
    */
}

