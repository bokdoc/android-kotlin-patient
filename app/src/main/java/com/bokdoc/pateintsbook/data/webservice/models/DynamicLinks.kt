package com.bokdoc.pateintsbook.data.webservice.models

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

class DynamicLinks() : Parcelable {
    var key: String = ""
    var label: String = ""
    @Json(name = "endpoint_url")
    var endpointUrl: String = ""
    @Json(name = "target_screen")
    var targetScreen: String = ""
    var fees: Fees = Fees()
    var count: String = ""
    var icon: String? = ""
    @Json(name = "total_pages")
    var totalPages: Int = 0

    var isTargetList: Boolean = false
        get() = targetScreen == TARGET_LIST

    var isTargetBookAppointments: Boolean = false
        get() = targetScreen == TARGET_BOOK_APPOINTMENTS || targetScreen == TARGET_BOOK_APPOINTMENTS_UNDERSCORE

    var isKeyClinics: Boolean = false
        get() = key == KEY_CLINICS

    var isKeySurgeries: Boolean = false
        get() = key == KEY_SURGERIES


    var isKeyHomeVisits: Boolean = false
        get() = key == KEY_HOME_VISITS

    var isKeyOnlineConsulting: Boolean = false
        get() = key == KEY_ONLINE_CONSULTING

    constructor(parcel: Parcel) : this() {
        key = parcel.readString()
        label = parcel.readString()
        endpointUrl = parcel.readString()
        targetScreen = parcel.readString()
        fees = parcel.readParcelable(Fees::class.java.classLoader)
        count = parcel.readString()
        icon = parcel.readString()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(key)
        parcel.writeString(label)
        parcel.writeString(endpointUrl)
        parcel.writeString(targetScreen)
        parcel.writeParcelable(fees, flags)
        parcel.writeString(count)
        parcel.writeString(icon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DynamicLinks> {
        override fun createFromParcel(parcel: Parcel): DynamicLinks {
            return DynamicLinks(parcel)
        }

        override fun newArray(size: Int): Array<DynamicLinks?> {
            return arrayOfNulls(size)
        }

        const val TARGET_LIST = "list-view"
        const val TARGET_BOOK_APPOINTMENTS = "book-form"
        const val TARGET_BOOK_APPOINTMENTS_UNDERSCORE = "book_form"
        const val KEY_CLINICS = "clinics_count"
        const val KEY_SURGERIES = "surgeries_link"
        const val KEY_DEPARTMENTS = "departments"
        const val KEY_DOCTORS = "doctors"
        const val KEY_HOME_VISITS = "home_visit"
        const val KEY_ONLINE_CONSULTING = "online_consulting"
        const val KEY_ONLINE_CONSULTING_ICON = "ConsultingLinkIcon"
        const val KEY_CLINIC_ICON = "AppointmentLinkIcon"
        const val KEY_SURGERIES_ICON = "SurgeryLinkIcon"

    }


}