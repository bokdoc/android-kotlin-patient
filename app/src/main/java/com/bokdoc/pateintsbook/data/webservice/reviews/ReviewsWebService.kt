package com.bokdoc.pateintsbook.data.webservice.reviews

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class ReviewsWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getReviews(id: String, token: String, serviceType: String, page: Int): Call<ResponseBody> {
        val reviewsApi = webserviceManagerImpl.createService(ReviewsApi::class.java, token)
        return reviewsApi.getReviews(id, "persona:type(profile)", serviceType, page)
    }

    fun getProvidersReviews(id: String, token: String, serviceType: String, page: Int): Call<ResponseBody> {
        val reviewsApi = webserviceManagerImpl.createService(ReviewsApi::class.java, token)
        return reviewsApi.getProvidersReviews(id, "review.persona:type(patient),review:service_type($serviceType)", page)
    }
}