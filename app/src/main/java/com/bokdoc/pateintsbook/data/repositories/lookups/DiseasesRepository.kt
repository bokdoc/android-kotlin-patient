package com.bokdoc.pateintsbook.data.repositories.lookups

import android.arch.lifecycle.LiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.lookups.LookupsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.Disease
import moe.banana.jsonapi2.Resource

class DiseasesRepository(appContext: Context,sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                         webservice: LookupsWebservice):
        LookupsRepository(appContext,sharedPreferencesManagerImpl,webservice) {

    fun lookupsDiseases():LiveData<DataResource<LookupsType>>{
        return lookups(LookupsWebservice.DISEASES_QUERY,Disease::class.java)
    }

}