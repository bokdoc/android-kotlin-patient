package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.InsuranceCarriers

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.InsuranceCarrierConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.SingleSurgeryConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.SurgeriesConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.InsuranceCarriers.InsuranceCarriersWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.surgeries.ProfileSurgeriesWebService
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.InsuranceCarriersRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SingleSurgery
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import com.bokdoc.pateintsbook.data.webservice.models.Speciality

class InsuranceCarriersRepository(private val appContext: Context,
                                  private val sharedPreferencesManager: SharedPreferencesManager,
                                  private val surgeriesWebService: InsuranceCarriersWebService) {

    fun getInsuranceCarriers(id: String, service: String): LiveData<DataResource<InsuranceCarriersRow>> {

        val liveData = MutableLiveData<DataResource<InsuranceCarriersRow>>()
        val dataResource = DataResource<InsuranceCarriersRow>()

        surgeriesWebService.getInsuranceCarriers(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (it.data!!.isNotEmpty()) {
                        dataResource.data = InsuranceCarrierConverter.convert(it.data!![0])
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, InsuranceCarrierResource::class.java, ProfileType::class.java)))

        return liveData
    }
}