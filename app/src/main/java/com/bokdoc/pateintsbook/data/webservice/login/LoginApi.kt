package com.bokdoc.pateintsbook.data.webservice.login

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

interface LoginApi {
    @POST(WebserviceConstants.AUTH+"/login")
    fun login(@Body credentialsJson:RequestBody):Call<ResponseBody>

    @POST(WebserviceConstants.AUTH+"/password")
    fun resetPassword(@Body credentialsJson:RequestBody):Call<ResponseBody>

    @POST(WebserviceConstants.AUTH+"/{provider}/login")
    fun loginWithSocials(@Path("provider") provider:String,@Body tokensJson:RequestBody):Call<ResponseBody>

}