package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.models.HealthProfileProvidersResource
import com.bokdoc.pateintsbook.data.webservice.models.ProfileType
import com.bokdoc.pateintsbook.data.webservice.models.ServiceResource
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.ServicesWebservice
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class ServicesRepository(private val appContext: Context,
                              private val sharedPreferencesManager: SharedPreferencesManager,
                              private val servicesWebservice: ServicesWebservice) {

    fun getServices(id: String): LiveData<DataResource<Service>> {
        val liveData = MutableLiveData<DataResource<Service>>()
        val dataResource = DataResource<Service>()
        servicesWebservice.getServices(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val profile = Parser.parseJson(response.body()!!.string(), arrayOf(HealthProfileProvidersResource::class.java, NextAvailabilityResource::class.java,
                            ProfileType::class.java, ServiceResource::class.java))[0] as HealthProfileProvidersResource
                    dataResource.data = profile.services.get(profile.document) as List<Service>
                    liveData.value = dataResource
                } else {
                    dataResource.message = response?.errorBody()!!
                    liveData.value = dataResource
                }
            }

        })
        return liveData
    }
}