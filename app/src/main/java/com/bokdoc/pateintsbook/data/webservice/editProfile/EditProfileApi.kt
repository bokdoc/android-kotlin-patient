package com.bokdoc.pateintsbook.data.webservice.editProfile

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface EditProfileApi {

    @PUT(WebserviceConstants.PATIENTS + WebserviceConstants.EDIT + "/{id}")
    fun editProfile(@Path("id") type: String, @Body requestBody: RequestBody): Call<ResponseBody>

}