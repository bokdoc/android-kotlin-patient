package com.bokdoc.pateintsbook.data.repositories.twillio

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.models.ConsultingStreamResource
import com.bokdoc.pateintsbook.data.webservice.twillio.TwillioWebService

class TwillioRepository(val context: Context, val twillioWebService: TwillioWebService, val sharedPreferencesManagerImpl: UserSharedPreference) {


    fun getTwillioToken(id: String, token: String): LiveData<DataResource<ConsultingStreamResource>> {
        val liveData = MutableLiveData<DataResource<ConsultingStreamResource>>()
        val callable = twillioWebService.getTwillioToken(id, token)
        callable.enqueue(CustomResponse<ConsultingStreamResource>({
            liveData.value = it
        }, arrayOf(ConsultingStreamResource::class.java)))
        return liveData
    }


    fun leaveTwillioToken(url: String): LiveData<DataResource<Boolean>> {
        val liveData = MutableLiveData<DataResource<Boolean>>()
        val callable = twillioWebService.leaveTwillio(url, sharedPreferencesManagerImpl.getToken())
        callable.enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

}