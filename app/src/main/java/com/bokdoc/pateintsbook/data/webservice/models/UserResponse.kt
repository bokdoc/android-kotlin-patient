package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "patient")
class UserResponse : Resource() {
    var currencyId: Int = 0

    @Json(name = "status_type")
    var statusType: String = ""

    @Json(name = "country_id")
    var countryId: Int = 0
    @Json(name = "region_id")
    var regionId: Int = 0
    @Json(name = "deleted_at")
    lateinit var deletedAt: String
    @Json(name = "deleter_id")
    var deleterId: Int = 0
    @Json(name = "creator_id")
    var creatorId: Int = 0
    @Json(name = "city_id")
    var cityId: Int = 0
    var picture: Picture = Picture()
    var overview: String = ""
    var age = ""
    /*
    @Json(name="language_id")
    var languageId:Int = 0
    */

    var countryLookup = LookUpsParcelable()


    var currencyLookup = LookUpsParcelable()


    var languageLookup = LookUpsParcelable()

    var country: HasOne<Country> = HasOne()
    var language: HasOne<Language> = HasOne()
    var currency: HasOne<Currency> = HasOne()

    @Json(name = "country_code")
    var countryString: String = ""
    @Json(name = "currency_code")
    var currencyString: String = ""
    @Json(name = "language_code")
    var languageString: String = ""

    @Json(name = "updated_at")
    lateinit var updatedAt: String
    var name: String = ""
    var birthdate: String = ""
    @Json(name = "created_at")
    var createdId: Int = 0
    lateinit var gender: String
    var token: String = ""
    var email: String = ""
    var password: String = ""
    @Json(name = "mobile_number")
    var phone: String  =""

    var isGuest = false
    get() = statusType == "guest"


    companion object {
        const val GENDER_MALE = "Male"
        const val GENDER_FEMALE = "Female"
    }

}

open class Info : Resource(),LookupsType {
    override var name: String = ""
    override var idLookup: String = ""
        get() {
            return id
        }
    lateinit var code: String
    lateinit var summary: String
    lateinit var description: String
}


@JsonApi(type = "country")
class Country : Info()

@JsonApi(type = "currency")
class Currency : Info()