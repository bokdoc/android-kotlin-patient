package com.bokdoc.pateintsbook.data.webservice.utils

import okhttp3.MediaType
import okhttp3.RequestBody

object RequestBodyCreator {

    fun createJsonRequest(content:String):RequestBody{
        return RequestBody.create(MediaType.parse(JSON_REQUEST_TYPE),content)
    }

    const val JSON_REQUEST_TYPE = "application/json"
}