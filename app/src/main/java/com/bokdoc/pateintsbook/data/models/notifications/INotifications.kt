package com.bokdoc.pateintsbook.data.models.notifications

import com.bokdoc.pateintsbook.data.models.DateTime
import com.bokdoc.pateintsbook.data.models.params.IParams
import com.bokdoc.pateintsbook.data.models.params.Params
import com.bokdoc.pateintsbook.data.models.profile.IProfile
import com.bokdoc.pateintsbook.data.models.profile.ProfileNotifications
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Profile


interface INotifications {
    var notificationsId: String?
    var title: String?
    var content: String?
    var dateTime: DateTime?
    var params: Params?
    var actionUrl: List<DynamicLinks>?
    var deleteUrl: String?
    var profile: ProfileNotifications?
}