package com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory

import android.content.Context
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.models.medicalHistory.Speciality
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.MedicalHistoryWebservice

class SpecialitiesRepository(appContext: Context,
                                sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                                medicalHistoryWebservice: MedicalHistoryWebservice)
    :MedicalHistoryRepository(appContext,sharedPreferencesManagerImpl,medicalHistoryWebservice,Speciality::class.java)