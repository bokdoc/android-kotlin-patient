package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile

object ProviderReviewsConverter {

    fun convert(healthProvider: HealthProviderProfile): List<Feedback> {
        val reviews = ArrayList<Feedback>()
        if (healthProvider.review != null) {
            healthProvider.review!!.get(healthProvider.document).forEach {
                val feedback = Feedback()
                feedback.content = it.content
                it.personaResource?.get(healthProvider.document)?.name?.let {
                    feedback.name = it
                }
                it.personaResource?.get(healthProvider.document)?.let {
                    feedback.picture = it.picture.url
                }
                feedback.rate = it.rate
                feedback.date = it.date_time.date
                feedback.time = it.date_time.time
                reviews.add(feedback)
            }
        }
        return reviews
    }
}