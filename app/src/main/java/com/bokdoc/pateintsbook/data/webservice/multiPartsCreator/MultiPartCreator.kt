package com.bokdoc.pateintsbook.data.webservice.multiPartsCreator

import com.bokdoc.pateintsbook.data.webservice.progressableRequest.IUploadProgressable
import com.bokdoc.pateintsbook.data.webservice.progressableRequest.ProgressableRequestBody
import okhttp3.MediaType
import okhttp3.MultipartBody
import java.io.File
import okhttp3.RequestBody



class MultiPartCreator {

    fun getFilesParts(files: Array<File>,types:Array<String>,progressableListeners:IUploadProgressable)
            :List<MultipartBody.Part>{

        val multiPartsList = ArrayList<MultipartBody.Part>()
        var requestBody:RequestBody
        val size = files.size
        (0 until size).forEach {
            requestBody = createRequest(files[it],types[it],progressableListeners)
            multiPartsList.add(MultipartBody.Part.createFormData(DEFAULT_FORM_NAME,files[it].name,requestBody))
        }
        return multiPartsList
    }


    private fun createRequest(file:File,type:String,progressableListener:IUploadProgressable):RequestBody{
        val requestFile = RequestBody.create(
                MediaType.parse(type),
                file
        )
        return ProgressableRequestBody(requestFile,progressableListener)

    }

    companion object {
        private const val DEFAULT_FORM_NAME = "file[]"
    }
}