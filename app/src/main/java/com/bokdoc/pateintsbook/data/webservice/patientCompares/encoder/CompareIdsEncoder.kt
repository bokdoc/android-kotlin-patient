package com.bokdoc.pateintsbook.data.webservice.patientCompares.encoder

import com.bokdoc.pateintsbook.data.webservice.models.CompareRequest
import com.bokdoc.pateintsbook.data.webservice.models.LoginCredentials
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object CompareIdsEncoder {

    fun getJson(list: List<String>): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<CompareRequest>()
        val compareRequest = CompareRequest()
        compareRequest.compare_ids = list
        document.set(compareRequest)

        return moshi.adapter(Document::class.java).toJson(document)
    }

    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(LoginCredentials::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi

    }

}