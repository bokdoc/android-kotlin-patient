package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.doctors

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.DoctorsConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.doctors.DoctorsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DoctorRow


class DoctorsRepository(val appContext: Context,
                        val doctorsWebservice: DoctorsWebservice,
                        val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl) {

    fun getAllDoctors(id: String, service: HashMap<String, String>, isDepartment: Boolean): LiveData<DataResource<DoctorRow>> {
        val liveData = MutableLiveData<DataResource<DoctorRow>>()
        val dataResource = DataResource<DoctorRow>()

        doctorsWebservice.getDoctors(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<HospitalDoctor>({
                    dataResource.status = it.status
                    if (it.data!!.isNotEmpty()) {
                        dataResource.data = DoctorsConverter.convert(it.data, isDepartment)
                        dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, HospitalDoctor::class.java, ProfileType::class.java, ProfileTitle::class.java, Departments::class.java, LanguageSpoken::class.java)))

        return liveData
    }
}