package com.bokdoc.pateintsbook.data.webservice.bookAppointements.encoder

import com.bokdoc.pateintsbook.data.models.promo.PromoCode
import com.bokdoc.pateintsbook.data.webservice.models.PromoCodeResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object AddPromoEncoder {

    fun getJson(bookInfo: PromoCode): String {

        val moshi = getMoshiParser()
        val document = ObjectDocument<PromoCodeResource>()
        val promoCodeResource = PromoCodeResource().apply {
            id = ""
            servicePivotId = bookInfo.servicePivotId
            servicePivotType = bookInfo.servicePivotType
            patientId = bookInfo.patientId
            coupon = bookInfo.coupon
            bookSecondaryDuration = bookInfo.bookSecondaryDuration
        }
        document.set(promoCodeResource)
        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(PromoCodeResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}