package com.bokdoc.pateintsbook.data.webservice.contactUs.encoder

 import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object ContactUsEncoder {
    fun getJson(id: String, message: String,name:String,email:String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<ContactUsResource>()
        val contactus = ContactUsResource()
        contactus.id = id
        contactus.message = message
        contactus.name = name
        contactus.email = email
        document.set(contactus)

        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(ContactUsResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }

}