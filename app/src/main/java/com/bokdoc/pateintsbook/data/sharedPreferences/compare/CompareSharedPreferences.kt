package com.bokdoc.pateintsbook.data.sharedPreferences.compare

import android.content.Context
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl

class CompareSharedPreferences(context: Context) : SharedPreferencesManagerImpl(context) {
    private var comparesIDs = HashSet<String>()

    init {
        loadCompares()
    }

    fun saveCompares() {
        sharedPreferences.edit().putStringSet(COMPARES_IDS, comparesIDs).apply()
    }

    fun getComparesList(): List<String> {
        return comparesIDs.toList()
    }

    private fun loadCompares() {
        comparesIDs = sharedPreferences.getStringSet(COMPARES_IDS, HashSet<String>()) as HashSet<String>
    }

    fun addCompare(id: String) {
        comparesIDs.add(id)
        saveCompares()
    }

    fun removeCompare(id: String) {
        if (comparesIDs.isNotEmpty())
            comparesIDs.remove(id)
    }

    fun removeCompares() {
        sharedPreferences.edit().remove(COMPARES_IDS)
    }

    companion object {
        private const val COMPARES_IDS = "comparesIds"
    }
}