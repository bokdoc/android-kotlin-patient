package com.bokdoc.pateintsbook.data.webservice

import okhttp3.Interceptor
import okhttp3.Response

class AcceptResponseInterceptor(private val acceptType:String):Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain?.request()
        val builder = original?.newBuilder()?.header("Accept",acceptType)
        return chain!!.proceed(builder?.build())
    }
}