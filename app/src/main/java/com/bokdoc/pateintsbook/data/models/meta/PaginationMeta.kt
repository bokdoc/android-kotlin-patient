package com.bokdoc.pateintsbook.data.models.meta

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaginationMeta {

    @SerializedName("pagination")
    @Expose
    lateinit var pagination: Pagination
}