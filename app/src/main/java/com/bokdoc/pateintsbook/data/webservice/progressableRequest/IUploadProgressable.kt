package com.bokdoc.pateintsbook.data.webservice.progressableRequest

interface IUploadProgressable {
    var totalLength: Long
    fun onProgress(byteCounts: Long)
}