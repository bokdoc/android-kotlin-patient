package com.bokdoc.pateintsbook.data.common.parser

import com.bokdoc.pateintsbook.data.models.CustomError
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import org.json.JSONObject


object ErrorsParser {
    fun parseError(errorJson:String):List<CustomError>{

        try {

            if (errorJson.isEmpty())
                return ArrayList()

            if (errorJson.contains("<!DOCTYPE html>"))
                return ArrayList()

            if (JSONObject(errorJson).has("errors") && JSONObject(errorJson).optJSONArray("errors") != null) {
                val errorsArray = JSONObject(errorJson).getJSONArray("errors")
                val collectionType = object : TypeToken<List<CustomError>>() {}.type
                return Gson().fromJson<List<CustomError>>(errorsArray.toString(), collectionType)
            }

            if (JSONObject(errorJson).has("errors") && JSONObject(errorJson).optJSONObject("errors") != null) {
                val errorsJsonObject = JSONObject(errorJson).getJSONObject("errors")
                val error = Gson().fromJson<CustomError>(errorsJsonObject.toString(),
                        CustomError::class.java)
                return arrayListOf(error)
            }

        }catch (e:Throwable){
            return  ArrayList()
        }

       // if(JSONObject(errorJson))

        return  ArrayList()
    }
}