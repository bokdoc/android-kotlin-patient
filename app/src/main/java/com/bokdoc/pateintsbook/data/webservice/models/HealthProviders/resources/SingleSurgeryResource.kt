package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources

import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.models.ProfileType
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class SingleSurgeryResource : Resource() {

    var name: String = ""
    var overview: String = ""
    var main_speciality: String = ""
    var picture: Picture = Picture()
    var location: Location = Location()
    var singleSurgery: HasOne<SurgeryDetailsResource>? = null
    var profileType: HasOne<ProfileType>? = null
}

@JsonApi(type = "profileSurgery")
class SurgeryDetailsResource : Resource() {
    //attributes
    lateinit var name: String
    lateinit var description: String
    @Json(name = "duration_of_program")
    lateinit var durationOfProgram: String
    @Json(name = "duration_of_surgery")
    lateinit var durationOfSurgery: String
    @Json(name = "program_includes_description")
    lateinit var programIncludesDescription: String
    @Json(name = "method_of_surgery")
    lateinit var methodOfSurgery: String
    @Json(name = "devices_used_in_surgery")
    var devicesUsedInSurgery: String = ""
    @Json(name = "doctor_name")
    lateinit var doctorName: String
    lateinit var fees: Fees
    var location: Location = Location()
    @Json(name = "show_url")
    lateinit var showUrl: String
    @Json(name = "main_fees")
    var mainFees = MainFees()
    @Json(name = "sub_main_fees")
    var subMainFees = MainFees()

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null

    @Json(name = "is_appointment_reservation_message")
    lateinit var is_appointment_reservation_message: String

    @Json(name = "auto_approve_request_message")
    lateinit var auto_approve_request_message: String

    @Json(name = "pre_payment_required_message")
    lateinit var pre_payment_required_message: String


    //relations

    var doctor: HasOne<SingleSurgeryResource>? = null
    var speciality: HasOne<Speciality>? = null
    var nextAvailabilities: HasMany<NextAvailabilityResource>? = null

    var anesthesiaTypes: HasMany<Anesthesia>? = null
    var preOrderRadiologyScans: HasMany<ScanService>? = null
    var laboratoryTestIncluded: HasMany<TestService>? = null
    var preOrderLabTests: HasMany<TestService>? = null
    var media: HasMany<SurgeryMedia>? = null

//    var programIncludes: HasMany<Speciality>? = null

}

@JsonApi(type = "anesthesia")
class Anesthesia : Resource() {
    var name: String = ""
    var description: String = ""
}

@JsonApi(type = "scanService")
class ScanService : Resource() {
    var name: String = ""
    var description: String = ""
}

@JsonApi(type = "testService")
class TestService : Resource() {
    var name: String = ""
    var description: String = ""
}

@JsonApi(type = "surgeryMedia")
class SurgeryMedia : Resource(),IMedia {
    override var mediaId: String = ""
        get() = id

    @Json(name = "media_url")
    override var url: String = ""
    override var name: String = ""
    @Json(name = "mime_type")
    override var mediaType: String = ""

}

