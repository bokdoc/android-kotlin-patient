package com.bokdoc.pateintsbook.data.webservice

import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.ErrorsParser
import com.bokdoc.pateintsbook.data.common.parser.MetaParser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import moe.banana.jsonapi2.Resource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class UserCustomResponse(val onResponse: (DataResource<UserResponse>) -> Unit,
                         val resources: Array<Class<out Resource>> ,
                         val userSharedPreference: WeakReference<UserSharedPreference>,
                         val context: WeakReference<Context>) : Callback<ResponseBody> {
    val dataResource = DataResource<UserResponse>()

    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
        dataResource.data = arrayListOf()
        dataResource.status = DataResource.FAIL
        onResponse.invoke(dataResource)
    }

    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {

        dataResource.status = response?.code().toString()

        if (response?.body() != null) {
            val json = response.body()!!.string()
            try {
                val userResponse = UserParser.parse(json)
                if (userResponse.id.isNullOrEmpty()) {
                    dataResource.status = DataResource.INTERNAL_SERVER_ERROR
                    dataResource.meta = MetaParser.paresMeta(json)
                    dataResource.message = dataResource.meta.message
                    onResponse.invoke(dataResource)
                    return
                }
                if (userResponse.currency.get(userResponse.document) != null)

                    userResponse.currency.get(userResponse.document).let {
                        userResponse.currencyLookup = LookUpsParcelable().apply {
                            idLookup = it.idLookup
                            name = it.name
                        }
                    }

                if (userResponse.language.get(userResponse.document) != null)
                    userResponse.language.get(userResponse.document).let {
                        userResponse.languageLookup = LookUpsParcelable().apply {
                            idLookup = it.idLookup
                            name = it.name
                        }
                    }

                if (userResponse.country.get(userResponse.document) != null)

                    userResponse.country.get(userResponse.document).let {
                        userResponse.countryLookup = LookUpsParcelable().apply {
                            idLookup = it.idLookup
                            name = it.name
                        }
                    }


                userSharedPreference.get()!!.sharedPrefrenceManger.saveData(context.get()!!.getString(R.string.language_key), userResponse.languageString)
                userSharedPreference.get()!!.sharedPrefrenceManger.saveData(context.get()!!.getString(R.string.token), userResponse.token)
                userSharedPreference.get()?.saveUser(UserEncoder.getJson(userResponse))
                dataResource.data = arrayListOf(userResponse)
                dataResource.status = DataResource.SUCCESS
            }catch (e:Exception){

            }
            dataResource.meta = MetaParser.paresMeta(json)
            dataResource.message = dataResource.meta.message

        } else {
            if (dataResource.status == DataResource.INTERNAL_SERVER_ERROR) {
                //dataResource.status = DataResource.INTERNAL_SERVER_ERROR
            } else {
                if (response?.errorBody() != null ) {
                    dataResource.errors = ErrorsParser.parseError(response.errorBody()!!.string()!!)
                    if(dataResource.errors.isNotEmpty())
                        dataResource.message = dataResource.errors[0].detail
                }
            }

        }

        if(dataResource.status.toInt()<300){
            dataResource.status = DataResource.SUCCESS
        }

        if(dataResource.status == DataResource.INTERNAL_SERVER_ERROR)
            dataResource.data = ArrayList()

        onResponse.invoke(dataResource)
    }


}