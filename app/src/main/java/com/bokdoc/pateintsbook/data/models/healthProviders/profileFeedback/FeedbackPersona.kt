package com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback

interface FeedbackPersona {
    var picture:String
    var name:String
}