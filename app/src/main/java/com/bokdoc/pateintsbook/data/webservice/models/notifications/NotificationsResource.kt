package com.bokdoc.pateintsbook.data.webservice.models.notifications

import com.bokdoc.pateintsbook.data.models.DateTime
import com.bokdoc.pateintsbook.data.models.notifications.INotifications
import com.bokdoc.pateintsbook.data.models.params.Params
import com.bokdoc.pateintsbook.data.models.profile.ProfileNotifications
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "notification")
class NotificationsResource : Resource(), INotifications {

    @Json(name = "action_url")
    override var actionUrl: List<DynamicLinks>? = null

    override var notificationsId: String? = null
        get() = id

    override var title: String? = null

    override var content: String? = null

    @Json(name = "date")
    override var dateTime: DateTime? = null

    override var params: Params? = null


    @Json(name = "delete_url")
    override var deleteUrl: String? = null

    @Transient
    override var profile: ProfileNotifications? = null

    @Json(name = "source")
    var profileSource: HasOne<Profile>? = null

}