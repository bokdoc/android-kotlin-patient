package com.bokdoc.pateintsbook.data


class CurrencyLanguageRow:AddEditRow() {

    companion object {
        const val CURRENCY_SLUG = "currency"
        const val LANGUAGE_SLUG = "language"
        const val COUNTRY_SLUG = "country"
    }
}