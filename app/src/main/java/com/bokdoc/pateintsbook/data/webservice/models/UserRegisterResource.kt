package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "registeredInfo")
class UserRegisterResource:Resource() {
    var email:String?=null
    lateinit var password:String
    lateinit var name:String
    @Json(name = "mobile_number")
    lateinit var phone:String
}