package com.bokdoc.pateintsbook.data.repositories.contactUs

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.contactUs.ContactUsWebService
import com.bokdoc.pateintsbook.data.webservice.contactUs.encoder.ContactUsEncoder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsRepository(val contactUsWebService: ContactUsWebService,
                          val userSharedPreferences: UserSharedPreference) {

    fun contactUs(message: String,name: String,email: String): LiveData<DataResource<Boolean>> {
        val json = ContactUsEncoder.getJson(userSharedPreferences.getId(), message,name,email)
        val liveData = MutableLiveData<DataResource<Boolean>>()
        contactUsWebService.sendContactUs(userSharedPreferences.getToken(), json).enqueue(ContactUsResponse(liveData))
        return liveData
    }

}

class ContactUsResponse(val liveData: MutableLiveData<DataResource<Boolean>>) : Callback<ResponseBody> {
    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
        val dataResource = DataResource<Boolean>()
        dataResource.status = DataResource.FAIL
        liveData.value = dataResource
    }

    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
        val dataResource = DataResource<Boolean>()
        if (response?.code() == 200) {
            dataResource.status = DataResource.SUCCESS
        } else {
            dataResource.status = DataResource.FAIL
        }
        liveData.value = dataResource
    }

}
