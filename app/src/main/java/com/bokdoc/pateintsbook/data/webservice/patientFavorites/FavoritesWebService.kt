package com.bokdoc.pateintsbook.data.webservice.patientFavorites

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Query

class FavoritesWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getFavorites(id: String, token: String,page:String="1"): Call<ResponseBody> {
        val favouritesApi = webserviceManagerImpl.createService(FavoritesApi::class.java, token)
        return favouritesApi.getFavorites(id,page)
    }
}