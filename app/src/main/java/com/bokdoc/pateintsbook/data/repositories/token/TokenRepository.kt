package com.bokdoc.pateintsbook.data.repositories.token

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.webservice.token.TokenWebserviceImpl
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.login.parser.UserParser
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import java.util.*


class TokenRepository(val context: Context) {
    var tokenWebserviceImpl: TokenWebserviceImpl = TokenWebserviceImpl()
    var sharedPreferencesManager = UserSharedPreference(context)

    fun getToken(deviceId: String): MutableLiveData<DataResource<UserResponse>> {
        val liveData = MutableLiveData<DataResource<UserResponse>>()
        val dataResource = DataResource<UserResponse>()
        if ( sharedPreferencesManager.sharedPrefrenceManger.getData(context.getString(R.string.token),"").isEmpty()) {
            tokenWebserviceImpl.webserviceManagerImpl.
                    formatBaseUrl(sharedPreferencesManager.getCountry(), sharedPreferencesManager.sharedPrefrenceManger
                            .getData(context.getString(R.string.language_key),
                            Locale.getDefault().language), sharedPreferencesManager.getCurrency())
            tokenWebserviceImpl.generateToken(deviceId).enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    dataResource.data = arrayListOf()
                    liveData.value = dataResource
                    dataResource.status = DataResource.FAIL
                }

                override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {

                    if (response?.code() == 200) {

                        val json = response.body()!!.string()
                        val userResponse = UserParser.parse(json)
                        if (userResponse.currency.get(userResponse.document) != null)

                            userResponse.currency.get(userResponse.document).let {
                                userResponse.currencyLookup = LookUpsParcelable().apply {
                                    idLookup = it.idLookup
                                    name = it.name
                                }
                            }

                        if (userResponse.language.get(userResponse.document) != null)
                            userResponse.language.get(userResponse.document).let {
                                userResponse.languageLookup = LookUpsParcelable().apply {
                                    idLookup = it.idLookup
                                    name = it.name
                                }
                            }

                        if (userResponse.country.get(userResponse.document) != null)

                            userResponse.country.get(userResponse.document).let {
                                userResponse.countryLookup = LookUpsParcelable().apply {
                                    idLookup = it.idLookup
                                    name = it.name
                                }
                            }

                        tokenWebserviceImpl.webserviceManagerImpl.formatBaseUrl(userResponse.countryString,
                                userResponse.languageString,userResponse.currencyString)

                        sharedPreferencesManager.sharedPrefrenceManger.saveData(context.getString(R.string.language_key), userResponse.languageString)
                        sharedPreferencesManager.sharedPrefrenceManger.saveData(context.getString(R.string.token), userResponse.token)
                        sharedPreferencesManager.saveUser(UserEncoder.getJson(userResponse))
                        dataResource.data = arrayListOf(userResponse)
                        dataResource.status = DataResource.SUCCESS
                        liveData.value = dataResource

                    } else {
                        dataResource.status = DataResource.FAIL

                    }
                }

            })
        }

        else {
            tokenWebserviceImpl.webserviceManagerImpl.formatBaseUrl(sharedPreferencesManager.getUser()?.countryString!!,
                    sharedPreferencesManager.sharedPrefrenceManger.getData(context.getString(R.string.language_key),
                            Locale.getDefault().language), sharedPreferencesManager.getUser()?.currencyString!!)
            dataResource.data = arrayListOf()
            dataResource.status = DataResource.SUCCESS

            liveData.value = dataResource
        }

        return liveData
    }
}