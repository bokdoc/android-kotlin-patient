package com.bokdoc.pateintsbook.data.webservice.patientFavorites

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.Call
import retrofit2.http.Query


interface FavoritesApi {
    @GET(WebserviceConstants.PATIENTS + "/{id}/favorites")
    fun getFavorites(@Path("id") id: String,@Query("page")page:String): Call<ResponseBody>
}