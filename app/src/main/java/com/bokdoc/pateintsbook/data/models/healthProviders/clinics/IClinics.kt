package com.bokdoc.pateintsbook.data.models.healthProviders.clinics

import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.INextAvailabilities

interface IClinics {
    var name:String
    var picture:String
    var location:String
    var overview:String
    var appointmentsFreeCount:Int
    var fees: Fees
    var media: List<MediaParcelable>
    var nextAvailability:String
    var nextAvailabilities:List<INextAvailabilities>
}