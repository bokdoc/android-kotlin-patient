package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.Picture
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "patient")
class UpdateProfileResource : Resource() {

    var name: String = ""
    var email: String? = null
    var mobile_number: String = ""
    var birthdate: String? = null
    var gender: String? = null
    var picture = Picture()
}