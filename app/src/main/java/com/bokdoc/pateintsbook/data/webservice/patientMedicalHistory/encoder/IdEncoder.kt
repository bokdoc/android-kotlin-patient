package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.encoder

import com.bokdoc.pateintsbook.data.webservice.models.LoginSocials
import com.bokdoc.pateintsbook.data.webservice.models.medicalHistory.IdResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object IdEncoder {
    fun encode(id: String): String {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(IdResource::class.java)
                .build()


        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()


        val document = ObjectDocument<IdResource>()
        document.set(IdResource().apply {
            this.idAtt = id
        })
        return moshi.adapter(Document::class.java).toJson(document)
    }

}