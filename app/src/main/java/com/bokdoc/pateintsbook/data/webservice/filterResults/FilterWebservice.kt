package com.bokdoc.pateintsbook.data.webservice.filterResults

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.login.LoginApi
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class FilterWebservice {
    private val webserviceManagerImpl = WebserviceManagerImpl.instance()

    fun getFilterOptions(screenType: String, token: String): Call<ResponseBody> {
        val filterApi = webserviceManagerImpl.createService(FilterApi::class.java, token)
        return filterApi.getFilterOptions(screenType)
    }


    fun getFilterQuery(filtersSelection: Map<String, ArrayList<String>>): String {
        var query = ""
        filtersSelection.forEach {filter->
            getValues(filter.value).let {
                if(it.isNotEmpty()){
                    query += "${filter.key}=${it}&"
                }
            }
        }

        if (query.isNotEmpty())
            query += "is_filter=true&"

        if(query.isEmpty())
            return StringsConstants.EMPTY

        return query.substring(0, query.lastIndexOf("&"))

    }

    private fun getValues(values: ArrayList<String>): String {
        var string = ""
        values.forEach {
            string += "$it,"
        }

        if(string.isEmpty())
            return StringsConstants.EMPTY

        return string.substring(0, string.lastIndexOf(","))
    }

}