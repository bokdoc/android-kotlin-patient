package com.bokdoc.pateintsbook.data.webservice.bookAppointements

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface BookAppointementsApis {

    @POST(WebserviceConstants.PATIENTS_URL + "request")
    @Headers("Accept:application/json")
    fun confirm(@Body bookJson: RequestBody): Call<ResponseBody>

    @POST(WebserviceConstants.ADD_PROMO_CODE)
    fun addPromoCode(@Body promoCode: RequestBody): Call<ResponseBody>
}