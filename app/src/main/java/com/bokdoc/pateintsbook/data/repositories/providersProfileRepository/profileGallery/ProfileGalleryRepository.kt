package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileGallery

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.MediaConverter
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.webservice.models.HealthProfileProvidersResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProvidersProfileType
import com.bokdoc.pateintsbook.data.webservice.models.MediaResource
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileGallery.ProfileGalleryWebservice
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileGalleryRepository(val appContext:Context,
                               val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                               val profileGalleryWebservice: ProfileGalleryWebservice) {

    fun getGallery(id:String):LiveData<DataResource<MediaParcelable>>{
        val liveData = MutableLiveData<DataResource<MediaParcelable>>()
        val dataResource = DataResource<MediaParcelable>()

        profileGalleryWebservice.getGallery(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token),
                ""),id).enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if(response?.body()!=null){
                    val profile = Parser.parseJson(response.body()?.string()!!, arrayOf(HealthProfileProvidersResource::class.java,
                            HealthProvidersProfileType::class.java,MediaResource::class.java))[0] as HealthProfileProvidersResource
                    dataResource.data = MediaConverter.convert( profile.media.get(profile.document))
                    liveData.value = dataResource
                }else{
                    dataResource.message = response?.errorBody()!!
                    liveData.value = dataResource
                }
            }

        })

        return liveData
    }
}