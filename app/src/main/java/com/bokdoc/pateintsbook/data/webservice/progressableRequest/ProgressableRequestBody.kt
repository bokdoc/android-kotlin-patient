package com.bokdoc.pateintsbook.data.webservice.progressableRequest

import okhttp3.MediaType
import okhttp3.RequestBody
import okio.*
import java.io.IOException

class ProgressableRequestBody(private val requestBody: RequestBody,
                              private val listener:IUploadProgressable):RequestBody() {

    init {
        listener.totalLength+=contentLength()
    }

    override fun contentType(): MediaType? {
        return requestBody.contentType()
    }

    override fun contentLength(): Long {
        try {
            return requestBody.contentLength()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return -1
    }


    override fun writeTo(sink: BufferedSink?) {
        val countingSink = CountingSink(sink!!)
        val bufferedSink = Okio.buffer(countingSink)
        requestBody.writeTo(bufferedSink)
        bufferedSink.flush()
    }

    inner class CountingSink(sink: Sink): ForwardingSink(sink){

        private var bytesWritten = 0L

        override fun write(source: Buffer?, byteCount: Long) {
            super.write(source,byteCount)
            bytesWritten+=byteCount
            //val progress = (1.0 * bytesWritten) / contentLength()
            listener.onProgress(bytesWritten)
        }
    }

}