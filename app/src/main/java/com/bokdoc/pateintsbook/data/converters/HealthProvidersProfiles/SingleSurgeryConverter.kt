package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.converters.MediaConverter
import com.bokdoc.pateintsbook.data.converters.NextAvailabilitiesParcelableConverter
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.SingleSurgeryResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SingleSurgery

object SingleSurgeryConverter {

    fun convert(singleSurgeryResource: SingleSurgeryResource): SingleSurgery {
        val surgery = SingleSurgery()
        val surgeryData = singleSurgeryResource.singleSurgery!!.get(singleSurgeryResource.document)

        surgery.name = surgeryData.name
        surgery.nationalFees = surgeryData.mainFees
        surgery.interNationalFees = surgeryData.subMainFees
        surgery.location = surgeryData.location
        surgery.devices = surgeryData.devicesUsedInSurgery
        surgery.bookButton = surgeryData.bookButton
        surgery.duration = surgeryData.durationOfSurgery
        surgery.programDuration = surgeryData.durationOfProgram
        surgery.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(surgeryData.nextAvailabilities!!.get(surgeryData.document))
        surgery.paymentType = surgeryData.pre_payment_required_message
        surgery.approvalType = surgeryData.auto_approve_request_message
        surgery.watingQueu = surgeryData.is_appointment_reservation_message
        surgeryData.media?.let {
            surgery.media = MediaConverter.convert(it.get(surgeryData.document))
        }

        if (surgeryData.doctor != null) {
            surgery.doctorName = surgeryData.doctor!!.get(surgeryData.document).name
            surgery.doctorImage = surgeryData.doctor!!.get(surgeryData.document).picture.url
            surgery.speciality = surgeryData.speciality!!.get(surgeryData.document).name

        } else {
            surgery.doctorName = singleSurgeryResource.name
            surgery.doctorImage = singleSurgeryResource.picture.url
            surgery.speciality = singleSurgeryResource.main_speciality
        }

        if (surgeryData.anesthesiaTypes != null) {
            surgeryData.anesthesiaTypes!!.get(surgeryData.document).forEach {
                surgery.anesthesiaTypes.add(it.name)
            }
        }

        if (surgeryData.preOrderRadiologyScans != null) {
            surgeryData.preOrderRadiologyScans!!.get(surgeryData.document).forEach {
                surgery.preOrderRadiologyScans.add(it.name)
            }
        }

        if (surgeryData.laboratoryTestIncluded != null) {
            surgeryData.laboratoryTestIncluded!!.get(surgeryData.document).forEach {
                surgery.laboratoryTestIncluded.add(it.name)
            }
        }

        if (surgeryData.preOrderLabTests != null) {
            surgeryData.preOrderLabTests!!.get(surgeryData.document).forEach {
                surgery.preOrderLabTests.add(it.name)
            }
        }
        return surgery
    }
}