package com.bokdoc.pateintsbook.data.webservice.models.autoComplete

import com.squareup.moshi.Json

open class DoctorAndRadResource:ResourceWithNameAttr() {
    var picture:String = ""
    var location:String = ""
    @Json(name = "profile_id")
    var profileId = ""
}