package com.bokdoc.pateintsbook.data.models.healthProviders.fees

import android.os.Parcel
import android.os.Parcelable

class FeesParacable() : IFeesParacable{
    override var amount: Double = 0.0
    override var symbols: String = ""

    constructor(parcel: Parcel) : this() {
        amount = parcel.readDouble()
        symbols = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(amount)
        parcel.writeString(symbols)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeesParacable> {
        override fun createFromParcel(parcel: Parcel): FeesParacable {
            return FeesParacable(parcel)
        }

        override fun newArray(size: Int): Array<FeesParacable?> {
            return arrayOfNulls(size)
        }
    }
}