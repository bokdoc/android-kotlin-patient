package com.bokdoc.pateintsbook.data.repositories.requests

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.MetaParser
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.meta.ServiceType
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.login.encoder.LoginCredentialsEncoder
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.patientRequests.RequestsWebService
import com.bokdoc.pateintsbook.data.webservice.patientRequests.encoder.RequestCancellationResourceEncoder
import com.bokdoc.pateintsbook.utils.Utils
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestsRepository(val context: Context, val requestsWebService: RequestsWebService, val sharedPreferencesManagerImpl: UserSharedPreference) {

    val dataMeta: Int = 0

    fun getRequests(serviceType: String,page:Int): LiveData<DataResource<Request>> {
        val liveData = MutableLiveData<DataResource<Request>>()
        val callable = requestsWebService.getRequests(
                 sharedPreferencesManagerImpl.getUser()!!.id,
                sharedPreferencesManagerImpl.getToken(),serviceType,page)
        callable.enqueue(CustomResponse<Request>({
            liveData.value = it
        },arrayOf(Request::class.java, Profile::class.java,ProfileTitle::class.java,ProfileType::class.java)))
        return liveData
    }

    fun cancelRequest(url:String,reason:String):MutableLiveData<DataResource<String>>{
        val liveData = MutableLiveData<DataResource<String>>()
        Log.i("cancelRequestUrl",url)
        val body = RequestCancellationResourceEncoder.getJson(reason)
        Log.i("requestCancelBody",body)
        val callable = requestsWebService.cancelRequest(url,
                sharedPreferencesManagerImpl.getToken(),body)
        callable.enqueue(CustomResponse<String>({
            liveData.value = it
        },arrayOf()))

        return liveData
    }

    class RequestsResponse(val liveData: MutableLiveData<DataResource<Request>>) : Callback<ResponseBody> {
        val dataResource = DataResource<Request>()
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            dataResource.message = t?.message!!
            liveData.value = dataResource
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if (response?.body() != null) {
                val json = response.body()!!.string()
                val request = Parser.parseJson(json, arrayOf(Request::class.java, DoctorProfile::class.java, ProfileTitleData::class.java, ProfileData::class.java))
                dataResource.data = request as List<Request>
                dataResource.meta = MetaParser.paresMeta(json)
                liveData.value = dataResource
            }
        }

    }


}