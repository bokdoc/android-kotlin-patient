package com.bokdoc.pateintsbook.data.repositories.attachmentMedia

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.net.Uri
import com.bokdoc.pateintsbook.data.converters.MediaConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.attachmentMedia.AttachmentMediaWebservice
import com.bokdoc.pateintsbook.data.webservice.models.AttachmentMediaResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.Departments
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.utils.files.FilesUtils
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.File

class AttachmentsMediaRepository(private val appContext: Context,
                                 private val userSharedPreference: UserSharedPreference,
                                 private val attachmentMediaWebservice: AttachmentMediaWebservice) {

    val supportedMedia = arrayOf(".jpeg", ".png", ".jpg", ".gif", ".svg")
    val supportedDocuments = arrayOf(".pdf")

    private lateinit var attachmentCallResponse: Call<ResponseBody>

    fun uploadFiles(filesUris: List<Uri>, onProgressListener: (progress: Double) -> Unit): LiveData<DataResource<IMediaParcelable>> {
        val files = Array(filesUris.size) { File("") }
        val types = Array(filesUris.size) { "" }
        getFilesWithTypes(filesUris, files, types)

        val liveData = MutableLiveData<DataResource<IMediaParcelable>>()
        val dataResource = DataResource<IMediaParcelable>()

        attachmentCallResponse = attachmentMediaWebservice.uploadFiles(userSharedPreference.getToken(),
                files, types, onProgressListener)


        attachmentCallResponse.enqueue(CustomResponse<AttachmentMediaResource>({
            if (it.status == DataResource.SUCCESS) {
                dataResource.data = MediaConverter.convert(it.data as List<IMedia>)
            }
            liveData.value = dataResource
        }, arrayOf(AttachmentMediaResource::class.java)))

        return liveData
    }

    fun getUserMedia(): LiveData<DataResource<IMediaParcelable>> {
        val liveData = MutableLiveData<DataResource<IMediaParcelable>>()
        val dataResource = DataResource<IMediaParcelable>()
        attachmentMediaWebservice.getMedia(userSharedPreference.getId(), userSharedPreference.getToken())
                .enqueue(CustomResponse<AttachmentMediaResource>({
                    if (it.status == DataResource.SUCCESS && it.data != null) {
                        dataResource.data = MediaConverter.convert(it.data as List<IMedia>)
                    }
                    dataResource.meta = it.meta
                    dataResource.status = it.status
                    dataResource.errors = it.errors
                    dataResource.message = it.message
                    liveData.value = dataResource

                }, arrayOf(AttachmentMediaResource::class.java)))

        return liveData
    }


    fun cancelUpload() {
        attachmentCallResponse.isCanceled.let {
            if (!it) {
                attachmentCallResponse.cancel()
            }
        }
    }

    private fun getFilesWithTypes(uris: List<Uri>, files: Array<File>, types: Array<String>) {
        val size = files.size
        (0 until size).forEach {
            files[it] = FilesUtils.getFile(uris[it])
            types[it] = FilesUtils.getFileType(uris[it])
        }
    }

    fun isFileSizeSmallerThan(uri: Uri, limitedSize: Int): Boolean {
        return FilesUtils.isFilesSizeSmallerThan(arrayOf(uri), limitedSize)
    }

    fun isFilesSizeSmallerThan(uri: Array<Uri>, limitedSize: Int): Boolean {
        return FilesUtils.isFilesSizeSmallerThan(uri, limitedSize)
    }

    fun getPaths(uris: Array<Uri>): ArrayList<String> {
        val urisArrayList = ArrayList<Uri>()
        uris.forEach {
            urisArrayList.add(it)
        }
        return if (uris.isNotEmpty()) {
            FilesUtils.getPaths(urisArrayList)
        } else {
            ArrayList()
        }
    }

    fun getUris(paths: ArrayList<String>): Array<Uri> {
        return if (paths.isNotEmpty()) {
            FilesUtils.getUris(paths)
        } else {
            arrayOf()
        }
    }

    fun isFilesSupport(paths: List<String>, types: Array<String>): Boolean {
        return FilesUtils.isFilesSupport(paths, types)
    }


    fun showMedia(url: String): LiveData<DataResource<IMediaParcelable>> {

        val liveData = MutableLiveData<DataResource<IMediaParcelable>>()
        val dataResource = DataResource<IMediaParcelable>()

        attachmentMediaWebservice.getMediaWithUrl(url, userSharedPreference.getToken())
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (it.status == DataResource.SUCCESS && it.data != null) {
                        dataResource.data = MediaConverter.convertResource(it.data!![0].singleDepartment.get(it.data!![0].document).media!!.get(it.data!![0].document))
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, Departments::class.java, AttachmentMediaResource::class.java)))

        return liveData
    }
}