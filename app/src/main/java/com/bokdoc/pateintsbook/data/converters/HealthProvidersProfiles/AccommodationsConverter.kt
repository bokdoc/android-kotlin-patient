package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.AccommodationsRow

object AccommodationsConverter {

    fun convert(allClinicsResource: HealthProviderProfile): AccommodationsRow {
        val accommodationsRow = AccommodationsRow()
        allClinicsResource.accommodations?.get(allClinicsResource.document)!![0].let {
            accommodationsRow.apply {
                patientRoom = it.patientRoom
                rooms = it.rooms
                accompaniedPersons = it.accompaniedPersons
                meals = it.meals
            }
        }
        return accommodationsRow
    }
}