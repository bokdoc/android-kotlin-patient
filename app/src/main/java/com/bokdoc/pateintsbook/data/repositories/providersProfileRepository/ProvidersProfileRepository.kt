package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository

import android.content.Context
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl

class ProvidersProfileRepository(val appContext: Context,
                                 val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl)