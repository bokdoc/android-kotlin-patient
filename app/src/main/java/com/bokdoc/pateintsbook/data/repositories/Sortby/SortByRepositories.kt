package com.bokdoc.pateintsbook.data.repositories.Sortby

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.TextWithParamConverter
import com.bokdoc.pateintsbook.data.models.TextWithParam
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.sortBy.SortWebservice
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SortByRepositories(val context: Context) {
    val preferenceManger = SharedPreferencesManagerImpl(context)
    val sortWebservice = SortWebservice()

    fun getOptions(screenType: String): LiveData<List<TextWithParam>> {
        val callable = sortWebservice.getOptions(preferenceManger.getData(context.getString(R.string.token), ""), screenType)
        val liveData = MutableLiveData<List<TextWithParam>>()
        callable.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

            }
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val sorts = Parser.parseJsonObject(response.body()?.string()!!, arrayOf(SortOptions::class.java, Options::class.java)) as SortOptions
                    liveData.value = TextWithParamConverter.convert(sorts.sorts.get(sorts.document))
                } else {

                }
            }

        })
        return liveData
    }
}