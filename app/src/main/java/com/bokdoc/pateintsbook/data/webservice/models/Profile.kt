package com.bokdoc.pateintsbook.data.webservice.models

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.ITimes
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class Profile() : Resource(), Parcelable {
    var name: String = ""
    var overview: String = ""
    var picture: Picture = Picture()
    var gender: String = ""
    @Json(name = "birth_date")
    var birthDate: String = ""
    var location: Location = Location()
    @Json(name = "licence_number")
    var licenceNumber: String = ""
    @Json(name = "experience_years")
    var experienceYears: Int = 0
    @Json(name = "appointment_free_count")
    var appointmentFreeCount: Int = 0
    @Json(name = "surgery_free_count")
    var surgeryFreeCount: String = "0"
    @Json(name = "is_bokdoc_grantee")
    var isBokdocGrantee: Boolean = false
    var rating: Float = 0.0f
    @Json(name = "reviews_count")
    var reviewsCount: Int = 0
    @Json(name = "likes_count")
    var likesCount: Int = 0
    var country: String = ""
    var region: String = ""
    var city: String = ""
    var fees: Fees = Fees()
    @Json(name = "departments_ids")
    var departmentsIds: List<Int> = ArrayList()

    @Json(name = "health_provider_messages")
    var healthProviderMessages: List<String> = ArrayList()

    @Json(name = "clinics_addresses")
    var clinicAddresses: String? = null

    var is_message: Boolean = false
    var message: String = ""


    @Json(name = "service_ids")
    var serviceIds: List<Int> = ArrayList()
    var profileType: HasOne<ProfileType>? = null
    lateinit var languages: HasMany<Language>
    lateinit var languagesSpoken: HasMany<LanguageSpoken>
    lateinit var specialities: HasMany<Speciality>
    var paymentTypes = HasMany<PaymentType>()
    lateinit var services: HasMany<Service>
    lateinit var insuranceCarriers: HasMany<InsuranceCarrier>
    lateinit var departments: HasMany<Department>
    var doctors: List<Profile> = ArrayList()
    var clinics: List<Profile> = ArrayList()
    @Json(name = "next_availabilities")
    var nextAvailabilities: List<NextAvailabilities> = ArrayList<NextAvailabilities>()
    @Json(name = "home_visit_fees")
    var homeVisitFees: HomeVisitFees = HomeVisitFees()
    @Json(name = "online_consultation_fees")
    var onlineConsultationFees: OnlineConsultationFees = OnlineConsultationFees()
    @Json(name = "profile_type_id")
    var profileTypeId: Int = 0
    @Json(name = "views_count")
    var views_count: Int = 0
    @Json(name = "main_speciality")
    var mainSpeciality: String = ""
    var status: Boolean = false
    @Json(name = "total_booked_today")
    var totalBookedToday: Int = 0
    @Json(name = "appointment_left_today")
    var appointmentLeftToday: Int = 0
    @Json(name = "today_appointment_text")
    var todayAppointmentText: String = ""
    @Json(name = "next_availability")
    var nextAvailability: String? = " "
    @Json(name = "free_appointment")
    var freeAppointment: Int = 0
    @Json(name = "clinics_count")
    var clinicsCount: Int = 0
    @Json(name = "doctors_count")
    var doctorsCount: Int = 0
    @Json(name = "services_count")
    var servicesCount: Int = 0
    @Json(name = "departments_count")
    var departmentsCount: Int = 0
    @Json(name = "scans_count")
    var scansCount: Int = 0
    @Json(name = "tests_count")
    var testsCount: Int = 0
    @Json(name = "surgeries_count")
    var surgeriesCount: Int = 0
    @Json(name = "is_compared")
    var isCompared: Int = 0
    @Json(name = "is_favourite")
    var isFavourite: Int = 0
    @Json(name = "is_liked")
    var isLiked: Int = 0
    @Json(name = "is_shared")
    var isShared: Int = 0
    @Json(name = "profile_within")
    var profileWithin: String = ""
    @Json(name = "facebook_url")
    var facebookUrl: String = ""
    @Json(name = "twitter_url")
    var twitterUrl: String = ""
    @Json(name = "googleplus_url")
    var googlePlusUrl: String = ""
    @Json(name = "linkedin_url")
    var linkedinUrl: String = ""
    @Json(name = "instagram_url")
    var instagramUrl: String = ""
    @Json(name = "patient_room")
    var patientRoom: String = ""
    @Json(name = "beds_number")
    var bedsNumber: Int = 0
    @Json(name = "ambulance_count")
    var ambulanceCount: Int = 0
    @Json(name = "accompanying_persons")
    var accompanyingPersons: Int = 0
    @Json(name = "latitude")
    var lat: Double? = null
    @Json(name = "longitude")
    var lng: Double? = null
    var paymentMethods: HasMany<PaymentMethods> = HasMany()
    var profileTitle: HasOne<ProfileTitle> = HasOne()

    @Json(name = "speciality_title")
    var specialityTitle: String? = null

    var paymentOptions: HasMany<PaymentOptions> = HasMany()


    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null

    @Json(name = "book_buttons")
    var bookButtons: List<DynamicLinks>? = null

    @Json(name = "card_links")
    var cardLinks: List<DynamicLinks>? = null

    @Json(name = "card_buttons")
    var cardButtons: List<DynamicLinks>? = null

    @Json(name = "free_buttons")
    var freeButtons: List<DynamicLinks>? = null


    var profileTypeName = ""


    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        overview = parcel.readString()
        picture = parcel.readParcelable(Picture::class.java.classLoader)
        gender = parcel.readString()
        birthDate = parcel.readString()
        location = parcel.readParcelable(Location::class.java.classLoader)
        licenceNumber = parcel.readString()
        experienceYears = parcel.readInt()
        appointmentFreeCount = parcel.readInt()
        surgeryFreeCount = parcel.readString()
        isBokdocGrantee = parcel.readByte() != 0.toByte()
        rating = parcel.readFloat()
        reviewsCount = parcel.readInt()
        likesCount = parcel.readInt()
        country = parcel.readString()
        region = parcel.readString()
        city = parcel.readString()
        fees = parcel.readParcelable(Fees::class.java.classLoader)
        healthProviderMessages = parcel.createStringArrayList()
        doctors = parcel.createTypedArrayList(CREATOR)
        clinics = parcel.createTypedArrayList(CREATOR)
        // homeVisitFees = parcel.readParcelable(Fees::class.java.classLoader)
        //  onlineConsultationFees = parcel.readParcelable(OnlineConsultationFees::class.java.classLoader)
        profileTypeId = parcel.readInt()
        views_count = parcel.readInt()
        mainSpeciality = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        totalBookedToday = parcel.readInt()
        appointmentLeftToday = parcel.readInt()
        todayAppointmentText = parcel.readString()
        nextAvailability = parcel.readString()
        freeAppointment = parcel.readInt()
        clinicsCount = parcel.readInt()
        doctorsCount = parcel.readInt()
        servicesCount = parcel.readInt()
        departmentsCount = parcel.readInt()
        scansCount = parcel.readInt()
        testsCount = parcel.readInt()
        surgeriesCount = parcel.readInt()
        isCompared = parcel.readInt()
        isFavourite = parcel.readInt()
        isLiked = parcel.readInt()
        isShared = parcel.readInt()
        profileWithin = parcel.readString()
        facebookUrl = parcel.readString()
        twitterUrl = parcel.readString()
        googlePlusUrl = parcel.readString()
        linkedinUrl = parcel.readString()
        instagramUrl = parcel.readString()
        patientRoom = parcel.readString()
        bedsNumber = parcel.readInt()
        ambulanceCount = parcel.readInt()
        accompanyingPersons = parcel.readInt()
        lat = parcel.readValue(Double::class.java.classLoader) as? Double
        lng = parcel.readValue(Double::class.java.classLoader) as? Double
        bookButton = parcel.readParcelable(DynamicLinks::class.java.classLoader)
        bookButtons = parcel.createTypedArrayList(DynamicLinks)
        cardLinks = parcel.createTypedArrayList(DynamicLinks)
        cardButtons = parcel.createTypedArrayList(DynamicLinks)
        freeButtons = parcel.createTypedArrayList(DynamicLinks)
        profileTypeName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(overview)
        parcel.writeParcelable(picture, flags)
        parcel.writeString(gender)
        parcel.writeString(birthDate)
        parcel.writeParcelable(location, flags)
        parcel.writeString(licenceNumber)
        parcel.writeInt(experienceYears)
        parcel.writeInt(appointmentFreeCount)
        parcel.writeString(surgeryFreeCount)
        parcel.writeByte(if (isBokdocGrantee) 1 else 0)
        parcel.writeFloat(rating)
        parcel.writeInt(reviewsCount)
        parcel.writeInt(likesCount)
        parcel.writeString(country)
        parcel.writeString(region)
        parcel.writeString(city)
        parcel.writeParcelable(fees, flags)
        parcel.writeStringList(healthProviderMessages)
        parcel.writeTypedList(doctors)
        parcel.writeTypedList(clinics)
        // parcel.writeParcelable(homeVisitFees, flags)
        //   parcel.writeParcelable(onlineConsultationFees, flags)
        parcel.writeInt(profileTypeId)
        parcel.writeInt(views_count)
        parcel.writeString(mainSpeciality)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeInt(totalBookedToday)
        parcel.writeInt(appointmentLeftToday)
        parcel.writeString(todayAppointmentText)
        parcel.writeString(nextAvailability)
        parcel.writeInt(freeAppointment)
        parcel.writeInt(clinicsCount)
        parcel.writeInt(doctorsCount)
        parcel.writeInt(servicesCount)
        parcel.writeInt(departmentsCount)
        parcel.writeInt(scansCount)
        parcel.writeInt(testsCount)
        parcel.writeInt(surgeriesCount)
        parcel.writeInt(isCompared)
        parcel.writeInt(isFavourite)
        parcel.writeInt(isLiked)
        parcel.writeInt(isShared)
        parcel.writeString(profileWithin)
        parcel.writeString(facebookUrl)
        parcel.writeString(twitterUrl)
        parcel.writeString(googlePlusUrl)
        parcel.writeString(linkedinUrl)
        parcel.writeString(instagramUrl)
        parcel.writeString(patientRoom)
        parcel.writeInt(bedsNumber)
        parcel.writeInt(ambulanceCount)
        parcel.writeInt(accompanyingPersons)
        parcel.writeValue(lat)
        parcel.writeValue(lng)
        parcel.writeParcelable(bookButton, flags)
        parcel.writeTypedList(bookButtons)
        parcel.writeTypedList(cardLinks)
        parcel.writeTypedList(cardButtons)
        parcel.writeTypedList(freeButtons)
        parcel.writeString(profileTypeName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Profile> {
        override fun createFromParcel(parcel: Parcel): Profile {
            return Profile(parcel)
        }

        override fun newArray(size: Int): Array<Profile?> {
            return arrayOfNulls(size)
        }


        fun isHospitalCategory(profileType: Int): Boolean {
            return profileType == HOSPITAL || profileType == POL_CLINIC || profileType == CENTER
        }

        const val DOCTOR = 1
        const val HOSPITAL = 2
        const val POL_CLINIC = 3
        const val LAB = 4
        const val RAD = 5
        const val CENTER = 7
    }
}

open class Fees() : Parcelable {
    /*
    @Json(name = "fees_from")
    var feesFrom:Int = 0
    @Json(name = "fees_to")
    var feesTo:Int = 0
    */
    @Json(name = "amount")
    var feesAverage: String? = ""

    @Json(name = "before_discount")
    var oldAmount: String? = ""
    /*
    @Json(name="currency_id")
    var currencyId:Int = 0
    */
    @Json(name = "currency_symbols")
    var currencySymbols: String? = ""

    constructor(parcel: Parcel) : this() {
        feesAverage = parcel.readString()
        oldAmount = parcel.readString()
        currencySymbols = parcel.readString()
    }


    override fun toString(): String {
        return feesAverage.toString() + currencySymbols
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(feesAverage)
        parcel.writeString(oldAmount)
        parcel.writeString(currencySymbols)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Fees> {
        override fun createFromParcel(parcel: Parcel): Fees {
            return Fees(parcel)
        }

        override fun newArray(size: Int): Array<Fees?> {
            return arrayOfNulls(size)
        }
    }

}

class HomeVisitFees : Fees()
class OnlineConsultationFees() : Fees()

class NextAvailabilities {
    lateinit var date: String
    lateinit var times: List<Time>
}

class Time : ITimes {
    override var from: String = ""
    override val to: String = ""


}

@JsonApi(type = "profileType")
class ProfileType : Resource() {
    var name: String?=null
    var description: String?=null

}

@JsonApi(type = "language")
class Language : Resource(), LookupsType {
    override var idLookup: String = ""
        get() {
            return id
        }
    override var name: String = ""
}

@JsonApi(type = "languageSpoken")
class LanguageSpoken : Resource() {
    var name: String = ""
    var code: String = "l"
}

@JsonApi(type = "speciality")
class Speciality : Resource() {
    lateinit var name: String
    lateinit var summary: String
    lateinit var description: String
}

@JsonApi(type = "profileTitle")
class ProfileTitle : Resource() {
    var name: String = ""
    var description: String = ""
}

@JsonApi(type = "service")
class Service : Resource() {
    lateinit var name: String
    lateinit var summary: String
    lateinit var description: String
    lateinit var fees: Fees
    @Json(name = "type")
    lateinit var types: List<String>
    lateinit var picture: String
    @Json(name = "service_time")
    lateinit var serviceTime: String
}

@JsonApi(type = "insuranceCarrier")
class InsuranceCarrier : Resource() {
    lateinit var title: String
    lateinit var description: String
}

@JsonApi(type = "paymentType")
class PaymentType : Resource() {
    lateinit var name: String
    lateinit var description: String
}

@JsonApi(type = "department")
class Department : Resource() {
    lateinit var name: String
    lateinit var summary: String
    lateinit var description: String
}

@JsonApi(type = "paymentMethod")
class PaymentMethods : Resource() {
    lateinit var name: String
    lateinit var description: String
}

@JsonApi(type = "paymentOptions")
class PaymentOptions : Resource() {
    lateinit var name: String
    lateinit var description: String
}

class Location() : Parcelable {

    var address: String = ""
    var longitude: String = ""
    var latitude: String = ""
    var country: String = ""
    var city: String = ""
    var region: String = ""

    constructor(lat: String, lng: String, address: String, state: String, country: String, city: String) : this() {
        this.latitude = lat
        this.longitude = lng
        this.address = address
        this.region = state
        this.country = country
        this.city = city
    }

    constructor(parcel: Parcel) : this() {
        address = parcel.readString()
        longitude = parcel.readString()
        latitude = parcel.readString()
        country = parcel.readString()
        city = parcel.readString()
        region = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(longitude)
        parcel.writeString(latitude)
        parcel.writeString(country)
        parcel.writeString(city)
        parcel.writeString(region)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }

}
