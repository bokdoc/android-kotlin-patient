package com.bokdoc.pateintsbook.data.models.filters

class Filters(
    var name:String,
    var options:List<FilterChild>,
    var dataType:String){

    companion object {
        const val CHECKBOX_TYPE = "checkbox"
        const val RANGER_TYPE = "ranger"
        const val STAR_TYPE = "star"
        const val CALENDAR_TYPE = "calender"
        const val TIMES_RANGER_TYPE = "timeRanger"
        const val MAP_TYPE = "map"
        const val FREE_APPOINTMENT = "free_appointment"
        const val IS_HOME_VISIT = "is_home_visit"
    }
}