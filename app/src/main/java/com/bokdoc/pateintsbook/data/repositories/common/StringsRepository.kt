package com.bokdoc.pateintsbook.data.repositories.common

import android.content.Context
import com.bokdoc.pateintsbook.R

class StringsRepository {

    fun getStringRes(context: Context,resId:Int):String{
        return context.getString(resId)
    }




    object HealthProvidersProfileRes{
        const val PHOTOS_VIDEOS = R.string.photosVideos
        const val NEXT_AVAILABILITY = R.string.nextAvailability
        const val PAYMENT_METHODS = R.string.paymentMethods
        const val FEES = R.string.fees
    }


    object QualificationsRes{
        const val EDUCATION = R.string.education
        const val SUB_SPECIALITY = R.string.sub_speciality
        const val CERTIFICATES =  R.string.certificates
        const val MEMBERSHIPS = R.string.memberShips
        const val PUBLICATIONS = R.string.publications
        const val SPECIAL_WORDS = R.string.specialWords
        const val Accreditation = R.string.accreditation
     }

    object SurgeriesRes{
        const val SURGERY_NAME = R.string.surgeryName
        const val SURGERY_DESCRIPTION = R.string.surgeryDescription
        const val SURGERY_LOCATION = R.string.surgeryLocation
        const val PHOTOS_VIDEOS = R.string.photosVideos
        const val NEXT_AVAILABILITY = R.string.nextAvailability
        const val SURGERY_METHODS = R.string.surgeryMethods
        const val USED_TOOLS = R.string.usedTools
        const val FEES = R.string.fees
        const val PRE_ORDER_TESTS = R.string.preOrderTests
        const val POST_TEST = R.string.postTests
        const val PRE_ORDER_SCANS = R.string.preOrderScans
        const val POST_SCANS = R.string.postScans
        const val PAYMENT_METHODS = R.string.paymentMethods
        const val FREE_SURGERIES = R.string.freeSurgeries
    }

    object ClinicsRes{
        const val CLINICS_NAME = R.string.clinicName
        const val ABOUT_AS = R.string.aboutAs
        const val CLINIC_LOCATION = R.string.clinicLocation
        const val FREE_APPOINTMENTS = R.string.freeAppointments
    }

    companion object {
        const val PLACE_HOLDERS = R.string.placeHolder
        const val ONE_PLACE_HOLDER = R.string.onePlaceHolder
    }
}