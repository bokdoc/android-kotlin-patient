package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.departments

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DepartmentsApi {
    @GET(WebserviceConstants.PROFILES_WITH_SLASH+"{id}?include=departments")
    fun getDepartments(@Path("id")id:String): Call<ResponseBody>
}