package com.bokdoc.pateintsbook.data.webservice.models

import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "Patient")
class AddFcmResource :Resource(){
    var fcm_token: String = ""
    var device_os: String = "android"
}