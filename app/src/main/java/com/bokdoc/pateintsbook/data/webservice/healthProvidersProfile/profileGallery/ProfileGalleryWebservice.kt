package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileGallery

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class ProfileGalleryWebservice(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getGallery(token:String,id:String):Call<ResponseBody>{
        return webserviceManagerImpl.createService(ProfileGalleryApi::class.java,token).getGallery(id)
    }
}