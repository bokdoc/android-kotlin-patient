package com.bokdoc.pateintsbook.data.models.healthProviders.doctor

import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees

interface Doctor {
    var picture:String
    var name:String
    var location:String
    var rating:Int
    var fee: Fees?
}
