package com.bokdoc.pateintsbook.data.webservice.register.encoder

import com.bokdoc.pateintsbook.data.webservice.models.LoginSocials
import com.bokdoc.pateintsbook.data.webservice.models.UserRegisterResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object UserRegisterEncoder {
    fun getJson(email:String,password:String,phone:String,name:String):String{
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(UserRegisterResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()
        val user = UserRegisterResource()

        if(email.isNotEmpty())
        user.email = email

        user.phone = phone
        user.name = name
        user.password = password
        val document = ObjectDocument<UserRegisterResource>()
        document.set(user)
        return moshi.adapter(Document::class.java).toJson(document)
    }
}