package com.bokdoc.pateintsbook.data.repositories.favoritesRepository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfileConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.patientFavorites.FavoritesWebService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavoritesRepository(val context: Context
                          , val favoritesWebService: FavoritesWebService
                          , val userSharedPreference: UserSharedPreference) {


    fun getPatientFavorites(page:String = "1"): LiveData<DataResource<Profile>> {
        val liveData = MutableLiveData<DataResource<Profile>>()
        val callable = favoritesWebService.getFavorites(userSharedPreference.getId(),
                userSharedPreference.getToken())
        callable.enqueue(CustomResponse<Profile>({
            liveData.value = it
        }, arrayOf(Profile::class.java,ProfileType::class.java)))
        return liveData
    }

    fun getHealthProvidersProfile(profile: Profile, isLoadNextAvailabilities: Boolean): HealthProvidersProfile {
        return HealthProvidersProfileConverter.convert(profile, isLoadNextAvailabilities)
    }

}