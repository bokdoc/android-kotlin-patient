package com.bokdoc.pateintsbook.data.common.parser

import com.bokdoc.pateintsbook.data.webservice.models.UnknownResource
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.Resource
import moe.banana.jsonapi2.ResourceAdapterFactory

object Parser {

    fun parseJson(json: String, resources: Array<Class<out Resource>>): List<Resource> {

        val resourceAdapterFactory = ResourceAdapterFactory.builder()


        resources.forEach {
            resourceAdapterFactory.add(it)
                    .build()
        }

        resourceAdapterFactory.add(UnknownResource::class.java)

        val moshi = Moshi.Builder()
                .add(resourceAdapterFactory.build())
                .build()

        return moshi.adapter(Document::class.java).fromJson(json)!!.asArrayDocument()
    }

    fun parseJsonObject(json: String, resources: Array<Class<out Resource>>): Resource {

        val resourceAdapterFactory = ResourceAdapterFactory.builder()

        resources.forEach {
            resourceAdapterFactory.add(it)
                    .build()
        }

        resourceAdapterFactory.add(UnknownResource::class.java)

        val moshi = Moshi.Builder()
                .add(resourceAdapterFactory.build())
                .build()


        return moshi.adapter(Document::class.java).fromJson(json)?.asObjectDocument<Resource>()?.get()!!
    }


//    fun pares(userResponse: UserResponse):String{
//        val resourceAdapterFactory = ResourceAdapterFactory.builder()
//
//        val moshi = Moshi.Builder()
//                .add(resourceAdapterFactory.build())
//                .build()
//
//        return moshi.adapter(Document::class.java).toJson(userResponse).()
//
//    }
}