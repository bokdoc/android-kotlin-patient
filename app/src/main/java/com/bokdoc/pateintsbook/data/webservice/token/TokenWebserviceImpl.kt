package com.bokdoc.pateintsbook.data.webservice.token

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class TokenWebserviceImpl:TokenWebservice{

     var webserviceManagerImpl = WebserviceManagerImpl.instance()


    override fun generateToken(deviceId:String):Call<ResponseBody>{
        val apiToken = webserviceManagerImpl.createService(TokenApi::class.java)
        return apiToken.generateToken(deviceId)
    }

}