package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface MedicalHistoryActions {

    @POST(WebserviceConstants.PATIENTS_URL + TYPE_PATH)
    fun add(@Path(TYPE) type: String, @Body json: RequestBody): Call<ResponseBody>

    @GET(WebserviceConstants.PATIENTS_URL + ID_PATH + TYPE_PATH)
    fun get(@Path(TYPE) type: String, @Path(ID) id: String): Call<ResponseBody>

    @DELETE(WebserviceConstants.PATIENTS_URL + TYPE_PATH + "/{id}")
    fun remove(@Path(TYPE) type: String, @Path(ID) id: String): Call<ResponseBody>

    companion object {
        const val ID_PATH = "{id}/"
        const val ID = "id"
        const val TYPE = "type"
        const val TYPE_PATH = "{type}"
    }
}