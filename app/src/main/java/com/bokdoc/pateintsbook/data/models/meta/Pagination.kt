package com.bokdoc.pateintsbook.data.models.meta

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Pagination {

    @SerializedName("total")
    @Expose
    val total: Int? = null
    @SerializedName("count")
    @Expose
    val count: Int? = null
    @SerializedName("per_page")
    @Expose
    val perPage: Int? = null
    @SerializedName("current_page")
    @Expose
    var currentPage: Int? = null
    @SerializedName("total_pages")
    @Expose
    val totalPages: Int? = null
}