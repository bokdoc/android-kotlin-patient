package com.bokdoc.pateintsbook.data.webservice.sortBy

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class SortWebservice {
    var webserviceManagerImpl = WebserviceManagerImpl.instance()


    fun getOptions(token:String,screenType:String): Call<ResponseBody> {
        val searchApi = webserviceManagerImpl.createService(SortByApi::class.java,token)
        return searchApi.getSortByOptions("sorts",screenType)
    }
}