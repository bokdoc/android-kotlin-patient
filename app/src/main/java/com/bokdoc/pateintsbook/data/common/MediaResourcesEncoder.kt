package com.bokdoc.pateintsbook.data.common

import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.webservice.models.MediaResource
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.HasMany

object MediaResourcesEncoder {
    fun encode(document: Document, media:List<IMedia>): HasMany<MediaResource> {
        return HasMany<MediaResource>().apply {
            media.forEach {
                val mediaResource = MediaResource().apply {
                    id = it.mediaId
                    url = it.url
                }
                add(mediaResource)
                document.addInclude(mediaResource)
            }
        }
    }

    /*
    fun encodeWithDeletedMedia(document: Document, media:List<IMedia>,
                               outDeletedMedia: HasMany<DeletedMediaResource>, inType:String=""): HasMany<MediaResource> {
        val mediaResources = HasMany<MediaResource>()
        media.forEach {
            if (it.status) {
                if(it.mediaId.isNotEmpty()) {
                    val mediaResource = MediaResource().apply {
                        if (inType.isNotEmpty()) {
                            type = inType
                        }
                        id = it.mediaId
                        name = it.name
                        url = it.url
                        status = it.status
                    }
                    mediaResources.add(mediaResource)
                    document.addInclude(mediaResource)
                }
            } else {
                if (it.isAssigned) {
                    val outDeleteResource = DeletedMediaResource().apply {
                        id = it.mediaId
                    }
                    outDeletedMedia.add(outDeleteResource)
                }
            }
        }

        return mediaResources
    }
    */
}