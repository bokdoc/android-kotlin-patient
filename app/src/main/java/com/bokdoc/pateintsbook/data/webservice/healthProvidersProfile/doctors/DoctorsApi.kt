package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.doctors

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface DoctorsApi {

    @GET(DOCTORS_API)
    fun getDoctors(@Path("id") id: String, @QueryMap query: HashMap<String, String> = HashMap()): Call<ResponseBody>

    companion object {
        const val DOCTORS = "doctors"
        const val DOCTORS_API = "${WebserviceConstants.PROFILES_WITH_SLASH}{id}" + "/doctors"
        const val DEPARTMENTS_ID = ":department_id"
        const val DEPARTMENTS_DOCTORS = DOCTORS + DEPARTMENTS_ID
    }
}