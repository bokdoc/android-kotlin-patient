package com.bokdoc.pateintsbook.data.models.medicalHistory

interface MedicalHistory {
    var historyId:String
    var name: String
    var summary:String
    var description:String
}