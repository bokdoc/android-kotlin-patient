package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "")
class ForgetCredentials:Resource() {
    @Json(name = "email_mobile")
    lateinit var email:String
}