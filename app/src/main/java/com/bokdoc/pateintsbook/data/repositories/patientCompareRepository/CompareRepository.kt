package com.bokdoc.pateintsbook.data.repositories.patientCompareRepository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.ProfileTitle
import com.bokdoc.pateintsbook.data.webservice.models.ProfileType
import com.bokdoc.pateintsbook.data.webservice.patientCompares.CompareWebService
import com.bokdoc.pateintsbook.data.webservice.patientCompares.encoder.CompareIdsEncoder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompareRepository(val context: Context, val compareWebService: CompareWebService, val userSharedPreference: UserSharedPreference
                        , val compareSharedPreferences: CompareSharedPreferences) {

    fun getCompares(): LiveData<DataResource<Profile>> {
        val liveData = MutableLiveData<DataResource<Profile>>()
        val body = CompareIdsEncoder.getJson(compareSharedPreferences.getComparesList())
        val callable = compareWebService.getCompares(body,userSharedPreference.getToken())
        callable.enqueue(CustomResponse<Profile>({
            if(it.data!=null && it.data!!.isNotEmpty()) {
                initializeProfileIsCompare(it.data)
            }
            liveData.value = it
        }, arrayOf(Profile::class.java, ProfileTitle::class.java,ProfileType::class.java)))
        return liveData
    }

    private fun initializeProfileIsCompare(profiles:List<Profile>?){
        profiles?.forEach {
            it.isCompared = 1
        }
    }


    class CompareResponse(val liveData: MutableLiveData<DataResource<Profile>>) : Callback<ResponseBody> {
        val dataResource = DataResource<Profile>()
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            dataResource.message = t?.message!!
            liveData.value = dataResource

        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            if (response?.body() != null) {
                val profiles = Parser.parseJson(response?.body()!!.string(), arrayOf(Profile::class.java, ProfileTitle::class.java))
                dataResource.data = profiles as List<Profile>
                liveData.value = dataResource
            }
        }

    }

}