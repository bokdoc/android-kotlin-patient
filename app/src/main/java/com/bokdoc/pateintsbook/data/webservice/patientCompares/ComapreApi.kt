package com.bokdoc.pateintsbook.data.webservice.patientCompares

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
 import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ComapreApi {

    @POST(WebserviceConstants.PATIENT + "/compare")
    fun getComapres(@Body data: RequestBody): Call<ResponseBody>

}