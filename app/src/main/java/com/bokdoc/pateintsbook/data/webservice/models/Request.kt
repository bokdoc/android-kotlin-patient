package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.IListItemType
import com.bokdoc.pateintsbook.data.models.Picture
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "request")
class Request : Resource(), IListItemType {
    @Json(name = "code")
    var request_Code: String = ""
    var reservation_time: String = ""
    var reservation_date: String = ""
    var request_status: String = ""
    @Json(name = "patient_name")
    var patientName: String = ""
    var patient_phone: String = ""
    var reservation_time_normalize: String = ""
    var reservation_date_normalize: String = ""
    var patient_email: String = ""
    var patient_age: String = ""
    var patient_gender: String = ""
    var comment: String = ""
    var date_time_normalize: String = ""
    var case_of_description: String = ""
    var surgery_attachment_url: String = ""
    var status_label: String = ""
    var service_label: String = ""
    var edit_url: String = ""
    var cancel_url: String = ""
    var is_new: Boolean = false
    var reviewed: Boolean = false
    @Json(name = "book_type")
    var bookType = ""

    @Json(name = "service_type")
    var serviceType = ""

    var actions: Actions? = null

    @Json(name = "patient_picture")
    var picture: Picture = Picture()

    @Json(name = "parent")
    lateinit var doctor_data: HasOne<Profile>

    override var type: IListItemType.Types = IListItemType.Types.ITEM

    companion object {
        const val REQUEST_STATUS_PENDING = "pending"
        const val REQUEST_STATUS_CANCELLED_BY_PATIENT = "cancelled_by_patient"
        const val REQUEST_STATUS_CONFIRMED_BY_PATIENT = "confirmed"
        const val REQUEST_STATUS_FINISHED = "finished"
        const val REQUEST_STATUS_CANCELLED_BY_HP = "cancelled_by_hp"
        const val REQUEST_STATUS_GARBAGE = "garbage"
        const val REQUEST_APPROVED_BY_HP = "approved"
    }
}


@JsonApi(type = "profile")
class DoctorProfile : Resource() {
    var name: String = ""
    var overview: String = ""
    var picture: Picture = Picture()
    var main_speciality: String = ""
    var rating: Int = 0
    @Json(name = "reviews_count")
    var reviewsCount = 0

    var profileType: HasOne<ProfileData>? = null

    var location: Location? = null
    //lateinit var profileTitle: HasOne<ProfileTitleData>


}


@JsonApi(type = "profileType")
class ProfileData : Resource() {
    lateinit var name: String
    lateinit var description: String

}


@JsonApi(type = "profileTitle")
class ProfileTitleData : Resource() {
    lateinit var title: String
    lateinit var description: String
}

class Actions {

    @Json(name = "endpoint_url")
    val url: String? = null
    @Json(name = "target_screen")
    val slug: String? = null
    val label: String? = null

    companion object {
        const val REVIEW_SLUG = "review"
        const val CANCEL_SLUG = "cancel"
    }
}
