package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface HealthProvidersProfileApi {
    @GET(WebserviceConstants.PROFILES_WITH_SLASH + "{id}")
    fun getProfileSection(@Path("id") id: String, @Query("include",encoded = true) query: String): Call<ResponseBody>

    @GET
    fun show(@Url url: String): Call<ResponseBody>

    @GET(WebserviceConstants.PROFILES_WITH_SLASH + "{id}")
    fun getProfileOverview(@Path("id") id: String, @Query("include",encoded = true) query: String): Call<ResponseBody>


    @GET(WebserviceConstants.PROFILES_WITH_SLASH + "{id}/" + WebserviceConstants.ADDRESS)
    fun getProfileAddress(@Path("id") id: String): Call<ResponseBody>

    @GET(WebserviceConstants.PROFILES_WITH_SLASH + "{id}" + WebserviceConstants.SURGERIES)
    fun getSurgeries(@Path("id") id: String, @QueryMap query: HashMap<String, String> = HashMap()): Call<ResponseBody>


}