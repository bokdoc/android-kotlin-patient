package com.bokdoc.pateintsbook.data.models.profile

import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.lookups.ILookupsParcelable

class ProfileNotifications:IProfile {
    override var profileId: String?=null
    override var name: String? = null
    override var picture: Picture? = null
    override var mainSpeciality: String?=null
    override var specialityTitle: String?=null
    override var profileTitle: ILookupsParcelable?=null
    override var profileType: ILookupsParcelable?=null
}