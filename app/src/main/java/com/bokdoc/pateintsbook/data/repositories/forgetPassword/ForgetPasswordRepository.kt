package com.bokdoc.healthproviders.data.repositories.forgetPassword

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.bokdoc.healthproviders.data.webservice.forgetPassword.ForgetPasswordWebservice
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.DataResource.Companion.SUCCESS
import com.bokdoc.pateintsbook.data.models.forgetPassword.ForgetPassword
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.forgetPassword.encoder.ForgetPasswordEncoder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgetPasswordRepository(val forgetPasswordWebservice: ForgetPasswordWebservice) {

    fun passwordMobile(forgetPassword: ForgetPassword): LiveData<DataResource<Boolean>> {
        val json = ForgetPasswordEncoder.getJson(forgetPassword)
        val liveData = MutableLiveData<DataResource<Boolean>>()
        forgetPasswordWebservice.forgetMobile(json).enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

    fun passwordVerification(forgetPassword: ForgetPassword): LiveData<DataResource<Boolean>> {
        val json = ForgetPasswordEncoder.getJson(forgetPassword)
        val liveData = MutableLiveData<DataResource<Boolean>>()
        forgetPasswordWebservice.forgetCode(json).enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

    fun passwordNew(forgetPassword: ForgetPassword): LiveData<DataResource<Boolean>> {
        val json = ForgetPasswordEncoder.getJson(forgetPassword)
        val liveData = MutableLiveData<DataResource<Boolean>>()
        val dataResource = DataResource<Boolean>()

        forgetPasswordWebservice.forgetNewPassword(json).enqueue(CustomResponse<Boolean>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

}

class ForgetPasswordResponse(val liveData: MutableLiveData<DataResource<Boolean>>) : Callback<ResponseBody> {
    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
        val dataResource = DataResource<Boolean>()
        dataResource.status = DataResource.FAIL
        liveData.value = dataResource
    }

    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
        val dataResource = DataResource<Boolean>()
        if (response!!.code() == 200) {
            dataResource.status = DataResource.SUCCESS
        } else {
            dataResource.status = DataResource.FAIL
        }
        liveData.value = dataResource
    }

}
