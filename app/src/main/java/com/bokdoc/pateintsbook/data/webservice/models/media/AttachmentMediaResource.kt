package com.bokdoc.pateintsbook.data.webservice.models.media

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.media.IMediaParcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "media")
class AttachmentMediaResource() : Resource(), IMediaParcelable {

    override var mediaId: String = ""
        get() {
            if (field.isNotEmpty())
                return field
            if (id != null)
                return id
            return ""
        }
    @Json(name = "title")
    override var name: String = ""
    override var status: Boolean = false
    @Json(name = "media_type")
    override var mediaType: String = ""

    @Json(name = "poster")
    override var url: String = ""


    constructor(parcel: Parcel) : this() {
        mediaId = parcel.readString()
        name = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        mediaType = parcel.readString()
        url = parcel.readString()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mediaId)
        parcel.writeString(name)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeString(mediaType)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AttachmentMediaResource> {
        override fun createFromParcel(parcel: Parcel): AttachmentMediaResource {
            return AttachmentMediaResource(parcel)
        }

        override fun newArray(size: Int): Array<AttachmentMediaResource?> {
            return arrayOfNulls(size)
        }
    }

}