package com.bokdoc.pateintsbook.data.models.lookups

import android.os.Parcelable

interface ILookupsParcelable : LookupsType, Parcelable