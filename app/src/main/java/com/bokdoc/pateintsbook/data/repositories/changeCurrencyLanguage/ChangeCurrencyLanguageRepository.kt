package com.bokdoc.pateintsbook.data.repositories.changeCurrencyLanguage

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages.ChangeCurrencyLanguageEncoder
import com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages.ChangeCurrencyLanguageWebservice
import com.bokdoc.pateintsbook.data.webservice.login.encoder.UserEncoder
import com.bokdoc.pateintsbook.data.webservice.models.Country
import com.bokdoc.pateintsbook.data.webservice.models.Currency
import com.bokdoc.pateintsbook.data.webservice.models.Language
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class ChangeCurrencyLanguageRepository(val userSharedPreferences: UserSharedPreference,
                                       private val changeCurrencyLanguage: ChangeCurrencyLanguageWebservice) {


    fun change(currency: LookUpsParcelable, language: LookUpsParcelable, country: LookUpsParcelable): LiveData<DataResource<UserResponse>> {
        val liveData = MutableLiveData<DataResource<UserResponse>>()
        val json = ChangeCurrencyLanguageEncoder.encode(currency, language, country, userSharedPreferences.getId())

        changeCurrencyLanguage.change(userSharedPreferences.getToken(), userSharedPreferences.getId(), json).
                enqueue(CustomResponse<UserResponse>({
            if (it.data != null) {
                it.data!![0].let {
                    /*
                    it.countryLookup = country
                    it.languageLookup = language
                    it.currencyLookup = currency
                    */
                    it.token = userSharedPreferences.getToken()
                    val userJson = UserEncoder.getJson(it)
                    userSharedPreferences.saveUser(userJson)
                    changeCurrencyLanguage.webserviceManagerImpl.formatBaseUrl(it.countryString, it.languageString, it.currencyString)
                }
            }
            liveData.value = it
        }, arrayOf(UserResponse::class.java,
                Language::class.java,
                Currency::class.java,
                Country::class.java)))

        return liveData
    }

}