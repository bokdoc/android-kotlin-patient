package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accommodation

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class AccommodationWebservice(private val webServiceManger: WebserviceManagerImpl) {

//    fun getAccommodations(token: String, id: String): Call<ResponseBody> {
//        val accommodationApi = webServiceManger.createService(AccommodationApi::class.java, token)
//        return accommodationApi.getAccommodations(id)
//    }
    fun getAccommodations(token: String, id: String, service: String): Call<ResponseBody> {
        val accommodationWebservice = webServiceManger.createService(HealthProvidersProfileApi::class.java, token)
        return accommodationWebservice.getProfileSection(id, service)
    }


}