package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.bookAppointement.BookInfo
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "Request")
class BookResource : Resource(), BookInfo {

    @Json(name = "coupon")
    override var coupon: String? = ""

    @Json(name = "discount_id")
    override var discountId: String? = null
    @Transient
    @Deprecated("you must use nextAvailabilityIdNullable")
    override var nextAvailabilityId: String = ""

    @Json(name = "next_availability_id")
    var nextAvailabilityIdNullable: String? = null

    @Json(name = "service_pivot_id")
    override var servicePivotId: Int = 0
    @Json(name = "service_pivot_type")
    override var servicePivotType: String = ""

    @Transient
    @Json(name = "reservation_date")
    override var reservationDate: String = ""

    @Transient
    @Json(name = "reservation_time")
    override var reservationTime: String? = ""


    @Json(name = "service_type")
    override var serviceType: String = ""
    @Json(name = "book_type")
    override var bookType: String = ""
    @Json(name = "patient_name")
    override var patientName: String = ""
    @Json(name = "patient_mobile_number")
    override var patientMobile: String = ""
    @Transient
    @Json(name = "patient_email")
    override var patientEmail: String = ""
    @Json(name = "patient_gender")
    override var patientGender: String = ""
    @Json(name = "patient_age")
    override var patientAge: String = ""
    @Json(name = "case_of_description")
    override var surgeriesCaseDescription: String? = null
    @Json(name = "code")
    override var code: String? = null

    @Transient
    @Json(name = "profile_id")
    override var profileId: Int = 0
    @Transient
    @Json(name = "payment_type_id")
    override var paymentTypeId: Int? = null
    @Transient
    @Json(name = "payment_method_id")
    override var paymentTypeMethod: Int? = 1
    @Transient
    @Json(name = "service_id")
    override var serviceId: Int? = null
    @Json(name = "clinic_id")
    override var clinicId: Int? = null
    @Json(name = "department_id")
    override var departmentId: Int? = null
    @Json(name = "doctor_id")
    override var doctorId: Int? = null

    override var location: Location? = null

    @Transient
    override var surgeriesMedia: List<MediaParcelable>? = null

    var media: HasMany<MediaResource>? = null

    @Json(name = "device_type")
    override var deviceType: String = "mobile"

    @Json(name = "device_os")
    var deviceOs: String = "android"


    override var bookRequestType: String? = null

    @Json(name = "book_secondary_duration")
    override var bookSecondaryDuration: Boolean? = null

    companion object {
        const val BOOK_SERVICE_NAME = ""
    }

}
