package com.bokdoc.pateintsbook.data.webservice.addFcm

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.contactUs.ContactUsApi
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class AddFcmWebService (val webserviceManagerImpl: WebserviceManagerImpl) {

    fun addFcm(token: String, json: String): Call<ResponseBody> {
        val addFcmApi = webserviceManagerImpl.createService(AddFcmApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        return addFcmApi.addFcm(requestBody)
    }


    fun checkUpdate(token: String, json: String): Call<ResponseBody> {
        val addFcmApi = webserviceManagerImpl.createService(AddFcmApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        return addFcmApi.checkUpdate(requestBody)
    }

}