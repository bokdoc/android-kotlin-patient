package com.bokdoc.pateintsbook.data.models

class CustomError {
    var status:Int = 0
    var title:String=""
    var detail:String=""
}