package com.bokdoc.pateintsbook.data.webservice.lookups.encoder

import com.bokdoc.pateintsbook.data.models.AddLookup
import com.bokdoc.pateintsbook.data.webservice.models.AddLookupsResource
import com.bokdoc.pateintsbook.data.webservice.models.LookupSpeciality
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.*

object AddLookupEncoder {

    fun getJson(addLookup: AddLookup): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<AddLookupsResource>()

        val lookup = AddLookupsResource()
        lookup.id = ""

        val specialtyResource = LookupSpeciality()
        specialtyResource.id = "1"
        when {
            addLookup.disase.name.isNotEmpty() -> {
                specialtyResource.name = addLookup.disase.name
                specialtyResource.type = "diseases"
                lookup.diseases = HasOne(specialtyResource)
            }
            addLookup.medication.name.isNotEmpty() -> {
                specialtyResource.name = addLookup.medication.name
                specialtyResource.type = "medications"
                lookup.medications = HasOne(specialtyResource)
            }
        }


        document.addInclude(specialtyResource)
        document.set(lookup)

        return moshi.adapter(Document::class.java).toJson(document)
    }

    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder().add(AddLookupsResource::class.java).build()
        val moshi = Moshi.Builder().add(jsonApiAdapterFactory).build()
        return moshi
    }
}