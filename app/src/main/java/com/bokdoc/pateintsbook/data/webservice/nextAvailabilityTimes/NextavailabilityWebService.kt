package com.bokdoc.pateintsbook.data.webservice.nextAvailabilityTimes

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class NextavailabilityWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun get(url: String, token: String): Call<ResponseBody> {
        val nextAvailabilityApi = webserviceManagerImpl.createService(NextAvailabilityApi::class.java, token)
        return nextAvailabilityApi.show(url)
    }
}