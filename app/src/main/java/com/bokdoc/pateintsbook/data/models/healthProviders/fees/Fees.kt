package com.bokdoc.pateintsbook.data.models.healthProviders.fees

interface Fees {
    var amount:Double
    get() {
        return 0.0
    }
    set(value) {
        amount = value
    }
    var symbols:String
    get() {
        return ""
    }
    set(value) {
        symbols = value
    }
}

