package com.bokdoc.pateintsbook.data.webservice.models


import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class LanguageCurrencyResource:Resource() {
    var countryStr = "country"
    var country:HasOne<Country>?=null
    var currency:HasOne<Currency>?=null
    var language:HasOne<Language>?=null
}