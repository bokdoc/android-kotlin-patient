package com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback

class Feedback : IFeedback {
    override var time: String = ""

    override var name: String? =null
    override var picture: String = ""
    override var rate: Int = 0
    override var content: String = ""
    override var date: String = ""


}