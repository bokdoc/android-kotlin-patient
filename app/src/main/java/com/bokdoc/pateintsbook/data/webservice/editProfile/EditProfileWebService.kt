package com.bokdoc.pateintsbook.data.webservice.editProfile

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class EditProfileWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun update(id: String, token: String, profile: String): Call<ResponseBody> {
        val updateApi = webserviceManagerImpl.createService(EditProfileApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), profile)
        return updateApi.editProfile(id, requestBody)
    }
}