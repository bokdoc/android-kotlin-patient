package com.bokdoc.pateintsbook.data.models.healthProviders.clinics

import com.intrusoft.sectionedrecyclerview.Section

class ClinicsSections(override val title: String, override var children: List<String>, override val sectionType: String)
    :IClinicsSections,Section<String>{
    override fun getChildItems(): List<String> {
        return children
    }

}