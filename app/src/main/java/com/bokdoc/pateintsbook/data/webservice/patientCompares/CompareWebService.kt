package com.bokdoc.pateintsbook.data.webservice.patientCompares

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class CompareWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getCompares(ids: String, token: String): Call<ResponseBody> {
        val comparesApi = webserviceManagerImpl.createService(ComapreApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), ids)
        return comparesApi.getComapres(requestBody)
    }
}