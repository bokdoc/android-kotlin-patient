package com.bokdoc.pateintsbook.data.webservice.register

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.login.LoginApi
import com.bokdoc.pateintsbook.data.webservice.login.LoginWebservice
import com.bokdoc.pateintsbook.data.webservice.models.UserResponse
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class RegisterWebservice:LoginWebservice() {

    fun register(userJson:String,token:String):Call<ResponseBody>{
        val registerApi = webserviceManagerImpl.createService(RegisterApi::class.java)
        val requestBody = RequestBody.create(MediaType.parse("application/json"),userJson)
        return registerApi.register(requestBody, "Bearer $token")
    }

}