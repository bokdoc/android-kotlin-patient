package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.converters.NextAvailabilitiesParcelableConverter
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Profile

object ConsultingConverter {

    fun convert(profilesResource: HealthProviderProfile): List<ConsultingRow> {

        val consulting = ArrayList<ConsultingRow>()
        profilesResource.consulting?.get(profilesResource.document)?.forEach {

            var consultRow = ConsultingRow()
            consultRow.location = it.location
            consultRow.profileType = profilesResource.profileType.get(it.document).id.toInt()

            consultRow.id = it.id
            if (it.name != null)
                consultRow.name = it.name!!
            if (it.profile != null) {
                consultRow.doctorName = it.profile!!.get(it.document).name
                consultRow.doctorPhoto = it.profile!!.get(it.document).picture.url
                consultRow.speciality = it.speciality.get(it.document).name
                if (it.profile!!.get(it.document).profileTitle != null)
                    consultRow.doctorTitle = it.profile!!.get(it.document).profileTitle!!.get(it.document).name
            } else {
                if (consultRow.profileType == Profile.DOCTOR) {
                    consultRow.doctorName = profilesResource.mainSpeciality
                    consultRow.doctorPhoto = profilesResource.picture.url
                    consultRow.speciality = profilesResource.mainSpeciality
                }
            }
            consultRow.speciality = profilesResource.mainSpeciality
            consultRow.nationalFees = it.mainFees
            consultRow.interNationalFees = it.subMainFees
            consultRow.internationalFeesLabel = it.subMainFees.label
            consultRow.nationalFeesLabel = it.mainFees.label
            consultRow.feesCurrancy = it.fees!!.currency_symbols
            consultRow.nextAvailabilities = NextAvailabilitiesParcelableConverter.convert(it.nextAvailabilities!!.get(it.document))
            consultRow.bookButton = it.bookButton
            consulting.add(consultRow)
        }

        return consulting

    }

    fun convert(linksCardsDetails: LinksCardsDetails): List<ConsultingRow> {

        val consulting = ArrayList<ConsultingRow>()
        linksCardsDetails.rowsInfo.forEach {

            var consultRow = ConsultingRow()
            consultRow.location = it.location
            consultRow.id = it.id
            consultRow.name = it.name
            consultRow.profileType = linksCardsDetails.mainProfileInfo.profileType

            if (it.profileInfo.profileId.isNotEmpty()) {
                consultRow.doctorName = it.profileInfo.profileName
                consultRow.doctorPhoto = it.profileInfo.profilePicture.url
                consultRow.speciality = it.profileInfo.profileMainSpeciality
                consultRow.gender = it.profileInfo.gender
            } else {
                if(consultRow.profileType == Profile.DOCTOR) {
                    consultRow.doctorName = linksCardsDetails.mainProfileInfo.profileName
                    consultRow.doctorPhoto = linksCardsDetails.mainProfileInfo.profilePicture.url
                    consultRow.speciality = linksCardsDetails.mainProfileInfo.profileMainSpeciality
                }
            }
            consultRow.nationalFees = it.mainFees
            consultRow.interNationalFees = it.subMainFees
            consultRow.internationalFeesLabel = it.subMainFees.label
            consultRow.nationalFeesLabel = it.mainFees.label

            consultRow.nextAvailabilities = it.nextAvailabilities

            consultRow.bookButton = it.bookButton


            consulting.add(consultRow)
        }

        return consulting

    }
}

