package com.bokdoc.pateintsbook.data.models.lookups

interface LookupsType {
    var name:String
    var idLookup:String
}