package com.bokdoc.pateintsbook.data.models.healthProviders.clinics

import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.IFeesParacable
import com.bokdoc.pateintsbook.data.models.media.MediaParcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.INextAvailabilities
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.INextAvailabilitiesParcelable

interface IClinicsParacable: IClinics,Parcelable{
    val feesParcelable:IFeesParacable
    var profileMediaParacable:List<MediaParcelable>
    var nextAvailabilitesParceable:List<INextAvailabilitiesParcelable>
    override var nextAvailabilities:List<INextAvailabilities>
    get() {
        return nextAvailabilitesParceable
    }
    set(value) {}

    override var fees: Fees
        get() = feesParcelable
        set(value) {}

    override var media: List<MediaParcelable>
        get() = profileMediaParacable
        set(value) {}
}