package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.healthProviders.AccommodationSections
import com.bokdoc.pateintsbook.data.webservice.models.Accommodation
import com.bokdoc.pateintsbook.data.webservice.models.HealthProfileProvidersResource
import com.bokdoc.pateintsbook.utils.constants.StringsConstants

object AccommodationsSectionsConverter {

    fun convert(profileResource: HealthProfileProvidersResource): List<AccommodationSections> {


        val accommodationSections = ArrayList<AccommodationSections>()
        profileResource.description.let {
            if (it.isNotEmpty())
                accommodationSections.add(AccommodationSections(AccommodationSections.ABOUT_SECTION, listOf(it)))
        }

        profileResource.patientRoom.let {
            if (it.isNotEmpty())
                accommodationSections.add(AccommodationSections(AccommodationSections.PATIENT_ROOM_SECTION, listOf(it)))
        }

        profileResource.location.let {
            if (it.address.isNotEmpty())
                accommodationSections.add(AccommodationSections(AccommodationSections.LOCATION_ROOM_SECTION, listOf(it.address)))
        }

        getSocialNetworks(profileResource).let {
            if (it.isNotEmpty())
                accommodationSections.add(AccommodationSections(AccommodationSections.SOCIAL_NETWORKS_SECTION, listOf(it)))
        }

        profileResource.ambulanceCount.let {
            if (it != 0)
                accommodationSections.add(AccommodationSections(AccommodationSections.AMBULANCE_SECTION, arrayListOf(it.toString())))
        }


        profileResource.bedsNumber.let {
            if (it != 0)
                accommodationSections.add(AccommodationSections(AccommodationSections.BEDS_NUMBER_SECTION, arrayListOf(it.toString())))
        }

        profileResource.accompanyingPersons.let {
            if (it != 0)
                accommodationSections.add(AccommodationSections(AccommodationSections.ACCOMPANYING_PERSON_SECTION, arrayListOf(it.toString())))
        }

        getAccommodationsStrings(profileResource.accommodations.get(profileResource.document)).let {
            if (it.isNotEmpty()) {
                accommodationSections.add(AccommodationSections(AccommodationSections.ACCOMMODATIONS_SECTION, it))
            }
        }


        return accommodationSections

    }

    private fun getSocialNetworks(profileResource: HealthProfileProvidersResource): String {
        val socialNetworks = ArrayList<String>()
        profileResource.facebookUrl.let {
            if (it.isNotEmpty())
                socialNetworks.add(it)
        }

        profileResource.twitterUrl.let {
            if (it.isNotEmpty())
                socialNetworks.add(it)
        }

        profileResource.googlePlusUrl.let {
            if (it.isNotEmpty())
                socialNetworks.add(it)
        }

        profileResource.linkedinUrl.let {
            if (it.isNotEmpty())
                socialNetworks.add(it)
        }

        profileResource.instagramUrl.let {
            if (it.isNotEmpty())
                socialNetworks.add(it)
        }

        return StringsConstants.buildStringWithSplitter(socialNetworks, StringsConstants.SPACE)

    }

    private fun getAccommodationsStrings(accommodations: List<Accommodation>): List<String> {
        val accommodationStrings = ArrayList<String>()
        val size = accommodations.size
        var texts: ArrayList<String>
        (0 until size).forEach {
            texts = ArrayList()
            accommodations[it].let { accommodation ->
                accommodation.title.let {
                    if (it.isNotEmpty())
                        texts.add(it)
                }
                accommodation.description.let {
                    if (it.isNotEmpty())
                        texts.add(StringsConstants.COLON)
                    texts.add(StringsConstants.SPACE)
                    texts.add(it)
                }
                accommodationStrings.add(StringsConstants.buildString(texts))
            }
        }

        return accommodationStrings
    }

}