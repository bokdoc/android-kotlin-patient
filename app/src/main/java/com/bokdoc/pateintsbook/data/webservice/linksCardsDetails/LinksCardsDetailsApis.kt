package com.bokdoc.pateintsbook.data.webservice.linksCardsDetails

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface LinksCardsDetailsApis {

    @GET
    fun getDetails(@Url url:String):Call<ResponseBody>

    @GET
    fun getDurationWithDiscountDetails(@Url url:String,@Query("coupon")coupon:String):Call<ResponseBody>
}