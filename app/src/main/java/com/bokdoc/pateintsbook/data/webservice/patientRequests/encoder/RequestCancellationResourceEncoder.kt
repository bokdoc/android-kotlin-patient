package com.bokdoc.pateintsbook.data.webservice.patientRequests.encoder

import com.bokdoc.pateintsbook.data.webservice.models.LoginSocials
import com.bokdoc.pateintsbook.data.webservice.models.RequestCancellationResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object RequestCancellationResourceEncoder {
    fun getJson(reason:String):String{
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(RequestCancellationResource::class.java)
                .build()


        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        val requestCancellationResource = RequestCancellationResource()
        requestCancellationResource.reason = reason
        requestCancellationResource.id = " "



        val document = ObjectDocument<RequestCancellationResource>()
        document.set(requestCancellationResource)
        return moshi.adapter(Document::class.java).toJson(document)

    }
}