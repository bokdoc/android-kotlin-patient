package com.bokdoc.pateintsbook.data.webservice.filterResults

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FilterApi {
    @GET(WebserviceConstants.SEARCH_SORTS_BY_URL)
    fun getFilterOptions(@Query("screen_type")screenType:String): Call<ResponseBody>
}