package com.bokdoc.pateintsbook.data.webservice.editProfile

import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.User
import com.bokdoc.pateintsbook.data.webservice.models.UpdateProfileResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object EditProfileEncoder {

    fun getJson(user: User, id: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<UpdateProfileResource>()
        val updateProfileResource = UpdateProfileResource()

        updateProfileResource.id = id
        updateProfileResource.name = user.name
        updateProfileResource.mobile_number = user.phone

        if (user.email.isNotEmpty())
            updateProfileResource.email = user.email

        if (user.birthDate.isNotEmpty())
            updateProfileResource.birthdate = user.birthDate

        if (user.gender.isNotEmpty())
            updateProfileResource.gender = user.gender

        updateProfileResource.picture = user.picture

        document.set(updateProfileResource)
        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(UpdateProfileResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}