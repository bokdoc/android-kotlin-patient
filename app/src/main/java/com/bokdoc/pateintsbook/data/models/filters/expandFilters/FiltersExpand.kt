package com.bokdoc.pateintsbook.data.models.filters.expandFilters

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class FiltersExpand(
        var name:String,
        var options:List<FilterExpandChild>,
        var dataType:String,
        var key:String): ExpandableGroup<FilterExpandChild>(name,options) {


    companion object {
        const val CHECKBOX_TYPE = "checkbox"
        const val RANGER_TYPE = "ranger"
        const val STAR_TYPE = "star"
        const val CALENDAR_TYPE = "calender"
        const val TIMES_RANGER_TYPE = "timeRanger"
        const val MAP_TYPE = "map"
        const val FREE_APPOINTMENT = "free_appointment"
        const val IS_HOME_VISIT = "is_home_visit"
    }
}