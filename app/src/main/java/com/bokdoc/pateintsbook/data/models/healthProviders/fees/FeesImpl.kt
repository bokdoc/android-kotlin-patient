package com.bokdoc.pateintsbook.data.models.healthProviders.fees

class FeesImpl: Fees {
    override var amount: Double = 0.0
    override var symbols: String = ""
}