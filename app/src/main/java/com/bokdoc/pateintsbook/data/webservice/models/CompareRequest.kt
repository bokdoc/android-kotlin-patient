package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource
import java.util.jar.Attributes

@JsonApi(type = "compare_ids")
class CompareRequest : Resource() {
    @Json(name = "compare_ids")
    var compare_ids = listOf<String>()
}