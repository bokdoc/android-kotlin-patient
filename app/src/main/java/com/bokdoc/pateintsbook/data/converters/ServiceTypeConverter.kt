package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.FilterModel
import com.bokdoc.pateintsbook.data.models.meta.FilterData
import com.bokdoc.pateintsbook.data.models.meta.ServiceType

object ServiceTypeConverter {


    fun convert(filterData: List<FilterData>): List<FilterModel> {

        val menu = ArrayList<FilterModel>()

        /*
        for (data in serviceType) {
            if (!data.appointment.isNullOrEmpty()) {
                menu.add(FilterModel("appointment", data.appointment.toString()))
            } else if (!data.scan.isNullOrEmpty()) {
                menu.add(FilterModel("scan", data.scan.toString()))

            } else if (!data.test.isNullOrEmpty()) {
                menu.add(FilterModel("test", data.test.toString()))

            } else if (!data.consulting.isNullOrEmpty()) {
                menu.add(FilterModel("consulting", data.consulting.toString()))

            } else if (!data.homeVisit.isNullOrEmpty()) {
                menu.add(FilterModel("home_visit", data.homeVisit.toString()))

            } else if (!data.surgery.isNullOrEmpty()) {
                menu.add(FilterModel("surgery", data.surgery.toString()))
            }
        }
        */
        filterData.forEach {
            menu.add(FilterModel(it.key.toString(),it.value))
        }
        return menu
    }


}