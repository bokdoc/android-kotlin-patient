package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.surgeries

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.SingleSurgeryConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.SurgeriesConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.surgeries.ProfileSurgeriesWebService
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SingleSurgery
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import com.bokdoc.pateintsbook.data.webservice.models.Speciality

class ProfileSurgeriesRepository(private val appContext: Context,
                                 private val sharedPreferencesManager: SharedPreferencesManager,
                                 private val surgeriesWebService: ProfileSurgeriesWebService) {


    fun getSurgeries(id: String, service: HashMap<String, String>): LiveData<DataResource<SurgeriesRow>> {

        val liveData = MutableLiveData<DataResource<SurgeriesRow>>()
        val dataResource = DataResource<SurgeriesRow>()

        surgeriesWebService.getSurgeries(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<Surgeries>({
                    dataResource.status = it.status
                    if (it.data != null && it.data!!.isNotEmpty()) {
                        dataResource.data = SurgeriesConverter.convert(it.data!!)
                        dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java, ProfileTitle::class.java, NextAvailabilityResource::class.java,
                        Surgeries::class.java, ProfileType::class.java, Speciality::class.java)))

        return liveData
    }


    fun showSurgery(url: String): LiveData<DataResource<SingleSurgery>> {

        val liveData = MutableLiveData<DataResource<SingleSurgery>>()
        val dataResource = DataResource<SingleSurgery>()

        surgeriesWebService.show(url, sharedPreferencesManager.getData(appContext.getString(R.string.token), ""))
                .enqueue(CustomResponse<SingleSurgeryResource>({
                    dataResource.status = it.status
                    if (it.data != null && it.data!!.isNotEmpty()) {
                        dataResource.data = arrayListOf(SingleSurgeryConverter.convert(it.data!![0]))
                        //dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(SingleSurgeryResource::class.java, Anesthesia::class.java, ScanService::class.java,
                        TestService::class.java, SurgeryDetailsResource::class.java, Speciality::class.java,
                        NextAvailabilityResource::class.java, WorkingTimes::class.java, SurgeryMedia::class.java)))

        return liveData
    }

}