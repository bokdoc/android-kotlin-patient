package com.bokdoc.pateintsbook.data.models.healthProviders.awards

interface Awards {
    val icon:String
    val image:String
    val title:String
    val content:String
}