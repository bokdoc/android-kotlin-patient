package com.bokdoc.pateintsbook.data.bookAppointement

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.KeyValue
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Location

class HealthProvidersUIBookInfoImp() : HealthProvidersBookUIInfo, Parcelable {
    override var doctorId: Int? = 0
    override var profileId: Int = 0
    override var profileTypeId: Int = 0
    override var paymentTypeId: Int? = 0
    override var paymentTypeMethod: Int? = 0
    override var serviceId: Int? = 0
    override var clinicId: Int? = 0
    override var departmentId: Int? = 0
    override var name: String = ""
    override var location: Location? = Location()
    override var picture: String = ""
    override var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()
    override var profileTypeName: String = ""
    override var specialty: String = ""
    override var gender: String = ""
    override var serviceInfo = LinksCardsDetails.RowsInfo()
    override var serviceSlug: String = ""
    override var rotators: List<String> = ArrayList()
    override var secondaryDurationConsulting: Boolean =false
    override var secondaryDurations: List<KeyValue> = arrayListOf()

    constructor(parcel: Parcel) : this() {
        doctorId = parcel.readValue(Int::class.java.classLoader) as? Int
        profileId = parcel.readInt()
        profileTypeId = parcel.readInt()
        paymentTypeId = parcel.readValue(Int::class.java.classLoader) as? Int
        paymentTypeMethod = parcel.readValue(Int::class.java.classLoader) as? Int
        serviceId = parcel.readValue(Int::class.java.classLoader) as? Int
        clinicId = parcel.readValue(Int::class.java.classLoader) as? Int
        departmentId = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        location = parcel.readParcelable(Location::class.java.classLoader)
        picture = parcel.readString()
        nextAvailabilities = parcel.createTypedArrayList(NextAvailabilitiesParcelable)
        profileTypeName = parcel.readString()
        specialty = parcel.readString()
        gender = parcel.readString()
        serviceSlug = parcel.readString()
        rotators = parcel.createStringArrayList()
        secondaryDurationConsulting = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(doctorId)
        parcel.writeInt(profileId)
        parcel.writeInt(profileTypeId)
        parcel.writeValue(paymentTypeId)
        parcel.writeValue(paymentTypeMethod)
        parcel.writeValue(serviceId)
        parcel.writeValue(clinicId)
        parcel.writeValue(departmentId)
        parcel.writeString(name)
        parcel.writeParcelable(location, flags)
        parcel.writeString(picture)
        parcel.writeTypedList(nextAvailabilities)
        parcel.writeString(profileTypeName)
        parcel.writeString(specialty)
        parcel.writeString(gender)
        parcel.writeString(serviceSlug)
        parcel.writeStringList(rotators)
        parcel.writeByte(if (secondaryDurationConsulting) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HealthProvidersUIBookInfoImp> {
        override fun createFromParcel(parcel: Parcel): HealthProvidersUIBookInfoImp {
            return HealthProvidersUIBookInfoImp(parcel)
        }

        override fun newArray(size: Int): Array<HealthProvidersUIBookInfoImp?> {
            return arrayOfNulls(size)
        }
    }


}