package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "request")
class RequestCancellationResource:Resource() {
    @Json(name = "reason_of_cancellation")
    var reason:String?=null
}