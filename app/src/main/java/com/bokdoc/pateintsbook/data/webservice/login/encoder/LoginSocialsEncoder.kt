package com.bokdoc.pateintsbook.data.webservice.login.encoder

import com.bokdoc.pateintsbook.data.webservice.models.LoginSocials
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object LoginSocialsEncoder {
    fun getJson(providerId:String,token:String,secret:String="",email:String="",name:String=""
                ,picture:String="",clientType:String ="android"):String{
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(LoginSocials::class.java)
                .build()


        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()


        val loginSocials = LoginSocials()

        loginSocials.providerId = providerId
        loginSocials.token = token

        if(secret.isNotEmpty())
        loginSocials.secret = secret
        if(name.isNotEmpty())
        loginSocials.name = name
        if(picture.isNotEmpty())
        loginSocials.picture = picture
        if(email.isNotEmpty())
        loginSocials.email = email

        loginSocials.clientType = clientType

        val document = ObjectDocument<LoginSocials>()
        document.set(loginSocials)
        return moshi.adapter(Document::class.java).toJson(document)

    }
}