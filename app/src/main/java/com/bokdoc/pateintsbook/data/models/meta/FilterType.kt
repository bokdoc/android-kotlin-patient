package com.bokdoc.pateintsbook.data.models.meta

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FilterType {

    @SerializedName("title")
    @Expose
    var title: String = "";
    @SerializedName("param")
    @Expose
    var param: String = "";
    @SerializedName("data")
    @Expose
    var data: List<FilterData>? = null

}