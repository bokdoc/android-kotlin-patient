package com.bokdoc.pateintsbook.data.webservice.models

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "socials")
class LoginSocials:Resource(){
    @Json(name = "provider_id")
    lateinit var providerId:String
    lateinit var token:String
    var secret:String?=null
    var email:String?=null
    var name:String?=null
    var picture:String?=null
    @Json(name="client-type")
    lateinit var clientType:String
}