package com.bokdoc.pateintsbook.data.webservice.autoComplete

import okhttp3.ResponseBody
import retrofit2.Call

interface AutoCompleteWebservice {

    fun getSpecialityAutoResults(keyword: String, screenType: String): Call<ResponseBody>
    fun getLocationsAutoResults(keyword: String): Call<ResponseBody>
    fun getInsuranceAutoResults(keyword: String): Call<ResponseBody>
}