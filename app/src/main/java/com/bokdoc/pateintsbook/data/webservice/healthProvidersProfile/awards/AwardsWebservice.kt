package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.awards

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class AwardsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {


    fun getAwards(token:String,id:String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java,token)
                .getProfileSection(id, AWARDS)
    }

    companion object {
        const val AWARDS = "awards"
    }
}