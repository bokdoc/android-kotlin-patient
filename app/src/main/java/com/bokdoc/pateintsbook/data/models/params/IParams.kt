package com.bokdoc.pateintsbook.data.models.params

interface IParams {
    var id:String?
    var screenType:String?
    var token:String?
}