package com.bokdoc.pateintsbook.data.bookAppointement

import com.bokdoc.pateintsbook.data.models.KeyValue
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.LinksCardsDetails
import com.bokdoc.pateintsbook.data.webservice.models.Location

interface HealthProvidersUIInfo {
    var location: Location?
        set(value) {}
        get() = Location()
    var picture: String
        set(value) {}
        get() = ""
    var nextAvailabilities: List<NextAvailabilitiesParcelable>
        set(value) {}
        get() = ArrayList()
    var profileTypeId: Int
        set(value) {}
        get() = 0
    var profileTypeName: String
        set(value) {}
        get() = ""
    var specialty: String
        set(value) {}
        get() = ""
    var name: String
        set(value) {}
        get() = ""
    var gender: String
        set(value) {}
        get() = ""
    var serviceSlug: String
        set(value) {}
        get() = ""
    var serviceInfo: LinksCardsDetails.RowsInfo
        set(value) {}
        get() = LinksCardsDetails.RowsInfo()
    var rotators: List<String>
        set(value) {}
        get() = ArrayList()

    var secondaryDurationConsulting:Boolean
        set(value) {}
        get() = false

    var secondaryDurations:List<KeyValue>
        set(value) {}
        get() = arrayListOf()

}