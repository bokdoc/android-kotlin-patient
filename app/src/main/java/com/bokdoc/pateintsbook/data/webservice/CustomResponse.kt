package com.bokdoc.pateintsbook.data.webservice

import com.bokdoc.pateintsbook.data.common.parser.ErrorsParser
import com.bokdoc.pateintsbook.data.common.parser.MetaParser
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.webservice.models.UnknownResource
import moe.banana.jsonapi2.Resource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

open class CustomResponse<T>(val onResponse: (DataResource<T>) -> Unit, val resources: Array<Class<out Resource>>) :
        Callback<ResponseBody> {
    val dataResource = DataResource<T>()

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        dataResource.status = DataResource.FAIL
        if (t is UnknownHostException) {
            dataResource.status = DataResource.FAIL_NO_INTERNET
        } else if (t is TimeoutException) {
            dataResource.status = DataResource.FAIL_TIME_OUT
        }
        onResponse.invoke(dataResource)
    }

    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        dataResource.status = response.code().toString()
        if (response.body() != null) {
            val json = response.body()!!.string()
            if (resources.isNotEmpty()) {
                try {
                    val data = Parser.parseJson(json, resources)
                    if (data.isNotEmpty()) {
                        if (data[0] is UnknownResource) {
                            dataResource.status = DataResource.INTERNAL_SERVER_ERROR
                        } else {
                            dataResource.data = data as List<T>
                        }
                    } else {
                        dataResource.data = ArrayList()
                    }
                } catch (e: Exception) {
                    dataResource.status = DataResource.INTERNAL_SERVER_ERROR
                }

            }

            try {
                dataResource.meta = MetaParser.paresMeta(json)
                dataResource.message = dataResource.meta.message
            } catch (e: Exception) {

            }
        } else {
            if (dataResource.status == DataResource.INTERNAL_SERVER_ERROR) {
                //dataResource.status = DataResource.INTERNAL_SERVER_ERROR
            } else {
                if (response.errorBody() != null) {
                    dataResource.errors = ErrorsParser.parseError(response.errorBody()!!.string()!!)
                    if (dataResource.errors.isNotEmpty())
                        dataResource.message = dataResource.errors[0].detail
                }
            }
        }

        if (dataResource.status.toInt() < 300) {
            dataResource.status = DataResource.SUCCESS
        }

        if (dataResource.status == DataResource.INTERNAL_SERVER_ERROR)
            dataResource.data = ArrayList()

        onResponse.invoke(dataResource)
    }
}