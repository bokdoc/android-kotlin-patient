package com.bokdoc.pateintsbook.data.webservice.models

import com.bokdoc.pateintsbook.data.models.nextAvailabilities.INextAvailabilities
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.ITimes
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.ITimesParcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "nextAvailability")
class NextAvailabilityResource : Resource(), INextAvailabilities {
    @Json(name = "date_id")
    override val dateId: Int = 0
    @Json(name = "date_normalize")
    override val dateNormalized: String = ""
    override val idNextAvailabilities: String
        get() = id
    override val book_button: DynamicLinks? = null
    override val date: String = ""
    override val date_day: String = ""
    override val to: String = ""
    override val from: String = ""
    override val status: Boolean = false


}