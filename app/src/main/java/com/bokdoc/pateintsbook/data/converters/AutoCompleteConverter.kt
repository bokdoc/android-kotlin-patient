package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.models.AutoCompleteValue
import com.bokdoc.pateintsbook.data.webservice.models.Profile
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.*
import moe.banana.jsonapi2.HasMany

object AutoCompleteConverter {

    fun convert(autoCompleteResource: List<AutoCompleteResource>): List<AutoComplete> {

        var autoCompletes = ArrayList<AutoComplete>()

        autoCompleteResource.forEach {
            autoCompletes.add(AutoComplete(it.header, getAutoCompletesValues(it)))
        }
        return autoCompletes
    }

    private fun getAutoCompletesValues(autoCompleteResource: AutoCompleteResource): List<AutoCompleteValue> {
        var autoCompleteValues = ArrayList<AutoCompleteValue>()
        when {
            autoCompleteResource.slug == AutoCompleteResource.DOCTORS_SLUG -> {
                val doctorsList = autoCompleteResource.doctors?.get(autoCompleteResource.document)
                doctorsList?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(doctorsList[it].name, doctorsList[it].params
                                , doctorsList[it].picture, doctorsList[it].location,doctorsList[it].profileId,Profile.DOCTOR))
                    }
                }
            }
            autoCompleteResource.slug == AutoCompleteResource.LINKS -> {
                val linksList = autoCompleteResource.links?.get(autoCompleteResource.document)
                 linksList?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(linksList[it].name, linksList[it].params))
                    }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.RADIOLOGY -> {
                val rads = autoCompleteResource.rads?.get(autoCompleteResource.document)
                rads?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(rads[it].name, rads[it].params,
                                rads[it].picture, rads[it].location,rads[it].profileId,Profile.RAD))
                    }
                }

            }
            autoCompleteResource.header == AutoCompleteResource.SERVICES_HEADER -> {
                val services = autoCompleteResource.services?.get(autoCompleteResource.document)
                val size = services?.size
                (0 until size!!).forEach {
                    autoCompleteValues.add(AutoCompleteValue(services[it].name, services[it].params))
                }
            }
            autoCompleteResource.slug == AutoCompleteResource.SPECIALITIES_SLUG -> {
                val specialities = autoCompleteResource.specialities?.get(autoCompleteResource.document)
                specialities?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(specialities[it].name, specialities[it].params))
                    }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.LOCATIONS -> {
                val locations = autoCompleteResource.locations?.get(autoCompleteResource.document)
                locations?.size?.let {
                   (0 until it).forEach {
                       autoCompleteValues.add(AutoCompleteValue(locations[it].name, locations[it].params))
                   }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.INSURANCE_CARRIER_PLANS -> {
                val insuranceCarrierPlans = autoCompleteResource.insuranceCarriers?.get(autoCompleteResource.document)
                insuranceCarrierPlans?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(insuranceCarrierPlans[it].name, insuranceCarrierPlans[it].params))
                    }
                }

            }

            autoCompleteResource.slug == AutoCompleteResource.HOSPITALS -> {
                val hospitals = autoCompleteResource.hospitals?.get(autoCompleteResource.document)
                hospitals?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(hospitals[it].name,
                                hospitals[it].params, hospitals[it].picture, hospitals[it].location,hospitals[it].profileId,
                                Profile.HOSPITAL))
                    }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.LABORATORIES -> {
                val labs = autoCompleteResource.laboratories?.get(autoCompleteResource.document)
                labs?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(labs[it].name, labs[it].params, labs[it].picture, labs[it].location,
                                labs[it].profileId,Profile.LAB))
                    }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.TEST -> {
                val tests = autoCompleteResource.tests?.get(autoCompleteResource.document)
                tests?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(tests[it].name, tests[it].params))
                    }
                }

            }
            autoCompleteResource.slug == AutoCompleteResource.SCANS -> {
                val scans = autoCompleteResource.scans?.get(autoCompleteResource.document)
                scans?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(scans[it].name, scans[it].params))
                    }
                }

            }

            autoCompleteResource.slug == AutoCompleteResource.SURGERIES_SLUG -> {
                val surgeries = autoCompleteResource.surgeries?.get(autoCompleteResource.document)
                surgeries?.size?.let {size->
                    (0 until size).forEach {
                        autoCompleteValues.add(AutoCompleteValue(surgeries[it].name, surgeries[it].params))
                    }
                }

            }



        }
        return autoCompleteValues
    }


}