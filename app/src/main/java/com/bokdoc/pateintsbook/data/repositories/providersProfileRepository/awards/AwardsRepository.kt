package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.awards

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.awards.Awards
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.awards.AwardsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.AwardsResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProfileProvidersResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProvidersProfileType
import retrofit2.Callback
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class AwardsRepository(private val appContext:Context,
                       private val sharedPreferencesManager: SharedPreferencesManager,
                        private val awardsWebservice: AwardsWebservice) {


    fun getAwards(id:String):LiveData<DataResource<Awards>>{
        val liveData = MutableLiveData<DataResource<Awards>>()
        val dataResource = DataResource<Awards>()
        awardsWebservice.getAwards(sharedPreferencesManager.getData(appContext.getString(R.string.token),""),id)
                .enqueue(object : Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        dataResource.message = t?.message!!
                        liveData.value = dataResource
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if(response?.body()!=null){
                            val profile = Parser.parseJson(response.body()?.string()!!, arrayOf(HealthProfileProvidersResource::class.java,
                                    HealthProvidersProfileType::class.java,AwardsResource::class.java))[0] as HealthProfileProvidersResource
                            dataResource.data = profile.awards.get(profile.document)
                            liveData.value = dataResource
                        }else{
                            dataResource.message = response?.errorBody()!!.string()
                            liveData.value = dataResource
                        }
                    }

                })

        return liveData
    }
}