package com.bokdoc.pateintsbook.data.models.healthProviders.clinics

interface IClinicsSections {
    val title:String
    val children:List<String>
    val sectionType:String

    companion object {
        const val TEXT_SECTION = "text"
        const val LOCATION_SECTION = "location"
        const val NEXT_AVAILABILITY_SECTION = "availability"
        const val PHOTOS_VIDEOS_SECTION = "photos"
        const val FEES_SECTION = "fees"
        const val APPOINTEMENT_SECTION = "free"
    }
}