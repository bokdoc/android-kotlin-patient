package com.bokdoc.pateintsbook.data.repositories.autoComplete

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.AutoCompleteConverter
import com.bokdoc.pateintsbook.data.models.AutoComplete
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.autoComplete.AutoCompleteWebservice
import com.bokdoc.pateintsbook.data.webservice.autoComplete.AutoCompleteWebserviceImpl
import com.bokdoc.pateintsbook.data.webservice.autoComplete.parser.AutoCompleteParser
import com.bokdoc.pateintsbook.data.webservice.models.autoComplete.AutoCompleteResource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AutoCompleteRepository(val context: Context) {
    private var autoCompleteWebservice: AutoCompleteWebservice
    var sharedPreferencesManager = SharedPreferencesManagerImpl(context)

    init {
        autoCompleteWebservice = AutoCompleteWebserviceImpl(sharedPreferencesManager.getData(context.getString(R.string.token), ""))
    }

    fun getSpecialityAutoResults(keyword: String, screenType: String): LiveData<List<AutoComplete>> {
        val data = MutableLiveData<List<AutoComplete>>()
        autoCompleteWebservice.getSpecialityAutoResults(keyword, screenType).enqueue(CustomCallBack(data))
        return data
    }

    fun getLocationAutoResults(keyword: String): MutableLiveData<List<AutoComplete>> {
        val data = MutableLiveData<List<AutoComplete>>()
        autoCompleteWebservice.getLocationsAutoResults(keyword).enqueue(CustomCallBack(data))
        return data
    }

    fun getInsuranceAutoResults(keyword: String): MutableLiveData<List<AutoComplete>> {
        val data = MutableLiveData<List<AutoComplete>>()
        autoCompleteWebservice.getInsuranceAutoResults(keyword).enqueue(CustomCallBack(data))
        return data
    }


    inner class CustomCallBack(private val data: MutableLiveData<List<AutoComplete>>) : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            //
            if (response?.body() != null) {
                val json = response.body()!!.string()
                Log.i("autoComplete", json)
                val jsonApiObject = AutoCompleteParser.parse(json)
                val list = AutoCompleteConverter.convert(jsonApiObject)
                data.value = list
            } else {
                data.value = ArrayList()
            }
        }


    }


}