package com.bokdoc.pateintsbook.data.models.nextAvailabilities

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks

class NextAvailabilitiesParcelable() : INextAvailabilitiesParcelable {

    override var idNextAvailabilities: String?=""
    override var date: String? = ""
    override var date_day: String?=""
    override var book_button: DynamicLinks? = null
    override var to: String? = ""
    override var from: String?=""
    override var status: Boolean = false
    var isSelected = false
    var isSelectedAfterClick = false
    var isShowButton = true
    override var dateNormalized: String?=""
    override var dateId: Int = 0
    var isVisible: Boolean = true
    var buttonText: String?=""

    constructor(parcel: Parcel) : this() {
        idNextAvailabilities = parcel.readString()
        date = parcel.readString()
        date_day = parcel.readString()
        book_button = parcel.readParcelable(DynamicLinks::class.java.classLoader)
        to = parcel.readString()
        from = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        isSelected = parcel.readByte() != 0.toByte()
        isSelectedAfterClick = parcel.readByte() != 0.toByte()
        isShowButton = parcel.readByte() != 0.toByte()
        dateNormalized = parcel.readString()
        dateId = parcel.readInt()
        isVisible = parcel.readByte() != 0.toByte()
        buttonText = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idNextAvailabilities)
        parcel.writeString(date)
        parcel.writeString(date_day)
        parcel.writeParcelable(book_button, flags)
        parcel.writeString(to)
        parcel.writeString(from)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeByte(if (isSelectedAfterClick) 1 else 0)
        parcel.writeByte(if (isShowButton) 1 else 0)
        parcel.writeString(dateNormalized)
        parcel.writeInt(dateId)
        parcel.writeByte(if (isVisible) 1 else 0)
        parcel.writeString(buttonText)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NextAvailabilitiesParcelable> {
        override fun createFromParcel(parcel: Parcel): NextAvailabilitiesParcelable {
            return NextAvailabilitiesParcelable(parcel)
        }

        override fun newArray(size: Int): Array<NextAvailabilitiesParcelable?> {
            return arrayOfNulls(size)
        }
    }


}