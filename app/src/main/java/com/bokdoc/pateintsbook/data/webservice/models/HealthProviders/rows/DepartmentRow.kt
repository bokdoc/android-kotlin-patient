package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks

class DepartmentRow() :Parcelable {
    var id: String = ""
    var name: String = ""
    var doctorCount: String = ""
    var description: String = ""
    var showUrl: String = ""
    var bookButton: DynamicLinks? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        doctorCount = parcel.readString()
        description = parcel.readString()
        showUrl = parcel.readString()
        bookButton = parcel.readParcelable(DynamicLinks::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(doctorCount)
        parcel.writeString(description)
        parcel.writeString(showUrl)
        parcel.writeParcelable(bookButton, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DepartmentRow> {
        override fun createFromParcel(parcel: Parcel): DepartmentRow {
            return DepartmentRow(parcel)
        }

        override fun newArray(size: Int): Array<DepartmentRow?> {
            return arrayOfNulls(size)
        }
    }


}