package com.bokdoc.pateintsbook.data.webservice.twillio

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface TwillioApi {

    @GET(WebserviceConstants.CONSULTINGDATA + "/{id}/get-consulting-data")
    fun getTwillioToken(@Path("id") id: String): Call<ResponseBody>

    @GET
    fun leave(@Url url: String): Call<ResponseBody>
}