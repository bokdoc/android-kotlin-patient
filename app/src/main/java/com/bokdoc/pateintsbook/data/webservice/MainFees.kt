package com.bokdoc.pateintsbook.data.webservice

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.SurgeryFees
import com.squareup.moshi.Json

class MainFees {
    var amount: String? = null
    @Json(name = "currency_symbols")
    var currencySymbol: String? = null
    var label: String = ""
    var fixed_fees: SurgeryFees? = null
    var fees_from: SurgeryFees? = null
    var fees_to: SurgeryFees? = null
}

class MainFeesWithoutFixed {
    var amount: String = ""
    @Json(name = "currency_symbols")
    var currencySymbols = ""
    var fees_from = SurgeryFees()
    var fees_to = SurgeryFees()
    var label: String = ""
}