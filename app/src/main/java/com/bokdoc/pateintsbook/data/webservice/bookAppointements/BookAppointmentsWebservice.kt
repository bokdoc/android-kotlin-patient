package com.bokdoc.pateintsbook.data.webservice.bookAppointements

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class BookAppointmentsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun confirm(token: String, bookJson: String): Call<ResponseBody> {
        val api = webserviceManagerImpl.createService(BookAppointementsApis::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), bookJson)
        return api.confirm(requestBody)
    }

    fun addPromoCode(token: String, promoJson: String): Call<ResponseBody> {
        val api = webserviceManagerImpl.createService(BookAppointementsApis::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), promoJson)
        return api.addPromoCode(requestBody)
    }


}