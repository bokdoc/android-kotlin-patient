package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
import com.bokdoc.pateintsbook.data.webservice.models.Location

class SurgeriesRow() : Parcelable {
    var id: String = ""
    var surgeryName: String = ""
    var doctorName: String? = ""
    var gender: String? = ""
    var doctorPhoto: String = ""
    var doctorTitle: String? = ""

    var speciality: String = ""
    var nationalFees: MainFees? = null
    var interNationalFees: MainFees? = null
    var nationalFeesLable: String = ""
    var internationalFeesLable: String = ""
    var appointmentReservation: String = ""
    var feesCurrancy: String = ""
    var showUrl: String = ""
    var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()

    var location: Location = Location()

    var type: Int = 0

    var profileType = 1

    var bookButton: DynamicLinks? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        surgeryName = parcel.readString()
        doctorName = parcel.readString()
        gender = parcel.readString()
        doctorPhoto = parcel.readString()
        doctorTitle = parcel.readString()
        speciality = parcel.readString()
        nationalFeesLable = parcel.readString()
        appointmentReservation = parcel.readString()
        internationalFeesLable = parcel.readString()
        feesCurrancy = parcel.readString()
        showUrl = parcel.readString()
        nextAvailabilities = parcel.createTypedArrayList(NextAvailabilitiesParcelable)
        location = parcel.readParcelable(Location::class.java.classLoader)
        type = parcel.readInt()
        profileType = parcel.readInt()
        bookButton = parcel.readParcelable(DynamicLinks::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(surgeryName)
        parcel.writeString(doctorName)
        parcel.writeString(gender)
        parcel.writeString(doctorPhoto)
        parcel.writeString(doctorTitle)
        parcel.writeString(speciality)
        parcel.writeString(nationalFeesLable)
        parcel.writeString(appointmentReservation)
        parcel.writeString(internationalFeesLable)
        parcel.writeString(feesCurrancy)
        parcel.writeString(showUrl)
        parcel.writeTypedList(nextAvailabilities)
        parcel.writeParcelable(location, flags)
        parcel.writeInt(type)
        parcel.writeInt(profileType)
        parcel.writeParcelable(bookButton, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SurgeriesRow> {
        override fun createFromParcel(parcel: Parcel): SurgeriesRow {
            return SurgeriesRow(parcel)
        }

        override fun newArray(size: Int): Array<SurgeriesRow?> {
            return arrayOfNulls(size)
        }
    }


}