package com.bokdoc.pateintsbook.data.webservice.sortBy

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SortByApi {

    @GET(WebserviceConstants.SEARCH_PROFILES_URL+"/filter-options/sort_by")
    fun getSortByOptions(@Query("include")include:String,@Query("screen_type")screenType:String): Call<ResponseBody>

}