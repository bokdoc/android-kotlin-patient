package com.bokdoc.pateintsbook.data.webservice.autoComplete

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface AutoCompleteApi {

    @GET("$AUTO_COMPLETE_TEXT/profile")
    fun completeSpeciality(@Query("keyword") keyword: String, @Query("screen_type") screenType: String?): Call<ResponseBody>

    @GET("$AUTO_COMPLETE_TEXT/location")
    fun completeLocation(@Query("keyword") keyword: String): Call<ResponseBody>

    @GET("$AUTO_COMPLETE_TEXT/insurance")
    fun completeInsuranceCarrier(@Query("keyword") keyword: String): Call<ResponseBody>

    companion object {
        const val AUTO_COMPLETE_TEXT = "auto-complete"
    }
}