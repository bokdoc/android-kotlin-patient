package com.bokdoc.pateintsbook.data.models


class ImageTextRow(val image: Int, val text: String, var slug: String = "")