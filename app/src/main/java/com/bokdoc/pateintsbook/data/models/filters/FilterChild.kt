package com.bokdoc.pateintsbook.data.models.filters

class FilterChild(val name:String, val params:String, val unit:String="", var isSelected:Boolean=false,
                  var rating:Float = 0.0f,var ranges:Array<Int> = Array(0){0})