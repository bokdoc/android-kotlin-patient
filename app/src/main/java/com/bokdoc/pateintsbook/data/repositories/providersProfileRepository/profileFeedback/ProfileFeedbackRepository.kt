package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileFeedback

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileFeedback.ProfileFeedbackWebservice
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFeedbackRepository(val appContext: Context,
                                val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                                val profileFeedbackWebservice: ProfileFeedbackWebservice) {


    fun getFeedback(id: String): LiveData<DataResource<IFeedback>> {
        val liveData = MutableLiveData<DataResource<IFeedback>>()
        val dataResource = DataResource<IFeedback>()
        profileFeedbackWebservice.getFeedback(sharedPreferencesManagerImpl.getData(appContext.getString(R.string.token), ""), id).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    val profile = Parser.parseJson(response.body()?.string()!!, arrayOf(HealthProfileProvidersResource::class.java,
                            HealthProvidersProfileType::class.java, FeedbackResource::class.java,
                            FeedbackPersonaResource::class.java))[0] as HealthProfileProvidersResource
                    dataResource.data = profile.feedback.get(profile.document)
                    liveData.value = dataResource
                } else {
                    dataResource.message = response?.errorBody()!!
                    liveData.value = dataResource
                }
            }

        })
        return liveData
    }
}