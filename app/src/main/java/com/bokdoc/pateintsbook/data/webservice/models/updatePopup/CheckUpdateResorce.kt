package com.bokdoc.pateintsbook.data.webservice.models.updatePopup

import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "update")
class CheckUpdateResorce : Resource() {
    var version: String = ""
    var device_os: String = ""
    var fcm_token = ""
}