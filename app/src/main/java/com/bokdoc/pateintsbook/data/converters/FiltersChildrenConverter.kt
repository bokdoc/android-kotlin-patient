package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.filters.FilterChild
import com.bokdoc.pateintsbook.data.webservice.models.FilterOption
import com.bokdoc.pateintsbook.data.webservice.models.FilterOptionUnit
import com.bokdoc.pateintsbook.data.webservice.models.FiltersOptions
import com.bokdoc.pateintsbook.ui.filters.FiltersSectionHeader

object FiltersChildrenConverter {

    fun convert(filtersOptions: List<FiltersOptions>):List<FiltersSectionHeader>{
        val filters = ArrayList<FiltersSectionHeader>()
        filtersOptions.forEach {
            filters.add(FiltersSectionHeader(it.header, getChildren(it),it.dataType,it.key))
        }
        return filters
    }

    private fun getChildren(filtersOptions: FiltersOptions):List<FilterChild>{

        when(filtersOptions.slug){
            FiltersOptions.SPECIALITIES_SLUG -> return getOptions(filtersOptions.specialities.get(filtersOptions.document))
            FiltersOptions.YEARS_SLUG->{
                val filterOption =
                        getOptions(filtersOptions.yearsExperience.get(filtersOptions.document),
                                true)
                return filterOption
            }
            FiltersOptions.FEES_SLUG -> return getOptions(filtersOptions.fees.get(filtersOptions.document))
            FiltersOptions.DATES_SLUG -> return getOptions(filtersOptions.dates.get(filtersOptions.document))
            FiltersOptions.FREE_APPOINTMENT_SLUG -> return getOptions(filtersOptions.freeAppointment.get(filtersOptions.document))
            FiltersOptions.GENDERS -> return getOptions(filtersOptions.genders.get(filtersOptions.document))
            FiltersOptions.INSURANCE_CARRIERS_SLUG -> return getOptions(filtersOptions.insuranceCarriers.get(filtersOptions.document))
            FiltersOptions.IS_HOME_VISIT_SLUG-> return getOptions(filtersOptions.isHomeVisit.get(filtersOptions.document))
            FiltersOptions.LANGUAGES_SLUG -> return getOptions(filtersOptions.languages.get(filtersOptions.document))
            FiltersOptions.TIMES_SLUG -> return getOptions(filtersOptions.times.get(filtersOptions.document))
            FiltersOptions.RATING_SLUG-> return arrayListOf(FilterChild("","","",false,0.0f,Array(0){0}))
            FiltersOptions.MAP_SLUG -> return getOptions(filtersOptions.map.get(filtersOptions.document))
            FiltersOptions.PAYMENT_TYPES_SLUG -> return getOptions(filtersOptions.paymentTypes.get(filtersOptions.document))
            FiltersOptions.PROFILE_TYPES -> return getOptions(filtersOptions.profileTypes.get(filtersOptions.document))
            FiltersOptions.CLINIC_APPOINTMENT_RESERVATION -> {
                if(filtersOptions.clinicAppointmentReservation!=null) {
                    return getOptions(filtersOptions.clinicAppointmentReservation!!.get(filtersOptions.document))
                }else{
                    return ArrayList()
                }
            }
            else -> return ArrayList()
        }
    }

    private fun getOptions(filtersOptions:List<FilterOption>,isAddPlusToMax:Boolean=false):List<FilterChild>{
        val filtersChildren = ArrayList<FilterChild>()
        if(filtersOptions[0] is FilterOptionUnit){
            return getOptionsWithUnit(filtersOptions as List<FilterOptionUnit>,isAddPlusToMax)
        }

        filtersOptions.forEach {
                filtersChildren.add(FilterChild(it.name,it.params))
            }

        return filtersChildren
    }

    private fun getOptionsWithUnit(filtersOptions: List<FilterOptionUnit>,isAddPlusToMax:Boolean=false):List<FilterChild>{
        var name = ""
        var params = ""
        var unit = ""

        val size = filtersOptions.size
        (0 until size).forEach {
            if(it == 0){
                name+=filtersOptions[it].name
                params+=filtersOptions[it].params
                unit+=filtersOptions[it].unit
            }   else{
                if(isAddPlusToMax) {
                    name += ",+" + filtersOptions[it].name
                }else{
                    name += "," + filtersOptions[it].name
                }
                params+=","+filtersOptions[it].params
                unit+=","+filtersOptions[it].unit
            }
        }
        return listOf(FilterChild(name,params,unit))
    }


}