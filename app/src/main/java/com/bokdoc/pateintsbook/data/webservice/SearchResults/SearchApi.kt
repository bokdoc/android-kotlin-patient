package com.bokdoc.pateintsbook.data.webservice.SearchResults

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface SearchApi {

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/appointments")
    fun searchAppointments(@Query("") query: String = "", @Query("page") page: Int): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/appointments")
    fun searchAppointments(@QueryMap queries: Map<String, String>): Call<ResponseBody>


    @Headers("Content-Type: application/json")
    @GET
    fun search(@Url url:String): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/surgeries")
    fun searchSurgery(@Query("") query: String = "", @Query("page") page: Int): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/surgeries")
    fun searchSurgery(@QueryMap queries: Map<String, String>): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/consultings")
    fun searchConsulting(@Query("query") query: String = "", @Query("page") page: Int): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET(WebserviceConstants.SEARCH_PROFILES_URL + "/consultings")
    fun searchConsulting(@QueryMap queries: Map<String, String>): Call<ResponseBody>

}