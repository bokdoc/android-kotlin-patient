package com.bokdoc.pateintsbook.data.webservice

import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val token:String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain?.request()
        val builder = original?.newBuilder()?.header("Authorization", "Bearer $token")
        return chain!!.proceed(builder?.build())
    }


    companion object {
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer "
    }
}