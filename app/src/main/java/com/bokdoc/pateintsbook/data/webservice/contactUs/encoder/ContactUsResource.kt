package com.bokdoc.pateintsbook.data.webservice.contactUs.encoder

import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class ContactUsResource : Resource() {
    lateinit var name: String
    lateinit var message: String
    lateinit var email: String
}