package com.bokdoc.pateintsbook.data.webservice.models.updatePopup

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "mobileApplicationAction")
class CheckUpdate() : Resource(), Parcelable {
    var title: String = ""
    var conent: String = ""
    @Json(name = "image_url")
    var imageUrl: String = ""
    var buttons: List<UpdateButton> = ArrayList()

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()
        conent = parcel.readString()
        imageUrl = parcel.readString()
        buttons = parcel.createTypedArrayList(UpdateButton)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(conent)
        parcel.writeString(imageUrl)
        parcel.writeTypedList(buttons)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CheckUpdate> {
        override fun createFromParcel(parcel: Parcel): CheckUpdate {
            return CheckUpdate(parcel)
        }

        override fun newArray(size: Int): Array<CheckUpdate?> {
            return arrayOfNulls(size)
        }
    }


}

class UpdateButton() : Parcelable {
    var action: String = ""
    var url: String = ""
    var label: String = ""

    constructor(parcel: Parcel) : this() {
        action = parcel.readString()
        url = parcel.readString()
        label = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(action)
        parcel.writeString(url)
        parcel.writeString(label)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpdateButton> {
        override fun createFromParcel(parcel: Parcel): UpdateButton {
            return UpdateButton(parcel)
        }

        override fun newArray(size: Int): Array<UpdateButton?> {
            return arrayOfNulls(size)
        }
    }


}