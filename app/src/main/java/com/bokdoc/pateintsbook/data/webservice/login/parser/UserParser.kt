package com.bokdoc.pateintsbook.data.webservice.login.parser

import com.bokdoc.pateintsbook.data.webservice.models.*
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ResourceAdapterFactory

object UserParser {
    fun parse(json: String): UserResponse {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(UserResponse::class.java)
                .add(Language::class.java)
                .add(Currency::class.java)
                .add(Country::class.java)
                .add(UnknownResource::class.java)
                .build()

        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()
        val userResponse = moshi.adapter(Document::class.java).fromJson(json)
        try {
            if(userResponse!=null)
                return userResponse.asObjectDocument<UserResponse>().get()
        }catch (e:Throwable){

        }


        return UserResponse()
    }
}