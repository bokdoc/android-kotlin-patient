package com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles

import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.DepartmentRow

object DepartmentsConverter {

    fun convert(allClinicsResource: HealthProviderProfile): List<DepartmentRow> {
        val departments = ArrayList<DepartmentRow>()
        allClinicsResource.departments?.get(allClinicsResource.document)?.forEach {
            var department = DepartmentRow()
            department.id = it.id
            department.name = it.name
            department.description = it.description
            department.doctorCount = it.doctors_count
            department.showUrl = it.show_url
            department.bookButton = it.bookButton
            departments.add(department)
        }

        return departments
    }
}

