package com.bokdoc.pateintsbook.data.webservice.forgetPassword

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface ForgetPasswordApi {

    @POST( WebserviceConstants.AUTH + "/" + WebserviceConstants.PASSWORD)
    fun passwordMobile(@Body requestBody: RequestBody): Call<ResponseBody>

    @POST(  WebserviceConstants.AUTH + "/" + WebserviceConstants.VERIFY_CODE)
    fun passwordMobileVerify(@Body requestBody: RequestBody): Call<ResponseBody>

    @POST(WebserviceConstants.AUTH + "/" + WebserviceConstants.PASSWORD_RECOVER)
    fun passwordRecover(@Body requestBody: RequestBody): Call<ResponseBody>
}