package com.bokdoc.pateintsbook.data.converters

import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.webservice.models.LookupsResource
import moe.banana.jsonapi2.Resource

object LookupsConverter {
    fun getLookups(lookups: List<LookupsResource>):List<LookupsType>{
        lookups.forEach {
            when {
                it.specialties!=null -> return convert(it.specialties!!.get(it.document))
                it.diseases!=null -> return convert(it.diseases!!.get(it.document))
                it.medications!=null -> return convert(it.medications!!.get(it.document))
                it.countries!=null -> return convert(it.countries!!.get(it.document))
                it.currencies!=null -> return convert(it.currencies!!.get(it.document))
                it.languages!=null -> return convert(it.languages!!.get(it.document))
            }
        }
        return ArrayList()
    }


    fun convert(lookupsResources:List<LookupsType>):List<LookupsType>{
        val lookups = ArrayList<LookUpsParcelable>()

        lookupsResources.forEach {
            lookups.add(LookUpsParcelable().apply {
                idLookup = it.idLookup
                name = it.name
            })
        }

        return lookups
    }
}