package com.bokdoc.pateintsbook.data.models.healthProviders.services

import com.bokdoc.pateintsbook.data.models.healthProviders.fees.Fees
import com.bokdoc.pateintsbook.data.models.healthProviders.fees.FeesImpl

interface Service {
    val name:String
    val summary:String
    val picture:String
    val serviceType:String
    var feesService: Fees?
    get() {return FeesImpl()
    }
    set(value) {
        feesService = value
    }

    companion object {
        const val SCAN_TYPE = "scan"
        const val TEST_TYPE = "test"
        const val SURGERY_TYPE = "surgery"
    }
}