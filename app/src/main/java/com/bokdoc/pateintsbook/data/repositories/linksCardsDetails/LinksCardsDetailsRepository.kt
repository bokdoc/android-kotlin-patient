package com.bokdoc.pateintsbook.data.repositories.linksCardsDetails

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ClinicsConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ConsultingConverter
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.SurgeriesConverter
import com.bokdoc.pateintsbook.data.converters.LinkCardDetailsConverter
import com.bokdoc.pateintsbook.data.models.*
import com.bokdoc.pateintsbook.data.models.Department
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.CustomResponseConverter
import com.bokdoc.pateintsbook.data.webservice.linksCardsDetails.LinksCardsDetailsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ClinicRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.SurgeriesRow
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LinksCardsDetailsRepository(private val context: Context,
                                  private val sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                                  private val linksCardsDetailsWebservice: LinksCardsDetailsWebservice) {
   // val liveData = MutableLiveData<DataResource<LinksCardsDetails>>()

    fun getDetails(url: String): MutableLiveData<DataResource<LinksCardsDetails>> {
        val liveData = MutableLiveData<DataResource<LinksCardsDetails>>()
        /*
        linksCardsDetailsWebservice.getDetails(sharedPreferencesManagerImpl.getData(context.getString(R.string.token), "")
                , url).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.status = DataResource.FAIL
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    dataResource.status = DataResource.SUCCESS
                    try {
                        val dataResources =
                                Parser.parseJson(response.body()!!.string(), )
                        dataResource.data = LinkCardDetailsConverter.convert(dataResources as List<LinksCardsDetailsResource>)
                    } catch (e: Exception) {
                        dataResource.status = DataResource.INTERNAL_SERVER_ERROR
                    }
                }
                liveData.value = dataResource
            }

        })
        */

        linksCardsDetailsWebservice.getDetails(sharedPreferencesManagerImpl.getData(context.getString(R.string.token), "")
                , url).enqueue(getCustomDetailsResponse(liveData))

        return liveData
    }

    fun getDetails(url: String,query:String=""): MutableLiveData<DataResource<LinksCardsDetails>> {
        val liveData = MutableLiveData<DataResource<LinksCardsDetails>>()
        /*
        linksCardsDetailsWebservice.getDetails(sharedPreferencesManagerImpl.getData(context.getString(R.string.token), "")
                , url).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                dataResource.status = DataResource.FAIL
                dataResource.message = t?.message!!
                liveData.value = dataResource
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.body() != null) {
                    dataResource.status = DataResource.SUCCESS
                    try {
                        val dataResources =
                                Parser.parseJson(response.body()!!.string(), )
                        dataResource.data = LinkCardDetailsConverter.convert(dataResources as List<LinksCardsDetailsResource>)
                    } catch (e: Exception) {
                        dataResource.status = DataResource.INTERNAL_SERVER_ERROR
                    }
                }
                liveData.value = dataResource
            }

        })
        */

        linksCardsDetailsWebservice.getDetailsWithDiscountId(sharedPreferencesManagerImpl.getData(context.getString(R.string.token), "")
                , url,query).enqueue(getCustomDetailsResponse(liveData))

        return liveData
    }

    private fun getCustomDetailsResponse(liveData:MutableLiveData<DataResource<LinksCardsDetails>>):CustomResponseConverter<LinksCardsDetailsResource,LinksCardsDetails>{
        return CustomResponseConverter({
            liveData.value = it
        },arrayOf(LinksCardsDetailsResource::class.java,
                ClinicsCardsDetails::class.java, SurgeryCardDetails::class.java, HomeVisitsService::class.java,
                ClinicsSections::class.java,
                FilterRelations::class.java, SectionFilters::class.java,
                SectionFilter::class.java,
                NextAvailabilityResource::class.java,
                SectionData::class.java,
                Speciality::class.java,
                ProfileType::class.java,
                ProfileTitle::class.java,
                BokDocServicesSection::class.java,
                ConsultingService::class.java, Department::class.java),LinkCardDetailsConverter::convert)
    }


    fun getClinicsRows(linksCardsDetails: LinksCardsDetails): List<ClinicRow> {
        return ClinicsConverter.convert(linksCardsDetails)
    }

    fun getSurgeriesRows(linksCardsDetails: LinksCardsDetails): List<SurgeriesRow> {
        return SurgeriesConverter.convert(linksCardsDetails)
    }

    fun getConsultingRows(linksCardsDetails: LinksCardsDetails): List<ConsultingRow> {
        return ConsultingConverter.convert(linksCardsDetails)
    }


}