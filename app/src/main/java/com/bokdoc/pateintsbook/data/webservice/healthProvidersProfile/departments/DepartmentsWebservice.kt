package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.departments

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class DepartmentsWebservice(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getDepartments(id: String, token: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(DepartmentsApi::class.java, token).getDepartments(id)
    }
}