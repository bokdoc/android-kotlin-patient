package com.bokdoc.pateintsbook.data.webservice.twillio

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class TwillioWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getTwillioToken(id: String, token: String): Call<ResponseBody> {
        val twillioTokenApi = webserviceManagerImpl.createService(TwillioApi::class.java, token)
        return twillioTokenApi.getTwillioToken(id)
    }

    fun leaveTwillio(url: String, token: String): Call<ResponseBody> {
        val twillioTokenApi = webserviceManagerImpl.createService(TwillioApi::class.java, token)
        return twillioTokenApi.leave(url)
    }

}