package com.bokdoc.pateintsbook.data.webservice.models.autoComplete

import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.JsonApi

@JsonApi(type = "autoComplete")
class AutoCompleteResource : moe.banana.jsonapi2.Resource() {

    lateinit var header: String
    lateinit var slug: String
    var specialities: HasMany<Speciality>? = null
    var services: HasMany<Service>? = null
    var doctors: HasMany<Doctor>? = null
    var links: HasMany<Link>? = null
    @Json(name = "radiology")
    var rads: HasMany<Rad>? = null
    var insuranceCarriers: HasMany<InsuranceCarrierPlans>? = null
    var locations: HasMany<Location>? = null
    var hospitals: HasMany<Hospital>? = null

    var tests: HasMany<Test>? = null

    var laboratories: HasMany<Lab>? = null
    var scans: HasMany<Scan>? = null

    var surgeries:HasMany<Surgery>?=null


    companion object {
        const val SERVICES_HEADER = "services"
        const val SPECIALITIES_SLUG = "specialities"
        const val DOCTORS_SLUG = "doctors"
        const val RADIOLOGY = "radiology"
        const val LINKS = "links"
        const val INSURANCE_CARRIER_PLANS = "insuranceCarriers"
        const val LOCATIONS = "locations"
        const val HOSPITALS = "hospitals"
        const val LABORATORIES = "laboratories"
        const val TEST = "tests"
        const val SCANS = "scans"
        const val SURGERIES_SLUG = "surgeries"
    }
}

@JsonApi(type = "speciality")
class Speciality : ResourceWithNameAttr()

@JsonApi(type = "service")
class Service : ResourceWithNameAttr()

@JsonApi(type = "doctor")
class Doctor : DoctorAndRadResource()

@JsonApi(type = "rad")
class Rad : DoctorAndRadResource()

@JsonApi(type = "link")
class Link : ResourceWithNameAttr()

@JsonApi(type = "insuranceCarrier")
class InsuranceCarrierPlans : ResourceWithNameAttr()

@JsonApi(type = "location")
class Location : ResourceWithNameAttr()

@JsonApi(type = "hospital")
class Hospital : DoctorAndRadResource()

@JsonApi(type = "scan")
class Scan : ResourceWithNameAttr()

@JsonApi(type = "test")
class Test : ResourceWithNameAttr()

@JsonApi(type = "lab")
class Lab : DoctorAndRadResource()

@JsonApi(type = "surgery")
class Surgery : ResourceWithNameAttr()