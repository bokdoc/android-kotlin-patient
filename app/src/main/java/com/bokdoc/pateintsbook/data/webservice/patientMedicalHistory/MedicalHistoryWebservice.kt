package com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.encoder.IdEncoder
import com.bokdoc.pateintsbook.data.webservice.utils.RequestBodyCreator
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

abstract class MedicalHistoryWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {

    private var medicalHistoryActions: MedicalHistoryActions? = null
    abstract fun getType():String

    fun add(id: String, token: String): Call<ResponseBody> {
        getMedicationsActionsApis(token)
        return medicalHistoryActions!!.add(getType(),RequestBodyCreator.createJsonRequest(IdEncoder.encode(id)))
    }

    fun delete(id: String, token: String): Call<ResponseBody> {
        getMedicationsActionsApis(token)
        return medicalHistoryActions!!.remove(getType(),id)
    }

    fun get(id: String, token: String): Call<ResponseBody> {
        getMedicationsActionsApis(token)
        return medicalHistoryActions!!.get(getType(),id)
    }

    fun getMedicationsActionsApis(token: String): MedicalHistoryActions {
        if (medicalHistoryActions == null)
            medicalHistoryActions =
                    webserviceManagerImpl.createService(MedicalHistoryActions::class.java, token)

        return medicalHistoryActions as MedicalHistoryActions
    }
}