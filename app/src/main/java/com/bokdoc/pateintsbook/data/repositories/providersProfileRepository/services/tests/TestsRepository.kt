package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.tests

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Test
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.ServicesRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.tests.TestsWebservice

class TestsRepository(appContext: Context,sharedPreferencesManager: SharedPreferencesManager,
                      testsWebservice: TestsWebservice):ServicesRepository(appContext,sharedPreferencesManager,testsWebservice) {

    fun getTests(id:String):LiveData<DataResource<Test>>{
        val liveData = MutableLiveData<DataResource<Test>>()
        getServices(id).observeForever {
            liveData.value = it as DataResource<Test>
        }
        return liveData
    }
}