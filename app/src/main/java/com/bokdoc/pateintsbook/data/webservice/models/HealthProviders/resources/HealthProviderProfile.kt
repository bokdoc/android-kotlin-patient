package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources

import com.bokdoc.pateintsbook.data.models.DateTime
import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.media.IMedia
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.squareup.moshi.Json
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource

@JsonApi(type = "profile")
class HealthProviderProfile : Resource() {
    //attributes
    var name: String = ""
    var overview: String = ""
    var location: Location = Location()
    var rating: Float = 0.0f
    var picture: Picture = Picture()
    var gender: String = ""
    @Json(name = "main_speciality")
    var mainSpeciality: String = ""

    @Json(name = "speciality_title")
    var specialityTitle: String = ""

    @Json(name = "departments_count")
    var departmentsCount: String = ""
    @Json(name = "doctors_count")
    var doctorsCount: String = ""
    @Json(name = "clinics_count")
    var clinicsCount: String = ""
    @Json(name = "likes_count")
    var likesCount: String = ""

    @Json(name = "beds_number")
    var bedsNumber: String = ""
    @Json(name = "reviews_count")
    var reviewsCount: String = ""
    @Json(name = "birth_date")
    var birthDate: String = ""
    @Json(name = "ambulance_count")
    var ambulanceCount: String = ""

    @Json(name = "online_consultation_fees")
    var onlineConsultationFees: Fees = Fees()

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null

    /*
    @Json(name = "consulting_book_button")
    var consultingBook: DynamicLinks = DynamicLinks()
    */

    //relations
    lateinit var profileType: HasOne<ProfileType>
    var clinics: HasMany<Clinic>? = null
    var surgeries: HasMany<Surgeries>? = null
    var consulting: HasMany<Consulting>? = null
    var insuranceCarriers: HasMany<InsuranceCarrierResource>? = null
    var doctors: HasMany<HospitalDoctor> = HasMany()
    var profileTitle: HasOne<ProfileTitle>? = null
    var singleDepartment: HasOne<Departments> = HasOne()
    var accommodations: HasMany<Accommodation>? = null
    var accreditation: HasMany<Accreditation>? = null
    var certifications: HasMany<Certification>? = null
    var educations: HasMany<Educations>? = null
    var memberships: HasMany<Membership>? = null
    var publications: HasMany<Publication>? = null
    var languagesSpoken: HasMany<LanguageSpoken>? = null
    var review: HasMany<FeedbackResource>? = null
    var paymentOptions: HasMany<PaymentOptions>? = null
    var departments: HasMany<Departments>? = null


}

@JsonApi(type = "review")
class FeedbackResource : Resource() {
    var rate = 0
    var content = ""
    var date_time = DateTime()
    @Json(name = "persona")
    var personaResource: HasOne<FeedbackPersonaResource>? = null
}

@JsonApi(type = "persona")
class FeedbackPersonaResource : Resource() {
    val name: String? = null
    var picture: Picture = Picture()
}


class Fees {
    var amount: String = ""
    var currency_symbols: String = ""
    @Json(name = "before_discount")
    var oldAmount: String? = ""
}

@JsonApi(type = "languageSpoken")
class LanguageSpoken : Resource() {
    var name: String = ""
    var code: String = "l"
}

@JsonApi(type = "certification")
class Certification : Resource() {
    var title: String = ""
    var content: String = ""
    var years: String = ""
    @Json(name = "university_id")
    var universityId: String = ""
}

@JsonApi(type = "education")
class Educations : Resource() {
    var content: String = ""
    var years: String = ""
    @Json(name = "university_id")
    var universityId: String = ""
}


@JsonApi(type = "membership")
class Membership : Resource() {
    var content: String = ""
    var years: String = ""
    var title: String = ""

    @Json(name = "university_id")
    var universityId: String = ""
}


@JsonApi(type = "insuranceCarrier")
class InsuranceCarrierResource : Resource() {
    //attributes
    var title: String = ""
    var description: String = ""

}

@JsonApi(type = "accommodation")
class Accommodation : Resource() {
    @Json(name = "patient_room")
    var patientRoom: String = ""
    @Json(name = "accompanied_persons")
    var accompaniedPersons: String = ""
    var meals: String = ""
    var rooms: String = ""

}

@JsonApi(type = "accreditation")
class Accreditation : Resource() {
    var icon: String = ""
    var slug: String = ""
    var title: String = ""
    var description: String = ""

}

@JsonApi(type = "profileTitle")
class ProfileTitle : Resource() {
    var name: String = ""
    var description: String = ""
}

@JsonApi(type = "department")
class Departments : Resource() {
    var name: String = ""
    var doctors_count: String = ""
    var description: String = ""
    var show_url: String = ""

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null

    var doctors: HasMany<HealthProviderProfile> = HasMany()
    var media: HasMany<AttachmentMediaResource>? = null

}

@JsonApi(type = "clinic")
class Clinic : Resource() {
    //attributes
    lateinit var name: String
    lateinit var fees: Fees
    @Json(name = "main_fees")
    var mainFees = MainFees()
    @Json(name = "sub_main_fees")
    var subMainFees = MainFees()
    var location: Location = Location()
    @Json(name = "show_url")
    lateinit var showUrl: String
    var is_appointment_reservation: String = ""

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null
    //relations
    var parent: HasOne<HealthProviderProfile>? = null
    var profile: HasOne<HealthProviderProfile>? = null
    var speciality: HasOne<Speciality>? = null
    lateinit var workingTimes: HasMany<WorkingTimes>
    lateinit var appointments: HasMany<Appointments>
    var media: HasMany<ProfileAttachmentMediaResource>? = null
    //profileMedia
    var nextAvailabilities: HasMany<NextAvailabilityResource>? = null
    //relations

}

@JsonApi(type = "profileSurgery")
class Surgeries : Resource() {
    //attributes
    lateinit var name: String
    @Json(name = "show_url")
    lateinit var showUrl: String
    @Json(name = "main_fees")
    var mainFees = MainFees()
    @Json(name = "sub_main_fees")
    var subMainFees = MainFees()
    var is_appointment_reservation: String = ""

    var picture: Picture = Picture()

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null
    var location: Location = Location()
    var nextAvailabilities: HasMany<NextAvailabilityResource>? = null
    //relations
    var parent: HasOne<HealthProviderProfile>? = null

    var doctor: HasOne<HealthProviderProfile>? = null
    var speciality: HasOne<Speciality>? = null
    var profile: HasOne<HealthProviderProfile>? = null
}

@JsonApi(type = "profileMedia")
class ProfileAttachmentMediaResource() : Resource(), IMedia {
    @Transient
    override var mediaId: String = ""
        get() {
            return id
        }
    @Transient
    override var name: String = ""
    @Json(name = "media_type")
    override var mediaType: String = ""
    @Json(name = "media_url")
    override var url: String = ""
}

@JsonApi(type = "hospitalDoctor")
class HospitalDoctor : Resource() {
    //attributes
    var name: String = ""
    var overview: String = ""
    var location: Location = Location()
    var rating: Float = 0.0f
    var picture: Picture = Picture()
    var gender: String = ""
    @Json(name = "main_speciality")
    var mainSpeciality: String = ""
    @Json(name = "departments_count")
    var departmentsCount: String = ""
    @Json(name = "doctors_count")
    var doctorsCount: String = ""
    @Json(name = "clinics_count")
    var clinicsCount: String = ""
    @Json(name = "likes_count")
    var likesCount: String = ""

    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null
    @Json(name = "beds_number")
    var bedsNumber: String = ""
    @Json(name = "reviews_count")
    var reviewsCount: String = ""
    @Json(name = "birth_date")
    var birthDate: String = ""

    //relations
    lateinit var profileType: HasOne<ProfileType>
    var profileTitle: HasOne<ProfileTitle>? = null

    var languagesSpoken: HasMany<LanguageSpoken>? = null

}

@JsonApi(type = "consulting")
class Consulting : Resource() {
    //attributes
    var name: String? = null
    var duration: String = ""
    var fees: Fees? = null
    @Json(name = "main_fees")
    var mainFees = MainFees()
    @Json(name = "sub_main_fees")
    var subMainFees = MainFees()
    var location: Location = Location()
    @Json(name = "book_button")
    var bookButton: DynamicLinks? = null
    //relations
    lateinit var parent: HasOne<HealthProviderProfile>
    var profile: HasOne<HealthProviderProfile>? = null
    lateinit var speciality: HasOne<Speciality>
    lateinit var contacts: HasMany<Contacts>
    lateinit var workingTimes: HasMany<WorkingTimes>
    var nextAvailabilities: HasMany<NextAvailabilityResource>? = null

}

@JsonApi(type = "workingTimes")
class WorkingTimes : Resource() {
    var date_day: String = ""
    var time_from: String = ""
    var time_to: String = ""
}

@JsonApi(type = "appointment")
class Appointments : Resource() {
    lateinit var fees: Fees

}

@JsonApi(type = "profileType")
class ProfileType : Resource() {
    lateinit var name: String
    lateinit var description: String

}

@JsonApi(type = "contact")
class Contacts : Resource() {
    @Json(name = "is_primary")
    var isPrimary: Boolean = false
    lateinit var mobile_number: String
    lateinit var name: String
    lateinit var mobile_part1: String
    lateinit var mobile_part2: String

}

@JsonApi(type = "paymentOption")
class PaymentOptions : Resource() {
    lateinit var name: String
    lateinit var description: String
}

class SurgeryFees {
    var amount: String? = null
    var currency_symbols: String? = null
    @Json(name = "before_discount")
    var before_discount: String? = ""

}