package com.bokdoc.pateintsbook.data.webservice.addFcm

import com.bokdoc.pateintsbook.data.webservice.models.AddFcmResource
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object AddFcmEncoder {

    fun getJson(fcmToken: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<AddFcmResource>()
        val addFcm = AddFcmResource()
        addFcm.id = ""
        addFcm.fcm_token = fcmToken
        addFcm.device_os = "android"
        document.set(addFcm)

        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(AddFcmResource::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}