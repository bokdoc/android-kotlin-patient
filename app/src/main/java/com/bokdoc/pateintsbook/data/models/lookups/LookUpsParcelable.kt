package com.bokdoc.pateintsbook.data.models.lookups

import android.os.Parcel
import android.os.Parcelable

open class LookUpsParcelable() : ILookupsParcelable {
    override var name: String = ""
    override var idLookup: String = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        idLookup = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(idLookup)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LookUpsParcelable> {
        override fun createFromParcel(parcel: Parcel): LookUpsParcelable {
            return LookUpsParcelable(parcel)
        }

        override fun newArray(size: Int): Array<LookUpsParcelable?> {
            return arrayOfNulls(size)
        }
    }


}