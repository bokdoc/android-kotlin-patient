package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileFeedback

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileFeedbackApi {
    @GET(WebserviceConstants.PROFILES_WITH_SLASH+"{id}?include=review.persona:type(patient)")
    fun getFeedback(@Path("id")id:String): Call<ResponseBody>
}