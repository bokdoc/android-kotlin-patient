package com.bokdoc.pateintsbook.data.webservice

import android.net.Uri
import android.text.TextUtils
import com.bokdoc.pateintsbook.data.repositories.SearchResults.SearchResultsRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit


open class WebserviceManagerImpl : WebServiceManger {
    private lateinit var retrofit: Retrofit
    private var retrofitBuilder: Retrofit.Builder = Retrofit.Builder()
    private var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private var authenticationInterceptor: AuthenticationInterceptor? = null

    constructor() {
        retrofitBuilder.baseUrl(BASE_URL)
        val contentTypeInterceptor = AcceptResponseInterceptor("application/json")

        if (!httpClient.interceptors().contains(contentTypeInterceptor)) {
            httpClient.addInterceptor(contentTypeInterceptor)
        }
    }

    /*
    override fun addConverterFactory(jsonConverterFactory: JSONConverterFactory){
        retrofitBuilder.addConverterFactory(jsonConverterFactory)
    }
    */
    override fun <S> createService(serviceClass: Class<S>, token: String): S {
        if (!TextUtils.isEmpty(token)) {
            authenticationInterceptor = AuthenticationInterceptor(token)
            if (!httpClient.interceptors().contains(authenticationInterceptor)) {
                httpClient.addInterceptor(authenticationInterceptor)
            }
        }

        retrofitBuilder.client(httpClient.build())
        retrofit = retrofitBuilder.build()
        return retrofit.create(serviceClass)
    }

    override fun <S> createService(serviceClass: Class<S>): S {
        removeToken()
        retrofitBuilder.client(httpClient.build())

        retrofit = retrofitBuilder.build()

        return retrofit.create(serviceClass)
    }

    private fun removeToken() {
        if (authenticationInterceptor != null && httpClient.interceptors().contains(authenticationInterceptor))
            httpClient.interceptors().remove(authenticationInterceptor)
    }

    fun isStaging(): Boolean {
        return BASE_URL.contains("staging", true)

    }

    fun formatBaseUrl(country: String, language: String, currency: String) {
        val builder = Uri.Builder()
        builder.scheme("https")
                .authority("apis.bokdoc.com")
                .appendPath("api")
                .appendPath("v1")
                .appendPath(country)
                .appendPath(language)
                .appendPath(currency)
                .appendPath("")

        BASE_URL = builder.build().toString()
        retrofitBuilder.baseUrl(BASE_URL)
    }

    fun formatBaseUrlWithQueries(paths: Array<String>, queries: Map<String, String>): String {
        val builder = Uri.Builder()
        val languagePaths = BASE_URL.split("/")
        builder.scheme("https")
                .authority("apis.bokdoc.com")
                .appendPath("api")
                .appendPath("v1")
                .appendPath(languagePaths[5])
                .appendPath(languagePaths[6])
                .appendPath(languagePaths[7])

        paths.forEach {
            builder.appendPath(it)
        }

        queries.forEach {
            if ((it.key == "" && it.value.isNotEmpty()) || it.key.contains(SearchResultsRepository.IGNORED_QUERY_KEY)) {
                builder.encodedQuery(it.value)
            } else {
                builder.appendQueryParameter(it.key, it.value)
            }
        }
        return builder.build().toString()
    }


    companion object {
        private var webserviceManagerImpl: WebserviceManagerImpl? = null
        const val SERVER_CODE_200 = 200
        const val SERVER_CODE_500 = 500
        const val SERVER_CODE_422 = 422

        fun instance(): WebserviceManagerImpl {
            if (webserviceManagerImpl == null) {
                webserviceManagerImpl = WebserviceManagerImpl()
            }
            return webserviceManagerImpl!!
        }

        var BASE_URL = "https://apis.bokdoc.com/api/v1/eg/en/egp/"

    }
}