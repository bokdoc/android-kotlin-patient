package com.bokdoc.pateintsbook.data.models.meta

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FilterData() :Parcelable {

    @SerializedName("key")
    @Expose
    var key: Int = 0;
    @SerializedName("value")
    @Expose
    var value: String = "";

    constructor(parcel: Parcel) : this() {
        key = parcel.readInt()
        value = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(key)
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilterData> {
        override fun createFromParcel(parcel: Parcel): FilterData {
            return FilterData(parcel)
        }

        override fun newArray(size: Int): Array<FilterData?> {
            return arrayOfNulls(size)
        }
    }
}