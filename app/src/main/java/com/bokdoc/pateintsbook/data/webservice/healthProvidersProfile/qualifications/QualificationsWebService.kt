package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.qualifications

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class QualificationsWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getQualificationsDetails(token: String, id: String, query: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token).getProfileSection(id, query)
    }

    companion object {
        const val EDUCATIONS_QUERY = "educations"
        const val SPECIALITIES_QUERY = "specialities"
        const val CERTIFICATIONS_QUERY = "certifications"
        const val MEMBERSHIPS_QUERY = "memberships"
        const val PUBLICATIONS_QUERY = "publications"
        const val SPECIAL_WORDS_QUERY = "specialWords"
        const val ACCREDITION_QUERY = "accreditation"
    }

}