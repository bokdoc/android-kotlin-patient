package com.bokdoc.pateintsbook.data.models.media

import android.os.Parcelable

interface IMediaParcelable: IMedia,Parcelable