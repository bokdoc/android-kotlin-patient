package com.bokdoc.pateintsbook.data.repositories.bookAppointment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.bookAppointement.BookInfo
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.promo.PromoCode
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.bookAppointements.BookAppointmentsWebservice
import com.bokdoc.pateintsbook.data.webservice.bookAppointements.encoder.AddPromoEncoder
import com.bokdoc.pateintsbook.data.webservice.bookAppointements.encoder.BookEncoder

class BookAppointmentRepository(private val appContext: Context,
                                private val sharedPreferences: SharedPreferencesManagerImpl,
                                private val bookAppointmentsWebservice: BookAppointmentsWebservice) {

    fun confirm(bookInfo: BookInfo, coupon: String): LiveData<DataResource<String>> {
        val liveData = MutableLiveData<DataResource<String>>()
        val json = BookEncoder.getJson(bookInfo,coupon)
        Log.i("book",json)
        bookAppointmentsWebservice.confirm(sharedPreferences.getData(appContext.getString(R.string.token), "")
                , json).enqueue(CustomResponse<String>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }

    fun addPromoCode(promoCode: PromoCode): LiveData<DataResource<ArrayList<String>>> {
        val liveData = MutableLiveData<DataResource<ArrayList<String>>>()
        val json = AddPromoEncoder.getJson(promoCode)
        bookAppointmentsWebservice.addPromoCode(sharedPreferences.getData(appContext.getString(R.string.token), "")
                , json).enqueue(CustomResponse<ArrayList<String>>({
            liveData.value = it
        }, arrayOf()))
        return liveData
    }
}