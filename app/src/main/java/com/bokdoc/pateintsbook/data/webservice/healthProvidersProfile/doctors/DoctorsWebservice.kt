package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.doctors

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import okhttp3.ResponseBody
import retrofit2.Call

class DoctorsWebservice(val webserviceManagerImpl: WebserviceManagerImpl) {

//    //call in when open doctors activity from departments
//    fun getDepartmentsDoctors(id: String, departmentId: String, token: String): Call<ResponseBody> {
//        val query = StringsConstants.buildString(arrayListOf(DoctorsApi.DEPARTMENTS_DOCTORS,
//                StringsConstants.OPEN_BRACKET, departmentId, StringsConstants.CLOSED_BRACKET))
//        return webserviceManagerImpl.createService(DoctorsApi::class.java, token).getDoctors(id, query)
//    }

    //call in when open doctors activity from hospital profile or card sub list
    fun getDoctors(token: String, id: String, service: HashMap<String, String>): Call<ResponseBody> {
        val doctorsWebservice = webserviceManagerImpl.createService(DoctorsApi::class.java, token)
        return doctorsWebservice.getDoctors(id, service)
    }

}