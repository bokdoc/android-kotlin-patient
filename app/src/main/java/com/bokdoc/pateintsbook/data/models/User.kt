package com.bokdoc.pateintsbook.data.models

class User {
    var email: String = ""
    var name: String = ""
    var phone: String = ""
    var id: String = ""
    var gender: String = ""
    var birthDate: String = ""
    var picture: Picture = Picture()

    companion object {
        const val MALE = "Male"
        const val FEMALE = "Female"
        val GENDER = arrayListOf(MALE, FEMALE)
    }
}