package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.consulting

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.converters.HealthProvidersProfiles.ConsultingConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.surgeries.ProfileSurgeriesWebService
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows.ConsultingRow
import com.bokdoc.pateintsbook.data.webservice.models.NextAvailabilityResource
import com.bokdoc.pateintsbook.data.webservice.models.Speciality

class ProfileConsultingRepository(private val appContext: Context,
                                  private val sharedPreferencesManager: SharedPreferencesManager,
                                  private val surgeriesWebService: ProfileSurgeriesWebService) {

    fun getConsulting(id: String, service: String): LiveData<DataResource<ConsultingRow>> {

        val liveData = MutableLiveData<DataResource<ConsultingRow>>()
        val dataResource = DataResource<ConsultingRow>()

        surgeriesWebService.getAllSurgeries(sharedPreferencesManager.getData(appContext.getString(R.string.token), ""), id, service)
                .enqueue(CustomResponse<HealthProviderProfile>({
                    dataResource.status = it.status
                    if (it.data!!.isNotEmpty()) {
                        dataResource.data = ConsultingConverter.convert(it.data!![0])
                        //dataResource.meta = it.meta
                    } else {
                        dataResource.errors = it.errors
                    }
                    liveData.value = dataResource
                }, arrayOf(HealthProviderProfile::class.java
                        , Contacts::class.java, WorkingTimes::class.java, Consulting::class.java,ProfileTitle::class.java ,NextAvailabilityResource::class.java, ProfileType::class.java, Speciality::class.java)))

        return liveData
    }

}