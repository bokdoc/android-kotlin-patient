package com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.scans

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Scan
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.ServicesRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManager
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.scans.ScansWebservice


class ScansRepository(private val appContext: Context,
                      private val sharedPreferencesManagerImpl: SharedPreferencesManager,
                      private val scansWebservice: ScansWebservice):
        ServicesRepository(appContext,sharedPreferencesManagerImpl,scansWebservice) {


    fun getScans(id:String):LiveData<DataResource<Scan>>{
        val liveData = MutableLiveData<DataResource<Scan>>()
        getServices(id).observeForever {
            liveData.value = it as DataResource<Scan>
        }
        return liveData
    }
}