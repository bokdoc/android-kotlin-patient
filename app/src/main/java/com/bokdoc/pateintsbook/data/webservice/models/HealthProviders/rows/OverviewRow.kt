package com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.rows

  import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.IFeedback
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.Qualifications
import com.bokdoc.pateintsbook.data.models.healthProviders.qualifications.QualificationsData
import com.bokdoc.pateintsbook.data.webservice.models.DynamicLinks
  import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.Fees
  import com.bokdoc.pateintsbook.data.webservice.models.Location

class OverviewRow {
    var name: String = ""
    var location: Location? = null
    var consultingFees = Fees()
    var doctorsNumbers: String = ""
    var departmentsNumbers: String = ""
    var likesNumbers: String = ""
    var ambulanceCount: String = ""
    var reviewsNumbers: String = ""
    var rating: Float = 0.0f
    var bookButton: DynamicLinks?=null
    var consultingBookButton: DynamicLinks?=null
    var picture: String = ""
    var establishBirthDate: String = ""
    var spokenLanguage: ArrayList<String> = ArrayList()
    var paymentOptions: ArrayList<String> = ArrayList()
    var gender: String = "l"
    var speciality: String = ""
    var bedsNumber: String = ""
    var accommodations = AccommodationsRow()
    var accreditation = ArrayList<AccreditationRow>()
    var reviews = ArrayList<Feedback>()
    var education = ArrayList<QualificationsData>()
    var certification = ArrayList<QualificationsData>()
    var publication = ArrayList<QualificationsData>()
    var membership = ArrayList<QualificationsData>()
    var insuranceCarriersRow = ArrayList<InsuranceCarriersRow>()
}