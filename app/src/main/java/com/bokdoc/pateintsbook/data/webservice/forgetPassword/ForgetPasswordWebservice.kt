package com.bokdoc.healthproviders.data.webservice.forgetPassword

 import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.forgetPassword.ForgetPasswordApi
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class ForgetPasswordWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun forgetMobile(forgetPassword: String): Call<ResponseBody> {
        val forgetPasswordApi = webserviceManagerImpl.createService(ForgetPasswordApi::class.java)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), forgetPassword)
        return forgetPasswordApi.passwordMobile(requestBody)
    }

    fun forgetCode(forgetPassword: String): Call<ResponseBody> {
        val forgetPasswordApi = webserviceManagerImpl.createService(ForgetPasswordApi::class.java)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), forgetPassword)
        return forgetPasswordApi.passwordMobileVerify(requestBody)
    }


    fun forgetNewPassword(forgetPassword: String): Call<ResponseBody> {
        val forgetPasswordApi = webserviceManagerImpl.createService(ForgetPasswordApi::class.java)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), forgetPassword)
        return forgetPasswordApi.passwordRecover(requestBody)
    }


}