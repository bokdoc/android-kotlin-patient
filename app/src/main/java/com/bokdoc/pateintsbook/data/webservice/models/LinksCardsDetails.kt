package com.bokdoc.pateintsbook.data.webservice.models


import com.bokdoc.pateintsbook.data.models.KeyValue
import com.bokdoc.pateintsbook.data.models.Picture
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.MainFees

class LinksCardsDetails {

    var title:String = ""
    var mainProfileInfo = ProfileInfo()
    var rowsInfo = ArrayList<RowsInfo>()
    var slug: String = ""
    var bookType = ""
    var serviceType = ""
    var rotators:List<String> = ArrayList<String>().apply {
        add("Test Rotators 1")
        add("Test Rotators 2")
        add("Test Rotators 3")
    }

    class RowsInfo {
        var id: String = ""
        var name: String = ""
        var nextAvailabilities: List<NextAvailabilitiesParcelable> = ArrayList()
        var secondaryDurationConsulting:Boolean = false
        var secondaryDurations:List<KeyValue> = arrayListOf()
        var bookSecondaryDuration:Boolean?=null
        var bookButton:DynamicLinks = DynamicLinks()
        var profileInfo = ProfileInfo()
        var location = Location()
        var speciality = ""
        var fees = Fees()
        var mainFees = MainFees()
        var subMainFees = MainFees()
        var isAppointmentReservation = false
        var duration = ""
    }

    class ProfileInfo{
        var profileName = ""
        var profileMainSpeciality = ""
        var profileId = ""
        var profilePicture = Picture()
        var profileType:Int = 1
        var location = Location()
        var gender: String = ""
    }

    companion object {
        const val SURGERIES_SLUG = "surgeries"
        const val CLINICS_SLUG = "clinics"
        const val CONSULTING_SLUG = "consulting"
    }
}