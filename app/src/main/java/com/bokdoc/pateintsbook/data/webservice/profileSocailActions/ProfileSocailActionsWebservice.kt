package com.bokdoc.pateintsbook.data.webservice.profileSocailActions

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.ResponseBody
import retrofit2.Call

class ProfileSocailActionsWebservice(private val webserviceManagerImpl: WebserviceManagerImpl) {


    fun toggleLike(token: String, id: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(ProfileSocialActionsApi::class.java, token).toggleLike(id)
    }

    fun toggleFavorite(token: String, id: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(ProfileSocialActionsApi::class.java, token).toggleFavorite(id)
    }


    companion object {
        const val SHARABLE_URL = "http://bokdoc.com/profile"
        const val ID_KEY = "id"
        const val PICTURE_KEY = "picture"
        const val NAME_KEY = "name"
        const val LOCATION_KEY = "location"
        const val TYPE_KEY = "type"
        const val SUCCESS_MESSAGE = "auth.Done Successfully"
        const val IS_LIKED = "isLiked"
        const val IS_FAVOURITE = "is_favourite"
        const val IS_COMPARED = "isCompared"
    }
}