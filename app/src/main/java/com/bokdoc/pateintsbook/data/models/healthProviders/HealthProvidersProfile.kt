package com.bokdoc.pateintsbook.data.models.healthProviders

import android.os.Parcel
import android.os.Parcelable
import com.bokdoc.pateintsbook.data.models.nextAvailabilities.NextAvailabilitiesParcelable
import com.bokdoc.pateintsbook.data.webservice.models.Location

class HealthProvidersProfile(val id: String, var speciality: String, val type: Int, val picture: String, val name: String, val gender: String, val location: String,
                             val docsNum: String, val departmentsNum: String, val rateing: Float, val reviewsCount: String, val date: String,val locationData: Location, val likesNum: String,
                             var isLiked: Boolean, var isFavourite: Boolean, var isCompare: Boolean,
                             var nextAvailabilitesParceable: List<NextAvailabilitiesParcelable> = ArrayList()) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Location::class.java.classLoader),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.createTypedArrayList(NextAvailabilitiesParcelable)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(speciality)
        parcel.writeInt(type)
        parcel.writeString(picture)
        parcel.writeString(name)
        parcel.writeString(gender)
        parcel.writeString(location)
        parcel.writeString(docsNum)
        parcel.writeString(departmentsNum)
        parcel.writeFloat(rateing)
        parcel.writeString(reviewsCount)
        parcel.writeString(date)
        parcel.writeParcelable(locationData, flags)
        parcel.writeString(likesNum)
        parcel.writeByte(if (isLiked) 1 else 0)
        parcel.writeByte(if (isFavourite) 1 else 0)
        parcel.writeByte(if (isCompare) 1 else 0)
        parcel.writeTypedList(nextAvailabilitesParceable)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HealthProvidersProfile> {
        override fun createFromParcel(parcel: Parcel): HealthProvidersProfile {
            return HealthProvidersProfile(parcel)
        }

        override fun newArray(size: Int): Array<HealthProvidersProfile?> {
            return arrayOfNulls(size)
        }
    }


}