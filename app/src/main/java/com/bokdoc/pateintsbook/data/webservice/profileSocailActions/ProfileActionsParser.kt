package com.bokdoc.pateintsbook.data.webservice.profileSocailActions

import org.json.JSONObject

object ProfileActionsParser {
    private const val META = "meta"
    private const val MESSAGE = "message"

    fun getMessage(json:String):String{
       return JSONObject(json).
                getJSONObject(META).
                getString(MESSAGE)
    }

}