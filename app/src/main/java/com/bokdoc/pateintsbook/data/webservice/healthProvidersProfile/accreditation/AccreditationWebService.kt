package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accreditation

import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class AccreditationWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getAccreditation(token: String, id: String, service: String): Call<ResponseBody> {
        val surgeriesWebService = webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token)
        return surgeriesWebService.getProfileSection(id, service)
    }

}