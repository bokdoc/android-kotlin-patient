package com.bokdoc.pateintsbook.data.webservice.addFcm

import com.bokdoc.pateintsbook.data.webservice.models.updatePopup.CheckUpdateResorce
import com.squareup.moshi.Moshi
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ObjectDocument
import moe.banana.jsonapi2.ResourceAdapterFactory

object CheckUpdateEncoder {

    fun getJson(version: String, fcmToken: String): String {
        val moshi = getMoshiParser()
        val document = ObjectDocument<CheckUpdateResorce>()
        val checkUpdate = CheckUpdateResorce()
        checkUpdate.id = ""
        checkUpdate.version = version
        checkUpdate.device_os = "android"
        checkUpdate.fcm_token = fcmToken
        document.set(checkUpdate)

        return moshi.adapter(Document::class.java).toJson(document)
    }


    private fun getMoshiParser(): Moshi {
        val jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(CheckUpdateResorce::class.java)
                .build()
        val moshi = Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build()

        return moshi
    }
}