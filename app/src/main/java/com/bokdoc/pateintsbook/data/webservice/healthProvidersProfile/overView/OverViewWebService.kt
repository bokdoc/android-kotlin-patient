package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView

import android.util.Log
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.HealthProvidersProfileApi
import okhttp3.ResponseBody
import retrofit2.Call

class OverViewWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getProfileOverView(id: String, token: String, services: String): Call<ResponseBody> {
        Log.i("token",token)
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token).
                getProfileOverview(id, services)
    }

    fun getProfileAddess(id: String, token: String): Call<ResponseBody> {
        return webserviceManagerImpl.createService(HealthProvidersProfileApi::class.java, token).getProfileAddress(id)
    }

}