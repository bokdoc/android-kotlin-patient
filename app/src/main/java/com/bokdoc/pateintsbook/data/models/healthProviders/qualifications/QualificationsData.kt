package com.bokdoc.pateintsbook.data.models.healthProviders.qualifications

class QualificationsData : Qualifications {
    override var description: String = ""
    override var title: String = ""
    override var link: String = ""
    override var content: String = ""
    override var university_id: String = ""
    override var years: String = ""
    override var icon: String = ""
}