package com.bokdoc.pateintsbook.data.webservice.contactUs

 import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
 import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class ContactUsWebService(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun sendContactUs(token: String, json: String): Call<ResponseBody> {
        val contactUsApi = webserviceManagerImpl.createService(ContactUsApi::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        return contactUsApi.contactUs(requestBody)
    }

}