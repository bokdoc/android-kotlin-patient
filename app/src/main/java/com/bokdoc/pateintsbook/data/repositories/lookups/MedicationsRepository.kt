package com.bokdoc.pateintsbook.data.repositories.lookups

import android.arch.lifecycle.LiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.lookups.LookupsType
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.webservice.lookups.LookupsWebservice
import com.bokdoc.pateintsbook.data.webservice.models.Medication

class MedicationsRepository(appContext: Context,sharedPreferencesManagerImpl: SharedPreferencesManagerImpl,
                            lookupsWebservice: LookupsWebservice):LookupsRepository(appContext,sharedPreferencesManagerImpl,lookupsWebservice) {

    fun lookupsMedications():LiveData<DataResource<LookupsType>>{
        return super.lookups(LookupsWebservice.MEDICATIONS_QUERY,Medication::class.java)
    }
}