package com.bokdoc.pateintsbook.data.repositories.reviews

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.bokdoc.pateintsbook.data.common.parser.MetaParser
import com.bokdoc.pateintsbook.data.common.parser.Parser
import com.bokdoc.pateintsbook.data.converters.ProviderReviewsConverter
import com.bokdoc.pateintsbook.data.models.DataResource
import com.bokdoc.pateintsbook.data.models.healthProviders.profileFeedback.Feedback
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.CustomResponse
import com.bokdoc.pateintsbook.data.webservice.models.*
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.FeedbackPersonaResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.FeedbackResource
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.HealthProviderProfile
import com.bokdoc.pateintsbook.data.webservice.reviews.ReviewsWebService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewsRepository(val context: Context, val reviewsWebService: ReviewsWebService,
                        val userSharedPreference: UserSharedPreference) {

    fun getReviews(id: String, serviceType: String, page: Int): LiveData<DataResource<Review>> {
        val liveData = MutableLiveData<DataResource<Review>>()
        val callable = reviewsWebService.getReviews(id, userSharedPreference.getToken(),
                serviceType, page)
        callable.enqueue(CustomResponse<Review>({
            liveData.value = it
        }, arrayOf(Review::class.java, Persona::class.java)))
        return liveData
    }


    fun getProviderReviews(id: String, serviceType: String, page: Int): LiveData<DataResource<Feedback>> {
        val liveData = MutableLiveData<DataResource<Feedback>>()
        val dataResource = DataResource<Feedback>()
        val callable = reviewsWebService.getProvidersReviews(id, userSharedPreference.getToken(),
                serviceType, page)
        callable.enqueue(CustomResponse<HealthProviderProfile>({
            dataResource.status = it.status
            if (it.data!!.isNotEmpty()) {
                dataResource.data = ProviderReviewsConverter.convert(it.data!![0])
                dataResource.meta = it.meta
            } else {
                dataResource.errors = it.errors
            }
            liveData.value = dataResource
        }, arrayOf(HealthProviderProfile::class.java, FeedbackResource::class.java, FeedbackPersonaResource::class.java)))
        return liveData
    }

    fun getUserId(): String {
        return userSharedPreference.getId()
    }


}