package com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileGallery

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileGalleryApi {
    @GET(WebserviceConstants.PROFILES_WITH_SLASH+"{id}?include=media")
    fun getGallery(@Path("id")id:String):Call<ResponseBody>
}