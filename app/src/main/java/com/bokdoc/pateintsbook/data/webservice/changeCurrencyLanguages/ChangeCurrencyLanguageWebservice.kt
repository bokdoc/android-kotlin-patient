package com.bokdoc.pateintsbook.data.webservice.changeCurrencyLanguages


import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

class ChangeCurrencyLanguageWebservice(val webserviceManagerImpl: WebserviceManagerImpl) {

    fun change(token:String,id:String,json:String):Call<ResponseBody>{
        val changeCurrency = webserviceManagerImpl.createService(ChangeCurrencyLanguagesApis::class.java, token)
        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        return changeCurrency.change(id, requestBody)
    }
}