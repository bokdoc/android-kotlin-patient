package com.bokdoc.pateintsbook.utils.ui

import android.content.Context
import android.support.v7.app.AlertDialog
import com.bokdoc.pateintsbook.R
import com.karumi.dexter.PermissionToken

object PermissionsAlertDialog {
    fun show(context: Context,token: PermissionToken,title:Int,description:Int){
            AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(description)
                    .setPositiveButton(android.R.string.ok) { dialog, p1 ->
                        dialog.dismiss()
                        token.continuePermissionRequest()
                    }
                    .setNegativeButton(R.string.cancel){ dialog, p1 ->
                        dialog.dismiss()
                        token.cancelPermissionRequest()
                    }
                    .setOnDismissListener {
                        it.dismiss()
                        token.cancelPermissionRequest()
                    }.show()
        }
}