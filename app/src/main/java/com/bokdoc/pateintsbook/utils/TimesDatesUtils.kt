package com.bokdoc.pateintsbook.utils

import android.text.format.DateFormat.is24HourFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


object TimesDatesUtils {

    const val FORMAT_12_AM_PM_TYPE = "hh:mm aa"
    const val FORMAT_12_FLOAT_TYPE = "floatType"
    const val FORMAT_24_FLOAT_TYPE = "float24Type"
    const val FORMAT_12_TYPE = "hh:mm"
    const val FORMAT_24_TYPE = "HH:mm"
    const val TIME_SPLITTER = ":"
    const val FORMAT_DATE_DASH_SPLITTER = "yyyy-MM-dd"
    const val FORMAT_DATE_MONTH_YEAR_DASH = "MM-yyyy"

    fun getHoursType(time:String):String{
        val hour = time.toDouble()
        if(hour>=12){
            return "PM"
        }else{
            return "AM"
        }
    }

    fun getHourIn12Type(time: String):Float{
        val hour = time.toFloat()
        if(hour<12) {
            return hour + 12
        }else{
            if(hour>12)
            return hour - 12
        }
        return 0.0f
    }

    fun formatString(fromFormat:String,time: String,toFormat:String):String{
        var formattedTime = arabicToDecimal(time)
        var fromFormatDate:SimpleDateFormat
        if(fromFormat == FORMAT_24_FLOAT_TYPE){
            formattedTime = convertFloatToTime(time.toFloat())
            fromFormatDate = SimpleDateFormat(FORMAT_24_TYPE,Locale.US)
        }else{
            fromFormatDate = SimpleDateFormat(fromFormat,Locale.US)
        }

        val toFormatDate = SimpleDateFormat(toFormat,Locale.US)
        var date:Date = Date()
        try {
            date = fromFormatDate.parse(formattedTime)
        }
        catch (e:Exception){

        }

        return toFormatDate.format(date)
    }

    fun convertFloatToTime(timeFloat: Float):String{
        val number = timeFloat.toInt()
        val fraction = timeFloat.rem(1)
        return "$number:${Math.floor(fraction.toDouble())}"
    }

    private fun arabicToDecimal(number: String): String {
        return number
                .replace("٠","0")
                .replace("١","1")
                .replace("٢","2").
                replace("٣","3").replace("٤","4")
                .replace("٥","5")
                .replace("٦","6")
                .replace("٧","7")
                .replace("٨","8")
                .replace("٩","9")
    }

    fun convertTimeToFloat(time: String,format: String):Float{
        var formattedTime = arabicToDecimal(time)
        if(format == FORMAT_12_AM_PM_TYPE || format == FORMAT_12_TYPE){
            formattedTime = formatString(format,formattedTime,FORMAT_24_TYPE)
        }
        val values = formattedTime.split(TIME_SPLITTER)
        val timeFloat = values[0].toFloat() + (values[1].toFloat().div(100))
        return String.format(Locale.US,"%.2f",arabicToDecimal(timeFloat.toString()).toFloat()).toFloat()
    }


    fun getFormattedDate(calendar: Calendar,format:String):String{
        val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
        return simpleDateFormat.format(calendar.time)
    }

    fun getFormattedDate(date: Date,format: String):String{
        val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
        return simpleDateFormat.format(date)
    }

    fun getTimesFromDate(date:String,dateFormat:String):Long{
        val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
        return simpleDateFormat.parse(date).time
    }


    fun getFormattedDayWithSplitter(year:Int,monthOfYear:Int,dayOfMonth:Int,splitter:String):String{
        return year.toString() + splitter + (monthOfYear + 1) + splitter + dayOfMonth
    }

    fun getDateParts(date:String,splitter:String):List<String>{
        if(date.isEmpty())
            return arrayListOf()

        return date.split(splitter)
    }

}