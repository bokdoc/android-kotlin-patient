package com.bokdoc.pateintsbook.utils.listeners

import android.app.Activity
import android.support.v4.app.Fragment

import android.view.MotionEvent
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.navigators.AutoCompleteNavigator

abstract class HomeSearchEditsListeners(val activity: Activity, val keys:Array<String>, var screenType:String, val completeType:Int
                                        , val requestType:Int, val fragment: Fragment?=null, val hint:String = ""): View.OnTouchListener {

    override fun onTouch(view: View?, motion: MotionEvent?): Boolean {
        if(MotionEvent.ACTION_UP == motion?.action) {
            passCompletedType(completeType)
            /*
            if(fragment!=null) {
                AutoCompleteNavigator.navigate(fragment, keys, screenType, completeType, requestType,hint)
            }else{
                AutoCompleteNavigator.navigate(activity, keys, screenType, completeType, requestType)
            }
            */
        }
        return true
    }

   abstract fun passCompletedType(completeType: Int)


}