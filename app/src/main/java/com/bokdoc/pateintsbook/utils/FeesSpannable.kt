package com.bokdoc.pateintsbook.utils

import android.text.Spannable
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StrikethroughSpan

object FeesSpannable {
    fun getSpannable(text:String,colorableText:String,color: Int,
                     strokeThroughText:String?): Spannable {
        val span = Spannable.Factory.getInstance().newSpannable(text)
        span.setSpan(ForegroundColorSpan(color), text.indexOf(colorableText),
                text.indexOf(colorableText) + colorableText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        strokeThroughText?.let {
            if(it.isNotEmpty() && it!="0" && it!="0.0"){
                span.setSpan(StrikethroughSpan(), text.indexOf(strokeThroughText),
                        text.indexOf(strokeThroughText) + strokeThroughText.length,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }

        return span
    }

    fun getStrokedFees(fees:String):Spannable{
        val span = Spannable.Factory.getInstance().newSpannable(fees)
        if(fees.isNotEmpty() && fees!="0" && fees!="0.0"){
            span.setSpan(StrikethroughSpan(),0,fees.length,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return span
    }
}