package com.bokdoc.pateintsbook.utils.repositories

import com.bokdoc.pateintsbook.utils.constants.WebserviceConstants

object QueryFormatter {

    fun formatQuery(query: String):String{
        var formattedQuery = ""
        if(query.isNotEmpty())
            formattedQuery= "${WebserviceConstants.QUERY_MARK}$query"

        return formattedQuery
    }
}