package com.bokdoc.pateintsbook.utils.files

import android.graphics.Bitmap
import android.net.Uri
import android.webkit.MimeTypeMap
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import java.io.File
import java.io.FileOutputStream


class FilesUtils {

    companion object {
        fun isFilesSizeSmallerThan(paths: Array<Uri>, maxMegaSize: Int): Boolean {
            var size = 0.0
            paths.forEach {
                val file = getFile(it)
                val fileSizeInBytes = file.length()
                // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                val fileSizeInKB = fileSizeInBytes / KILOBYTE_IN_BYTES
                // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                val fileSizeInMB = fileSizeInKB.toFloat() / MEGA_IN_KILOBYTES
                size += fileSizeInMB
            }
            return size <= maxMegaSize
        }

        fun getFileType(uriFile: Uri): String {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uriFile.toString().replace(" ", ""))
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)
        }

        fun getFile(uri: Uri): File {
            return File(uri.path)
        }

        fun getUri(path: String): Uri {
            return Uri.fromFile(File(path))
        }

        fun getPath(uri: Uri): String {
            return uri.path
        }

        fun getPaths(paths: ArrayList<Uri>): ArrayList<String> {
            return ArrayList<String>().apply {
                paths.forEach {
                    this.add(com.bokdoc.pateintsbook.utils.files.FilesUtils.getPath(it))
                }
            }
        }

        fun getUris(paths: ArrayList<String>): Array<Uri> {
            val array = Array<Uri>(paths.size) { Uri.parse("") }
            val size = paths.size
            return array.apply {
                (0 until size).forEach {
                    this[it] = getUri(paths[it])
                }
            }
        }

        fun isFileSupport(path: String, types: Array<String>): Boolean {
            val selectedType = path.substring(path.lastIndexOf("."), path.length)
            types.forEach {
                if (it == selectedType) return true
            }
            return false
        }


        fun isFilesSupport(paths: List<String>, types: Array<String>): Boolean {
            paths.forEach {
                return isFileSupport(it, types)
            }
            return false
        }

        fun getFileName(uri: Uri): String {
            val path = uri.path
            return path.substring(path.lastIndexOf(StringsConstants.SLASH) + 1)
        }

        fun saveBitmap(bitmap: Bitmap, dirFile: File, name: String): Uri {
            val file = File(dirFile, name)
            val fOut = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.flush()
            fOut.close()
            return Uri.fromFile(file)
        }

        const val MB_2 = 2
        const val KILOBYTE_IN_BYTES = 1024
        const val MEGA_IN_KILOBYTES = 1024
    }
}