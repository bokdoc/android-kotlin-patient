package com.bokdoc.pateintsbook.utils.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView



class DividerItemDecoration: RecyclerView.ItemDecoration {
    private lateinit var divider:Drawable


    constructor(context:Context):super(){
        val styledAttrs = context.obtainStyledAttributes(ATTRS)
        divider = styledAttrs.getDrawable(0)
        styledAttrs.recycle()
    }


    constructor(context: Context,dividerResId:Int):super(){
        divider = ContextCompat.getDrawable(context,dividerResId)!!
    }



    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent?.paddingLeft
        val right = parent?.width?.minus(parent.paddingRight)

        val childCount = parent?.getChildCount()
        for (i in 0 until childCount!!) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + divider.intrinsicHeight

            divider.setBounds(left!!, top, right!!, bottom)
            divider.draw(c)
        }
    }


























    companion object {
        private val ATTRS:IntArray = intArrayOf(android.R.attr.listDivider)
    }
}