package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.support.v7.app.AlertDialog
import com.bokdoc.pateintsbook.R


object DialogCreator {
    fun show(context: Context, message: Int, positiveText:Int=R.string.mdtp_ok,
             positiveCLickListener:()->Unit={},
             negativeClickListener:()->Unit={},
             negativeText:Int=R.string.cancel,
             dismissListener:()->Unit={}){

        val dialog = AlertDialog.Builder(context)
        dialog.setMessage(message)
        dialog.setPositiveButton(positiveText) { paramDialogInterface,_ ->
            positiveCLickListener.invoke()
            paramDialogInterface.dismiss()
        }
        if(negativeText!=-1) {
            dialog.setNegativeButton(negativeText) { paramDialogInterface, _ ->
                negativeClickListener.invoke()
                paramDialogInterface.dismiss()
            }
            dialog.setOnDismissListener {
                dismissListener.invoke()
                it.dismiss()
            }
        }
        dialog.show()
    }

    fun show(context: Context, message: String, positiveText:Int=R.string.mdtp_ok,
             positiveCLickListener:()->Unit={},
             negativeClickListener:()->Unit={},
             negativeText:Int=R.string.cancel,
             dismissListener:()->Unit={}){

        val dialog = AlertDialog.Builder(context)
        dialog.setMessage(message)
        dialog.setPositiveButton(positiveText) { paramDialogInterface,_ ->
            positiveCLickListener.invoke()
            paramDialogInterface.dismiss()
        }
        if(negativeText!=-1) {
            dialog.setNegativeButton(negativeText) { paramDialogInterface, _ ->
                negativeClickListener.invoke()
                paramDialogInterface.dismiss()
            }
            dialog.setOnDismissListener {
                dismissListener.invoke()
                it.dismiss()
            }
        }
        dialog.show()
    }
}