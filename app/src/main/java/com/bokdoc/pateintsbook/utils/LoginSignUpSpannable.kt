package com.bokdoc.pateintsbook.utils

import android.text.Spannable
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View

object LoginSignUpSpannable {
    fun getClickableSpannable(text:String,clickableText:String,clickableTextColor:Int,clickableMethod:()->Unit):Spannable{
        val clickableTextLength = clickableText.length
        val clickableTextPosition = text.indexOf(clickableText)
        val span  = Spannable.Factory.getInstance().newSpannable(text)
        span.setSpan(object :ClickableSpan(){
            override fun onClick(view: View?) {
                clickableMethod.invoke()
            }

            override fun updateDrawState(ds: TextPaint?) {
                ds?.isUnderlineText = false
            }

        },clickableTextPosition,clickableTextPosition+clickableTextLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        span.setSpan(ForegroundColorSpan(clickableTextColor),clickableTextPosition,
                clickableTextPosition+clickableTextLength,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        return span
    }
}