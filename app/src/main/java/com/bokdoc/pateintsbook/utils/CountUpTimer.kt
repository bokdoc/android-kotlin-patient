package com.bokdoc.pateintsbook.utils

import android.os.CountDownTimer
import java.util.*


abstract class CountUpTimer protected constructor(private val duration: Long) : CountDownTimer(duration, INTERVAL_MS) {

    abstract fun onTick(second: String)

    override fun onTick(msUntilFinished: Long) {

        val minutes = (msUntilFinished / 1000) / 60
        val seconds = (msUntilFinished / 1000) % 60

        val timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)


      //  val second = ((duration - msUntilFinished) / 1000).toInt()
        onTick(timeLeftFormatted)
    }

    override fun onFinish() {
        onTick(duration / 1000)
    }

    companion object {
        private val INTERVAL_MS: Long = 1000
    }
}
