package com.bokdoc.pateintsbook.utils.constants

object SocialConstants {
    const val FACEBOOK = "facebook"
    const val TWITTER = "twitter"
    const val GOOGLE_PLUS = "google"
    const val INSTAGRAM = "instagram"
    const val LINKEDIN = "linkedin"
}