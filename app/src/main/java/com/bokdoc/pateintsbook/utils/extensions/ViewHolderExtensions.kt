package com.bokdoc.pateintsbook.utils.extensions

import android.content.Context
import android.support.v7.widget.RecyclerView

 val RecyclerView.ViewHolder.View:Context
get() {
    return this.itemView.context
}