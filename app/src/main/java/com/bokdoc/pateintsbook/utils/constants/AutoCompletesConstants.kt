package com.bokdoc.pateintsbook.utils.constants

object AutoCompletesConstants {
    const val SPECIALITY = 0
    const val LOCATIONS = 1
    const val INSURANCE = 2
}