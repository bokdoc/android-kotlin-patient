package com.bokdoc.pateintsbook.utils.listeners

import android.support.constraint.Group
import android.view.View

object GroupListner {
    fun Group.setAllOnClickListener(listener: View.OnClickListener?) {
        referencedIds.forEach { id ->
            rootView.findViewById<View>(id).setOnClickListener(listener)
        }
    }
}