package com.bokdoc.pateintsbook.utils.constants

object MediaTypes {
    const val VIDEO_MP4 = "video/mp4"
}