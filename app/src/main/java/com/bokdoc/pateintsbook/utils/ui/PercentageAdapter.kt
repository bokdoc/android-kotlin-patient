package com.bokdoc.pateintsbook.utils.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_text_radio.view.*

class PercentageAdapter(private var percentages: Array<String>,
                        private val onAdapterClickListener: (String, Int) -> Unit,var inSelectedValue:String="")
    : RecyclerView.Adapter<PercentageAdapter.PercentaggViewHolder>() {


    private var onBind = true

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PercentaggViewHolder {
        return PercentaggViewHolder(parent.inflateView(R.layout.item_text_radio))
    }

    override fun getItemCount(): Int {
        return percentages.size
    }

    override fun onBindViewHolder(holder: PercentaggViewHolder, p1: Int) {
        percentages[p1].let {
            holder.itemView.tv_num.text = it
            onBind = true
            holder.itemView.rb_numb.isChecked = it == inSelectedValue
            onBind= false
        }
    }


    inner class PercentaggViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.rb_numb.setOnCheckedChangeListener { compoundButton, b ->
                if(!onBind)
                onAdapterClickListener.invoke(percentages[adapterPosition], adapterPosition)
            }
        }
    }
}