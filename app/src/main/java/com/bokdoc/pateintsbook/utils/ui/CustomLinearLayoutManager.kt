package com.bokdoc.pateintsbook.utils.ui

import android.support.v7.widget.LinearLayoutManager
import android.R.attr.x
import android.content.Context
import android.view.Display
import android.content.Context.WINDOW_SERVICE
import android.graphics.Point
import android.support.v4.content.ContextCompat.getSystemService
import android.view.WindowManager
import android.view.View.MeasureSpec
import android.view.View.MeasureSpec.EXACTLY
import android.view.View.MeasureSpec.makeMeasureSpec
import android.support.v7.widget.RecyclerView
import android.view.View


class CustomLinearLayoutManager(mContext: Context) : LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {

    private var spanCount: Int = 0
    /*
    override fun measureChild(child: View, widthUsed: Int, heightUsed: Int) {
        val lpTmp = child.getLayoutParams() as RecyclerView.LayoutParams
        val lp = RecyclerView.LayoutParams(measureScreenWidth(mContext) / spanCount, lpTmp.height)
        val widthSpec = View.MeasureSpec.makeMeasureSpec(measureScreenWidth(mContext) / spanCount, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(lp.height, View.MeasureSpec.EXACTLY)
        child.measure(widthSpec, heightSpec)
    }

    override fun measureChildWithMargins(child: View, widthUsed: Int, heightUsed: Int) {
        val lpTmp = child.getLayoutParams() as RecyclerView.LayoutParams
        val lp = RecyclerView.LayoutParams(measureScreenWidth(mContext) / spanCount, lpTmp.height)
        val widthSpec = View.MeasureSpec.makeMeasureSpec(measureScreenWidth(mContext) / spanCount, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(lp.height, View.MeasureSpec.EXACTLY)
        child.measure(widthSpec, heightSpec)
    }

    private fun measureScreenWidth(context: Context): Int {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)

        return point.x
    }
    */

    override fun canScrollVertically(): Boolean {
        return false
    }

    override fun canScrollHorizontally(): Boolean {
        return false
    }

    fun getSpanCount(): Int {
        return spanCount
    }

    fun setSpanCount(spanCount: Int) {
        this.spanCount = spanCount
    }
}