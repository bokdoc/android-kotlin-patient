package com.bokdoc.pateintsbook.utils.ui

import android.content.Context
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.widget.NumberPicker
import com.bokdoc.pateintsbook.R.id.numberPicker
import android.widget.EditText
import java.lang.reflect.AccessibleObject.setAccessible



class NumberPickerTextColor {


    companion object {
        fun applyColor(context:Context,colorRes:Int,numberPicker: NumberPicker){
            val count = numberPicker.childCount
            for (i in 0 until count) {
                val child = numberPicker.getChildAt(i)
                if (child is EditText) {
                    try {
                        val selectorWheelPaintField = numberPicker::class.java
                                .getDeclaredField("mSelectorWheelPaint")
                        selectorWheelPaintField.setAccessible(true)
                        (selectorWheelPaintField.get(numberPicker) as Paint).color = ContextCompat.getColor(context,colorRes)
                        child.setTextColor(ContextCompat.getColor(context,colorRes))
                        numberPicker.invalidate()
                    } catch (e: NoSuchFieldException) {
                    } catch (e: IllegalAccessException) {
                    } catch (e: IllegalArgumentException) {
                    }
                }
            }
        }
    }
}