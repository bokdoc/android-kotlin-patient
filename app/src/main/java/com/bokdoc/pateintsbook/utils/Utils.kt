package com.bokdoc.pateintsbook.utils

import android.app.*
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.content.res.Configuration
import android.graphics.Typeface
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.provider.Settings
import android.support.customtabs.CustomTabsClient.getPackageName
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bokdoc.pateintsbook.BuildConfig
import com.bokdoc.pateintsbook.R


import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.lang.reflect.Field
import java.net.URLEncoder
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by Shamyyoun on 18/12/15.
 * A class, with general purpose utility methods (useful for many projects).
 */
class Utils {

    /**
     * show key board in edit text field
     */

    companion object {
        val DEBUGGABLE = false // TODO disable when generate signed apk
        val PACKAGE_FACEBOOK = "com.facebook.katana"
        val PACKAGE_FACEBOOK_MESSENGER = "com.facebook.orca"
        val PACKAGE_GOOGLE_PLUS = "com.google.android.apps.plus"
        val PACKAGE_WHATSAPP = "com.whatsapp"
        val PACKAGE_PLAY_STORE = "com.android.vending"
        val REGEX_MOBILE_NUMBER = "^(\\+\\d{1,3}[- ]?)?\\d{10,15}$"
        val REGEX_EGYPTIAN_MOBILE_NUMBER = "^01[0-2|5]{1}[0-9]{8}"
        val REGEX_EGYPTIAN_VODAFONE_MOBILE_NUMBER = "^010{1}[0-9]{8}"
        private val KEY_APP_VERSION_CODE = "app_version_code_key"
        val STRONG_PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$"
        private val FACEBOOK_SHARER_URL = "https://www.facebook.com/sharer/sharer.php?u="


        fun isAppIsInBackground(context: Context): Boolean {
            var isInBackground = true
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                val runningProcesses = am.runningAppProcesses
                for (processInfo in runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (activeProcess in processInfo.pkgList) {
                            if (activeProcess == context.packageName) {
                                isInBackground = false
                            }
                        }
                    }
                }
            } else {
                val taskInfo = am.getRunningTasks(1)
                val componentInfo = taskInfo[0].topActivity
                if (componentInfo.packageName == context.packageName) {
                    isInBackground = false
                }
            }

            return isInBackground
        }


        fun showNotification(context: Context, id: Int, title: String, message: String, intent: Intent?) {
            var intent = intent
            // create the intent if required
            if (intent == null) {
                // create empty intent
                intent = Intent()
            }


            // create the pending intent
            val pendingIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_ONE_SHOT)
            val channelId = context.getString(R.string.default_notification_channel_id)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher_foreground)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)

            // change notification style
            val style = NotificationCompat.BigTextStyle()
            style.bigText(message)
            notificationBuilder.setStyle(style)

            // show the notification
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(id, notificationBuilder.build())


            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelId,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_DEFAULT)
                notificationManager.createNotificationChannel(channel)
            }

            notificationManager.notify(id, notificationBuilder.build())

        }

        /**
         * Validate email address.
         *
         * @param email the email to validate
         * @return true if valid email or false.
         */
        fun isValidEmail(email: CharSequence): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }


        /**
         * Checks the device has a connection (not necessarily connected to the internet)
         *
         * @param ctx
         * @return
         */
        fun hasConnection(ctx: Context): Boolean {
            val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            if (wifiNetwork != null && wifiNetwork.isConnected) {
                return true
            }
            val mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if (mobileNetwork != null && mobileNetwork.isConnected) {
                return true
            }
            val activeNetwork = cm.activeNetworkInfo
            return if (activeNetwork != null && activeNetwork.isConnected) {
                true
            } else false
        }

        /**
         * Gets a formatted % percent from numerator/denominator values.
         *
         * @param numerator
         * @param denominator
         * @param locale      the locale of the returned string.
         * @return the formatted percent.
         */
        fun getPercent(numerator: Int, denominator: Int, locale: Locale): String {
            //http://docs.oracle.com/javase/tutorial/i18n/format/decimalFormat.html
            val nf = NumberFormat.getNumberInstance(locale)  //Locale.US, .....
            val df = nf as DecimalFormat
            df.applyPattern("###.#")
            if (denominator == 0) {
                return df.format(0) + "%"
            }
            val percent = numerator / denominator.toFloat() * 100
            return df.format(percent.toDouble()) + "%"
        }

        fun isEmpty(et: EditText): Boolean {
            return TextUtils.isEmpty(et.text.toString().trim { it <= ' ' })
        }

        /**
         * hide keyboard in edit text field
         */
        fun hideKeyboard(view: View) {
            val inputManager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }

        /**
         * hide keyboard in activity
         */
        fun hideKeyboard(activity: Activity) {
            activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }

        /**
         * hide keyboard in fragment
         */
        fun hideKeyboard(fragment: Fragment) {
            fragment.activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }


        fun showShortToast(context: Context, text: String) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }


        fun showShortToast(context: Context, textID: Int) {
            Toast.makeText(context, textID, Toast.LENGTH_SHORT).show()
        }

        fun showLongToast(context: Context, text: String) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }

        fun showLongToast(context: Context, textID: Int) {
            Toast.makeText(context, textID, Toast.LENGTH_LONG).show()
        }


        /**
         * Executes the given AsyncTask Efficiently.
         *
         * @param task the task to execute.
         */
        fun executeAsyncTask(task: AsyncTask<Void, Void, Void>) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            } else {
                task.execute()
            }
        }


        /**
         * converts the given timestamp to  readable date.
         *
         * @param timeStamp
         * @return
         */
        fun timeStampToString(timeStamp: Long, locale: Locale): String {
            val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", locale)
            return dateFormat.format(Date(timeStamp))
        }


        /**
         * Gets the app version code.
         *
         * @param context
         * @return the version code.
         */
        fun getAppVersionCode(context: Context): Int {
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                return packageInfo.versionCode
            } catch (e: PackageManager.NameNotFoundException) {
                // should never happen
                throw RuntimeException("Could not get package name: $e")
            }

        }


        /**
         * Get the application version name
         *
         * @param context
         * @return The app version name
         */
        fun getAppVersionName(context: Context): String {
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                return packageInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                // should never happen
                throw RuntimeException("Could not get package name: $e")
            }

        }


        /**
         * Set the app language
         *
         * @param ctx  the application context
         * @param lang the language as ar, en, .....
         */
        fun changeAppLocale(ctx: Context, lang: String) {
            val locale = Locale(lang)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            ctx.resources.updateConfiguration(config, ctx.resources.displayMetrics)
        }


        /**
         * Checks if a specified service is running or not.
         *
         * @param ctx          the context
         * @param serviceClass the class of the service
         * @return true if the service is running otherwise false
         */
        fun isServiceRunning(ctx: Context, serviceClass: Class<*>): Boolean {
            val manager = ctx.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
            return false
        }


        /**
         * method, used to check if list is null or empty
         *
         * @param list to check
         * @return boolean true if null or empty
         */
        fun isNullOrEmpty(list: List<*>?): Boolean {
            return list == null || list.isEmpty()
        }

        /**
         * method, used to check if arr is null or empty
         *
         * @param arr to check
         * @return boolean true if null or empty
         */
        fun isNullOrEmpty(arr: Array<Any>?): Boolean {
            return arr == null || arr.size == 0
        }


        /**
         * method, used to check if app is installed in the device or not
         *
         * @param context
         * @param appPackageName
         * @return
         */
        fun isAppInstalledAndEnabled(context: Context, appPackageName: String): Boolean {
            try {
                val pm = context.packageManager
                val info = pm.getPackageInfo(appPackageName, PackageManager.GET_ACTIVITIES)
                val installed = true
                val enabled = info.applicationInfo.enabled

                return installed && enabled
            } catch (e: PackageManager.NameNotFoundException) {
                return false
            }

        }

        /**
         * method, used to share text to specific app
         *
         * @param context
         * @param appPackageName
         * @param text
         * @return
         */
        fun shareTextToApp(context: Context, appPackageName: String, text: String): Boolean {
            try {
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                intent.setPackage(appPackageName)
                intent.putExtra(Intent.EXTRA_TEXT, text)
                context.startActivity(intent)
                return true
            } catch (e: Exception) {
                // app is not installed
                return false
            }

        }

        /**
         * method, used to convert string number to double number
         *
         * @param number
         * @return
         */
        fun convertToDouble(number: String): Double {
            try {
                return java.lang.Double.parseDouble(number)
            } catch (e: Exception) {
                e.printStackTrace()
                return 0.0
            }

        }

        /**
         * method, used to convert string number to int number
         *
         * @param number
         * @return
         */
        fun convertToInt(number: String): Int {
            try {
                return Integer.parseInt(number)
            } catch (e: Exception) {
                e.printStackTrace()
                return 0
            }

        }


        /**
         * method, used to match the regex on the passed text and return true or false
         *
         * @param regex
         * @param text
         * @return
         */
        fun matchRegex(regex: String, text: String): Boolean {
            val pattern = Pattern.compile(regex)
            val matcher = pattern.matcher(text)

            return matcher.matches()
        }


        /**
         * method, used to set the text underlined in the text view
         *
         * @param textView
         * @param text
         */
        fun setUnderlined(textView: TextView, text: String) {
            val spannable = SpannableString(text)
            spannable.setSpan(UnderlineSpan(), 0, text.length, 0)
            textView.text = spannable
        }


        /**
         * method, used to open the app page in play store
         *
         * @param context
         */
        fun getAppPlayStorePageIntent(context: Context): Intent {
            val packageName = context.packageName
            val url: String
            if (isAppInstalledAndEnabled(context, PACKAGE_PLAY_STORE)) {
                url = "market://details?id=$packageName"
            } else {
                url = "https://play.google.com/store/apps/details?id=$packageName"
            }
            return Intent(Intent.ACTION_VIEW, Uri.parse(url))
        }

        /**
         * method, used to open the app page in play store
         *
         * @param context
         */
        fun openAppInPlayStore(context: Context) {
            context.startActivity(getAppPlayStorePageIntent(context))
        }

        /**
         * method, used to get facebook profile url
         *
         * @param userId
         * @return
         */
        fun getFacebookProfileUrl(userId: String): String {
            return "https://www.facebook.com/$userId"
        }


        /**
         * method, check if the url is valid or not
         *
         * @param url
         * @return
         */
        fun isValidUrl(url: CharSequence): Boolean {
            return !TextUtils.isEmpty(url) && Patterns.WEB_URL.matcher(url).matches()
        }

        /**
         * method, used to convert a boolean to int
         *
         * @param b
         * @return
         */
        fun convertToInt(b: Boolean): Int {
            return if (b) 1 else 0
        }

        /**
         * method used to check if the recycler linear layout manager has reached the end or not
         *
         * @param layoutManager
         * @return
         */
        fun isReachedEndOfRecycler(layoutManager: LinearLayoutManager): Boolean {
            // get counts
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()

            // check total item count
            return if (totalItemCount == 0) {
                true
            } else {
                visibleItemCount + pastVisibleItems >= totalItemCount
            }
        }

        /**
         * method, used to check if reached end of the recycler or not
         *
         * @param recyclerView
         * @return
         */
        fun isReachedEndOfRecycler(recyclerView: RecyclerView): Boolean {
            return !recyclerView.canScrollVertically(1)
        }


        /**
         * method, used to convert int value to boolean
         *
         * @param value
         * @return
         */
        fun convertToBoolean(value: Int): Boolean {
            return value != 0
        }


        /**
         * method, used to play the default notification sound
         */
        fun playNotificationSound(context: Context) {
            try {
                val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val ringtone = RingtoneManager.getRingtone(context, notification)
                ringtone.play()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        /**
         * method, used to copy text to the clipboard
         *
         * @param context
         * @param text
         */
        fun copyToClipboard(context: Context, text: String) {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("", text)
            clipboard.primaryClip = clip
        }

        /**
         * method, used to encode a query param to be sent in the url
         *
         * @param param
         * @return
         */
        fun encodeQueryParam(param: String): String {
            var param = param
            try {
                param = URLEncoder.encode(param, "utf-8")
            } catch (e: UnsupportedEncodingException) {
            }

            return param
        }


        /**
         * method, used to get the app url in play store
         *
         * @param context
         * @return
         */
        fun getPlayStoreAppUrl(context: Context): String {
            var url = "https://play.google.com/store/apps/details?id="
            url += context.packageName

            return url
        }

        fun grtPlayStoreReferrerlAppUrl(context: Context, referrer: String): String {
            var url = "https://play.google.com/store/apps/details?id="
            url += context.packageName
            url += "&referrer=" + referrer
            return url
        }

        /**
         * method, used to share text
         *
         * @param context
         * @param text
         * @return
         */
        fun shareText(context: Context, text: String): Boolean {
            try {
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, text)
                context.startActivity(intent)
                return true
            } catch (e: Exception) {
                return false
            }

        }

        /**
         * Validate phone number.
         *
         * @param phoneNumber the phone number to validate
         * @return true if valid phone number or false.
         */
        fun isValidPhoneNumber(phoneNumber: String): Boolean {
            return matchRegex(REGEX_MOBILE_NUMBER, phoneNumber)
        }


        /**
         * method, used to get query value of a url
         *
         * @param url
         * @param key
         * @return
         */
        fun getQueryValue(url: String, key: String): String? {
            try {
                val uri = Uri.parse(url)
                return uri.getQueryParameter(key)
            } catch (e: Exception) {
                return null
            }

        }


        /**
         * method, used to return facebook profile picture url
         *
         * @param facebookUserId
         * @return
         */
        fun getFacebookProfilePictureUrl(facebookUserId: String): String {
            return "https://graph.facebook.com/$facebookUserId/picture?type=large"
        }

        /**
         * method, used to check if the password is strong or not
         *
         * @param password
         * @return
         */
        fun isStrongPassword(password: String): Boolean {
            return matchRegex(STRONG_PASSWORD_REGEX, password)
        }

        fun getFirstWord(data: String): String {
            val arr = data.split(" ".toRegex(), 2).toTypedArray()
            return arr[0]   //the
        }

        fun getVirsionCode(): Int {
            val versionCode = BuildConfig.VERSION_CODE
            return versionCode
        }

    }
}
