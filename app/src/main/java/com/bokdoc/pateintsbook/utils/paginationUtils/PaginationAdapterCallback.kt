package com.bokdoc.pateintsbook.utils.paginationUtils

/**
 * Created by wisyst on 2/19/2018.
 */

interface PaginationAdapterCallback {
    fun retryPageLoad()
}
