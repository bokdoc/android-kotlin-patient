package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import android.view.LayoutInflater
import android.view.View
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar
import kotlinx.android.synthetic.main.filter_child.view.*
import android.support.constraint.ConstraintSet


class CustomCrystalRangeSeekbar : ConstraintLayout {

    private lateinit var minTextView: TextView
    private lateinit var maxTextView: TextView
    private lateinit var rangeSeekbar: CrystalRangeSeekbar
    private lateinit var seekBar: CrystalSeekbar
    private var rangeListener: ((String, String) -> Unit)? = null
    private var extraString = ""
    // private var isRightPosition = false
    private lateinit var parent: ConstraintLayout

    var minRange: Float = 0.0f

    var maxRange: Float = 0.0f

    var minAppendText: String? = ""

    var maxAppenedText: String? = ""

    var rangerType: Int = 0
    var selectedValue: Float = 0.0f

    constructor(context: Context) : super(context) {
        setupViews()
    }

    constructor(context: Context, attrSets: AttributeSet) : super(context, attrSets) {
        //val a = context.obtainStyledAttributes(attrSets, R.styleable.CrystalRanger,0, 0)
        //rangerType = a.getInt(R.styleable.CrystalRanger_rangerTypes,0)
        setupViews()
    }

    constructor(context: Context, attrSets: AttributeSet, defStyle: Int) : super(context, attrSets, defStyle) {
        //val a = context.obtainStyledAttributes(attrSets, R.styleable.CrystalRanger, defStyle, 0)
        //rangerType = a.getInt(R.styleable.CrystalRanger_rangerTypes,0)
        setupViews()
    }

    private fun setupViews() {
        //isRightPosition = LocaleManager.isReverse(context)
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.ranger_view, this, true)
    }

    fun initializeMinMax(minRange: Float, maxRange: Float,selectedMin:Float=0f,selectedMax:Float=0f) {
        this.minRange = minRange
        this.maxRange = maxRange
        extraString = ""
        rangeSeekbar.setMinValue(minRange)
        rangeSeekbar.setMaxValue(maxRange)
        if(selectedMin!=0f){
            if(rangerType == DEFAULT){
                seekBar.minStartValue = selectedMin
                selectedValue = selectedMin
            }else{
                rangeSeekbar.setMinStartValue(selectedMin)
            }
        }
        if(selectedMax!=0f){
            rangeSeekbar.setMaxStartValue(selectedMax)
        }
        minTextView.text = getText(minRange, false, false)
        maxTextView.text = getText(maxRange)

    }

    fun initializeMinMaxForExperiance(minRange: String, maxRange: String,selectedMin: Float=0f,
                                      selectedMax:Float=0f) {
        this.minRange = minRange.toFloat()
        extraString = ""
        if (maxRange.contains("+", true)) {
            extraString = "+"
            this.maxRange = maxRange.replace("+", "").toFloat()
        } else {
            this.maxRange = maxRange.toFloat()
        }

        rangeSeekbar.setMinValue(minRange.toFloat())
        rangeSeekbar.setMaxValue(this.maxRange)
        minTextView.text = getText(minRange.toInt(), false, false)
        maxTextView.text = getText(this.maxRange)

    }

    fun setRangeSeekListener(rangeListener: (String, String) -> Unit) {
        this.rangeListener = rangeListener
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        minTextView = findViewById(R.id.minRangeTextView)
        maxTextView = findViewById(R.id.maxRangeTextView)
        seekBar = findViewById(R.id.seekBar)
        rangeSeekbar = findViewById(R.id.rangeSeekbar)
    }


    fun setRightPosition() {
        /*
        isRightPosition = LocaleManager.isReverse(context)
        seekBar.position = CrystalSeekbar.Position.RIGHT
        rangeSeekbar.layoutDirection = View.LAYOUT_DIRECTION_RTL
        */
    }


    fun setRangeType(rangeType: Int) {
        this.rangerType = rangerType
        if (rangeType == DEFAULT) {
            inflateMinMaxSeekbar()
            seekBar.visibility = View.VISIBLE
            seekBar.setOnSeekbarChangeListener {
                rangeListener?.invoke(it.toString(), "")
                minTextView.text = getText(it, false, false)
                selectedValue = it.toFloat()
            }
            rangeSeekbar.visibility = View.GONE
        } else {
            seekBar.visibility = View.GONE
            rangeSeekbar.visibility = View.VISIBLE
            rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                getMinMaxBasedOnPosition(minValue, maxValue)
            }
        }
        minTextView.visibility = View.VISIBLE
        maxTextView.visibility = View.VISIBLE
    }

    private fun inflateMinMaxSeekbar() {
        minTextView = findViewById(R.id.minSeekBarTextView)
        maxTextView = findViewById(R.id.maxSeekBarTextView)
    }


    private fun getText(value: Number, isMax: Boolean = false, isShowExtraString: Boolean = true): String {
        var valueStr = String.format("%.0f", value.toFloat())
        if (rangerType == RANGER_TIME) {
            valueStr = TimesDatesUtils.formatString(TimesDatesUtils.FORMAT_24_FLOAT_TYPE, valueStr
                    , TimesDatesUtils.FORMAT_12_AM_PM_TYPE)
        } else {
            if (isMax) {
                valueStr = context?.getString(R.string.placeHolder, valueStr, maxAppenedText)!!
            } else {
                if (isShowExtraString && extraString.isNotEmpty()) {
                    valueStr = context?.getString(R.string.placeHolderWithoutSpace, valueStr, extraString)!!
                }
                valueStr = context?.getString(R.string.placeHolder, valueStr, minAppendText)!!
            }
        }
        return valueStr
    }


    fun getMinMaxBasedOnPosition(minValue: Number, maxValue: Number) {
        var newMinValue = minValue.toFloat()
        var newMaxValue = maxValue.toFloat()
        /*
        if(isRightPosition){

                if (minValue.toFloat() != minRange && minTextView.text.toString().isNotEmpty()) {
                    newMinValue = maxRange.minus(newMinValue)
                    newMaxValue = getNumber(minTextView)
                }
                if (maxValue.toFloat() != maxRange && maxTextView.text.toString().isNotEmpty()) {
                    newMinValue = maxRange.minus(newMaxValue)
                    newMaxValue = getNumber(maxTextView)
                }
        }
        */

        if (rangerType == RANGER_TIME) {
            rangeListener?.invoke(TimesDatesUtils.formatString(TimesDatesUtils.FORMAT_24_FLOAT_TYPE, newMinValue.toString(), TimesDatesUtils.FORMAT_24_TYPE),
                    TimesDatesUtils.formatString(TimesDatesUtils.FORMAT_24_FLOAT_TYPE, newMaxValue.toString(), TimesDatesUtils.FORMAT_24_TYPE))
            minTextView.text = TimesDatesUtils.formatString(TimesDatesUtils.FORMAT_24_FLOAT_TYPE, newMinValue.toString(), TimesDatesUtils.FORMAT_12_AM_PM_TYPE)
            maxTextView.text = TimesDatesUtils.formatString(TimesDatesUtils.FORMAT_24_FLOAT_TYPE, newMaxValue.toString(), TimesDatesUtils.FORMAT_12_AM_PM_TYPE)
        } else {
            rangeListener?.invoke(newMinValue.toString(), newMaxValue.toString())
            minTextView.text = getText(newMinValue, false, false)
            maxTextView.text = getText(newMaxValue)
        }
    }

    fun getNumber(numberTextView: TextView): Float {
        return numberTextView.text.toString().split(" ")[0].toFloat()
    }


    companion object {
        const val DEFAULT = 0
        const val RANGER_DEFAULT = 1
        const val RANGER_TIME = 2
    }


}