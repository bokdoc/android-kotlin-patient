package com.bokdoc.pateintsbook.utils.ui


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.RecoverySystem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaActivity
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import kotlinx.android.synthetic.main.progressbar_dialog.*

class ProgressBarDialog(context: Context,
                        private val onCancelListener:()->Unit,
                        private val onRetryListener:()->Unit): AttachmentMediaActivity.OnProgressListener,AlertDialog(context){

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.progressbar_dialog)
       // window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        cancel.setOnClickListener {
            onCancelListener.invoke()
            dismiss()
        }
        retry.setOnClickListener {
            onRetryListener.invoke()
        }
        horizontalProgress.max = DEFAULT_PROGRESS_TOTAL
    }

    fun setProgressValue(progress: Double){
        horizontalProgress.progress = progress.toInt()
    }

    fun updateProgress(progress:Double){
        errorGroup.visibility = GONE
        horizontalProgress.progress = progress.toInt()
        progressText.text = StringsConstants.buildString(arrayListOf(progress.toInt().toString(),
                context.getString(R.string.percentage)))
    }

    fun stopProgressWithError(){
        horizontalProgress.progress = 0
        errorGroup.visibility = VISIBLE
    }

    override fun onProgress(progress: Double) {
       horizontalProgress.progress = progress.toInt()
    }


    companion object {
        const val DEFAULT_PROGRESS_TOTAL = 100
    }
}