package com.bokdoc.pateintsbook.utils.formatters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.util.TypedValue
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.webservice.MainFees
import com.bokdoc.pateintsbook.data.webservice.models.HealthProviders.resources.SurgeryFees
import com.bokdoc.pateintsbook.utils.FeesSpannable
import com.bokdoc.pateintsbook.utils.constants.StringsConstants


object FeesFormatter {

    fun setFees(mainFees: MainFees?,
                mainFeesTextViewFrom: TextView, mainFeesTextViewTo: TextView,
                subMainFees: MainFees?,
                subMainFeesTextViewFrom: TextView, subMainFeesTextViewTo: TextView,
                feesImage: ImageView) {

        setFees(mainFees, mainFeesTextViewFrom, mainFeesTextViewTo, feesImage)

        if (!isMainFeesHasValue(mainFees)) {
            mainFeesTextViewFrom.setTextColor(ContextCompat.getColor(mainFeesTextViewFrom.context,
                    R.color.colorDescription))
            mainFeesTextViewTo.setTextColor(ContextCompat.getColor(mainFeesTextViewFrom.context,
                    R.color.colorDescription))
            mainFeesTextViewFrom.textSize = mainFeesTextViewFrom.context.resources.getInteger(R.integer.value_9).toFloat()
            mainFeesTextViewTo.textSize = mainFeesTextViewFrom.context.resources.getInteger(R.integer.value_9).toFloat()

            setFees(subMainFees, mainFeesTextViewFrom, mainFeesTextViewTo, feesImage, R.color.colorAccent, true)
        } else {
            setFees(subMainFees, subMainFeesTextViewFrom, subMainFeesTextViewTo, feesImage, R.color.colorAccent, true)
        }

        if (isMainFeesHasValue(mainFees) || isMainFeesHasValue(subMainFees)) {
            feesImage.visibility = VISIBLE
        } else {
            feesImage.visibility = GONE
        }


    }

    private fun setFees(mainFees: MainFees?,
                        mainFeesTextViewFrom: TextView, mainFeesTextViewTo: TextView,
                        feesImage: ImageView, colorableTextColor: Int = R.color.colorAccent, isShowLabel: Boolean = false) {
        if (mainFees != null) {

            if (mainFees.amount != null) {
                mainFees.fixed_fees = SurgeryFees()
                mainFees.fixed_fees!!.amount = mainFees.amount
                if (mainFees.currencySymbol != null) {
                    mainFees.fixed_fees!!.currency_symbols = mainFees.currencySymbol
                }
            }
            if (mainFees.fixed_fees == null) {
                mainFees.fixed_fees = SurgeryFees().apply {
                    amount = "0"
                    currency_symbols = "Egp"
                }
            }
            when (mainFees.fixed_fees?.amount) {
                "", "0" -> {
                    if (mainFees.fees_from?.amount != null && mainFees.fees_from?.amount!!.isNotEmpty()

                            && mainFees.fees_from?.amount != "0" && mainFees.fees_to?.amount != null && mainFees.fees_to?.amount!!.isNotEmpty()
                            && mainFees.fees_to?.amount != "0") {
                        mainFeesTextViewFrom.visibility = View.VISIBLE
                        feesImage.visibility = VISIBLE
                        //mainFeesTextView.visibility = View.GONE
                        var feesFrom = ""
                        feesFrom = if (mainFees.label.isNotEmpty() && isShowLabel) {
                            mainFees.label + "  " + mainFeesTextViewFrom.context.getString(R.string.from)
                        } else {
                            mainFeesTextViewFrom.context.getString(R.string.feesFrom)
                        }
                        mainFeesTextViewFrom.text =
                                FeesSpannable.getSpannable(mainFeesTextViewFrom.context.getString(R.string.fees_texts_placeholder,
                                        feesFrom,
                                        mainFees.fees_from!!.amount + " " + mainFees.fees_from!!.currency_symbols,
                                        getFeesWithLabel(mainFees.fees_from!!.before_discount,
                                                mainFees.fees_from!!.currency_symbols,
                                                mainFeesTextViewFrom.context, isShowLabel)),
                                        mainFees.fees_from!!.amount + " " + mainFees.fees_from!!.currency_symbols,
                                        ContextCompat.getColor(mainFeesTextViewFrom.context, colorableTextColor),
                                        getFeesWithCurrency(mainFees.fees_from!!.before_discount,
                                                mainFees.fees_from!!.currency_symbols,
                                                mainFeesTextViewFrom.context))

                        mainFeesTextViewTo.visibility = View.VISIBLE

                        mainFeesTextViewTo.text =
                                FeesSpannable.getSpannable(mainFeesTextViewFrom.context.getString(R.string.fees_texts_placeholder,
                                        mainFeesTextViewFrom.context.getString(R.string.to),
                                        mainFees.fees_to!!.amount + " " + mainFees.fees_to!!.currency_symbols,
                                        getFeesWithLabel(mainFees.fees_to?.before_discount, mainFees.fees_to!!.currency_symbols,
                                                mainFeesTextViewFrom.context, isShowLabel)),
                                        mainFees.fees_to!!.amount + " " + mainFees.fees_to!!.currency_symbols,
                                        ContextCompat.getColor(mainFeesTextViewFrom.context, colorableTextColor),
                                        getFeesWithCurrency(mainFees.fees_to?.before_discount,
                                                mainFees.fees_to?.currency_symbols!!,
                                                mainFeesTextViewFrom.context))

                    } else {
                        mainFeesTextViewFrom.visibility = GONE
                        mainFeesTextViewTo.visibility = GONE
                        //feesImage.visibility = GONE
                    }
                }
                else -> {
                    feesImage.visibility = VISIBLE
                    mainFeesTextViewFrom.visibility = View.VISIBLE
                    var fees = ""
                    if (mainFees.label.isNotEmpty() && isShowLabel) {
                        fees = mainFees.label
                    } else {
                        fees = mainFeesTextViewFrom.context.getString(R.string.fees)
                    }
                    // mainFeesTextView.visibility = View.GONE
                    mainFeesTextViewFrom.text =
                            FeesSpannable.getSpannable(mainFeesTextViewFrom.context.getString(R.string.fees_texts_placeholder,
                                    fees,
                                    mainFees.fixed_fees?.amount + " " + mainFees.fixed_fees?.currency_symbols,
                                    getFeesWithLabel(mainFees.fixed_fees?.before_discount, mainFees.fixed_fees!!.currency_symbols,
                                            mainFeesTextViewFrom.context, isShowLabel)),
                                    mainFees.fixed_fees?.amount + " " + mainFees.fixed_fees?.currency_symbols,
                                    ContextCompat.getColor(mainFeesTextViewFrom.context, colorableTextColor),
                                    getFeesWithCurrency(mainFees.fixed_fees?.before_discount,
                                            mainFees.fixed_fees!!.currency_symbols,
                                            mainFeesTextViewFrom.context))

                    mainFeesTextViewTo.text = ""
                }
            }
        }
    }


    private fun updateInternationalFeesConstraint(subMainFeesTextViewLabel: TextView, feesIcon: ImageView) {
        try {
            val parent = subMainFeesTextViewLabel.parent as ConstraintLayout
            val constraintSet = ConstraintSet()
            constraintSet.clone(parent)

            constraintSet.connect(feesIcon.id, ConstraintSet.TOP, parent.id, ConstraintSet.TOP, 0)
            constraintSet.connect(feesIcon.id, ConstraintSet.BOTTOM, parent.id, ConstraintSet.BOTTOM, 0)

            constraintSet.applyTo(parent)

        } catch (e: Exception) {

        }

    }

    fun isMainFeesHasValue(mainFees: MainFees?): Boolean {
        return mainFees != null && mainFees.fees_from?.amount != null && mainFees.fees_from?.amount!!.isNotEmpty()
                && mainFees.fees_from?.amount != "0" && mainFees.fees_to?.amount != null && mainFees.fees_to?.amount!!.isNotEmpty()
                && mainFees.fees_to?.amount != "0" || (mainFees?.fixed_fees != null && mainFees.fixed_fees!!.amount != "0" &&
                mainFees.fixed_fees?.amount!!.isNotEmpty())
    }

    fun getValue(resource: Int, context: Context): Float {
        val outValue = TypedValue()
        context.resources.getValue(R.dimen.text_size_float_12, outValue, true)
        return outValue.float

    }


    private fun getFeesWithCurrency(fees: String?, currency: String?, context: Context): String {
        fees?.let {
            if (it.isNotEmpty() && it != "0")
                return context.getString(R.string.two_texts_placeholder,
                        fees, currency)
        }
        return StringsConstants.EMPTY
    }

    private fun getFeesWithLabel(fees: String?, currency: String?, context: Context,
                                 isShowLabel: Boolean = false, labelRes: Int = R.string.instead_of): String {
        val currencyFees = getFeesWithCurrency(fees, currency, context)
        if (isShowLabel) {
            currencyFees.let {
                if (it.isNotEmpty() && it != "0")
                    return context.getString(R.string.two_texts_placeholder,
                            context.getString(labelRes), currencyFees)
            }
        }
        return currencyFees
    }
}