package com.bokdoc.pateintsbook.utils.constants

object FilterSearchConstants {
    const val SORT_BY = "sort_by"
    const val MAP_VIEW = "map_view"
    const val PAGE = "page"
    const val TRUE = "true"
    const val ONE ="1"
}