package com.bokdoc.pateintsbook.utils.constants

object StringsConstants {
    const val COLON = ":"
    const val SPACE = " "
    const val OPEN_BRACKET = "("
    const val CLOSED_BRACKET = ")"
    const val COMMA: String = ","
    const val SLASH = "/"
    const val DOT = "."
    const val EMPTY = ""
    const val DASH  = "-"

    fun buildString(strings:List<String>):String{
        var string = ""
        strings.forEach {
            string = string.plus(it)
        }
        return string
    }

    fun buildStringWithSplitter(strings:List<String>,splitter:String):String{
        var string = ""
        strings.forEach {
            string = string.plus(it).plus(splitter)
        }
        return string
    }

    fun splitString(string:String,splitter: String):List<String>{
        return splitter.split(splitter)
    }
}