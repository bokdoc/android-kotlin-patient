package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.location.LocationManager
import android.support.v4.content.ContextCompat.getSystemService



object LocationEnabledChecker {
    fun isLocationEnabled(context: Context):Boolean{
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {

        }


        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        return gpsEnabled || networkEnabled
    }
}