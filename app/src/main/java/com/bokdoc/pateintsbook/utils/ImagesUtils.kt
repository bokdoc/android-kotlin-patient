package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.os.Environment
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class ImagesUtils {
    companion object {
        val simpleDateFormat = SimpleDateFormat("yyyymmddhhmmss",Locale("en"))

        fun getCompressed(context: Context, path:String):String{

            //getting device external cache directory, might not be available on some devices,
            // so our code fall back to internal storage cache directory, which is always available but in smaller quantity
            var cacheDir = context.externalCacheDir
            if(cacheDir == null)
            //fall back
                cacheDir = context.cacheDir

            val rootDir = cacheDir.getAbsolutePath() + "/ImageCompressor";
            val root = File(rootDir)

            //Create ImageCompressor folder if it doesnt already exists.
            if(!root.exists())
                root.mkdirs();

            //decode and resize the original bitmap from @param path.
            var bitmap = decodeImageFromFiles(path, /* your desired width*/1024, /*your desired height*/ 1024)

            bitmap = getScaledDownBitmap(bitmap,1024,false)

            //create placeholder for the compressed image file
            val compressed = File(root, simpleDateFormat.format(Date()) +bitmap.generationId+ ".jpg" /*Your desired format*/)

            //convert the decoded bitmap to stream
            val byteArrayOutputStream =  ByteArrayOutputStream()

            /*compress bitmap into byteArrayOutputStream
                Bitmap.compress(Format, Quality, OutputStream)
                Where Quality ranges from 1 - 100.
             */
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)

            /*
            Right now, we have our bitmap inside byteArrayOutputStream Object, all we need next is to write it to the compressed file we created earlier,
            java.io.FileOutputStream can help us do just That!
             */
            val fileOutputStream =  FileOutputStream(compressed)
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
            fileOutputStream.flush()

            fileOutputStream.close()

            //File written, return to the caller. Done!
            return compressed.absolutePath
        }


        fun getCompressed(context: Context, paths:ArrayList<String>):ArrayList<Uri>{
            val compressedPaths = ArrayList<Uri>()
            paths.forEach {
                if(!it.contains("pdf")) {
                    compressedPaths.add(Uri.parse(getCompressed(context, it)))
                }else{
                    compressedPaths.add(Uri.parse(it))
                }
            }
            return compressedPaths
        }



        private fun decodeImageFromFiles(path:String, width:Int, height:Int): Bitmap {
            val scaleOptions = BitmapFactory.Options()
            scaleOptions.inJustDecodeBounds = true
            BitmapFactory.decodeFile(path, scaleOptions)
            var scale = 1
            while (scaleOptions.outWidth / scale / 2 >= width
                    && scaleOptions.outHeight / scale / 2 >= height) {
                scale *= 2
            }
            // decode with the sample size
            val outOptions =  BitmapFactory.Options()
            outOptions.inSampleSize = scale
            return BitmapFactory.decodeFile(path, outOptions)
        }

        fun getScaledDownBitmap(bitmap: Bitmap, threshold: Int, isNecessaryToKeepOrig: Boolean): Bitmap {
            val width = bitmap.width
            val height = bitmap.height
            var newWidth = width
            var newHeight = height

            if (width > height && width > threshold) {
                newWidth = threshold
                newHeight = (height * newWidth.toFloat() / width).toInt()
            }

            if (width > height && width <= threshold) {
                //the bitmap is already smaller than our required dimension, no need to resize it
                return bitmap
            }

            if (width < height && height > threshold) {
                newHeight = threshold
                newWidth = (width * newHeight.toFloat() / height).toInt()
            }

            if (width < height && height <= threshold) {
                //the bitmap is already smaller than our required dimension, no need to resize it
                return bitmap
            }

            if (width == height && width > threshold) {
                newWidth = threshold
                newHeight = newWidth
            }

            return if (width == height && width <= threshold) {
                //the bitmap is already smaller than our required dimension, no need to resize it
                bitmap
            } else getResizedBitmap(bitmap, newWidth, newHeight, isNecessaryToKeepOrig)

        }

        private fun getResizedBitmap(bm: Bitmap, newWidth: Int, newHeight: Int, isNecessaryToKeepOrig: Boolean): Bitmap {
            val width = bm.width
            val height = bm.height
            val scaleWidth = newWidth.toFloat() / width
            val scaleHeight = newHeight.toFloat() / height
            // CREATE A MATRIX FOR THE MANIPULATION
            val matrix = Matrix()
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight)

            // "RECREATE" THE NEW BITMAP
            val resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true)
            if (!isNecessaryToKeepOrig) {
                bm.recycle()
            }
            return resizedBitmap
        }

        fun scaleBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap {
            val scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)

            val scaleX = newWidth / bitmap.width.toFloat()
            val scaleY = newHeight / bitmap.height.toFloat()
            val pivotX = 0f
            val pivotY = 0f

            val scaleMatrix = Matrix()
            scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY)

            val canvas = Canvas(scaledBitmap)
            canvas.setMatrix(scaleMatrix)
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, Paint(Paint.FILTER_BITMAP_FLAG))

            return scaledBitmap
        }

        fun createImageFile(context: Context): File {
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(Date())
            val imageFileName = "IMG_" + timeStamp + "_"
            val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val image = File.createTempFile(
                    imageFileName, /* prefix */
                    ".jpg", /* suffix */
                    storageDir      /* directory */
            )
            return image
        }

        fun scaleBitmap(context: Context, path:String):String{
            if(context == null)
                throw NullPointerException("Context must not be null.");
            //getting device external cache directory, might not be available on some devices,
            // so our code fall back to internal storage cache directory, which is always available but in smaller quantity
            var cacheDir = context.externalCacheDir
            if(cacheDir == null)
            //fall back
                cacheDir = context.cacheDir

            val rootDir = cacheDir.getAbsolutePath() + "/ImageCompressor";
            val root = File(rootDir)

            //Create ImageCompressor folder if it doesnt already exists.
            if(!root.exists())
                root.mkdirs()

            //decode and resize the original bitmap from @param path.
            var bitmap = decodeImageFromFiles(path, /* your desired width*/1024, /*your desired height*/ 1024)

            bitmap = getScaledDownBitmapWithSize(bitmap,1024,false)

            //create placeholder for the compressed image file
            val compressed = File(root, simpleDateFormat.format(Date()) + ".jpg" /*Your desired format*/)

            //convert the decoded bitmap to stream
            val byteArrayOutputStream =  ByteArrayOutputStream()

            /*compress bitmap into byteArrayOutputStream
                Bitmap.compress(Format, Quality, OutputStream)
                Where Quality ranges from 1 - 100.
             */
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)

            /*
            Right now, we have our bitmap inside byteArrayOutputStream Object, all we need next is to write it to the compressed file we created earlier,
            java.io.FileOutputStream can help us do just That!
             */
            val fileOutputStream =  FileOutputStream(compressed)
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
            fileOutputStream.flush()

            fileOutputStream.close()

            //File written, return to the caller. Done!
            return compressed.absolutePath
        }

        fun getScaledDownBitmapWithSize(bitmap: Bitmap, size: Int, isNecessaryToKeepOrig: Boolean): Bitmap {
            val width = bitmap.width
            val height = bitmap.height
            var newWidth = width
            var newHeight = height

            if (width > height) {
                newWidth = size
                newHeight = (height * newWidth.toFloat() / width).toInt()
            }


            if (width < height) {
                newHeight = size
                newWidth = (width * newHeight.toFloat() / height).toInt()
            }


            if (width == height) {
                newWidth = size
                newHeight = newWidth
            }

            return getResizedBitmap(bitmap, newWidth, newHeight, isNecessaryToKeepOrig)

        }


    }
}