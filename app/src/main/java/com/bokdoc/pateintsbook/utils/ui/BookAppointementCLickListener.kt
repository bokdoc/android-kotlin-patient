package com.bokdoc.pateintsbook.utils.ui

import android.app.Activity
import android.view.View
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.healthProviders.HealthProvidersProfile
import com.bokdoc.pateintsbook.navigators.Navigator
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsActivity
import java.lang.ref.WeakReference

class BookAppointementCLickListener(private val activity: WeakReference<Activity>,
                                    private val profile:HealthProvidersProfile):View.OnClickListener {

    override fun onClick(view: View?) {
        Navigator.navigate(activity.get()!!,view?.context?.getString(R.string.profileKey)!!, arrayListOf(profile),
                BookAppointmentsActivity::class.java)
    }

}