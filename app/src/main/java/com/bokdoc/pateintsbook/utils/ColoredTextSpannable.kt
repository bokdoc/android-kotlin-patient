package com.bokdoc.pateintsbook.utils

import android.text.Spannable
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan


object ColoredTextSpannable {

    fun getSpannable(text: String, colorableText: String, color: Int): Spannable {
        val span = Spannable.Factory.getInstance().newSpannable(text)
        span.setSpan(ForegroundColorSpan(color), text.indexOf(colorableText),
                text.indexOf(colorableText) + colorableText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return span
    }

    fun getSpannable(text: String, color: Int): Spannable {
        val span = Spannable.Factory.getInstance().newSpannable(text)
        span.setSpan(ForegroundColorSpan(color), text.indexOf(text),
                text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return span
    }

}