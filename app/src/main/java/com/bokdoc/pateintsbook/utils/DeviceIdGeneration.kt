package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.provider.Settings
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import java.util.*

object DeviceIdGeneration {
    fun generateDeviceId(context: Context, sharedPreferencesManger: SharedPreferencesManagerImpl): String {

        var deviceId = sharedPreferencesManger.getData(context.getString(R.string.deviceId), "")
        if (deviceId.isEmpty()) {
            deviceId = UUID.randomUUID().toString()
            sharedPreferencesManger.saveData(context.getString(R.string.deviceId), deviceId)
        }
        return deviceId
    }
}