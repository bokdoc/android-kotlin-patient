package com.bokdoc.pateintsbook.utils

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.Spannable
import com.bokdoc.pateintsbook.R

object PriceTextSpannable {
     fun getTextWithPrice(context: Context, price:Double, currentSymbols:String, stringRes:Int, color:Int = R.color.colorFees): Spannable {
        val price = context.getString(R.string.pricePlaceHolder,price.toString(),
                currentSymbols)
        val consulting = context.getString(stringRes,price)
        return ColoredTextSpannable.getSpannable(consulting,price, ContextCompat.
                getColor(context,color))
    }

    fun getTextWithPrice(context: Context, price:String, currentSymbols:String, stringRes:Int, color:Int = R.color.colorFees): Spannable {
        val price = context.getString(R.string.pricePlaceHolder,price,
                currentSymbols)
        val consulting = context.getString(stringRes,price)
        return ColoredTextSpannable.getSpannable(consulting,price, ContextCompat.
                getColor(context,color))
    }
}