package com.bokdoc.pateintsbook.utils.ui

import android.net.Uri
import android.view.View
import android.graphics.drawable.Drawable
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import com.bokdoc.pateintsbook.utils.constants.StringsConstants
import com.bokdoc.pateintsbook.utils.files.FilesUtils
import com.google.zxing.common.StringUtils
import java.io.File
import java.io.FileOutputStream


object ViewScreenShotHelper {
    fun takeScreenShot(view:View,imageName:String = "viewScreenShot.png"):Uri{
        val bitmap = getBitmapFromView(view)
        try {
            return FilesUtils.saveBitmap(bitmap,view.context.externalCacheDir,imageName)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Uri.EMPTY
    }

    private fun getBitmapFromView(view: View): Bitmap {
        val screenView = view.rootView
        screenView.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(screenView.drawingCache)
        screenView.isDrawingCacheEnabled = false
        return bitmap
    }
}