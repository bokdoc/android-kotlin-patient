package com.bokdoc.pateintsbook.utils.constants

object WebserviceConstants {
    const val AUTH = "auth"
    const val PROFILES = "profiles"
    const val SEARCH = "search"
    const val SLASH = "/"
    const val PATIENTS = "patients"
    const val PATIENT = "patient"
    const val PATIENTS_URL = PATIENTS + SLASH
    const val FILTER_OPTIONS = "filter-options"
    const val SEARCH_PROFILES_URL = "$PROFILES/$SEARCH"
    const val SEARCH_SORTS_BY_URL = "$SEARCH_PROFILES_URL/$FILTER_OPTIONS"
    const val PROFILES_WITH_SLASH = "$PROFILES$SLASH"
    const val QUERY_MARK = "?"
    const val AND = "&"
    const val COMMA = ","
    const val PASSWORD = "password"
    const val EDIT = "/edit"
    const val VERIFY_CODE = "verify-code"
    const val ADDRESS = "address"
    const val CONTACT_US = "contact-us/create"
    const val CONSULTINGDATA = "stream-consulting"
    const val Add_FCM = "auth/add-fcm-token"
    const val CHECK_UPDATE = "user-mobile-app/check-action"
    const val PAGE_QUERY = "page"
    const val SURGERIES = "/surgeries"
    const val CLINICS = "/clinics"
    const val REQUESTS = "requests/"
    const val CHECK_COUPON = "check-coupon"

    const val ADD_PROMO_CODE = PATIENTS_URL + REQUESTS + CHECK_COUPON


    const val PASSWORD_RECOVER = "password-recover"

}
