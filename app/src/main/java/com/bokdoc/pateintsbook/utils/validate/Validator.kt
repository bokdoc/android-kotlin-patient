package com.bokdoc.pateintsbook.utils.validate

import android.util.Patterns

object Validator {
    fun validateEmail(email:String):Boolean{
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}