package com.bokdoc.pateintsbook.utils.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.Window.FEATURE_NO_TITLE
import android.os.Bundle
import android.app.Activity
import android.content.DialogInterface
import android.view.Window
import com.bokdoc.pateintsbook.R
import kotlinx.android.synthetic.main.timer_picker_popup.*


class PickerPopup(context:Context,val data:Array<String>,val onDataSelected:(String)->Unit):android.support.v7.app.AlertDialog(context) {

     override fun onCreate(savedInstanceState: Bundle?) {
         requestWindowFeature(Window.FEATURE_NO_TITLE);
         setContentView(R.layout.timer_picker_popup)
         numberPicker.minValue = 0
         numberPicker.maxValue = data.size-1
         numberPicker.displayedValues = data
         NumberPickerTextColor.applyColor(context,R.color.colorBlack,numberPicker)
         ok.setOnClickListener {
             onDataSelected.invoke(data[numberPicker.value])
             dismiss()
         }
         cancel.setOnClickListener {
             dismiss()
         }
     }

}