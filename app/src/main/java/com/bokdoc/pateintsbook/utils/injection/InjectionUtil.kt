package com.bokdoc.pateintsbook.utils.injection

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.bokdoc.healthproviders.data.repositories.forgetPassword.ForgetPasswordRepository
import com.bokdoc.healthproviders.data.webservice.forgetPassword.ForgetPasswordWebservice
import com.bokdoc.pateintsbook.data.models.healthProviders.services.Service
import com.bokdoc.pateintsbook.data.repositories.attachmentMedia.AttachmentsMediaRepository
import com.bokdoc.pateintsbook.data.repositories.bookAppointment.BookAppointmentRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.awards.AwardsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accommodation.AccommodationRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.clinics.ClinicsRepository
import com.bokdoc.pateintsbook.data.repositories.common.StringsRepository
import com.bokdoc.pateintsbook.data.repositories.contactUs.ContactUsRepository
import com.bokdoc.pateintsbook.data.repositories.favoritesRepository.FavoritesRepository
import com.bokdoc.pateintsbook.data.repositories.linksCardsDetails.LinksCardsDetailsRepository
import com.bokdoc.pateintsbook.data.repositories.lookups.LookupsRepository
import com.bokdoc.pateintsbook.data.repositories.nextavailability.NextAvailabilityRepository
import com.bokdoc.pateintsbook.data.repositories.patientCompareRepository.CompareRepository
import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.DiseasesRepository
import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.MedicationsRepository
import com.bokdoc.pateintsbook.data.repositories.patientsMedicalHistory.SpecialitiesRepository
import com.bokdoc.pateintsbook.data.repositories.profileSocailActions.ProfileSocialActionsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.InsuranceCarriers.InsuranceCarriersRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.accreditation.AccreditationRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.consulting.ProfileConsultingRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.departments.DepartmentsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.doctors.DoctorsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileFeedback.ProfileFeedbackRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.profileGallery.ProfileGalleryRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.qualifications.QualificationsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.scans.ScansRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.surgeries.SurgeriesRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.services.tests.TestsRepository
import com.bokdoc.pateintsbook.data.repositories.providersProfileRepository.surgeries.ProfileSurgeriesRepository
import com.bokdoc.pateintsbook.data.repositories.requests.RequestsRepository
import com.bokdoc.pateintsbook.data.repositories.reviews.ReviewsRepository
import com.bokdoc.pateintsbook.data.repositories.twillio.TwillioRepository
import com.bokdoc.pateintsbook.data.repositories.updateProfile.UpdateProfileRepository
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.sharedPreferences.compare.CompareSharedPreferences
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.attachmentMedia.AttachmentMediaWebservice
import com.bokdoc.pateintsbook.data.webservice.bookAppointements.BookAppointmentsWebservice
import com.bokdoc.pateintsbook.data.webservice.contactUs.ContactUsWebService
import com.bokdoc.pateintsbook.data.webservice.editProfile.EditProfileWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.InsuranceCarriers.InsuranceCarriersWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accommodation.AccommodationWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.accreditation.AccreditationWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.awards.AwardsWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.clinics.ClinicsWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.departments.DepartmentsWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.doctors.DoctorsWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.overView.OverViewWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileFeedback.ProfileFeedbackWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.profileGallery.ProfileGalleryWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.qualifications.QualificationsWebService
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.scans.ScansWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.surgeries.SurgeriesWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.services.tests.TestsWebservice
import com.bokdoc.pateintsbook.data.webservice.healthProvidersProfile.surgeries.ProfileSurgeriesWebService
import com.bokdoc.pateintsbook.data.webservice.linksCardsDetails.LinksCardsDetailsWebservice
import com.bokdoc.pateintsbook.data.webservice.lookups.LookupsWebservice
import com.bokdoc.pateintsbook.data.webservice.multiPartsCreator.MultiPartCreator
import com.bokdoc.pateintsbook.data.webservice.nextAvailabilityTimes.NextavailabilityWebService
import com.bokdoc.pateintsbook.ui.bookAppointments.BookAppointmentsViewModel
import com.bokdoc.pateintsbook.data.webservice.patientCompares.CompareWebService
import com.bokdoc.pateintsbook.data.webservice.patientFavorites.FavoritesWebService
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.diseases.DiseasesWebservice
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.medications.MedicationsWebservice
import com.bokdoc.pateintsbook.data.webservice.patientMedicalHistory.specialities.SpecialitiesWebservice
import com.bokdoc.pateintsbook.data.webservice.patientRequests.RequestsWebService
import com.bokdoc.pateintsbook.data.webservice.profileSocailActions.ProfileSocailActionsWebservice
import com.bokdoc.pateintsbook.data.webservice.reviews.ReviewsWebService
import com.bokdoc.pateintsbook.data.webservice.twillio.TwillioWebService
import com.bokdoc.pateintsbook.ui.attachment.AttachmentMediaViewModel
import com.bokdoc.pateintsbook.ui.common.ViewModelsCustomProvider
import com.bokdoc.pateintsbook.ui.contactUs.ContactUsViewModel
import com.bokdoc.pateintsbook.ui.forgetPassword.ForgetPasswordViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation.AccommodationModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.accommodation.AccommodationViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.accreditation.AccreditationViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards.AwardsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.awards.AwardsViewModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.ClinicsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.ClinicsViewModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.clinics.details.ClinicsDetailsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.consulting.ConsultingViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments.DepartmentsModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.departments.DepartmentsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.doctors.DoctorsViewModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.insuranceCarriers.InsuranceCarriersViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewRepository
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.overviewProfile.OverViewViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback.FeedbackModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileFeedback.ProfileFeedbackViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery.ProfileGalleryModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.profileGallery.ProfileGalleryViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details.QualificationsDetailsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.qualifications.details.QualificationsViewModelFactory
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.ServicesViewModelProvider
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.services.surgeries.SurgeriesDetailsViewModel
import com.bokdoc.pateintsbook.ui.healthProvidersProfile.surgeries.SurgeriesViewModel
import com.bokdoc.pateintsbook.ui.linksCardsDetails.LinksCardsDetailsViewModel
import com.bokdoc.pateintsbook.ui.lookups.LookupsViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.disease.DiseaseViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.medications.MedicationsViewModel
import com.bokdoc.pateintsbook.ui.medicalHistory.speciality.SpecialitiesViewModel
import com.bokdoc.pateintsbook.ui.nextAvailability.NextAvailabilityViewModel
import com.bokdoc.pateintsbook.ui.patientCompares.CompareViewModel
import com.bokdoc.pateintsbook.ui.patientFavorites.FavoritesViewModel
import com.bokdoc.pateintsbook.ui.patientProfile.PatientProfileRepository
import com.bokdoc.pateintsbook.ui.patientProfile.PatientProfileViewModel
import com.bokdoc.pateintsbook.ui.patientRequests.RequestsViewModel
import com.bokdoc.pateintsbook.ui.patientReviews.ReviewsViewModel
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioViewModel
import com.bokdoc.pateintsbook.ui.updateProfile.UpdateProfileViewModel
import com.bokdoc.pateintsbook.utils.background.AppExecutors

object InjectionUtil {

    fun getAccommodationViewModel(activity: FragmentActivity): AccommodationViewModel {
        val accommodationWebservice = AccommodationWebservice(WebserviceManagerImpl.instance())
        val accommodationRepository = AccommodationRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), accommodationWebservice)
        return ViewModelProviders.of(activity, AccommodationModelProvider(accommodationRepository))
                .get(AccommodationViewModel::class.java)
    }

    fun getGalleryViewModel(activity: FragmentActivity): ProfileGalleryViewModel {
        val webservice = ProfileGalleryWebservice(WebserviceManagerImpl.instance())
        val repository = ProfileGalleryRepository(activity, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ProfileGalleryModelProvider(repository))
                .get(ProfileGalleryViewModel::class.java)
    }

    fun getFeedbackViewModel(activity: FragmentActivity): ProfileFeedbackViewModel {
        val webservice = ProfileFeedbackWebservice(WebserviceManagerImpl.instance())
        val repository = ProfileFeedbackRepository(activity, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, FeedbackModelProvider(repository))
                .get(ProfileFeedbackViewModel::class.java)
    }

    fun getDepartmentsViewModel(activity: FragmentActivity): DepartmentsViewModel {
        val webservice = DepartmentsWebservice(WebserviceManagerImpl.instance())
        val repository = DepartmentsRepository(activity, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, DepartmentsModelProvider(repository))
                .get(DepartmentsViewModel::class.java)
    }

    fun getDoctorsViewModel(activity: FragmentActivity): DoctorsViewModel {
        val webservice = DoctorsWebservice(WebserviceManagerImpl.instance())
        val repository = DoctorsRepository(activity.applicationContext, webservice, SharedPreferencesManagerImpl(activity))
        return ViewModelProviders.of(activity, DoctorsViewModelProvider(repository))
                .get(DoctorsViewModel::class.java)
    }

    fun getServicesViewModel(activity: FragmentActivity, type: String): ServicesViewModel {
        return if (type == Service.SCAN_TYPE) {
            val webservice = ScansWebservice(WebserviceManagerImpl.instance())
            val repository = ScansRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
            ViewModelProviders.of(activity, ServicesViewModelProvider(repository))
                    .get(ServicesViewModel::class.java)
        } else if (type == Service.SURGERY_TYPE) {
            val webservice = SurgeriesWebservice(WebserviceManagerImpl.instance())
            val repository = SurgeriesRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
            ViewModelProviders.of(activity, ViewModelsCustomProvider(ServicesViewModel(repository)))
                    .get(ServicesViewModel::class.java)
        } else {
            val webservice = TestsWebservice(WebserviceManagerImpl.instance())
            val repository = TestsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
            ViewModelProviders.of(activity, ServicesViewModelProvider(repository))
                    .get(ServicesViewModel::class.java)
        }
    }

    fun getAwardsViewModel(activity: FragmentActivity): AwardsViewModel {
        val webservice = AwardsWebservice(WebserviceManagerImpl.instance())
        val repository = AwardsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, AwardsViewModelProvider(repository))
                .get(AwardsViewModel::class.java)
    }

    fun getQualificationsViewModel(activity: FragmentActivity): QualificationsDetailsViewModel {
        val webservice = QualificationsWebService(WebserviceManagerImpl.instance())
        val repository = QualificationsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice, StringsRepository())
        return ViewModelProviders.of(activity, QualificationsViewModelFactory(repository))
                .get(QualificationsDetailsViewModel::class.java)
    }


    fun getClinicsViewModel(activity: FragmentActivity): ClinicsViewModel {
        val webservice = ClinicsWebservice(WebserviceManagerImpl.instance())
        val repository = ClinicsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ClinicsViewModelProvider(repository))
                .get(ClinicsViewModel::class.java)
    }


    fun getSurgeriesDetailsViewModel(activity: FragmentActivity): SurgeriesDetailsViewModel {
        val repository = StringsRepository()
        return ViewModelProviders.of(activity,
                ViewModelsCustomProvider(SurgeriesDetailsViewModel(activity.applicationContext, repository)))
                .get(SurgeriesDetailsViewModel::class.java)
    }

    fun getClinicsDetailsViewModel(activity: FragmentActivity): ClinicsDetailsViewModel {
        val repository = StringsRepository()
        return ViewModelProviders.of(activity,
                ViewModelsCustomProvider(ClinicsDetailsViewModel(activity.applicationContext, repository)))
                .get(ClinicsDetailsViewModel::class.java)
    }

    fun getBookAppointementsViewModel(activity: FragmentActivity): BookAppointmentsViewModel {
        val repository = StringsRepository()
        val bookRepository = BookAppointmentRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity),
                BookAppointmentsWebservice(WebserviceManagerImpl.instance()))
        val userRepository = UserSharedPreference(activity.applicationContext)
        val linksCardsDetailsRepository = LinksCardsDetailsRepository(activity.applicationContext,
                SharedPreferencesManagerImpl(activity.applicationContext), LinksCardsDetailsWebservice(WebserviceManagerImpl.instance()))
        return ViewModelProviders.of(activity,
                ViewModelsCustomProvider(BookAppointmentsViewModel(activity.applicationContext, repository,
                        bookRepository, userRepository, linksCardsDetailsRepository)))
                .get(BookAppointmentsViewModel::class.java)
    }

    fun getFavoriteViewModel(activity: FragmentActivity): FavoritesViewModel {
        val webservice = FavoritesWebService(WebserviceManagerImpl.instance())
        val profileSocailActionsWebservice = ProfileSocailActionsWebservice(WebserviceManagerImpl.instance())
        val favoritesRepository = FavoritesRepository(activity.applicationContext, webservice, UserSharedPreference(activity))
        val profileSocialActionsRepository = ProfileSocialActionsRepository(activity.applicationContext,
                SharedPreferencesManagerImpl(activity), CompareSharedPreferences(activity), profileSocailActionsWebservice)

        return ViewModelProviders.of(activity,
                ViewModelsCustomProvider(FavoritesViewModel(favoritesRepository, profileSocialActionsRepository)))
                .get(FavoritesViewModel::class.java)
    }

    fun getComparesViewModel(activity: FragmentActivity): CompareViewModel {
        val webservice = CompareWebService(WebserviceManagerImpl.instance())
        val repository = CompareRepository(activity.applicationContext, webservice, UserSharedPreference(activity), CompareSharedPreferences(activity))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(CompareViewModel(repository)))
                .get(CompareViewModel::class.java)
    }

    fun getReviewsViewModel(activity: FragmentActivity): ReviewsViewModel {
        val webservice = ReviewsWebService(WebserviceManagerImpl.instance())
        val repository = ReviewsRepository(activity.applicationContext, webservice, UserSharedPreference(activity))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(ReviewsViewModel(repository))).get(ReviewsViewModel::class.java)
    }

    fun getRequestsViewModel(activity: FragmentActivity): RequestsViewModel {
        val webservice = RequestsWebService(WebserviceManagerImpl.instance())
        val repository = RequestsRepository(activity.applicationContext, webservice, UserSharedPreference(activity))
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(RequestsViewModel(repository))).get(RequestsViewModel::class.java)
    }

    fun getRequestsViewModel(fragment: Fragment,application:Context): RequestsViewModel {
        val webservice = RequestsWebService(WebserviceManagerImpl.instance())
        val repository = RequestsRepository(application, webservice,
                UserSharedPreference(application))
        return ViewModelProviders.of(fragment, ViewModelsCustomProvider(RequestsViewModel(repository))).get(RequestsViewModel::class.java)
    }

    fun getAttachmentMediaViewModel(activity: FragmentActivity): AttachmentMediaViewModel {
        val webservice = AttachmentMediaWebservice(WebserviceManagerImpl.instance(), MultiPartCreator())
        val repository = AttachmentsMediaRepository(activity.applicationContext, UserSharedPreference(activity),
                webservice)
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(AttachmentMediaViewModel(repository,activity.application, AppExecutors())))
                .get(AttachmentMediaViewModel::class.java)
    }

    fun getLookUpsViewModel(activity: FragmentActivity): LookupsViewModel {
        val webservice = LookupsWebservice(WebserviceManagerImpl.instance())
        val repository = LookupsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity),
                webservice)
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(LookupsViewModel(repository)))
                .get(LookupsViewModel::class.java)
    }

    fun getDiseaseViewModel(activity: FragmentActivity): DiseaseViewModel {
        val diseasesWebservice = DiseasesWebservice(WebserviceManagerImpl.instance())
        val diseasesRepository = DiseasesRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity.applicationContext)
                , diseasesWebservice)

        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(DiseaseViewModel(diseasesRepository)))
                .get(DiseaseViewModel::class.java)
    }

    fun getSpecialitiesViewModel(activity: FragmentActivity): SpecialitiesViewModel {
        val specialitiesWebservice = SpecialitiesWebservice(WebserviceManagerImpl.instance())
        val specialitiesRepository = SpecialitiesRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity.applicationContext)
                , specialitiesWebservice)

        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(SpecialitiesViewModel(specialitiesRepository))).get(SpecialitiesViewModel::class.java)
    }

    fun getMei(activity: FragmentActivity): SpecialitiesViewModel {
        val specialitiesWebservice = SpecialitiesWebservice(WebserviceManagerImpl.instance())
        val specialitiesRepository = SpecialitiesRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity.applicationContext)
                , specialitiesWebservice)

        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(SpecialitiesViewModel(specialitiesRepository))).get(SpecialitiesViewModel::class.java)
    }

    fun getMedicationsViewModel(activity: FragmentActivity): MedicationsViewModel {
        val webservice = MedicationsWebservice(WebserviceManagerImpl.instance())
        val repository = MedicationsRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity.applicationContext)
                , webservice)

        return ViewModelProviders.of(activity, ViewModelsCustomProvider(MedicationsViewModel(repository))).get(MedicationsViewModel::class.java)
    }

    fun getPatientsViewModel(activity: FragmentActivity): PatientProfileViewModel {

        val favoritesRepository = FavoritesRepository(activity.applicationContext, FavoritesWebService(WebserviceManagerImpl()),
                UserSharedPreference(activity))
        val comparesRepository = CompareRepository(activity.applicationContext, CompareWebService(WebserviceManagerImpl()),
                UserSharedPreference(activity), CompareSharedPreferences(activity))

        val requestsRepository = RequestsRepository(activity.applicationContext, RequestsWebService(WebserviceManagerImpl()),
                UserSharedPreference(activity))

        val attachmentRepository = AttachmentsMediaRepository(activity.applicationContext,
                UserSharedPreference(activity.applicationContext), AttachmentMediaWebservice(WebserviceManagerImpl(), MultiPartCreator())
        )

        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(PatientProfileViewModel(PatientProfileRepository(favoritesRepository,
                requestsRepository, comparesRepository, attachmentRepository)))).get(PatientProfileViewModel::class.java)
    }

    fun getPOverViewViewModel(activity: FragmentActivity): OverViewViewModel {
        val overViewWebservice = OverViewWebService(WebserviceManagerImpl.instance())
        val overViewRepository = OverViewRepository(activity, overViewWebservice, UserSharedPreference(activity.applicationContext))
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(OverViewViewModel(activity.application, overViewRepository))).get(OverViewViewModel::class.java)
    }

    fun getLinkCardDetailsViewModel(activity: FragmentActivity): LinksCardsDetailsViewModel {
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(LinksCardsDetailsViewModel(LinksCardsDetailsRepository(activity.applicationContext,
                SharedPreferencesManagerImpl(activity.applicationContext), LinksCardsDetailsWebservice(WebserviceManagerImpl.instance())),
                activity.application)))
                .get(LinksCardsDetailsViewModel::class.java)
    }

    fun getSurgeriesViewModel(activity: FragmentActivity): SurgeriesViewModel {
        val webservice = ProfileSurgeriesWebService(WebserviceManagerImpl.instance())
        val repository = ProfileSurgeriesRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(SurgeriesViewModel(repository))).get(SurgeriesViewModel::class.java)


    }

    fun getConsultingViewModel(activity: FragmentActivity): ConsultingViewModel {
        val webservice = ProfileSurgeriesWebService(WebserviceManagerImpl.instance())
        val repository = ProfileConsultingRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(ConsultingViewModel(repository))).get(ConsultingViewModel::class.java)


    }


    fun getInsuranceCarriersViewModel(activity: FragmentActivity): InsuranceCarriersViewModel {
        val webservice = InsuranceCarriersWebService(WebserviceManagerImpl.instance())
        val repository = InsuranceCarriersRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(InsuranceCarriersViewModel(repository))).get(InsuranceCarriersViewModel::class.java)


    }

    fun getAccreditationViewModel(activity: FragmentActivity): AccreditationViewModel {
        val webservice = AccreditationWebService(WebserviceManagerImpl.instance())
        val repository = AccreditationRepository(activity.applicationContext, SharedPreferencesManagerImpl(activity), webservice)
        return ViewModelProviders.of(activity, ViewModelsCustomProvider(AccreditationViewModel(repository))).get(AccreditationViewModel::class.java)

    }

    fun getForgetPasswordViewModel(activity: FragmentActivity): ForgetPasswordViewModel {
        val forgetPasswordRepository = ForgetPasswordRepository(ForgetPasswordWebservice(WebserviceManagerImpl.instance()))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(ForgetPasswordViewModel(forgetPasswordRepository))).get(ForgetPasswordViewModel::class.java)
    }

    fun getUpdateProfileViewModel(activity: FragmentActivity): UpdateProfileViewModel {
        val updateProfileRepository = UpdateProfileRepository(activity.applicationContext, UserSharedPreference(activity.applicationContext)
                , EditProfileWebService(WebserviceManagerImpl.instance()))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(UpdateProfileViewModel(updateProfileRepository))).get(UpdateProfileViewModel::class.java)
    }

    fun getContactUsViewModel(activity: FragmentActivity): ContactUsViewModel {
        val contactUseRepo = ContactUsRepository(ContactUsWebService(WebserviceManagerImpl.instance()), UserSharedPreference(activity.applicationContext))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(ContactUsViewModel(contactUseRepo))).get(ContactUsViewModel::class.java)
    }


    fun getNextAvailabilitiesViewModel(activity: FragmentActivity): NextAvailabilityViewModel {
        val nextAvailabilityRepository = NextAvailabilityRepository(activity.applicationContext
                , SharedPreferencesManagerImpl(activity.applicationContext), NextavailabilityWebService(WebserviceManagerImpl.instance()))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(NextAvailabilityViewModel(nextAvailabilityRepository))).get(NextAvailabilityViewModel::class.java)
    }

    fun getTwillioViewModel(activity: FragmentActivity): TwillioViewModel {
        val twillioRepository = TwillioRepository(activity.applicationContext, TwillioWebService(WebserviceManagerImpl.instance()), UserSharedPreference(activity.applicationContext))
        return ViewModelProviders.of(activity
                , ViewModelsCustomProvider(TwillioViewModel(twillioRepository))).get(TwillioViewModel::class.java)
    }

}