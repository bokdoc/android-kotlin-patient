package com.bokdoc.pateintsbook.utils.ui

import android.content.Context
import android.content.res.Configuration
import java.util.*
import android.util.DisplayMetrics
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.SharedPreferencesManagerImpl
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference


class LocaleManager {

    companion object {
        fun setLocale(context: Context): Context {
            val sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(context)
            return setNewLocale(context, sharedPreferencesManagerImpl.getData(context.getString(R.string.language_key),Locale.getDefault().language))
        }

        private fun setNewLocale(context: Context, language: String): Context {
            //persistLanguage(context, language)
            Locale.setDefault(Locale(language))
            return updateResources(context, language)
        }

        private fun updateResources(context: Context, language: String): Context {
            val res = context.resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.setLocale(Locale(language)) // API 17+ only.
            // Use conf.locale = new Locale(...) if targeting lower versions
            res.updateConfiguration(conf, dm)
            return context.createConfigurationContext(conf)
        }

        fun isReverse(context: Context):Boolean{
            val sharedPreferencesManagerImpl = SharedPreferencesManagerImpl(context)
            return sharedPreferencesManagerImpl.getData(context.getString(R.string.language_key),"ar") == "ar"
        }
    }




}