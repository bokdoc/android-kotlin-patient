package com.bokdoc.pateintsbook.utils.constants

object SearchScreenTypes {
    const val APPOINTMENTS = "appointment"
    const val SURGERIES = "surgery"
    const val CONSULTING = "online_consulting"

    const val CONSULTING_DISPLAY = "onlineConsulting"

    const val APPOINTMENTS_POSITION = 1
    const val SURGERIES_POSITION = 2
    const val CONSULTING_POSITION = 0
    val values = arrayOf(CONSULTING, APPOINTMENTS, SURGERIES)

    fun getPosition(value:String):Int{
        return when(value){
            APPOINTMENTS -> APPOINTMENTS_POSITION
            SURGERIES -> SURGERIES_POSITION
            CONSULTING -> CONSULTING_POSITION
            else -> -1
        }
    }

    fun getString(position:Int):String{
        return when(position){
            APPOINTMENTS_POSITION -> APPOINTMENTS
            SURGERIES_POSITION -> SURGERIES
            CONSULTING_POSITION -> CONSULTING
            else -> ""
        }
    }
}