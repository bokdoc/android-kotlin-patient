package com.bokdoc.pateintsbook.utils.paginationUtils


import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.bokdoc.pateintsbook.data.models.meta.Pagination


abstract class PaginationScrollListener(var layoutManager: LinearLayoutManager,var pagnation:Pagination? = null) : RecyclerView.OnScrollListener() {

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        if(pagnation!=null && pagnation?.totalPages!=null){
            if ((visibleItemCount + firstVisibleItemPosition >= totalItemCount) && firstVisibleItemPosition >= 0) {
                if (pagnation!!.currentPage!=1 && pagnation!!.currentPage!!<=pagnation!!.totalPages!!) {
                    loadMoreItems()
                }
            }

        }else {
            if (!isLoading() && !isLastPage()) {
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    loadMoreItems()
                }
            }
        }
    }

    abstract fun loadMoreItems()

}
