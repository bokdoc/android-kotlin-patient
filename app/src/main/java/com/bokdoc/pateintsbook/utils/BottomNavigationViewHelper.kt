package com.bokdoc.pateintsbook.utils

import android.annotation.SuppressLint
import android.support.design.R.id.largeLabel
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.widget.TextView



object BottomNavigationViewHelper {
    @SuppressLint("RestrictedApi")
    fun disableShiftMode(navigationView: BottomNavigationView){
        val menuView = navigationView.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode");
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            val itemCount = menuView.childCount
            var item:BottomNavigationItemView
            (0 until itemCount).forEach {
                item = menuView.getChildAt(it) as BottomNavigationItemView
                item.setShifting(false)
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.itemData.isChecked)

            }
        } catch (e:NoSuchFieldException ) {
            Log.e("BNVHelper", "Unable to get shift mode field", e)
        } catch (e:IllegalAccessException) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e)
        }
    }
}