package com.bokdoc.pateintsbook.utils

import android.icu.util.ULocale.getLanguage
import com.hanks.htextview.base.DisplayUtils.getDisplayMetrics
import android.content.Context.MODE_PRIVATE
import android.app.Activity
import android.content.Context
import java.util.*


object LanguageHelper {
    private var DEFAULT_LANG = "ar"

    fun applyLanguage(context: Context) {
        val locale = Locale("ar")
        Locale.setDefault(locale)
        val resources = context.resources
        val configuration = resources.configuration
        configuration.setLocale(locale)
        resources.updateConfiguration(configuration, resources.displayMetrics)
    }

}