package com.bokdoc.pateintsbook.utils.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.view.View.VISIBLE
import android.view.Window
import android.view.WindowManager
import com.bokdoc.pateintsbook.R
import kotlinx.android.synthetic.main.dialog_numbers.*


class PercentageDialog(context: Context?) : AppCompatDialog(context) {

    var selectedValue:String = ""
    var selectedValues:ArrayList<String> = ArrayList()
    var callback: OnDialogClickedListener? = null
    var message = ""



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.dialog_numbers)

        if(message.isNotEmpty()) {
            tv_message.visibility = VISIBLE
            tv_message.text = message
        }

        if(selectedValues.isEmpty()) {
            /*
            rv_numbers.adapter = PercentageAdapter(context.resources.getStringArray(R.array.referral_percentages),
                    this::onSelectNum,selectedValue)
                    */
        }else{
            rv_numbers.adapter = PercentageAdapter(selectedValues.toTypedArray(),this::onSelectNum,selectedValue)
        }

    }

    fun updateList(){
        rv_numbers.adapter!!.notifyDataSetChanged()
    }

    fun onSelectNum(num: String, position: Int) {
         callback!!.onDialogClicked(position, num)
         dismiss()
    }



    override fun show() {

        super.show()

        val layoutParams = WindowManager.LayoutParams()

        layoutParams.copyFrom(window!!.attributes)

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT

        window!!.attributes = layoutParams



        if(rv_numbers.adapter!=null) {
            (rv_numbers.adapter as PercentageAdapter).let {
                it.inSelectedValue = selectedValue
                it.notifyDataSetChanged()
            }
        }


    }



    fun setOnDialogClickedListener(l: PercentageDialog.OnDialogClickedListener) {

        callback = l

    }



    interface OnDialogClickedListener {

        fun onDialogClicked(position: Int, name: String)

    }
 }