package com.bokdoc.pateintsbook.utils.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.ImageTextRow
import com.bokdoc.pateintsbook.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_image_text_row.view.*
import java.lang.ref.WeakReference

class CustomListPopupMenu(context: Context, anchorView: View, textsImages: ArrayList<ImageTextRow>) :
        android.support.v7.widget.ListPopupWindow(context) {

    var selectedListener: ((ImageTextRow) -> Unit)? = null
    lateinit var selectedImageView:ImageView

    init {
        setAnchorView(anchorView)
        // this will change in future to calculate dynamic
        width = 300
        height = 400
        isModal = true
        setAdapter(CustomMenuAdapter(context,textsImages))
    }

      inner class CustomMenuAdapter(context: Context,textsImages:ArrayList<ImageTextRow>):
            ArrayAdapter<ImageTextRow>(context,0,textsImages){

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view:View? = convertView
            if(convertView==null) {
                view = parent?.inflateView(R.layout.item_image_text_row)
                view?.setOnClickListener {
                   selectedListener?.invoke(getItem(position))
                    selectedImageView = view.image
                    dismiss()
                }
            }

            val imageTextRow = getItem(position)
            view?.findViewById<ImageView>(R.id.image)?.setImageResource(imageTextRow.image)
            view?.findViewById<TextView>(R.id.text)?.text = imageTextRow.text

            return view!!
        }
    }
}