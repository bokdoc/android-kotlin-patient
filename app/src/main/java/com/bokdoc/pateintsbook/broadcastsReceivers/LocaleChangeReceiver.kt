package com.bokdoc.pateintsbook.broadcastsReceivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import java.util.*

class LocaleChangeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if(context!=null)
       LocaleManager.setLocale(context)
    }

}