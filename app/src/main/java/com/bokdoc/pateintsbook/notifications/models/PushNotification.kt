package com.bokdoc.pateintsbook.notifications.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.json.JSONObject

class PushNotification {

    lateinit var id: String
    lateinit var title: String
    lateinit var body: String

    lateinit var token: String

    var params: JSONObject = JSONObject()

    @SerializedName("screen_type")
    @Expose
    lateinit var screenType: String

    @SerializedName("url")
    @Expose
    lateinit var url: String
}
