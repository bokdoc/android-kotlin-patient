package com.bokdoc.pateintsbook.notifications.notificationParser

import com.bokdoc.pateintsbook.notifications.models.PushNotification
import com.google.gson.Gson
import org.json.JSONObject

object NotificationParser {

    fun parseNotification(json: String): PushNotification {
        val notification = PushNotification()
        val data = JSONObject(json)

        notification.screenType = data.getString("screen_type").toString()

        if (data.has("url"))
            notification.url = data.has("url").toString()

        if (data.has("id"))
            notification.id = data.getString("id").toString()

        if (data.has("params"))
            notification.params = data.getJSONObject("params")


        if (data.has("token"))
            notification.token = data.getString("token").toString()

        return notification
    }
}