package com.bokdoc.pateintsbook.notifications

import android.content.ContentValues.TAG
import android.content.Intent
import android.util.Log
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.sharedPreferences.UserSharedPreference
import com.bokdoc.pateintsbook.data.webservice.WebserviceManagerImpl
import com.bokdoc.pateintsbook.data.webservice.addFcm.AddFcmWebService
import com.bokdoc.pateintsbook.notifications.models.PushNotification
import com.bokdoc.pateintsbook.notifications.notificationParser.NotificationParser
import com.bokdoc.pateintsbook.ui.WebViewActivity
import com.bokdoc.pateintsbook.ui.homeMenuList.MainActivity
import com.bokdoc.pateintsbook.ui.searchResults.SearchResultsActivity
import com.bokdoc.pateintsbook.ui.splash.SplashRepository
import com.bokdoc.pateintsbook.ui.twilioVideoConsulting.TwillioLoadingActivity
import com.bokdoc.pateintsbook.utils.Utils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private lateinit var sharedPrefrenceManger: UserSharedPreference
    private lateinit var splashRepository: SplashRepository

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.d("token", "Refreshed token: $token")
        sharedPrefrenceManger = UserSharedPreference(this)
        splashRepository = SplashRepository(this, sharedPrefrenceManger, AddFcmWebService(WebserviceManagerImpl()))
        if (token != null)
            sharedPrefrenceManger.sharedPrefrenceManger.saveData(getString(R.string.fcm_token), token)

        if (sharedPrefrenceManger.getUser() != null && !sharedPrefrenceManger.getUser()?.token.isNullOrEmpty()) {
            splashRepository.addFcmToken()
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if (remoteMessage != null && remoteMessage.data.isNotEmpty()) {
            try {
                val notification = NotificationParser.parseNotification(remoteMessage.data.toString())
                sendPushNotification(notification, remoteMessage)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        }
    }


    private fun sendPushNotification(notification: PushNotification, remoteMessage: RemoteMessage) {
        //    LocaleManager.setLocale(baseContext)
        when (notification.screenType) {
            PAYMENT_TYPE_MESSAGE -> {
                var paymentUrl = "https://payment.bokdoc.com/view-options/"
                paymentUrl += notification.params.get("id").toString() + "?is_payment=true"

                val intent = Intent(this, WebViewActivity::class.java)
                intent.putExtra(getString(R.string.url_key), paymentUrl)
                intent.putExtra(getString(R.string.title_key), getString(R.string.payment))

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }

            SCREEN_TYPE_REQUEST -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(getString(R.string.requests_screen), getString(R.string.requests_screen))
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
            SEARCH_TYPE_MESSAGE -> {
                val intent = Intent(this, SearchResultsActivity::class.java)
                intent.putExtra(getString(R.string.screenType), "online_consulting")
                intent.putExtra(getString(R.string.query), "")
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
            NOTIFICATION_TYPE_MESSAGE -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(getString(R.string.notification_screen), getString(R.string.notification_screen))
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
            SCREEN_TYPE_MESSAGE -> {
                val intent = Intent(this, WebViewActivity::class.java)
                intent.putExtra(getString(R.string.url_key), notification.url)
                intent.putExtra(getString(R.string.title_key), getString(R.string.chat_with_doctor))
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
            CONSULTING_TYPE -> {
                val intent = Intent(this, TwillioLoadingActivity::class.java)
                intent.putExtra(getString(R.string.request_id_key), notification.params.get("id").toString())
                intent.putExtra(getString(R.string.twillio_token_key), notification.params.get("token").toString())
                intent.putExtra(getString(R.string.is_dialing), getString(R.string.no_dialing))
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
            CONSULT_NOW -> {
                val intent = Intent(this, TwillioLoadingActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra(getString(R.string.request_id_key), notification.params.get("id").toString())
                intent.putExtra(getString(R.string.twillio_token_key), notification.params.get("token").toString())
                intent.putExtra(getString(R.string.is_dialing), getString(R.string.is_dialing))
                startActivity(intent)
            }
            else -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                Utils.showNotification(this, 1, remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), intent)
            }
        }


    }


    companion object {
        const val SCREEN_TYPE_REQUEST = "list_requests"
        const val SCREEN_TYPE_MESSAGE = "message"
        const val PAYMENT_TYPE_MESSAGE = "payment"
        const val SEARCH_TYPE_MESSAGE = "search"
        const val CONSULT_NOW = "consult_now"
        const val CONSULTING_TYPE = "consulting_room"
        const val NOTIFICATION_TYPE_MESSAGE = "notifications_list"
    }

}