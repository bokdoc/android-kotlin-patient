package com.bokdoc.pateintsbook.navigators

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import com.bokdoc.pateintsbook.R

object ShareNavigator {
    fun shareImage(context: Context,uri: Uri,shareMessage:Int = R.string.messageDefaultShareImage){
        val intent = Intent(Intent.ACTION_SEND);
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
        intent.putExtra(Intent.EXTRA_STREAM,uri)
        intent.type = "image/png"
        context.startActivity(Intent.createChooser(intent,context.getString(shareMessage)))
    }
}