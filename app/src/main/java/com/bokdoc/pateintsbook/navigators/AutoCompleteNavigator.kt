package com.bokdoc.pateintsbook.navigators


import android.app.Activity
import android.content.Intent
import android.support.v4.app.Fragment
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.ui.autoCompleteResults.AutoCompleteResultsActivity

object AutoCompleteNavigator {
    fun navigate(fragment: Fragment, keys:Array<String>, screenType:String, completeType:Int, requestType:Int,searchHint:String=""){
        val intent = Intent(fragment.activity, AutoCompleteResultsActivity::class.java)
        intent.putExtra(keys[0],screenType)
        intent.putExtra(keys[1],completeType)
        intent.putExtra(fragment.getString(R.string.hint_key),searchHint)
        fragment.startActivityForResult(intent,requestType)
    }
    fun navigate(activity: Activity, keys:Array<String>, screenType:String, completeType:Int, requestType:Int){
        val intent = Intent(activity, AutoCompleteResultsActivity::class.java)
        intent.putExtra(keys[0],screenType)
        intent.putExtra(keys[1],completeType)
        activity.startActivityForResult(intent,requestType)
    }

}