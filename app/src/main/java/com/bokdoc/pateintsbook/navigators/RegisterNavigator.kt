package com.bokdoc.pateintsbook.navigators

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.bokdoc.pateintsbook.ui.login.LoginActivity
import com.bokdoc.pateintsbook.ui.register.RegisterActivity

object RegisterNavigator {
    fun navigate(context: Context){
        val intent = Intent(context, RegisterActivity::class.java)
        context.startActivity(intent)
    }

    fun navigateWithExits(activity: Activity){
        val intent = Intent(activity, RegisterActivity::class.java)
        activity.startActivity(intent)
    }
}