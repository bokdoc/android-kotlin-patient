package com.bokdoc.pateintsbook.navigators

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.bokdoc.pateintsbook.ui.tutorials.TutorialsActivity

object TutorialsNavigator {
    fun navigate(context: Context){
        val intent = Intent(context,TutorialsActivity::class.java)
        context.startActivity(intent)
        (context as Activity).finish()
    }
}