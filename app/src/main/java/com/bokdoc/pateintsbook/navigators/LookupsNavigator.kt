package com.bokdoc.pateintsbook.navigators

import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.bokdoc.pateintsbook.R
import com.bokdoc.pateintsbook.data.models.lookups.LookUpsParcelable
import com.bokdoc.pateintsbook.ui.lookups.LookupsActivity

object LookupsNavigator {

    /*
    fun navigate(activity: FragmentActivity, title: String, query: String
                 , selectedTexts: ArrayList<String>, requestCode: Int, isMulti: Boolean = false) {
        val intent = Intent(activity, LookupsActivity::class.java)
        intent.putExtra(activity.getString(R.string.lookupsTitle), title)
        intent.putExtra(activity.getString(R.string.lookupsQuery), query)
        if (isMulti) {
            intent.putExtra(activity.getString(R.string.is_multi), activity.getString(R.string.is_multi))
            intent.putExtra(activity.getString(R.string.selected_texts_key), selectedTexts)
        } else {
            var text = ""
            if (selectedTexts.isNotEmpty())
                text = selectedTexts[0]
            intent.putExtra(activity.getString(R.string.selected_text_key), text)
        }
        Navigator.navigate(activity, intent, requestCode)
    }

    fun navigateWithLookups(activity: FragmentActivity, title: String, query: String
                            , selectedLookups: ArrayList<LookUpsParcelable>, requestCode: Int, isMulti: Boolean = false,
                            description: String = "",
                            key:String = "",values:Array<String> = Array(0){""}) {
        val intent = Intent(activity, LookupsActivity::class.java)
        intent.putExtra(activity.getString(R.string.lookups_title), title)
        intent.putExtra(activity.getString(R.string.lookups_query), query)
        if (description.isNotEmpty()) {
            intent.putExtra(activity.getString(R.string.screen_desc), description)
        }
        intent.putParcelableArrayListExtra(activity.getString(R.string.lookups), selectedLookups)
        if (isMulti) {
            intent.putExtra(activity.getString(R.string.is_multi), activity.getString(R.string.is_multi))
        }
        if(key.isNotEmpty()){
            intent.putExtra(key,values)
        }
        Navigator.navigate(activity, intent, requestCode)
    }
    */
    fun navigate(activity: FragmentActivity, title: String, query: String
                 , selectedLookups: ArrayList<LookUpsParcelable>, requestCode: Int, isMulti: Boolean = false,
                 description: String = "",
                 key:String = "", values:Array<String> = Array(0){""}) {
        val intent = Intent(activity, LookupsActivity::class.java)
        intent.putExtra(activity.getString(R.string.lookupsTitle), title)
        intent.putExtra(activity.getString(R.string.lookupsQuery), query)
        intent.putParcelableArrayListExtra(activity.getString(R.string.selected_lookups), selectedLookups)
        if (isMulti) {
            intent.putExtra(activity.getString(R.string.is_multi), activity.getString(R.string.is_multi))
        }
        if(key.isNotEmpty()){
            intent.putExtra(key,values)
        }
        Navigator.navigate(activity, intent, requestCode)
    }
    fun navigate(fragment: Fragment, title: String, query: String
                 , selectedLookups: ArrayList<LookUpsParcelable>, requestCode: Int, isMulti: Boolean = false,
                 description: String = "",
                 key:String = "", values:Array<String> = Array(0){""}) {
        val intent = Intent(fragment.activity, LookupsActivity::class.java)
        intent.putExtra(fragment.getString(R.string.lookupsTitle), title)
        intent.putExtra(fragment.getString(R.string.lookupsQuery), query)
        intent.putParcelableArrayListExtra(fragment.getString(R.string.selected_lookups), selectedLookups)
        if (isMulti) {
            intent.putExtra(fragment.getString(R.string.is_multi), fragment.getString(R.string.is_multi))
        }
        if(key.isNotEmpty()){
            intent.putExtra(key,values)
        }
        Navigator.navigate(fragment, intent, requestCode)
    }
}