package com.bokdoc.pateintsbook.navigators

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.content.ContextCompat.startActivity
import com.bokdoc.pateintsbook.R


object Navigator {

    fun navigate(activity: Activity, keys: Array<String>, values: Array<String>, to: Class<*>) {
        val intent = Intent(activity, to)
        val count = keys.count()
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        activity.startActivity(intent)
    }

    fun navigateFromBottom(activity: Activity, keys: Array<String>, values: Array<String>, to: Class<*>) {
        val intent = Intent(activity, to)
        val count = keys.count()
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);

    }

    fun navigate(activity: Activity, keys: Array<String>, values: Array<String>, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        val count = keys.count()
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigateWithClearTop(activity: Activity, keys: Array<String>, values: Array<String>, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        val count = keys.count()
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigate(activity: Activity, key: String, parcelableList: ArrayList<out Parcelable>,
                 keys: Array<String>, values: Array<Any>, to: Class<*>) {
        val intent = Intent(activity, to)
        intent.putParcelableArrayListExtra(key, parcelableList)
        val count = keys.count()
        (0 until count).forEach { position ->
            values[position].let {
                when (it) {
                    is Boolean -> intent.putExtra(keys[position], it)
                    else -> {
                        intent.putExtra(keys[position], it as String)
                    }
                }
            }

        }
        activity.startActivity(intent)
    }

    fun navigate(activity: Activity, intent: Intent, requestCode: Int) {
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigate(fragment: Fragment, intent: Intent, requestCode: Int) {
        fragment.startActivityForResult(intent, requestCode)
    }


    fun navigate(activity: Activity, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigate(fragment: Fragment, to: Class<*>, requestCode: Int) {
        val intent = Intent(fragment.activity, to)
        fragment.startActivityForResult(intent, requestCode)
    }


    fun navigate(activity: Activity, key: String, values: ArrayList<out Parcelable>, to: Class<*>) {
        val intent = Intent(activity, to)
        intent.putParcelableArrayListExtra(key, values)
        activity.startActivity(intent)
    }

    fun navigate(activity: Activity, key: String, value: Parcelable, to: Class<*>) {
        val intent = Intent(activity, to)
        intent.putExtra(key, value)
        activity.startActivity(intent)
    }

    fun navigate(activity: Activity, key: String, value: Parcelable, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        intent.putExtra(key, value)
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigate(activity: Activity, key: String, values: ArrayList<out Parcelable>, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        intent.putParcelableArrayListExtra(key, values)
        activity.startActivityForResult(intent, requestCode)
    }

    fun navigate(activity: Activity, keys: Array<String>, values: Array<String>, parcelableKey: String, parcelableList: ArrayList<out Parcelable>, to: Class<*>, requestCode: Int) {
        val intent = Intent(activity, to)
        val count = keys.count()
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        intent.putParcelableArrayListExtra(parcelableKey, parcelableList)
        activity.startActivityForResult(intent, requestCode)
    }

    //fun navigate(fragment:Fragment,keys: Array<String>,values:Array<String>,to:Class<*>,requestCode:Int){}

    fun navigateWithExist(activity: Activity, keys: Array<String>, values: Array<String>, to: Class<*>) {
        navigate(activity, keys, values, to)
        activity.finish()
    }

    fun navigateToBrowser(activity: Activity, url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        activity.startActivity(browserIntent)
    }

    fun navigateToVideoPlayer(activity: Activity, url: String, videoType: String) {
        val intentUri = Uri.parse(url)
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(intentUri, videoType)
        activity.startActivity(intent)
    }

    fun naviagteBack(activity: Activity, keys: Array<String>, values: Array<String>) {
        val intent = Intent()
        val count = keys.size
        (0 until count).forEach {
            intent.putExtra(keys[it], values[it])
        }
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }

    fun navigateBack(activity: Activity) {
        activity.setResult(Activity.RESULT_OK, Intent())
        activity.finish()
    }

    fun navigate(activity: Activity, to: Class<*>) {
        val intent = Intent(activity, to)
        activity.startActivity(intent)

    }

    fun navigateWithClearStack(activity: Activity, to: Class<*>) {
        val intent = Intent(activity, to)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        activity.startActivity(intent)
    }

    fun navigate(activity: Activity, key: String, values: Array<String>, to: Class<*>) {
        val intent = Intent(activity, to)
        intent.putExtra(key, values)
        activity.startActivity(intent)
    }

    fun naviagteBack(activity: Activity, key: String, value: Parcelable) {
        val intent = Intent()
        intent.putExtra(key, value)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }


    fun naviagteBack(activity: Activity, key: String, values: ArrayList<out Parcelable>) {
        val intent = Intent()
        intent.putExtra(key, values)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }

    /*
    * This fun should moved to another class in refactor  name OutsideNavigator
    * */
    fun share(context: Context, url: String) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, url)
        sendIntent.type = "text/plain"
        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.share)))
    }


    fun navigateBack(activity: Activity, key: String, values: Array<String>) {
        val intent = Intent()
        intent.putExtra(key, values)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }


}