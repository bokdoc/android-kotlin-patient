package com.bokdoc.pateintsbook.navigators

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.bokdoc.pateintsbook.ui.homeSearch.HomeSearchActivity

object HomeNavigator {
    fun navigate(context: Context,selectedPositionKey:String,selectedPositionValue: Int){
        val intent = Intent(context, HomeSearchActivity::class.java)
        intent.putExtra(selectedPositionKey,selectedPositionValue)
        context.startActivity(intent)
    }

    fun naviagteBack(activity: Activity,keys:Array<String>,values:Array<String>){
        val intent = Intent()
        val count = keys.size
        (0 until count).forEach {
            intent.putExtra(keys[it],values[it])
        }
        activity.setResult(Activity.RESULT_OK,intent)
        activity.finish()
    }
}