package com.bokdoc.pateintsbook.navigators

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.bokdoc.pateintsbook.ui.login.LoginActivity

object LoginNavigator {
    fun navigate(context: Context){
        val intent = Intent(context,LoginActivity::class.java)
        context.startActivity(intent)
    }

    fun navigateWithExist(activity: Activity){
        val intent = Intent(activity,LoginActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}