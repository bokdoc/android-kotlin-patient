package com.bokdoc.pateintsbook

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.support.multidex.MultiDex
import android.util.Log
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.TwitterConfig
import com.bokdoc.pateintsbook.utils.ui.LocaleManager
import com.google.firebase.iid.FirebaseInstanceId
import java.util.*


class PatientApp : Application() {


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(baseContext!!)
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        val config = TwitterConfig.Builder(this)
                .logger(DefaultLogger(Log.DEBUG))//enable logging when app is in debug mode
                .twitterAuthConfig(TwitterAuthConfig(resources.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), resources.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))//pass the created app Consumer KEY and Secret also called API Key and Secret
                .debug(true)//enable debug mode
                .build()

        Twitter.initialize(config)


    }
}